#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>			// Header File For The GLu32 Library
#include "ManipulatorNSections.h"

#include <string>
#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/common/transforms.h>
#include <pcl/point_cloud.h>
#include <pcl/point_representation.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/surface/gp3.h>
#include <pcl/ModelCoefficients.h>
#include <pcl/features/normal_3d.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/sample_consensus/method_types.h>
#include <pcl/sample_consensus/model_types.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/segmentation/extract_clusters.h>

#include <algorithm>    // std::sort
#include <math.h> 
#include <Eigen/Dense>
#include <stdlib.h>

//~ #define DEBUG

extern vector<vector<float> > g_configs_todraw;
extern vector<vector<float> > g_base_frames_todraw;

using namespace std;
typedef OpenMesh::TriMesh_ArrayKernelT<>  MyMesh;
typedef OpenMesh::PolyMesh_ArrayKernelT<> MyMesh_poly;

class Object // represented by meshes
{
public:
	MyMesh mesh;
	MyMesh_poly mesh_poly;
	vector<float> basetransform;
	vector<bool> seen_faces;
	
	
	Object(MyMesh _myMesh, vector<float> _basetransform);
	Object(MyMesh_poly _myMesh, vector<float> _basetransform);
	Object(string infilename, vector<float> _basetransform);
	void getObject(MyMesh &Outmesh, vector<float> basetransform);
	void drawObject();
	void drawObject(float translation_3f [3], float scale_3f [3], float orientation[9]);
	
	void DrawInFrustumObject(float Frustum[6][4]);
	void get_mesh(MyMesh &_Outmesh);
	
	void drawObject_poly(int max_idx_face);
};

class PartialConstCurve3D
{
public:
	vector<float> center;
	vector<float> u_axis;
	vector<float> v_axis;
	vector<float> n_axis;
	vector<float> basept;
	vector<float> tippt;
	float radious;
	float length;
	PartialConstCurve3D(vector<float> _tippt,vector<float> _basept, vector<float> _center, vector<float> _u_axis, vector<float> _v_axis, vector<float> _n_axis,float _radious,float _length);
	
};

class fetching_unknown_objects_test_case_parameters
{
	public:
		fetching_unknown_objects_test_case_parameters();
		void initialize();
		// arm base frame
		std::vector<float> arm_base_position;
		std::vector<float> arm_base_x;
		std::vector<float> arm_base_y;
		std::vector<float> arm_base_z;
		
		std::vector<float> arm_starting_config;
		std::vector<string> environment_object_model_pcd_paths;
		
		std::vector<float> env_cloud_position_adjuster_1;
		std::vector<float> env_cloud_position_adjuster_2;
		std::vector<float> env_cloud_position_adjuster_3;
		std::vector<float> env_cloud_position_adjuster_4;
		
		float corner_angle_threshold;
		
		std::vector<float> arc1_dist_to_coord;
		std::vector<float> arc2_dist_to_coord;
		std::vector<float> arc3_dist_to_coord;
		
		std::vector<float> line_motion_direction;
		
		std::string partial_model_path;
		
		std::vector<std::vector<float> > partial_model_position_adjusters;
};

class Environment
{
public:
	
	int n_objects;
	GLUquadric* quadratic_x;
	GLUquadric* quadratic_y;
	GLUquadric* quadratic_z;
	
	vector<Object> objects;
	ManipulatorNSections* mani;

	vector<pcl::PointCloud<pcl::PointXYZRGBA>::Ptr> point_clouds_;
	
	int min_cluster_pnt_size;
	int clustered_pnt_clouds_ind;
	float cluster_tolerance;
	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr newly_sensed_pnt_clouds;
	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr sensed_pnt_clouds;
	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr next_model_pcl;
	int current_model_index;
	fetching_unknown_objects_test_case_parameters parameters;
	
	Environment(float* _mani_config, int n_ManiSections, float* _maniBaseFrame,vector<Object> objects);
	Environment(float* _mani_config, int n_ManiSections, float* _maniBaseFrame);
	Environment(float* _mani_config, int n_ManiSections, float* _maniBaseFrame, const vector<string> & pcd_paths);
	Environment(float* _mani_config, int n_ManiSections, float* _maniBaseFrame, const vector<string> & pcd_paths, const string & mesh_obj_path);
	
	void ExtractFrustum(float frustum[6][4]);
	void drawWorldCoord();
	void drawObjects();
	void drawObjects(float translation[3], float scale[3], float orientation [9]);
	void drawObjects_poly(int max_idx_face);
	
	void drawPointsCloud();
	void drawPointsCloud_DepthColor(float _eye_pos[3], float _look_at_point[3]);
	void drawPointsCloud(vector<int> indices_pt_clouds);
	void SetObjFrameAsReferenceFrameForPointsCloud();
    void ScaleAndPositioningPointsClouds(const float scale, const vector<vector<float> > &point_cloud_positions);
	
	void drawManipulator();
	bool CreatCylinderTriMeshes(MyMesh& OutputMesh, float base [3],float central_axis_dir[3], float bend_dir[3], float bend_plane_normal[3], float cylind_radius, float cur_cen [3], float cur_rad,float length);
	void drawManipulator_Mesh();
	void drawManipulator_Mesh_with_tip_cam();
	
	void drawMeshObjectsWithTipEye();
	void drawMeshObjectsWithTipEye_SubViewPort();
	void drawPointCloudObjectsWithTipEye(vector<int> _point_cloud_indices, vector<bool> &_points_seen);
	
	bool update_arm_config(float* _mani_config);
	bool update_arm_config(vector<float> arm_config); // only update the configuration, with fixed base
	bool update_arm_config(vector<float> arm_config, vector<float> base_frame_update);// also update the base frame
	bool get_cur_arm_config(vector<float> & config);
	bool get_cur_arm_config_wbase(vector<float> & config, vector<float>& base_frame);
	
	
	ManipulatorNSections* get_mani_in_env();
	void get_objects_in_env(vector<Object> &objects);

	void SecPlaneIntersecPoly(int sec_idx, int objec_idx, vector<vector<float> >& intersectedPoly);
	
	void drawAnPolygon(vector<vector<float> >&polygon);
	void drawAnPolygon_visib(vector<vector<float> >&polygon, vector<bool>& visible);
	void labelVisiVexsOnPolyByTip(vector<vector<float> > &ConvexPolygon, vector<bool> &visibleVector, int obj_idx);
	void fastTriangulationOfUnorderedPointClouds( pcl::PointCloud<pcl::PointXYZRGBA>::Ptr& cloud,  pcl::PolygonMesh& triangles);
	
	// process point clouds
	void printSensedPclInfo();
	void drawSensedPcl();
	void clusterNewlySensedPcl();
	bool mergeNewCluster(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &new_cls);
	void findEdgePnts(float final_tgt_pnt[3], float fgp_pln_normal[3]);
	// generate motion plan towards a given point with sec1/2 endpoints interpolated
	bool generateLineMotionGivenTgtPntNormalAndLineDirection(float final_tgt_pnt[3], 
															 float pln_normal_array[3],
															 float line_direction[3]);
	// returns an arm configuration satisfying specified tip position and tip orientation
	// with sec1/2 endpoints interpolated
	bool wholeArmIkByTipPose(float arm_tip_orientation[3], float tgt_pnt[3],
							 float sec2_vec_scale, float sec1_vec_scale);
	bool generateAdjustedArcMotionGivenTgtPnt(float tgt_pnt[3],int corner_ind);
	bool generateArcMotionGivenTgtPntPlaneNormalAndDist(float tgt_pnt[3],int corner_ind);
	bool wholeArmIkByTgtPntAndArc2CordDistAndNormal( float arc2cord_dist, 
													 float sec1_tgt_pnt[3], // sec1 end pnt
													 float arc_start[3],    // sec2 end pnt
													 float tgt_pnt[3],      // sec3 end pnt
													 float arc_cir_plane_normal[3]);
	bool tgtPntPlaneFitting(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &new_tgt_pnts_cl,
								 std::vector<float> &plane_normal);
	void loadTargetObjPartialModel(int model_index, bool isCW);
	void drawNextModelPcl();
	bool loadPartialModelImage(int model_ind);
	void drawPartialModelImageInWindowCorner();
	void drawSpecifiedPointCloud(int pcl_ind);
};

#endif
