/*
 * environment_with_tactile_sensing.h
 * 
 * Copyright 2016 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef INTERFACE_H
#define INTERFACE_H

#include <Eigen/Dense>
#include <iostream>
#include <fstream>
#include <cmath>
#include <ctime>
#include "MotionPlanner.h"
#include "ArcBall.h"
#include <string>
#include "Timer.h"

#include "robotobjectinteraction.h"

using namespace std;

float PI_jinglin = 3.14159265358979323846;

/************** OpenGL global variables***********/

// Main Window setting
float Look_x = 0.1;  float Look_y = 0.3;  float Look_z = 1.5;
float refer_x = 0.0; float refer_y = 0.3; float refer_z = 1.0;
float up_x = 0.0;    float up_y = 1.0;    float up_z = 0.0;

float perspecive_fovy = 45.0;  float ratial = 1.0; float near_plane = 0.01; float far_plane = 600.0;

Matrix4fT   Transform   = {  1.0f,  0.0f,  0.0f,  0.0f,			// NEW: Final Transform
                             0.0f,  1.0f,  0.0f,  0.0f,
                             0.0f,  0.0f,  1.0f,  0.0f,
                             0.0f,  0.0f,  0.0f,  1.0f };

Matrix3fT   LastRot     = {  1.0f,  0.0f,  0.0f,		// NEW: Last Rotation
                             0.0f,  1.0f,  0.0f,
                             0.0f,  0.0f,  1.0f };

Matrix3fT   ThisRot     = {  1.0f,  0.0f,  0.0f,		// NEW: This Rotation
                             0.0f,  1.0f,  0.0f,
                             0.0f,  0.0f,  1.0f };

ArcBallT ArcBall(640.0f, 480.0f); // NEW: ArcBall Instance THIS IS FIXED BOUND(640 BY 480)
Point2fT MousePt;				  // NEW: Current Mouse Point
bool isClicked  = false;	      // NEW: Clicking The Mouse?
bool isRClicked = false;	      // NEW: Clicking The Right Mouse Button?
bool isDragging = false;	      // NEW: Dragging The Mouse?

// SecondWindow setting

#endif /* INTERFACE_H */ 
