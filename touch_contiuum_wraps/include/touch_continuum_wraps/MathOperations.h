#ifndef MATHOPERATIONS_H
#define MATHOPERATIONS_H

#include <vector>
#include <math.h>
#include <stack>
#include <cfloat>
#include <stdio.h>
#define pi_jinglin 3.1415926
#define Pi_jinglin 3.1415926
#include <Eigen/Dense>

using namespace std;



class _Point
{
public:
	float x,y,z;
	_Point(float x_, float z_);
	_Point();
	_Point(float x_, float y_, float z_);
	void Normalize();
	void SetValue(float x_, float z_);
	void SetValue(float x_,float y_, float z_);
	float GetLength();
};

float* MatrixMulti(float* a, float* b, int R_1, int C_1, int R_2, int C_2);
float* MatrixMulti(vector<float> a, float* b, int R_1, int C_1, int R_2, int C_2);
float* MatrixMulti(vector<float> a, vector<float> b, int R_1, int C_1, int R_2, int C_2);
float* MatrixMulti(float* a, vector<float> b, int R_1, int C_1, int R_2, int C_2);
bool Normalize(vector<float> &_vector);
void CrossProduct(float*a, float*b, float*result);
float* CrossProduct(float* a, float* b);
void DotProduct(float*a, float*b, float* result);
float DotProduct(float*a, float*b);
void Normalize(float* a, int dim);
float* TwolineIntersection(float* Pa, float* Pb,float* va, float* vb);
float* InverMatrix(float* Matrix); // only 4 by 4 matrix
float* InverMatrix(float* Matrix, int _row, int _column); //All in column Majors, pusdo-inverse

bool plane_rotating_angle(float* _x_af_rot, float* _y_af_rot, float* _z_axis, float* _point1_on_z, float* _point2);
float line_line_dist_3D(float* l1_p1, float* l1_vec, float* l2_p1, float* l2_p2);
float point_lineseg_dist_3D(float* p, float* lineseg_p1, float* lineseg_p2);
float two_pts_dist(float* p1, float* p2, int dim);
float two_pts_dist(vector<float> p1, vector<float> p2);
float two_pts_dist(Eigen::Vector3f p1, Eigen::Vector3f p2);

bool Proj_Pt_to_Plane (float point_to_proj [3], float point_on_plane[3], float plane_normal[3], float projection_des[3]);
bool Proj_vec_to_Plane (float vec [3], float vec_base [3], float point_on_plane[3], float plane_normal[3], float projection_des [3]);
bool Point2LinesegDist3D(float p1seg[3], float p2seg[3], float p[3], vector<float>& output);// output is in format of float dist,float t, float cl_on_seg
float norm_vec_3 (float vec [3]);
float Angle_bewteen_two_vecs(float* v_1, float* v_2);

vector<float>  plane_edge_intersection(vector<float> plane_normal, vector<float> a_point_on_plane, vector<float> edge_p1, vector<float> edge_p2);

vector<float> plane_edge_intersection(vector<float> plane_normal, vector<float> a_point_on_plane, Eigen::Vector3f edge_p1, Eigen::Vector3f edge_p2);

vector<vector<float> >Compute_ConvexHull_3d_to_2D(vector<vector<float> > &points_on_same_plane,vector<float> plane_normal, vector<float> a_point_on_plane);

bool compute_circle_given_twopoints_one_tangentVec(float p1[3], float p2[3], float tangentV_on_p1 [3], float cir_plane_normal[3], float cir_cen [3], float& cir_rad);

bool compute_circle_given_twopoints_arc2cordDis(float p1[3], float p2[3], float arc2cordDis, float cir_plane_normal[3], float cir_cen [3], float& cir_rad);

bool compute_circle_given_twopoints_arc2cordDis2ndSol(float p1[3], float p2[3], float arc2cordDis, float cir_plane_normal[3], float cir_cen [3], float& cir_rad);

void RotAVec(float unit_axis [3], float vector_tobe_rot [3], float theta_rt, float rotated_vec[3]);
void RotA3by3Frame(float unit_axis [3], float frame_tobe_rot [9], float theta_rt, float rotated_frame[9]);

/****HM*****/
//convert a regular 4X4 tf matrix to 6d vector <pos_x, pos_y, pos_z, RPY> -->BUG FOUND! cos(beta) may be zero!!!
Eigen::VectorXd regular_eigen_tf_matrix_to_vector (Eigen::Matrix4f given_tf_matrix);
bool isRotCW (float vec1[3], float vec2[3]);
class CVector
{

public:
	float *mVect;
	int length;
        CVector(float *v,int nLen);
        CVector();
        void setv(float *v,int nLen);
};
float gVectDP(CVector *a,CVector *b);
float *gVectCP(CVector *a,CVector *b);
bool TwolineIntersect(float P1[3], float P2[3], float Q1[3], float Q2[3],float *InterP);

#endif
