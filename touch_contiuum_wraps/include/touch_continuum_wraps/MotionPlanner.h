#ifndef MOTIONPLANER_H
#define MOTIONPLANER_H

#include "Environment.h"

class TaskConstraints
{
public:
	vector<float> Pos;
	vector<float> z_axis;
	vector<float> y_axis;
	vector<float> x_axis;
	TaskConstraints(float _Pos[3], float _tip_z_axis[3], float _tip_y_axis[3]);
	TaskConstraints(const TaskConstraints& other);
	TaskConstraints();
	
};

typedef TaskConstraints heuristic_pos;
typedef TaskConstraints Frames;

class feasible_base
{
public:
	int sec_idx;
	heuristic_pos base;
	vector<float> feasible_config;
	vector<float> tip_x;
	vector<float> tip_y;
	vector<float> tip_z;
	vector<float> base_x;
	vector<float> base_y;
	vector<float> base_z;
	
};

class MotionPlanner
{
private:
	int timeStamp;
	Environment *env; // includes objects and manipulator in the environment
	TaskConstraints task_constraints;
	vector<heuristic_pos> heuristic_pos_bases;
	vector<float> planned_config;
	vector<float> planned_arm_base_frame;
	vector<vector<float> > planned_path;
	vector<vector<float> > planned_path_base_frames;

public:
	MotionPlanner(Environment *_env,int _timestamp);
	MotionPlanner(Environment *_env, TaskConstraints _task_constraints,vector<heuristic_pos> _heuristic_pos_bases);

	bool plan_a_config();
	bool compute_a_config_based_on_end_effector(vector<float>& planned_config, vector<float>& planned_config_base_frame, vector<heuristic_pos>heuristic_base_frame);
	bool compute_a_config_based_on_end_effector(vector<float>& planned_config, vector<float>& planned_config_base_frame);
	
	bool compute_section_frames_based_on_end_effector_fixedBase_Orientation(vector<Frames>& arm_frames, vector<float>& arm_config,vector<heuristic_pos>heuristic_base_frame);
	
	bool compute_section_frames_based_on_end_effector_fixedBase_position(vector<Frames>& arm_frames, vector<float>& arm_config,vector<heuristic_pos >heuristic_base_frame);
	
	bool compute_section_frames_based_on_end_effector_fixedBase_position(int n_sections, vector<Frames>& arm_frames, vector<float>& arm_config,vector<heuristic_pos >heuristic_base_frame);
	
	vector<vector<float> > get_planned_path();
	vector<vector<float> > get_planned_path_base_frames();
	
	bool Rescursive_task(vector<Frames>& arm_frames, vector<float>& arm_config,vector<heuristic_pos >heuristic_base_frame);
    bool Rescursive_task_cons(int timeStamp, int sec_idx, vector<heuristic_pos>& out_base_frame, TaskConstraints tipconstraints, vector<vector<feasible_base> >& heuristic_base_poses_all_secs);
};

#endif
