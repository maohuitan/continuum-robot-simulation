#ifndef MANIPULATORNSECTIONS_H
#define MANIPULATORNSECTIONS_H

#include <vector>
#include <cmath>
#include <Eigen/Dense>
#include "MathOperations.h"
#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <stdlib.h>
#include <map>
#include "ros/ros.h"
using namespace std;

struct sectionPhysicalLimits
{
	float length_min, length_max;
	float curvature_min, curvature_max;
	float phi_min, phi_max;
};

typedef std::vector<Eigen::Vector3f> secPnts;

class ManipulatorNSections
{
	private:
		float detaR ; // section width
		GLUquadric* quad; // GLUquadric for drawing secion section
		vector<float> config;
		int n_sections;
		std::map<int, sectionPhysicalLimits> section_bounds;

	public:
		vector<float> base_trans_matrix; // base frame with respect to the world frame, a Column Major matrix
		vector<float> trans_matrix; // tip frame with respect to the base frame (local frame) of the manipulator, a Column Major matrix
		vector<float> trans_matrix_wrt_world;
		
		vector<float> section_cir_centers_wrt_world;
		vector<float> section_cir_centers_local;
		
		vector<float> section_plane_normals_wrt_world;
		vector<float> section_plane_normals_local;
		
		vector<float> section_end_points_wrt_world;

		float* ComputeTransMatrix(float s,float k, float phi); //compute the local transformation matrix given only configurations
		vector<float> compute_trans_matrix_wrt_world();
		int get_n_sections () ;
		vector<float> get_config () ;
		void get_config (float* config_des) ;
		void get_config (vector<float>& config_des) ;
		float get_sec_width();
		
		////Utility functions
		bool compute_section_cir_centers_local( vector<float>& _section_cir_centers);
		bool compute_section_plane_normals_local( vector<float>& _section_plane_normals);
		
		bool compute_trans_matrix(vector<float>& _trans_matrix);
		bool compute_section_cir_centers_wrt_world(vector<float>& _section_cir_centers);
		bool compute_section_plane_normals_wrt_world(vector<float>& _section_plane_normals);
		bool compute_section_end_points_wrt_world(vector<float>& _section_end_points);
		
		bool compute_section_frame_wrt_world(vector<float>& _section_frame, int _sec_idx);
		bool compute_section_frame_wrt_world(float* _section_frame, int _sec_idx);
		
		bool draw_sections();
		bool draw_sections_3_sections();
		
		bool set_base_frame(vector<float> _base_trans_matrix);
		bool set_base_frame(float* _base_trans_matrix);
		float* ComputeJacobian(float* Config);

		vector<float> ComputeJacobian_whole_mobile(float* Config, float* base_pos);
		vector<float> ComputeJacobian_whole_non_mobile(float* Config);
		vector<float> ComputeJacobian_each_sec_tips_pos_non_mobile_base(float* Config, int sec_ind);
	
		ManipulatorNSections(int _n_sections, float* _config, float* _base_frame);
		ManipulatorNSections(int _n_sections, std::vector<float> init_config, Eigen::Matrix4f init_base_frame);

		vector<float> inverseKinematic_one_section(float* _end_point_f, float* base_frame);

		bool instantaneous_inverse_kine_w_SelfM(float* _end_velocity, float* _delta_configs_output,float null_space_motion [9]);
		bool instantaneous_inverse_kine(float* _end_velocity, float* _delta_configs);
		//bool instantaneous_inverse_avoid_pointObs(float* DisteVel, int col_sec_idx, float* _delta_configs_output, float obstacle_pt[3], float para_theta_on_sec);
		
		bool Getbaseframe (float*_out_base_frame);
		bool Getbaseframe (vector<float>&_out_base_frame);
		
		vector<float> get_section_cir_centers_wrt_world();
		vector<float> get_section_plane_normals_wrt_world();
		vector<float> get_section_end_points_wrt_world();

		//update function is important
		bool update_config(vector<float> _config);
		bool update_config(vector<float> _config, vector<float> base_frame);
		
		bool get_trans_matrix_wrt_world(vector<float>& _trans_mat_world);
	
        bool generateKnotConfigs(const std::vector<float> &start_config, const std::vector<float> &start_base_frame, const std::vector<float> &end_config, const std::vector<float> &end_base_frame, std::vector<std::vector<float> >  &knot_configs, std::vector<std::vector<float> >  &knot_base_frames);
        void generatePointsOnArm(std::vector<secPnts> &sec_pnts, const float deta_s);

		bool isCfgWithinPhysicalLimits(const std::vector<float>& cfg);
		std::vector<bool> isSecCfgWithinPhysicalLimits(const std::vector<float>& cfg, const int sec_index);
};
#endif
