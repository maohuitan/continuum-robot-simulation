/*
 * environment_with_tactile_sensing.h
 * 
 * Copyright 2016 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef ENVIRONMENT_WITH_TACTILE_SENSING_H
#define ENVIRONMENT_WITH_TACTILE_SENSING_H

//Project headers
#include <vector>
#include <Eigen/Dense>
#include <math.h>
#include "ManipulatorNSections.h"
#include "MathOperations.h"
#include "meshobject.h"
#include "chordpoint.h"

//ROS service
#include "chord_training_msgs/ChordSrv.h"
#include "chord_training_msgs/ProbWriterSrv.h"
#include "chord_training_msgs/Chord.h"
#include "ros/ros.h"
#include <ros/package.h>
extern ros::ServiceClient client;
extern ros::ServiceClient prob_client;

//ROS messages
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/Vector3.h>
#include <std_msgs/Float32MultiArray.h>
#include <std_msgs/Int32MultiArray.h>

//ROS quaternion conversions
#include <tf/transform_datatypes.h>
#include <tf_conversions/tf_eigen.h>

//OpenGL
#include <FreeImage.h>
#include <GL/glu.h>
#include "SOIL.h"

class environment_with_tactile_sensing
{
	public:
			environment_with_tactile_sensing(int n_secs, 
											std::vector<float> init_config,
											Eigen::Matrix4f init_base_frame,
											string mesh_path,
											string mesh_env_path,
											Eigen::Matrix4f mesh_tf,
											Eigen::Matrix4f mesh_env_tf,
											std::vector<Eigen::Vector3f> ver_list,
											Eigen::Vector3f cross_sec_normal);
			ManipulatorNSections mani;
			meshObject mesh, mesh_env;
			Eigen::Matrix4f arm_init_tip_frame;
			std::vector<std::vector<float> > arm_config_collector;
			std::vector<std::vector<float> > arm_base_frame_collector;
			std::vector<ContactPatch> contacts_collector;
			Face cross_sec, env_cross_sec;
			Face inflated_cross_sec;
			
			// control flags
			bool arm_physical_limit_warning;
			bool arm_ik_not_found_warning;
			bool enough_tip_z_axis_rot_flag;
			bool final_sequeeze_finished_flag;
			bool empty_cross_sec_flag;
			
			// motion parameters
			float sec_curvature_adjust_rate;  
			float sec_len_adjust_rate;
			float sec_endpnt_moving_dist; // a small distance that sec end points moving for
			float angle_tip_rot_threshold;
			
			// chords container
			std::vector<chord_training_msgs::Chord> chords;

			int num_of_screenshots;

			/* 
				Collision checking against a mesh model cross section
				Caution! Jinglin's collision checker implementation has bug
				of checking straight line intersection!!!!
				Use curvature 0.1 for near-straight line representation
			*/
			// fast collision check. return once one contact is found
			bool armSectionPolygonCollisionCheck(Face &InterPolygon,int SectionIdx,int &colliding_edge_ind);
			bool armPolygonCollisionCheck(Face &cross_sec, std::vector<bool> &colliding_sec_status, std::vector<int> &sec_colliding_edge_inds);
			// full collision check. returns all the contacts
			bool armSectionPolygonFullCollisionCheck(Face &InterPolygon, int SectionIdx, std::vector<int> &colliding_edge_inds);
			bool armPolygonFullCollisionCheck(Face &cross_sec, std::vector<bool> &colliding_sec_status, std::vector<int> &sec_colliding_edge_inds);

			/* 
				Touch-based continuum wraps
				given ground truth cross section, 
				achieve continuum grasping (collision check stops once a contact is found).
				Section endpoints interpolation only depend on one contact per section
			*/ 
			bool touchDrivenGraspHW();
			int curveOneSectionUntilContact(int sec_ind);

			/* 
				Touch-based wraps on Origami robot
			*/
			bool touchDrivenGraspHWOrigami();
			int curveOneSectionUntilContactOrigami(int sec_ind);
			bool armSectionPolygonCollisionCheckOrigami(int SectionIdx,int &colliding_edge_ind);
			bool armPolygonCollisionCheckOrigami(std::vector<bool> &colliding_sec_status, std::vector<int> &sec_colliding_edge_inds);

			/* 
				check the angle tip z-axis has rotated
			*/
			void checkTipZaxisRotatedAngle();
			/* 
				based on section plane normal and cross section normal, 
				determine the curvature adjusting sign to sweep-until-contact 
			*/
			bool determineCurvatureAdjustSign(int sec_id);
			/* 
				check arm collision with inflated cross section and save contacts 
			*/
			void collectFullContacts();
			/* 
				given an arm config and base frame, generate chords  
			*/
			void generateChords(const std::vector<float> &arm_config,const std::vector<float> &arm_base_tf);
			void cartesian2sphere(Eigen::Vector3f &pnt_in_cart, Eigen::Vector3f &pnt_in_sp);
			void writeChords();
			void writeProbabilities(std::string &cat, std::string &base, int hist_cnt, bool valid_signal, bool finish_signal);
			/* 
				Whole arm IK flexible base 
				given section end points and last section arc2coord distance, find whole arm ik
				note this function has a known bug. It either computes all shorter arcs for 3 sections or longer arcs.
				Cannot compute combinations of shorter and longer arcs. [To-Fix]
				The current implementation only works for 3 sections!
			*/
			bool wholeArmIkByTgtPntAndArc2CordDistAndNormal(float arc2cord_dist, float sec1_tgt_pnt[3],
															float arc_start[3], float tgt_pnt[3], float arc_cir_plane_normal[3]); 
			/* 
				Environment reset
			*/
			/* reset arm to do a grasping in a different cross section */
			void resetEnv(int ort_choice, float proportion);
			/* reset arm according to the relative tf (at testing mode) */
			void resetEnvByPlnNormal(geometry_msgs::Vector3 &plane_normal);
			/* 
				Utility functions
			 */
			void takeScreenshot(const std::string &obj_name);
			void reportRobotStatus();
			/* visualize collected contacts. One contact is drawn as a point. */
			void visualizeCollectedContacts();
			void visualizeCrossSection();
			void visualizeEnvCrossSection();
			/* ground-truth cross section is inflated by a scaling factor */
			void visualizeInflatedCrossSection();

			void robotStateCallBack(const std_msgs::Float32MultiArray::ConstPtr& msg);
			void contactStateCallBack(const std_msgs::Int32MultiArray::ConstPtr& msg);
			
			ros::Publisher motion_cmd_pub;
			std::vector<std::vector<int> > cnt_history;
	private:
			ros::Subscriber robot_state_sub, contact_state_sub;
			std::vector<int> section_cnt_state;
	
};

#endif /* ENVIRONMENT_WITH_TACTILE_SENSING_H */ 
