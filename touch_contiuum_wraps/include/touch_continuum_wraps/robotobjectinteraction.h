/*
 * robotobjectinteraction.h
 * 
 * Copyright 2016 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef ROBOTOBJECTINTERACTION_H
#define ROBOTOBJECTINTERACTION_H
//Project header files
#include "environment_with_tactile_sensing.h"
#include "objwrapsettings.h"

//ROS
#include <ros/package.h> 

//ROS service
#include "ros/ros.h"
#include "touch_continuum_wraps/wrapByPlane.h"

//ROS messages
#include <geometry_msgs/Quaternion.h>

//System header files
#include <unistd.h>

//C++
#include <iostream>
#include <fstream>
#include <ctime>
#include "boost/date_time/posix_time/posix_time.hpp"

//Project macros
#define TAKESCREENSHOTS
// #define ONLYVISFINALCONFIG //only visualize key configs to save time
// #define SAVEPROBS // save probabilities

class robotObjectInteraction
{
	public:
		robotObjectInteraction();
                virtual ~robotObjectInteraction();
                void drawEnvironment();
                void drawManipulator();
                void drawCrossSectionAndSensedContacts();
                void runChordWriterPY(bool isPerWrap);
                void runChordWriterPYTesting(std::string chord_writer_cmd);
                void runProbWriterPY();
                void test();
                void printOptions();
                bool wrapNextPlane(touch_continuum_wraps::wrapByPlane::Request& req, touch_continuum_wraps::wrapByPlane::Response& res);
                void motionGenerationHardware();
                void motionGenerationHardwareOrigamiRobot();
                void saveCfgsToFile();
                void chordsGeneration();
			
	private:
		environment_with_tactile_sensing *env_tactile; 
                int drawingIndex;
                std::string obj_name;
                std::string obj_cat;
                std::vector<objWrapSettings> ows;
                std::ofstream log_file;  
                int hist_write_cnt;     
                
                int program_mode;    
                bool next_plane_avail; // if a new plane is specified (only in testing mode)  
                bool wrap_service_called; 
                bool testing_mode_chord_wrt_sgn;  
                // accumulative chords container
                std::vector<chord_training_msgs::Chord> accum_chords;

                /* passed arm cfgs for stats */
                std::vector<std::vector<float> > passed_arm_cfgs;
                std::vector<float> passed_arm_base_frame;

                bool terminated; 
};

#endif /* ROBOTOBJECTINTERACTION_H */ 
