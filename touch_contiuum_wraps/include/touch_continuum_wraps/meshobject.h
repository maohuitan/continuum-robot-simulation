/*
 * meshobject.h
 * 
 * Copyright 2016 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#ifndef MESHOBJECT_H
#define MESHOBJECT_H

#include <OpenMesh/Core/IO/MeshIO.hh>
#include <OpenMesh/Core/Mesh/TriMesh_ArrayKernelT.hh>
#include <OpenMesh/Core/Mesh/PolyMesh_ArrayKernelT.hh>

#include <Eigen/Dense>

#include <string>
#include <iostream>

#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>

#include "MathOperations.h"

typedef OpenMesh::TriMesh_ArrayKernelT<>  triMesh;
typedef OpenMesh::PolyMesh_ArrayKernelT<> polyMesh;

class Edge
{
	public:
		Eigen::Vector3f pnt1;
		Eigen::Vector3f pnt2;
		
		Edge(Eigen::Vector3f p1, Eigen::Vector3f p2);
                Edge();
};

class Face
{
	public:
	    Eigen::Vector3f face_normal;
	    std::vector<Edge> face_edges;
            std::vector<Eigen::Vector3f> face_vertices;
	    
	    Face(std::vector<Eigen::Vector3f> vertex_list, Eigen::Vector3f normal); // every 2 vertices make an edge
            void setFace(std::vector<Eigen::Vector3f> vertex_list, Eigen::Vector3f normal);
};

class BoundingCircle
{
       public:
            Eigen::Vector3f center;
            Eigen::Vector3f normal;
            float radius;

            BoundingCircle(Eigen::Vector3f cir_center, Eigen::Vector3f cir_normal, float rad);
};

class ContactPatch
{
       public:
            Eigen::Vector3f tangent;
            Eigen::Vector3f normal;
            Eigen::Vector3f center;
            Edge edge;
            bool isEmpty;

            ContactPatch(Eigen::Vector3f contact_tangent, Eigen::Vector3f contact_normal, Edge contact_edge);
            ContactPatch(bool isNull);
};


class meshObject
{
	public:
		meshObject(std::string tri_mesh_path, Eigen::Matrix4f mesh_tf);
                void drawOrgTriMesh();
		void drawOrgTriMeshShaded();
		void drawTransformedTriMesh(float translation[3], float scale[3], float orientation[9]);
                // compute intersection of a plane and mesh
	        Face planeIntersectTriMesh(const std::vector<float> &plane_normal, const std::vector<float> &pnt_on_plane);	
                // compute a bounding circle of a polygon face
                BoundingCircle computeBoundingCicle(const Face &polygon_face);
              
	private:
		triMesh tri_mesh;
		polyMesh poly_mesh;
		Eigen::Matrix4f mesh_transform;
};

#endif /* MESHOBJECT_H */ 
