#include "MotionPlanner.h"
#include<assert.h>

TaskConstraints::TaskConstraints(float _Pos[3], float _z_axis[3], float _y_axis[3])
{
	Pos = vector<float>(3);
	z_axis = vector<float>(3);
	y_axis = vector<float>(3);
	x_axis = vector<float>(3);
	for(int i =0; i < 3; ++i)
	{
		Pos[i] = _Pos[i];
		z_axis[i] = _z_axis[i];
		y_axis[i] = _y_axis[i];
	}
	float _x_axis [3] = {0};
	CrossProduct(_y_axis,_z_axis,_x_axis);
	for(int i =0; i < 3; ++i)
	{
		x_axis[i] = _x_axis[i];
	}
}
TaskConstraints::TaskConstraints()
{
	Pos = vector<float>(3,0.0);
	z_axis = vector<float>(3,0.0);
	y_axis = vector<float>(3,0.0);
	x_axis = vector<float>(3,0.0);
}

TaskConstraints::TaskConstraints(const TaskConstraints& other)
{
	Pos = vector<float>(3);
	z_axis = vector<float>(3);
	y_axis = vector<float>(3);
	x_axis = vector<float>(3);
	for(int i =0; i < 3; ++i)
	{
		Pos[i] = other.Pos[i];
		z_axis[i] = other.z_axis[i];
		y_axis[i] = other.y_axis[i];
		x_axis[i] = other.x_axis[i];
	}
}

MotionPlanner::MotionPlanner(Environment *_env, TaskConstraints _task_constraints, vector<heuristic_pos> _heuristic_pos_bases)
{
	assert(_env);
	
	ManipulatorNSections* mani_in_env = _env->get_mani_in_env();
	int n_section_mani_env = mani_in_env->get_n_sections();
	float* config_mani_env  = new float [n_section_mani_env*3];
	mani_in_env->get_config(config_mani_env);
	
	float base_mani_env [16] = {0};
	mani_in_env->Getbaseframe(base_mani_env);
	vector<Object> _objects;
	_env->get_objects_in_env(_objects);
	
	env = new Environment(config_mani_env,n_section_mani_env,base_mani_env,_objects);
	task_constraints = _task_constraints;
	heuristic_pos_bases = _heuristic_pos_bases;
	planned_path.clear();
	planned_path_base_frames.clear();
}

MotionPlanner::MotionPlanner(Environment *_env ,int _timestamp)
{
	timeStamp = _timestamp;
	assert(_env);
	ManipulatorNSections* mani_in_env = _env->get_mani_in_env();
	int n_section_mani_env = mani_in_env->get_n_sections();
	float* config_mani_env  = new float [n_section_mani_env*3];
	mani_in_env->get_config(config_mani_env);

	float base_mani_env [16] = {0};
	mani_in_env->Getbaseframe(base_mani_env);
	vector<Object> _objects;
	_env->get_objects_in_env(_objects);
	
	env = new Environment(config_mani_env,n_section_mani_env,base_mani_env,_objects);
	planned_path.clear();
	planned_path_base_frames.clear();
}

vector<vector<float> > MotionPlanner::get_planned_path()
{
	return planned_path;
}

vector<vector<float> > MotionPlanner::get_planned_path_base_frames()
{
	return planned_path_base_frames;
}

bool MotionPlanner::compute_section_frames_based_on_end_effector_fixedBase_Orientation(vector<Frames>& arm_frames, vector<float>& arm_config,vector<heuristic_pos >heuristic_base_frame)
{
	ManipulatorNSections* mani = env->get_mani_in_env();
	int  n_sections = mani->get_n_sections();
	float end_pos [3] =  {task_constraints.Pos[0],task_constraints.Pos[1],task_constraints.Pos[2]};
	float end_z_axis [3] =  {task_constraints.z_axis[0],task_constraints.z_axis[1],task_constraints.z_axis[2]};
	float end_y_axis [3] = {task_constraints.y_axis[0],task_constraints.y_axis[1],task_constraints.y_axis[2]};
	float end_x_axis[3] = {task_constraints.x_axis[0],task_constraints.x_axis[1],task_constraints.x_axis[2]};

	for(int i = n_sections-1; i >= 0; --i)
	{
		// only used for sec_i, where i > 1
		float heur_sec_base [3] = {heuristic_base_frame[i].Pos[0], heuristic_base_frame[i].Pos[1], heuristic_base_frame[i].Pos[2]};
		// only used for sec_i, where i == 1
		float heur_sec_z_axis [3] = {heuristic_base_frame[i].z_axis[0], heuristic_base_frame[i].z_axis[1], heuristic_base_frame[i].z_axis[2]};

		// complete the end tip constraints based on sec_i's (i != n) heuristic base (position) and its tip's z-axis
		if(i != n_sections-1 && i != 0)
		{
			for(int j = 0; j < 3; ++j)
			{
				end_z_axis[j] = arm_frames[i+1].z_axis[j]; //sec_i's tip constraints
				end_pos[j] = arm_frames[i+1].Pos[j]; //sec_i's tip position
			}

			/// compute the section plane P_i, use its normal as sec_i's tip y-axis
			float vec_heur_base_to_tip[3] = {end_pos[0] - heur_sec_base[0], end_pos[1] - heur_sec_base[1], end_pos[2] - heur_sec_base[2]};
			Normalize(vec_heur_base_to_tip,3);

			CrossProduct(vec_heur_base_to_tip,end_z_axis,end_y_axis);
			Normalize(end_y_axis,3);
			CrossProduct(end_y_axis,end_z_axis,end_x_axis);
			Normalize(end_x_axis,3);
		}

		if( i == 0)
		{

			float arm_base [16] = {0};
			mani->Getbaseframe(arm_base);
			for(int j = 0;j < 3; ++j)
			{
				heur_sec_z_axis[j] = arm_base[8+j]; //sec_i's base axis
				heur_sec_base[j] = arm_base[12+j];
			}	
		
			for(int j = 0; j < 3; ++j)
			{
				end_z_axis[j] = arm_frames[i+1].z_axis[j]; //sec_i's tip constraints
				end_pos[j] = arm_frames[i+1].Pos[j]; //sec_i's tip position
			}

			/// compute the section plane P_i, use its normal as sec_i's tip y-axis
			Normalize(heur_sec_z_axis,3);
			CrossProduct(heur_sec_z_axis,end_z_axis,end_y_axis);
			Normalize(end_y_axis,3);
			CrossProduct(end_y_axis,end_z_axis,end_x_axis);
			Normalize(end_x_axis,3);
		}
		
		//update the output sec_i's tip frame, i.e., x and y axes
		for(int j = 0; j < 3; ++j)
		{
			arm_frames[i+1].Pos[j] = end_pos[j];
			arm_frames[i+1].z_axis[j] = end_z_axis[j];
			arm_frames[i+1].y_axis[j] = end_y_axis[j]; 
			arm_frames[i+1].x_axis[j] = end_x_axis[j]; 
		}

		if(i < n_sections-1 && arm_config[(i+1)*3+2] != 0.0)
		{
			//////////////now the sec_i+1's base frame is determined, then compute the phi_i+1
			float arm_base_matrix[16] = {arm_frames[i+1].x_axis[0],arm_frames[i+1].x_axis[1],arm_frames[i+1].x_axis[2],0.0,
											arm_frames[i+1].y_axis[0],arm_frames[i+1].y_axis[1],arm_frames[i+1].y_axis[2],0.0,
											arm_frames[i+1].z_axis[0],arm_frames[i+1].z_axis[1],arm_frames[i+1].z_axis[2],0.0,
											arm_frames[i+1].Pos[0], arm_frames[i+1].Pos[1],  arm_frames[i+1].Pos[2],1.0};
			float EndPoint_sec_i_plus_1 [4]= {arm_frames[i+2].Pos[0],arm_frames[i+2].Pos[1], arm_frames[i+2].Pos[2], 1.0};
			float* LoacalPoint = MatrixMulti(InverMatrix(arm_base_matrix),EndPoint_sec_i_plus_1,4,4,4,1);
			float phi;
			if(arm_config[(i+1)*3+1] >= 0)
			{
				if(LoacalPoint[0] < 0)
				{
					phi= atan(LoacalPoint[1]/LoacalPoint[0]);
				}
				else if(LoacalPoint[0] > 0)
				{
					if(LoacalPoint[1] >= 0)
					{
						phi= -(3.1415926-atan(LoacalPoint[1]/LoacalPoint[0]));
					}
					else
					{
						phi= 3.1415926 + atan(LoacalPoint[1]/LoacalPoint[0]);
					}
				}
				else
				{
					int error = 1;
				}
			}
			else
			{
				if(LoacalPoint[0] > 0)
				{
					phi= atan(LoacalPoint[1]/LoacalPoint[0]);
				}
				else if(LoacalPoint[0] < 0)
				{
					if(LoacalPoint[1] <= 0)
					{
						phi= -(3.1415926-atan(LoacalPoint[1]/LoacalPoint[0]));
					}
					else
					{
						phi= 3.1415926 + atan(LoacalPoint[1]/LoacalPoint[0]);
					}
				}
				else
				{
					int error = 1;
				}
			}
			arm_config[(i+1)*3+2] = phi;
			//////////////
		}

		float section_plane_norm [3] = {end_y_axis[0],end_y_axis[1], end_y_axis[2]};
		float section_plane_a_point [3] = {end_pos[0], end_pos[1], end_pos[2]};

		float Projected_base_pt [3] = {0};
		float Projected_base_z_axis [3] = {0};
		// for sec_n to sec_2, the heuristic base is given to compute section plane, for sec_1 the base position combining with its z-axis is used to specify the plane that robot have moved on 
		Proj_Pt_to_Plane(heur_sec_base,section_plane_a_point,section_plane_norm,Projected_base_pt);
		//Projected_base_z_axis is only useful for sec_i, i == 0
	    Proj_vec_to_Plane (heur_sec_z_axis, heur_sec_base, section_plane_a_point, section_plane_norm, Projected_base_z_axis);

		//////////////////////////////////////////// further adjust the z_axis of the current section's base on the section plane///////////////
		if(i == 0)
		{
			///////////////............FOR SECTION ONE: On the section plane....///////////
			float x_rotated [3] = {0};
			CrossProduct(section_plane_norm,heur_sec_z_axis,x_rotated);
			float adjusted_base_pt [3] = {0};
			float* sec_center = TwolineIntersection(end_pos, Projected_base_pt,end_x_axis, x_rotated); //ALMEM
			float section_length = 0;
			float radius = 0;
			float k = 0;
			if(sec_center != NULL)
			{
				radius = two_pts_dist(end_pos,sec_center,3);
				// first compute one possible base position of sec_i
				for(int j = 0;j < 3; ++j)
				{
					adjusted_base_pt[j] =  sec_center[j] + x_rotated[j]*radius;
				}

				// Then check if they yield same rotation axis with the tip's z-axis
				// compute the rotation direction of the end z-axis
				float end_z_rot_direction [3] = {0};
				float vec_end_to_center [3] = {sec_center[0] - end_pos[0], sec_center[1] - end_pos[1], sec_center[2] - end_pos[2]};
				Normalize(vec_end_to_center,3);
				CrossProduct(end_z_axis,vec_end_to_center,end_z_rot_direction);
				Normalize(end_z_rot_direction,3);

				// compute the rotation direction of the base z-axis
				float base_z_rot_direction [3] = {0};	
				float vec_base_to_center [3] = {sec_center[0] - adjusted_base_pt[0], sec_center[1] - adjusted_base_pt[1], sec_center[2] - adjusted_base_pt[2]};
				Normalize(vec_base_to_center,3);
				CrossProduct(heur_sec_z_axis,vec_base_to_center,base_z_rot_direction);
				Normalize(base_z_rot_direction,3);

				// Compute the updated base position based on whether the above rotation direction is the same or not.
				float dotProcut = DotProduct(base_z_rot_direction,end_z_rot_direction);
				if(dotProcut < 0)
				{
					for(int j = 0;j < 3; ++j)
					{
						adjusted_base_pt[j] =  sec_center[j] - x_rotated[j]*radius;
					}	
				}
			}
			else
			{			
				Proj_Pt_to_Plane(end_pos,heur_sec_base,heur_sec_z_axis,adjusted_base_pt);
			}

			//update the base frame of sec_1 
			for(int j = 0;j < 3; ++j)
			{
				arm_frames[i].Pos[j] = adjusted_base_pt[j];
				arm_frames[i].z_axis[j] = heuristic_base_frame[i].z_axis[j];
				arm_frames[i].x_axis[j] = heuristic_base_frame[i].x_axis[j];
				arm_frames[i].y_axis[j] = heuristic_base_frame[i].y_axis[j];
			}

			//////////////////////Compute the section_1's configuration 
			// Compute curvature k;
			if(sec_center)
			{
				float sign_x = arm_frames[i+1].x_axis[0]*(sec_center[0] - end_pos[0]) + arm_frames[i+1].x_axis[1]*(sec_center[1] - end_pos[1])+ arm_frames[i+1].x_axis[2]*(sec_center[2] - end_pos[2]); 
				 k = (sign_x>0) ? -(1.0/radius) :(1.0/radius); 

				// compute the section length s
				float vec_tip_to_center[3] = {sec_center[0] - end_pos[0], sec_center[1] - end_pos[1], sec_center[2] - end_pos[2]};
				float vec_base_to_center[3] = {sec_center[0] - adjusted_base_pt[0], sec_center[1] - adjusted_base_pt[1], sec_center[2] - adjusted_base_pt[2]};

				// check if the tip point is in the opposite direction of the adjusted z-axis.
				float sign = heur_sec_z_axis[0]*(end_pos[0] - adjusted_base_pt[0]) + heur_sec_z_axis[1]*(end_pos[1] - adjusted_base_pt[1])+ heur_sec_z_axis[2]*(end_pos[2] - adjusted_base_pt[2]); 
		
				float theta_angle = (sign > 0) ? Angle_bewteen_two_vecs(vec_tip_to_center,vec_base_to_center):(2*Pi_jinglin- Angle_bewteen_two_vecs(vec_tip_to_center,vec_base_to_center));
				section_length = theta_angle*radius;

				// compute the section length phi
				float arm_base_matrix[16] = {arm_frames[i].x_axis[0],arm_frames[i].x_axis[1],arm_frames[i].x_axis[2],0.0,
												arm_frames[i].y_axis[0],arm_frames[i].y_axis[1],arm_frames[i].y_axis[2],0.0,
												arm_frames[i].z_axis[0],arm_frames[i].z_axis[1],arm_frames[i].z_axis[2],0.0,
												arm_frames[i].Pos[0], arm_frames[i].Pos[1],  arm_frames[i].Pos[2],1.0};
				float EndPoint_sec_i [4]= {end_pos[0],end_pos[1], end_pos[2], 1.0};
				float* LoacalPoint = MatrixMulti(InverMatrix(arm_base_matrix),EndPoint_sec_i,4,4,4,1);
				float phi;
				if(k >= 0)
				{
					if(LoacalPoint[0] < 0)
					{
						phi= atan(LoacalPoint[1]/LoacalPoint[0]);
					}
					else if(LoacalPoint[0] > 0)
					{
						if(LoacalPoint[1] >= 0)
						{
							phi= -(3.1415926-atan(LoacalPoint[1]/LoacalPoint[0]));
						}
						else
						{
							phi= 3.1415926 + atan(LoacalPoint[1]/LoacalPoint[0]);
						}
					}
					else
					{
						int error = 1;
					}
				}
				else
				{
					if(LoacalPoint[0] > 0)
					{
						phi= atan(LoacalPoint[1]/LoacalPoint[0]);
					}
					else if(LoacalPoint[0] < 0)
					{
						if(LoacalPoint[1] <= 0)
						{
							phi= -(3.1415926-atan(LoacalPoint[1]/LoacalPoint[0]));
						}
						else
						{
							phi= 3.1415926 + atan(LoacalPoint[1]/LoacalPoint[0]);
						}
					}
					else
					{
						int error = 1;
					}
				}
			//	 phi = atan(LoacalPoint[1]/LoacalPoint[0]);
	
				arm_config[3*i] = section_length;
				arm_config[3*i+1] =  k;
				arm_config[3*i+2] = phi;
				delete [] sec_center;
			}
			else
			{
				arm_config[3*i] = two_pts_dist(end_pos,adjusted_base_pt,3);
				arm_config[3*i+1] = 1.0/999.0;
				arm_config[3*i+2] = 0.0;
			}
			////////////////////////////
			continue;
		}


		///////////////////////////Begin for section from n-1 to 1//////////////
		float vec_base_to_tip[3] = {end_pos[0] - Projected_base_pt[0], end_pos[1] - Projected_base_pt[1], end_pos[2] - Projected_base_pt[2]};
		float mid_pt_base_and_tip [3] = {(end_pos[0] + Projected_base_pt[0])/2, (end_pos[1] + Projected_base_pt[1])/2, (end_pos[2] + Projected_base_pt[2])/2};
		Normalize(vec_base_to_tip,3);
		float vec_per_bisector[3] = {0};
		CrossProduct(end_y_axis,vec_base_to_tip,vec_per_bisector);
		
		// Compute the section center and then the radius of the section circle
		float* sec_center = TwolineIntersection(end_pos, mid_pt_base_and_tip,end_x_axis, vec_per_bisector); // ALMEM
		float adjusted_z_axis [3] = {0};
		float radius = 0;
		if(sec_center != NULL)// 
		{
			 radius = two_pts_dist(sec_center,Projected_base_pt,3);

			// Compute the section base's new z-axis////////////
			////////first compute the rotation direction of the end's z-axis
			float end_z_rot_direction [3] = {0};
			float vec_end_to_center [3] = {sec_center[0] - end_pos[0], sec_center[1] - end_pos[1], sec_center[2] - end_pos[2]};
			Normalize(vec_end_to_center,3);
			CrossProduct(end_z_axis,vec_end_to_center,end_z_rot_direction);
			Normalize(end_z_rot_direction,3);
		
			float vec_base_to_center [3] = {sec_center[0] - Projected_base_pt[0], sec_center[1] - Projected_base_pt[1], sec_center[2] - Projected_base_pt[2]};
			Normalize(vec_base_to_center,3);
	
			CrossProduct(vec_base_to_center,end_y_axis,adjusted_z_axis);
			Normalize(adjusted_z_axis,3);

			// then compute the base z-axis's rotation direction
			float base_z_rot_direction [3] = {0};		
			CrossProduct(adjusted_z_axis,vec_base_to_center,base_z_rot_direction);
			Normalize(base_z_rot_direction,3);

			// the above two rotation direction should be the same, if not, change reverse the direction of the base z-axis
			float dotProcut = DotProduct(base_z_rot_direction,end_z_rot_direction);
			if(dotProcut < 0)
			{
				for(int j = 0; j < 3; ++j)
				{
					adjusted_z_axis[j] = -adjusted_z_axis[j];
				}
			}
		}
		else
		{
			for(int j = 0; j < 3; ++j)
			{
				adjusted_z_axis[j] = vec_base_to_tip[j];
			}
		}

		/// update the output arm frames
		for(int j = 0; j < 3; ++j)
		{
			arm_frames[i].z_axis[j] = adjusted_z_axis[j]; // section i's base z-axis
			arm_frames[i+1].z_axis[j] = end_z_axis[j]; // section i's tip z-axis
			arm_frames[i].Pos[j] = Projected_base_pt[j];
		}

		if(sec_center)
		{
			//Compute curvature k
			float sign_x = arm_frames[i+1].x_axis[0]*(sec_center[0] - end_pos[0]) + arm_frames[i+1].x_axis[1]*(sec_center[1] - end_pos[1])+ arm_frames[i+1].x_axis[2]*(sec_center[2] - end_pos[2]); 
			float k =  ((sign_x>0) ? -(1.0/radius) :(1.0/radius));

			// compute the section length s

			float vec_tip_to_center[3] = {sec_center[0] - end_pos[0], sec_center[1] - end_pos[1], sec_center[2] - end_pos[2]};
			float vec_base_to_center[3] = {sec_center[0] - arm_frames[i].Pos[0], sec_center[1] - arm_frames[i].Pos[1], sec_center[2] - arm_frames[i].Pos[2]};

			// check if the tip point is in the opposite direction of the adjusted z-axis.
			float sign = arm_frames[i].z_axis[0]*(end_pos[0] - arm_frames[i].Pos[0]) + arm_frames[i].z_axis[1]*(end_pos[1] - arm_frames[i].Pos[1])+ arm_frames[i].z_axis[2]*(end_pos[2] - arm_frames[i].Pos[2]); 
		
			float theta_angle = (sign > 0) ? Angle_bewteen_two_vecs(vec_tip_to_center,vec_base_to_center):(2*Pi_jinglin- Angle_bewteen_two_vecs(vec_tip_to_center,vec_base_to_center));

			arm_config[3*i] = theta_angle*radius;;
			arm_config[3*i+1] =  ((sign_x>0) ? -(1.0/radius) :(1.0/radius));
			arm_config[3*i+2] = -999;
		    delete [] sec_center;	
		}
		else
		{
			float sec_base_pos [3] = {arm_frames[i].Pos[0],arm_frames[i].Pos[1],arm_frames[i].Pos[2]};
			arm_config[3*i]  = two_pts_dist(end_pos,sec_base_pos,3);
			arm_config[3*i+1] = 1.0/999.0;
			arm_config[3*i+2] = 0.0;
		}
	}
	return true;
}

bool MotionPlanner::compute_section_frames_based_on_end_effector_fixedBase_position(vector<Frames>& arm_frames, vector<float>& arm_config,vector<heuristic_pos >heuristic_base_frame)
{
	ManipulatorNSections* mani = env->get_mani_in_env();
	int  n_sections = mani->get_n_sections();
	float end_pos [3] =  {task_constraints.Pos[0],task_constraints.Pos[1],task_constraints.Pos[2]};
	float end_z_axis [3] =  {task_constraints.z_axis[0],task_constraints.z_axis[1],task_constraints.z_axis[2]};
	float end_y_axis [3] = {task_constraints.y_axis[0],task_constraints.y_axis[1],task_constraints.y_axis[2]};
	float end_x_axis[3] = {task_constraints.x_axis[0],task_constraints.x_axis[1],task_constraints.x_axis[2]};
    cout<<"task_constraint: "<<end_pos[0]<<" "<<end_pos[1]<<" "<<end_pos[2]<<endl;
	for(int i = n_sections-1; i >= -1; --i)
	{
		float heur_sec_base [3] = {0};
		if(i >=0)
		{
			for(int j = 0; j < 3; j++)
			{
				heur_sec_base[j] = heuristic_base_frame[i].Pos[j];
			}
			cout<<i<<": "<<heur_sec_base[0]<<" "<<heur_sec_base[1]<<" "<<heur_sec_base[2]<<endl;
        
			// complete the end tip constraints based on sec_i's (i != n) heuristic base (position) and its tip's z-axis
			if(i != n_sections-1)
			{
				for(int j = 0; j < 3; ++j)
				{
					end_z_axis[j] = arm_frames[i+1].z_axis[j]; //sec_i's tip constraints
					end_pos[j] = arm_frames[i+1].Pos[j]; //sec_i's tip position
				}

				// compute the section plane P_i, use its normal as sec_i's tip y-axis
				float vec_heur_base_to_tip[3] = {end_pos[0] - heur_sec_base[0], end_pos[1] - heur_sec_base[1], end_pos[2] - heur_sec_base[2]};
				Normalize(vec_heur_base_to_tip,3);

				CrossProduct(vec_heur_base_to_tip,end_z_axis,end_y_axis);
				Normalize(end_y_axis,3);
				CrossProduct(end_y_axis,end_z_axis,end_x_axis);
				Normalize(end_x_axis,3);
			}
			for(int j = 0; j < 3; ++j)
			{
				arm_frames[i+1].Pos[j] = end_pos[j];
				arm_frames[i+1].z_axis[j] = end_z_axis[j];
				arm_frames[i+1].y_axis[j] = end_y_axis[j]; 
				arm_frames[i+1].x_axis[j] = end_x_axis[j]; 
			}
		}
		else
		{
			float arm_base_current [16] = {0};
			mani->Getbaseframe(arm_base_current);
			float desired_z_axis [3] = {0.0};
			float adjusted_x_axis [3] = {0.0};
			float adjusted_y_axis [3] = {0.0};

			float current_x_axis [3] = {0.0};
			float current_y_axis [3] = {0.0};
			 
			for(int j = 0; j < 3; ++j)
			{
				desired_z_axis[j] = arm_frames[i+1].z_axis[j]; 
				current_y_axis[j] = arm_base_current[4+j];
				current_x_axis[j] = arm_base_current[j];
			}

			CrossProduct(desired_z_axis,current_x_axis,adjusted_y_axis);
			CrossProduct(adjusted_y_axis,desired_z_axis,adjusted_x_axis);
			Normalize(adjusted_x_axis,3);
			Normalize(adjusted_y_axis,3);
			for(int j = 0; j < 3; ++j)
			{
				arm_frames[i+1].x_axis[j] = adjusted_x_axis[j]; 
				arm_frames[i+1].y_axis[j] = adjusted_y_axis[j]; 
			}

		}
		if(i < n_sections-1 && arm_config[(i+1)*3+2] != 0.0)
		{
			//////////////now the sec_i+1's base frame is determined, then compute the phi_i+1
			float arm_base_matrix[16] = {arm_frames[i+1].x_axis[0],arm_frames[i+1].x_axis[1],arm_frames[i+1].x_axis[2],0.0,
											arm_frames[i+1].y_axis[0],arm_frames[i+1].y_axis[1],arm_frames[i+1].y_axis[2],0.0,
											arm_frames[i+1].z_axis[0],arm_frames[i+1].z_axis[1],arm_frames[i+1].z_axis[2],0.0,
											arm_frames[i+1].Pos[0], arm_frames[i+1].Pos[1],  arm_frames[i+1].Pos[2],1.0};
			float EndPoint_sec_i_plus_1 [4]= {arm_frames[i+2].Pos[0],arm_frames[i+2].Pos[1], arm_frames[i+2].Pos[2], 1.0};
			float* LoacalPoint = MatrixMulti(InverMatrix(arm_base_matrix),EndPoint_sec_i_plus_1,4,4,4,1);
			float phi;
			if(arm_config[(i+1)*3+1] >= 0)
			{
				if(LoacalPoint[0] < 0)
				{
					phi= atan(LoacalPoint[1]/LoacalPoint[0]);
				}
				else if(LoacalPoint[0] > 0)
				{
					if(LoacalPoint[1] >= 0)
					{
						phi= -(3.1415926-atan(LoacalPoint[1]/LoacalPoint[0]));
					}
					else
					{
						phi= 3.1415926 + atan(LoacalPoint[1]/LoacalPoint[0]);
					}
				}
				else
				{
			
					cout<<"inverse Kinematics failed !!!!"<<endl;
					getchar();
				}
			}
			else
			{
				if(LoacalPoint[0] > 0)
				{
					phi= atan(LoacalPoint[1]/LoacalPoint[0]);
				}
				else if(LoacalPoint[0] < 0)
				{
					if(LoacalPoint[1] <= 0)
					{
						phi= -(3.1415926-atan(LoacalPoint[1]/LoacalPoint[0]));
					}
					else
					{
						phi= 3.1415926 + atan(LoacalPoint[1]/LoacalPoint[0]);
					}
				}
				else
				{
					cout<<"inverse Kinematics failed !!!!"<<endl;
					getchar();
					
				}
			}
			arm_config[(i+1)*3+2] = phi;
			//////////////
			if(i < 0)
			{
				return true;
			}
		}

		float section_plane_norm [3] = {end_y_axis[0],end_y_axis[1], end_y_axis[2]};
		float section_plane_a_point [3] = {end_pos[0], end_pos[1], end_pos[2]};

		float Projected_base_pt [3] = {0};
		Proj_Pt_to_Plane(heur_sec_base,section_plane_a_point,section_plane_norm,Projected_base_pt);
	
		///////////////////////////Begin for section from n-1 to 1//////////////

		float vec_base_to_tip[3] = {end_pos[0] - Projected_base_pt[0], end_pos[1] - Projected_base_pt[1], end_pos[2] - Projected_base_pt[2]};
		float mid_pt_base_and_tip [3] = {(end_pos[0] + Projected_base_pt[0])/2, (end_pos[1] + Projected_base_pt[1])/2, (end_pos[2] + Projected_base_pt[2])/2};
		Normalize(vec_base_to_tip,3);
		float vec_per_bisector[3] = {0};
		CrossProduct(end_y_axis,vec_base_to_tip,vec_per_bisector);
		
		// Compute the section center and then the radius of the section circle
		float* sec_center = TwolineIntersection(end_pos, mid_pt_base_and_tip,end_x_axis, vec_per_bisector); // ALMEM
		float adjusted_z_axis [3] = {0};
		float radius = 0;
		if(sec_center != NULL)// 
		{
			 radius = two_pts_dist(sec_center,Projected_base_pt,3);

			// Compute the section base's new z-axis////////////
			////////first compute the rotation direction of the end's z-axis
			float end_z_rot_direction [3] = {0};
			float vec_end_to_center [3] = {sec_center[0] - end_pos[0], sec_center[1] - end_pos[1], sec_center[2] - end_pos[2]};
			Normalize(vec_end_to_center,3);
			CrossProduct(end_z_axis,vec_end_to_center,end_z_rot_direction);
			Normalize(end_z_rot_direction,3);
		
			float vec_base_to_center [3] = {sec_center[0] - Projected_base_pt[0], sec_center[1] - Projected_base_pt[1], sec_center[2] - Projected_base_pt[2]};
			Normalize(vec_base_to_center,3);
	
			CrossProduct(vec_base_to_center,end_y_axis,adjusted_z_axis);
			Normalize(adjusted_z_axis,3);

			// then compute the base z-axis's rotation direction
			float base_z_rot_direction [3] = {0};		
			CrossProduct(adjusted_z_axis,vec_base_to_center,base_z_rot_direction);
			Normalize(base_z_rot_direction,3);
           
			// the above two rotation direction should be the same, if not, change reverse the direction of the base z-axis
			float dotProcut = DotProduct(base_z_rot_direction,end_z_rot_direction);
			if(dotProcut < 0)
			{
				for(int j = 0; j < 3; ++j)
				{
					adjusted_z_axis[j] = -adjusted_z_axis[j];
				}
			}
		}
		else
		{
			for(int j = 0; j < 3; ++j)
			{
				adjusted_z_axis[j] = vec_base_to_tip[j];
			}
		}

		// update the output arm frames
		for(int j = 0; j < 3; ++j)
		{
			arm_frames[i].z_axis[j] = adjusted_z_axis[j]; // section i's base z-axis
			arm_frames[i+1].z_axis[j] = end_z_axis[j]; // section i's tip z-axis
			arm_frames[i].Pos[j] = Projected_base_pt[j];
		}

		if(sec_center)
		{
			//Compute curvature k
			float sign_x = arm_frames[i+1].x_axis[0]*(sec_center[0] - end_pos[0]) + arm_frames[i+1].x_axis[1]*(sec_center[1] - end_pos[1])+ arm_frames[i+1].x_axis[2]*(sec_center[2] - end_pos[2]); 
			float k =  ((sign_x>0) ? -(1.0/radius) :(1.0/radius));

			// compute the section length s

			float vec_tip_to_center[3] = {sec_center[0] - end_pos[0], sec_center[1] - end_pos[1], sec_center[2] - end_pos[2]};
			float vec_base_to_center[3] = {sec_center[0] - arm_frames[i].Pos[0], sec_center[1] - arm_frames[i].Pos[1], sec_center[2] - arm_frames[i].Pos[2]};

			// check if the tip point is in the opposite direction of the adjusted z-axis.
			float sign = arm_frames[i].z_axis[0]*(end_pos[0] - arm_frames[i].Pos[0]) + arm_frames[i].z_axis[1]*(end_pos[1] - arm_frames[i].Pos[1])+ arm_frames[i].z_axis[2]*(end_pos[2] - arm_frames[i].Pos[2]); 
			
			float theta_angle = (sign > 0) ? Angle_bewteen_two_vecs(vec_tip_to_center,vec_base_to_center):(2*Pi_jinglin- Angle_bewteen_two_vecs(vec_tip_to_center,vec_base_to_center));

			arm_config[3*i] = theta_angle*radius;;
			arm_config[3*i+1] =  ((sign_x>0) ? -(1.0/radius) :(1.0/radius));
			arm_config[3*i+2] = -999;
		    delete [] sec_center;	
		}
		else
		{
			float sec_base_pos [3] = {arm_frames[i].Pos[0],arm_frames[i].Pos[1],arm_frames[i].Pos[2]};
			arm_config[3*i]  = two_pts_dist(end_pos,sec_base_pos,3);
			arm_config[3*i+1] = 1.0/999.0;
			arm_config[3*i+2] = 0.0;
		}
	}
	return true;
}


bool MotionPlanner::compute_section_frames_based_on_end_effector_fixedBase_position(int n_sections, vector<Frames>& arm_frames, vector<float>& arm_config,vector<heuristic_pos >heuristic_base_frame)
{
	ManipulatorNSections* mani = env->get_mani_in_env();
	
	float end_pos [3] =  {task_constraints.Pos[0],task_constraints.Pos[1],task_constraints.Pos[2]};
	float end_z_axis [3] =  {task_constraints.z_axis[0],task_constraints.z_axis[1],task_constraints.z_axis[2]};
	float end_y_axis [3] = {task_constraints.y_axis[0],task_constraints.y_axis[1],task_constraints.y_axis[2]};
	float end_x_axis[3] = {task_constraints.x_axis[0],task_constraints.x_axis[1],task_constraints.x_axis[2]};

	for(int i = n_sections-1; i >= -1; --i)
	{
		float heur_sec_base [3] = {0};
		if(i >=0)
		{
			for(int j = 0; j < 3; j++)
			{
				heur_sec_base[j] = heuristic_base_frame[i].Pos[j];
			}
             
            
			// complete the end tip constraints based on sec_i's (i != n) heuristic base (position) and its tip's z-axis
			if(i != n_sections-1)
			{
				for(int j = 0; j < 3; ++j)
				{
					end_z_axis[j] = arm_frames[i+1].z_axis[j]; //sec_i's tip constraints
					end_pos[j] = arm_frames[i+1].Pos[j]; //sec_i's tip position
				}

				// compute the section plane P_i, use its normal as sec_i's tip y-axis
				float vec_heur_base_to_tip[3] = {end_pos[0] - heur_sec_base[0], end_pos[1] - heur_sec_base[1], end_pos[2] - heur_sec_base[2]};
				Normalize(vec_heur_base_to_tip,3);

				CrossProduct(vec_heur_base_to_tip,end_z_axis,end_y_axis);
				Normalize(end_y_axis,3);
				CrossProduct(end_y_axis,end_z_axis,end_x_axis);
				Normalize(end_x_axis,3);
			}
			for(int j = 0; j < 3; ++j)
			{
				arm_frames[i+1].Pos[j] = end_pos[j];
				arm_frames[i+1].z_axis[j] = end_z_axis[j];
				arm_frames[i+1].y_axis[j] = end_y_axis[j]; 
				arm_frames[i+1].x_axis[j] = end_x_axis[j]; 
			}
		}
		else
		{
			float arm_base_current [16] = {0};
			mani->Getbaseframe(arm_base_current);
			float desired_z_axis [3] = {0.0};
			float adjusted_x_axis [3] = {0.0};
			float adjusted_y_axis [3] = {0.0};

			float current_x_axis [3] = {0.0};
			float current_y_axis [3] = {0.0};
			 
			for(int j = 0; j < 3; ++j)
			{
				desired_z_axis[j] = arm_frames[i+1].z_axis[j]; 
				current_y_axis[j] = arm_base_current[4+j];
				current_x_axis[j] = arm_base_current[j];
			}

			CrossProduct(desired_z_axis,current_x_axis,adjusted_y_axis);
			CrossProduct(adjusted_y_axis,desired_z_axis,adjusted_x_axis);
			Normalize(adjusted_x_axis,3);
			Normalize(adjusted_y_axis,3);
			for(int j = 0; j < 3; ++j)
			{
				arm_frames[i+1].x_axis[j] = adjusted_x_axis[j]; 
				arm_frames[i+1].y_axis[j] = adjusted_y_axis[j]; 
			}

		}
		if(i < n_sections-1 && arm_config[(i+1)*3+2] != 0.0)
		{
			//////////////now the sec_i+1's base frame is determined, then compute the phi_i+1
			float arm_base_matrix[16] = {arm_frames[i+1].x_axis[0],arm_frames[i+1].x_axis[1],arm_frames[i+1].x_axis[2],0.0,
											arm_frames[i+1].y_axis[0],arm_frames[i+1].y_axis[1],arm_frames[i+1].y_axis[2],0.0,
											arm_frames[i+1].z_axis[0],arm_frames[i+1].z_axis[1],arm_frames[i+1].z_axis[2],0.0,
											arm_frames[i+1].Pos[0], arm_frames[i+1].Pos[1],  arm_frames[i+1].Pos[2],1.0};
			float EndPoint_sec_i_plus_1 [4]= {arm_frames[i+2].Pos[0],arm_frames[i+2].Pos[1], arm_frames[i+2].Pos[2], 1.0};
			float* LoacalPoint = MatrixMulti(InverMatrix(arm_base_matrix),EndPoint_sec_i_plus_1,4,4,4,1);
			float phi;
			if(arm_config[(i+1)*3+1] >= 0)
			{
				if(LoacalPoint[0] < 0)
				{
					phi= atan(LoacalPoint[1]/LoacalPoint[0]);
				}
				else if(LoacalPoint[0] > 0)
				{
					if(LoacalPoint[1] >= 0)
					{
						phi= -(3.1415926-atan(LoacalPoint[1]/LoacalPoint[0]));
					}
					else
					{
						phi= 3.1415926 + atan(LoacalPoint[1]/LoacalPoint[0]);
					}
				}
				else
				{
					int error = 1;
				}
			}
			else
			{
				if(LoacalPoint[0] > 0)
				{
					phi= atan(LoacalPoint[1]/LoacalPoint[0]);
				}
				else if(LoacalPoint[0] < 0)
				{
					if(LoacalPoint[1] <= 0)
					{
						phi= -(3.1415926-atan(LoacalPoint[1]/LoacalPoint[0]));
					}
					else
					{
						phi= 3.1415926 + atan(LoacalPoint[1]/LoacalPoint[0]);
					}
				}
				else
				{
					int error = 1;
				}
			}
			arm_config[(i+1)*3+2] = phi;
			//////////////
			if(i < 0)
			{
				return true;
			}
		}

		float section_plane_norm [3] = {end_y_axis[0],end_y_axis[1], end_y_axis[2]};
		float section_plane_a_point [3] = {end_pos[0], end_pos[1], end_pos[2]};

		float Projected_base_pt [3] = {0};
		Proj_Pt_to_Plane(heur_sec_base,section_plane_a_point,section_plane_norm,Projected_base_pt);
	
		///////////////////////////Begin for section from n-1 to 1//////////////

		float vec_base_to_tip[3] = {end_pos[0] - Projected_base_pt[0], end_pos[1] - Projected_base_pt[1], end_pos[2] - Projected_base_pt[2]};
		float mid_pt_base_and_tip [3] = {(end_pos[0] + Projected_base_pt[0])/2, (end_pos[1] + Projected_base_pt[1])/2, (end_pos[2] + Projected_base_pt[2])/2};
		Normalize(vec_base_to_tip,3);
		float vec_per_bisector[3] = {0};
		CrossProduct(end_y_axis,vec_base_to_tip,vec_per_bisector);
		
		// Compute the section center and then the radius of the section circle
		float* sec_center = TwolineIntersection(end_pos, mid_pt_base_and_tip,end_x_axis, vec_per_bisector); // ALMEM
		float adjusted_z_axis [3] = {0};
		float radius = 0;
		if(sec_center != NULL)// 
		{
			 radius = two_pts_dist(sec_center,Projected_base_pt,3);

			// Compute the section base's new z-axis////////////
			////////first compute the rotation direction of the end's z-axis
			float end_z_rot_direction [3] = {0};
			float vec_end_to_center [3] = {sec_center[0] - end_pos[0], sec_center[1] - end_pos[1], sec_center[2] - end_pos[2]};
			Normalize(vec_end_to_center,3);
			CrossProduct(end_z_axis,vec_end_to_center,end_z_rot_direction);
			Normalize(end_z_rot_direction,3);
		
			float vec_base_to_center [3] = {sec_center[0] - Projected_base_pt[0], sec_center[1] - Projected_base_pt[1], sec_center[2] - Projected_base_pt[2]};
			Normalize(vec_base_to_center,3);
	
			CrossProduct(vec_base_to_center,end_y_axis,adjusted_z_axis);
			Normalize(adjusted_z_axis,3);

			// then compute the base z-axis's rotation direction
			float base_z_rot_direction [3] = {0};		
			CrossProduct(adjusted_z_axis,vec_base_to_center,base_z_rot_direction);
			Normalize(base_z_rot_direction,3);

			// the above two rotation direction should be the same, if not, change reverse the direction of the base z-axis
			float dotProcut = DotProduct(base_z_rot_direction,end_z_rot_direction);
			if(dotProcut < 0)
			{
				for(int j = 0; j < 3; ++j)
				{
					adjusted_z_axis[j] = -adjusted_z_axis[j];
				}
			}
		}
		else
		{
			for(int j = 0; j < 3; ++j)
			{
				adjusted_z_axis[j] = vec_base_to_tip[j];
			}
		}

		// update the output arm frames
		for(int j = 0; j < 3; ++j)
		{
			arm_frames[i].z_axis[j] = adjusted_z_axis[j]; // section i's base z-axis
			arm_frames[i+1].z_axis[j] = end_z_axis[j]; // section i's tip z-axis
			arm_frames[i].Pos[j] = Projected_base_pt[j];
		}

		if(sec_center)
		{
			//Compute curvature k
			float sign_x = arm_frames[i+1].x_axis[0]*(sec_center[0] - end_pos[0]) + arm_frames[i+1].x_axis[1]*(sec_center[1] - end_pos[1])+ arm_frames[i+1].x_axis[2]*(sec_center[2] - end_pos[2]); 
			float k =  ((sign_x>0) ? -(1.0/radius) :(1.0/radius));

			// compute the section length s

			float vec_tip_to_center[3] = {sec_center[0] - end_pos[0], sec_center[1] - end_pos[1], sec_center[2] - end_pos[2]};
			float vec_base_to_center[3] = {sec_center[0] - arm_frames[i].Pos[0], sec_center[1] - arm_frames[i].Pos[1], sec_center[2] - arm_frames[i].Pos[2]};

			// check if the tip point is in the opposite direction of the adjusted z-axis.
			float sign = arm_frames[i].z_axis[0]*(end_pos[0] - arm_frames[i].Pos[0]) + arm_frames[i].z_axis[1]*(end_pos[1] - arm_frames[i].Pos[1])+ arm_frames[i].z_axis[2]*(end_pos[2] - arm_frames[i].Pos[2]); 
		
			float theta_angle = (sign > 0) ? Angle_bewteen_two_vecs(vec_tip_to_center,vec_base_to_center):(2*Pi_jinglin- Angle_bewteen_two_vecs(vec_tip_to_center,vec_base_to_center));

			arm_config[3*i] = theta_angle*radius;;
			arm_config[3*i+1] =  ((sign_x>0) ? -(1.0/radius) :(1.0/radius));
			arm_config[3*i+2] = -999;
		    delete [] sec_center;	
		}
		else
		{
			float sec_base_pos [3] = {arm_frames[i].Pos[0],arm_frames[i].Pos[1],arm_frames[i].Pos[2]};
			arm_config[3*i]  = two_pts_dist(end_pos,sec_base_pos,3);
			arm_config[3*i+1] = 1.0/999.0;
			arm_config[3*i+2] = 0.0;
		}
	}
	return true;
}

/*
bool MotionPlanner::Rescursive_task_cons(int timeStamp, int sec_idx, vector<heuristic_pos>& out_base_frame, TaskConstraints tipconstraints, vector<vector<feasible_base> >& heuristic_base_poses_all_secs)
{
	ManipulatorNSections* mani = env->get_mani_in_env();
	int  n_sections = mani->get_n_sections();

	float end_pos [3] =  {0.0};
	float end_z_axis [3] =  {0.0};
	float end_y_axis [3] = {0.0};
	float end_x_axis[3] = {0.0};
	float heur_sec_base[3] = {0.0};
	if(sec_idx == n_sections-1)
	{
		for(int j = 0; j < 3; ++j)
		{
			end_pos[j] = tipconstraints.Pos[j];
			end_z_axis[j] = task_constraints.z_axis[j];
			end_y_axis[j] = task_constraints.y_axis[j];
			end_x_axis[j] = task_constraints.x_axis[j];
		}
		
		for(int j = 0; j < 3; j++)
		{
			heur_sec_base[j] = heuristic_base_frame[i].Pos[j];
		}

		for(int j = 0; j < 3; ++j)
		{
			arm_frames[i+1].Pos[j] = end_pos[j];
			arm_frames[i+1].z_axis[j] = end_z_axis[j];
			arm_frames[i+1].y_axis[j] = end_y_axis[j]; 
			arm_frames[i+1].x_axis[j] = end_x_axis[j]; 
		}
		
			for(int j = 0; j < 3; ++j)
			{
				heur_sec_base[j] = heuristic_base_poses_all_secs[timeStamp-1][sec_idx].base.Pos[j];
			}	
			
			float section_plane_norm [3] = {end_y_axis[0],end_y_axis[1], end_y_axis[2]};
			float section_plane_a_point [3] = {end_pos[0], end_pos[1], end_pos[2]};

			float Projected_base_pt [3] = {0};
			Proj_Pt_to_Plane(heur_sec_base,section_plane_a_point,section_plane_norm,Projected_base_pt);
		
			///////////////////////////Begin for section from n-1 to 1//////////////
			float vec_base_to_tip[3] = {end_pos[0] - Projected_base_pt[0], end_pos[1] - Projected_base_pt[1], end_pos[2] - Projected_base_pt[2]};
			float mid_pt_base_and_tip [3] = {(end_pos[0] + Projected_base_pt[0])/2, (end_pos[1] + Projected_base_pt[1])/2, (end_pos[2] + Projected_base_pt[2])/2};
			Normalize(vec_base_to_tip,3);
			float vec_per_bisector[3] = {0};
			CrossProduct(end_y_axis,vec_base_to_tip,vec_per_bisector);
			
			// Compute the section center and then the radius of the section circle
			float* sec_center = TwolineIntersection(end_pos, mid_pt_base_and_tip,end_x_axis, vec_per_bisector); // ALMEM
			float adjusted_z_axis [3] = {0};
			float radius = 0;
			if(sec_center != NULL)// 
			{
				radius = two_pts_dist(sec_center,Projected_base_pt,3);
				// Compute the section base's new z-axis////////////
				////////first compute the rotation direction of the end's z-axis
				float end_z_rot_direction [3] = {0};
				float vec_end_to_center [3] = {sec_center[0] - end_pos[0], sec_center[1] - end_pos[1], sec_center[2] - end_pos[2]};
				Normalize(vec_end_to_center,3);
				CrossProduct(end_z_axis,vec_end_to_center,end_z_rot_direction);
				Normalize(end_z_rot_direction,3);
			
				float vec_base_to_center [3] = {sec_center[0] - Projected_base_pt[0], sec_center[1] - Projected_base_pt[1], sec_center[2] - Projected_base_pt[2]};
				Normalize(vec_base_to_center,3);
		
				CrossProduct(vec_base_to_center,end_y_axis,adjusted_z_axis);
				Normalize(adjusted_z_axis,3);

				// then compute the base z-axis's rotation direction
				float base_z_rot_direction [3] = {0};		
				CrossProduct(adjusted_z_axis,vec_base_to_center,base_z_rot_direction);
				Normalize(base_z_rot_direction,3);

				// the above two rotation direction should be the same, if not, change reverse the direction of the base z-axis
				float dotProcut = DotProduct(base_z_rot_direction,end_z_rot_direction);
				if(dotProcut < 0)
				{
					for(int j = 0; j < 3; ++j)
					{
						adjusted_z_axis[j] = -adjusted_z_axis[j];
					}
				}
				
				// z-axis, basept, tippt, center,						
				float sign = adjusted_z_axis[0]*(end_pos[0] - Projected_base_pt[0]) + adjusted_z_axis[1]*(end_pos[1] - Projected_base_pt[1])+ adjusted_z_axis[2]*(end_pos[2] - Projected_base_pt[2]); 
			
				float theta_angle = (sign > 0) ? Angle_bewteen_two_vecs(vec_tip_to_center,vec_base_to_center):(2*Pi_jinglin- Angle_bewteen_two_vecs(vec_tip_to_center,vec_base_to_center));
				
				float n_axis[3] = {0.0};
				CrossProduct(vec_base_to_center,adjusted_z_axis,n_axis);
				Normalize(n_axis,3);
				Normalize(adjusted_z_axis,3);			
				float sec_length = theta_angle*radius;	
				PartialConstCurve3D curve(end_pos,Projected_base_pt, sec_center, vec_base_to_center, adjusted_z_axis, n_axis,radius, sec_length);
				
				if(!env->CollisionCheck_section_to_meshes(curve))
				{
					for(int j = sec_idx; j >=0; j--)
					{
						heuristic_base_poses_all_secs[timeStamp][j] = heuristic_base_poses_all_secs[timeStamp-1][j];
					}		
				}
				else
				{
					//Random sample a new base of sec_idx, then recursively call this function to calculate the following bases in a lazy fashion.
					
				}	
			}
			else
			{
				for(int j = 0; j < 3; ++j)
				{
					adjusted_z_axis[j] = vec_base_to_tip[j];
				}
			}
		
	}

		float section_plane_norm [3] = {end_y_axis[0],end_y_axis[1], end_y_axis[2]};
		float section_plane_a_point [3] = {end_pos[0], end_pos[1], end_pos[2]};

		float Projected_base_pt [3] = {0};
		Proj_Pt_to_Plane(heur_sec_base,section_plane_a_point,section_plane_norm,Projected_base_pt);
	
		///////////////////////////Begin for section from n-1 to 1//////////////

		float vec_base_to_tip[3] = {end_pos[0] - Projected_base_pt[0], end_pos[1] - Projected_base_pt[1], end_pos[2] - Projected_base_pt[2]};
		float mid_pt_base_and_tip [3] = {(end_pos[0] + Projected_base_pt[0])/2, (end_pos[1] + Projected_base_pt[1])/2, (end_pos[2] + Projected_base_pt[2])/2};
		Normalize(vec_base_to_tip,3);
		float vec_per_bisector[3] = {0};
		CrossProduct(end_y_axis,vec_base_to_tip,vec_per_bisector);
		
		// Compute the section center and then the radius of the section circle
		float* sec_center = TwolineIntersection(end_pos, mid_pt_base_and_tip,end_x_axis, vec_per_bisector); // ALMEM
		float adjusted_z_axis [3] = {0};
		float radius = 0;
		if(sec_center != NULL)// 
		{
			 radius = two_pts_dist(sec_center,Projected_base_pt,3);

			// Compute the section base's new z-axis////////////
			////////first compute the rotation direction of the end's z-axis
			float end_z_rot_direction [3] = {0};
			float vec_end_to_center [3] = {sec_center[0] - end_pos[0], sec_center[1] - end_pos[1], sec_center[2] - end_pos[2]};
			Normalize(vec_end_to_center,3);
			CrossProduct(end_z_axis,vec_end_to_center,end_z_rot_direction);
			Normalize(end_z_rot_direction,3);
		
			float vec_base_to_center [3] = {sec_center[0] - Projected_base_pt[0], sec_center[1] - Projected_base_pt[1], sec_center[2] - Projected_base_pt[2]};
			Normalize(vec_base_to_center,3);
	
			CrossProduct(vec_base_to_center,end_y_axis,adjusted_z_axis);
			Normalize(adjusted_z_axis,3);

			// then compute the base z-axis's rotation direction
			float base_z_rot_direction [3] = {0};		
			CrossProduct(adjusted_z_axis,vec_base_to_center,base_z_rot_direction);
			Normalize(base_z_rot_direction,3);

			// the above two rotation direction should be the same, if not, change reverse the direction of the base z-axis
			float dotProcut = DotProduct(base_z_rot_direction,end_z_rot_direction);
			if(dotProcut < 0)
			{
				for(int j = 0; j < 3; ++j)
				{
					adjusted_z_axis[j] = -adjusted_z_axis[j];
				}
			}
		}
		else
		{
			for(int j = 0; j < 3; ++j)
			{
				adjusted_z_axis[j] = vec_base_to_tip[j];
			}
		}

		// update the output arm frames
		for(int j = 0; j < 3; ++j)
		{
			arm_frames[i].z_axis[j] = adjusted_z_axis[j]; // section i's base z-axis
			arm_frames[i+1].z_axis[j] = end_z_axis[j]; // section i's tip z-axis
			arm_frames[i].Pos[j] = Projected_base_pt[j];
		}

		if(sec_center)
		{
			//Compute curvature k
			float sign_x = arm_frames[i+1].x_axis[0]*(sec_center[0] - end_pos[0]) + arm_frames[i+1].x_axis[1]*(sec_center[1] - end_pos[1])+ arm_frames[i+1].x_axis[2]*(sec_center[2] - end_pos[2]); 
			float k =  ((sign_x>0) ? -(1.0/radius) :(1.0/radius));

			// compute the section length s

			float vec_tip_to_center[3] = {sec_center[0] - end_pos[0], sec_center[1] - end_pos[1], sec_center[2] - end_pos[2]};
			float vec_base_to_center[3] = {sec_center[0] - arm_frames[i].Pos[0], sec_center[1] - arm_frames[i].Pos[1], sec_center[2] - arm_frames[i].Pos[2]};

			// check if the tip point is in the opposite direction of the adjusted z-axis.
			float sign = arm_frames[i].z_axis[0]*(end_pos[0] - arm_frames[i].Pos[0]) + arm_frames[i].z_axis[1]*(end_pos[1] - arm_frames[i].Pos[1])+ arm_frames[i].z_axis[2]*(end_pos[2] - arm_frames[i].Pos[2]); 
		
			float theta_angle = (sign > 0) ? Angle_bewteen_two_vecs(vec_tip_to_center,vec_base_to_center):(2*Pi_jinglin- Angle_bewteen_two_vecs(vec_tip_to_center,vec_base_to_center));

			arm_config[3*i] = theta_angle*radius;
			arm_config[3*i+1] =  ((sign_x>0) ? -(1.0/radius) :(1.0/radius));
			arm_config[3*i+2] = -999;
		    delete [] sec_center;	
		}
		else
		{
			float sec_base_pos [3] = {arm_frames[i].Pos[0],arm_frames[i].Pos[1],arm_frames[i].Pos[2]};
			arm_config[3*i]  = two_pts_dist(end_pos,sec_base_pos,3);
			arm_config[3*i+1] = 1.0/999.0;
			arm_config[3*i+2] = 0.0;
		}
	 return true;

	}
*/

bool MotionPlanner::plan_a_config()
{	
	ManipulatorNSections* mani = env->get_mani_in_env();
	int n_sections = mani->get_n_sections();
	vector<float> planned_config(n_sections*3);
	vector<float> planned_config_base_frame(16,0.0);
	vector<Frames> constr_section_frames(n_sections+1);

	float start_pos[3] = {1.0,-0.8,1.0};
	float start_pos_bases_sec1_to_sec3 [9] = {-0.2,-3.0,-1.0, 0.0, -2.5,0.5, 0.7, -2.0,1.0};
	
	float motion_v [3] = {1.0,0.0,0.0};
	float point_cur[3] = {start_pos[0] + 0.01*timeStamp*motion_v[0],start_pos[1] + 0.01*timeStamp*motion_v[1],start_pos[2] + 0.01*timeStamp*motion_v[2]}; 
	float tip_y_axis[3] = {0.0,0.0,1.0};
	
	//////////////Specifying task constraints for each task frame////////////////////////
	float Pos[3] = {point_cur[0],point_cur[1],point_cur[2]}; 
	float tip_z_axis [3] = {0.0, 1.0, 0.0};
	Normalize(tip_z_axis,3);
	float tip_x_aixs [3] = {0};
	CrossProduct(tip_y_axis,tip_z_axis,tip_x_aixs);
	
	TaskConstraints t_constraint(Pos, tip_z_axis, tip_y_axis);
	vector<heuristic_pos> heur_pos(n_sections);

	task_constraints = t_constraint;
	
	for(int  i = n_sections-1; i >= 0; --i)
	{
		float  heur_base[3] = {0};
		heur_base[0] = start_pos_bases_sec1_to_sec3[i*3+0]+0.005*timeStamp*motion_v[0];
		heur_base[1] = start_pos_bases_sec1_to_sec3[i*3+1]+0.005*timeStamp*motion_v[1];
		heur_base[2] = start_pos_bases_sec1_to_sec3[i*3+2]+0.005*timeStamp*motion_v[2];

		float heur_base_z_axis[3] = {0.0,0.0,1.0};
		float rotating_dirction[3] = {0.0, 0.0, 1.0};
		heuristic_pos heur_pos_i (heur_base,heur_base_z_axis,rotating_dirction);
		heur_pos[i] = heur_pos_i;	
	}

	compute_section_frames_based_on_end_effector_fixedBase_position(constr_section_frames,planned_config,heur_pos);
	for(int i = 0; i < 3; ++i)
	{
		planned_config_base_frame[i] = constr_section_frames[0].x_axis[i];
		planned_config_base_frame[4+i] = constr_section_frames[0].y_axis[i];
		planned_config_base_frame[8+i] = constr_section_frames[0].z_axis[i];
		planned_config_base_frame[12+i] = constr_section_frames[0].Pos[i];
	}
	planned_config_base_frame[15] = 1.0;
	
	planned_config = planned_config;
	planned_arm_base_frame = planned_config_base_frame;
	
	planned_path.push_back(planned_config);
	planned_path_base_frames.push_back(planned_config_base_frame);

	return true;
}

/*
bool MotionPlanner::PlanAConfig_prossive_grasp(vector<vector<vector<float> > > section_plane_polys, vector<int> cur_reach_vex_idx_each_sec)
{
	
	ManipulatorNSections* mani = env->get_mani_in_env();
	int n_sections = mani->get_n_sections();
	float next_target_tip_pos [3] = {section_plane_polys[cur_reach_vex_idx_each_sec]};
	for(int i = 0; i < 3; ++i)
	{
		mani->inverseKinematic_one_section
	}
}
*/
