#include "Environment.h"
#include <iostream>
#include <fstream>
#include <assert.h>
#include "SOIL.h"
#include "MathOperations.h"

GLuint   texture[2];
	
///////utility functions for the environment............
bool PointInFrustum( float frustum [6][4], float x, float y, float z )
{
   int p;
   for( p = 0; p < 6; p++ )
      if( frustum[p][0] * x + frustum[p][1] * y + frustum[p][2] * z + frustum[p][3] <= 0 )
         return false;
   return true;
}


///////////////////////////////////////////////////////

int LoadGLTextures()                                    // Load images
{
    /* load an image file directly as a new OpenGL texture */
    texture[0] = SOIL_load_OGL_texture(
		"/home/huitan/codes/continuum_manipulator/models/mesh_textures/meshtexture1.jpg",
		SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_INVERT_Y 
	);
 
   /* check for an error during the load process */
	if( 0 == texture[0] )
	{
		printf( "SOIL loading error: '%s'\n", SOIL_last_result() );
		return 0;
	}
	
	glBindTexture(GL_TEXTURE_2D, texture[0]);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
 
    return 1;                                        // Return Success
}


///////////////////////////////////////
void Environment::ExtractFrustum(float frustum[6][4]) // compute the Frustum planes according to current prospective matrix and model view matrix
{
   float   proj[16];
   float   modl[16];
   float   clip[16];
   float   t;
   /* Get the current PROJECTION matrix from OpenGL */
   glGetFloatv( GL_PROJECTION_MATRIX, proj );

   /* Get the current MODELVIEW matrix from OpenGL */
   glGetFloatv( GL_MODELVIEW_MATRIX, modl );

   /* Combine the two matrices (multiply projection by modelview) */
   clip[ 0] = modl[ 0] * proj[ 0] + modl[ 1] * proj[ 4] + modl[ 2] * proj[ 8] + modl[ 3] * proj[12];
   clip[ 1] = modl[ 0] * proj[ 1] + modl[ 1] * proj[ 5] + modl[ 2] * proj[ 9] + modl[ 3] * proj[13];
   clip[ 2] = modl[ 0] * proj[ 2] + modl[ 1] * proj[ 6] + modl[ 2] * proj[10] + modl[ 3] * proj[14];
   clip[ 3] = modl[ 0] * proj[ 3] + modl[ 1] * proj[ 7] + modl[ 2] * proj[11] + modl[ 3] * proj[15];

   clip[ 4] = modl[ 4] * proj[ 0] + modl[ 5] * proj[ 4] + modl[ 6] * proj[ 8] + modl[ 7] * proj[12];
   clip[ 5] = modl[ 4] * proj[ 1] + modl[ 5] * proj[ 5] + modl[ 6] * proj[ 9] + modl[ 7] * proj[13];
   clip[ 6] = modl[ 4] * proj[ 2] + modl[ 5] * proj[ 6] + modl[ 6] * proj[10] + modl[ 7] * proj[14];
   clip[ 7] = modl[ 4] * proj[ 3] + modl[ 5] * proj[ 7] + modl[ 6] * proj[11] + modl[ 7] * proj[15];

   clip[ 8] = modl[ 8] * proj[ 0] + modl[ 9] * proj[ 4] + modl[10] * proj[ 8] + modl[11] * proj[12];
   clip[ 9] = modl[ 8] * proj[ 1] + modl[ 9] * proj[ 5] + modl[10] * proj[ 9] + modl[11] * proj[13];
   clip[10] = modl[ 8] * proj[ 2] + modl[ 9] * proj[ 6] + modl[10] * proj[10] + modl[11] * proj[14];
   clip[11] = modl[ 8] * proj[ 3] + modl[ 9] * proj[ 7] + modl[10] * proj[11] + modl[11] * proj[15];

   clip[12] = modl[12] * proj[ 0] + modl[13] * proj[ 4] + modl[14] * proj[ 8] + modl[15] * proj[12];
   clip[13] = modl[12] * proj[ 1] + modl[13] * proj[ 5] + modl[14] * proj[ 9] + modl[15] * proj[13];
   clip[14] = modl[12] * proj[ 2] + modl[13] * proj[ 6] + modl[14] * proj[10] + modl[15] * proj[14];
   clip[15] = modl[12] * proj[ 3] + modl[13] * proj[ 7] + modl[14] * proj[11] + modl[15] * proj[15];

   /* Extract the numbers for the RIGHT plane */
   frustum[0][0] = clip[ 3] - clip[ 0];
   frustum[0][1] = clip[ 7] - clip[ 4];
   frustum[0][2] = clip[11] - clip[ 8];
   frustum[0][3] = clip[15] - clip[12];

   /* Normalize the result */
   t = sqrt( frustum[0][0] * frustum[0][0] + frustum[0][1] * frustum[0][1] + frustum[0][2] * frustum[0][2] );
   frustum[0][0] /= t;
   frustum[0][1] /= t;
   frustum[0][2] /= t;
   frustum[0][3] /= t;

   /* Extract the numbers for the LEFT plane */
   frustum[1][0] = clip[ 3] + clip[ 0];
   frustum[1][1] = clip[ 7] + clip[ 4];
   frustum[1][2] = clip[11] + clip[ 8];
   frustum[1][3] = clip[15] + clip[12];

   /* Normalize the result */
   t = sqrt( frustum[1][0] * frustum[1][0] + frustum[1][1] * frustum[1][1] + frustum[1][2] * frustum[1][2] );
   frustum[1][0] /= t;
   frustum[1][1] /= t;
   frustum[1][2] /= t;
   frustum[1][3] /= t;

   /* Extract the BOTTOM plane */
   frustum[2][0] = clip[ 3] + clip[ 1];
   frustum[2][1] = clip[ 7] + clip[ 5];
   frustum[2][2] = clip[11] + clip[ 9];
   frustum[2][3] = clip[15] + clip[13];

   /* Normalize the result */
   t = sqrt( frustum[2][0] * frustum[2][0] + frustum[2][1] * frustum[2][1] + frustum[2][2] * frustum[2][2] );
   frustum[2][0] /= t;
   frustum[2][1] /= t;
   frustum[2][2] /= t;
   frustum[2][3] /= t;

   /* Extract the TOP plane */
   frustum[3][0] = clip[ 3] - clip[ 1];
   frustum[3][1] = clip[ 7] - clip[ 5];
   frustum[3][2] = clip[11] - clip[ 9];
   frustum[3][3] = clip[15] - clip[13];

   /* Normalize the result */
   t = sqrt( frustum[3][0] * frustum[3][0] + frustum[3][1] * frustum[3][1] + frustum[3][2] * frustum[3][2] );
   frustum[3][0] /= t;
   frustum[3][1] /= t;
   frustum[3][2] /= t;
   frustum[3][3] /= t;

   /* Extract the FAR plane */
   frustum[4][0] = clip[ 3] - clip[ 2];
   frustum[4][1] = clip[ 7] - clip[ 6];
   frustum[4][2] = clip[11] - clip[10];
   frustum[4][3] = clip[15] - clip[14];

   /* Normalize the result */
   t = sqrt( frustum[4][0] * frustum[4][0] + frustum[4][1] * frustum[4][1] + frustum[4][2] * frustum[4][2] );
   frustum[4][0] /= t;
   frustum[4][1] /= t;
   frustum[4][2] /= t;
   frustum[4][3] /= t;

   /* Extract the NEAR plane */
   frustum[5][0] = clip[ 3] + clip[ 2];
   frustum[5][1] = clip[ 7] + clip[ 6];
   frustum[5][2] = clip[11] + clip[10];
   frustum[5][3] = clip[15] + clip[14];

   /* Normalize the result */
   t = sqrt( frustum[5][0] * frustum[5][0] + frustum[5][1] * frustum[5][1] + frustum[5][2] * frustum[5][2] );
   frustum[5][0] /= t;
   frustum[5][1] /= t;
   frustum[5][2] /= t;
   frustum[5][3] /= t;
}

Object::Object(string infilename, vector<float> _basetransform)
{
	basetransform.resize(16);
	// Add vertex normals as default property 
	 mesh.request_vertex_normals();
	 // Add face normals as default property
	 mesh.request_face_normals();
	 OpenMesh::IO::Options opt;
	if(! OpenMesh::IO::read_mesh(mesh, infilename))
	{
		std::cerr << "Error: Cannot read mesh from " << infilename << std::endl;
	}
	 
	 for (MyMesh::VertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it)
	 {
		MyMesh::Point vert = mesh.point(*v_it);
		float point_original [4] = {vert[0],vert[1],vert[2],1.0};
		float base_trans_f [16] = {0.0};
		
		for(int i = 0; i < 16; ++i)
		{
			base_trans_f[i] = _basetransform[i];
			basetransform[i] = _basetransform[i];
		}
		float* point_after = MatrixMulti(base_trans_f,point_original,4,4,4,1);
		MyMesh::Point point_new = MyMesh::Point(point_after[0], point_after[1],  point_after[2]);
		mesh.set_point( *v_it, point_new);
		delete [] point_after;
	 }
	 
	 // ReCompute the normals ...
	  mesh.update_normals();  
	  int n_faces = mesh.n_faces();
	  seen_faces = vector<bool>(n_faces,false);
}

Object::Object(MyMesh _myMesh, vector<float> _basetransform)
{
	 mesh = _myMesh;
	 basetransform.resize(16);

	 for (MyMesh::VertexIter v_it = mesh.vertices_begin(); v_it != mesh.vertices_end(); ++v_it)
	 {
		 
		MyMesh::Point vert = mesh.point(*v_it);
	
		float point_original [4] = {vert[0],vert[1],vert[2],1.0};
		float base_trans_f [16] = {0.0};
	
		for(int i = 0; i < 16; ++i)
		{
			base_trans_f[i] = _basetransform[i];
			basetransform[i] = _basetransform[i];
		}
		
		float* point_after = MatrixMulti(base_trans_f,point_original,4,4,4,1);
		MyMesh::Point point_new = MyMesh::Point(point_after[0], point_after[1],  point_after[2]);
		
		mesh.set_point(*v_it, point_new);
		
		delete [] point_after;
	
	 }
	 
	 // ReCompute the normals ...
	  mesh.update_normals();  
	 
	  int n_faces = mesh.n_faces();
	  seen_faces = vector<bool>(n_faces,false);
	 
}


Object::Object(MyMesh_poly _myMesh, vector<float> _basetransform)
{
	 mesh_poly = _myMesh;
	 basetransform.resize(16);

	 for (MyMesh_poly::VertexIter v_it = mesh_poly.vertices_begin(); v_it != mesh_poly.vertices_end(); ++v_it)
	 {
		 
		MyMesh_poly::Point vert = mesh_poly.point(*v_it);
	
		float point_original [4] = {vert[0],vert[1],vert[2],1.0};
		float base_trans_f [16] = {0.0};
	
		for(int i = 0; i < 16; ++i)
		{
			base_trans_f[i] = _basetransform[i];
			basetransform[i] = _basetransform[i];
		}
		
		float* point_after = MatrixMulti(base_trans_f,point_original,4,4,4,1);
		MyMesh_poly::Point point_new = MyMesh_poly::Point(point_after[0], point_after[1],  point_after[2]);
		
		mesh_poly.set_point(*v_it, point_new);
		
		delete [] point_after;
	
	 }
	 
	 // ReCompute the normals ...
	  mesh_poly.update_normals();  
	 
	  int n_faces = mesh_poly.n_faces();
	  seen_faces = vector<bool>(n_faces,false);
	
}

void Object::get_mesh(MyMesh &_outmesh)
{
	_outmesh = mesh;
}

void Object::drawObject()
{
	MyMesh::FaceIter          f_it, f_end(mesh.faces_end());
	MyMesh::FaceVertexIter    fv_it;

    glBegin(GL_TRIANGLES);
    MyMesh::Point vert_previous;
    for(f_it=mesh.faces_begin(); f_it!=f_end; ++f_it)
    {
	  MyMesh::Normal norm = mesh.normal(*f_it);
	  glNormal3f(norm[0],norm[1],norm[2]);
      for (fv_it=mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it)
      {
			MyMesh::Point vert = mesh.point(*fv_it);
		    glColor3f(0.5, 0.5, 0.5);
		    glVertex3f(vert[0], vert[1], vert[2]);
      }
    }
    glEnd();
}

void Object::drawObject(float translation_3f [3], float scale_3f [3], float orientation[9])
{
	MyMesh::FaceIter          f_it, f_end(mesh.faces_end());
	MyMesh::FaceVertexIter    fv_it;
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();			
	float orientationM [16] = {orientation[0],orientation[1],orientation[2],0.0,
							   orientation[3],orientation[4],orientation[5],0.0,
							   orientation[6],orientation[7],orientation[8],0.0,
							   translation_3f[0],translation_3f[1],translation_3f[2],1.0};
	glMultMatrixf(orientationM);	
	glScalef(scale_3f[0],scale_3f[1],scale_3f[2]);
    MyMesh::Point vert_previous;
    
    //draw mesh grids		
    for(f_it=mesh.faces_begin(); f_it!=f_end; ++f_it)
    {
	
	  glBegin(GL_LINE_LOOP);
	  MyMesh::Normal norm = mesh.normal(*f_it);
	  glNormal3f(norm[0],norm[1],norm[2]);
	  int color_count = 0;
      for (fv_it=mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it)
      {
		    //~ if(color_count%3 == 0)
		    //~ {
				 //~ glColor3f(1.0, 1.0, 0.0);
			//~ }
			//~ if(color_count%3 == 1)
		    //~ {
				 //~ glColor3f(0.0, 1.0, 1.0);
			//~ }
			//~ if(color_count%3 == 2)
		    //~ {
				 //~ glColor3f(1.0, 0.0, 1.0);
			//~ }	
			glColor3f(0.0, 0.0, 0.6);		
			MyMesh::Point vert = mesh.point(*fv_it);   
		    glVertex3f(vert[0], vert[1], vert[2]);
		    color_count++;
      }
       glEnd();
     
    } 
  // draw mesh triangles  
    for(f_it=mesh.faces_begin(); f_it!=f_end; ++f_it)
    {
	  glBegin(GL_TRIANGLES); 
	  MyMesh::Normal norm = mesh.normal(*f_it);
	  glNormal3f(norm[0],norm[1],norm[2]);
	  int color_count = 0;
      for (fv_it=mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it)
      {
		    //~ if(color_count%3 == 0)
		    //~ {
				 //~ glColor3f(1.0, 1.0, 0.0);
			//~ }
			//~ if(color_count%3 == 1)
		    //~ {
				 //~ glColor3f(0.0, 1.0, 1.0);
			//~ }
			//~ if(color_count%3 == 2)
		    //~ {
				 //~ glColor3f(1.0, 0.0, 1.0);
			//~ }		
			glColor3f(0.7, 0.7, 0.7);	
			MyMesh::Point vert = mesh.point(*fv_it);   
		    glVertex3f(vert[0], vert[1], vert[2]);
		    color_count++;
      }

     glEnd();
    } 
    glPopMatrix();
}

void Object::drawObject_poly(int max_idx_face)
{
	MyMesh_poly::FaceIter          f_it, f_end(mesh_poly.faces_end());
	MyMesh_poly::FaceVertexIter    fv_it;
    int n_faces = mesh_poly.n_faces();
  
    int idx = 0;
  
    for(f_it=mesh_poly.faces_begin(); f_it!=f_end; ++f_it)
    {
	  if((idx > max_idx_face && idx <= 5 && max_idx_face < 3) || (idx > max_idx_face+6))
	  {
		idx++;
		continue;
	  }
	  vector<vector<float> > face_vertices(0);
	  MyMesh_poly::Normal norm = mesh_poly.calc_face_normal (*f_it); 
	
      for (fv_it=mesh_poly.fv_iter(*f_it); fv_it.is_valid(); ++fv_it)
      {    
			MyMesh_poly::Point vert = mesh_poly.point(*fv_it);	    
		    float temp [3] = {vert[0], vert[1], vert[2]}; 
		    face_vertices.push_back(vector<float>(temp,temp+3));
      }
	  if(idx > 5)
	  {
		  glColor3f(0.3, 0.3, 0.3);
		  glBegin(GL_QUADS);
		  glNormal3f(-norm[0],-norm[1],-norm[2]); 		
		  for(int i = 0; i <face_vertices.size();++i)
		  {   
			 glVertex3f(face_vertices[i][0],face_vertices[i][1],face_vertices[i][2]); 
		  }
		  glEnd();
	  }
	  else
	  {
		  glColor3f(0.0, 0.5, 0.5);
		  glBegin(GL_QUADS);
		  glNormal3f(norm[0],norm[1],norm[2]); 		
		  for(int i = 0; i <face_vertices.size();++i)
		  {   
			 glVertex3f(face_vertices[i][0],face_vertices[i][1],face_vertices[i][2]); 
		  }
		  glEnd();
		  
		  glBegin(GL_QUADS);
		  glNormal3f(-norm[0],-norm[1],-norm[2]); 	
		  for(int i =face_vertices.size()-1; i >= 0;--i)
		  {   
			  float temp_pt[3] = {0.0};
			  for(int j = 0; j <3 ; ++j)
			  {
				  temp_pt[j] = face_vertices[i][j]-0.003*norm[j];
			  }
			
			 glVertex3f(temp_pt[0],temp_pt[1],temp_pt[2]); 
		  }
		  glEnd();
	  }
	  
      //corresponding obstacles..............
	  if(idx > 5)
	  {
		   // face 0.............
		  glBegin(GL_QUADS);
		  glNormal3f(norm[0],norm[1],norm[2]);
		  for (int i = 0; i < face_vertices.size();++i)
		  {     		
			glVertex3f(face_vertices[i][0]+norm[0]*0.3,face_vertices[i][1]+norm[1]*0.3,face_vertices[i][2]+norm[2]*0.3);
		  }
		  glEnd();
	
		  float vec1 [3] = {0.0};
		  float vec2 [3] = {0.0};
		  for(int j = 0; j < 3; ++j)
		  {
			vec1[j] =  face_vertices[1][j] - face_vertices[0][j];
			vec2[j] =  face_vertices[1][j]+norm[j]*0.3 - face_vertices[1][j];
		  }
		  
		  // face 1.............
		  float norm_tmp[3] = {0.0};
		  CrossProduct(vec1,vec2,norm_tmp);
		  Normalize(norm_tmp,3);
		  glNormal3f(norm_tmp[0],norm_tmp[1],norm_tmp[2]);
		  glBegin(GL_QUADS);
		  glVertex3f(face_vertices[0][0], face_vertices[0][1], face_vertices[0][2]);
		  glVertex3f(face_vertices[1][0], face_vertices[1][1], face_vertices[1][2]);
		  glVertex3f(face_vertices[1][0]+norm[0]*0.3, face_vertices[1][1]+norm[1]*0.3, face_vertices[1][2]+norm[2]*0.3);
		  glVertex3f(face_vertices[0][0]+norm[0]*0.3, face_vertices[0][1]+norm[1]*0.3, face_vertices[0][2]+norm[2]*0.3);
		  glEnd();
		
		 
		  // face 2.............
		  for(int j = 0; j < 3; ++j)
		  {
			vec1[j] =  face_vertices[3][j] - face_vertices[2][j];
			vec2[j] =  face_vertices[3][j]+norm[j]*0.3 - face_vertices[3][j];
		  }
		  CrossProduct(vec1,vec2,norm_tmp);
		  Normalize(norm_tmp,3);
		  glNormal3f(norm_tmp[0],norm_tmp[1],norm_tmp[2]);
		  glBegin(GL_QUADS);
		  glVertex3f(face_vertices[2][0], face_vertices[2][1], face_vertices[2][2]);
		  glVertex3f(face_vertices[3][0], face_vertices[3][1], face_vertices[3][2]);
		  glVertex3f(face_vertices[3][0]+norm[0]*0.3, face_vertices[3][1]+norm[1]*0.3, face_vertices[3][2]+norm[2]*0.3);
		  glVertex3f(face_vertices[2][0]+norm[0]*0.3, face_vertices[2][1]+norm[1]*0.3, face_vertices[2][2]+norm[2]*0.3);
		  glEnd();
		 
		  
		  // face 3.............
		  for(int j = 0; j < 3; ++j)
		  {
			vec1[j] =  face_vertices[0][j]+norm[j]*0.3 - face_vertices[0][j];
			vec2[j] =  face_vertices[3][j]+norm[j]*0.3 - face_vertices[0][j]+norm[j]*0.3;
		  }
		  CrossProduct(vec1,vec2,norm_tmp);
		  Normalize(norm_tmp,3);
		  glNormal3f(norm_tmp[0],norm_tmp[1],norm_tmp[2]);
		  glBegin(GL_QUADS);
		  glVertex3f(face_vertices[0][0], face_vertices[0][1], face_vertices[0][2]);
		  glVertex3f(face_vertices[0][0]+norm[0]*0.3, face_vertices[0][1]+norm[1]*0.3, face_vertices[0][2]+norm[2]*0.3);
		  glVertex3f(face_vertices[3][0]+norm[0]*0.3, face_vertices[3][1]+norm[1]*0.3, face_vertices[3][2]+norm[2]*0.3);
		  glVertex3f(face_vertices[3][0], face_vertices[3][1], face_vertices[3][2]); 
		  glEnd();
		
		  // face 4.............
		  for(int j = 0; j < 3; ++j)
		  {
			vec1[j] =  face_vertices[2][j] - face_vertices[1][j];
			vec2[j] =  face_vertices[2][j]+norm[j]*0.3 - face_vertices[2][j];
		  }
		  CrossProduct(vec1,vec2,norm_tmp);
		  Normalize(norm_tmp,3);
		  glNormal3f(norm_tmp[0],norm_tmp[1],norm_tmp[2]);
		  glBegin(GL_QUADS);
		  glVertex3f(face_vertices[1][0], face_vertices[1][1], face_vertices[1][2]);
		  glVertex3f(face_vertices[2][0], face_vertices[2][1], face_vertices[2][2]);
		  glVertex3f(face_vertices[2][0]+norm[0]*0.3, face_vertices[2][1]+norm[1]*0.3, face_vertices[2][2]+norm[2]*0.3);
		  glVertex3f(face_vertices[1][0]+norm[0]*0.3, face_vertices[1][1]+norm[1]*0.3, face_vertices[1][2]+norm[2]*0.3);
		  glEnd();
		 
	  }
		idx++;
    }
  
}


void Object::DrawInFrustumObject(float Frustum[6][4])
{
	MyMesh::FaceIter          f_it, f_end(mesh.faces_end());
	MyMesh::FaceVertexIter    fv_it;
    int face_idx = 0;
    int size_seen_faces = seen_faces.size();
    
    for(f_it=mesh.faces_begin(); f_it!=f_end; ++f_it, face_idx++)
    {	
		
		vector<vector<float> > vertex_to_draw(0);
		MyMesh::Normal norm = mesh.normal(*f_it);
		for(fv_it=mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it)
		{
		
			MyMesh::Point vert = mesh.point(*fv_it);	
			float curPt[3] = {vert[0], vert[1], vert[2]};
			if(!PointInFrustum(Frustum,curPt[0],curPt[1],curPt[2]))
			{
				break;
			}
			else
			{
				vector<float> pt(curPt,curPt+3);
				vertex_to_draw.push_back(pt);
			} 
		}
		if(vertex_to_draw.size() == 3)
		{
			
			this->seen_faces[face_idx] = true;
		}
		
		if(vertex_to_draw.size() != 3 && this->seen_faces[face_idx] == false)
		{	
			continue;
		}
		else 
		{		 
			 glBegin(GL_TRIANGLES);
			 glNormal3f(norm[0],norm[1],norm[2]);
			 glColor3f(0.5, 0.5, 0.5);
			 for(fv_it=mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it)
			 {
				MyMesh::Point vert = mesh.point(*fv_it);	
				glVertex3f(vert[0], vert[1],vert[2]);
			 }
			 glEnd();			
		}
    }
}

Environment::Environment(float* _mani_config, int n_ManiSections, float* _maniBaseFrame, vector<Object> _objects)
{
	quadratic_x = gluNewQuadric();
	quadratic_y = gluNewQuadric();
	quadratic_z = gluNewQuadric();

	if(n_ManiSections != 0)
	{
		mani = new ManipulatorNSections(n_ManiSections,_mani_config,_maniBaseFrame);
	}
	objects = _objects;
}

Environment::Environment(float* _mani_config, int n_ManiSections, float* _maniBaseFrame)
{
	quadratic_x = gluNewQuadric();
	quadratic_y = gluNewQuadric();
	quadratic_z = gluNewQuadric();
	
	if(n_ManiSections != 0)
	{
		mani = new ManipulatorNSections(n_ManiSections,_mani_config,_maniBaseFrame);
	}
	
}

Environment::Environment(float* _mani_config, int n_ManiSections, float* _maniBaseFrame, const vector<string> & pcd_paths)
{
	quadratic_x = gluNewQuadric();
	quadratic_y = gluNewQuadric();
	quadratic_z = gluNewQuadric();

	if(n_ManiSections != 0)
	{
		mani = new ManipulatorNSections(n_ManiSections,_mani_config,_maniBaseFrame);
	}
	point_clouds_.resize(pcd_paths.size());

	for(int i = 0; i < pcd_paths.size(); ++i)
	{	
		pcl::PointCloud<pcl::PointXYZRGBA>::Ptr tmp(new  pcl::PointCloud<pcl::PointXYZRGBA>);
		point_clouds_[i] = tmp;
		pcl::io::loadPCDFile<pcl::PointXYZRGBA>(pcd_paths[i], *point_clouds_[i]);
	}
	
	LoadGLTextures();
	
}

Environment::Environment(float* _mani_config, int n_ManiSections, float* _maniBaseFrame, const vector<string> & pcd_paths, const string & _mesh_obj_path) : 
	parameters()
{
	quadratic_x = gluNewQuadric();
	quadratic_y = gluNewQuadric();
	quadratic_z = gluNewQuadric();

	if(n_ManiSections != 0)
	{
		mani = new ManipulatorNSections(n_ManiSections,_mani_config,_maniBaseFrame);
	}
	point_clouds_.resize(pcd_paths.size());

	for(int i = 0; i < pcd_paths.size(); ++i)
	{	
		pcl::PointCloud<pcl::PointXYZRGBA>::Ptr tmp(new  pcl::PointCloud<pcl::PointXYZRGBA>);
		point_clouds_[i] = tmp;
		pcl::io::loadPCDFile<pcl::PointXYZRGBA>(pcd_paths[i], *point_clouds_[i]);
	}
	
	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr sensed_tmp(new  pcl::PointCloud<pcl::PointXYZRGBA>);
	sensed_pnt_clouds = sensed_tmp;
	
	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr newly_sensed_tmp(new  pcl::PointCloud<pcl::PointXYZRGBA>);
	newly_sensed_pnt_clouds = newly_sensed_tmp;
	
	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr next_model_pcl_tmp(new pcl::PointCloud<pcl::PointXYZRGBA>);
	next_model_pcl = next_model_pcl_tmp;
	
	current_model_index = 0;
	min_cluster_pnt_size = 50;
	clustered_pnt_clouds_ind = 0;
	cluster_tolerance = 0.2; // 10 cm
	
	LoadGLTextures();
	float obj_baseFrame_f [16] = {1,0,0,0,
							    0,1,0,0,
							    0,0,1,0,
							    0,0,0,1};
	vector<float> obj_baseFrame_v(obj_baseFrame_f,obj_baseFrame_f+16); 				    
	Object target_obj(_mesh_obj_path, obj_baseFrame_v);
	objects.push_back(target_obj);
}

void Environment::drawWorldCoord()
{
	glPushMatrix();
	glColor3f(1.0,0.0,0.0);
	glRotatef(90.0,0.0,1.0,0.0);
	gluCylinder(quadratic_x,0.02,0.02,0.5,50,50);
	glPopMatrix();

	glPushMatrix();
	glColor3f(0.0,1.0,0.0);
	glRotatef(-90.0,1.0,0.0,0.0);
	gluCylinder(quadratic_y,0.02,0.02,0.5,50,50);
	glPopMatrix();

	glColor3f(0.0,0.0,1.0);
	gluCylinder(quadratic_z,0.02,0.02,0.5,50,50);
}

bool Environment::get_cur_arm_config(vector<float> & config)
{
	config = mani->get_config();
}

bool Environment::update_arm_config(float* arm_config)
{
	int nsections = mani->get_n_sections();
	vector<float> arm_config_vec (arm_config,arm_config+3*nsections);

	if(mani->update_config(arm_config_vec))
		return true;
	else
		return false;
}

bool Environment::update_arm_config(vector<float> arm_config)
{
	if(mani->update_config(arm_config))
		return true;
	else
		return false;
}

bool Environment::update_arm_config(vector<float> arm_config, vector<float> base_frame_update)
{
	if(mani->update_config(arm_config,base_frame_update))
		return true;
	else
		return false;
}

ManipulatorNSections* Environment::get_mani_in_env()
{
	assert(mani);
	return mani;
}

void Environment::drawManipulator()
{
	mani->draw_sections();
}

void Environment::drawManipulator_Mesh()
{

	int nSections = mani->get_n_sections();
	vector<MyMesh> ArmSectionMeshes(nSections);
    float section_width = mani->get_sec_width();
	vector<float> Config = mani->get_config();
		
	vector<float> s(nSections);
	vector<float> k(nSections);
	vector<float> phi(nSections);
	vector<float> r(nSections);

	for(int i = 0; i < nSections; ++i)
	{
		s[i] = Config[i*3];
		k[i] = Config[i*3+1];
		r[i] = 1/fabs(k[i]);
		phi[i] = Config[i*3+2];
	}

	vector<vector<float> >RotaAboutZs(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		float RotaAboutZi[16] = {cos(phi[i]),sin(phi[i]),0,0, -sin(phi[i]),cos(phi[i]),0,0, 0,0,1,0, 0,0,0,1};
		vector<float> temp(RotaAboutZi,RotaAboutZi+16);
		RotaAboutZs[i] = temp;
	}

	vector<float>ForwardTransMat(nSections*16);
	if(mani->trans_matrix.size() == 0)
	{
		mani->compute_trans_matrix(ForwardTransMat);
	}
	else
	{
		ForwardTransMat = mani->trans_matrix;
	}
	
	vector<vector<float> >endPoints(nSections+1, vector<float>(3,0));
	
	for(int i = 1; i < nSections+1; ++i)
	{	
		float endPoints_tmp [3] = {ForwardTransMat[(i-1)*16+12],ForwardTransMat[(i-1)*16+13],ForwardTransMat[(i-1)*16+14]};
		endPoints[i] = vector<float>(endPoints_tmp,endPoints_tmp+3);
	}


	vector<vector<float> >rotateEPs(nSections);
	for(int i = 1; i < nSections; ++i)
	{
		float* rotateEPi = MatrixMulti(&ForwardTransMat[(i-1)*16], RotaAboutZs[i],4,4,4,4); //ALMEM
		rotateEPs[i] = vector<float>(rotateEPi,rotateEPi+16);
		delete [] rotateEPi;
	}

	vector<vector<float> >eP_xs(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		float eP_xs_tmp [3] = {ForwardTransMat[0+16*i],ForwardTransMat[1+16*i],ForwardTransMat[2+16*i]};
		eP_xs[i] = vector<float>(eP_xs_tmp,eP_xs_tmp+3);
	}

	vector<vector<float> >ReP_xs(nSections);
	vector<vector<float> >ReP_ys(nSections);
	vector<vector<float> >ReP_zs(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		if(i == 0)
		{
			float ReP_xs_tmp [3] = {cos(phi[0]),sin(phi[0]),0};
			float ReP_ys_tmp [3] = {-sin(phi[0]),cos(phi[0]),0};
			float ReP_zs_tmp [3] = {0,0,1};
			ReP_xs[i] = vector<float> (ReP_xs_tmp,ReP_xs_tmp+3);
			ReP_ys[i] = vector<float> (ReP_ys_tmp,ReP_ys_tmp+3);
			ReP_zs[i] = vector<float> (ReP_zs_tmp,ReP_zs_tmp+3);
		}
		else
		{		
			float ReP_xs_tmp [3] = {rotateEPs[i][0],rotateEPs[i][1],rotateEPs[i][2]};
			float ReP_ys_tmp [3] = {rotateEPs[i][4],rotateEPs[i][5],rotateEPs[i][6]};
			float ReP_zs_tmp [3] = {rotateEPs[i][8],rotateEPs[i][9],rotateEPs[i][10]};
			ReP_xs[i] = vector<float> (ReP_xs_tmp,ReP_xs_tmp+3);
			ReP_ys[i] = vector<float> (ReP_ys_tmp,ReP_ys_tmp+3);
			ReP_zs[i] = vector<float> (ReP_zs_tmp,ReP_zs_tmp+3);
		}
	}

	vector<vector<float> >Cents(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		float endpt1[3]= {endPoints[i][0], endPoints[i][1],endPoints[i][2]};
		float endpt2[3]= {endPoints[i+1][0], endPoints[i+1][1],endPoints[i+1][2]};
		float pt1_vec[3] = {ReP_xs[i][0],ReP_xs[i][1], ReP_xs[i][2]};
		float pt2_vec[3] = {eP_xs[i][0], eP_xs[i][1], eP_xs[i][2]};
		float *Centi = TwolineIntersection(endpt1,endpt2,pt1_vec,pt2_vec);//ALMEM
		
		if(!Centi)
		{
			float vec_enp1_enpt2[3] = {endpt2[0] - endpt1[0], endpt2[1] - endpt1[1], endpt2[2] - endpt1[2]};
			Normalize(vec_enp1_enpt2,3);
			float cross_product[3] = {0.0};
			CrossProduct(vec_enp1_enpt2,pt1_vec,cross_product);
			float cross_product_len = norm_vec_3(cross_product);
			if(fabs(cross_product_len) <= 0.0001)
			{
				float center_tmp [3] = {(endpt2[0] + endpt1[0])/2, (endpt2[1] + endpt1[1])/2, (endpt2[2] + endpt1[2])/2};
				Cents[i] = vector<float>(center_tmp,center_tmp+3);
			}
			else
			{
				Cents[i].clear();
			}
		}
		else
		{
			Cents[i] = vector<float>(Centi,Centi+3);
		}
		delete [] Centi;
		
		float base_tmp[3] = {0};
		float central_axis_dir_tmp[3] = {0};
		float bend_dir_tmp [3] = {0.0};
		float bend_plane_normal_tmp[3] = {0.0};
		float cylind_radius_tmp = section_width;
		float cur_cen_tmp [3] = {0.0};
		float cur_rad_tmp = r[i];
		float length_tmp = s[i];
		for(int j = 0; j < 3; ++j)
		{
			base_tmp[j] = endpt1[j];
			central_axis_dir_tmp[j] = ReP_zs[i][j];
		
			if(Cents[i].size() == 0)
			{
				cur_cen_tmp == NULL;
				cur_rad_tmp = 0.0;
				bend_dir_tmp[j] = ReP_xs[i][j];
				
			}
			else
			{
				cur_cen_tmp[j] = Cents[i][j];
				bend_dir_tmp[j] = ( Cents[i][j] - base_tmp[j]);
			}
			
		}

		Normalize(central_axis_dir_tmp,3);
		Normalize(bend_dir_tmp,3);
		
		CrossProduct(central_axis_dir_tmp,bend_dir_tmp,bend_plane_normal_tmp);
		if(Cents[i].size() == 0)
		{
			CreatCylinderTriMeshes(ArmSectionMeshes[i], base_tmp,central_axis_dir_tmp,bend_dir_tmp, bend_plane_normal_tmp, cylind_radius_tmp, NULL, cur_rad_tmp,length_tmp);
		}
		else
		{
		   CreatCylinderTriMeshes(ArmSectionMeshes[i], base_tmp,central_axis_dir_tmp,bend_dir_tmp, bend_plane_normal_tmp, cylind_radius_tmp,cur_cen_tmp, cur_rad_tmp,length_tmp);
		}
	   
	}

 //Begin to draw Meshes...........
	float base_frame_f[16] = {0};
	for(int i =0; i < 16; ++i)
	{
		base_frame_f[i] = mani->base_trans_matrix[i];
	}
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix(); // save the model view matrix before setting the base frame
	glMultMatrixf(base_frame_f);
	
	for(int i = 0; i < nSections; ++i)
	{
		if( i == 0)
		{
		   glColor3f(0.0,0.0,0.0);
		}
		else if (i == 1)
		{
		   glColor3f(1.0,0.0,0.0);
		}
		else if( i == 2)
		{
		   glColor3f(0.0,1.0,0.0);
		}
		else if( i == 3)
		{
		   glColor3f(0.0,0.0,1.0);
		}
		MyMesh mesh = ArmSectionMeshes[i];
		MyMesh::FaceIter          f_it, f_end(mesh.faces_end());
		MyMesh::FaceVertexIter    fv_it;

		glBegin(GL_TRIANGLES);
	
		for(f_it=mesh.faces_begin(); f_it!=f_end; ++f_it)
		{
		  MyMesh::Normal norm = mesh.normal(*f_it);
		  glNormal3f(norm[0],norm[1],norm[2]);
		  for (fv_it=mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it)
		  {
			MyMesh::Point vert = mesh.point(*fv_it);
			glVertex3f(vert[0], vert[1], vert[2]);	
		  }
		}
		glEnd();
	}
	glPopMatrix();
	
}

void Environment::drawManipulator_Mesh_with_tip_cam()
{

	int nSections = mani->get_n_sections();
	vector<MyMesh> ArmSectionMeshes(nSections);
    float section_width = mani->get_sec_width();
	vector<float> Config = mani->get_config();
		
	vector<float> s(nSections);
	vector<float> k(nSections);
	vector<float> phi(nSections);
	vector<float> r(nSections);

	for(int i = 0; i < nSections; ++i)
	{
		s[i] = Config[i*3];
		k[i] = Config[i*3+1];
		r[i] = 1/fabs(k[i]);
		phi[i] = Config[i*3+2];
	}

	vector<vector<float> >RotaAboutZs(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		float RotaAboutZi[16] = {cos(phi[i]),sin(phi[i]),0,0, -sin(phi[i]),cos(phi[i]),0,0, 0,0,1,0, 0,0,0,1};
		vector<float> temp(RotaAboutZi,RotaAboutZi+16);
		RotaAboutZs[i] = temp;
	}

	vector<float>ForwardTransMat(nSections*16);
	if(mani->trans_matrix.size() == 0)
	{
		mani->compute_trans_matrix(ForwardTransMat);
	}
	else
	{
		ForwardTransMat = mani->trans_matrix;
	}
	
	vector<vector<float> >endPoints(nSections+1, vector<float>(3,0));
	
	for(int i = 1; i < nSections+1; ++i)
	{	
		float endPoints_tmp [3] = {ForwardTransMat[(i-1)*16+12],ForwardTransMat[(i-1)*16+13],ForwardTransMat[(i-1)*16+14]};
		endPoints[i] = vector<float>(endPoints_tmp,endPoints_tmp+3);
	}


	vector<vector<float> >rotateEPs(nSections);
	for(int i = 1; i < nSections; ++i)
	{
		float* rotateEPi = MatrixMulti(&ForwardTransMat[(i-1)*16], RotaAboutZs[i],4,4,4,4); //ALMEM
		rotateEPs[i] = vector<float>(rotateEPi,rotateEPi+16);
		delete [] rotateEPi;
	}

	vector<vector<float> >eP_xs(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		float eP_xs_tmp [3] = {ForwardTransMat[0+16*i],ForwardTransMat[1+16*i],ForwardTransMat[2+16*i]};
		eP_xs[i] = vector<float>(eP_xs_tmp,eP_xs_tmp+3);
	}

	vector<vector<float> >ReP_xs(nSections);
	vector<vector<float> >ReP_ys(nSections);
	vector<vector<float> >ReP_zs(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		if(i == 0)
		{
			float ReP_xs_tmp [3] = {cos(phi[0]),sin(phi[0]),0};
			float ReP_ys_tmp [3] = {-sin(phi[0]),cos(phi[0]),0};
			float ReP_zs_tmp [3] = {0,0,1};
			ReP_xs[i] = vector<float> (ReP_xs_tmp,ReP_xs_tmp+3);
			ReP_ys[i] = vector<float> (ReP_ys_tmp,ReP_ys_tmp+3);
			ReP_zs[i] = vector<float> (ReP_zs_tmp,ReP_zs_tmp+3);
		}
		else
		{		
			float ReP_xs_tmp [3] = {rotateEPs[i][0],rotateEPs[i][1],rotateEPs[i][2]};
			float ReP_ys_tmp [3] = {rotateEPs[i][4],rotateEPs[i][5],rotateEPs[i][6]};
			float ReP_zs_tmp [3] = {rotateEPs[i][8],rotateEPs[i][9],rotateEPs[i][10]};
			ReP_xs[i] = vector<float> (ReP_xs_tmp,ReP_xs_tmp+3);
			ReP_ys[i] = vector<float> (ReP_ys_tmp,ReP_ys_tmp+3);
			ReP_zs[i] = vector<float> (ReP_zs_tmp,ReP_zs_tmp+3);
		}
	}

	vector<vector<float> >Cents(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		float endpt1[3]= {endPoints[i][0], endPoints[i][1],endPoints[i][2]};
		float endpt2[3]= {endPoints[i+1][0], endPoints[i+1][1],endPoints[i+1][2]};
		float pt1_vec[3] = {ReP_xs[i][0],ReP_xs[i][1], ReP_xs[i][2]};
		float pt2_vec[3] = {eP_xs[i][0], eP_xs[i][1], eP_xs[i][2]};
		float *Centi = TwolineIntersection(endpt1,endpt2,pt1_vec,pt2_vec);//ALMEM
		
		if(!Centi)
		{
			float vec_enp1_enpt2[3] = {endpt2[0] - endpt1[0], endpt2[1] - endpt1[1], endpt2[2] - endpt1[2]};
			Normalize(vec_enp1_enpt2,3);
			float cross_product[3] = {0.0};
			CrossProduct(vec_enp1_enpt2,pt1_vec,cross_product);
			float cross_product_len = norm_vec_3(cross_product);
			if(fabs(cross_product_len) <= 0.0001)
			{
				float center_tmp [3] = {(endpt2[0] + endpt1[0])/2, (endpt2[1] + endpt1[1])/2, (endpt2[2] + endpt1[2])/2};
				Cents[i] = vector<float>(center_tmp,center_tmp+3);
			}
			else
			{
				Cents[i].clear();
			}
		}
		else
		{
			Cents[i] = vector<float>(Centi,Centi+3);
		}
		delete [] Centi;
		
		float base_tmp[3] = {0};
		float central_axis_dir_tmp[3] = {0};
		float bend_dir_tmp [3] = {0.0};
		float bend_plane_normal_tmp[3] = {0.0};
		float cylind_radius_tmp = section_width;
		float cur_cen_tmp [3] = {0.0};
		float cur_rad_tmp = r[i];
		float length_tmp = s[i];
		for(int j = 0; j < 3; ++j)
		{
			base_tmp[j] = endpt1[j];
			central_axis_dir_tmp[j] = ReP_zs[i][j];
		
			if(Cents[i].size() == 0)
			{
				cur_cen_tmp == NULL;
				cur_rad_tmp = 0.0;
				bend_dir_tmp[j] = ReP_xs[i][j];
				
			}
			else
			{
				cur_cen_tmp[j] = Cents[i][j];
				bend_dir_tmp[j] = ( Cents[i][j] - base_tmp[j]);
			}
			
		}

		Normalize(central_axis_dir_tmp,3);
		Normalize(bend_dir_tmp,3);
		
		CrossProduct(central_axis_dir_tmp,bend_dir_tmp,bend_plane_normal_tmp);
		if(Cents[i].size() == 0)
		{
			CreatCylinderTriMeshes(ArmSectionMeshes[i], base_tmp,central_axis_dir_tmp,bend_dir_tmp, bend_plane_normal_tmp, cylind_radius_tmp, NULL, cur_rad_tmp,length_tmp);
		}
		else
		{
		   CreatCylinderTriMeshes(ArmSectionMeshes[i], base_tmp,central_axis_dir_tmp,bend_dir_tmp, bend_plane_normal_tmp, cylind_radius_tmp,cur_cen_tmp, cur_rad_tmp,length_tmp);
		}
	   
	}

 //Begin to draw Meshes...........
	float base_frame_f[16] = {0};
	for(int i =0; i < 16; ++i)
	{
		base_frame_f[i] = mani->base_trans_matrix[i];
	}
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix(); // save the model view matrix before setting the base frame
	glMultMatrixf(base_frame_f);
	
	glEnable(GL_LIGHTING);
	glEnable(GL_TEXTURE_2D); 
	
	glBindTexture(GL_TEXTURE_2D, texture[0]);       
	
	for(int i = 0; i < nSections; ++i)
	{
		MyMesh mesh = ArmSectionMeshes[i];
		MyMesh::FaceIter          f_it, f_end(mesh.faces_end());
		MyMesh::FaceVertexIter    fv_it;

		glBegin(GL_TRIANGLES);
	    int face_idx = 0;
		for(f_it=mesh.faces_begin(); f_it!=f_end; ++f_it)
		{
		  MyMesh::Normal norm = mesh.normal(*f_it);
		  glNormal3f(norm[0],norm[1],norm[2]);
		  int idx_vert_on_face = 0;
		  for (fv_it=mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it)
		  {
			MyMesh::Point vert = mesh.point(*fv_it);
			if(idx_vert_on_face == 0&&face_idx%4 == 0)
			{
				glTexCoord2f(0.0f, 0.0f);
			}
			else if(idx_vert_on_face ==2&&face_idx%3 == 0)
			{
				glTexCoord2f(0.0f, 1.0f);
			}
			else if(idx_vert_on_face ==1&&face_idx%4 == 0)
			{
				glTexCoord2f(1.f, 0.0f);
			}
			else if(idx_vert_on_face ==2&&face_idx%3 == 0)
			{
				glTexCoord2f(1.0f, 1.0f);
			}
			glVertex3f(vert[0], vert[1], vert[2]);	
			idx_vert_on_face++;
		  }
		  face_idx++;
		}
		glEnd();
	}
	glPopMatrix();
	glDisable(GL_LIGHTING);
	glDisable(GL_TEXTURE_2D); 
	
	vector<float> arm_tip_frame (16);
	mani->compute_section_frame_wrt_world(arm_tip_frame, nSections-1);
	
	float camera_frame_f [16] = {0.0};
	for(int i  = 0 ; i < 16; ++i)
	{
		camera_frame_f[i] = arm_tip_frame[i];
	}


	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glMultMatrixf(camera_frame_f);
	glTranslatef(0.0,0.0,0.1);
	glColor3f(0.4,0.4,0.4);
	glutSolidCube(0.15);
	glTranslatef(0.0,0.0,0.1);
	glColor3f(0.2,0.2,0.2);
	glutSolidSphere(0.08,30,30);
		glPushMatrix();
		glColor3f(1.0,0.0,0.0);
		glRotatef(90.0,0.0,1.0,0.0);
	//	gluCylinder(quadratic_x,0.02,0.02,0.2,50,50);
		glPopMatrix();

		glPushMatrix();
		glColor3f(0.0,1.0,0.0);
		glRotatef(-90.0,1.0,0.0,0.0);
	//	gluCylinder(quadratic_y,0.02,0.02,0.2,50,50);
		glPopMatrix();

		glColor3f(0.0,0.0,1.0);
	//	gluCylinder(quadratic_z,0.02,0.02,0.2,50,50);
	glPopMatrix();
		
	/* ************************
	 *  Set Window Corner Tip View
	 * ************************/
	int curent_viewport[4] = {0};
	glGetIntegerv(GL_VIEWPORT,curent_viewport);

	float tip[3] = {arm_tip_frame[12],arm_tip_frame[13],arm_tip_frame[14]};
	float tip_z_axis [3] = {arm_tip_frame[8],arm_tip_frame[9],arm_tip_frame[10]};
    float targetLookedpt [3] = {tip[0] + 1.5*tip_z_axis[0], tip[1] + 1.5*tip_z_axis[1], tip[2] + 1.5*tip_z_axis[2]};
 
    float tip_plane_normal [3] = {arm_tip_frame[4],arm_tip_frame[5],arm_tip_frame[6]};
    glViewport(0.1*curent_viewport[2]/4.0, 3.0*curent_viewport[3]/4.0, curent_viewport[2]/4.0, curent_viewport[3]/4.0);	
    
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	// Tempororaily disable tip view 
    //~ gluLookAt(tip[0], tip[1], tip[2], targetLookedpt[0], targetLookedpt[1], targetLookedpt[2], -tip_plane_normal[0], -tip_plane_normal[1], -tip_plane_normal[2]);  
		glMatrixMode(GL_PROJECTION);
		glPushMatrix();
		glLoadIdentity();
		gluPerspective(45.0, 1.0, 0.01, 1.0);	
		this->drawPointsCloud();
		glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    
	glViewport(curent_viewport[0],curent_viewport[1], curent_viewport[2], curent_viewport[3]);
}

void Environment::ScaleAndPositioningPointsClouds(const float scale, const vector<vector<float> > &point_cloud_positions)
{
	//  float scale =10.0;
	  
	for(int j = 0; j < point_clouds_.size(); ++j)
	{
	  float position_adjuster [3] = {point_cloud_positions[j][0], point_cloud_positions[j][1], point_cloud_positions[j][2]};
	  float temp_scale = scale;
	  float local_scale = 0.0;
	  if (j==1)
		local_scale = temp_scale;
	  else
		local_scale = temp_scale;
	  
	  float ScaleAndPositionMatrix [16] = {  local_scale, 0.0, 0.0, 0.0,  
											 0.0, local_scale, 0.0, 0.0,   
											 0.0, 0.0, local_scale, 0.0,
											 position_adjuster[0], position_adjuster[1], position_adjuster[2], 1.0}; 
	   for(size_t i = 0; i < point_clouds_[j]->points.size(); ++i)
	   {
		 float PointPosition [4] = {point_clouds_[j]->points[i].x, point_clouds_[j]->points[i].y,point_clouds_[j]->points[i].z, 1.0};
		 float* ModifiedPosition = MatrixMulti(ScaleAndPositionMatrix,PointPosition,4,4,4,1);
		 point_clouds_[j]->points[i].x = ModifiedPosition[0];
		 point_clouds_[j]->points[i].y = ModifiedPosition[1];
		 point_clouds_[j]->points[i].z = ModifiedPosition[2];	
		 delete [] ModifiedPosition;
      }
   }
}

void Environment::SetObjFrameAsReferenceFrameForPointsCloud()
{
	 float z_axis [3] = {0,-1.0,-1};
	 Normalize(z_axis,3);
	 float x_axis [3] = {1.0, 0, 0};
	 float y_axis [3] = {0.0,0.0,0.0};
	 CrossProduct(z_axis,x_axis,y_axis);
	 Normalize(y_axis,3);
	
	 float ObjectFrame_WRT_World [16] = {x_axis[0], x_axis[1], x_axis[2], 0.0,  
								         y_axis[0], y_axis[1], y_axis[2], 0.0,   
										 z_axis[0], z_axis[1], z_axis[2], 0.0,
										-0.3, 0.32, 0.52, 1.0}; 
										
	float* inverse_ObjectFrame_WRT_World = InverMatrix(ObjectFrame_WRT_World);
	for(int j = 0; j < point_clouds_.size(); ++j)
	{
		for(size_t i = 0; i < point_clouds_[j]->points.size(); ++i)
		{	
			 float PointPosition [4] = {point_clouds_[j]->points[i].x, point_clouds_[j]->points[i].y,point_clouds_[j]->points[i].z, 1.0};
			 float* ModifiedPosition = MatrixMulti(inverse_ObjectFrame_WRT_World,PointPosition,4,4,4,1);		
			 point_clouds_[j]->points[i].x = ModifiedPosition[0];
			 point_clouds_[j]->points[i].y = ModifiedPosition[1];
			 point_clouds_[j]->points[i].z = ModifiedPosition[2];	
			 delete [] ModifiedPosition;								 
		}		
    }	
    delete []inverse_ObjectFrame_WRT_World;		
}
	
void Environment::drawPointsCloud()
{
//	std::cout << "Loaded width:"<< cloud->width <<"and height:"<< cloud->height<< std::endl;
    glPointSize(2.8);
	glEnable(GL_POINT_SMOOTH);
	 for(int idx_pt_clouds = 0; idx_pt_clouds < point_clouds_.size(); ++idx_pt_clouds)
	 {
		
		  glBegin(GL_POINTS);
		  for(size_t i = 0; i < point_clouds_[idx_pt_clouds]->points.size(); ++i)
		  {
			  float red = point_clouds_[idx_pt_clouds]->points[i].r/(255.0);
			  float green = point_clouds_[idx_pt_clouds]->points[i].g/(255.0);
			  float blue = point_clouds_[idx_pt_clouds]->points[i].b/(255.0);
				  
			  glColor3f(red, green, blue);
			  //~ cout<<cloud->points[i].x<<" "<<cloud->points[i].y<<" "<<cloud->points[i].z<<endl;
			  //~ if(i == 20)
			  //~ {getchar();}
			  glVertex3f(point_clouds_[idx_pt_clouds]->points[i].x, point_clouds_[idx_pt_clouds]->points[i].y,point_clouds_[idx_pt_clouds]->points[i].z);		 
		  }
		  glEnd();
	}  
	glPointSize(1);
	glDisable(GL_POINT_SMOOTH);
}

void Environment::drawPointsCloud_DepthColor(float _eye_pos[3], float _look_at_point[3])
{
	
     float vec_eye_to_lookat_point [3] = {_look_at_point[0] - _eye_pos[0], _look_at_point[1] - _eye_pos[1],_look_at_point[2] - _eye_pos[2]};
     Normalize(vec_eye_to_lookat_point,3);
     glPointSize(2.8);
     glEnable(GL_POINT_SMOOTH);
	 for(int idx_pt_clouds = 0; idx_pt_clouds < point_clouds_.size(); ++idx_pt_clouds)
	 {	 
		  glBegin(GL_POINTS);
		  for(size_t i = 0; i < point_clouds_[idx_pt_clouds]->points.size(); ++i)
		  {
			  float position [3] = {point_clouds_[idx_pt_clouds]->points[i].x, point_clouds_[idx_pt_clouds]->points[i].y,point_clouds_[idx_pt_clouds]->points[i].z};
			  float vec_to_eye [3] = {position[0] - _eye_pos[0], position[1] - _eye_pos[1], position[2] - _eye_pos[2]};
			  float depth = DotProduct(vec_to_eye,vec_eye_to_lookat_point);
			  
			   float red = ((1.0-fabsf(depth))/0.5);
			   float green  = 0.0;
			   float blue = 0.0;
			   glColor3f(red, green, blue);
			
			  glVertex3f(position[0],position[1],position[2]);		 
		  }
		  glEnd();
	
	}
	 glDisable(GL_POINT_SMOOTH);
		  glPointSize(1.0);  
}

void Environment::drawPointsCloud(vector<int> indices_pt_clouds)
{

	  glBegin(GL_POINTS);
	  for(size_t j = 0;  j < indices_pt_clouds.size(); ++j)
	  {
		  for(size_t i = 0; i < point_clouds_[indices_pt_clouds[j]]->points.size(); ++i)
		  {
			  float red = point_clouds_[indices_pt_clouds[j]]->points[i].r/(255.0);
			  float green = point_clouds_[indices_pt_clouds[j]]->points[i].g/(255.0);
			  float blue = point_clouds_[indices_pt_clouds[j]]->points[i].b/(255.0);
				  
			  glColor3f(red, green, blue);
			  //~ cout<<cloud->points[i].x<<" "<<cloud->points[i].y<<" "<<cloud->points[i].z<<endl;
			  //~ if(i == 20)
			  //~ {getchar();}
			  glVertex3f(point_clouds_[indices_pt_clouds[j]]->points[i].x, point_clouds_[indices_pt_clouds[j]]->points[i].y,point_clouds_[indices_pt_clouds[j]]->points[i].z);		 
		  }
      }
	  glEnd();
	
	  
}

void Environment::drawObjects()
{
	for(int i =0 ; i < objects.size(); ++i)
		objects[i].drawObject();
}

void Environment::drawObjects(float translation[3], float scale[3], float orientation [9])
{
	for(int i =0 ; i < objects.size(); ++i)
		objects[i].drawObject( translation,  scale,  orientation);
}

void Environment::drawObjects_poly(int max_idx_face)
{
	for(int i =0 ; i < objects.size(); ++i)
		objects[i].drawObject_poly(max_idx_face);
}

void Environment::drawAnPolygon(vector<vector<float> >&polygon)
{
	  glBegin(GL_LINE_STRIP);
      for (int i = 0;i < polygon.size(); ++i)
      {     
		   glColor3f(1.0, 0.0, 0.0);
		   glVertex3f(polygon[i][0], polygon[i][1], polygon[i][2]);
      }
      glEnd();
}

void Environment::drawAnPolygon_visib(vector<vector<float> >&polygon, vector<bool>& visible)
{
	  glBegin(GL_LINE_STRIP);
      for (int i = 0;i < polygon.size(); ++i)
      {     
		   glColor3f(1.0, 0.0, 0.0);
		   if(visible[i] == true)
		   {
			 glVertex3f(polygon[i][0], polygon[i][1], polygon[i][2]);
		   }
      }
      glEnd();
}

bool Environment::CreatCylinderTriMeshes(MyMesh& OutputMesh, float base [3],float central_axis_dir[3], float bend_dir[3], float bend_plane_normal[3], float cylind_radius, float cur_cen [3], float cur_rad,float length) // generalized cylinder, a curved one as well
{
	// if cur_cen == NULL then the cylinder is a straight one, otherwise is a torus like cylinder.
	vector<MyMesh::VertexHandle> vhandles;
	float slice_plane_u [3] = {0.0};
	float slice_plane_v [3] = {0.0};
	float cen_pt [3] = {0.0};
	if(cur_cen == NULL)
	{
		
		for(int i = 0; i < 3; ++i)
		{
			slice_plane_u[i] = bend_dir[i];
			slice_plane_v[i] = bend_plane_normal[i];
		}
		Normalize(slice_plane_v,3);
		Normalize(slice_plane_u,3);
		Normalize(central_axis_dir,3);
		
		float dist_along_central_axis = 0;
		float delta_length_stack_plane= 0.5;
		float delta_length_slice_plane = 0.10;
		float delta_theta_on_slice = delta_length_slice_plane/cylind_radius;
		
		int stack_idx = 0;
		int n_vertices_per_stack = 0;
		
		for(; dist_along_central_axis < length; ++stack_idx)
		{
			dist_along_central_axis = stack_idx*delta_length_stack_plane;
			if(stack_idx == 1)
			{
				n_vertices_per_stack = vhandles.size();		
			}
		//	vhandles.clear();
			for(int i = 0; i < 3; i++)
				cen_pt[i] = base[i] + dist_along_central_axis*central_axis_dir[i];
				
	//		vhandles.push_back(OutputMesh.add_vertex(MyMesh::Point(cen_pt[0], cen_pt[1], cen_pt[2])));
			float first_pt [3] ={cen_pt[0] + cylind_radius* slice_plane_u[0], cen_pt[1] + cylind_radius* slice_plane_u[1], cen_pt[2] + cylind_radius* slice_plane_u[2]};
			vhandles.push_back(OutputMesh.add_vertex(MyMesh::Point(first_pt[0], first_pt[1], first_pt[2])));
			
			int slice_idx = 1;	
			float theta_on_slice = slice_idx*delta_theta_on_slice;		
			for(; theta_on_slice< 2.0*Pi_jinglin; ++slice_idx )
			{
				theta_on_slice = slice_idx*delta_theta_on_slice;
				float pt [3] = {0.0};
				for(int i = 0; i < 3; ++i)
				{
					pt[i] = cen_pt[i] + cylind_radius* (cos(theta_on_slice)*slice_plane_u[i] + sin(theta_on_slice)*slice_plane_v[i]);
				}
				vhandles.push_back(OutputMesh.add_vertex(MyMesh::Point(pt[0], pt[1], pt[2])));
				
				if(stack_idx >= 1)
				{
					int idx1 = (stack_idx-1)*n_vertices_per_stack + slice_idx-1;
					int idx2 = (stack_idx)*n_vertices_per_stack + slice_idx;
					int idx3 = (stack_idx)*n_vertices_per_stack + slice_idx-1;
					int idx4 = (stack_idx-1)*n_vertices_per_stack + slice_idx;
					OutputMesh.add_face(vhandles[idx1],vhandles[idx2],vhandles[idx3]);	
					OutputMesh.add_face(vhandles[idx1],vhandles[idx4],vhandles[idx2]);	
				}					
			}	
		}	
		OutputMesh.request_face_normals();	
	}
	else
	{
		float stack_plane_u [3] = {0.0};
		float stack_plane_v [3] = {0.0};
		float central_axis_pt [3] = {0.0};
		for(int i = 0; i < 3; ++i)
		{
			stack_plane_u[i] = -bend_dir[i];
			stack_plane_v[i] = central_axis_dir[i];
		}
		Normalize(stack_plane_u,3);
		Normalize(stack_plane_v,3);
		
		float theta_stack_plane = length/cur_rad;

		float delta_length_stack_plane = 0.05;
		float delta_theta_stack_plane = delta_length_stack_plane/cur_rad;
		float delta_length_slice_plane = 0.05;
		float delta_theta_on_slice = delta_length_slice_plane/cylind_radius;
		
		int stack_idx = 0;
		float current_theta_stack_plane = 0;
		int n_vertices_per_stack = 0;
		for(; current_theta_stack_plane < theta_stack_plane; ++stack_idx)
		{
			
			current_theta_stack_plane = stack_idx*delta_theta_stack_plane;

			if(stack_idx == 1)
			{
				n_vertices_per_stack = vhandles.size();
			}
			

			for(int i = 0 ;i < 3; ++i)
			{
				central_axis_pt[i] = cur_cen[i] + cur_rad*(cos(current_theta_stack_plane)*stack_plane_u[i] + sin(current_theta_stack_plane)*stack_plane_v[i]);
				
			}
			

			float slice_plane_u [3] = {cur_cen[0] - central_axis_pt[0], cur_cen[1] - central_axis_pt[1], cur_cen[2] - central_axis_pt[2]};
			Normalize(slice_plane_u,3);
			float slice_plane_v [3] = {0.0};
			for(int i = 0; i < 3; ++i)
			{
				slice_plane_v[i] = bend_plane_normal[i];
			}
			Normalize(slice_plane_v,3);
			
			
			float first_pt [3] ={central_axis_pt[0] + cylind_radius* slice_plane_u[0], central_axis_pt[1] + cylind_radius* slice_plane_u[1], central_axis_pt[2] + cylind_radius* slice_plane_u[2]};
			vhandles.push_back(OutputMesh.add_vertex(MyMesh::Point(first_pt[0], first_pt[1], first_pt[2])));
			
			int slice_idx = 1;	
			float theta_on_slice = slice_idx*delta_theta_on_slice;
			
			for(; theta_on_slice < 2.0*Pi_jinglin; ++slice_idx )
			{
				theta_on_slice = slice_idx*delta_theta_on_slice;
				float pt [3] = {0.0};
				for(int i = 0; i < 3; ++i)
				{
					pt[i] = central_axis_pt[i] + cylind_radius*(cos(theta_on_slice)*slice_plane_u[i] + sin(theta_on_slice)*slice_plane_v[i]);
				}
				vhandles.push_back(OutputMesh.add_vertex(MyMesh::Point(pt[0], pt[1], pt[2])));	
					
				if(stack_idx >= 1)
				{
					int idx1 = (stack_idx-1)*n_vertices_per_stack + slice_idx-1;
					int idx2 = (stack_idx)*n_vertices_per_stack + slice_idx;
					int idx3 = (stack_idx)*n_vertices_per_stack + slice_idx-1;
					int idx4 = (stack_idx-1)*n_vertices_per_stack + slice_idx;
					OutputMesh.add_face(vhandles[idx1],vhandles[idx2],vhandles[idx3]);	
					OutputMesh.add_face(vhandles[idx1],vhandles[idx4],vhandles[idx2]);	
				}
			}	
						
		}
		OutputMesh.request_face_normals();		
	}
	return true;
}

void Environment::drawMeshObjectsWithTipEye()
{
	vector<float> endpts = mani->get_section_end_points_wrt_world();
	int n_sections = mani->get_n_sections();
	float tip[3] = {endpts[n_sections*3], endpts[n_sections*3+1], endpts[n_sections*3+2]};
	
	float tipframe[16] = {0.0};
    mani->compute_section_frame_wrt_world(tipframe,2);
    float tip_z_axis [3] = {tipframe[8],tipframe[9],tipframe[10]};
    float targetLookedpt [3] = {tip[0] + 0.5*tip_z_axis[0], tip[1] + 0.5*tip_z_axis[1], tip[2] + 0.5*tip_z_axis[2]};
 
    float tip_plane_normal [3] = {tipframe[4],tipframe[5],tipframe[6]};
    
    glBegin(GL_LINES);
	glVertex3f(tip[0], tip[1], tip[2]);
	glVertex3f(targetLookedpt[0], targetLookedpt[1], targetLookedpt[2]);
	glEnd();
	
	//glBegin(GL_LINES);
	//glVertex3f(tip[0], tip[1], tip[2]);
	//glVertex3f(tip[0]+2.5*tip_plane_normal[0], targetLookedpt[1] + 2.5*tip_plane_normal[1], targetLookedpt[2]+2.5*tip_plane_normal[2]);
	//glEnd();
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
    gluLookAt(tip[0], tip[1], tip[2], targetLookedpt[0], targetLookedpt[1], targetLookedpt[2], tip_plane_normal[0], tip_plane_normal[1], tip_plane_normal[2]);
    
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
	glLoadIdentity();
	gluPerspective(45.0, 1.0, 0.01, 1.5);
    float currentFrustum[6][4] = {0.0};
    ExtractFrustum(currentFrustum);  
    glPopMatrix();
    
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
   
    
	for(int i =0 ; i < objects.size(); ++i)
		objects[i].DrawInFrustumObject(currentFrustum);
}

void Environment::drawPointCloudObjectsWithTipEye(vector<int> indices_pt_clouds, vector<bool> &_points_seen)
{
	vector<float> endpts = mani->get_section_end_points_wrt_world();
	int n_sections = mani->get_n_sections();
	float tip[3] = {endpts[n_sections*3], endpts[n_sections*3+1], endpts[n_sections*3+2]};
	
	float tipframe[16] = {0.0};
    mani->compute_section_frame_wrt_world(tipframe,2);
    float tip_z_axis [3] = {tipframe[8],tipframe[9],tipframe[10]};
    float targetLookedpt [3] = {tip[0] + 0.5*tip_z_axis[0], tip[1] + 0.5*tip_z_axis[1], tip[2] + 0.5*tip_z_axis[2]};
    float tip_plane_normal [3] = {tipframe[4],tipframe[5],tipframe[6]};
  
    glBegin(GL_LINES);
	glVertex3f(tip[0], tip[1], tip[2]);
	glVertex3f(targetLookedpt[0], targetLookedpt[1], targetLookedpt[2]);
	glEnd();
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
    gluLookAt(tip[0], tip[1], tip[2], targetLookedpt[0], targetLookedpt[1], targetLookedpt[2], tip_plane_normal[0], tip_plane_normal[1], tip_plane_normal[2]);
    
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
	glLoadIdentity();
	gluPerspective(70.0, 1.4, 0.01, 0.7); // Specify the frustum
    float currentFrustum[6][4] = {0.0};
    ExtractFrustum(currentFrustum);  
    glPopMatrix();
    
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    
    int start_idx_of_current_point = 0;
    //~ // clear newly_sensed_pnt_clouds
    //~ if (newly_sensed_pnt_clouds->points.size() > min_cluster_pnt_size)
		//~ newly_sensed_pnt_clouds->points.clear();
    
	for(int j = 0; j< indices_pt_clouds.size(); ++j)
    {
		  glBegin(GL_POINTS);
		  for(size_t i = 0; i < point_clouds_[indices_pt_clouds[j]]->points.size(); ++i)
		  {			
			float point_x_y_z [3] = {point_clouds_[indices_pt_clouds[j]]->points[i].x, 
									 point_clouds_[indices_pt_clouds[j]]->points[i].y,
									 point_clouds_[indices_pt_clouds[j]]->points[i].z};
			start_idx_of_current_point = 0;
			   
			for(int k = 0; k <j;++k)
			{
				start_idx_of_current_point += (point_clouds_[indices_pt_clouds[k]]->points.size());
			}
			 
		    if(_points_seen[start_idx_of_current_point+i] != true && PointInFrustum(currentFrustum,point_x_y_z[0], point_x_y_z[1], point_x_y_z[2]))
		    {
				_points_seen[start_idx_of_current_point+i] = true;
				newly_sensed_pnt_clouds->points.push_back
										(point_clouds_[indices_pt_clouds[j]]->points[i]);
		    } 
		}
		glEnd();
	}
	*sensed_pnt_clouds += *newly_sensed_pnt_clouds;
}

void Environment::drawMeshObjectsWithTipEye_SubViewPort()
{
	vector<float> endpts = mani->get_section_end_points_wrt_world();
	int n_sections = mani->get_n_sections();
	float tip[3] = {endpts[n_sections*3], endpts[n_sections*3+1], endpts[n_sections*3+2]};
	
	float tipframe[16] = {0.0};
    mani->compute_section_frame_wrt_world(tipframe,2);
    float tip_z_axis [3] = {tipframe[8],tipframe[9],tipframe[10]};
    float targetLookedpt [3] = {tip[0] + 0.5*tip_z_axis[0], tip[1] + 0.5*tip_z_axis[1], tip[2] + 0.5*tip_z_axis[2]};
 
    float tip_plane_normal [3] = {tipframe[4],tipframe[5],tipframe[6]};
    
    glBegin(GL_LINES);
	glVertex3f(tip[0], tip[1], tip[2]);
	glVertex3f(targetLookedpt[0], targetLookedpt[1], targetLookedpt[2]);
	glEnd();
	
	//glBegin(GL_LINES);
	//glVertex3f(tip[0], tip[1], tip[2]);
	//glVertex3f(tip[0]+2.5*tip_plane_normal[0], targetLookedpt[1] + 2.5*tip_plane_normal[1], targetLookedpt[2]+2.5*tip_plane_normal[2]);
	//glEnd();
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
    gluLookAt(tip[0], tip[1], tip[2], targetLookedpt[0], targetLookedpt[1], targetLookedpt[2], tip_plane_normal[0], tip_plane_normal[1], tip_plane_normal[2]);
    
    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
	glLoadIdentity();
	
	gluPerspective(45.0, 1.0, 0.01, 1.5);
	this->drawObjects();
	
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    
}
void Environment::get_objects_in_env(vector<Object> &_objects)
{
	_objects = objects;
}

//Compute the section plane section (convex hull) polygon based obj_idx and sec_idx
void Environment::SecPlaneIntersecPoly(int sec_idx, int objec_idx, vector<vector<float> >& convex_hull_poly)
{
	ManipulatorNSections* Cur_mani = this->mani;
	vector<float> config = Cur_mani->get_config();
	vector<float> cur_sec_base_frame (16);
	Cur_mani->Getbaseframe(cur_sec_base_frame);
	
	if(sec_idx > 0)
	{
        Cur_mani->compute_section_frame_wrt_world(cur_sec_base_frame, sec_idx-1);
	}
	
	vector<float> endpts = Cur_mani->get_section_end_points_wrt_world();
	float sec_tip[3] = {endpts[(sec_idx+1)*3], endpts[(sec_idx+1)*3+1], endpts[(sec_idx+1)*3+2]};
    float sec_normal[3] = {Cur_mani->section_plane_normals_wrt_world[sec_idx*3],Cur_mani->section_plane_normals_wrt_world[sec_idx*3+1],Cur_mani->section_plane_normals_wrt_world[sec_idx*3+2]};
    Normalize(sec_normal,3);
    
    Object obj = objects[objec_idx];
    
	vector<float> plane_normal(sec_normal,sec_normal+3);
	vector<float> a_point_on_plane(sec_tip,sec_tip+3);
	vector<vector<float> >	intersectedPoly;
	for (MyMesh_poly::EdgeIter e_it = obj.mesh_poly.edges_begin(); e_it != obj.mesh_poly.edges_end();e_it++)
	{
		
		MyMesh_poly::HalfedgeHandle h_dl = obj.mesh_poly.halfedge_handle(*e_it, 0);
		MyMesh_poly::VertexHandle v_h_1 = obj.mesh_poly.from_vertex_handle(h_dl);
		MyMesh_poly::VertexHandle v_h_2 = obj.mesh_poly.to_vertex_handle(h_dl);
		
		MyMesh_poly::Point p1 = obj.mesh_poly.point(v_h_1);
		MyMesh_poly::Point p2 = obj.mesh_poly.point(v_h_2);
		
	
		
		float p1f[3] = {p1[0],p1[1],p1[2]};
		float p2f[3] = {p2[0],p2[1],p2[2]};
		
		
		vector<float> point1(p1f, p1f+3);
		vector<float> point2(p2f, p2f+3);
		
		vector<float> intsec_point = plane_edge_intersection(plane_normal,a_point_on_plane,point1,point2);

		if(intsec_point.size() != 0)
		{
			intersectedPoly.push_back(intsec_point);	
		}
	}	
	
	convex_hull_poly = Compute_ConvexHull_3d_to_2D(intersectedPoly,plane_normal, a_point_on_plane);
	
}


void Environment::labelVisiVexsOnPolyByTip(vector<vector<float> > &ConvexPolygon, vector<bool> &visibleVector, int objec_idx)
{
	ManipulatorNSections* Cur_mani = this->mani;
	int n_sections = Cur_mani->get_n_sections();
	vector<float> endpts = Cur_mani->get_section_end_points_wrt_world();
	float tip_pos_f[3] = {endpts[n_sections*3],endpts[n_sections*3+1],endpts[n_sections*3+2]};
	vector<float> tip_pos(tip_pos_f,tip_pos_f+3);
	Object obj = objects[objec_idx];
	int test = 0;
	for(int i = 0 ; i < ConvexPolygon.size(); ++i)
	{
		vector<float> cur_vex_on_poly = ConvexPolygon[i];
		bool cur_vex_visible = true;
		for (MyMesh_poly::FaceIter f_it = obj.mesh_poly.faces_begin(); f_it!=obj.mesh_poly.faces_end(); ++f_it) 
		{
			MyMesh_poly::Normal cur_face_normal= obj.mesh_poly.calc_face_normal(*f_it);
			MyMesh_poly::Point  cur_face_centroid= obj.mesh_poly.calc_face_centroid(*f_it);
			
			vector<float> cur_face_normal_v = vector<float>(&cur_face_normal[0],&cur_face_normal[0]+3);
			vector<float> cur_face_centroid_v = vector<float>(&cur_face_centroid[0],&cur_face_centroid[0]+3);

			vector<float> intsec_point = plane_edge_intersection(cur_face_normal_v,cur_face_centroid_v,tip_pos,cur_vex_on_poly);
				
			if(intsec_point.size() != 0)
			{ 
				
				bool inside_cur_face = true;
				for(MyMesh_poly::FaceEdgeIter fe_it = obj.mesh_poly.fe_iter(*f_it); fe_it.is_valid();++fe_it)
				{
					MyMesh_poly::Normal edge_vec = obj.mesh_poly.calc_edge_vector(*fe_it);
					float edge_vec_f [3] = {edge_vec[0],edge_vec[1],edge_vec[2]};
					float face_normal_f [3] = {cur_face_normal[0],cur_face_normal[1],cur_face_normal[2]};
					Normalize(edge_vec_f,3);
					Normalize(face_normal_f,3);
					
					float inside_face_vec [3] = {0};
					CrossProduct(edge_vec_f,face_normal_f,inside_face_vec);
			
					MyMesh_poly::HalfedgeHandle halfedgehdl = obj.mesh_poly.halfedge_handle(*fe_it, 0);
					MyMesh_poly::VertexHandle from_vex_hdl	= obj.mesh_poly.from_vertex_handle (halfedgehdl);
					MyMesh_poly::Point vex_point = obj.mesh_poly.point(from_vex_hdl);
					
					float sign_inside_face = inside_face_vec[0]*(intsec_point[0] - vex_point[0])
											+ inside_face_vec[1]*(intsec_point[1] - vex_point[1])
											+ inside_face_vec[2]*(intsec_point[2] - vex_point[2]);
											
					if(sign_inside_face <= 0)
					{
						inside_cur_face = false;
						break;
					}
				}
				if(inside_cur_face == true)
				{
					cur_vex_visible = false;
					break;
				}
			}
		}
		visibleVector[i] = cur_vex_visible;
	}
}

/*********************************************************
 ******************* Process Point Clouds *******************
 ************************************************************/
void Environment::printSensedPclInfo()
{
	//~ for (size_t cnt=0; cnt < sensed_pnt_clouds->points.size(); ++cnt )
	//~ {
		//~ std::cout << "point located at " << sensed_pnt_clouds->points[cnt].x 
					//~ << " " << sensed_pnt_clouds->points[cnt].y 
					//~ << " " << sensed_pnt_clouds->points[cnt].z << std::endl;
	//~ }
	std::cout << "number of points is " << sensed_pnt_clouds->points.size() << std::endl;
}

// This function only takes 0.5ms
void Environment::drawSensedPcl()
{
	glBegin(GL_POINTS);
		for (size_t cnt=0; cnt < sensed_pnt_clouds->points.size(); ++cnt )
		{
			float red = sensed_pnt_clouds->points[cnt].r/(255.0);
			float green = sensed_pnt_clouds->points[cnt].g/(255.0);
			float blue = sensed_pnt_clouds->points[cnt].b/(255.0);
				  
			glColor3f(red, green, blue);
			glVertex3f(sensed_pnt_clouds->points[cnt].x,
					   sensed_pnt_clouds->points[cnt].y,
					   sensed_pnt_clouds->points[cnt].z + 0.0  );
		}
	glEnd();
}

void Environment::clusterNewlySensedPcl()
{
	if (newly_sensed_pnt_clouds->points.size() >= min_cluster_pnt_size) {
		pcl::PCDWriter writer;
		pcl::search::KdTree<pcl::PointXYZRGBA>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBA>);
		tree->setInputCloud (newly_sensed_pnt_clouds);
		
		std::vector<pcl::PointIndices> cluster_indices;
		pcl::EuclideanClusterExtraction<pcl::PointXYZRGBA> ec;
		ec.setClusterTolerance (cluster_tolerance); 
		ec.setMinClusterSize (min_cluster_pnt_size);
		ec.setMaxClusterSize (15000);
		ec.setSearchMethod (tree);
		ec.setInputCloud (newly_sensed_pnt_clouds);
		ec.extract (cluster_indices);
		
		if (cluster_indices.size() == 0)
			printf("No cluster found\n");
		
		for (std::vector<pcl::PointIndices>::const_iterator it = cluster_indices.begin (); it != cluster_indices.end (); ++it)
		{
			pcl::PointCloud<pcl::PointXYZRGBA>::Ptr cloud_cluster (new pcl::PointCloud<pcl::PointXYZRGBA>);
			for (std::vector<int>::const_iterator pit = it->indices.begin (); pit != it->indices.end (); ++pit)
				cloud_cluster->points.push_back (newly_sensed_pnt_clouds->points[*pit]); 
			cloud_cluster->width = cloud_cluster->points.size ();
			cloud_cluster->height = 1;
			cloud_cluster->is_dense = true;
			
			std::cout << "PointCloud representing the Cluster: " << cloud_cluster->points.size () << " data points." << std::endl;
			
			// 1st cluster ?
			if (clustered_pnt_clouds_ind != 0) {
				if ( mergeNewCluster(cloud_cluster) ) {
					std::cout << "new cluster merged" << std::endl;
				}
				// make a new cluster if merge is not successful
				else {
					std::stringstream ss;
					ss << "cloud_cluster_" << clustered_pnt_clouds_ind << ".pcd";
					writer.write<pcl::PointXYZRGBA> (ss.str (), *cloud_cluster, false);
					std::cout << "new cluster with index " << clustered_pnt_clouds_ind
							  << " made" << std::endl;
					clustered_pnt_clouds_ind++;
				}
			}
			// make a new cluster if no existing
			else {
				// copy 1
				std::stringstream ss;
				ss << "cloud_cluster_" << clustered_pnt_clouds_ind << ".pcd";
				writer.write<pcl::PointXYZRGBA> (ss.str (), *cloud_cluster, false);
				std::cout << "new cluster with index " << clustered_pnt_clouds_ind
							  << " made" << std::endl;
							  
				// copy 2
				std::string new_tgt_pnts("newly_sensed_tgt_pnts.pcd");
				writer.write<pcl::PointXYZRGBA> (new_tgt_pnts, *cloud_cluster, false);
						
				clustered_pnt_clouds_ind++;
			}
			
			/***** remove clustered pnts from newly_sensed_pnt_clouds (keep unclustered pnts) ****/
			// create extract object
			pcl::ExtractIndices<pcl::PointXYZRGBA> extract;
			extract.setInputCloud (newly_sensed_pnt_clouds);
			// set cluster indices
			pcl::PointIndices::Ptr cur_cluster_indices_ptr(new pcl::PointIndices());
			cur_cluster_indices_ptr->indices = it->indices;
			extract.setIndices (cur_cluster_indices_ptr);
			extract.setNegative (true); // remove already clustered and keep rest
			extract.filter (*newly_sensed_pnt_clouds);
		}
	}
	else
		std::cout << "No newly sensed points or Not enough new points" << std::endl;
}

bool Environment::mergeNewCluster(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &new_cls)
{
	pcl::PCDReader reader;
	pcl::PCDWriter writer;
	// load existing clusters
	for (int down_cnt = (clustered_pnt_clouds_ind-1); down_cnt>=0; --down_cnt)
	{
		std::stringstream cls_name;
		cls_name << "cloud_cluster_" << down_cnt << ".pcd";
		
		pcl::PointCloud<pcl::PointXYZRGBA>::Ptr existing_cls(new pcl::PointCloud<pcl::PointXYZRGBA>);
		reader.read (cls_name.str(), *existing_cls);
		
		pcl::KdTreeFLANN<pcl::PointXYZRGBA> kdtree;
		kdtree.setInputCloud (existing_cls);

		pcl::PointXYZRGBA searchPoint;
		int K = 1;
		// make storage
		std::vector<int> pointIdxNKNSearch(K);
		std::vector<float> pointNKNSquaredDistance(K);
		float min_dist = 999.0;
		for (size_t ct=0; ct < new_cls->points.size(); ++ct)
		{
			searchPoint.x = new_cls->points[ct].x;
			searchPoint.y = new_cls->points[ct].y;
			searchPoint.z = new_cls->points[ct].z;
			if ( kdtree.nearestKSearch (searchPoint, K, 
										pointIdxNKNSearch, 
										pointNKNSquaredDistance) > 0 )
			{
				for (size_t i = 0; i < pointIdxNKNSearch.size (); ++i)
				{
					if ( sqrtf( pointNKNSquaredDistance[i] ) < min_dist )
						min_dist = sqrtf( pointNKNSquaredDistance[i] );
				}
				
				// return if one close-enough point is found
				if (min_dist < cluster_tolerance)
				{
					*existing_cls += *new_cls; 
					// rewrite existing cls  M_PI / 3.0
					writer.write<pcl::PointXYZRGBA> (cls_name.str (), *existing_cls, false);
					std::cout << "new cls merged with cloud_cluster_" << down_cnt << std::endl;
					// save newly sensed tgt obj points
					if (down_cnt == 0)
					{
						std::string new_tgt_pnts("newly_sensed_tgt_pnts.pcd");
						writer.write<pcl::PointXYZRGBA> (new_tgt_pnts, *new_cls, false);
					}
					return true;
				}
			}
		}
	}
	// after all existing cls are compared
	return false;
}

void Environment::findEdgePnts(float final_tgt_pnt[3], float fgp_pln_normal[3])
{
	pcl::PCDReader reader;
	std::stringstream tgt_pcl_name;
	tgt_pcl_name << "newly_sensed_tgt_pnts.pcd";
	
	pcl::PointCloud<pcl::PointXYZRGBA>::Ptr tgt_pcl(new pcl::PointCloud<pcl::PointXYZRGBA>);
	reader.read (tgt_pcl_name.str(), *tgt_pcl);	
	
	/********Find Z-max Z-min**********/
	std::vector<float> pnts_z;
	for (size_t i=0; i < tgt_pcl->points.size(); ++i)
	{
		pnts_z.push_back(tgt_pcl->points[i].z);
	}
	std::sort(pnts_z.begin(), pnts_z.end());
	float z_min = pnts_z.front()-0.001;
	float z_max = pnts_z.back()+0.001;
	//~ std::cout << "min is " << z_min << " max is " << z_max << std::endl;
	int level = 10;
	float resolution = (z_max-z_min)/level;
	std::vector<std::vector<int> > pnt_ind_by_level;
	pnt_ind_by_level.resize(level);
	
	/********Arrange point index by Z values**********/
	for (size_t ct=0; ct < tgt_pcl->points.size(); ++ct)
	{
		int cur_pnt_level = floor( (tgt_pcl->points[ct].z - z_min) / resolution );
		pnt_ind_by_level[cur_pnt_level].push_back(ct);
	}
	
	/********Find edge pnt in each level*********/
	// get current arm tip position
	int n_sections = mani->get_n_sections();
	vector<float> mani_endpoints = mani->get_section_end_points_wrt_world();
	float tipt_f [3] = { mani_endpoints[3*(n_sections)], 
						 mani_endpoints[3*(n_sections)+1],
						 mani_endpoints[3*(n_sections)+2]}; 
	std::vector<float> tip_pos(tipt_f, tipt_f+3);
	
	float pts_dist = 0.0;
	std::vector<float> temp_pnt(3,0.0);
	int edge_pnt_ind = 9999;
	std::vector<int> edge_pnt_ind_by_level; // sequence is 0 1 2 ... 9 (unsorted)
	
	for (size_t j=0; j<pnt_ind_by_level.size(); ++j)
	{
		//~ std::cout << "level " << j << " has pnts " << std::endl;
		for (size_t k=0; k<pnt_ind_by_level[j].size(); ++k)
		{
			temp_pnt[0] = tgt_pcl->points[pnt_ind_by_level[j][k]].x;
			temp_pnt[1] = tgt_pcl->points[pnt_ind_by_level[j][k]].y;
			temp_pnt[2] = tgt_pcl->points[pnt_ind_by_level[j][k]].z;
			//~ std::cout << "ind " << pnt_ind_by_level[j][k] << std::endl;
			float cur_pts_dist = two_pts_dist(tip_pos, temp_pnt);
			if (pts_dist < cur_pts_dist) {
				pts_dist = cur_pts_dist;
				edge_pnt_ind = pnt_ind_by_level[j][k];
			}
		}
		pts_dist = 0.0; //re-initialize pnt distance
		edge_pnt_ind_by_level.push_back(edge_pnt_ind);
		//~ std::cout << "level " << j << " edge pnt has index " << edge_pnt_ind << std::endl;
	}
	
	/********Sort edge pnts based on dist to tip*********/
	// find level the tip at
	int top_level = floor( (z_max - z_min) / resolution );
	int tip_level = floor( (tip_pos[2] - z_min) / resolution );
	
	// catch the case tip lower than newly sensed tgt pnts lowest height
	if (tip_level < 1)
		tip_level = 1;
	
	// catch the case tip upper than newly sensed tgt pnts highest height
	if (tip_level >= top_level)
		tip_level = top_level-2;
	#ifdef DEBUG
	//~ printf("tip level is %d\n", tip_level);
	//~ printf("top level is %d\n", top_level);
	#endif
	std::vector<int> sorted_edge_pnt_ind; //closest first
	sorted_edge_pnt_ind.push_back( edge_pnt_ind_by_level[tip_level] );
	int up_space = level-1-tip_level;
	int down_space = tip_level;
	int alternate_limit = 0;
	if (up_space < down_space)
		alternate_limit = up_space;
	else
		alternate_limit = down_space;
	
	for (int g=1; g<=alternate_limit; ++g)
	{
		sorted_edge_pnt_ind.push_back(edge_pnt_ind_by_level[tip_level - g]);
		sorted_edge_pnt_ind.push_back(edge_pnt_ind_by_level[tip_level + g]);
	}
	for (int h=1; h<=(std::abs(up_space-down_space)); ++h)
	{
		sorted_edge_pnt_ind.push_back(edge_pnt_ind_by_level[tip_level + alternate_limit + h]);
	}
	/**** Visualize sorted edge pnts
	//~ std::cout << "tip is at level " << tip_level << " sorted edge pnts are level" << std::endl;
	//~ glPointSize(5.0);
	//~ glBegin(GL_POINTS);
		//~ for (int u=0; u<sorted_edge_pnt_ind.size();++u) {
			//~ glColor3f(1.0-u*0.1, 0., 0.);
			//~ 
			//~ glVertex3f(tgt_pcl->points[sorted_edge_pnt_ind[u]].x,
					   //~ tgt_pcl->points[sorted_edge_pnt_ind[u]].y,
					   //~ tgt_pcl->points[sorted_edge_pnt_ind[u]].z + 3.0  );
		//~ }
	//~ glEnd();
	***/
	
	/********Visualize edge pnts*********/
	#ifdef VISUAL_EDGE_PNTS
	glLineWidth(3.0);
	glBegin(GL_LINE_STRIP);
	for (size_t q=0; q<edge_pnt_ind_by_level.size(); ++q)
	{  
		glColor3f(1.0, 0., 0.);
		glVertex3f(tgt_pcl->points[edge_pnt_ind_by_level[q]].x,
				   tgt_pcl->points[edge_pnt_ind_by_level[q]].y,
				   tgt_pcl->points[edge_pnt_ind_by_level[q]].z + 0.0  );
	}
	glEnd();
	#endif
	
	/*******Estimate newly sensed tgt object plane normal*********/
	#ifdef DEBUG
	printf("sorted edge ind vector size is %lu\n", sorted_edge_pnt_ind.size());
	printf("sorted edge ind is %d\n", sorted_edge_pnt_ind.at(0));
	#endif
	float tgt_pnt[3] = {tgt_pcl->points[sorted_edge_pnt_ind.at(0)].x, // sec 3 tgt pnt
						tgt_pcl->points[sorted_edge_pnt_ind.at(0)].y,
						tgt_pcl->points[sorted_edge_pnt_ind.at(0)].z};
	// estimate by plane fitting
	std::vector<float> pln_normal(3,0.0); 
	
	tgtPntPlaneFitting(tgt_pcl, pln_normal); 
	// enforce direction
	float pnt_inside_coffee [3] = {1.0,2.2,0.08};
	
	float vec_pnt_in_cof_to_tgt_pnt [3] = { tgt_pnt[0]-pnt_inside_coffee[0],
											tgt_pnt[1]-pnt_inside_coffee[1],
											tgt_pnt[2]-pnt_inside_coffee[2]};
	float pln_normal_array[3] = {pln_normal[0],pln_normal[1],pln_normal[2]};
	float normal_ort_check = DotProduct(vec_pnt_in_cof_to_tgt_pnt, pln_normal_array);
	if (normal_ort_check<0) { // flip direction
		pln_normal_array[0] = -pln_normal[0];
		pln_normal_array[1] = -pln_normal[1];
		pln_normal_array[2] = -pln_normal[2];
	}
	
	#ifdef DEBUG
	std::cout << "[plane fitting]normal is " << pln_normal_array[0] << " " << pln_normal_array[1] 
			 << " " << pln_normal_array[2] <<std::endl;
	#endif	
	// collect results
	float tip_moving_scale = 0.35; // 0.35
	final_tgt_pnt[0] = tgt_pnt[0] + tip_moving_scale*pln_normal_array[0];
	final_tgt_pnt[1] = tgt_pnt[1] + tip_moving_scale*pln_normal_array[1]; 
	final_tgt_pnt[2] = tgt_pnt[2] + tip_moving_scale*pln_normal_array[2];
	
	fgp_pln_normal[0] = pln_normal_array[0];
	fgp_pln_normal[1] = pln_normal_array[1];
	fgp_pln_normal[2] = pln_normal_array[2];
}

// generate a line motion
bool Environment::generateLineMotionGivenTgtPntNormalAndLineDirection(float final_tgt_pnt[3], 
																	  float pln_normal_array[3],
																	  float line_direction[3])
{
	std::vector<float> mani_endpoints = mani->get_section_end_points_wrt_world();
	int n_sections = mani->get_n_sections();
	float deviation = sqrt( pow(mani_endpoints[3*n_sections+0]-final_tgt_pnt[0],2) + 
							pow(mani_endpoints[3*n_sections+1]-final_tgt_pnt[1],2) + 
							pow(mani_endpoints[3*n_sections+2]-final_tgt_pnt[2],2) );
	float tip_pos[3] = {0.0};
	#ifdef DEBUG
	printf("Planning to (%f, %f, %f) with Plane Normal (%f, %f, %f)\n", final_tgt_pnt[0],
																	  final_tgt_pnt[1],
																	  final_tgt_pnt[2],
																	  pln_normal_array[0],
																	  pln_normal_array[1],
																	  pln_normal_array[2]);
	#endif 
	int cnt = 0;
	while (deviation > 1e-2) {
		#ifdef DEBUG
		printf("deviation is %f\n", deviation);
		#endif
		// update current tip position
		mani_endpoints = mani->get_section_end_points_wrt_world();
		tip_pos[0] = mani_endpoints[3*n_sections];
		tip_pos[1] = mani_endpoints[3*n_sections+1];
		tip_pos[2] = mani_endpoints[3*n_sections+2];
		// update deviation
		deviation = sqrt(pow(mani_endpoints[3*n_sections+0]-final_tgt_pnt[0],2) + 
						 pow(mani_endpoints[3*n_sections+1]-final_tgt_pnt[1],2) + 
						 pow(mani_endpoints[3*n_sections+2]-final_tgt_pnt[2],2) );
						 
		// Get arm tip moving vector
		float arm_tip_moving_vector[3]={final_tgt_pnt[0]-tip_pos[0],
										final_tgt_pnt[1]-tip_pos[1],
										final_tgt_pnt[2]-tip_pos[2]};
		#ifdef DEBUG
		printf("Arm tip moving vector is (%f, %f, %f)\n", arm_tip_moving_vector[0],
														arm_tip_moving_vector[1],
														arm_tip_moving_vector[2]);
		#endif
		Normalize(arm_tip_moving_vector,3);
		float arm_tip_orientation[3] = {2.0*arm_tip_moving_vector[0]-pln_normal_array[0], 
										2.0*arm_tip_moving_vector[1]-pln_normal_array[1], 
										2.0*arm_tip_moving_vector[2]-pln_normal_array[2]};
		Normalize(arm_tip_orientation,3);
		
		// project to x-z plane
		float vec_base[3] = {0.0};
		float pnt_on_plane[3] = {0.0};
		float plane_normal[3] = {0.0,0.0,1.0};
		float projected_arm_tip_orn[3];
		Proj_vec_to_Plane(arm_tip_orientation, vec_base,pnt_on_plane,plane_normal,projected_arm_tip_orn);
		Normalize(projected_arm_tip_orn,3);
		
		// if almost aligns with frame axis
		if ( std::abs(projected_arm_tip_orn[0]-1.0) < 1e-3 || std::abs(projected_arm_tip_orn[1]-1.0) < 1e-3 
			 || std::abs(projected_arm_tip_orn[0]+1.0) < 1e-3 || std::abs(projected_arm_tip_orn[1]+1.0) < 1e-3 )
		{
			float world_z_axis[3] = {0.0,0.0,1.0};
			float rotated_vec[3] = {0.0};
			RotAVec(world_z_axis, projected_arm_tip_orn, 0.1, rotated_vec);
			Normalize(rotated_vec,3);
			projected_arm_tip_orn[0] = rotated_vec[0];
			projected_arm_tip_orn[1] = rotated_vec[1];
			projected_arm_tip_orn[2] = rotated_vec[2];
		}
		
		float temp_tgt_pnt[3] = {tip_pos[0]+0.01*arm_tip_moving_vector[0],
								 tip_pos[1]+0.01*arm_tip_moving_vector[1],
								 tip_pos[2]+0.01*arm_tip_moving_vector[2]};
		#ifdef DEBUG
		printf("Planning to (%f, %f, %f) with Desired arm tip orient is (%f, %f, %f)\n", 
														 temp_tgt_pnt[0],
														 temp_tgt_pnt[1] ,
														 temp_tgt_pnt[2], 
														 projected_arm_tip_orn[0], 
														 projected_arm_tip_orn[1],
														 projected_arm_tip_orn[2]);
		printf("current arm config is\n");
		std::vector<float> cur_config;
		cur_config.resize(9);
		mani->get_config(cur_config);
		for(int i=0; i<9; ++i)
			std::cout << cur_config[i] << std::endl;
		
		printf("current arm base is\n");
		float cur_base_frame[16] = {0.0};
		mani->Getbaseframe(cur_base_frame);
		for(int i=0; i<16; ++i)
			std::cout << cur_base_frame[i] << std::endl;
		
		printf("\n");
		#endif
		
		// enforce arm tip direction along with line direction
		projected_arm_tip_orn[0] = line_direction[0];
		projected_arm_tip_orn[1] = line_direction[1];
		projected_arm_tip_orn[2] = line_direction[2];
		
		if (!wholeArmIkByTipPose(projected_arm_tip_orn, temp_tgt_pnt,0.7,0.4)) {
			std::cout << "[" << cnt << "] " << "Plan Next Move Failed !" << std::endl;
			return false;
		}
		else {
			std::vector<float> new_config(9,0.0);
			mani->get_config(new_config);
			g_configs_todraw.push_back(new_config);
			
			float cur_base_frame[16] = {0.0};
			mani->Getbaseframe(cur_base_frame);
			std::vector<float> new_base_frame(cur_base_frame, cur_base_frame+16);
			g_base_frames_todraw.push_back(new_base_frame);
		}
		cnt++;
	}
	return true;
	//~ float arc_cir_plane_normal[3] = {0.0,0.0,-1.0};
	//~ if (!wholeArmIkByTgtPntAndArc2CordDistAndNormal(0.2, tgt_pnt, arc_cir_plane_normal))
		//~ std::cout << "Plan Next Move Failed !" << std::endl;
}

bool Environment::wholeArmIkByTipPose(float arm_tip_orientation[3], float tgt_pnt[3],
									  float sec2_vec_scale, float sec1_vec_scale)
{
	int n_secs = mani->get_n_sections();
	vector<float> mani_eps = mani->get_section_end_points_wrt_world();
	float tip [3] = {mani_eps[3*(n_secs)], 
					 mani_eps[3*(n_secs)+1],
					 mani_eps[3*(n_secs)+2]}; 
	std::vector<float> tip_pos(tip, tip+3);
	
	float vec_armtip_to_tgtpnt[3] ={tgt_pnt[0]-tip_pos[0],
									tgt_pnt[1]-tip_pos[1],
									tgt_pnt[2]-tip_pos[2]};
	
	// get section 3 tip frame
	float sec3_frame[16] = {0.0};
	mani->compute_section_frame_wrt_world(sec3_frame, 2); // 2 is section 3
	// get section 2 tip frame
	float sec2_frame[16] = {0.0};
	mani->compute_section_frame_wrt_world(sec2_frame, 1); // 1 is section 2
	// get section 1 tip frame
	float sec1_frame[16] = {0.0};
	mani->compute_section_frame_wrt_world(sec1_frame, 0); // 0 is section 1
	
	/****** section 3 *******/
	sec2_frame[12] += sec2_vec_scale * vec_armtip_to_tgtpnt[0];
	sec2_frame[13] += sec2_vec_scale * vec_armtip_to_tgtpnt[1];
	sec2_frame[14] += sec2_vec_scale * vec_armtip_to_tgtpnt[2];
	float sec2_tgt_pnt[3] = {sec2_frame[12], sec2_frame[13], sec2_frame[14]};
	
	float cir3_plane_normal[3] = {0.0};
	float cir3_center[3] = {0.0};
	float cir3_radius = 0.0;
	bool isCir3Found = compute_circle_given_twopoints_one_tangentVec(tgt_pnt, sec2_tgt_pnt, 
																	 arm_tip_orientation, 
																	 cir3_plane_normal,
																	 cir3_center,
																	 cir3_radius);
	std::vector<float> output_sec3_config(3, 0.0);
	float sec2_tgt_pnt_tangent[3] = {0.0};
	if (isCir3Found) {
		float half_dist_uv = two_pts_dist(tgt_pnt, sec2_tgt_pnt, 3)/2.0;
		// 这里需要选择要用优弧还是劣弧
		output_sec3_config[0] = 2.0 * cir3_radius * asin(half_dist_uv / cir3_radius);
		output_sec3_config[1] = 1.0 / cir3_radius;
		output_sec3_config[2] = 0.0;
	
		float vec_cir3_center_to_sec2_tgt[3] = {sec2_tgt_pnt[0] - cir3_center[0],
												sec2_tgt_pnt[1] - cir3_center[1],
												sec2_tgt_pnt[2] - cir3_center[2]};
		CrossProduct(cir3_plane_normal, vec_cir3_center_to_sec2_tgt, sec2_tgt_pnt_tangent);
		Normalize(sec2_tgt_pnt_tangent,3);
	}
	else
		std::cout << "cir3 is not found!" << std::endl;
	
	// compute section 3 frames
	float sec3_tip_x_axis [3] = {cir3_center[0]-tgt_pnt[0],
								 cir3_center[1]-tgt_pnt[1],
								 cir3_center[2]-tgt_pnt[2]};
	Normalize(sec3_tip_x_axis, 3);
	float sec3_tip_z_axis [3] = {arm_tip_orientation[0], 
								 arm_tip_orientation[1],
								 arm_tip_orientation[2]};
	Normalize(sec3_tip_z_axis, 3);
	float sec3_y_axis [3] = {0.0};
	CrossProduct(sec3_tip_z_axis, sec3_tip_x_axis, sec3_y_axis);
	Normalize(sec3_y_axis, 3);
	float sec3_base_x_axis [3] ={cir3_center[0]-sec2_tgt_pnt[0],
								 cir3_center[1]-sec2_tgt_pnt[1],
								 cir3_center[2]-sec2_tgt_pnt[2]};
	Normalize(sec3_base_x_axis, 3);
	float sec3_base_z_axis [3] = {0.0};
	CrossProduct(sec3_base_x_axis, sec3_y_axis, sec3_base_z_axis);
	Normalize(sec3_base_z_axis, 3);
	
	/****** section 2 *******/
	sec1_frame[12] += sec1_vec_scale * vec_armtip_to_tgtpnt[0];
	sec1_frame[13] += sec1_vec_scale * vec_armtip_to_tgtpnt[1];
	sec1_frame[14] += sec1_vec_scale * vec_armtip_to_tgtpnt[2];
	float sec1_tgt_pnt[3] = {sec1_frame[12], sec1_frame[13], sec1_frame[14]};
	
	float cir2_plane_normal[3] = {0.0};
	float cir2_center[3] = {0.0};
	float cir2_radius = 0.0;
	bool isCir2Found = compute_circle_given_twopoints_one_tangentVec(sec2_tgt_pnt, sec1_tgt_pnt, 
																	 sec2_tgt_pnt_tangent, 
																	 cir2_plane_normal,
																	 cir2_center,
																	 cir2_radius);
				
	std::vector<float> output_sec2_config(3, 0.0);
	float sec1_tgt_pnt_tangent[3] = {0.0};
	if (isCir2Found) {
		float half_dist_uv_sec2 = two_pts_dist(sec2_tgt_pnt, sec1_tgt_pnt, 3)/2.0;
		output_sec2_config[0] = 2.0 * cir2_radius * asin(half_dist_uv_sec2 / cir2_radius);
		output_sec2_config[1] = 1.0 / cir2_radius;
		output_sec2_config[2] = 0.0;
		
		float vec_cir2_center_to_sec1_tgt[3] = {sec1_tgt_pnt[0] - cir2_center[0],
												sec1_tgt_pnt[1] - cir2_center[1],
												sec1_tgt_pnt[2] - cir2_center[2]};
		CrossProduct(cir2_plane_normal, vec_cir2_center_to_sec1_tgt, sec1_tgt_pnt_tangent);
		Normalize(sec1_tgt_pnt_tangent, 3);
	}
	else
		std::cout << "cir2 is not found!" << std::endl;
	
	// compute section 2 frames
	float sec2_tip_z_axis [3] = {sec3_base_z_axis[0],
								 sec3_base_z_axis[1],
								 sec3_base_z_axis[2]};
	float sec2_tip_x_axis [3] = {cir2_center[0]-sec2_tgt_pnt[0],
								 cir2_center[1]-sec2_tgt_pnt[1],
								 cir2_center[2]-sec2_tgt_pnt[2]};
	Normalize(sec2_tip_x_axis,3);
	float sec2_y_axis[3] = {0.0};
	CrossProduct(sec2_tip_z_axis, sec2_tip_x_axis, sec2_y_axis);
	Normalize(sec2_y_axis,3);
	float sec2_base_x_axis [3] ={cir2_center[0]-sec1_tgt_pnt[0],
								 cir2_center[1]-sec1_tgt_pnt[1],
								 cir2_center[2]-sec1_tgt_pnt[2]};
	Normalize(sec2_base_x_axis,3);
	float sec2_base_z_axis[3] = {0.0};
	CrossProduct(sec2_base_x_axis, sec2_y_axis, sec2_base_z_axis);
	Normalize(sec2_base_z_axis,3);
	
	/****** section 1 *******/
	float origin[3] = {0.0};
	float cir1_plane_normal[3] = {0.0};
	float cir1_center[3] = {0.0};
	float cir1_radius = 0.0;
	bool isCir1Found = compute_circle_given_twopoints_one_tangentVec(sec1_tgt_pnt, origin, 
																	 sec1_tgt_pnt_tangent, 
																	 cir1_plane_normal,
																	 cir1_center,
																	 cir1_radius);
	
	std::vector<float> output_sec1_config(3, 0.0);
	if (isCir1Found) {
		float half_dist_uv_sec1 = two_pts_dist(sec1_tgt_pnt, origin, 3)/2.0;
		output_sec1_config[0] = 2.0 * cir1_radius * asin(half_dist_uv_sec1 / cir1_radius);
		output_sec1_config[1] = 1.0 / cir1_radius;
		output_sec1_config[2] = 0.0;
	}
	else 
		std::cout << "cir1 is not found!" << std::endl;
	
	// compute section 1 frames
	float sec1_tip_z_axis [3] = {sec2_base_z_axis[0],
								 sec2_base_z_axis[1],
								 sec2_base_z_axis[2]};
	float sec1_tip_x_axis [3] = {cir1_center[0]-sec1_tgt_pnt[0],
								 cir1_center[1]-sec1_tgt_pnt[1],
								 cir1_center[2]-sec1_tgt_pnt[2]};
	Normalize(sec1_tip_x_axis,3);
	float sec1_y_axis[3] = {0.0};
	CrossProduct(sec1_tip_z_axis, sec1_tip_x_axis, sec1_y_axis);
	Normalize(sec1_y_axis,3);
	float sec1_base_x_axis [3] ={cir1_center[0]-0.0,
								 cir1_center[1]-0.0,
								 cir1_center[2]-0.0};
	Normalize(sec1_base_x_axis,3);
	float sec1_base_z_axis[3] = {0.0};
	CrossProduct(sec1_base_x_axis, sec1_y_axis, sec1_base_z_axis);
	Normalize(sec1_base_z_axis,3);
	
	/********* calculate phi angles *********/
	// -> step 1 collect frames
	Eigen::Matrix4f sec1_tip_frame;
	sec1_tip_frame << sec1_tip_x_axis[0], sec1_y_axis[0], sec1_tip_z_axis[0], sec1_tgt_pnt[0],
					  sec1_tip_x_axis[1], sec1_y_axis[1], sec1_tip_z_axis[1], sec1_tgt_pnt[1],
					  sec1_tip_x_axis[2], sec1_y_axis[2], sec1_tip_z_axis[2], sec1_tgt_pnt[2],
					  0.0,                0.0,            0.0,                1.0;
	Eigen::Matrix4f sec2_base_frame;
	sec2_base_frame << sec2_base_x_axis[0], sec2_y_axis[0], sec2_base_z_axis[0], sec1_tgt_pnt[0],
					   sec2_base_x_axis[1], sec2_y_axis[1], sec2_base_z_axis[1], sec1_tgt_pnt[1],
					   sec2_base_x_axis[2], sec2_y_axis[2], sec2_base_z_axis[2], sec1_tgt_pnt[2],
					   0.0,                0.0,            0.0,                1.0;
	Eigen::Matrix4f sec2_tip_frame;
	sec2_tip_frame << sec2_tip_x_axis[0], sec2_y_axis[0], sec2_tip_z_axis[0], sec2_tgt_pnt[0],
					  sec2_tip_x_axis[1], sec2_y_axis[1], sec2_tip_z_axis[1], sec2_tgt_pnt[1],
					  sec2_tip_x_axis[2], sec2_y_axis[2], sec2_tip_z_axis[2], sec2_tgt_pnt[2],
					  0.0,                0.0,            0.0,                1.0;
	Eigen::Matrix4f sec3_base_frame;
	sec3_base_frame << sec3_base_x_axis[0], sec3_y_axis[0], sec3_base_z_axis[0], sec2_tgt_pnt[0],
					   sec3_base_x_axis[1], sec3_y_axis[1], sec3_base_z_axis[1], sec2_tgt_pnt[1],
					   sec3_base_x_axis[2], sec3_y_axis[2], sec3_base_z_axis[2], sec2_tgt_pnt[2],
					   0.0,                0.0,            0.0,                1.0;
	// -> step 2 find phi angles	
	Eigen::Matrix4f tf_sec2_base_to_sec1_tip = (sec1_tip_frame.inverse()) * sec2_base_frame;
	Eigen::VectorXd tf_vec_sec2_base_to_sec1_tip = regular_eigen_tf_matrix_to_vector(tf_sec2_base_to_sec1_tip);
	
	Eigen::Matrix4f tf_sec3_base_to_sec2_tip = (sec2_tip_frame.inverse()) * sec3_base_frame;
	Eigen::VectorXd tf_vec_sec3_base_to_sec2_tip = regular_eigen_tf_matrix_to_vector(tf_sec3_base_to_sec2_tip);
	
	output_sec1_config[2] = 0.0;
	float phi_2 = tf_vec_sec2_base_to_sec1_tip[5];
	output_sec2_config[2] = phi_2;
	float phi_3 = tf_vec_sec3_base_to_sec2_tip[5];
	output_sec3_config[2] = phi_3;
	
	/*************Define new arm config and base frame*********************/
	// update arm config
	std::vector<float> cur_config;
	cur_config.resize(9);
	mani->get_config(cur_config);
	
	cur_config[0] = output_sec1_config[0];
	cur_config[1] = -output_sec1_config[1];
	cur_config[2] = output_sec1_config[2];
	
	cur_config[3] = output_sec2_config[0];
	cur_config[4] = -output_sec2_config[1];
	cur_config[5] = output_sec2_config[2];
	
	cur_config[6] = output_sec3_config[0];
	cur_config[7] = -output_sec3_config[1];
	cur_config[8] = output_sec3_config[2];
	
	// update base frame
	float new_base_x_axis[3] = {cir1_center[0], cir1_center[1], cir1_center[2]};
	Normalize(new_base_x_axis, 3);
	float new_base_z_axis[3] = {0.0};
	CrossProduct(new_base_x_axis, cir1_plane_normal, new_base_z_axis);
	
	float cur_base_frame[16] = {0.0};
	mani->Getbaseframe(cur_base_frame);
	
	cur_base_frame[0] = new_base_x_axis[0]; // x-axis
	cur_base_frame[1] = new_base_x_axis[1];
	cur_base_frame[2] = new_base_x_axis[2];
	
	cur_base_frame[4] = cir1_plane_normal[0]; // y-axis
	cur_base_frame[5] = cir1_plane_normal[1];
	cur_base_frame[6] = cir1_plane_normal[2];
	
	cur_base_frame[8] = new_base_z_axis[0]; // z-axis
	cur_base_frame[9] = new_base_z_axis[1];
	cur_base_frame[10] = new_base_z_axis[2];
	
	std::vector<float> new_base_frame(cur_base_frame, cur_base_frame+16);
	
	mani->update_config(cur_config, new_base_frame);
	
	vector<float> arm_endpoints = mani->get_section_end_points_wrt_world();	
	
	float arm_tip_diff = sqrt ( pow(arm_endpoints[3*n_secs]-tgt_pnt[0],2) + 
								pow(arm_endpoints[3*n_secs+1]-tgt_pnt[1],2) + 
								pow(arm_endpoints[3*n_secs+2]-tgt_pnt[2],2) );
	
	
	#ifdef DEBUG
	std::cout << "updated arm configuration is " << std::endl;
	for (int cfg=0; cfg < 9; ++cfg)
		std::cout << cur_config[cfg] << std::endl;
	std::cout << "updated arm base frame is " << std::endl;
	for (int bf=0; bf < 16; ++bf)
		std::cout << new_base_frame[bf] << std::endl;
	
	std::cout << "computed cir3 is " << std::endl;
	std::cout << "center " << cir3_center[0] << " " << cir3_center[1] << " " << cir3_center[2]<<std::endl;
	std::cout << "radius " << cir3_radius<<std::endl;
	std::cout << "normal " << cir3_plane_normal[0] << " " << cir3_plane_normal[1] << " " <<
				cir3_plane_normal[2] << std::endl;
	
	std::cout << "sec3_y_axis is " << std::endl;
	std::cout << sec3_y_axis[0] << " "  << sec3_y_axis[1] << " "  << sec3_y_axis[2] << std::endl;
	std::cout << "sec3_tip_x_axis is " << std::endl;
	std::cout << sec3_tip_x_axis[0] << " "  << sec3_tip_x_axis[1] << " "  << sec3_tip_x_axis[2] << std::endl;
	std::cout << "sec3_tip_z_axis is " << std::endl;
	std::cout << sec3_tip_z_axis[0] << " "  << sec3_tip_z_axis[1] << " "  << sec3_tip_z_axis[2] << std::endl;
	
	std::cout << "sec3_base_x_axis is " << std::endl;
	std::cout << sec3_base_x_axis[0] << " "  << sec3_base_x_axis[1] << " "  << sec3_base_x_axis[2] << std::endl;
	std::cout << "sec3_base_z_axis is " << std::endl;
	std::cout << sec3_base_z_axis[0] << " "  << sec3_base_z_axis[1] << " "  << sec3_base_z_axis[2] << std::endl;
	std::cout << std::endl;
	std::cout << "computed cir2 is " << std::endl;
	std::cout << "center " << cir2_center[0] << " " << cir2_center[1] << " " << cir2_center[2]<<std::endl;
	std::cout << "radius " << cir2_radius<<std::endl;
	std::cout << "normal " << cir2_plane_normal[0] << " " << cir2_plane_normal[1] << " " <<
				cir2_plane_normal[2] << std::endl;
	
	std::cout << "sec2_y_axis is " << std::endl;
	std::cout << sec2_y_axis[0] << " "  << sec2_y_axis[1] << " "  << sec2_y_axis[2] << std::endl;
	std::cout << "sec2_tip_x_axis is " << std::endl;
	std::cout << sec2_tip_x_axis[0] << " "  << sec2_tip_x_axis[1] << " "  << sec2_tip_x_axis[2] << std::endl;
	std::cout << "sec2_tip_z_axis is " << std::endl;
	std::cout << sec2_tip_z_axis[0] << " "  << sec2_tip_z_axis[1] << " "  << sec2_tip_z_axis[2] << std::endl;

	std::cout << std::endl;
	std::cout << "computed cir1 is " << std::endl;
	std::cout << "center " << cir1_center[0] << " " << cir1_center[1] << " " << cir1_center[2]<<std::endl;
	std::cout << "radius " << cir1_radius<<std::endl;
	std::cout << "normal " << cir1_plane_normal[0] << " " << cir1_plane_normal[1] << " " <<
				cir1_plane_normal[2] << std::endl;
	
	std::cout << "sec1_y_axis is " << std::endl;
	std::cout << sec1_y_axis[0] << " "  << sec1_y_axis[1] << " "  << sec1_y_axis[2] << std::endl;
	std::cout << "sec1_tip_x_axis is " << std::endl;
	std::cout << sec1_tip_x_axis[0] << " "  << sec1_tip_x_axis[1] << " "  << sec1_tip_x_axis[2] << std::endl;
	std::cout << "sec1_tip_z_axis is " << std::endl;
	std::cout << sec1_tip_z_axis[0] << " "  << sec1_tip_z_axis[1] << " "  << sec1_tip_z_axis[2] << std::endl;
	std::cout << "sec1_base_x_axis is " << std::endl;
	std::cout << sec1_base_x_axis[0] << " "  << sec1_base_x_axis[1] << " "  << sec1_base_x_axis[2] << std::endl;
	std::cout << "sec1_base_z_axis is " << std::endl;
	std::cout << sec1_base_z_axis[0] << " "  << sec1_base_z_axis[1] << " "  << sec1_base_z_axis[2] << std::endl;

	std::cout <<std::endl;
	std::cout << "sec3_tgt_pnt is " << tgt_pnt[0] << " " << tgt_pnt[1] 
			  << " " << tgt_pnt[2]<<std::endl;
	std::cout << "sec2_tgt_pnt is " << sec2_tgt_pnt[0] << " " 
			  << sec2_tgt_pnt[1] << " " << sec2_tgt_pnt[2] << std::endl;
	std::cout << "sec1_tgt_pnt is " << sec1_tgt_pnt[0] << " " 
			  << sec1_tgt_pnt[1] << " " << sec1_tgt_pnt[2] << std::endl;
	
	arm_endpoints = mani->get_section_end_points_wrt_world();
	n_secs = mani->get_n_sections();
	std::cout << "sec3 endpoint is " << arm_endpoints[3*n_secs] << " " << arm_endpoints[3*n_secs+1]
			 << " " << arm_endpoints[3*n_secs+2] << std::endl;
	std::cout << "sec2 endpoint is " << arm_endpoints[2*n_secs] << " " << arm_endpoints[2*n_secs+1]
			 << " " << arm_endpoints[2*n_secs+2] << std::endl;
	std::cout << "sec1 endpoint is " << arm_endpoints[1*n_secs] << " " << arm_endpoints[1*n_secs+1]
			 << " " << arm_endpoints[1*n_secs+2] << std::endl;
	
	vector<float> sec_plane_normals(9, 0.0);
	mani->compute_section_plane_normals_wrt_world(sec_plane_normals);
	std::cout << "sec 1 plane normal is " << sec_plane_normals[0] << " " <<  
			 sec_plane_normals[1] << " " <<  sec_plane_normals[2] << std::endl;
	std::cout << "sec 2 plane normal is " << sec_plane_normals[3] << " " <<  
			 sec_plane_normals[4] << " " <<  sec_plane_normals[5] << std::endl;
	std::cout << "sec 3 plane normal is " << sec_plane_normals[6] << " " <<  
			 sec_plane_normals[7] << " " <<  sec_plane_normals[8] << std::endl;
	
	std::cout << "new sec3 frame is "  << std::endl;
	float new_sec3_frame[16] = {0.0};
	mani->compute_section_frame_wrt_world(new_sec3_frame, 2); 
	for (int temp_cnt=0; temp_cnt<16; ++temp_cnt)
		std::cout << new_sec3_frame[temp_cnt] << std::endl;
	
	std::cout << "new sec2 frame is "  << std::endl;
	float new_sec2_frame[16] = {0.0};
	mani->compute_section_frame_wrt_world(new_sec2_frame, 1); 
	for (int temp_cnt=0; temp_cnt<16; ++temp_cnt)
		std::cout << new_sec2_frame[temp_cnt] << std::endl;
	
	std::cout << "new sec1 frame is "  << std::endl;
	float new_sec1_frame[16] = {0.0};
	mani->compute_section_frame_wrt_world(new_sec1_frame, 0); 
	for (int temp_cnt=0; temp_cnt<16; ++temp_cnt)
		std::cout << new_sec1_frame[temp_cnt] << std::endl;
		
	#endif
	
	#ifdef DEBUG
	std::cout << "sec3_tgt_pnt is " << tgt_pnt[0] << " " << tgt_pnt[1] 
			  << " " << tgt_pnt[2]<<std::endl;
	std::cout << "sec2_tgt_pnt is " << sec2_tgt_pnt[0] << " " 
			  << sec2_tgt_pnt[1] << " " << sec2_tgt_pnt[2] << std::endl;
	std::cout << "sec1_tgt_pnt is " << sec1_tgt_pnt[0] << " " 
			  << sec1_tgt_pnt[1] << " " << sec1_tgt_pnt[2] << std::endl;
	
	arm_endpoints = mani->get_section_end_points_wrt_world();
	n_secs = mani->get_n_sections();
	std::cout << "sec3 endpoint is " << arm_endpoints[3*n_secs] << " " << arm_endpoints[3*n_secs+1]
			 << " " << arm_endpoints[3*n_secs+2] << std::endl;
	std::cout << "sec2 endpoint is " << arm_endpoints[2*n_secs] << " " << arm_endpoints[2*n_secs+1]
			 << " " << arm_endpoints[2*n_secs+2] << std::endl;
	std::cout << "sec1 endpoint is " << arm_endpoints[1*n_secs] << " " << arm_endpoints[1*n_secs+1]
			 << " " << arm_endpoints[1*n_secs+2] << std::endl;
	
	printf("arm_tip_diff in 1st try is %f\n", arm_tip_diff);
	#endif
	
	if (arm_tip_diff < 1e-3) {
		#ifdef DEBUG
		printf("IK Solution found in 1st try\n");
		#endif
		return true;
	}
	else
	{
		// catch the case all secs use large arcs
		cur_config[0] = 2.0f*M_PI*cir1_radius - cur_config[0];
		cur_config[3] = 2.0f*M_PI*cir2_radius - cur_config[3];
		cur_config[6] = 2.0f*M_PI*cir3_radius - cur_config[6];
		mani->update_config(cur_config, new_base_frame);
		arm_endpoints = mani->get_section_end_points_wrt_world();
		arm_tip_diff = sqrt(pow(arm_endpoints[3*n_secs]-tgt_pnt[0],2) + 
							pow(arm_endpoints[3*n_secs+1]-tgt_pnt[1],2) + 
							pow(arm_endpoints[3*n_secs+2]-tgt_pnt[2],2) );
		//~ printf("arm_tip_diff in 2nd try is %f\n", arm_tip_diff);
		if (arm_tip_diff < 1e-3) {
			#ifdef DEBUG
			printf("IK Solution found in 2nd try\n");
			
			std::cout << "updated arm configuration is " << std::endl;
			for (int cfg=0; cfg < 9; ++cfg)
				std::cout << cur_config[cfg] << std::endl;
			std::cout << "updated arm base frame is " << std::endl;
			for (int bf=0; bf < 16; ++bf)
				std::cout << new_base_frame[bf] << std::endl;
			#endif
			
			return true;
		}
		else {
			return false;
		}
	}
}


bool Environment::generateAdjustedArcMotionGivenTgtPnt(float tgt_pnt[3],
													   int corner_ind)
{
	int n_secs = mani->get_n_sections();
	vector<float> mani_eps = mani->get_section_end_points_wrt_world();
	float arc_start[3]={mani_eps[3*(n_secs-1)], 
						mani_eps[3*(n_secs-1)+1],
						mani_eps[3*(n_secs-1)+2]}; 
	float vec_cord[3] = {tgt_pnt[0] - arc_start[0],
						 tgt_pnt[1] - arc_start[1],
						 tgt_pnt[2] - arc_start[2]};
	float sec2_frame[16] = {0.0};
	mani->compute_section_frame_wrt_world(sec2_frame, 1); 
	float arc_start_z_axis [3] = {sec2_frame[8], sec2_frame[9], sec2_frame[10]};
	float arc_cir_pln_normal[3] = {0.0};
	CrossProduct(arc_start_z_axis, vec_cord, arc_cir_pln_normal);
	Normalize(arc_cir_pln_normal,3);
	
	float sec1_tgt_pnt[3] = {0.0};
	if (corner_ind==1) // 1st gap
	{
		float sec1_frame[16] = {0.0};
		mani->compute_section_frame_wrt_world(sec1_frame, 0); // 0 is section 1
		sec1_tgt_pnt[0] = sec1_frame[12];
		sec1_tgt_pnt[1] = sec1_frame[13];
		sec1_tgt_pnt[2] = sec1_frame[14];
		
		// generate motion plan
		// get sec3 current length
		std::vector<float> new_config(9,0.0);
		mani->get_config(new_config);
		
		if(!wholeArmIkByTgtPntAndArc2CordDistAndNormal(parameters.arc1_dist_to_coord[1], 
													   sec1_tgt_pnt, 
													   arc_start, 
													   tgt_pnt, 
													   arc_cir_pln_normal)) {
			printf("Plan Arc Motion Failed.\n");
			return false;
		}
		else {
			float sec3_arc_len = new_config[6];
			
			mani->get_config(new_config);
			
			float cur_base_frame[16] = {0.0};
			mani->Getbaseframe(cur_base_frame);
			std::vector<float> new_base_frame(cur_base_frame, cur_base_frame+16);
			
			float sec3_arc_len_setPnt = new_config[6];
			while (sec3_arc_len < sec3_arc_len_setPnt)
			{
				new_config[6] = sec3_arc_len;
				mani->update_config(new_config, new_base_frame);
				
				sec3_arc_len += 0.01;
				g_configs_todraw.push_back(new_config);
				g_base_frames_todraw.push_back(new_base_frame);
			}
			return true;
		}
	
	}
	else if (corner_ind==2) // 2nd gap
	{
		float sec1_frame[16] = {0.0};
		mani->compute_section_frame_wrt_world(sec1_frame, 0);
		sec1_tgt_pnt[0] = sec1_frame[12];
		sec1_tgt_pnt[1] = sec1_frame[13];
		sec1_tgt_pnt[2] = sec1_frame[14];
		
		// generate motion plan
		// get sec3 current length
		std::vector<float> new_config(9,0.0);
		mani->get_config(new_config);
		
		if(!wholeArmIkByTgtPntAndArc2CordDistAndNormal(parameters.arc2_dist_to_coord[1],  
													   sec1_tgt_pnt, 
													   arc_start, 
													   tgt_pnt, 
													   arc_cir_pln_normal)) {
			printf("Plan Arc Motion Failed.\n");
			return false;
		}
		else {
			float sec3_arc_len = new_config[6];
			
			mani->get_config(new_config);
			
			float cur_base_frame[16] = {0.0};
			mani->Getbaseframe(cur_base_frame);
			std::vector<float> new_base_frame(cur_base_frame, cur_base_frame+16);
			
			float sec3_arc_len_setPnt = new_config[6];
			while (sec3_arc_len < sec3_arc_len_setPnt)
			{
				new_config[6] = sec3_arc_len;
				mani->update_config(new_config, new_base_frame);
				
				sec3_arc_len += 0.01;
				g_configs_todraw.push_back(new_config);
				g_base_frames_todraw.push_back(new_base_frame);
			}
			return true;
		}
	}
	else if (corner_ind == 3) //3rd gap
	{
		float sec1_frame[16] = {0.0};
		mani->compute_section_frame_wrt_world(sec1_frame, 0);
		sec1_tgt_pnt[0] = sec1_frame[12];
		sec1_tgt_pnt[1] = sec1_frame[13];
		sec1_tgt_pnt[2] = sec1_frame[14];
		
		// generate motion plan
		// get sec3 current length
		std::vector<float> new_config(9,0.0);
		mani->get_config(new_config);
		
		if(!wholeArmIkByTgtPntAndArc2CordDistAndNormal(parameters.arc3_dist_to_coord[1],  
													   sec1_tgt_pnt, 
													   arc_start, 
													   tgt_pnt, 
													   arc_cir_pln_normal)) {
			printf("Plan Arc Motion Failed.\n");
			return false;
		}
		else {
			float sec3_arc_len = new_config[6];
			
			mani->get_config(new_config);
			
			float cur_base_frame[16] = {0.0};
			mani->Getbaseframe(cur_base_frame);
			std::vector<float> new_base_frame(cur_base_frame, cur_base_frame+16);
			
			float sec3_arc_len_setPnt = new_config[6];
			while (sec3_arc_len < sec3_arc_len_setPnt)
			{
				new_config[6] = sec3_arc_len;
				mani->update_config(new_config, new_base_frame);
				
				sec3_arc_len += 0.01;
				g_configs_todraw.push_back(new_config);
				g_base_frames_todraw.push_back(new_base_frame);
			}
			return true;
		}
	}
	else
		printf("Too many corners to adjust for a 3-sec manipulator.\n");
		
}


bool Environment::generateArcMotionGivenTgtPntPlaneNormalAndDist(float tgt_pnt[3],
																 int corner_ind)
{
	// compute arc cir normal
	int n_secs = mani->get_n_sections();
	vector<float> mani_eps = mani->get_section_end_points_wrt_world();
	float tip [3] = {mani_eps[3*(n_secs)], 
					 mani_eps[3*(n_secs)+1],
					 mani_eps[3*(n_secs)+2]}; 
	
	float vec_cord [3] = {tgt_pnt[0] - tip[0],
						  tgt_pnt[1] - tip[1],
						  tgt_pnt[2] - tip[2]};
	
	float sec3_frame[16] = {0.0};
	mani->compute_section_frame_wrt_world(sec3_frame, 2); // 2 is section 3
	float tip_z_axis [3] = {sec3_frame[8], sec3_frame[9], sec3_frame[10]};
	
	float arc_cir_pln_normal[3] = {0.0};
	CrossProduct(tip_z_axis, vec_cord, arc_cir_pln_normal);
	Normalize(arc_cir_pln_normal,3);
	
	float arc_start [3] = {tip[0],tip[1],tip[2]}; //sec2 end pnt
	// compute sec1 end point
	float sec1_tgt_pnt[3] = {0.0};
	if (corner_ind==1) // 1st gap
	{
		float sec1_frame[16] = {0.0};
		mani->compute_section_frame_wrt_world(sec1_frame, 0); // 0 is section 1
		sec1_tgt_pnt[0] = sec1_frame[12];
		sec1_tgt_pnt[1] = sec1_frame[13];
		sec1_tgt_pnt[2] = sec1_frame[14];
		
		// generate motion plan
		if(!wholeArmIkByTgtPntAndArc2CordDistAndNormal(parameters.arc1_dist_to_coord[0], 
													   sec1_tgt_pnt,  //sec1 end pnt
													   arc_start,     //sec2 end pnt
													   tgt_pnt,       //sec3 end pnt
													   arc_cir_pln_normal)) {
			printf("Plan Arc Motion Failed.\n");
			return false;
		}
		else {
			std::vector<float> new_config(9,0.0);
			mani->get_config(new_config);
			
			float cur_base_frame[16] = {0.0};
			mani->Getbaseframe(cur_base_frame);
			std::vector<float> new_base_frame(cur_base_frame, cur_base_frame+16);
			
			#ifdef DEBUG
			printf("Corner planning result config is\n");
			for (int i=0; i<9; ++i)
			{
				std::cout << new_config[i] << std::endl;
			}
			printf("Corner planning result base frame is\n");
			for (int i=0; i<16; ++i)
			{
				std::cout << cur_base_frame[i] << std::endl;
			}
			#endif
			
			float sec3_arc_len = 0.01;
			float sec3_arc_len_setPnt = new_config[6];
			while (sec3_arc_len < sec3_arc_len_setPnt)
			{
				new_config[6] = sec3_arc_len;
				mani->update_config(new_config, new_base_frame);
				
				sec3_arc_len += 0.01;
				g_configs_todraw.push_back(new_config);
				g_base_frames_todraw.push_back(new_base_frame);
			}
			return true;
		}
	
	}
	else if (corner_ind==2) // 2nd gap
	{
		float sec2_frame[16] = {0.0};
		mani->compute_section_frame_wrt_world(sec2_frame, 1); // 0 is section 1
		sec1_tgt_pnt[0] = sec2_frame[12];
		sec1_tgt_pnt[1] = sec2_frame[13];
		sec1_tgt_pnt[2] = sec2_frame[14];
		
		// generate motion plan
		if(!wholeArmIkByTgtPntAndArc2CordDistAndNormal(parameters.arc2_dist_to_coord[0], 
													   sec1_tgt_pnt,  //sec1 end pnt
													   arc_start,     //sec2 end pnt
													   tgt_pnt,       //sec3 end pnt
													   arc_cir_pln_normal)) {
			printf("Plan Arc Motion Failed.\n");
			return false;
		}
		else {
			std::vector<float> new_config(9,0.0);
			mani->get_config(new_config);
			
			float cur_base_frame[16] = {0.0};
			mani->Getbaseframe(cur_base_frame);
			std::vector<float> new_base_frame(cur_base_frame, cur_base_frame+16);
			
			float sec3_arc_len = 0.01;
			float sec3_arc_len_setPnt = new_config[6];
			while (sec3_arc_len < sec3_arc_len_setPnt)
			{
				new_config[6] = sec3_arc_len;
				mani->update_config(new_config, new_base_frame);
				
				sec3_arc_len += 0.01;
				g_configs_todraw.push_back(new_config);
				g_base_frames_todraw.push_back(new_base_frame);
			}
			return true;
		}
	}
	else if (corner_ind==3) // 3rd gap
	{
		float sec1_frame[16] = {0.0};
		mani->compute_section_frame_wrt_world(sec1_frame, 0); // 0 is section 1
		sec1_tgt_pnt[0] = sec1_frame[12];
		sec1_tgt_pnt[1] = sec1_frame[13];
		sec1_tgt_pnt[2] = sec1_frame[14];
		
		// generate motion plan
		if(!wholeArmIkByTgtPntAndArc2CordDistAndNormal(parameters.arc3_dist_to_coord[0], 
													   sec1_tgt_pnt,  //sec1 end pnt
													   arc_start,     //sec2 end pnt
													   tgt_pnt,       //sec3 end pnt
													   arc_cir_pln_normal)) {
			printf("Plan Arc Motion Failed.\n");
			return false;
		}
		else {
			std::vector<float> new_config(9,0.0);
			mani->get_config(new_config);
			
			float cur_base_frame[16] = {0.0};
			mani->Getbaseframe(cur_base_frame);
			std::vector<float> new_base_frame(cur_base_frame, cur_base_frame+16);
			
			float sec3_arc_len = 0.01;
			float sec3_arc_len_setPnt = new_config[6];
				
			while (sec3_arc_len < sec3_arc_len_setPnt)
			{
				new_config[6] = sec3_arc_len;
				mani->update_config(new_config, new_base_frame);
				
				sec3_arc_len += 0.01;
				g_configs_todraw.push_back(new_config);
				g_base_frames_todraw.push_back(new_base_frame);
			}
			return true;
		}
	}
	else
		printf("Too many corners for a 3-sec manipulator!\n");
	
	
}


bool Environment::wholeArmIkByTgtPntAndArc2CordDistAndNormal(float arc2cord_dist, 
															 float sec1_tgt_pnt[3], // sec1 end pnt
															 float arc_start[3], // sec2 end pnt
															 float tgt_pnt[3], // sec3 end pnt
															 float arc_cir_plane_normal[3])
{
	float sec1_len_limit = 10.0;
	float sec2_len_limit = 10.0;
	float sec3_len_limit = 10.0;
	
	int n_secs = mani->get_n_sections();
	vector<float> mani_eps = mani->get_section_end_points_wrt_world();
	float tip [3] = {mani_eps[3*(n_secs)], 
					 mani_eps[3*(n_secs)+1],
					 mani_eps[3*(n_secs)+2]}; 
	std::vector<float> tip_pos(tip, tip+3);
	
	float vec_armtip_to_tgtpnt[3] ={tgt_pnt[0]-tip_pos[0],
									tgt_pnt[1]-tip_pos[1],
									tgt_pnt[2]-tip_pos[2]};
	
	// get section 3 tip frame
	float sec3_frame[16] = {0.0};
	mani->compute_section_frame_wrt_world(sec3_frame, 2); // 2 is section 3
	// get section 2 tip frame
	float sec2_frame[16] = {0.0};
	mani->compute_section_frame_wrt_world(sec2_frame, 1); // 1 is section 2
	// get section 1 tip frame
	float sec1_frame[16] = {0.0};
	mani->compute_section_frame_wrt_world(sec1_frame, 0); // 0 is section 1
	
	/****** Compute Arc Circle *******/	
	float arc_cir_center[3] = {0.0};
	float arc_cir_radius = 0.0;
	bool isArcCirFound = compute_circle_given_twopoints_arc2cordDis(arc_start, tgt_pnt, arc2cord_dist, 
																	arc_cir_plane_normal, 
																	arc_cir_center,
																	arc_cir_radius);
	
	#ifdef DEBUG
	printf("arc_cir_center is (%f,%f,%f)\n",arc_cir_center[0], arc_cir_center[1], arc_cir_center[2]);
	printf("arc_cir_radius is %f\n", arc_cir_radius);
	float vec1[3] = {arc_cir_center[0]-tgt_pnt[0],
					 arc_cir_center[1]-tgt_pnt[1],
					 arc_cir_center[2]-tgt_pnt[2]};
					 
	float vec2[3] = {arc_cir_center[0]-arc_start[0],
					 arc_cir_center[1]-arc_start[1],
					 arc_cir_center[2]-arc_start[2]};
	float test[3] = {0.0};
	CrossProduct(vec1, vec2, test);
	Normalize(test, 3);
	std::cout << "test normal is " << test[0] << " " << test[1] << " " << test[2] << std::endl;
	printf("real normal is (%f,%f,%f)\n", arc_cir_plane_normal[0],arc_cir_plane_normal[1],arc_cir_plane_normal[2]);
	#endif
	if (isArcCirFound) {
		float half_dist_uv = two_pts_dist(tgt_pnt, arc_start, 3)/2.0;
		float computed_arc_len = 2.0 * arc_cir_radius * asin(half_dist_uv / arc_cir_radius);
		if (computed_arc_len < sec3_len_limit) {
			// fill in information for sec 3
			std::vector<float> output_sec3_config(3, 0.0);
			output_sec3_config[0] = computed_arc_len;
			output_sec3_config[1] = 1.0 / arc_cir_radius;
			output_sec3_config[2] = 0.0;
			
			/****Compute Config for Sec 2****/
			float sec2_tgt_pnt[3] = {arc_start[0], arc_start[1], arc_start[2]};
			float sec2_tgt_pnt_tangent[3] = {0.0};
			// step 1 -> get tan vec at sec2_tgt_pnt
			float sec3_tip_x_axis[3] = {arc_cir_center[0] - tgt_pnt[0],
										arc_cir_center[1] - tgt_pnt[1],
										arc_cir_center[2] - tgt_pnt[2]};
			Normalize(sec3_tip_x_axis, 3);
			float sec3_y_axis[3] = {arc_cir_plane_normal[0],
									arc_cir_plane_normal[1],
									arc_cir_plane_normal[2]};
			float sec3_tip_z_axis[3] = {0.0};
			CrossProduct(sec3_tip_x_axis,sec3_y_axis, sec3_tip_z_axis);
			Normalize(sec3_tip_z_axis,3);
			float sec3_base_x_axis[3] ={arc_cir_center[0] - arc_start[0],
										arc_cir_center[1] - arc_start[1],
										arc_cir_center[2] - arc_start[2]};
			Normalize(sec3_base_x_axis,3);
			float sec3_base_z_axis[3] = {0.0};
			CrossProduct(sec3_base_x_axis, sec3_y_axis, sec3_base_z_axis);
			Normalize(sec3_base_z_axis,3);
			sec2_tgt_pnt_tangent[0] = sec3_base_z_axis[0];
			sec2_tgt_pnt_tangent[1] = sec3_base_z_axis[1];
			sec2_tgt_pnt_tangent[2] = sec3_base_z_axis[2];
			
			// step 2 -> get sec2 new base position
			float sec1_vec_scale = 0.;
			sec1_frame[12] += sec1_vec_scale * vec_armtip_to_tgtpnt[0];
			sec1_frame[13] += sec1_vec_scale * vec_armtip_to_tgtpnt[1];
			sec1_frame[14] += sec1_vec_scale * vec_armtip_to_tgtpnt[2];
			
			// step 3 -> compute cir for sec 2
			float cir2_plane_normal[3] = {0.0};
			float cir2_center[3] = {0.0};
			float cir2_radius = 0.0;
			bool isCir2Found = compute_circle_given_twopoints_one_tangentVec(sec2_tgt_pnt, sec1_tgt_pnt, 
																			sec2_tgt_pnt_tangent, 
																			cir2_plane_normal,
																			cir2_center,
																			cir2_radius);
			std::vector<float> output_sec2_config(3, 0.0);
			if (isCir2Found){
				float half_dist_uv_sec2 = two_pts_dist(sec2_tgt_pnt, sec1_tgt_pnt, 3)/2.0;
				output_sec2_config[0] = 2.0 * cir2_radius * asin(half_dist_uv_sec2 / cir2_radius);
				output_sec2_config[1] = 1.0 / cir2_radius;
				output_sec2_config[2] = 0.0;
			}
			else
				std::cout << "cir 2 is not found!" << std::endl;
			float sec2_tip_x_axis[3] = {cir2_center[0]-sec2_tgt_pnt[0],
										cir2_center[1]-sec2_tgt_pnt[1],
										cir2_center[2]-sec2_tgt_pnt[2]};
			Normalize(sec2_tip_x_axis,3);
			float sec2_y_axis[3] = {cir2_plane_normal[0],cir2_plane_normal[1],cir2_plane_normal[2]};
			float sec2_tip_z_axis[3] = {0.0};
			CrossProduct(sec2_tip_x_axis, sec2_y_axis, sec2_tip_z_axis);
			Normalize(sec2_tip_z_axis, 3);
			float sec2_base_x_axis[3] ={cir2_center[0]-sec1_tgt_pnt[0],
										cir2_center[1]-sec1_tgt_pnt[1],
										cir2_center[2]-sec1_tgt_pnt[2]};
			float sec2_base_z_axis[3] = {0.0};
			CrossProduct(sec2_base_x_axis, sec2_y_axis, sec2_base_z_axis);
			Normalize(sec2_base_z_axis,3);
			
			/****Compute Config for Sec 1****/
			float sec1_tgt_pnt_tangent[3] = {sec2_base_z_axis[0], sec2_base_z_axis[1], sec2_base_z_axis[2]};
			float origin[3] = {0.0};
			float cir1_plane_normal[3] = {0.0};
			float cir1_center[3] = {0.0};
			float cir1_radius = 0.0;
			bool isCir1Found = compute_circle_given_twopoints_one_tangentVec(sec1_tgt_pnt, origin, 
																			 sec1_tgt_pnt_tangent, 
																			 cir1_plane_normal,
																			 cir1_center,
																			 cir1_radius);
			std::vector<float> output_sec1_config(3, 0.0);
			if (isCir1Found) {
				float half_dist_uv_sec1 = two_pts_dist(sec1_tgt_pnt, origin, 3)/2.0;
				output_sec1_config[0] = 2.0 * cir1_radius * asin(half_dist_uv_sec1 / cir1_radius);
				output_sec1_config[1] = 1.0 / cir1_radius;
				output_sec1_config[2] = 0.0;
			}
			else 
				std::cout << "cir1 is not found!" << std::endl;
				
			// adjust base frame
			float sec1_tip_z_axis [3] = {sec2_base_z_axis[0],
										 sec2_base_z_axis[1],
										 sec2_base_z_axis[2]};
			float sec1_tip_x_axis [3] = {cir1_center[0]-sec1_tgt_pnt[0],
										 cir1_center[1]-sec1_tgt_pnt[1],
										 cir1_center[2]-sec1_tgt_pnt[2]};
			Normalize(sec1_tip_x_axis,3);
			float sec1_y_axis[3] = {cir1_plane_normal[0],cir1_plane_normal[1],cir1_plane_normal[2]};
			float sec1_base_x_axis [3] ={cir1_center[0]-0.0,
										 cir1_center[1]-0.0,
										 cir1_center[2]-0.0};
			Normalize(sec1_base_x_axis,3);
			float sec1_base_z_axis[3] = {0.0};
			CrossProduct(sec1_base_x_axis, sec1_y_axis, sec1_base_z_axis);
			Normalize(sec1_base_z_axis,3);
			
			/********* Calculate phi Angles *********/
			// -> step 1 collect frames
			Eigen::Matrix4f sec1_tip_frame;
			sec1_tip_frame << sec1_tip_x_axis[0], sec1_y_axis[0], sec1_tip_z_axis[0], sec1_tgt_pnt[0],
							  sec1_tip_x_axis[1], sec1_y_axis[1], sec1_tip_z_axis[1], sec1_tgt_pnt[1],
							  sec1_tip_x_axis[2], sec1_y_axis[2], sec1_tip_z_axis[2], sec1_tgt_pnt[2],
							  0.0,                0.0,            0.0,                1.0;
			Eigen::Matrix4f sec2_base_frame;
			sec2_base_frame << sec2_base_x_axis[0], sec2_y_axis[0], sec2_base_z_axis[0], sec1_tgt_pnt[0],
							   sec2_base_x_axis[1], sec2_y_axis[1], sec2_base_z_axis[1], sec1_tgt_pnt[1],
							   sec2_base_x_axis[2], sec2_y_axis[2], sec2_base_z_axis[2], sec1_tgt_pnt[2],
							   0.0,                0.0,            0.0,                1.0;
			Eigen::Matrix4f sec2_tip_frame;
			sec2_tip_frame << sec2_tip_x_axis[0], sec2_y_axis[0], sec2_tip_z_axis[0], sec2_tgt_pnt[0],
							  sec2_tip_x_axis[1], sec2_y_axis[1], sec2_tip_z_axis[1], sec2_tgt_pnt[1],
							  sec2_tip_x_axis[2], sec2_y_axis[2], sec2_tip_z_axis[2], sec2_tgt_pnt[2],
							  0.0,                0.0,            0.0,                1.0;
			Eigen::Matrix4f sec3_base_frame;
			sec3_base_frame << sec3_base_x_axis[0], sec3_y_axis[0], sec3_base_z_axis[0], sec2_tgt_pnt[0],
							   sec3_base_x_axis[1], sec3_y_axis[1], sec3_base_z_axis[1], sec2_tgt_pnt[1],
							   sec3_base_x_axis[2], sec3_y_axis[2], sec3_base_z_axis[2], sec2_tgt_pnt[2],
							   0.0,                0.0,            0.0,                1.0;
			// -> step 2 find phi angles	
			Eigen::Matrix4f tf_sec2_base_to_sec1_tip = (sec1_tip_frame.inverse()) * sec2_base_frame;
			Eigen::VectorXd tf_vec_sec2_base_to_sec1_tip = regular_eigen_tf_matrix_to_vector(tf_sec2_base_to_sec1_tip);
			
			Eigen::Matrix4f tf_sec3_base_to_sec2_tip = (sec2_tip_frame.inverse()) * sec3_base_frame;
			Eigen::VectorXd tf_vec_sec3_base_to_sec2_tip = regular_eigen_tf_matrix_to_vector(tf_sec3_base_to_sec2_tip);
			
			output_sec1_config[2] = 0.0;
			float phi_2 = tf_vec_sec2_base_to_sec1_tip[5];
			output_sec2_config[2] = phi_2;
			float phi_3 = tf_vec_sec3_base_to_sec2_tip[5];
			output_sec3_config[2] = phi_3;
			
			
			/*************Define new arm config and base frame*********************/
			// update arm config
			std::vector<float> cur_config;
			cur_config.resize(9);
			mani->get_config(cur_config);
			
			cur_config[0] = output_sec1_config[0];
			cur_config[1] = -output_sec1_config[1];
			cur_config[2] = output_sec1_config[2];
			
			cur_config[3] = output_sec2_config[0];
			cur_config[4] = -output_sec2_config[1];
			cur_config[5] = output_sec2_config[2];
			
			cur_config[6] = output_sec3_config[0];
			cur_config[7] = -output_sec3_config[1];
			cur_config[8] = output_sec3_config[2];
			
			// update base frame
			float new_base_x_axis[3] = {cir1_center[0], cir1_center[1], cir1_center[2]};
			Normalize(new_base_x_axis, 3);
			float new_base_z_axis[3] = {0.0};
			CrossProduct(new_base_x_axis, cir1_plane_normal, new_base_z_axis);
			
			float cur_base_frame[16] = {0.0};
			mani->Getbaseframe(cur_base_frame);
			cur_base_frame[0] = new_base_x_axis[0]; // x-axis
			cur_base_frame[1] = new_base_x_axis[1];
			cur_base_frame[2] = new_base_x_axis[2];
			
			cur_base_frame[4] = cir1_plane_normal[0]; // y-axis
			cur_base_frame[5] = cir1_plane_normal[1];
			cur_base_frame[6] = cir1_plane_normal[2];
			
			cur_base_frame[8] = new_base_z_axis[0]; // z-axis
			cur_base_frame[9] = new_base_z_axis[1];
			cur_base_frame[10] = new_base_z_axis[2];
			std::vector<float> new_base_frame(cur_base_frame, cur_base_frame+16);
			
			mani->update_config(cur_config, new_base_frame);
			
			vector<float> arm_endpoints = mani->get_section_end_points_wrt_world();	
			
			float arm_tip_diff = sqrt ( pow(arm_endpoints[3*n_secs]-tgt_pnt[0],2) + 
										pow(arm_endpoints[3*n_secs+1]-tgt_pnt[1],2) + 
										pow(arm_endpoints[3*n_secs+2]-tgt_pnt[2],2) );
			
			#ifdef DEBUG
			std::cout << "sec3_tgt_pnt is " << tgt_pnt[0] << " " << tgt_pnt[1] 
			  << " " << tgt_pnt[2]<<std::endl;
			std::cout << "sec2_tgt_pnt is " << sec2_tgt_pnt[0] << " " 
					  << sec2_tgt_pnt[1] << " " << sec2_tgt_pnt[2] << std::endl;
			std::cout << "sec1_tgt_pnt is " << sec1_tgt_pnt[0] << " " 
					  << sec1_tgt_pnt[1] << " " << sec1_tgt_pnt[2] << std::endl;
			
			arm_endpoints = mani->get_section_end_points_wrt_world();
			std::cout << "sec3 endpoint is " << arm_endpoints[3*n_secs] << " " << arm_endpoints[3*n_secs+1]
					 << " " << arm_endpoints[3*n_secs+2] << std::endl;
			std::cout << "sec2 endpoint is " << arm_endpoints[2*n_secs] << " " << arm_endpoints[2*n_secs+1]
					 << " " << arm_endpoints[2*n_secs+2] << std::endl;
			std::cout << "sec1 endpoint is " << arm_endpoints[1*n_secs] << " " << arm_endpoints[1*n_secs+1]
					 << " " << arm_endpoints[1*n_secs+2] << std::endl;
			
			std::cout << "sec3_y_axis is " << std::endl;
			std::cout << sec3_y_axis[0] << " "  << sec3_y_axis[1] << " "  << sec3_y_axis[2] << std::endl;
			std::cout << "sec3_tip_x_axis is " << std::endl;
			std::cout << sec3_tip_x_axis[0] << " "  << sec3_tip_x_axis[1] << " "  << sec3_tip_x_axis[2] << std::endl;
			std::cout << "sec3_tip_z_axis is " << std::endl;
			std::cout << sec3_tip_z_axis[0] << " "  << sec3_tip_z_axis[1] << " "  << sec3_tip_z_axis[2] << std::endl;
			
			std::cout << "sec3_base_x_axis is " << std::endl;
			std::cout << sec3_base_x_axis[0] << " "  << sec3_base_x_axis[1] << " "  << sec3_base_x_axis[2] << std::endl;
			std::cout << "sec3_base_z_axis is " << std::endl;
			std::cout << sec3_base_z_axis[0] << " "  << sec3_base_z_axis[1] << " "  << sec3_base_z_axis[2] << std::endl;
			std::cout << std::endl;
			
			std::cout << "new sec3 frame is "  << std::endl;
			float new_sec3_frame[16] = {0.0};
			mani->compute_section_frame_wrt_world(new_sec3_frame, 2); 
			for (int temp_cnt=0; temp_cnt<16; ++temp_cnt)
				std::cout << new_sec3_frame[temp_cnt] << std::endl;
				
			std::cout << "new sec2 frame is "  << std::endl;
			float new_sec2_frame[16] = {0.0};
			mani->compute_section_frame_wrt_world(new_sec2_frame, 1); 
			for (int temp_cnt=0; temp_cnt<16; ++temp_cnt)
				std::cout << new_sec2_frame[temp_cnt] << std::endl;
			#endif
			
			#ifdef DEBUG
			printf("1st try\n");
			std::cout << "sec3_tgt_pnt is " << tgt_pnt[0] << " " << tgt_pnt[1] 
			  << " " << tgt_pnt[2]<<std::endl;
			std::cout << "sec2_tgt_pnt is " << sec2_tgt_pnt[0] << " " 
					  << sec2_tgt_pnt[1] << " " << sec2_tgt_pnt[2] << std::endl;
			std::cout << "sec1_tgt_pnt is " << sec1_tgt_pnt[0] << " " 
					  << sec1_tgt_pnt[1] << " " << sec1_tgt_pnt[2] << std::endl;
			
			arm_endpoints = mani->get_section_end_points_wrt_world();
			std::cout << "sec3 endpoint is " << arm_endpoints[3*n_secs] << " " << arm_endpoints[3*n_secs+1]
					 << " " << arm_endpoints[3*n_secs+2] << std::endl;
			std::cout << "sec2 endpoint is " << arm_endpoints[2*n_secs] << " " << arm_endpoints[2*n_secs+1]
					 << " " << arm_endpoints[2*n_secs+2] << std::endl;
			std::cout << "sec1 endpoint is " << arm_endpoints[1*n_secs] << " " << arm_endpoints[1*n_secs+1]
					 << " " << arm_endpoints[1*n_secs+2] << std::endl;
			
			std::cout << "updated arm configuration is " << std::endl;
			for (int cfg=0; cfg < 9; ++cfg)
				std::cout << cur_config[cfg] << std::endl;
			std::cout << "updated arm base frame is " << std::endl;
			for (int bf=0; bf < 16; ++bf)
				std::cout << new_base_frame[bf] << std::endl;
			#endif
			
			if (arm_tip_diff < 1e-5) {
				printf("IK found in 1st try\n");
				return true;
			}
			else
			{
				// catch the case all secs use large arcs
				cur_config[0] = 2*M_PI*cir1_radius - cur_config[0];
				cur_config[3] = 2*M_PI*cir2_radius - cur_config[3];
				cur_config[6] = 2*M_PI*arc_cir_radius - cur_config[6];
				mani->update_config(cur_config, new_base_frame);
				arm_endpoints = mani->get_section_end_points_wrt_world();
				arm_tip_diff = sqrt(pow(arm_endpoints[3*n_secs]-tgt_pnt[0],2) + 
									pow(arm_endpoints[3*n_secs+1]-tgt_pnt[1],2) + 
									pow(arm_endpoints[3*n_secs+2]-tgt_pnt[2],2));
				
				#ifdef DEBUG
				printf("2nd try\n");
				arm_endpoints = mani->get_section_end_points_wrt_world();
				std::cout << "sec3 endpoint is " << arm_endpoints[3*n_secs] << " " << arm_endpoints[3*n_secs+1]
						 << " " << arm_endpoints[3*n_secs+2] << std::endl;
				std::cout << "sec2 endpoint is " << arm_endpoints[2*n_secs] << " " << arm_endpoints[2*n_secs+1]
						 << " " << arm_endpoints[2*n_secs+2] << std::endl;
				std::cout << "sec1 endpoint is " << arm_endpoints[1*n_secs] << " " << arm_endpoints[1*n_secs+1]
						 << " " << arm_endpoints[1*n_secs+2] << std::endl;
				#endif
				
				if (arm_tip_diff < 1e-5) {
					printf("IK found in 2nd try\n");
					return true;
				}
				else 
					return false;
			}
		}
		else if ( computed_arc_len < (sec3_len_limit+sec2_len_limit) ){
		
		}
		else if ( computed_arc_len < (sec3_len_limit+sec2_len_limit+sec1_len_limit) ){
		
		}
		else
			std::cout << "Computed Arc is Too Long !" << std::endl;
	}
	else 
		std::cout << "Arc is not found !" << std::endl;
		
		
		
		
		
		
	return false;
}

bool Environment::tgtPntPlaneFitting(pcl::PointCloud<pcl::PointXYZRGBA>::Ptr &new_tgt_pnts_cl,
									 std::vector<float> &plane_normal)
{
	pcl::ModelCoefficients::Ptr coefficients (new pcl::ModelCoefficients);
	pcl::PointIndices::Ptr inliers (new pcl::PointIndices);
	// Create the segmentation object
	pcl::SACSegmentation<pcl::PointXYZRGBA> seg;
	
	seg.setOptimizeCoefficients (true);
	// Mandatory
	seg.setModelType (pcl::SACMODEL_PLANE);
	seg.setMethodType (pcl::SAC_RANSAC);
	seg.setDistanceThreshold (0.02);

	seg.setInputCloud (new_tgt_pnts_cl);
	seg.segment (*inliers, *coefficients);
	
	if (inliers->indices.size () == 0)
	{
		PCL_ERROR ("Could not estimate a planar model for the given dataset.");
		return false;
	}
	
	plane_normal[0] = coefficients->values[0];
	plane_normal[1] = coefficients->values[1];
	plane_normal[2] = coefficients->values[2];
	/**********PRINT PLANE INFO************/
	//~ std::cerr << "Model coefficients: " << coefficients->values[0] << " " 
										//~ << coefficients->values[1] << " "
										//~ << coefficients->values[2] << " " 
										//~ << coefficients->values[3] << std::endl;
	//~ 
	//~ std::cerr << "Model inliers: " << inliers->indices.size () << std::endl;
	//~ for (size_t i = 0; i < inliers->indices.size (); ++i)
	//~ std::cerr << inliers->indices[i] << "    " << new_tgt_pnts_cl->points[inliers->indices[i]].x << " "
											   //~ << new_tgt_pnts_cl->points[inliers->indices[i]].y << " "
											   //~ << new_tgt_pnts_cl->points[inliers->indices[i]].z << std::endl;
	return true;
}

void Environment::loadTargetObjPartialModel(int model_index, bool isCW)
{	
	
}

void Environment::drawNextModelPcl()
{
	// draw next model point cloud
	glBegin(GL_POINTS);
		for (size_t cnt=0; cnt < next_model_pcl->points.size(); ++cnt )
		{
			float red = next_model_pcl->points[cnt].r/(255.0);
			float green = next_model_pcl->points[cnt].g/(255.0);
			float blue = next_model_pcl->points[cnt].b/(255.0);
				  
			glColor3f(red, green, blue);
			glVertex3f(next_model_pcl->points[cnt].x,
					   next_model_pcl->points[cnt].y,
					   next_model_pcl->points[cnt].z);
		}
	glEnd();
}

bool Environment::loadPartialModelImage(int model_ind)
{
	/* ************************************************
	* Load images corresponding to partial model
	* ************************************************/ 
	char image_name[1024];
	sprintf(image_name, 
			"%s/images/object_point_cloud_in_scene_%03d_organized.png",
			parameters.partial_model_path.c_str(),
			model_ind);
	texture[1] = SOIL_load_OGL_texture // load an image file directly as a new OpenGL texture
			(
				image_name,
				SOIL_LOAD_AUTO,
				SOIL_CREATE_NEW_ID,
				SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
			);
	if( 0 == texture[1] )
	{
		printf( "SOIL loading error: '%s'\n", SOIL_last_result() );
		return false;
	}
	
    return true;
}

void Environment::drawPartialModelImageInWindowCorner()
{
	// allocate a texture name
	int curent_viewport[4] = {0};
	glGetIntegerv(GL_VIEWPORT,curent_viewport);
	glViewport(0.0*curent_viewport[2]/4.0, 
			   3.0*curent_viewport[3]/4.0,
			   curent_viewport[2]/4.0,
			   curent_viewport[3]/4.0);	
	
	// reset modelview and projection matrix
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();
	
	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	
	glEnable(GL_TEXTURE_2D); 
	glEnable(GL_LIGHTING);
	//~ glDisable(GL_CULL_FACE);
	glBindTexture(GL_TEXTURE_2D, texture[1]);
	glColor3f(1.0f, 1.0f, 1.0f);
	
	// draw textured quad
	glBegin(GL_QUADS);
		glTexCoord2f(0.0f, 0.0f); glVertex2f(-1.0f, -1.0f); // only 2f will draw at corner of viewing window
		glTexCoord2f(1.0f, 0.0f); glVertex2f( 1.0f, -1.0f); // 3f will draw in 3d space
		glTexCoord2f(1.0f, 1.0f); glVertex2f( 1.0f,  1.0f);
		glTexCoord2f(0.0f, 1.0f); glVertex2f(-1.0f,  1.0f);
	glEnd();
	
	// draw small window boundary frame
	//~ glLineWidth(22.5); 
	//~ glColor3f(1.0, 0.0, 0.0);
	//~ glBegin(GL_LINES);
		//~ glVertex2f(-1.0f, -1.0f);
		//~ glVertex2f(1.0f, -1.0f);
		//~ glVertex2f(1.0f, -1.0f);
		//~ glVertex2f(1.0f, 1.0f);
	//~ glEnd();
	//~ glLineWidth(2.5); 
			
	glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    glDisable(GL_LIGHTING);
    glDisable(GL_TEXTURE_2D); 
    //~ glEnable(GL_CULL_FACE);
    
    glPopMatrix();
    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();
    
    glViewport(curent_viewport[0],curent_viewport[1], curent_viewport[2], curent_viewport[3]);
    
}

void Environment::drawSpecifiedPointCloud(int pcl_ind)
{
	glBegin(GL_POINTS);
		for (size_t cnt=0; cnt < point_clouds_[pcl_ind]->points.size(); ++cnt )
		{
			float red = point_clouds_[pcl_ind]->points[cnt].r/(255.0);
			float green = point_clouds_[pcl_ind]->points[cnt].g/(255.0);
			float blue = point_clouds_[pcl_ind]->points[cnt].b/(255.0);
			
			float pnt_temp[3] ={point_clouds_[pcl_ind]->points[cnt].x,
								point_clouds_[pcl_ind]->points[cnt].y,
								point_clouds_[pcl_ind]->points[cnt].z};	 
			glColor3f(red, green, blue); 
			if (pnt_temp[2] < 0.5)
			{		
				glVertex3f(pnt_temp[0], pnt_temp[1], pnt_temp[2]);
			}
			else 
			{
				glVertex3f(pnt_temp[0]+1.5, pnt_temp[1]+0.0, pnt_temp[2]+0.6);
			}
		}
	glEnd();
}

fetching_unknown_objects_test_case_parameters::fetching_unknown_objects_test_case_parameters()
{
}

void fetching_unknown_objects_test_case_parameters::initialize()
{
	arm_base_position.resize(3);
	arm_base_x.resize(3);
	arm_base_y.resize(3);
	arm_base_z.resize(3);
	arm_starting_config.resize(9);
	
	env_cloud_position_adjuster_1.resize(3);
	env_cloud_position_adjuster_2.resize(3);
	env_cloud_position_adjuster_3.resize(3);
	env_cloud_position_adjuster_4.resize(3);
	
	line_motion_direction.resize(3);
	
	partial_model_position_adjusters.resize(12);
}
