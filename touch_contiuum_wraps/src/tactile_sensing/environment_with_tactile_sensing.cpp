/*
 * environment_with_tactile_sensing.cpp
 * 
 * Copyright 2016 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include "environment_with_tactile_sensing.h"

environment_with_tactile_sensing::environment_with_tactile_sensing(int n_secs, 
                                                                   std::vector<float> init_config,
                                                                   Eigen::Matrix4f init_base_frame,
                                                                   string mesh_path,
                                                                   string mesh_env_path,
                                                                   Eigen::Matrix4f mesh_tf,
                                                                   Eigen::Matrix4f mesh_env_tf,
                                                                   std::vector<Eigen::Vector3f> ver_list,
                                                                   Eigen::Vector3f cross_sec_normal)
: mani(n_secs, init_config, init_base_frame), mesh(mesh_path, mesh_tf), mesh_env(mesh_env_path, mesh_env_tf),
cross_sec(ver_list, cross_sec_normal), inflated_cross_sec(ver_list, cross_sec_normal),
env_cross_sec(ver_list, cross_sec_normal)
{
    // test case parameters
    sec_curvature_adjust_rate = 0.1;
    sec_len_adjust_rate = 0.0001;
    sec_endpnt_moving_dist = 0.03;
    angle_tip_rot_threshold = 4.7;
    
    // initialize control flags
    arm_physical_limit_warning = false;
    arm_ik_not_found_warning = false;
    enough_tip_z_axis_rot_flag = false;
    final_sequeeze_finished_flag = false;
    empty_cross_sec_flag = false;

    num_of_screenshots = 0;
    
    // initialize arm tip frame
    std::vector<float> init_arm_tip_frame(16,0.0);
    mani.compute_section_frame_wrt_world(init_arm_tip_frame, 2);
    Eigen::Vector4f v1(init_arm_tip_frame[0],init_arm_tip_frame[4],init_arm_tip_frame[8],init_arm_tip_frame[12]);
    Eigen::Vector4f v2(init_arm_tip_frame[1],init_arm_tip_frame[5],init_arm_tip_frame[9],init_arm_tip_frame[13]);
    Eigen::Vector4f v3(init_arm_tip_frame[2],init_arm_tip_frame[6],init_arm_tip_frame[10],init_arm_tip_frame[14]);
    Eigen::Vector4f v4(init_arm_tip_frame[3],init_arm_tip_frame[7],init_arm_tip_frame[11],init_arm_tip_frame[15]);
    arm_init_tip_frame.row(0) = v1;
    arm_init_tip_frame.row(1) = v2;
    arm_init_tip_frame.row(2) = v3;
    arm_init_tip_frame.row(3) = v4;

    /* initialize subscribers */
    robot_state_sub = ros::NodeHandle().subscribe("/origami_robot_state", 1000, &environment_with_tactile_sensing::robotStateCallBack, this);
    contact_state_sub = ros::NodeHandle().subscribe("/origami_contact_state", 1000, &environment_with_tactile_sensing::contactStateCallBack, this);

    motion_cmd_pub = ros::NodeHandle().advertise<std_msgs::Float32MultiArray>("origami_motion_cmds", 1000);

    section_cnt_state.resize(mani.get_n_sections(), 0);
}

void environment_with_tactile_sensing::robotStateCallBack(const std_msgs::Float32MultiArray::ConstPtr& msg)
{
    std::cout << "Recieved Origami robot state" << std::endl;
    for (int i=0; i<msg->data.size(); ++i)
        std::cout << msg->data[i] << " ";
    std::cout << std::endl;
}

void environment_with_tactile_sensing::contactStateCallBack(const std_msgs::Int32MultiArray::ConstPtr& msg)
{
    std::cout << "Received Origami contact state" << std::endl;
    for (int i=0; i<msg->data.size(); ++i)
        std::cout << msg->data[i] << " ";
    std::cout << std::endl;
    if (msg->data.size() != mani.get_n_sections())
        ROS_ERROR("Contact state size not matching number of sections.");
    else
    {
        section_cnt_state = msg->data;
        cnt_history.push_back(section_cnt_state);
    }
}

bool environment_with_tactile_sensing::armSectionPolygonCollisionCheck(Face &InterPolygon,
                                                                       int SectionIdx,
                                                                       int &colliding_edge_ind)
{
    /**********************************************
     ********* Section Circle Information**********
     **********************************************/
    float s = mani.get_config()[(SectionIdx)*3+0];
    float kappa = mani.get_config()[(SectionIdx)*3+1];
    float phi = mani.get_config()[(SectionIdx)*3+2];
    float radius = 1.0/(std::abs(kappa));
    float RotaAboutZ[16] = {cos(phi),sin(phi),0,0, -sin(phi),cos(phi),0,0, 0,0,1.0,0, 0,0,0,1.0};
    std::vector<float> ForwardTransMat = mani.compute_trans_matrix_wrt_world();
    float end_s[3] = {0};
    float end_e[3] = {0};
    float z[3] = {0};
    if(SectionIdx == 0)
    {
        for(int i= 0; i < 3; ++i)
        {
            end_s[i] = 0.0;
            end_e[i] = ForwardTransMat[SectionIdx*16+12+i];
            
        }
        #ifdef COLLISIONCASE
        printf("computed z sec 1 is (%f,%f,%f)\n", mani.base_trans_matrix[8],  mani.base_trans_matrix[9],  mani.base_trans_matrix[10]);
        #endif
        z[0] = mani.base_trans_matrix[8];
        z[1] = mani.base_trans_matrix[9];
        z[2] = mani.base_trans_matrix[10];
    }
    else
    {
        for(int i= 0; i < 3; ++i)
        {
            end_s[i] = ForwardTransMat[(SectionIdx-1)*16+12+i];
            end_e[i] = ForwardTransMat[SectionIdx*16+12+i];
            z[i] = ForwardTransMat[(SectionIdx-1)*16+8+i];
        }
    }
    #ifdef COLLISIONCASE
    printf("section start pnt is (%f,%f,%f)\n", end_s[0], end_s[1], end_s[2]);
    printf("section end pnt is (%f,%f,%f)\n", end_e[0], end_e[1], end_e[2]);
    #endif
    

    // section circle centers
    std::vector<float> Centers(3*mani.get_n_sections(), 0.0);
    mani.compute_section_cir_centers_wrt_world(Centers);
    float c[3] = {Centers[SectionIdx*3+0], Centers[SectionIdx*3+1], Centers[SectionIdx*3+2]};
    float n[3] = {ForwardTransMat[SectionIdx*16+4+0], ForwardTransMat[SectionIdx*16+4+1], ForwardTransMat[SectionIdx*16+4+2]};
    Normalize(z,3);
    Normalize(n,3);
    
    float detaR = mani.get_sec_width();
    
    #ifdef DEBUG
    printf("end_s is (%f,%f,%f)\n", end_s[0], end_s[1], end_s[2]);
    printf("end_e is (%f,%f,%f)\n", end_e[0], end_e[1], end_e[2]);
    printf("center is (%f,%f,%f)\n", c[0], c[1], c[2]);
    printf("normal is (%f,%f,%f)\n", n[0], n[1], n[2]);
    printf("section width is %f\n", detaR);
    #endif
    
    /*******************************************
     ***************Collision Check*************
     *******************************************/
    bool collision = false;
    if(std::abs(kappa)<1e-3) // straight line section
    {  
        for(int i = 0; i < (InterPolygon.face_edges.size()); ++i)
          //do two line segment intersection
        {   
            float x1 = InterPolygon.face_edges[i].pnt1[0];
            float y1 = InterPolygon.face_edges[i].pnt1[1];
            float z1 = InterPolygon.face_edges[i].pnt1[2];
            float x2 = InterPolygon.face_edges[i].pnt2[0];
            float y2 = InterPolygon.face_edges[i].pnt2[1];
            float z2 = InterPolygon.face_edges[i].pnt2[2];
            
            float Q1[3] = {x1,y1,z1};
            float Q2[3] = {x2,y2,z2};
            float InterP[5] = {0};
            
             if(TwolineIntersect(end_s,end_e,Q1,Q2,InterP))
             {
                 if(InterP[3]>0 && InterP[3]<1 && InterP[4]>0 && InterP[4]<1)
                 {
                     colliding_edge_ind = i; // note down the colliding edge index
                     return true;// There is a collision
                 }
                 else
                 {
                     collision = false;// No collision
                     continue;
                 }
             }
             else
             {
                 collision = false;// No collision
                 continue;
             }
        }
        return collision;
    }
    else
    {
        #ifdef DEBUG
        printf("Enter non-line section %d check\n", SectionIdx+1);
        printf("polygon has %lu edges\n", InterPolygon.face_edges.size());
        #endif
        
        for(int i = 0; i < (InterPolygon.face_edges.size()); ++i)
        {
            float x1 = InterPolygon.face_edges[i].pnt1[0];
            float y1 = InterPolygon.face_edges[i].pnt1[1];
            float z1 = InterPolygon.face_edges[i].pnt1[2];
            float x2 = InterPolygon.face_edges[i].pnt2[0];
            float y2 = InterPolygon.face_edges[i].pnt2[1];
            float z2 = InterPolygon.face_edges[i].pnt2[2];
            
            float Q1[3] = {x1,y1,z1};
            float Q2[3] = {x2,y2,z2};
            float InterP1[5] = {0};
            float InterP2[5] = {0};

            float r1 = sqrt((x1-c[0])*(x1-c[0]) + (y1-c[1])*(y1-c[1]) + (z1-c[2])*(z1-c[2]));
            float r2 = sqrt((x2-c[0])*(x2-c[0]) + (y2-c[1])*(y2-c[1]) + (z2-c[2])*(z2-c[2]));
            float r = 1.0/abs(kappa);

            float EndS_c[3] = {end_s[0] - c[0], end_s[1] - c[1], end_s[2] - c[2]};
            float vector_v1_c[3] = {x1-c[0], y1 - c[1], z1 - c[2]};
            Normalize(vector_v1_c,3);
            Normalize(EndS_c,3);

            float z_sign_v1_c = 0;      
            float cos_Theta_v1 = 0; //cos(theta)
            float Theta_v1 = 0;//theta
            DotProduct(vector_v1_c,z,&z_sign_v1_c);
            DotProduct(vector_v1_c,EndS_c,&cos_Theta_v1);
            if(z_sign_v1_c>=0)
            {
                Theta_v1 = acos(cos_Theta_v1);
            }
            else
            {
                Theta_v1 = 2.0*M_PI - acos(cos_Theta_v1);
            }

            float vector_v2_c[3] = {x2-c[0], y2 - c[1], z2 - c[2]};
            Normalize(vector_v2_c,3);

            float z_sign_v2_c = 0;
            float cos_Theta_v2 = 0;
            float Theta_v2 = 0;
            DotProduct(vector_v2_c,z,&z_sign_v2_c);
            DotProduct(vector_v2_c,EndS_c,&cos_Theta_v2);
            
            #ifdef COLLISIONCASE
            printf("vector_v2_c is (%f,%f,%f)\n", vector_v2_c[0], vector_v2_c[1], vector_v2_c[2]);
            printf("z is (%f,%f,%f)\n", z[0], z[1], z[2]);
            #endif
            
            if(z_sign_v2_c>=0)
            {
                #ifdef COLLISIONCASE
                printf("z sign v2 greater than 0\n");
                #endif
                Theta_v2 = acos(cos_Theta_v2);
            }
            else
            {
                #ifdef COLLISIONCASE
                printf("z sign v2 less than 0\n");
                #endif
                Theta_v2 = 2.0*M_PI - acos(cos_Theta_v2);
            }
            
            #ifdef COLLISIONCASE
            printf("sec circle center is (%f,%f,%f)\n", c[0], c[1], c[2]);
            printf("r1 is %f\n", r1);
            printf("r2 is %f\n", r2);
            printf("r - detaR is %f\n", r-detaR);
            printf("r + detaR is %f\n", r+detaR);   
            printf("edge %d has pnt (%f,%f,%f)\n", i, x1, y1, z1);
            printf("edge %d has pnt (%f,%f,%f)\n", i, x2, y2, z2);
            #endif
            
            if( (r1>r2 ?r1 :r2) < (r-detaR))
            {
                collision = false;// No collision
                continue;
            }
            else 
            {
                //Case 2 eighter (p1,theta1) or (p2,theta2) satisfies both radius and angular range
                if(r1>=(r-detaR) && r1<=(r+detaR))
                {
                   if(Theta_v1 <= s/r)
                   {
                       collision = true;
                       #ifdef COLLISIONCASE
                       printf("Case 2A return\n");
                       #endif
                       colliding_edge_ind = i; // note down the colliding edge index
                       return collision;// There is a collision
                   }

                }

                if(r2>=r-detaR && r2<=r+detaR)
                {
                   //~ printf("theta v2 is %f\n",Theta_v2);
                   if(Theta_v2 <= s/r)
                   {
                       collision = true;
                       #ifdef COLLISIONCASE
                       printf("Case 2B return\n");
                       #endif
                       colliding_edge_ind = i; // note down the colliding edge index
                       return collision;// There is a collision
                   }
                }


                // Case 3 where check the distance between v1v2 and the center of circle
                if((r1<r2 ?r1 :r2) > (r+ detaR))
                {       
                    // compute the distance from center of circle to the line that v1v2 lie on
                    float d_p12 = (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2);
                    float t = -((x1 - c[0])*(x2 - x1) + (y1 - c[1])*(y2 - y1) + (z1 - c[2])*(z2 - z1))/(d_p12);

                    // if the minimun distance is within the range of segment v1v2 
                    if(t>0 && t<1)
                    {
                        // compute Vmin, the point on v1v2 that has the mininum distance to the center of circle
                        float x_min = x1+t*(x2-x1);
                        float y_min = y1+t*(y2-y1);
                        float z_min = z1+t*(z2-z1);
                        float Vmin_c[3] = {x_min - c[0], y_min - c[1], z_min - c[2]};
                        Normalize(Vmin_c,3);

                        // checking if Vmin within the angular range
                        float z_sign_Vmin_c = 0;        
                        float cos_Theta_Vmin_c = 0; //cos(theta)
                        float Theta_Vmin_c = 0;//theta
                        DotProduct(Vmin_c,z,&z_sign_Vmin_c);
                        DotProduct(Vmin_c,EndS_c,&cos_Theta_Vmin_c);
                        if(z_sign_Vmin_c >= 0)
                        {
                            Theta_Vmin_c = acos(cos_Theta_Vmin_c);
                        }
                        else
                        {
                            Theta_Vmin_c = 2.0*M_PI - acos(cos_Theta_Vmin_c);
                        }                       
                        float min_D = sqrt((x_min-c[0])*(x_min-c[0]) + (y_min-c[1])*(y_min-c[1]) + (z_min-c[2])*(z_min-c[2]));

                        // if larger that the width no collision
                        if(min_D > (r + detaR))
                        {
                            collision = false;// No collision
                            continue;
                            #ifdef COLLISIONCASE
                            printf("continue 1\n");
                            #endif
                                                    
                        }
                        // if within the range of section i; return collision
                        else if((Theta_Vmin_c <= s/r))
                        {
                            collision = true;
                            #ifdef COLLISIONCASE
                            printf("Case 3A return\n");
                            #endif
                            colliding_edge_ind = i; // note down the colliding edge index
                            return collision;// There is a collision
                        }
                        // if not within the range of section i;  no collision
                        else 
                        {
                            collision = false;
                            continue;
                            #ifdef COLLISIONCASE
                            printf("continue 2\n");
                            #endif
                        }
                
                    }
                    else 
                    {
                        float theta_tmp = ((r1 <= r2)?Theta_v1:Theta_v2);
                        float r_tmp = ((theta_tmp == Theta_v1)?r1:r2);
                        if(theta_tmp<=s/r && r_tmp >= r - detaR && r_tmp <= r + detaR)
                        {
                            #ifdef COLLISIONCASE
                            printf("Case 3B return\n");
                            #endif
                            colliding_edge_ind = i; // note down the colliding edge index
                            return true;
                        }
                        else
                        {
                            collision = false;
                            continue;
                            #ifdef COLLISIONCASE
                            printf("continue 3\n");
                            #endif
                        }
                    }
                }

                // Case 4 theta1 and theta2 satisfy the angular range
                if(Theta_v2 <= s/r && Theta_v1 <= s/r)
                {
                    collision = true;
                    #ifdef COLLISIONCASE
                    printf("Case 4 return\n");
                    #endif
                    colliding_edge_ind = i; // note down the colliding edge index
                    return collision;
                }

                // Case 5 if there is a intersection between line v1v2 and L_ik  
                if(TwolineIntersect(c,end_e,Q1,Q2,InterP1)/*&&!TwolineIntersect(c,end_s,Q1,Q2,InterP2)*/) // intersect only L1
                {
                    float Inter1_r = sqrt((InterP1[0] - c[0])*(InterP1[0] - c[0]) + (InterP1[1] - c[1])*(InterP1[1] - c[1]) + (InterP1[2] - c[2])*(InterP1[2] - c[2]));
                    
                    if(abs(Inter1_r) <=0.0001)// interceted at the center of circle
                    {
                        float Smallertheta = ((Theta_v1 <= Theta_v2)? Theta_v1:Theta_v2);
                        if(Smallertheta <= s/r)
                        {
                            #ifdef COLLISIONCASE
                            printf("Case 5 return\n");
                            #endif
                            collision = true;
                            colliding_edge_ind = i; // note down the colliding edge index
                            return collision;
                        }
                    }

                    //Case 5.1 colinear with L2 , return collision = true (cos(theta) = 1 or -1)    
                    float CosThetaV1_2_L2 = 0;
                    float V1_2[3] = {x1-x2, y1-y2, z1-z2}; // vector v1 to v2
                    float VL2[3] = {end_s[0] - c[0], end_s[1] - c[1], end_s[2] - c[2]};// L2
                    Normalize(V1_2,3);
                    Normalize(VL2,3);
                    DotProduct(V1_2,VL2,&CosThetaV1_2_L2);   
                    if(abs(CosThetaV1_2_L2 - 1) <= 0.0001 || abs(CosThetaV1_2_L2 + 1) <= 0.0001)
                    {
                        #ifdef COLLISIONCASE
                        printf("Case 5.1-C return\n");
                        #endif
                        collision = true;
                        colliding_edge_ind = i; // note down the colliding edge index
                        return collision;
                    }

                    //Case 5.2 the intersection with L1 is within the radius bound
                    if((r - detaR <= Inter1_r) && (Inter1_r<= r + detaR))
                    {
                        collision = true;
                        #ifdef COLLISIONCASE
                        printf("Case 5.2-C return\n");
                        #endif
                        colliding_edge_ind = i; // note down the colliding edge index
                        return collision;
                    }

                    //Case 5.4, the intersection point above the upper bound of radius
                    if((Inter1_r >= r + detaR))
                    {
                        float SmallTheta = (r1 <= r2 ? Theta_v1:Theta_v2); // choose the smaller vertex theta
                        if(SmallTheta <= s/r)
                        {
                            collision = true;
                            #ifdef COLLISIONCASE
                            printf("Case 5.4-C return\n");
                            #endif
                            colliding_edge_ind = i; // note down the colliding edge index
                            return collision;
                        }
                    }
                    // Case 5.5, the intersection point below the lower bound of radius
                    if((Inter1_r <= r - detaR))
                    {
                        float LargeTheta = (r1 <= r2 ? Theta_v2:Theta_v1); // choose the larger vertex theta
                        if(LargeTheta <= s/r)
                        {
                            collision = true;
                            #ifdef COLLISIONCASE
                            printf("Case 5.5-C return\n");
                            #endif
                            colliding_edge_ind = i; // note down the colliding edge index
                            return collision;
                        }
                    }
                }
                else if(/*!TwolineIntersect(c,end_e,Q1,Q2,InterP1)&&*/TwolineIntersect(c,end_s,Q1,Q2,InterP2)) // intersect L2
                {
                    float Inter2_r = sqrt((InterP2[0] - c[0])*(InterP2[0] - c[0]) + (InterP2[1] - c[1])*(InterP2[1] - c[1]) + (InterP2[2] - c[2])*(InterP2[2] - c[2]));
                    
                    if(abs(Inter2_r) <=0.0001)// interceted at the center of circle
                    {
                        float Smallertheta = ((Theta_v1 <= Theta_v2)? Theta_v1:Theta_v2);
                        if(Smallertheta <= s/r)
                        {
                            collision = true;
                            #ifdef COLLISIONCASE
                            printf("Case 5.6-C return\n");
                            #endif
                            colliding_edge_ind = i; // note down the colliding edge index
                            return collision;
                        }
                    }
                    //Case 5.1 Check if colinear with L1 , return collision = true if cos(theta) = 1 or -1.
                    float CosThetaV1_2_L1 = 0;
                    float V1_2[3] = {x1-x2, y1-y2, z1-z2}; // vector v1 to v2
                    float VL1[3] = {end_e[0] - c[0], end_e[1] - c[1], end_e[2] - c[2]};// L1
                    Normalize(V1_2,3);
                    Normalize(VL1,3);
                    DotProduct(V1_2,VL1,&CosThetaV1_2_L1);  

                    if(abs(CosThetaV1_2_L1 - 1) <= 0.001 || abs(CosThetaV1_2_L1 + 1) <= 0.001)
                    {
                        collision = true;
                        #ifdef COLLISIONCASE
                        printf("Collision case 5.1 occurred\n");
                        #endif
                        colliding_edge_ind = i; // note down the colliding edge index
                        return collision;
                    }

                    //Case 5.2 the intersection with L1 is within the radius bound
                    if((r - detaR <= Inter2_r) && (Inter2_r<= r + detaR))
                    {
                        collision = true;
                        #ifdef COLLISIONCASE
                        printf("Collision case 5.2 occurred\n");
                        #endif
                        colliding_edge_ind = i; // note down the colliding edge index
                        return collision;
                    }

                    //Case 5.4, the intersection point above the upper bound of radius
                    if((Inter2_r >= r + detaR))
                    {
                        float SmallTheta = (r1 <= r2 ? Theta_v1:Theta_v2); // choose the smaller vertex theta
                        if(SmallTheta <= s/r)
                        {
                            collision = true;
                            #ifdef COLLISIONCASE
                            printf("Collision case 5.4 occurred\n");
                            #endif
                            colliding_edge_ind = i; // note down the colliding edge index
                            return collision;
                        }
                    }
                    // Case 5.5, the intersection point below the lower bound of radius
                    if((Inter2_r <= r - detaR))
                    {
                        float LargeTheta = (r1 <= r2 ? Theta_v2:Theta_v1); // choose the larger vertex theta
                        if(LargeTheta <= s/r)
                        {
                            collision = true;
                            #ifdef COLLISIONCASE
                            printf("Collision case 5.5 occurred\n");
                            #endif
                            colliding_edge_ind = i; // note down the colliding edge index
                            return collision;
                        }
                    }
                }
                else if(TwolineIntersect(c,end_e,Q1,Q2,InterP1)&&TwolineIntersect(c,end_s,Q1,Q2,InterP2)) // intersect Both L1 and L2
                {
                    //Case 5.3
                    float Inter1_r = sqrt((InterP1[0] - c[0])*(InterP1[0] - c[0]) + (InterP1[1] - c[1])*(InterP1[1] - c[1]) + (InterP1[2] - c[2])*(InterP1[2] - c[2]));
                    float Inter2_r = sqrt((InterP2[0] - c[0])*(InterP2[0] - c[0]) + (InterP2[1] - c[1])*(InterP2[1] - c[1]) + (InterP2[2] - c[2])*(InterP2[2] - c[2]));
                    if((Inter1_r >= r + detaR && Inter2_r <= r - detaR) ||(Inter2_r >= r + detaR && Inter1_r <= r - detaR) )
                    {
                        collision = true;
                        #ifdef COLLISIONCASE
                        printf("Collision case 5.3 occurred\n");
                        #endif
                        colliding_edge_ind = i; // note down the colliding edge index
                        return collision;
                    }
                }
                else
                {
                    collision = false;
                }
            } // end if-else
        } // end for
        return collision;    
    }
}

bool environment_with_tactile_sensing::armPolygonCollisionCheck(Face &cross_sec, std::vector<bool> &colliding_sec_status, std::vector<int> &sec_colliding_edge_inds)
{
    int num_of_secs = mani.get_n_sections();
    colliding_sec_status = std::vector<bool>(num_of_secs, false);
    for (int i=0; i<num_of_secs; ++i)
    {
        if(armSectionPolygonCollisionCheck(cross_sec,i,sec_colliding_edge_inds[i]))
        {
            colliding_sec_status[i] = true;
        }
    }

    for (int i=0; i<num_of_secs; ++i)
    {
        if (colliding_sec_status[i])
            return true;
    }
    return false;
}

bool environment_with_tactile_sensing::armSectionPolygonFullCollisionCheck(Face &InterPolygon,
                                                                           int SectionIdx,
                                                                           std::vector<int> &colliding_edge_inds)
{
    /**********************************************
     ********* Section Circle Information**********
     **********************************************/
    float s = mani.get_config()[(SectionIdx)*3+0];
    float kappa = mani.get_config()[(SectionIdx)*3+1];
    float phi = mani.get_config()[(SectionIdx)*3+2];
    float radius = 1.0/(std::abs(kappa));
    float RotaAboutZ[16] = {cos(phi),sin(phi),0,0, -sin(phi),cos(phi),0,0, 0,0,1.0,0, 0,0,0,1.0};
    std::vector<float> ForwardTransMat = mani.compute_trans_matrix_wrt_world();
    float end_s[3] = {0};
    float end_e[3] = {0};
    float z[3] = {0};
    if(SectionIdx == 0)
    {
        for(int i= 0; i < 3; ++i)
        {
            end_s[i] = 0.0;
            end_e[i] = ForwardTransMat[SectionIdx*16+12+i];
            
        }
        #ifdef COLLISIONCASE
        printf("computed z sec 1 is (%f,%f,%f)\n", mani.base_trans_matrix[8],  mani.base_trans_matrix[9],  mani.base_trans_matrix[10]);
        #endif
        z[0] = mani.base_trans_matrix[8];
        z[1] = mani.base_trans_matrix[9];
        z[2] = mani.base_trans_matrix[10];
    }
    else
    {
        for(int i= 0; i < 3; ++i)
        {
            end_s[i] = ForwardTransMat[(SectionIdx-1)*16+12+i];
            end_e[i] = ForwardTransMat[SectionIdx*16+12+i];
            z[i] = ForwardTransMat[(SectionIdx-1)*16+8+i];
        }
    }
    #ifdef COLLISIONCASE
    printf("section start pnt is (%f,%f,%f)\n", end_s[0], end_s[1], end_s[2]);
    printf("section end pnt is (%f,%f,%f)\n", end_e[0], end_e[1], end_e[2]);
    #endif
    

    // section circle centers
    std::vector<float> Centers(3*mani.get_n_sections(), 0.0);
    mani.compute_section_cir_centers_wrt_world(Centers);
    float c[3] = {Centers[SectionIdx*3+0], Centers[SectionIdx*3+1], Centers[SectionIdx*3+2]};
    float n[3] = {ForwardTransMat[SectionIdx*16+4+0], ForwardTransMat[SectionIdx*16+4+1], ForwardTransMat[SectionIdx*16+4+2]};
    Normalize(z,3);
    Normalize(n,3);
    
    float detaR = mani.get_sec_width();
    
    #ifdef DEBUG
    printf("end_s is (%f,%f,%f)\n", end_s[0], end_s[1], end_s[2]);
    printf("end_e is (%f,%f,%f)\n", end_e[0], end_e[1], end_e[2]);
    printf("center is (%f,%f,%f)\n", c[0], c[1], c[2]);
    printf("normal is (%f,%f,%f)\n", n[0], n[1], n[2]);
    printf("section width is %f\n", detaR);
    #endif
    
    /*******************************************
     ***************Collision Check*************
     *******************************************/
    bool collision = false;
    if(std::abs(kappa)<1e-3) // straight line section
    {  
        for(int i = 0; i < (InterPolygon.face_edges.size()); ++i)
          //do two line segment intersection
        {   
            float x1 = InterPolygon.face_edges[i].pnt1[0];
            float y1 = InterPolygon.face_edges[i].pnt1[1];
            float z1 = InterPolygon.face_edges[i].pnt1[2];
            float x2 = InterPolygon.face_edges[i].pnt2[0];
            float y2 = InterPolygon.face_edges[i].pnt2[1];
            float z2 = InterPolygon.face_edges[i].pnt2[2];
            
            float Q1[3] = {x1,y1,z1};
            float Q2[3] = {x2,y2,z2};
            float InterP[5] = {0};
            
             if(TwolineIntersect(end_s,end_e,Q1,Q2,InterP))
             {
                 if(InterP[3]>0 && InterP[3]<1 && InterP[4]>0 && InterP[4]<1)
                 {
                     colliding_edge_inds.push_back(i); // note down the colliding edge index
                     collision = true;
                     continue;
                 }
                 else
                 {
                     collision = false;// No collision
                     continue;
                 }
             }
             else
             {
                 collision = false;// No collision
                 continue;
             }
        }
        return collision;
    }
    else
    {
        #ifdef DEBUG
        printf("Enter non-line section %d check\n", SectionIdx+1);
        printf("polygon has %lu edges\n", InterPolygon.face_edges.size());
        #endif
        
        for(int i = 0; i < (InterPolygon.face_edges.size()); ++i)
        {
            float x1 = InterPolygon.face_edges[i].pnt1[0];
            float y1 = InterPolygon.face_edges[i].pnt1[1];
            float z1 = InterPolygon.face_edges[i].pnt1[2];
            float x2 = InterPolygon.face_edges[i].pnt2[0];
            float y2 = InterPolygon.face_edges[i].pnt2[1];
            float z2 = InterPolygon.face_edges[i].pnt2[2];
            
            float Q1[3] = {x1,y1,z1};
            float Q2[3] = {x2,y2,z2};
            float InterP1[5] = {0};
            float InterP2[5] = {0};

            float r1 = sqrt((x1-c[0])*(x1-c[0]) + (y1-c[1])*(y1-c[1]) + (z1-c[2])*(z1-c[2]));
            float r2 = sqrt((x2-c[0])*(x2-c[0]) + (y2-c[1])*(y2-c[1]) + (z2-c[2])*(z2-c[2]));
            float r = 1.0/abs(kappa);

            float EndS_c[3] = {end_s[0] - c[0], end_s[1] - c[1], end_s[2] - c[2]};
            float vector_v1_c[3] = {x1-c[0], y1 - c[1], z1 - c[2]};
            Normalize(vector_v1_c,3);
            Normalize(EndS_c,3);

            float z_sign_v1_c = 0;      
            float cos_Theta_v1 = 0; //cos(theta)
            float Theta_v1 = 0;//theta
            DotProduct(vector_v1_c,z,&z_sign_v1_c);
            DotProduct(vector_v1_c,EndS_c,&cos_Theta_v1);
            if(z_sign_v1_c>=0)
            {
                Theta_v1 = acos(cos_Theta_v1);
            }
            else
            {
                Theta_v1 = 2.0*M_PI - acos(cos_Theta_v1);
            }

            float vector_v2_c[3] = {x2-c[0], y2 - c[1], z2 - c[2]};
            Normalize(vector_v2_c,3);

            float z_sign_v2_c = 0;
            float cos_Theta_v2 = 0;
            float Theta_v2 = 0;
            DotProduct(vector_v2_c,z,&z_sign_v2_c);
            DotProduct(vector_v2_c,EndS_c,&cos_Theta_v2);
            
            #ifdef COLLISIONCASE
            printf("vector_v2_c is (%f,%f,%f)\n", vector_v2_c[0], vector_v2_c[1], vector_v2_c[2]);
            printf("z is (%f,%f,%f)\n", z[0], z[1], z[2]);
            #endif
            
            if(z_sign_v2_c>=0)
            {
                #ifdef COLLISIONCASE
                printf("z sign v2 greater than 0\n");
                #endif
                Theta_v2 = acos(cos_Theta_v2);
            }
            else
            {
                #ifdef COLLISIONCASE
                printf("z sign v2 less than 0\n");
                #endif
                Theta_v2 = 2.0*M_PI - acos(cos_Theta_v2);
            }
            
            #ifdef COLLISIONCASE
            printf("sec circle center is (%f,%f,%f)\n", c[0], c[1], c[2]);
            printf("r1 is %f\n", r1);
            printf("r2 is %f\n", r2);
            printf("r - detaR is %f\n", r-detaR);
            printf("r + detaR is %f\n", r+detaR);   
            printf("edge %d has pnt (%f,%f,%f)\n", i, x1, y1, z1);
            printf("edge %d has pnt (%f,%f,%f)\n", i, x2, y2, z2);
            #endif
            
            if( (r1>r2 ?r1 :r2) < (r-detaR))
            {
                collision = false;// No collision
                continue;
            }
            else 
            {
                //Case 2 eighter (p1,theta1) or (p2,theta2) satisfies both radius and angular range
                if(r1>=(r-detaR) && r1<=(r+detaR))
                {
                   if(Theta_v1 <= s/r)
                   {
                       collision = true;
                       #ifdef COLLISIONCASE
                       printf("Case 2A return\n");
                       #endif
                       colliding_edge_inds.push_back(i); // note down the colliding edge index
                       continue;
                   }

                }

                if(r2>=r-detaR && r2<=r+detaR)
                {
                   //~ printf("theta v2 is %f\n",Theta_v2);
                   if(Theta_v2 <= s/r)
                   {
                       collision = true;
                       #ifdef COLLISIONCASE
                       printf("Case 2B return\n");
                       #endif
                       colliding_edge_inds.push_back(i); // note down the colliding edge index
                       continue;
                   }
                }


                // Case 3 where check the distance between v1v2 and the center of circle
                if((r1<r2 ?r1 :r2) > (r+ detaR))
                {       
                    // compute the distance from center of circle to the line that v1v2 lie on
                    float d_p12 = (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2) + (z1-z2)*(z1-z2);
                    float t = -((x1 - c[0])*(x2 - x1) + (y1 - c[1])*(y2 - y1) + (z1 - c[2])*(z2 - z1))/(d_p12);

                    // if the minimun distance is within the range of segment v1v2 
                    if(t>0 && t<1)
                    {
                        // compute Vmin, the point on v1v2 that has the mininum distance to the center of circle
                        float x_min = x1+t*(x2-x1);
                        float y_min = y1+t*(y2-y1);
                        float z_min = z1+t*(z2-z1);
                        float Vmin_c[3] = {x_min - c[0], y_min - c[1], z_min - c[2]};
                        Normalize(Vmin_c,3);

                        // checking if Vmin within the angular range
                        float z_sign_Vmin_c = 0;        
                        float cos_Theta_Vmin_c = 0; //cos(theta)
                        float Theta_Vmin_c = 0;//theta
                        DotProduct(Vmin_c,z,&z_sign_Vmin_c);
                        DotProduct(Vmin_c,EndS_c,&cos_Theta_Vmin_c);
                        if(z_sign_Vmin_c >= 0)
                        {
                            Theta_Vmin_c = acos(cos_Theta_Vmin_c);
                        }
                        else
                        {
                            Theta_Vmin_c = 2.0*M_PI - acos(cos_Theta_Vmin_c);
                        }                       
                        float min_D = sqrt((x_min-c[0])*(x_min-c[0]) + (y_min-c[1])*(y_min-c[1]) + (z_min-c[2])*(z_min-c[2]));

                        // if larger that the width no collision
                        if(min_D > (r + detaR))
                        {
                            collision = false;// No collision
                            continue;
                            #ifdef COLLISIONCASE
                            printf("continue 1\n");
                            #endif
                                                    
                        }
                        // if within the range of section i; return collision
                        else if((Theta_Vmin_c <= s/r))
                        {
                            collision = true;
                            #ifdef COLLISIONCASE
                            printf("Case 3A return\n");
                            #endif
                            colliding_edge_inds.push_back(i);// note down the colliding edge index
                            continue;
                        }
                        // if not within the range of section i;  no collision
                        else 
                        {
                            collision = false;
                            continue;
                            #ifdef COLLISIONCASE
                            printf("continue 2\n");
                            #endif
                        }
                
                    }
                    else 
                    {
                        float theta_tmp = ((r1 <= r2)?Theta_v1:Theta_v2);
                        float r_tmp = ((theta_tmp == Theta_v1)?r1:r2);
                        if(theta_tmp<=s/r && r_tmp >= r - detaR && r_tmp <= r + detaR)
                        {
                            #ifdef COLLISIONCASE
                            printf("Case 3B return\n");
                            #endif
                            colliding_edge_inds.push_back(i); // note down the colliding edge index
                            continue;
                        }
                        else
                        {
                            collision = false;
                            continue;
                            #ifdef COLLISIONCASE
                            printf("continue 3\n");
                            #endif
                        }
                    }
                }

                // Case 4 theta1 and theta2 satisfy the angular range
                if(Theta_v2 <= s/r && Theta_v1 <= s/r)
                {
                    collision = true;
                    #ifdef COLLISIONCASE
                    printf("Case 4 return\n");
                    #endif
                    colliding_edge_inds.push_back(i); // note down the colliding edge index
                    continue;
                }

                // Case 5 if there is a intersection between line v1v2 and L_ik  
                if(TwolineIntersect(c,end_e,Q1,Q2,InterP1)/*&&!TwolineIntersect(c,end_s,Q1,Q2,InterP2)*/) // intersect only L1
                {
                    float Inter1_r = sqrt((InterP1[0] - c[0])*(InterP1[0] - c[0]) + (InterP1[1] - c[1])*(InterP1[1] - c[1]) + (InterP1[2] - c[2])*(InterP1[2] - c[2]));
                    
                    if(abs(Inter1_r) <=0.0001)// interceted at the center of circle
                    {
                        float Smallertheta = ((Theta_v1 <= Theta_v2)? Theta_v1:Theta_v2);
                        if(Smallertheta <= s/r)
                        {
                            #ifdef COLLISIONCASE
                            printf("Case 5 return\n");
                            #endif
                            collision = true;
                            colliding_edge_inds.push_back(i); // note down the colliding edge index
                            continue;
                        }
                    }

                    //Case 5.1 colinear with L2 , return collision = true (cos(theta) = 1 or -1)    
                    float CosThetaV1_2_L2 = 0;
                    float V1_2[3] = {x1-x2, y1-y2, z1-z2}; // vector v1 to v2
                    float VL2[3] = {end_s[0] - c[0], end_s[1] - c[1], end_s[2] - c[2]};// L2
                    Normalize(V1_2,3);
                    Normalize(VL2,3);
                    DotProduct(V1_2,VL2,&CosThetaV1_2_L2);   
                    if(abs(CosThetaV1_2_L2 - 1) <= 0.0001 || abs(CosThetaV1_2_L2 + 1) <= 0.0001)
                    {
                        #ifdef COLLISIONCASE
                        printf("Case 5.1-C return\n");
                        #endif
                        collision = true;
                        colliding_edge_inds.push_back(i); // note down the colliding edge index
                        continue;
                    }

                    //Case 5.2 the intersection with L1 is within the radius bound
                    if((r - detaR <= Inter1_r) && (Inter1_r<= r + detaR))
                    {
                        collision = true;
                        #ifdef COLLISIONCASE
                        printf("Case 5.2-C return\n");
                        #endif
                        colliding_edge_inds.push_back(i); // note down the colliding edge index
                        continue;
                    }

                    //Case 5.4, the intersection point above the upper bound of radius
                    if((Inter1_r >= r + detaR))
                    {
                        float SmallTheta = (r1 <= r2 ? Theta_v1:Theta_v2); // choose the smaller vertex theta
                        if(SmallTheta <= s/r)
                        {
                            collision = true;
                            #ifdef COLLISIONCASE
                            printf("Case 5.4-C return\n");
                            #endif
                            colliding_edge_inds.push_back(i); // note down the colliding edge index
                            continue;
                        }
                    }
                    // Case 5.5, the intersection point below the lower bound of radius
                    if((Inter1_r <= r - detaR))
                    {
                        float LargeTheta = (r1 <= r2 ? Theta_v2:Theta_v1); // choose the larger vertex theta
                        if(LargeTheta <= s/r)
                        {
                            collision = true;
                            #ifdef COLLISIONCASE
                            printf("Case 5.5-C return\n");
                            #endif
                            colliding_edge_inds.push_back(i); // note down the colliding edge index
                            continue;
                        }
                    }
                }
                else if(/*!TwolineIntersect(c,end_e,Q1,Q2,InterP1)&&*/TwolineIntersect(c,end_s,Q1,Q2,InterP2)) // intersect L2
                {
                    float Inter2_r = sqrt((InterP2[0] - c[0])*(InterP2[0] - c[0]) + (InterP2[1] - c[1])*(InterP2[1] - c[1]) + (InterP2[2] - c[2])*(InterP2[2] - c[2]));
                    
                    if(abs(Inter2_r) <=0.0001)// interceted at the center of circle
                    {
                        float Smallertheta = ((Theta_v1 <= Theta_v2)? Theta_v1:Theta_v2);
                        if(Smallertheta <= s/r)
                        {
                            collision = true;
                            #ifdef COLLISIONCASE
                            printf("Case 5.6-C return\n");
                            #endif
                            colliding_edge_inds.push_back(i); // note down the colliding edge index
                            continue;
                        }
                    }
                    //Case 5.1 Check if colinear with L1 , return collision = true if cos(theta) = 1 or -1.
                    float CosThetaV1_2_L1 = 0;
                    float V1_2[3] = {x1-x2, y1-y2, z1-z2}; // vector v1 to v2
                    float VL1[3] = {end_e[0] - c[0], end_e[1] - c[1], end_e[2] - c[2]};// L1
                    Normalize(V1_2,3);
                    Normalize(VL1,3);
                    DotProduct(V1_2,VL1,&CosThetaV1_2_L1);  

                    if(abs(CosThetaV1_2_L1 - 1) <= 0.001 || abs(CosThetaV1_2_L1 + 1) <= 0.001)
                    {
                        collision = true;
                        #ifdef COLLISIONCASE
                        printf("Collision case 5.1 occurred\n");
                        #endif
                        colliding_edge_inds.push_back(i); // note down the colliding edge index
                            continue;
                    }

                    //Case 5.2 the intersection with L1 is within the radius bound
                    if((r - detaR <= Inter2_r) && (Inter2_r<= r + detaR))
                    {
                        collision = true;
                        #ifdef COLLISIONCASE
                        printf("Collision case 5.2 occurred\n");
                        #endif
                        colliding_edge_inds.push_back(i); // note down the colliding edge index
                            continue;
                    }

                    //Case 5.4, the intersection point above the upper bound of radius
                    if((Inter2_r >= r + detaR))
                    {
                        float SmallTheta = (r1 <= r2 ? Theta_v1:Theta_v2); // choose the smaller vertex theta
                        if(SmallTheta <= s/r)
                        {
                            collision = true;
                            #ifdef COLLISIONCASE
                            printf("Collision case 5.4 occurred\n");
                            #endif
                            colliding_edge_inds.push_back(i); // note down the colliding edge index
                            continue;
                        }
                    }
                    // Case 5.5, the intersection point below the lower bound of radius
                    if((Inter2_r <= r - detaR))
                    {
                        float LargeTheta = (r1 <= r2 ? Theta_v2:Theta_v1); // choose the larger vertex theta
                        if(LargeTheta <= s/r)
                        {
                            collision = true;
                            #ifdef COLLISIONCASE
                            printf("Collision case 5.5 occurred\n");
                            #endif
                            colliding_edge_inds.push_back(i); // note down the colliding edge index
                            continue;
                        }
                    }
                }
                else if(TwolineIntersect(c,end_e,Q1,Q2,InterP1)&&TwolineIntersect(c,end_s,Q1,Q2,InterP2)) // intersect Both L1 and L2
                {
                    //Case 5.3
                    float Inter1_r = sqrt((InterP1[0] - c[0])*(InterP1[0] - c[0]) + (InterP1[1] - c[1])*(InterP1[1] - c[1]) + (InterP1[2] - c[2])*(InterP1[2] - c[2]));
                    float Inter2_r = sqrt((InterP2[0] - c[0])*(InterP2[0] - c[0]) + (InterP2[1] - c[1])*(InterP2[1] - c[1]) + (InterP2[2] - c[2])*(InterP2[2] - c[2]));
                    if((Inter1_r >= r + detaR && Inter2_r <= r - detaR) ||(Inter2_r >= r + detaR && Inter1_r <= r - detaR) )
                    {
                        collision = true;
                        #ifdef COLLISIONCASE
                        printf("Collision case 5.3 occurred\n");
                        #endif
                        colliding_edge_inds.push_back(i); // note down the colliding edge index
                            continue;
                    }
                }
                else
                {
                    collision = false;
                }
            } // end if-else
        } // end for
        return collision;    
    }
}

bool environment_with_tactile_sensing::armPolygonFullCollisionCheck(Face &cross_sec, std::vector<bool> &colliding_sec_status, std::vector<int> &sec_colliding_edge_inds)
{
    int num_of_secs = mani.get_n_sections();
    colliding_sec_status = std::vector<bool>(num_of_secs, false);
    for (int i=0; i<num_of_secs; ++i)
    {
        if(armSectionPolygonFullCollisionCheck(cross_sec,i,sec_colliding_edge_inds))
            colliding_sec_status[i] = true;
    }

    for (int i=0; i<num_of_secs; ++i)
    {
        if (colliding_sec_status[i])
            return true;
    }
    return false;
}

bool environment_with_tactile_sensing::touchDrivenGraspHW()
{
    /* 
        step 1-enclosing motion step
        sweep until last section is in contact
        by curving sections one by one from base to tip
        if one section reaches its limit, stop curving 
        this one and move on to next section
     */
    ROS_INFO("Enclosing motion step.");
    int num_of_secs = mani.get_n_sections();
    int last_sec_ind = num_of_secs - 1, last_sec_col_edge_ind = -1, i = 0;
    while (i<num_of_secs)
    {
        /* curve sec i until it reaches its limit or a contact on sec i ... n */
        int terminate_sec_ind = curveOneSectionUntilContact(i);
        i = terminate_sec_ind + 1;
    }
    ROS_INFO("One enclosing motion step is finished. Last section either reaches its limit or is in contact.");

    /* 
        step 2-advancing motion step
        find new section endpoints by moving current 
        ones along section endpoints (-x+z) axis direction
        a small amount
        if no ik solution found, set flag and exit
        else move forward a little step
     */
    ROS_INFO("Advancing motion step.");
    std::vector<float> new_cfg(3*num_of_secs, 0.), cur_cfg(3*num_of_secs, 0.);
    for (int i=0; i<num_of_secs; ++i)
    {
        /* get sec_i end point */
        std::vector<float> sec_end_pnts(3*(num_of_secs+1), 0.0);
        if(!mani.compute_section_end_points_wrt_world(sec_end_pnts))
            ROS_ERROR("Failed to compute section end points");
        Eigen::Vector3f sec_i_cur_ep(sec_end_pnts[(i+1)*3], sec_end_pnts[(i+1)*3+1], sec_end_pnts[(i+1)*3+2]);

        /* get sec_i tip frame */
        std::vector<float> sec_i_tip_frame(16,0.);
        mani.compute_section_frame_wrt_world(sec_i_tip_frame, i);
        Eigen::Vector3f sec_i_ep_z_axis(sec_i_tip_frame[8], sec_i_tip_frame[9], sec_i_tip_frame[10]);
        Eigen::Vector3f sec_i_ep_x_axis(sec_i_tip_frame[0], sec_i_tip_frame[1], sec_i_tip_frame[2]);
        Eigen::Vector3f direction;
        /* check if sec_i is in contact, if yes contact with which object? */
        cur_cfg = mani.get_config();
        int choice = 2;
        switch (choice)
        {
            /* consider environment obstacle */
            case 1:
            {
                int col_ind = -1;
                if ( armSectionPolygonCollisionCheck(env_cross_sec, i, col_ind) )
                {
                    if (cur_cfg[i*3+1] < 0)
                        direction = sec_i_ep_z_axis+sec_i_ep_x_axis;
                    else
                        direction = sec_i_ep_z_axis-sec_i_ep_x_axis;
                    ROS_INFO("Touching obstacle");
                }
                else
                {
                    direction = sec_i_ep_z_axis-sec_i_ep_x_axis;
                    ROS_INFO("Touching target");
                }
                break;
            }
            /* not consider environment obstacle */
            case 2:
            {
                direction = sec_i_ep_z_axis-sec_i_ep_x_axis;
                break;
            }
        }
        direction.normalize();

        /* get sec_i new end point */
        Eigen::Vector3f sec_i_new_ep = sec_i_cur_ep + sec_endpnt_moving_dist*direction;
        float sec_i_new_ep_array[3] = {sec_i_new_ep[0], sec_i_new_ep[1], sec_i_new_ep[2]};

        /* get sec_i base frame */
        float sec_i_base_frame[16];
        if (i==0)
            mani.Getbaseframe(sec_i_base_frame);
        else
            mani.compute_section_frame_wrt_world(sec_i_base_frame, i-1);

        /* compute sec_i new cfg */
        std::vector<float> sec_i_new_cfg = mani.inverseKinematic_one_section(sec_i_new_ep_array, sec_i_base_frame);
        // cur_cfg = mani.get_config();
        new_cfg[i*3] = cur_cfg[i*3];
        new_cfg[i*3+1] = cur_cfg[i*3+1];
        new_cfg[i*3+2] = cur_cfg[i*3+2];

        cur_cfg[i*3] = sec_i_new_cfg[0];
        cur_cfg[i*3+1] = sec_i_new_cfg[1];
        cur_cfg[i*3+2] = sec_i_new_cfg[2];

        std::vector<bool> sec_i_limit = mani.isSecCfgWithinPhysicalLimits(cur_cfg, i);
        if (!sec_i_limit[0] || !sec_i_limit[1] || !sec_i_limit[2])
        {
            ROS_WARN("Sec %d new cfg reaches its limit!", i);
            continue;
        }
        else
        {
            mani.update_config(cur_cfg);
            /* save new cfg of sec_i */
            new_cfg[i*3] = sec_i_new_cfg[0];
            new_cfg[i*3+1] = sec_i_new_cfg[1];
            new_cfg[i*3+2] = sec_i_new_cfg[2];
        }
    }

    /*  
        step 3
        check new_cfg and robot physical limit status 
        to determine whether accept new_cfg or not
        and check terminate criteria
    */
    std::vector<bool> sec_limit_status(num_of_secs, true);
    for (int i=0; i<num_of_secs; ++i)
    {
        std::vector<bool> sec_i_phys_limit = mani.isSecCfgWithinPhysicalLimits(new_cfg, i);
        /* check sec_i physical limits */
        if (!sec_i_phys_limit[0] || !sec_i_phys_limit[1])
        {
            sec_limit_status[i] = false;
            ROS_WARN("Section %d reaches length or curvature limit",i);
        }
        /* check sec_i contact */
        int col_edge_ind = -1;
        if (armSectionPolygonCollisionCheck(cross_sec, i, col_edge_ind))
        {
            for (int j=i; j>=0; --j)
                sec_limit_status[j] = false;
            ROS_WARN("Section %d reaches contact",i);
        }
    }
    bool whole_arm_limit_status = true;
    int num_of_secs_limit_reached = 0;
    for (int i=0; i<num_of_secs; ++i)
    {
        if (!sec_limit_status[i])
            ++num_of_secs_limit_reached;
    }
    if (num_of_secs_limit_reached == num_of_secs)
        whole_arm_limit_status = false;
    
    if (whole_arm_limit_status)
        arm_config_collector.push_back(new_cfg);
    else
    {
        ROS_WARN("All sections reach their limits or are in contact!");
        ROS_INFO("Algorithm terminated.");
        arm_physical_limit_warning = true;
    }
    ROS_INFO("One advancing motion step is finished.");
}

/* return terminate section index */
int environment_with_tactile_sensing::curveOneSectionUntilContact(int sec_ind)
{
    int res = -1;
    bool sec_ind_limit_flag = true;
    bool arm_contact_flag = false;
    std::vector<float> cur_cfg;
    ROS_INFO("Curving section %d until contact...", sec_ind);
    while (sec_ind_limit_flag && !arm_contact_flag)
    {
        cur_cfg = mani.get_config();
        /* 
            terminationc case 1
            section limit reached
         */
        std::vector<bool> sec_limit = mani.isSecCfgWithinPhysicalLimits(cur_cfg, sec_ind);
        if (!sec_limit[0] || !sec_limit[1] || !sec_limit[2])
        {
            sec_ind_limit_flag = false;
            res = sec_ind;
            ROS_WARN("Section %d reaches its limits.", sec_ind);
            return res;
        }

        /* 
            terminationc case 2
            contact on arm detected
         */
        std::vector<bool> col_sec_status(mani.get_n_sections(),false);
        std::vector<int> sec_colliding_edge_inds(mani.get_n_sections(),-1);
        if (armPolygonCollisionCheck(cross_sec,col_sec_status, sec_colliding_edge_inds))
        {
            for (int i=0; i<col_sec_status.size(); ++i)
            {
                if (col_sec_status[i] && i >= sec_ind)
                {
                    arm_contact_flag = true;
                    res = i; 
                    ROS_WARN("Contact detected on section %d.", res);
                    return res;
                }
            }
        }

        /* 
            actual curving
         */
        if (determineCurvatureAdjustSign(sec_ind))
            cur_cfg[sec_ind*3+1] -= sec_curvature_adjust_rate; 
        else
            cur_cfg[sec_ind*3+1] += sec_curvature_adjust_rate; 
        mani.update_config(cur_cfg);
        arm_config_collector.push_back(cur_cfg);
    }
}

bool environment_with_tactile_sensing::touchDrivenGraspHWOrigami()
{
     /* 
        step 1-enclosing motion step
        sweep until last section is in contact
        by curving sections one by one from base to tip
        if one section reaches its limit, stop curving 
        this one and move on to next section
     */
    ROS_INFO("Enclosing motion step.");
    int num_of_secs = mani.get_n_sections();
    int last_sec_ind = num_of_secs - 1, last_sec_col_edge_ind = -1, i = 0;
    while (i<num_of_secs)
    {
        /* curve sec i until it reaches its limit or a contact on sec i ... n */
        int terminate_sec_ind = curveOneSectionUntilContactOrigami(i);
        i = terminate_sec_ind + 1;
    }
    ROS_INFO("One enclosing motion step is finished. Last section either reaches its limit or is in contact.");

    /* 
        step 2-advancing motion step
        find new section endpoints by moving current 
        ones along section endpoints (-x+z) axis direction
        a small amount
        if no ik solution found, set flag and exit
        else move forward a little step
     */
    ROS_INFO("Advancing motion step.");
    std::vector<float> new_cfg(3*num_of_secs, 0.), cur_cfg(3*num_of_secs, 0.);
    for (int i=0; i<num_of_secs; ++i)
    {
        /* get sec_i end point */
        std::vector<float> sec_end_pnts(3*(num_of_secs+1), 0.0);
        if(!mani.compute_section_end_points_wrt_world(sec_end_pnts))
            ROS_ERROR("Failed to compute section end points");
        Eigen::Vector3f sec_i_cur_ep(sec_end_pnts[(i+1)*3], sec_end_pnts[(i+1)*3+1], sec_end_pnts[(i+1)*3+2]);

        /* get sec_i tip frame */
        std::vector<float> sec_i_tip_frame(16,0.);
        mani.compute_section_frame_wrt_world(sec_i_tip_frame, i);
        Eigen::Vector3f sec_i_ep_z_axis(sec_i_tip_frame[8], sec_i_tip_frame[9], sec_i_tip_frame[10]);
        Eigen::Vector3f sec_i_ep_x_axis(sec_i_tip_frame[0], sec_i_tip_frame[1], sec_i_tip_frame[2]);
        Eigen::Vector3f direction = sec_i_ep_z_axis-sec_i_ep_x_axis;
        direction.normalize();

        /* get sec_i new end point */
        Eigen::Vector3f sec_i_new_ep = sec_i_cur_ep + sec_endpnt_moving_dist*direction;
        float sec_i_new_ep_array[3] = {sec_i_new_ep[0], sec_i_new_ep[1], sec_i_new_ep[2]};

        /* get sec_i base frame */
        float sec_i_base_frame[16];
        if (i==0)
            mani.Getbaseframe(sec_i_base_frame);
        else
            mani.compute_section_frame_wrt_world(sec_i_base_frame, i-1);

        /* compute sec_i new cfg */
        std::vector<float> sec_i_new_cfg = mani.inverseKinematic_one_section(sec_i_new_ep_array, sec_i_base_frame);
        cur_cfg = mani.get_config();
        new_cfg[i*3] = cur_cfg[i*3];
        new_cfg[i*3+1] = cur_cfg[i*3+1];
        new_cfg[i*3+2] = cur_cfg[i*3+2];

        cur_cfg[i*3] = sec_i_new_cfg[0];
        cur_cfg[i*3+1] = sec_i_new_cfg[1];
        cur_cfg[i*3+2] = sec_i_new_cfg[2];

        std::vector<bool> sec_i_limit = mani.isSecCfgWithinPhysicalLimits(cur_cfg, i);
        if (!sec_i_limit[0] || !sec_i_limit[1] || !sec_i_limit[2])
        {
            ROS_WARN("Sec %d new cfg reaches its limit!", i);
            continue;
        }
        else
        {
            mani.update_config(cur_cfg);
            /* save new cfg of sec_i */
            new_cfg[i*3] = sec_i_new_cfg[0];
            new_cfg[i*3+1] = sec_i_new_cfg[1];
            new_cfg[i*3+2] = sec_i_new_cfg[2];
        }
    }

    /*  
        step 3
        check new_cfg and robot physical limit status 
        to determine whether accept new_cfg or not
        and check terminate criteria
    */
    std::vector<bool> sec_limit_status(num_of_secs, true);
    for (int i=0; i<num_of_secs; ++i)
    {
        std::vector<bool> sec_i_phys_limit = mani.isSecCfgWithinPhysicalLimits(new_cfg, i);
        /* check sec_i physical limits */
        if (!sec_i_phys_limit[0] || !sec_i_phys_limit[1])
        {
            sec_limit_status[i] = false;
            ROS_WARN("Section %d reaches length or curvature limit",i);
        }
        /* check sec_i contact */
        int col_edge_ind = -1;
        if (armSectionPolygonCollisionCheckOrigami(i, col_edge_ind))
        {
            for (int j=i; j>=0; --j)
                sec_limit_status[j] = false;
            ROS_WARN("Section %d reaches contact",i);
        }
    }
    bool whole_arm_limit_status = true;
    int num_of_secs_limit_reached = 0;
    for (int i=0; i<num_of_secs; ++i)
    {
        if (!sec_limit_status[i])
            ++num_of_secs_limit_reached;
    }
    if (num_of_secs_limit_reached == num_of_secs)
        whole_arm_limit_status = false;
    
    if (whole_arm_limit_status)
    {
        arm_config_collector.push_back(new_cfg);
        
        /* publish motion cmd */
        std_msgs::Float32MultiArray cmd;
        cmd.data = new_cfg;
        motion_cmd_pub.publish(cmd);
        sleep(1.0);
        ROS_INFO("Wait until command is executed then press enter");
        getchar();
        ros::spinOnce();
    }
    else
    {
        ROS_WARN("All sections reach their limits or are in contact!");
        ROS_INFO("Algorithm terminated.");
        arm_physical_limit_warning = true;
    }
    ROS_INFO("One advancing motion step is finished.");
}

/* return terminate section index */
int environment_with_tactile_sensing::curveOneSectionUntilContactOrigami(int sec_ind)
{
    int res = -1;
    bool sec_ind_limit_flag = true;
    bool arm_contact_flag = false;
    std::vector<float> cur_cfg;
    ROS_INFO("Curving section %d until contact...", sec_ind);
    while (sec_ind_limit_flag && !arm_contact_flag)
    {
        cur_cfg = mani.get_config();
        /* 
            terminationc case 1
            section limit reached
         */
        std::vector<bool> sec_limit = mani.isSecCfgWithinPhysicalLimits(cur_cfg, sec_ind);
        if (!sec_limit[0] || !sec_limit[1] || !sec_limit[2])
        {
            sec_ind_limit_flag = false;
            res = sec_ind;
            ROS_WARN("Section %d reaches its limits.", sec_ind);
            return res;
        }

        /* 
            terminationc case 2
            contact on arm detected
         */
        std::vector<bool> col_sec_status(mani.get_n_sections(),false);
        std::vector<int> sec_colliding_edge_inds(mani.get_n_sections(),-1);
        if (armPolygonCollisionCheckOrigami(col_sec_status, sec_colliding_edge_inds))
        {
            for (int i=0; i<col_sec_status.size(); ++i)
            {
                if (col_sec_status[i] && i >= sec_ind)
                {
                    arm_contact_flag = true;
                    res = i; 
                    ROS_WARN("Contact detected on section %d.", res);
                    return res;
                }
            }
        }

        /* 
            actual curving
         */
        if (determineCurvatureAdjustSign(sec_ind))
            cur_cfg[sec_ind*3+1] -= sec_curvature_adjust_rate; 
        else
            cur_cfg[sec_ind*3+1] += sec_curvature_adjust_rate; 
        mani.update_config(cur_cfg);
        arm_config_collector.push_back(cur_cfg);

        /* publish motion cmd */
        std_msgs::Float32MultiArray cmd;
        cmd.data = cur_cfg;
        motion_cmd_pub.publish(cmd);
        ROS_INFO("Wait until command is executed then press enter");
        getchar();
        ros::spinOnce();
    }
}

bool environment_with_tactile_sensing::armSectionPolygonCollisionCheckOrigami(int SectionIdx,
                                                                              int &colliding_edge_ind)
{
    return section_cnt_state.at(SectionIdx);
}

bool environment_with_tactile_sensing::armPolygonCollisionCheckOrigami(std::vector<bool> &colliding_sec_status, std::vector<int> &sec_colliding_edge_inds)
{
    int num_of_secs = mani.get_n_sections();
    colliding_sec_status = std::vector<bool>(num_of_secs, false);
    for (int i=0; i<num_of_secs; ++i)
    {
        if(armSectionPolygonCollisionCheckOrigami(i,sec_colliding_edge_inds[i]))
        {
            colliding_sec_status[i] = true;
        }
    }

    for (int i=0; i<num_of_secs; ++i)
    {
        if (colliding_sec_status[i])
            return true;
    }
    return false;
}


bool environment_with_tactile_sensing::wholeArmIkByTgtPntAndArc2CordDistAndNormal(float arc2cord_dist, 
                                                                                  float sec1_tgt_pnt[3], // sec1 end pnt
                                                                                  float arc_start[3],    // sec2 end pnt
                                                                                  float tgt_pnt[3],      // sec3 end pnt
                                                                                  float arc_cir_plane_normal[3])
{
    std::cout << std::endl;
    std::cout << "********Solving IK**********" << std::endl;
    float sec1_len_limit = 10.0;
    float sec2_len_limit = 10.0;
    float sec3_len_limit = 10.0;
    
    int n_secs = mani.get_n_sections();
    vector<float> mani_eps = mani.get_section_end_points_wrt_world();
    float tip [3] = {mani_eps[3*(n_secs)], 
                     mani_eps[3*(n_secs)+1],
                     mani_eps[3*(n_secs)+2]}; 
    std::vector<float> tip_pos(tip, tip+3);
    
    float vec_armtip_to_tgtpnt[3] ={tgt_pnt[0]-tip_pos[0],
                                    tgt_pnt[1]-tip_pos[1],
                                    tgt_pnt[2]-tip_pos[2]};
    
    // get section 3 tip frame
    float sec3_frame[16] = {0.0};
    mani.compute_section_frame_wrt_world(sec3_frame, 2); // 2 is section 3
    // get section 2 tip frame
    float sec2_frame[16] = {0.0};
    mani.compute_section_frame_wrt_world(sec2_frame, 1); // 1 is section 2
    // get section 1 tip frame
    float sec1_frame[16] = {0.0};
    mani.compute_section_frame_wrt_world(sec1_frame, 0); // 0 is section 1
    
    /****** Compute Arc Circle *******/ 
    float arc_cir_center[3] = {0.0};
    float arc_cir_radius = 0.0;
    bool isArcCirFound = compute_circle_given_twopoints_arc2cordDis(arc_start, tgt_pnt, arc2cord_dist, 
                                                                    arc_cir_plane_normal, 
                                                                    arc_cir_center,
                                                                    arc_cir_radius);
    
    #ifdef DEBUGIK
    printf("sec3 arc_cir_center is (%f,%f,%f)\n",arc_cir_center[0], arc_cir_center[1], arc_cir_center[2]);
    printf("sec3 arc_cir_radius is %f\n", arc_cir_radius);
    float vec1[3] = {arc_cir_center[0]-tgt_pnt[0],
                     arc_cir_center[1]-tgt_pnt[1],
                     arc_cir_center[2]-tgt_pnt[2]};
                     
    float vec2[3] = {arc_cir_center[0]-arc_start[0],
                     arc_cir_center[1]-arc_start[1],
                     arc_cir_center[2]-arc_start[2]};
    float test[3] = {0.0};
    CrossProduct(vec1, vec2, test);
    Normalize(test, 3);
    std::cout << "test normal is " << test[0] << " " << test[1] << " " << test[2] << std::endl;
    printf("real normal is (%f,%f,%f)\n", arc_cir_plane_normal[0],arc_cir_plane_normal[1],arc_cir_plane_normal[2]);
    #endif
    if (isArcCirFound) {
        float half_dist_uv = two_pts_dist(tgt_pnt, arc_start, 3)/2.0;
        float computed_arc_len = 2.0 * arc_cir_radius * asin(half_dist_uv / arc_cir_radius);
        if (computed_arc_len < sec3_len_limit) {
            // fill in information for sec 3
            std::vector<float> output_sec3_config(3, 0.0);
            output_sec3_config[0] = computed_arc_len;
            output_sec3_config[1] = 1.0 / arc_cir_radius;
            output_sec3_config[2] = 0.0;
            
            /****Compute Config for Sec 2****/
            float sec2_tgt_pnt[3] = {arc_start[0], arc_start[1], arc_start[2]};
            float sec2_tgt_pnt_tangent[3] = {0.0};
            // step 1 -> get tan vec at sec2_tgt_pnt
            float sec3_tip_x_axis[3] = {arc_cir_center[0] - tgt_pnt[0],
                                        arc_cir_center[1] - tgt_pnt[1],
                                        arc_cir_center[2] - tgt_pnt[2]};
            Normalize(sec3_tip_x_axis, 3);
            float sec3_y_axis[3] = {arc_cir_plane_normal[0],
                                    arc_cir_plane_normal[1],
                                    arc_cir_plane_normal[2]};
            float sec3_tip_z_axis[3] = {0.0};
            CrossProduct(sec3_tip_x_axis,sec3_y_axis, sec3_tip_z_axis);
            Normalize(sec3_tip_z_axis,3);
            float sec3_base_x_axis[3] ={arc_cir_center[0] - arc_start[0],
                                        arc_cir_center[1] - arc_start[1],
                                        arc_cir_center[2] - arc_start[2]};
            Normalize(sec3_base_x_axis,3);
            float sec3_base_z_axis[3] = {0.0};
            CrossProduct(sec3_base_x_axis, sec3_y_axis, sec3_base_z_axis);
            Normalize(sec3_base_z_axis,3);
            sec2_tgt_pnt_tangent[0] = sec3_base_z_axis[0];
            sec2_tgt_pnt_tangent[1] = sec3_base_z_axis[1];
            sec2_tgt_pnt_tangent[2] = sec3_base_z_axis[2];
            
            // step 2 -> get sec2 new base position
            float sec1_vec_scale = 0.;
            sec1_frame[12] += sec1_vec_scale * vec_armtip_to_tgtpnt[0];
            sec1_frame[13] += sec1_vec_scale * vec_armtip_to_tgtpnt[1];
            sec1_frame[14] += sec1_vec_scale * vec_armtip_to_tgtpnt[2];
            
            // step 3 -> compute cir for sec 2
            float cir2_plane_normal[3] = {0.0};
            float cir2_center[3] = {0.0};
            float cir2_radius = 0.0;
            bool isCir2Found = compute_circle_given_twopoints_one_tangentVec(sec2_tgt_pnt, sec1_tgt_pnt, 
                                                                            sec2_tgt_pnt_tangent, 
                                                                            cir2_plane_normal,
                                                                            cir2_center,
                                                                            cir2_radius);
            
            #ifdef DEBUGIK
            printf("cir2 center is (%f,%f,%f)\n", cir2_center[0], cir2_center[1],cir2_center[2]);
            printf("sec2 tgt pnt tangent is (%f,%f,%f)\n", sec2_tgt_pnt_tangent[0], sec2_tgt_pnt_tangent[1],sec2_tgt_pnt_tangent[2]);
            printf("cir2 radius is %f\n", cir2_radius);
            printf("cir2 plane normal is (%f,%f,%f)\n", cir2_plane_normal[0],cir2_plane_normal[1],cir2_plane_normal[2]);
            #endif
            
            std::vector<float> output_sec2_config(3, 0.0);
            if (isCir2Found){
                float half_dist_uv_sec2 = two_pts_dist(sec2_tgt_pnt, sec1_tgt_pnt, 3)/2.0;
                output_sec2_config[0] = 2.0 * cir2_radius * asin(half_dist_uv_sec2 / cir2_radius);
                output_sec2_config[1] = 1.0 / cir2_radius;
                output_sec2_config[2] = 0.0;
            }
            else
                std::cout << "cir 2 is not found!" << std::endl;
            float sec2_tip_x_axis[3] = {cir2_center[0]-sec2_tgt_pnt[0],
                                        cir2_center[1]-sec2_tgt_pnt[1],
                                        cir2_center[2]-sec2_tgt_pnt[2]};
            Normalize(sec2_tip_x_axis,3);
            float sec2_y_axis[3] = {cir2_plane_normal[0],cir2_plane_normal[1],cir2_plane_normal[2]};
            float sec2_tip_z_axis[3] = {0.0};
            CrossProduct(sec2_tip_x_axis, sec2_y_axis, sec2_tip_z_axis);
            Normalize(sec2_tip_z_axis, 3);

            #ifdef DEBUGIK
            printf("sec2 tip z-axis is (%f,%f,%f)\n", sec2_tip_z_axis[0], sec2_tip_z_axis[1],sec2_tip_z_axis[2]);
            #endif
             
            float sec2_base_x_axis[3] ={cir2_center[0]-sec1_tgt_pnt[0],
                                        cir2_center[1]-sec1_tgt_pnt[1],
                                        cir2_center[2]-sec1_tgt_pnt[2]};
            float sec2_base_z_axis[3] = {0.0};
            CrossProduct(sec2_base_x_axis, sec2_y_axis, sec2_base_z_axis);
            Normalize(sec2_base_z_axis,3);
            
            /****Compute Config for Sec 1****/
            float sec1_tgt_pnt_tangent[3] = {sec2_base_z_axis[0], sec2_base_z_axis[1], sec2_base_z_axis[2]};
            float origin[3] = {0.0};
            float cir1_plane_normal[3] = {0.0};
            float cir1_center[3] = {0.0};
            float cir1_radius = 0.0;
            bool isCir1Found = compute_circle_given_twopoints_one_tangentVec(sec1_tgt_pnt, origin, 
                                                                             sec1_tgt_pnt_tangent, 
                                                                             cir1_plane_normal,
                                                                             cir1_center,
                                                                             cir1_radius);
            
            #ifdef DEBUGIK
            printf("cir1 center is (%f,%f,%f)\n", cir1_center[0], cir1_center[1],cir1_center[2]);
            printf("cir1 radius is %f\n", cir1_radius);
            printf("cir1 plane normal is (%f,%f,%f)\n", cir1_plane_normal[0],cir1_plane_normal[1],cir1_plane_normal[2]);
            #endif
            
            std::vector<float> output_sec1_config(3, 0.0);
            if (isCir1Found) {
                float half_dist_uv_sec1 = two_pts_dist(sec1_tgt_pnt, origin, 3)/2.0;
                output_sec1_config[0] = 2.0 * cir1_radius * asin(half_dist_uv_sec1 / cir1_radius);
                output_sec1_config[1] = 1.0 / cir1_radius;
                output_sec1_config[2] = 0.0;
            }
            else 
                std::cout << "cir1 is not found!" << std::endl;
                
            // adjust base frame
            float sec1_tip_z_axis [3] = {sec2_base_z_axis[0],
                                         sec2_base_z_axis[1],
                                         sec2_base_z_axis[2]};
            float sec1_tip_x_axis [3] = {cir1_center[0]-sec1_tgt_pnt[0],
                                         cir1_center[1]-sec1_tgt_pnt[1],
                                         cir1_center[2]-sec1_tgt_pnt[2]};
            Normalize(sec1_tip_x_axis,3);
            float sec1_y_axis[3] = {cir1_plane_normal[0],cir1_plane_normal[1],cir1_plane_normal[2]};
            float sec1_base_x_axis [3] ={cir1_center[0]-0.0,
                                         cir1_center[1]-0.0,
                                         cir1_center[2]-0.0};
            Normalize(sec1_base_x_axis,3);
            float sec1_base_z_axis[3] = {0.0};
            CrossProduct(sec1_base_x_axis, sec1_y_axis, sec1_base_z_axis);
            Normalize(sec1_base_z_axis,3);
            
            #ifdef DEBUGIK
            printf("sec1 tip z-axis is (%f,%f,%f)\n", sec1_tip_z_axis[0], sec1_tip_z_axis[1],sec1_tip_z_axis[2]);
            #endif
            /********* Calculate phi Angles *********/
            // -> step 1 collect frames
            Eigen::Matrix4f sec1_tip_frame;
            sec1_tip_frame << sec1_tip_x_axis[0], sec1_y_axis[0], sec1_tip_z_axis[0], sec1_tgt_pnt[0],
                              sec1_tip_x_axis[1], sec1_y_axis[1], sec1_tip_z_axis[1], sec1_tgt_pnt[1],
                              sec1_tip_x_axis[2], sec1_y_axis[2], sec1_tip_z_axis[2], sec1_tgt_pnt[2],
                              0.0,                0.0,            0.0,                1.0;
            Eigen::Matrix4f sec2_base_frame;
            sec2_base_frame << sec2_base_x_axis[0], sec2_y_axis[0], sec2_base_z_axis[0], sec1_tgt_pnt[0],
                               sec2_base_x_axis[1], sec2_y_axis[1], sec2_base_z_axis[1], sec1_tgt_pnt[1],
                               sec2_base_x_axis[2], sec2_y_axis[2], sec2_base_z_axis[2], sec1_tgt_pnt[2],
                               0.0,                0.0,            0.0,                1.0;
            Eigen::Matrix4f sec2_tip_frame;
            sec2_tip_frame << sec2_tip_x_axis[0], sec2_y_axis[0], sec2_tip_z_axis[0], sec2_tgt_pnt[0],
                              sec2_tip_x_axis[1], sec2_y_axis[1], sec2_tip_z_axis[1], sec2_tgt_pnt[1],
                              sec2_tip_x_axis[2], sec2_y_axis[2], sec2_tip_z_axis[2], sec2_tgt_pnt[2],
                              0.0,                0.0,            0.0,                1.0;
            Eigen::Matrix4f sec3_base_frame;
            sec3_base_frame << sec3_base_x_axis[0], sec3_y_axis[0], sec3_base_z_axis[0], sec2_tgt_pnt[0],
                               sec3_base_x_axis[1], sec3_y_axis[1], sec3_base_z_axis[1], sec2_tgt_pnt[1],
                               sec3_base_x_axis[2], sec3_y_axis[2], sec3_base_z_axis[2], sec2_tgt_pnt[2],
                               0.0,                0.0,            0.0,                1.0;
            // -> step 2 find phi angles    
            Eigen::Matrix4f tf_sec2_base_to_sec1_tip = (sec1_tip_frame.inverse()) * sec2_base_frame;
            Eigen::VectorXd tf_vec_sec2_base_to_sec1_tip = regular_eigen_tf_matrix_to_vector(tf_sec2_base_to_sec1_tip);
            
            Eigen::Matrix4f tf_sec3_base_to_sec2_tip = (sec2_tip_frame.inverse()) * sec3_base_frame;
            Eigen::VectorXd tf_vec_sec3_base_to_sec2_tip = regular_eigen_tf_matrix_to_vector(tf_sec3_base_to_sec2_tip);
            
            output_sec1_config[2] = 0.0;
            float phi_2 = tf_vec_sec2_base_to_sec1_tip[5];
            output_sec2_config[2] = phi_2;
            float phi_3 = tf_vec_sec3_base_to_sec2_tip[5];
            output_sec3_config[2] = phi_3;
            
            
            /*************Define new arm config and base frame*********************/
            // update arm config
            std::vector<float> cur_config;
            cur_config.resize(9);
            mani.get_config(cur_config);
            
            cur_config[0] = output_sec1_config[0];
            cur_config[1] = -output_sec1_config[1];
            cur_config[2] = output_sec1_config[2];
            
            cur_config[3] = output_sec2_config[0];
            cur_config[4] = -output_sec2_config[1];
            cur_config[5] = output_sec2_config[2];
            
            cur_config[6] = output_sec3_config[0];
            cur_config[7] = -output_sec3_config[1];
            cur_config[8] = output_sec3_config[2];
            
            // update base frame
            float new_base_x_axis[3] = {cir1_center[0], cir1_center[1], cir1_center[2]};
            Normalize(new_base_x_axis, 3);
            float new_base_z_axis[3] = {0.0};
            CrossProduct(new_base_x_axis, cir1_plane_normal, new_base_z_axis);
            
            float cur_base_frame[16] = {0.0};
            mani.Getbaseframe(cur_base_frame);
            cur_base_frame[0] = new_base_x_axis[0]; // x-axis
            cur_base_frame[1] = new_base_x_axis[1];
            cur_base_frame[2] = new_base_x_axis[2];
            
            cur_base_frame[4] = cir1_plane_normal[0]; // y-axis
            cur_base_frame[5] = cir1_plane_normal[1];
            cur_base_frame[6] = cir1_plane_normal[2];
            
            cur_base_frame[8] = new_base_z_axis[0]; // z-axis
            cur_base_frame[9] = new_base_z_axis[1];
            cur_base_frame[10] = new_base_z_axis[2];
            std::vector<float> new_base_frame(cur_base_frame, cur_base_frame+16);
            
            mani.update_config(cur_config, new_base_frame);
            
            vector<float> arm_endpoints = mani.get_section_end_points_wrt_world();  
            
            float arm_tip_diff = sqrt ( pow(arm_endpoints[3*n_secs]-tgt_pnt[0],2) + 
                                        pow(arm_endpoints[3*n_secs+1]-tgt_pnt[1],2) + 
                                        pow(arm_endpoints[3*n_secs+2]-tgt_pnt[2],2) );
            
            #ifdef DEBUGIK
            std::cout << "sec3_tgt_pnt is " << tgt_pnt[0] << " " << tgt_pnt[1] 
              << " " << tgt_pnt[2]<<std::endl;
            std::cout << "sec2_tgt_pnt is " << sec2_tgt_pnt[0] << " " 
                      << sec2_tgt_pnt[1] << " " << sec2_tgt_pnt[2] << std::endl;
            std::cout << "sec1_tgt_pnt is " << sec1_tgt_pnt[0] << " " 
                      << sec1_tgt_pnt[1] << " " << sec1_tgt_pnt[2] << std::endl;
            
            arm_endpoints = mani.get_section_end_points_wrt_world();
            std::cout << "sec3 endpoint is " << arm_endpoints[3*n_secs] << " " << arm_endpoints[3*n_secs+1]
                     << " " << arm_endpoints[3*n_secs+2] << std::endl;
            std::cout << "sec2 endpoint is " << arm_endpoints[2*n_secs] << " " << arm_endpoints[2*n_secs+1]
                     << " " << arm_endpoints[2*n_secs+2] << std::endl;
            std::cout << "sec1 endpoint is " << arm_endpoints[1*n_secs] << " " << arm_endpoints[1*n_secs+1]
                     << " " << arm_endpoints[1*n_secs+2] << std::endl;
            
            std::cout << "sec3_y_axis is " << std::endl;
            std::cout << sec3_y_axis[0] << " "  << sec3_y_axis[1] << " "  << sec3_y_axis[2] << std::endl;
            std::cout << "sec3_tip_x_axis is " << std::endl;
            std::cout << sec3_tip_x_axis[0] << " "  << sec3_tip_x_axis[1] << " "  << sec3_tip_x_axis[2] << std::endl;
            std::cout << "sec3_tip_z_axis is " << std::endl;
            std::cout << sec3_tip_z_axis[0] << " "  << sec3_tip_z_axis[1] << " "  << sec3_tip_z_axis[2] << std::endl;
            
            std::cout << "sec3_base_x_axis is " << std::endl;
            std::cout << sec3_base_x_axis[0] << " "  << sec3_base_x_axis[1] << " "  << sec3_base_x_axis[2] << std::endl;
            std::cout << "sec3_base_z_axis is " << std::endl;
            std::cout << sec3_base_z_axis[0] << " "  << sec3_base_z_axis[1] << " "  << sec3_base_z_axis[2] << std::endl;
            std::cout << std::endl;
            
            std::cout << "new sec3 frame is "  << std::endl;
            float new_sec3_frame[16] = {0.0};
            mani.compute_section_frame_wrt_world(new_sec3_frame, 2); 
            for (int temp_cnt=0; temp_cnt<16; ++temp_cnt)
                std::cout << new_sec3_frame[temp_cnt] << std::endl;
                
            std::cout << "new sec2 frame is "  << std::endl;
            float new_sec2_frame[16] = {0.0};
            mani.compute_section_frame_wrt_world(new_sec2_frame, 1); 
            for (int temp_cnt=0; temp_cnt<16; ++temp_cnt)
                std::cout << new_sec2_frame[temp_cnt] << std::endl;
            #endif
            
            #ifdef DEBUGIK
            printf("1st try\n");
            /*All shorter arcs*/
            std::cout << "sec3_tgt_pnt is " << tgt_pnt[0] << " " << tgt_pnt[1] 
              << " " << tgt_pnt[2]<<std::endl;
            std::cout << "sec2_tgt_pnt is " << sec2_tgt_pnt[0] << " " 
                      << sec2_tgt_pnt[1] << " " << sec2_tgt_pnt[2] << std::endl;
            std::cout << "sec1_tgt_pnt is " << sec1_tgt_pnt[0] << " " 
                      << sec1_tgt_pnt[1] << " " << sec1_tgt_pnt[2] << std::endl;
            
            arm_endpoints = mani.get_section_end_points_wrt_world();
            std::cout << "sec3 endpoint is " << arm_endpoints[3*n_secs] << " " << arm_endpoints[3*n_secs+1]
                     << " " << arm_endpoints[3*n_secs+2] << std::endl;
            std::cout << "sec2 endpoint is " << arm_endpoints[2*n_secs] << " " << arm_endpoints[2*n_secs+1]
                     << " " << arm_endpoints[2*n_secs+2] << std::endl;
            std::cout << "sec1 endpoint is " << arm_endpoints[1*n_secs] << " " << arm_endpoints[1*n_secs+1]
                     << " " << arm_endpoints[1*n_secs+2] << std::endl;
            
            std::cout << "updated arm configuration is " << std::endl;
            for (int cfg=0; cfg < 9; ++cfg)
                std::cout << cur_config[cfg] << std::endl;
            std::cout << "updated arm base frame is " << std::endl;
            for (int bf=0; bf < 16; ++bf)
                std::cout << new_base_frame[bf] << std::endl;
            #endif
            
            float diff_th = 1e-5;
            
            if (arm_tip_diff < diff_th) {
                printf("IK found in 1st try\n");
                return true;
            }
            else
            {
                // catch the case all secs use longer arcs
                cur_config[0] = 2.0*M_PI*cir1_radius - cur_config[0];
                cur_config[3] = 2.0*M_PI*cir2_radius - cur_config[3];
                cur_config[6] = 2.0*M_PI*arc_cir_radius - cur_config[6];
                mani.update_config(cur_config, new_base_frame);
                arm_endpoints = mani.get_section_end_points_wrt_world();
                arm_tip_diff = sqrt(pow(arm_endpoints[3*n_secs]-tgt_pnt[0],2) + 
                                    pow(arm_endpoints[3*n_secs+1]-tgt_pnt[1],2) + 
                                    pow(arm_endpoints[3*n_secs+2]-tgt_pnt[2],2));
                
                #ifdef DEBUGIK
                printf("2nd try\n");
                /*All longer arcs*/
                arm_endpoints = mani.get_section_end_points_wrt_world();
                std::cout << "sec3 endpoint is " << arm_endpoints[3*n_secs] << " " << arm_endpoints[3*n_secs+1]
                         << " " << arm_endpoints[3*n_secs+2] << std::endl;
                std::cout << "sec2 endpoint is " << arm_endpoints[2*n_secs] << " " << arm_endpoints[2*n_secs+1]
                         << " " << arm_endpoints[2*n_secs+2] << std::endl;
                std::cout << "sec1 endpoint is " << arm_endpoints[1*n_secs] << " " << arm_endpoints[1*n_secs+1]
                         << " " << arm_endpoints[1*n_secs+2] << std::endl;
                #endif
                
                if (arm_tip_diff < diff_th) {
                    printf("IK found in 2nd try\n");
                    return true;
                }
                else 
                {
                    cur_config[0] = 2.0*M_PI*cir1_radius - cur_config[0]; // shorter
                    cur_config[3] = 2.0*M_PI*cir2_radius - cur_config[3]; // shorter
                    cur_config[6] = cur_config[6]; // longer
                    
                    mani.update_config(cur_config, new_base_frame);
                    arm_endpoints = mani.get_section_end_points_wrt_world();
                    arm_tip_diff = sqrt(pow(arm_endpoints[3*n_secs]-tgt_pnt[0],2) + 
                                        pow(arm_endpoints[3*n_secs+1]-tgt_pnt[1],2) + 
                                        pow(arm_endpoints[3*n_secs+2]-tgt_pnt[2],2));
                    
                    #ifdef DEBUGIK
                    printf("3rd try\n");
                    /*Sec3 longer arc Sec1 and Sec2 shorter arcs*/
                    arm_endpoints = mani.get_section_end_points_wrt_world();
                    std::cout << "sec3 endpoint is " << arm_endpoints[3*n_secs] << " " << arm_endpoints[3*n_secs+1]
                             << " " << arm_endpoints[3*n_secs+2] << std::endl;
                    std::cout << "sec2 endpoint is " << arm_endpoints[2*n_secs] << " " << arm_endpoints[2*n_secs+1]
                             << " " << arm_endpoints[2*n_secs+2] << std::endl;
                    std::cout << "sec1 endpoint is " << arm_endpoints[1*n_secs] << " " << arm_endpoints[1*n_secs+1]
                             << " " << arm_endpoints[1*n_secs+2] << std::endl;
                    #endif

                    if (arm_tip_diff < diff_th) {
                        printf("IK found in 3rd try\n");
                        return true;
                    }
                    else {
                        /*Sec1 longer arc Sec2 and Sec3 shorter arcs*/
                        cur_config[0] = 2.0*M_PI*cir1_radius - cur_config[0]; // longer
                        cur_config[3] = cur_config[3]; // shorter
                        cur_config[6] = 2.0*M_PI*arc_cir_radius - cur_config[6]; // shorter
                        
                        mani.update_config(cur_config, new_base_frame);
                        arm_endpoints = mani.get_section_end_points_wrt_world();
                        arm_tip_diff = sqrt(pow(arm_endpoints[3*n_secs]-tgt_pnt[0],2) + 
                                            pow(arm_endpoints[3*n_secs+1]-tgt_pnt[1],2) + 
                                            pow(arm_endpoints[3*n_secs+2]-tgt_pnt[2],2));
                        #ifdef DEBUGIK
                        printf("4th try\n");
                        /*Sec1 longer arc Sec2 and Sec3 shorter arcs*/
                        arm_endpoints = mani.get_section_end_points_wrt_world();
                        std::cout << "sec3 endpoint is " << arm_endpoints[3*n_secs] << " " << arm_endpoints[3*n_secs+1]
                                 << " " << arm_endpoints[3*n_secs+2] << std::endl;
                        std::cout << "sec2 endpoint is " << arm_endpoints[2*n_secs] << " " << arm_endpoints[2*n_secs+1]
                                 << " " << arm_endpoints[2*n_secs+2] << std::endl;
                        std::cout << "sec1 endpoint is " << arm_endpoints[1*n_secs] << " " << arm_endpoints[1*n_secs+1]
                                 << " " << arm_endpoints[1*n_secs+2] << std::endl;
                        #endif

                        if (arm_tip_diff < diff_th) {
                            printf("IK found in 4th try\n");
                            return true;
                        }
                        else {
                            /*Sec2 longer arc Sec1 and Sec3 shorter arcs*/
                            cur_config[0] = 2.0*M_PI*cir1_radius - cur_config[0]; // shorter
                            cur_config[3] = 2.0*M_PI*cir2_radius - cur_config[3]; // longer
                            cur_config[6] = cur_config[6]; // shorter
                            
                            mani.update_config(cur_config, new_base_frame);
                            arm_endpoints = mani.get_section_end_points_wrt_world();
                            arm_tip_diff = sqrt(pow(arm_endpoints[3*n_secs]-tgt_pnt[0],2) + 
                                                pow(arm_endpoints[3*n_secs+1]-tgt_pnt[1],2) + 
                                                pow(arm_endpoints[3*n_secs+2]-tgt_pnt[2],2));
                            #ifdef DEBUGIK
                            printf("5th try\n");
                            /*Sec2 longer arc Sec1 and Sec3 shorter arcs*/
                            arm_endpoints = mani.get_section_end_points_wrt_world();
                            std::cout << "sec3 endpoint is " << arm_endpoints[3*n_secs] << " " << arm_endpoints[3*n_secs+1]
                                     << " " << arm_endpoints[3*n_secs+2] << std::endl;
                            std::cout << "sec2 endpoint is " << arm_endpoints[2*n_secs] << " " << arm_endpoints[2*n_secs+1]
                                     << " " << arm_endpoints[2*n_secs+2] << std::endl;
                            std::cout << "sec1 endpoint is " << arm_endpoints[1*n_secs] << " " << arm_endpoints[1*n_secs+1]
                                     << " " << arm_endpoints[1*n_secs+2] << std::endl;
                            #endif

                            if (arm_tip_diff < diff_th) {
                                printf("IK found in 5th try\n");
                                return true;
                            }
                            else {
                                return false;
                            }
                        }
                    }
                }
            }
        }
        else if ( computed_arc_len < (sec3_len_limit+sec2_len_limit) ){
        
        }
        else if ( computed_arc_len < (sec3_len_limit+sec2_len_limit+sec1_len_limit) ){
        
        }
        else
            std::cout << "Computed Arc is Too Long !" << std::endl;
    }
    else 
        std::cout << "Arc is not found !" << std::endl; 
        
    return false;
}

void environment_with_tactile_sensing::checkTipZaxisRotatedAngle()
{
     // if whole arm ik is not found
     // cannot check tip rotated angle
     // since arm config is ill-positioned
     if(!arm_ik_not_found_warning) {
         // get current arm tip frame wrt world
         std::vector<float> cur_arm_tip_frame(16,0.0);
         mani.compute_section_frame_wrt_world(cur_arm_tip_frame, 2);
         Eigen::Vector4f v1(cur_arm_tip_frame[0],cur_arm_tip_frame[4],cur_arm_tip_frame[8],cur_arm_tip_frame[12]);
         Eigen::Vector4f v2(cur_arm_tip_frame[1],cur_arm_tip_frame[5],cur_arm_tip_frame[9],cur_arm_tip_frame[13]);
         Eigen::Vector4f v3(cur_arm_tip_frame[2],cur_arm_tip_frame[6],cur_arm_tip_frame[10],cur_arm_tip_frame[14]);
         Eigen::Vector4f v4(cur_arm_tip_frame[3],cur_arm_tip_frame[7],cur_arm_tip_frame[11],cur_arm_tip_frame[15]);

         Eigen::Matrix4f arm_now_tip_frame;
         arm_now_tip_frame.row(0) = v1;
         arm_now_tip_frame.row(1) = v2;
         arm_now_tip_frame.row(2) = v3;
         arm_now_tip_frame.row(3) = v4;
         
         #ifdef DEBUGZANGLE
         std::cout << "arm_init_tip_frame is " << arm_init_tip_frame << std::endl;
         std::cout << "arm tip now frame is " << arm_now_tip_frame << std::endl;
         #endif
         
         Eigen::Matrix4f tf_now_to_init = (arm_init_tip_frame.inverse()) * arm_now_tip_frame;
         Eigen::VectorXd tip_transform = regular_eigen_tf_matrix_to_vector (tf_now_to_init);
         
         #ifdef DEBUGZANGLE
         std::cout << "arm tip tf is " << tip_transform << std::endl;
         #endif
         
         float angle_tip_rot = 0.0;
         if (std::abs(tip_transform[3]) > 1e-4 ) // if there exists non-zeron roll
            angle_tip_rot = M_PI - tip_transform[4];
         else {
            if (tip_transform[4] < 0.0)
                angle_tip_rot = 2.0*M_PI - std::abs(tip_transform[4]);
            else
                angle_tip_rot = tip_transform[4];
         }
         printf("[REPORT TIP Z-AXIS ROTATED ANGLE]\n");
         printf("current angle the tip z-axis rotated is %f (rad)\n", angle_tip_rot);
         if ( angle_tip_rot > angle_tip_rot_threshold)
             enough_tip_z_axis_rot_flag = true;
     }   
}

void environment_with_tactile_sensing::reportRobotStatus()
{
    /**********************
     * Collision Status
     * ********************/
    int n_of_secs = mani.get_n_sections();
    std::vector<bool> sec_status(n_of_secs,false);
    std::vector<int> sec_edge_inds(n_of_secs,-1);
    armPolygonCollisionCheck(cross_sec, sec_status, sec_edge_inds);
    printf("[REPORT ROBOT STATUS]\n");
    for (int i=0; i<n_of_secs; ++i)
    {
        if (sec_status[i])
            printf("Section %d is colliding with edge %d\n", i+1, sec_edge_inds[i]);
        else
            printf("Section %d is not in collision\n", i+1);
    }
    
    /**********************************
     * Number of Contacts collected so far
     * ********************************/    
    int num_of_contacts = 0;
    for(int j=0; j<contacts_collector.size(); ++j)
    {
        if (!contacts_collector[j].isEmpty)
            ++num_of_contacts;
    }
    printf("%d contacts have been collected.\n", num_of_contacts);
}

void environment_with_tactile_sensing::visualizeCollectedContacts()
{
     glMatrixMode(GL_MODELVIEW);
     glPushMatrix();
     glPointSize(6.0);
     glTranslatef(0.0, 0.0, 0.0);
     glColor3f(1.0, 1.0, 1.0);
     for(int j=0; j<contacts_collector.size(); ++j)
     {
         if(!contacts_collector[j].isEmpty)
         {
             glBegin(GL_POINTS);
                glVertex3f( contacts_collector[j].center[0],
                            contacts_collector[j].center[1],
                            contacts_collector[j].center[2]);
             glEnd();
         }  
     }
     glPointSize(1.0);
     glPopMatrix();
}

void environment_with_tactile_sensing::visualizeCrossSection()
{
    glLineWidth(6.0);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(0.0,0.0,0.0);
    glBegin(GL_LINES);
    for(int i=0; i<cross_sec.face_vertices.size(); ++i)
    {
        glColor3f(1.0, 0.0, 0.0);
        glVertex3f(cross_sec.face_vertices[i][0], 
                   cross_sec.face_vertices[i][1],
                   cross_sec.face_vertices[i][2]);
    }
    glEnd();
    glPopMatrix();
    glLineWidth(1.0);
}

void environment_with_tactile_sensing::visualizeEnvCrossSection()
{
    glLineWidth(6.0);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(0.0,0.0,0.0);
    glBegin(GL_LINES);
    for(int i=0; i<env_cross_sec.face_vertices.size(); ++i)
    {
        glColor3f(1.0, 0.0, 0.0);
        glVertex3f(env_cross_sec.face_vertices[i][0], 
                   env_cross_sec.face_vertices[i][1],
                   env_cross_sec.face_vertices[i][2]);
    }
    glEnd();
    glPopMatrix();
    glLineWidth(1.0);
}

void environment_with_tactile_sensing::visualizeInflatedCrossSection()
{
    glLineWidth(6.0);
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glTranslatef(0.0,0.0,0.0);
    glBegin(GL_LINES);
    for(int i=0; i<inflated_cross_sec.face_vertices.size(); ++i)
    {
        glColor3f(1.0,1.0,1.0);
        glVertex3f(inflated_cross_sec.face_vertices[i][0], 
                   inflated_cross_sec.face_vertices[i][1],
                   inflated_cross_sec.face_vertices[i][2]);
    }
    glEnd();
    glPopMatrix();
    glLineWidth(1.0);
}

bool environment_with_tactile_sensing::determineCurvatureAdjustSign(int sec_id)
{
    // get sec_id plane normal
    int n_secs = mani.get_n_sections();
    std::vector<float> sec_plane_normals(3*n_secs,0.0);
    mani.compute_section_plane_normals_wrt_world(sec_plane_normals);
    
    #ifdef CURVATURESIGNDEBUG
    printf("****************sec1 plane normal is (%f,%f,%f)\n", sec_plane_normals[0], sec_plane_normals[1], sec_plane_normals[2]);
    std::cout << "cross section face normal is " << cross_sec.face_normal << std::endl;
    #endif

    Eigen::Vector3f sec1_normal(sec_plane_normals[sec_id*3+0],
                                sec_plane_normals[sec_id*3+1],
                                sec_plane_normals[sec_id*3+2]);
    
    if (sec1_normal.dot(cross_sec.face_normal) > 0.0)
        return true;
    else 
        return false;
}

void environment_with_tactile_sensing::collectFullContacts()
{
     /* ********************************************************
      *  Step 1 inflate current cross section
      * ********************************************************/
     float inflate_ratio = 1.0;
     std::vector<float> inflate_factor(3, inflate_ratio);
     Eigen::Matrix4f inflate_scale;
     inflate_scale << inflate_factor[0], 0.0            ,   0.0,               0.0,
                      0.0,               inflate_factor[1], 0.0,               0.0,
                      0.0,               0.0            ,   inflate_factor[2], 0.0,
                      0.0,               0.0            ,   0.0,               1.0; 
     
     std::vector<Eigen::Vector3f> new_ver_list;
     for (int cnt=0; cnt<cross_sec.face_edges.size(); ++cnt)
     {
         Eigen::Vector4f new_pnt1 (cross_sec.face_edges[cnt].pnt1[0],
                                   cross_sec.face_edges[cnt].pnt1[1],
                                   cross_sec.face_edges[cnt].pnt1[2],
                                   1.0);
         Eigen::Vector4f new_pnt2 (cross_sec.face_edges[cnt].pnt2[0],
                                   cross_sec.face_edges[cnt].pnt2[1],
                                   cross_sec.face_edges[cnt].pnt2[2],
                                   1.0);
         new_pnt1 = inflate_scale * new_pnt1;
         new_pnt2 = inflate_scale * new_pnt2;
         
         new_ver_list.push_back(Eigen::Vector3f(new_pnt1[0],new_pnt1[1],new_pnt1[2]));
         new_ver_list.push_back(Eigen::Vector3f(new_pnt2[0],new_pnt2[1],new_pnt2[2]));
     }
     
     inflated_cross_sec.setFace(new_ver_list, cross_sec.face_normal);
     
    /* ********************************************************
     *  Step 2 collect contacts
     *  *******************************************************/
     std::vector<bool> full_col_status(mani.get_n_sections(),false);
     std::vector<int> full_colliding_edge_inds;
     armPolygonFullCollisionCheck(inflated_cross_sec, full_col_status, full_colliding_edge_inds);
     
     std::vector<ContactPatch> full_contacts;
     
     for (int i=0; i<full_colliding_edge_inds.size(); ++i) // capacity much greater than 3
     {
         Eigen::Vector3f cld_tan(0.0,0.0,0.0);
         Eigen::Vector3f cld_nor(0.0,0.0,0.0);
         
         ContactPatch non_empty_tmp_cp(cld_tan,cld_nor,inflated_cross_sec.face_edges[full_colliding_edge_inds[i]]);
         full_contacts.push_back(non_empty_tmp_cp);
     }
     // collect contact results
     contacts_collector.insert(contacts_collector.end(), full_contacts.begin(), full_contacts.end());
}

void environment_with_tactile_sensing::resetEnv(int ort_choice, float proportion)
{
    std::cout << std::endl;
    std::cout << "**************************************" << std::endl;
    std::cout << "Reseting Environment..." << std::endl;

    /*******************************
     * 1. Reset control flags
     * *****************************/
    arm_physical_limit_warning = false;
    arm_ik_not_found_warning = false;
    enough_tip_z_axis_rot_flag = false;
    final_sequeeze_finished_flag = false;
    empty_cross_sec_flag = false;
    std::cout << "Control flags are reset." << std::endl;
    
    /*******************************
     * 2. Reset arm config and base frame
     * *****************************/ 
    int n_secs = mani.get_n_sections();
    std::vector<float> init_config(3*n_secs, 0.0);
    for (int i=0; i<init_config.size(); i=i+3)
    {
        init_config[i] = 0.095;
        init_config[i+1] = 1e-1;
        init_config[i+2] = 0.;
    }
    
    /*******************************
     * 2-A Choose arm initial configs
     * *****************************/
    int arm_base_init_ort = ort_choice;
    std::vector<float> mani_base_tf_vec;
    float cros_sec_plane_theta = 0.0;
    std::vector<float> plane_normal;
    switch(arm_base_init_ort)
    {
        case 1: // init ort along world y-axis
        {
            /*Initialize arm base frame*/
            float prop = proportion;

            cros_sec_plane_theta = prop*M_PI;
            float arm_base_pos [3] = {0.0};               
            float arm_base_x [3] = {1.0, 0.0, 0.0};                     
            float arm_base_y [3] = {0.0, sin(cros_sec_plane_theta), -cos(cros_sec_plane_theta)};                    
            float arm_base_z [3] = {0.0, cos(cros_sec_plane_theta), sin(cros_sec_plane_theta)};

            Eigen::Matrix4f mani_base_frame;
            mani_base_frame << arm_base_x[0],   arm_base_y[0],   arm_base_z[0],  arm_base_pos[0], 
                               arm_base_x[1],   arm_base_y[1],   arm_base_z[1],  arm_base_pos[1], 
                               arm_base_x[2],   arm_base_y[2],   arm_base_z[2],  arm_base_pos[2], 
                               0.0, 0.0, 0.0, 1.0; 
            for (int i=0; i<4; ++i)
            {
                for (int j=0; j<4; ++j)
                {
                    mani_base_tf_vec.push_back(mani_base_frame(j,i));
                }
            }
            sec_curvature_adjust_rate = std::abs(sec_curvature_adjust_rate);

            /*Initialize cutting cross section normal*/
            plane_normal.push_back(0.0);
            plane_normal.push_back(sin(cros_sec_plane_theta));
            plane_normal.push_back(-cos(cros_sec_plane_theta));

            break;
        }
        case 2: // init ort along world x-axis
        {
            /*Initialize arm base frame*/
            float prop = proportion;

            cros_sec_plane_theta = prop*M_PI;
            float arm_base_pos [3] = {0.0};               
            float arm_base_x [3] = {0.0, 1.0, 0.0};                     
            float arm_base_y [3] = {-sin(cros_sec_plane_theta), 0.0, cos(cros_sec_plane_theta)};                    
            float arm_base_z [3] = {cos(cros_sec_plane_theta), 0.0, sin(cros_sec_plane_theta)};

            Eigen::Matrix4f mani_base_frame;
            mani_base_frame << arm_base_x[0],   arm_base_y[0],   arm_base_z[0],  arm_base_pos[0], 
                               arm_base_x[1],   arm_base_y[1],   arm_base_z[1],  arm_base_pos[1], 
                               arm_base_x[2],   arm_base_y[2],   arm_base_z[2],  arm_base_pos[2], 
                               0.0, 0.0, 0.0, 1.0; 
            
            for (int i=0; i<4; ++i)
            {
                for (int j=0; j<4; ++j)
                {
                    mani_base_tf_vec.push_back(mani_base_frame(j,i));
                }
            }
            sec_curvature_adjust_rate = std::abs(sec_curvature_adjust_rate);

            /*Initialize cutting cross section normal*/
            plane_normal.push_back(-sin(cros_sec_plane_theta));
            plane_normal.push_back(0.0);
            plane_normal.push_back(cos(cros_sec_plane_theta));
            break;
        }
        case 3: // init ort along world z-axis
        {
            /*Initialize arm base frame*/
            float prop = proportion;

            cros_sec_plane_theta = prop*M_PI;
            float arm_base_pos [3] = {0.0};               
            float arm_base_x [3] = {sin(cros_sec_plane_theta), cos(cros_sec_plane_theta), 0.0};                     
            float arm_base_y [3] = {-cos(cros_sec_plane_theta), sin(cros_sec_plane_theta), 0.0};                    
            float arm_base_z [3] = {0.0, 0.0, 1.0};

            Eigen::Matrix4f mani_base_frame;
            mani_base_frame << arm_base_x[0],   arm_base_y[0],   arm_base_z[0],  arm_base_pos[0], 
                               arm_base_x[1],   arm_base_y[1],   arm_base_z[1],  arm_base_pos[1], 
                               arm_base_x[2],   arm_base_y[2],   arm_base_z[2],  arm_base_pos[2], 
                               0.0, 0.0, 0.0, 1.0; 
            
            for (int i=0; i<4; ++i)
            {
                for (int j=0; j<4; ++j)
                {
                    mani_base_tf_vec.push_back(mani_base_frame(j,i));
                }
            }
            sec_curvature_adjust_rate = std::abs(sec_curvature_adjust_rate);

            /*Initialize cutting cross section normal*/
            plane_normal.push_back(-cos(cros_sec_plane_theta));
            plane_normal.push_back(sin(cros_sec_plane_theta));
            plane_normal.push_back(0.0);
            break;
        }
    }   
    
    mani.update_config(init_config, mani_base_tf_vec);
    
    /************************************************
     2-B Reset arm_init_tip_frame
         (for checking tip z-axis rotated angle)
     ************************************************/
    std::vector<float> init_arm_tip_frame(16,0.0);
    mani.compute_section_frame_wrt_world(init_arm_tip_frame, 2);
    Eigen::Vector4f v1(init_arm_tip_frame[0],init_arm_tip_frame[4],init_arm_tip_frame[8],init_arm_tip_frame[12]);
    Eigen::Vector4f v2(init_arm_tip_frame[1],init_arm_tip_frame[5],init_arm_tip_frame[9],init_arm_tip_frame[13]);
    Eigen::Vector4f v3(init_arm_tip_frame[2],init_arm_tip_frame[6],init_arm_tip_frame[10],init_arm_tip_frame[14]);
    Eigen::Vector4f v4(init_arm_tip_frame[3],init_arm_tip_frame[7],init_arm_tip_frame[11],init_arm_tip_frame[15]);
    arm_init_tip_frame.row(0) = v1;
    arm_init_tip_frame.row(1) = v2;
    arm_init_tip_frame.row(2) = v3;
    arm_init_tip_frame.row(3) = v4;
    std::cout << "arm_init_tip_frame is reset as " << std::endl;
    std::cout << arm_init_tip_frame << std::endl;
    std::cout << "Arm is reset." << std::endl;
    std::cout << std::endl;

    /**********************************
     3 Reset cross section
    **********************************/
    std::vector<float> pnt_on_plane(3,0.0);
    cross_sec = mesh.planeIntersectTriMesh(plane_normal, pnt_on_plane);
    std::cout << "Cross section is re-computed." << std::endl;
    if (cross_sec.face_edges.empty()) 
    {
        empty_cross_sec_flag = true;
        ROS_WARN("Empty cross section flag set");
    }
    
    /**********************************
     4 reset contact collector
    **********************************/
    contacts_collector.clear();
}

void environment_with_tactile_sensing::resetEnvByPlnNormal(geometry_msgs::Vector3 &plane_normal)
{
    Eigen::Vector3f pln_nrm(plane_normal.x,
                            plane_normal.y,
                            plane_normal.z);
    Eigen::Vector3f world_z_pos(0.0,0.0,1.0);
    Eigen::Vector3f world_z_neg(0.0,0.0,-1.0);
    int ort_choice = 0;
    float prop = 0.0;
    /* 1. Decide arm base frame */
    float sign_world_z_neg = pln_nrm.dot(world_z_neg);
    if (sign_world_z_neg > 0)
    {
        ort_choice = 1;
        prop = acos(sign_world_z_neg);
    }
    else
    {
        ort_choice = 2;
        float ang_nrm_and_z_pos = pln_nrm.dot(world_z_pos);
        prop = acos(ang_nrm_and_z_pos);
    }
    resetEnv(ort_choice, prop);
}

void environment_with_tactile_sensing::generateChords(const std::vector<float> &arm_config,
                                                      const std::vector<float> &arm_base_tf)
{
    mani.update_config(arm_config, arm_base_tf);
    
    /************************
     * Generate points on arm
     * **********************/
    int n_secs = mani.get_n_sections();
    std::vector<secPnts> sec_pnts;
    sec_pnts.resize(n_secs);
    float delta_s = 0.01; // 0.02
    mani.generatePointsOnArm(sec_pnts, delta_s);

    for (int i=0; i<n_secs; ++i)
        std::cout << "sec " << i << " has " << sec_pnts[i].size() << " pnts" << std::endl;
    
    std::vector<float> sec_cir_centers(3*n_secs,0.0);
    mani.compute_section_cir_centers_wrt_world(sec_cir_centers);
    // Eigen::Vector3f sec1_cir_cen(sec_cir_centers[0],sec_cir_centers[1],sec_cir_centers[2]);
    // Eigen::Vector3f sec2_cir_cen(sec_cir_centers[3],sec_cir_centers[4],sec_cir_centers[5]);
    // Eigen::Vector3f sec3_cir_cen(sec_cir_centers[6],sec_cir_centers[7],sec_cir_centers[8]);
    
    std::vector<ChordPoint> chordpnts;
    for (int i=0; i<n_secs; ++i)
    {
        Eigen::Vector3f sec_cir_cen(sec_cir_centers[3*i],sec_cir_centers[3*i+1],sec_cir_centers[3*i+2]);
        for (int j=0; j<sec_pnts[i].size(); ++j)
        {
            Eigen::Vector3f pnt_tmp = sec_pnts[i][j];
            Eigen::Vector3f pnt_normal = pnt_tmp - sec_cir_cen;
            pnt_normal.normalize();
            ChordPoint tmp_cp(pnt_tmp,pnt_normal);
            chordpnts.push_back(tmp_cp);
        }
    }
    
    #ifdef VISUALIZESAMPLEDPNTS
    glPointSize(15.0);
    for(int i=0; i<chordpnts.size(); ++i)
    {
        glBegin(GL_POINTS);
            glColor3f(1.0, 0.0, 0.0);
            glVertex3f(chordpnts[i].point[0],chordpnts[i].point[1],chordpnts[i].point[2]);
        glEnd();
    }
    glPointSize(1.0);
    #endif
    
    /************************
     * Generate chord pair pnts
     * **********************/
    int total_cps = chordpnts.size();
    std::cout << total_cps << " points sampled on arm" << std::endl;
    std::vector<std::pair<int, int> > chord_pairs;
    
    int chord_pair_mode;
    std::cout << "Choose how points are paired(no duplicates)" << std::endl;
    std::cout << "1. Randomly pair one point with another" << std::endl;
    std::cout << "2. Exhaustively" << std::endl;
    std::cout << "Choose >> (currently choose 2 in default)" << std::endl;
    //~ std::cin >> test_case;
    chord_pair_mode = 2;
    
    switch(chord_pair_mode)
    {
        case 1:
        {
            srand (time(NULL)); // should only call this once
            for(int i=0; i<chordpnts.size(); ++i)
            {
                int nextPnt = rand() % total_cps + 0;
                
                while (nextPnt == i)
                {
                    nextPnt = rand() % total_cps + 0;
                }
                
                std:pair<int, int> pnt_pair;
                pnt_pair = std::make_pair(i, nextPnt);
                
                // check to prevent duplicates
                bool duplicate = false;
                for(int j=0; j<chord_pairs.size(); ++j)
                {
                    if(chord_pairs[j].first == i && chord_pairs[j].second == nextPnt)
                    {
                        std::cout << "Duplicate Chord Found!" << std::endl;
                        duplicate = true;
                        break;
                    }
                    else if (chord_pairs[j].first == nextPnt && chord_pairs[j].second == i)
                    {
                        std::cout << "Duplicate Chord Found!" << std::endl;
                        duplicate = true;
                        break;
                    }
                    else
                        duplicate = false;
                }
                
                if (!duplicate)     
                {
                    chord_pairs.push_back(pnt_pair);
                    //~ std::cout << "pnt " << pnt_pair.first << " and pnt " << pnt_pair.second << std::endl;
                }
            }
            break;
        }
        case 2:
        {
            for(int i=0; i<chordpnts.size(); ++i)
            {
                for(int j=i+1; j<chordpnts.size(); ++j)
                {
                    std::pair<int, int> pnt_pair;
                    pnt_pair = std::make_pair(i, j);
                    
                    // check to prevent duplicates
                    bool duplicate = false;
                    for(int k=0; k<chord_pairs.size(); ++k)
                    {
                        if(chord_pairs[k].first == i && chord_pairs[k].second == j)
                        {
                            std::cout << "Duplicate Chord Found!" << std::endl;
                            duplicate = true;
                            break;
                        }
                        else if (chord_pairs[k].first == j && chord_pairs[k].second == i)
                        {
                            std::cout << "Duplicate Chord Found!" << std::endl;
                            duplicate = true;
                            break;
                        }
                        else
                            duplicate = false;
                    }
                    
                    if (!duplicate)     
                    {
                        chord_pairs.push_back(pnt_pair);
                        // std::cout << "pnt " << pnt_pair.first << " and pnt " << pnt_pair.second << std::endl;
                    }
                }
            }
            break;
        }
    }
    

    
    /************************
     * Make chords
     * **********************/
        
    for(int i=0; i<chord_pairs.size(); ++i)
    {   
        int ind1 = chord_pairs[i].first;
        int ind2 = chord_pairs[i].second;
        
        Eigen::Vector3f chord_pair_vec = chordpnts[ind1].point - chordpnts[ind2].point;
        Eigen::Vector3f chord_pair_vec_sp(0.0,0.0,0.0);
        Eigen::Vector3f n0_sp(0.0,0.0,0.0);
        Eigen::Vector3f n1_sp(0.0,0.0,0.0);
        cartesian2sphere(chord_pair_vec, chord_pair_vec_sp);
        cartesian2sphere(chordpnts[ind1].normal, n0_sp);
        cartesian2sphere(chordpnts[ind2].normal, n1_sp);
        
        chord_training_msgs::Chord cur_chord;
        cur_chord.l = chord_pair_vec.norm();
        cur_chord.c_lon = chord_pair_vec_sp[1];
        cur_chord.c_lat = chord_pair_vec_sp[2];
        cur_chord.n0_lon = n0_sp[1];
        cur_chord.n0_lat = n0_sp[2];
        cur_chord.n1_lon = n1_sp[1];
        cur_chord.n1_lat = n1_sp[2];
        
        chords.push_back(cur_chord);
    }

    #ifdef VISCHORDS
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLineWidth(1.0);
    glBegin(GL_LINES);
    for(int i=0; i<chord_pairs.size(); ++i)
    {   
        glColor3f(0.0, 0.0, 1.0);
        int ind1 = chord_pairs[i].first;
        int ind2 = chord_pairs[i].second;
        glVertex3f(chordpnts[ind1].point[0] ,chordpnts[ind1].point[1],chordpnts[ind1].point[2]);
        glVertex3f(chordpnts[ind2].point[0] ,chordpnts[ind2].point[1],chordpnts[ind2].point[2]);
        
        glColor3f(0.0, 0.5, 0.5);
        glVertex3f(chordpnts[ind1].point[0] ,chordpnts[ind1].point[1],chordpnts[ind1].point[2]);
        Eigen::Vector3f pnt_along_normal = chordpnts[ind1].point + 0.1*chordpnts[ind1].normal;
        glVertex3f(pnt_along_normal[0] ,pnt_along_normal[1],pnt_along_normal[2]);
    }
    glEnd();
    glPopMatrix();
    #endif

}

void environment_with_tactile_sensing::cartesian2sphere(Eigen::Vector3f &pnt_in_cart,
                                                        Eigen::Vector3f &pnt_in_sp)
{
    float r = pnt_in_cart.norm();
    float phi = acos(pnt_in_cart[1]/r);
    float theta = atan2(pnt_in_cart[1], pnt_in_cart[0]);
    pnt_in_sp[0] = r;
    pnt_in_sp[1] = phi;
    pnt_in_sp[2] = theta;
}

void environment_with_tactile_sensing::writeChords()
{
    /************************
     * Save chord data
     * **********************/
    std::cout << std::endl;
    std::cout << "**************************************" << std::endl;
    std::cout << "Saving chords to csv files..." << std::endl;

    if (!chords.empty())
    {
        for (int i=0; i<chords.size(); ++i)
        {
            chord_training_msgs::ChordSrv chord_srv;
            chord_srv.request.valid = true;
            chord_srv.request.chord = chords[i];
            
            if (i != (chords.size()-1) )
                chord_srv.request.finished = false; // set to true if this is the last chord
            else
                chord_srv.request.finished = true; 
            
            if(!client.call(chord_srv))
            {
                ROS_ERROR("Failed to call service chord_srv.");
            }
        }
    }
    std::cout << chords.size() << " chords have been written to file" << std::endl;
}

void environment_with_tactile_sensing::writeProbabilities(std::string &cat, 
                                                          std::string &base,
                                                          int hist_cnt,
                                                          bool valid_signal,
                                                          bool finish_signal)
{
    chord_training_msgs::ProbWriterSrv prob_srv;
    prob_srv.request.mode = "gl";
    prob_srv.request.obj_cat = cat;
    prob_srv.request.obj_base = base;
    prob_srv.request.disc_l = 0.05;
    prob_srv.request.disc_ang = 0.05;
    prob_srv.request.disc_pose = 0.05;
    geometry_msgs::Vector3 cutting_pln_normal;
    cutting_pln_normal.x = cross_sec.face_normal[0];
    cutting_pln_normal.y = cross_sec.face_normal[1];
    cutting_pln_normal.z = cross_sec.face_normal[2];

    prob_srv.request.abs_pose = cutting_pln_normal;

    // get a subvector of chords
    // std::vector<chord_training_msgs::Chord>::const_iterator first = chords.begin();
    // std::vector<chord_training_msgs::Chord>::const_iterator last = chords.begin()+10000;
    // std::vector<chord_training_msgs::Chord> sub_chords(first, last);
    // prob_srv.request.chords = sub_chords;
    
    // prob_srv.request.chords = chords;
    std::ostringstream hist_cnt_str;
    hist_cnt_str << hist_cnt;

    prob_srv.request.chord_name = "/home/huitan/collaboration/train/chord_sampling/csv_gl_chords/"
                                   + cat + "/" + base + "/" + base + "_" + hist_cnt_str.str() + ".csv";
    prob_srv.request.valid = valid_signal;
    prob_srv.request.finished = finish_signal;

    if(!prob_client.call(prob_srv))
    {
       ROS_ERROR("Failed to call service prob_writer_srv.");
    }
}

void environment_with_tactile_sensing::takeScreenshot(const std::string &obj_name)
{
    int width = 900;
    int height = 700;
    // Make the BYTE array, factor of 3 because it's RBG.
    BYTE* pixels = new BYTE[ 3 * width * height];

    glReadPixels(0, 0, width, height, GL_BGR, GL_UNSIGNED_BYTE, pixels);
    // Convert to FreeImage format & save to file
    FIBITMAP* image = FreeImage_ConvertFromRawBits(pixels, width, height, 3 * width, 24, 0x0000FF, 0xFF0000, 0x00FF00, false);
    
    std::string pkg_path = ros::package::getPath("touch_continuum_wraps");
    
    ostringstream convert;
    convert << num_of_screenshots;

    string num_of_ss_str = convert.str();

    std::string ss_path = pkg_path + "/screenshots/" + obj_name + "_" + num_of_ss_str +".bmp";

    FreeImage_Save(FIF_BMP, image, ss_path.c_str(), 0);
    std::cout << "screenshot saved as " << ss_path << std::endl;
    ++num_of_screenshots;
    
    // Free resources
    FreeImage_Unload(image);
    delete [] pixels;
}

