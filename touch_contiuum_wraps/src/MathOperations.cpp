#include "MathOperations.h"
#include <opencv2/core/core.hpp>
#include <iostream>
using namespace std;
float* MatrixMulti(float* a, float* b, int R_1, int C_1, int R_2, int C_2)
{
	if(R_2 != C_1)
	{
		return 0;
	}
	float* c = new float [R_1*C_2];
	for(int i = 0; i < C_2; ++i)//columns
	{

		for(int j = 0; j <R_1; ++j)//rows
		{
			c[i*R_1+j] = 0;
		    for(int k = 0; k < C_1; ++k)
			{
				c[i*R_1+j] += a[k*R_1+j]*b[i*R_2+k];
			}
		}

	}
	return c;
}



float* MatrixMulti(vector<float> a, float* b, int R_1, int C_1, int R_2, int C_2)
{
	if(R_2 != C_1)
	{
		return 0;
	}
	float* c = new float [R_1*C_2];
	for(int i = 0; i < C_2; ++i)//columns
	{

		for(int j = 0; j <R_1; ++j)//rows
		{
			c[i*R_1+j] = 0;
		    for(int k = 0; k < C_1; ++k)
			{
				c[i*R_1+j] += a[k*R_1+j]*b[i*R_2+k];
			}
		}

	}
	return c;
}

float* MatrixMulti(float* a, vector<float> b, int R_1, int C_1, int R_2, int C_2)
{
	if(R_2 != C_1)
	{
		return 0;
	}
	float* c = new float [R_1*C_2];
	for(int i = 0; i < C_2; ++i)//columns
	{

		for(int j = 0; j <R_1; ++j)//rows
		{
			c[i*R_1+j] = 0;
		    for(int k = 0; k < C_1; ++k)
			{
				c[i*R_1+j] += a[k*R_1+j]*b[i*R_2+k];
			}
		}

	}
	return c;
}

float* MatrixMulti(vector<float> a, vector<float> b, int R_1, int C_1, int R_2, int C_2)
{
	if(R_2 != C_1)
	{
		return 0;
	}
	float* c = new float [R_1*C_2];
	for(int i = 0; i < C_2; ++i)//columns
	{

		for(int j = 0; j <R_1; ++j)//rows
		{
			c[i*R_1+j] = 0;
		    for(int k = 0; k < C_1; ++k)
			{
				c[i*R_1+j] += a[k*R_1+j]*b[i*R_2+k];
			}
		}

	}
	return c;
}

bool Normalize(vector<float> &_vector)
{
	if(_vector.size() != 0)
	{
		int dim = _vector.size();
		float vector_length = 0;
		for(int i = 0; i < dim; ++i)
		{
			vector_length += (_vector[i]*_vector[i]);
		}
		vector_length = sqrt(vector_length);

		for(int i = 0; i < dim; ++i)
		{
			_vector[i] = _vector[i] / vector_length;
		}
		return true;
	}
	else
	{
		return false;
	}

}

void CrossProduct(float*a, float*b, float*result)
{
	result[0] = a[1]*b[2] - a[2]*b[1];
	result[1] = a[2]*b[0] - a[0]*b[2];
	result[2] = a[0]*b[1] - a[1]*b[0];
}

float* CrossProduct(float* a, float* b)
{
	float * result= new float [3];
	result[0] = a[1]*b[2] - a[2]*b[1];
	result[1] = a[2]*b[0] - a[0]*b[2];
	result[2] = a[0]*b[1] - a[1]*b[0];

	return result;
}

void DotProduct(float*a, float*b, float* result)
{
	*result = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];

}

float  two_pts_dist(float* p1, float* p2, int dim)
{
	float dist = 0;
	for(int i = 0; i < dim; ++i)
	{
		dist += (p2[i] - p1[i]) * (p2[i] - p1[i]);
	}
	dist = sqrt(dist);
	return dist;
}

float  two_pts_dist(vector<float> p1, vector<float> p2)
{
	float dist = 0;
	int dim = p1.size();
	if (dim != p2.size() || dim == 0)
	{
		return -1;
	}
	for(int i = 0; i < dim; ++i)
	{
		dist += (p2[i] - p1[i]) * (p2[i] - p1[i]);
	}
	dist = sqrt(dist);
	return dist;
}

float two_pts_dist(Eigen::Vector3f p1, Eigen::Vector3f p2)
{
	float dist = 0;
	int dim = p1.size();
	if (dim != p2.size() || dim == 0)
	{
		return -1;
	}
	for(int i = 0; i < dim; ++i)
	{
		dist += (p2[i] - p1[i]) * (p2[i] - p1[i]);
	}
	dist = sqrt(dist);
	return dist;
}

float DotProduct(float*a, float*b)
{
	float result = a[0]*b[0] + a[1]*b[1] + a[2]*b[2];
	return result;
}


float* TwolineIntersection(float* Pa, float* Pb,float* va, float* vb)
{

	float x1[] = {Pa[0],Pa[1],Pa[2]};
	float x2[] = {Pa[0]+va[0],Pa[1]+va[1],Pa[2]+va[2]};

	float x3[] = {Pb[0],Pb[1],Pb[2]};
	float x4[] = {Pb[0]+vb[0],Pb[1]+vb[1],Pb[2]+vb[2]};

	float vc[] = {x3[0] - x1[0], x3[1] - x1[1], x3[2] - x1[2]};
	float CCrossB[3] = {0};
	float ACrossB[3] = {0};
	float CBDotAB = 0;
	CrossProduct(vc,vb,CCrossB);
	CrossProduct(va,vb,ACrossB);
    DotProduct(CCrossB,ACrossB,&CBDotAB);

	float* Center= new float [3];

	float ACrossBLen = sqrtf(ACrossB[0]*ACrossB[0] + ACrossB[1]*ACrossB[1] + ACrossB[2]*ACrossB[2]);


	if(fabsf(ACrossBLen)<=0.0001)
	{
		return NULL;
	}
	else
	{
		float s = CBDotAB/(ACrossBLen*ACrossBLen);
		float _InterP[3] = {Pa[0]+s*va[0], Pa[1]+s*va[1],Pa[2]+s*va[2]};

		for(int i= 0;i<3;++i)
		{Center[i] = _InterP[i];}
	}

	return Center;
}

float Angle_bewteen_two_vecs(float* v_1, float* v_2)
{
	Normalize(v_1,3);
	Normalize(v_2,3);
	float dot_product = DotProduct(v_1,v_2);

	return acos(dot_product);
}

void Normalize(float* a, int dim)
{
	float length = 0;
	for(int i =0; i < dim; ++i)
	{
		length += a[i]*a[i];
	}
	length = sqrt(length);

	for(int i =0; i < dim; ++i)
	{
		a[i] = a[i]/ length;
	}
}

_Point::_Point()
{
		x = 0;
		y = 0;
		z = 0;
}

_Point::_Point(float x_, float z_)
	{
		x = x_;
		y = 0;
		z = z_;
	}

_Point::_Point(float x_, float y_, float z_)
	{
		x = x_;
		y = y_;
		z = z_;
	}
void _Point:: Normalize()
	{
		float length = sqrt(x*x + y*y + z*z);
		x = x/length; y = y/length; z = z/length;
	}

void _Point::SetValue(float x_, float z_)
	{
		x = x_;
		z = z_;
	}
void _Point:: SetValue(float x_,float y_, float z_)
	{
		x = x_;
		z = z_;
		y = y_;
	}

float _Point::GetLength()
	{
		float length = sqrt(x*x + y*y + z*z);
		return length;
	}

float* InverMatrix(float* Matrix)
{
	float CurMatrix[16] = {0};
	float test1[16] = {0};
	for(int i = 0; i < 16; ++i)
	{
		CurMatrix[i] = Matrix[i];
	}
	float* result= new float [16];
	cv::Mat M = cv::Mat(4,4,CV_32FC1,CurMatrix);
	M = M.t();
	cv::Mat Inv_M = M.inv();
	for(int i = 0; i < 4; ++i)
	{
		for(int j = 0; j < 4; ++j)
		{
			result[i*4+j] = Inv_M.at<float>(j,i);

		}
	}
	return result;
}

float* InverMatrix(float* Matrix, int _row, int _column) //All in column Majors
{
	int total_elements = _row*_column;
	float* CurMatrix = new float[total_elements];
	for(int i = 0; i < total_elements; ++i)
	{
		CurMatrix[i] = Matrix[i];
	}
	float* result= new float [total_elements];
	cv::Mat M = cv::Mat(_column,_row,CV_32FC1,CurMatrix);
	M = M.t();
	cv::Mat Inv_M = M.inv(cv::DECOMP_SVD);
	for(int i = 0; i < _row; ++i)
	{
		for(int j = 0; j < _column; ++j)
		{
			result[i*_column+j] = Inv_M.at<float>(j,i);
		}
	}
	return result;
}

bool plane_rotating_angle(float* _x_af_rot, float* _y_af_rot, float* _z_axis, float* _point1_on_z, float* _point2)
{
	float vector_p2_p1[3] = {_point2[0] - _point1_on_z[0], _point2[1] - _point1_on_z[1], _point2[2] - _point1_on_z[2]};
	CrossProduct(_z_axis,vector_p2_p1,_y_af_rot);
	Normalize(_y_af_rot,3);
	CrossProduct(_y_af_rot,_z_axis,_x_af_rot);
	Normalize(_x_af_rot,3);
	return true;
}

float line_line_dist_3D(float* l1_p1, float* l1_vec, float* l2_p1, float* l2_p2)
{
	float l2_vec [3] = {l2_p2[0] - l2_p1[0], l2_p2[1] - l2_p1[1], l2_p2[2] - l2_p1[2]};
	float commonN [3] = {0};
	CrossProduct(l1_vec,l2_vec,commonN);
	Normalize(commonN,3);

	float vectorl2_l1[3] = {l2_p2[0] - l1_p1[0], l2_p2[1] - l1_p1[1], l2_p2[2] - l1_p1[2]};
	float dis = 0;
	DotProduct(vectorl2_l1,commonN,&dis);
	return fabsf(dis);
}

float point_lineseg_dist_3D(float* p, float* lineseg_p1, float* lineseg_p2)
{
	float vec_p2_p1 [3] = {lineseg_p2[0] - lineseg_p1[0], lineseg_p2[1] - lineseg_p1[1],lineseg_p2[2] - lineseg_p1[2]};
	float vec_p1_p0 [3] = {lineseg_p1[0] - p[0], lineseg_p1[1] - p[1],lineseg_p1[2] - p[2]};
	float vec_p2_p0 [3] = {lineseg_p2[0] - p[0], lineseg_p2[1] - p[1],lineseg_p2[2] - p[2]};

	float length_v_p2_p1 = sqrt(vec_p2_p1[0]*vec_p2_p1[0]+vec_p2_p1[1]*vec_p2_p1[1]+vec_p2_p1[2]*vec_p2_p1[2]);
	float length_v_p1_p0 = sqrt(vec_p1_p0[0]*vec_p1_p0[0]+vec_p1_p0[1]*vec_p1_p0[1]+vec_p1_p0[2]*vec_p1_p0[2]);
	float length_v_p2_p0 = sqrt(vec_p2_p0[0]*vec_p2_p0[0]+vec_p2_p0[1]*vec_p2_p0[1]+vec_p2_p0[2]*vec_p2_p0[2]);

	float crossP_res [3] = {0};
	CrossProduct(vec_p2_p1,vec_p1_p0,crossP_res);
	float length_croP = sqrt(crossP_res[0]*crossP_res[0]+crossP_res[1]*crossP_res[1]+crossP_res[2]*crossP_res[2]);
	float dis_infin = (length_croP/length_v_p2_p1);

	float temp_dotP = 0;
	DotProduct(vec_p1_p0,vec_p2_p1,&temp_dotP);
	float t = -temp_dotP/(length_v_p2_p1*length_v_p2_p1);
	if(t>0 && t<1)
	{
		return dis_infin;
	}
	else
	{
		return ((length_v_p1_p0 < length_v_p2_p0)?length_v_p1_p0:length_v_p2_p0);
	}

}

bool Proj_Pt_to_Plane (float point_to_proj [3], float point_on_plane[3], float plane_normal[3], float projection_des [3])
{

	float t_point_to_proj_to_plane_tmp1 = plane_normal[0]*(point_to_proj[0] - point_on_plane[0]) +  plane_normal[1]*(point_to_proj[1] - point_on_plane[1]) + plane_normal[2]*(point_to_proj[2] - point_on_plane[2]);
	float t_point_to_proj_to_plane_tmp2 = plane_normal[0]*plane_normal[0] + plane_normal[1]*plane_normal[1] + plane_normal[2]*plane_normal[2];

	float t = -t_point_to_proj_to_plane_tmp1/t_point_to_proj_to_plane_tmp2;

	for(int i = 0; i < 3; ++i)
	{
		projection_des[i] = point_to_proj[i] + t*plane_normal[i];
	}
	return true;
}

bool Proj_vec_to_Plane (float vec [3], float vec_base [3], float point_on_plane[3], float plane_normal[3], float projection_des [3])
{
	float base_projection [3] = {0};
	Proj_Pt_to_Plane(vec_base,point_on_plane,plane_normal,base_projection);

	float a_point_on_the_ray[3] = {vec_base[0] + 1.0*vec[0], vec_base[1] + 1.0*vec[1], vec_base[2] + 1.0*vec[2]};
	float a_point_projection [3] = {0};
	Proj_Pt_to_Plane(a_point_on_the_ray,point_on_plane,plane_normal,a_point_projection);

	projection_des[0] = a_point_projection[0] - base_projection[0];
	projection_des[1] = a_point_projection[1] - base_projection[1];
	projection_des[2] = a_point_projection[2] - base_projection[2];
	Normalize(projection_des,3);

	return true;
}

float norm_vec_3 (float vec [3])
{
  float res = vec[0]*vec[0] + vec[1]*vec[1] + vec[2]*vec[2];
  return sqrtf(res);
}

bool Point2LinesegDist3D(float p1seg[3], float p2seg[3], float p[3], vector<float>& output)// output is in format of float dist,float t, float cl_p_on_seg
{
    float vec_p_to_p1 [3] ={p1seg[0] - p[0], p1seg[1] - p[1], p1seg[2] - p[2]};
    float vec_p1_to_p2 [3] ={p2seg[0] - p1seg[0], p2seg[1] - p1seg[1], p2seg[2] - p1seg[2]};
    float Dot_two_vecs = DotProduct(vec_p_to_p1,vec_p1_to_p2);
    float norm_vec_p1_to_p2 = norm_vec_3(vec_p1_to_p2);
    float t = - Dot_two_vecs/(norm_vec_p1_to_p2*norm_vec_p1_to_p2);
    float dist_p_to_lineseg = 0.0;
    float cl_p_on_seg [3] = {0.0};
    if(t < 0 || t > 1)
    {
        float dis_p_to_p1 = two_pts_dist(p,p1seg,3);
        float dis_p_to_p2 = two_pts_dist(p,p2seg,3);
        dist_p_to_lineseg = (dis_p_to_p1< dis_p_to_p2) ? dis_p_to_p1 : dis_p_to_p2;
        for(int i = 0; i < 3; ++i)
        {
            cl_p_on_seg[i] = (dis_p_to_p1< dis_p_to_p2) ? p1seg[i] : p2seg[i];
        }
    }
    else
    {
        for(int i = 0; i < 3; ++i)
        {
            cl_p_on_seg[i] = p1seg[i] + vec_p1_to_p2[i]*t;
        }
        dist_p_to_lineseg =two_pts_dist(p,cl_p_on_seg,3);
    }
    output[0] = dist_p_to_lineseg;
    output[1] = t;
    output[2] = cl_p_on_seg[0];
    output[3] = cl_p_on_seg[1];
    output[4] = cl_p_on_seg[2];
    return true;
}

vector<float>  plane_edge_intersection(vector<float> plane_normal, vector<float> a_point_on_plane, vector<float> edge_p1, vector<float> edge_p2)
{
	vector<float> result;
	float sign_point1 = (edge_p1[0] - a_point_on_plane[0])*plane_normal[0] + (edge_p1[1] - a_point_on_plane[1])*plane_normal[1] + (edge_p1[2] - a_point_on_plane[2])*plane_normal[2];
	
	float sign_point2 = (edge_p2[0] - a_point_on_plane[0])*plane_normal[0] + (edge_p2[1] - a_point_on_plane[1])*plane_normal[1] + (edge_p2[2] - a_point_on_plane[2])*plane_normal[2];
	
	
	if(sign_point1*sign_point2 > 0)
	{
		return result;
	}
	
	float r1_2U = plane_normal[0]*(a_point_on_plane[0] - edge_p1[0]) + plane_normal[1]*(a_point_on_plane[1] - edge_p1[1]) + plane_normal[2]*(a_point_on_plane[2] - edge_p1[2]);
	float p1Top2 [3] = {edge_p2[0] - edge_p1[0],edge_p2[1] - edge_p1[1], edge_p2[2] - edge_p1[2]};
	float r1_2D = plane_normal[0]*p1Top2[0] + plane_normal[1]*p1Top2[1] + plane_normal[2]*p1Top2[2];
	
	float r1_2 = r1_2U/r1_2D;
		
	float p_inters[3]  = {edge_p1[0] + r1_2*p1Top2[0], edge_p1[1] + r1_2*p1Top2[1], edge_p1[2] + r1_2*p1Top2[2]};
	result = vector<float> (p_inters,p_inters+3);
	return result;
	
}

vector<float> plane_edge_intersection(vector<float> plane_normal, vector<float> a_point_on_plane, Eigen::Vector3f edge_p1, Eigen::Vector3f edge_p2)
{
	vector<float> result;
	float sign_point1 = (edge_p1[0] - a_point_on_plane[0])*plane_normal[0] + (edge_p1[1] - a_point_on_plane[1])*plane_normal[1] + (edge_p1[2] - a_point_on_plane[2])*plane_normal[2];
	
	float sign_point2 = (edge_p2[0] - a_point_on_plane[0])*plane_normal[0] + (edge_p2[1] - a_point_on_plane[1])*plane_normal[1] + (edge_p2[2] - a_point_on_plane[2])*plane_normal[2];
		
	if(sign_point1*sign_point2 > 0)
	{
		return result;
	}
	
	float r1_2U = plane_normal[0]*(a_point_on_plane[0] - edge_p1[0]) + plane_normal[1]*(a_point_on_plane[1] - edge_p1[1]) + plane_normal[2]*(a_point_on_plane[2] - edge_p1[2]);
	float p1Top2 [3] = {edge_p2[0] - edge_p1[0],edge_p2[1] - edge_p1[1], edge_p2[2] - edge_p1[2]};
	float r1_2D = plane_normal[0]*p1Top2[0] + plane_normal[1]*p1Top2[1] + plane_normal[2]*p1Top2[2];
	
	float r1_2 = r1_2U/r1_2D;

	float p_inters[3]  = {edge_p1[0] + r1_2*p1Top2[0], edge_p1[1] + r1_2*p1Top2[1], edge_p1[2] + r1_2*p1Top2[2]};
    result = vector<float> (p_inters,p_inters+3);
	return result;
}

struct Point_2d {
	float x, y;
    Point_2d(float _x, float _y)
    {
			x = _x;
			y = _y;
	}
	Point_2d()
    {
			x = 0.0;
			y = 0.0;
	}
	bool operator <(const Point_2d &p) const {
		return x < p.x || (x == p.x && y < p.y);
	}
};

// 2D cross product of OA and OB vectors, i.e. z-component of their 3D cross product.
// Returns a positive value, if OAB makes a counter-clockwise turn,
// negative for clockwise turn, and zero if the points are collinear.
float cross(const Point_2d &O, const Point_2d &A, const Point_2d &B)
{
	return (A.x - O.x) * (B.y - O.y) - (A.y - O.y) * (B.x - O.x);
}

// Returns a list of points on the convex hull in counter-clockwise order.
// Note: the last point in the returned list is the same as the first one.
vector<Point_2d> convex_hull(vector<Point_2d> P)
{
	int n = P.size(), k = 0;
	vector<Point_2d> H(2*n);
 
	// Sort points lexicographically
	sort(P.begin(), P.end());
 
	// Build lower hull
	for (int i = 0; i < n; ++i) {
		while (k >= 2 && cross(H[k-2], H[k-1], P[i]) <= 0) k--;
		H[k++] = P[i];
	}
 
	// Build upper hull
	for (int i = n-2, t = k+1; i >= 0; i--) {
		while (k >= t && cross(H[k-2], H[k-1], P[i]) <= 0) k--;
		H[k++] = P[i];
	}
 
	H.resize(k);
	return H;
}

vector<vector<float> > Compute_ConvexHull_3d_to_2D(vector<vector<float> > &points_on_same_plane,vector<float> plane_normal, vector<float> a_point_on_plane)
{
	vector<vector<float> > res;
	float plane_coord_z [3] = {plane_normal[0],plane_normal[1],plane_normal[2]};
	float plane_coord_x [3] = {1.0,0.0,0.0};
	float plane_coord_y [3] = {0.0};
	
	CrossProduct(plane_coord_z,plane_coord_x,plane_coord_y);
	CrossProduct(plane_coord_y,plane_coord_z,plane_coord_x);
	
	Normalize(plane_coord_x,3);
	Normalize(plane_coord_y,3);
	
	float plane_coord_frame_wrt_w[16] = {plane_coord_x[0], plane_coord_x[1], plane_coord_x[2],0.0,
								   plane_coord_y[0], plane_coord_y[1], plane_coord_y[2],0.0,
								   plane_coord_z[0], plane_coord_z[1], plane_coord_z[2],0.0,
								   a_point_on_plane[0], a_point_on_plane[1],a_point_on_plane[2],1.0};
	
	
	float* inv_plane_coord_frame_wrt_w  = InverMatrix(plane_coord_frame_wrt_w);
	
	vector<Point_2d>points_local;
	for(int i = 0; i < points_on_same_plane.size(); ++i)
	{
		float p [4] = {points_on_same_plane[i][0], points_on_same_plane[i][1], points_on_same_plane[i][2], 1.0};
		float* p_local_f = MatrixMulti(inv_plane_coord_frame_wrt_w,p,4,4,4,1);
		Point_2d p_local(p_local_f[0],p_local_f[1]);
		points_local.push_back(p_local);
		delete [] p_local_f;
	}
	vector<Point_2d> convex_hull_points = convex_hull(points_local);
	for(int i = 0; i < convex_hull_points.size(); ++i)
	{
		float point_local_f [4] = {convex_hull_points[i].x,convex_hull_points[i].y,0.0,1.0};
		float* point_world_f = MatrixMulti(plane_coord_frame_wrt_w,point_local_f,4,4,4,1);
		vector<float> point_world (point_world_f,point_world_f+3);
		res.push_back(point_world);
		delete [] point_world_f;
	}
	
	delete [] inv_plane_coord_frame_wrt_w;

	return res;
}

bool compute_circle_given_twopoints_one_tangentVec(float p1[3], float p2[3], float tangentV_on_p1 [3], float cir_plane_normal[3], float cir_cen [3], float& cir_rad)
{
	    float vec_p1_to_p2[3] = {p2[0] - p1[0], p2[1] - p1[1], p2[2] - p1[2]};
		float mid_p1_p2_seg [3] = {(p1[0] + p2[0])/2, (p1[1] + p2[1])/2, (p1[2] + p2[2])/2};
		Normalize(vec_p1_to_p2,3);
		if(norm_vec_3(cir_plane_normal)<=0.00001)
		{
			CrossProduct(tangentV_on_p1,vec_p1_to_p2,cir_plane_normal);
			Normalize(cir_plane_normal,3);
	    }
		float vec_bisector[3] = {0};
		float plane_normal [3] = {cir_plane_normal[0],cir_plane_normal[1],cir_plane_normal[2]};
		Normalize(plane_normal,3);
		CrossProduct(plane_normal,vec_p1_to_p2,vec_bisector);
		
		float tanV_on_p1 [3] = {tangentV_on_p1[0],tangentV_on_p1[1],tangentV_on_p1[2]};
		Normalize(tanV_on_p1,3);
		float vec_passing_center_on_p1 [3] = {0.0};
		
		CrossProduct(plane_normal,tanV_on_p1,vec_passing_center_on_p1);
		
		float p1_f [3] = {p1[0],p1[1],p1[2]};
		// Compute the section center and then the radius of the section circle
		float* circenter = TwolineIntersection(p1_f, mid_p1_p2_seg,vec_passing_center_on_p1, vec_bisector); // ALMEM
		float radius = 0;
		if(circenter != NULL)// if is a circle not a straght line
		{
			 radius = two_pts_dist(circenter,p1_f,3);
		}
		else
		{
			return false;
		}
		
		cir_rad = radius;
		cir_cen[0] = circenter[0];
		cir_cen[1] = circenter[1];
		cir_cen[2] = circenter[2];
		
	delete [] circenter;
	return true;
}


bool compute_circle_given_twopoints_arc2cordDis(float p1[3], float p2[3], float arc2cordDis, float cir_plane_normal[3], float cir_cen [3], float& cir_rad)
{
	float Disp1_p2 = two_pts_dist(p1,p2,3);
	cir_rad = (4.0*arc2cordDis*arc2cordDis + Disp1_p2*Disp1_p2)/(8.0*arc2cordDis);
	
	float vec_p1_to_p2[3] = {p2[0] - p1[0], p2[1] - p1[1], p2[2] - p1[2]};
	Normalize(vec_p1_to_p2,3);
	
	float vec_bisector[3] = {0};
	float mid_p1_p2_seg [3] = {(p1[0] + p2[0])/2.0, (p1[1] + p2[1])/2.0, (p1[2] + p2[2])/2.0};
	float plane_normal [3] = {cir_plane_normal[0],cir_plane_normal[1],cir_plane_normal[2]};
	Normalize(plane_normal,3);
	CrossProduct(plane_normal,vec_p1_to_p2,vec_bisector);
	Normalize(vec_bisector,3);
	
	for(int i = 0; i < 3; ++i)
	{
		cir_cen[i] = mid_p1_p2_seg[i] + vec_bisector[i]*(cir_rad-arc2cordDis);
	}
	
	return true;
	
}

bool compute_circle_given_twopoints_arc2cordDis2ndSol(float p1[3], float p2[3], float arc2cordDis, float cir_plane_normal[3], float cir_cen [3], float& cir_rad)
{
	float Disp1_p2 = two_pts_dist(p1,p2,3);
	cir_rad = (4*arc2cordDis*arc2cordDis + Disp1_p2*Disp1_p2)/(8*arc2cordDis);
	
	float vec_p1_to_p2[3] = {p2[0] - p1[0], p2[1] - p1[1], p2[2] - p1[2]};
	Normalize(vec_p1_to_p2,3);
	
	float vec_bisector[3] = {0};
	float mid_p1_p2_seg [3] = {(p1[0] + p2[0])/2, (p1[1] + p2[1])/2, (p1[2] + p2[2])/2};
	float plane_normal [3] = {cir_plane_normal[0],cir_plane_normal[1],cir_plane_normal[2]};
	Normalize(plane_normal,3);
	CrossProduct(plane_normal,vec_p1_to_p2,vec_bisector);
	Normalize(vec_bisector,3);
	
	for(int i = 0; i < 3; ++i)
	{
		cir_cen[i] = mid_p1_p2_seg[i] - vec_bisector[i]*(cir_rad-arc2cordDis);
	}
	
	return true;
	
}

void RotAVec(float unit_axis [3], float vector_tobe_rot [3], float theta_rt, float rotated_vec[3])
{
	float rot_matrix [9] = {
		cosf(theta_rt) + unit_axis[0]*unit_axis[0]*(1-cosf(theta_rt)), unit_axis[0]*unit_axis[1]*(1-cosf(theta_rt))+unit_axis[2]*sinf(theta_rt), unit_axis[2]*unit_axis[0]*(1-cosf(theta_rt))-unit_axis[1]*sinf(theta_rt), 
		
		unit_axis[0]*unit_axis[1]*(1-cosf(theta_rt))-unit_axis[2]*sinf(theta_rt), cosf(theta_rt) + unit_axis[1]*unit_axis[1]*(1-cosf(theta_rt)), 
		unit_axis[2]*unit_axis[1]*(1-cosf(theta_rt))+unit_axis[0]*sinf(theta_rt), 
		
		unit_axis[0]*unit_axis[2]*(1-cosf(theta_rt))+unit_axis[1]*sinf(theta_rt), unit_axis[2]*unit_axis[1]*(1-cosf(theta_rt))-unit_axis[0]*sinf(theta_rt), cosf(theta_rt) + unit_axis[2]*unit_axis[2]*(1-cosf(theta_rt))
		};
		
	float* res = MatrixMulti(rot_matrix,vector_tobe_rot,3,3,3,1);
	
	for(int i =0; i < 3; ++i)
	{
		rotated_vec[i] = res[i];
	}
	delete [] res;
}

void RotA3by3Frame(float unit_axis [3], float frame_tobe_rot [9], float theta_rt, float rotated_frame[9])
{
	float rot_matrix [9] = {
		cosf(theta_rt) + unit_axis[0]*unit_axis[0]*(1-cosf(theta_rt)), unit_axis[0]*unit_axis[1]*(1-cosf(theta_rt))+unit_axis[2]*sinf(theta_rt), unit_axis[2]*unit_axis[0]*(1-cosf(theta_rt))-unit_axis[1]*sinf(theta_rt), 
		
		unit_axis[0]*unit_axis[1]*(1-cosf(theta_rt))-unit_axis[2]*sinf(theta_rt), cosf(theta_rt) + unit_axis[1]*unit_axis[1]*(1-cosf(theta_rt)), 
		unit_axis[2]*unit_axis[1]*(1-cosf(theta_rt))+unit_axis[0]*sinf(theta_rt), 
		
		unit_axis[0]*unit_axis[2]*(1-cosf(theta_rt))+unit_axis[1]*sinf(theta_rt), unit_axis[2]*unit_axis[1]*(1-cosf(theta_rt))-unit_axis[0]*sinf(theta_rt), cosf(theta_rt) + unit_axis[2]*unit_axis[2]*(1-cosf(theta_rt))
		};
		
	float* res = MatrixMulti(rot_matrix,frame_tobe_rot,3,3,3,3);
	
	for(int i =0; i < 9; ++i)
	{
		rotated_frame[i] = res[i];
	}
	delete [] res;
}

Eigen::VectorXd regular_eigen_tf_matrix_to_vector (Eigen::Matrix4f given_tf_matrix)
{
	float alpha, beta, gama; 
	beta = atan2(-given_tf_matrix(2,0), sqrt(given_tf_matrix(0,0)*given_tf_matrix(0,0)+given_tf_matrix(1,0)*given_tf_matrix(1,0)));
	alpha = atan2(given_tf_matrix(1,0)/cos(beta),given_tf_matrix(0,0)/cos(beta));    
	gama = atan2(given_tf_matrix(2,1)/cos(beta),given_tf_matrix(2,2)/cos(beta));
	//~ beta = atan2(-given_tf_matrix(2,0), sqrt(given_tf_matrix(2,1)*given_tf_matrix(2,1)+given_tf_matrix(2,2)*given_tf_matrix(2,2)));
	//~ alpha = atan2(given_tf_matrix(1,0),given_tf_matrix(0,0));    
	//~ gama = atan2(given_tf_matrix(2,1),given_tf_matrix(2,2));
	Eigen::VectorXd pose_vector(6);
	pose_vector <<  given_tf_matrix(0,3),
					given_tf_matrix(1,3),
					given_tf_matrix(2,3),
					gama,  //roll
					beta,  //pitch
					alpha; //yaw
	return pose_vector;
}

bool isRotCW (float vec1[3], float vec2[3])
{
	float cross_product_vec[3];
	CrossProduct(vec1, vec2, cross_product_vec);
	Normalize(cross_product_vec, 3);
	//~ printf("cp result is (%f,%f,%f)\n", cross_product_vec[0],  cross_product_vec[1], cross_product_vec[2]);
	float global_z_axis [3] = {0.0, 0.0, 1.0};
	float angle = Angle_bewteen_two_vecs(cross_product_vec, global_z_axis);
	//~ printf("angle is %f\n", angle);
	if (angle > M_PI/2.0)
		return true;
	else
		return false;
}

CVector::CVector(float *v,int nLen)
{
	mVect = v;
	length =nLen;
}
CVector::CVector()
{
	mVect = 0;
	length =0;
}

void CVector::setv(float *v,int nLen)
{
    mVect = v;
    length =nLen;
}


//Method to compute the Dot Product of two vectors, the Parameters are two Vectors a and b,returns the result in float format
float gVectDP(CVector *a,CVector *b)
{
	if(a->length != b->length)
	{
		printf("error! two vectors are not the same length!\n");
		return 0;
	}
	else
	{
		int nLength = a->length;
		float fResult = 0.0;
		for(int i = 0; i < nLength; ++i)
		{
			fResult = fResult + (a->mVect[i])*(b->mVect[i]);
		}
		return fResult;
	}
}

//Method to compute the cross Product of two vectors, the Parameters are two Vectors a and b, returns the result in float * format
float *gVectCP(CVector *a,CVector *b)
{
	if(a->length != b->length|| (a->length != 2 && a->length!= 3))
	{
		printf( "error! two vectors are not the same length!\n");
		return 0;
	}
	else
	{
		float* a1 = new float[3];
		float* b1 = new float[3];
		float* fResult = new float[3];
		if(a->length == 2)
		{
			for(int i = 0; i<2; i++)
			{
				a1[i] = a->mVect[i];
				b1[i] = b->mVect[i];
			}
			a1[2] = 0;
			b1[2] = 0;
			fResult[0] = a1[1]*b1[2] - a1[2]*b1[1];
			fResult[1] = -(a1[0]*b1[2] - a1[2]*b1[0]);
			fResult[2] = a1[0]*b1[1] - a1[1]*b1[0];
		}
		else
		{
			fResult[0] = a->mVect[1]*b->mVect[2] - a->mVect[2]*b->mVect[1];
			fResult[1] = -(a->mVect[0]*b->mVect[2] - a->mVect[2]*b->mVect[0]);
			fResult[2] = a->mVect[0]*b->mVect[1] - a->mVect[1]*b->mVect[0];

		}

		delete [] a1;
		delete [] b1;
		return fResult;
	}
}

bool TwolineIntersect(float P1[3], float P2[3], float Q1[3], float Q2[3],float *InterP)
{
	float fa[3] = {P2[0] - P1[0], P2[1] - P1[1], P2[2] - P1[2]};
	float fb[3] = {Q2[0] - Q1[0], Q2[1] - Q1[1], Q2[2] - Q1[2]};
	float fc[3] = {Q1[0] - P1[0], Q1[1] - P1[1], Q1[2] - P1[2]};

    CVector* a = new CVector(fa,3);
	CVector* b = new CVector(fb,3);
	CVector* c = new CVector(fc,3);

	float* fcb = gVectCP(c,b);
	float* fab = gVectCP(a,b);

	
	CVector* cb = new  CVector(fcb,3);
	CVector* ab = new  CVector(fab,3);

	float Dotab = gVectDP(cb,ab);
	float Detab = fab[0]*fab[0] + fab[1]*fab[1] + fab[2]*fab[2];

	if(abs(Detab) <= 0.0001)// two lines parallel
	{
		return false;
	}

	float t_P = Dotab/Detab;
	float inter[3] = {P1[0]+t_P*fa[0], P1[1]+t_P*fa[1], P1[2] + t_P*fa[2]};
	float t_Q = 0;
	for(int i = 0; i < 3; ++i)
	{
		if(abs(fb[i]) > 0.0001)
		{
			t_Q= (inter[i] - Q1[i])/fb[i];
		}
	}

	delete [] fcb;
	delete [] fab;

	if(t_Q < 0 || t_Q>1||t_P < 0)
	{
		return false;
	}
    InterP[0] = inter[0];
	InterP[1] = inter[1];
	InterP[2] = inter[2];
	InterP[3] = t_P;
	InterP[4] = t_Q;
	
	return true;
}
