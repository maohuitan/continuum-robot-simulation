/*
 * meshobject.cpp
 * 
 * Copyright 2016 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include "meshobject.h"

Edge::Edge(Eigen::Vector3f p1, Eigen::Vector3f p2)
{
	pnt1 = p1;
	pnt2 = p2;
}
Edge::Edge()
{
	Eigen::Vector3f p_tmp(0.0, 0.0, 0.0);
	pnt1 = p_tmp;
	pnt2 = p_tmp;
}

Face::Face(std::vector<Eigen::Vector3f> vertex_list, Eigen::Vector3f normal)
{
	for(int i=0; i<vertex_list.size(); i=i+2)
	{
		Edge tmp_edge(vertex_list.at(i), vertex_list.at(i+1));
		face_edges.push_back(tmp_edge);
	}
	face_normal = normal;
	face_vertices = vertex_list;
}

void Face::setFace(std::vector<Eigen::Vector3f> vertex_list, Eigen::Vector3f normal)
{
	face_edges.clear();
	face_vertices.clear();
	for(int i=0; i<vertex_list.size(); i=i+2)
	{
		Edge tmp_edge(vertex_list.at(i), vertex_list.at(i+1));
		face_edges.push_back(tmp_edge);
	}
	face_normal = normal;
	face_vertices = vertex_list;
}

BoundingCircle::BoundingCircle(Eigen::Vector3f cir_center,Eigen::Vector3f cir_normal, float rad)
{
	center = cir_center;
	normal = cir_normal;
	radius = rad;
}

ContactPatch::ContactPatch(Eigen::Vector3f contact_tangent, Eigen::Vector3f contact_normal, Edge contact_edge)
:edge(contact_edge.pnt1, contact_edge.pnt2)
{
	tangent = contact_tangent;
	normal = contact_normal;
	center = (edge.pnt1 + edge.pnt2)*0.5;
	isEmpty = false;
}

ContactPatch::ContactPatch(bool isNull)
: edge()
{
	isEmpty = isNull;
}

meshObject::meshObject(std::string tri_mesh_path, Eigen::Matrix4f mesh_tf)
{
	 tri_mesh.request_vertex_normals();  // Add vertex normals as default property 
	 tri_mesh.request_face_normals();    // Add face normals as default property
	 
	 OpenMesh::IO::Options opt;
     if(!OpenMesh::IO::read_mesh(tri_mesh, tri_mesh_path)) 
		std::cerr << "Error: Cannot read mesh from " << tri_mesh_path << std::endl;
     
     mesh_transform = mesh_tf;
     		 
	 for (triMesh::VertexIter v_it = tri_mesh.vertices_begin(); v_it != tri_mesh.vertices_end(); ++v_it)
	 {
		triMesh::Point vert = tri_mesh.point(*v_it);
		Eigen::Vector4f vert_before_tf (vert[0],vert[1],vert[2],1.0);
		Eigen::Vector4f vert_after_tf = mesh_transform * vert_before_tf;
		
		triMesh::Point point_new = triMesh::Point(vert_after_tf[0], vert_after_tf[1],  vert_after_tf[2]);
		tri_mesh.set_point(*v_it, point_new);
	 }
	 
	 // Recompute the normals 
	 tri_mesh.update_normals();  
}

void meshObject::drawOrgTriMesh()
{
	triMesh::FaceIter          f_it, f_end(tri_mesh.faces_end());
	triMesh::FaceVertexIter    fv_it;
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
    
    // draw wireframe
    for(f_it=tri_mesh.faces_begin(); f_it!=f_end; ++f_it)
    {
	  glBegin(GL_LINE_LOOP);
	  triMesh::Normal norm = tri_mesh.normal(*f_it);
	  glNormal3f(norm[0],norm[1],norm[2]);
      for (fv_it=tri_mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it)
      {
			triMesh::Point vert = tri_mesh.point(*fv_it);
		    glColor3f(0.0, 0.0, 0.6);
		    glVertex3f(vert[0], vert[1], vert[2]);
      }
      glEnd();
    }
    
    // draw triangles
    for(f_it=tri_mesh.faces_begin(); f_it!=f_end; ++f_it)
    {
	  glBegin(GL_TRIANGLES); 
	  triMesh::Normal norm = tri_mesh.normal(*f_it);
	  glNormal3f(norm[0],norm[1],norm[2]);
      for (fv_it=tri_mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it)
      {	
			glColor3f(0.7, 0.7, 0.7);	
			triMesh::Point vert = tri_mesh.point(*fv_it);   
		    glVertex3f(vert[0], vert[1], vert[2]);
      }
     glEnd();
    } 
    
    glPopMatrix();
}

void meshObject::drawOrgTriMeshShaded()
{
	triMesh::FaceIter          f_it, f_end(tri_mesh.faces_end());
	triMesh::FaceVertexIter    fv_it;
	
    glBegin(GL_TRIANGLES);
    for(f_it=tri_mesh.faces_begin(); f_it!=f_end; ++f_it)
    {
	  triMesh::Normal norm = tri_mesh.normal(*f_it);
	  glNormal3f(norm[0],norm[1],norm[2]);
      for (fv_it=tri_mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it)
      {
			triMesh::Point vert = tri_mesh.point(*fv_it);
		    glColor3f(1.0, 0.0, 0.0);
		    glVertex3f(vert[0], vert[1], vert[2]);
      }
    }
    glEnd();
}

void meshObject::drawTransformedTriMesh(float translation[3], float scale[3], float orientation[9])
{
	triMesh::FaceIter          f_it, f_end(tri_mesh.faces_end());
	triMesh::FaceVertexIter    fv_it;
	
	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();			
	float orientationM [16] = {orientation[0],orientation[1],orientation[2],0.0,
							   orientation[3],orientation[4],orientation[5],0.0,
							   orientation[6],orientation[7],orientation[8],0.0,
							   translation[0],translation[1],translation[2],1.0};
	glMultMatrixf(orientationM);	
	glScalef(scale[0],scale[1],scale[2]);
    triMesh::Point vert_previous;
    
    //draw mesh grids		
    for(f_it=tri_mesh.faces_begin(); f_it!=f_end; ++f_it)
    {
	
	  glBegin(GL_LINE_LOOP);
	  triMesh::Normal norm = tri_mesh.normal(*f_it);
	  glNormal3f(norm[0],norm[1],norm[2]);
      for (fv_it=tri_mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it)
      {
		  	glColor3f(0.0, 0.0, 0.6);		
			triMesh::Point vert = tri_mesh.point(*fv_it);   
		    glVertex3f(vert[0], vert[1], vert[2]);
      }
       glEnd();
     
    } 
    //draw mesh triangles  
    for(f_it=tri_mesh.faces_begin(); f_it!=f_end; ++f_it)
    {
	  glBegin(GL_TRIANGLES); 
	  triMesh::Normal norm = tri_mesh.normal(*f_it);
	  glNormal3f(norm[0],norm[1],norm[2]);
      for (fv_it=tri_mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it)
      {	
			glColor3f(0.7, 0.7, 0.7);	
			triMesh::Point vert = tri_mesh.point(*fv_it);   
		    glVertex3f(vert[0], vert[1], vert[2]);
      }
     glEnd();
    } 
    glPopMatrix();
}

Face meshObject::planeIntersectTriMesh(const std::vector<float> &plane_normal,
                                       const std::vector<float> &pnt_on_plane)
{
	triMesh::FaceIter          f_it, f_end(tri_mesh.faces_end());
	triMesh::FaceVertexIter    fv_it;
	
	std::vector<Eigen::Vector3f> intersection_pnts;
	
	for(f_it=tri_mesh.faces_begin(); f_it!=f_end; ++f_it)
    {
	  std::vector<Eigen::Vector3f> this_tri_pnts; // pnts on this triangle
	  this_tri_pnts.resize(3);
	  int index = 0;
	  for (fv_it=tri_mesh.fv_iter(*f_it); fv_it.is_valid(); ++fv_it)
      {	
			triMesh::Point vert = tri_mesh.point(*fv_it);   
		    Eigen::Vector3f this_pnt(vert[0], vert[1], vert[2]);
		    this_tri_pnts[index] = this_pnt;
		    ++index;
      }
      
      #ifdef DEBUG
      std::cout << "this triangle has pnts " << this_tri_pnts[0] << std::endl;
      std::cout << "this triangle has pnts " << this_tri_pnts[1] << std::endl;
      std::cout << "this triangle has pnts " << this_tri_pnts[2] << std::endl;
      std::cout << std::endl;
      #endif
      
      // make three edges of the triangle
      Edge edge1(this_tri_pnts[0], this_tri_pnts[1]);
      Edge edge2(this_tri_pnts[0], this_tri_pnts[2]);
      Edge edge3(this_tri_pnts[1], this_tri_pnts[2]);
      
      // plane and edge1 intersection check
      std::vector<float> inter_pnt_edge1 = plane_edge_intersection(plane_normal, pnt_on_plane,
                                                                   edge1.pnt1, edge1.pnt2);
	  if (inter_pnt_edge1.size() !=0)
	  {
		  Eigen::Vector3f int_pnt_edge1(inter_pnt_edge1.data());
		  intersection_pnts.push_back(int_pnt_edge1);
	  }
      // plane and edge2 intersection check
      std::vector<float> inter_pnt_edge2 = plane_edge_intersection(plane_normal, pnt_on_plane,
                                                                   edge2.pnt1, edge2.pnt2);
	  if (inter_pnt_edge2.size() !=0)
	  {
		  Eigen::Vector3f int_pnt_edge2(inter_pnt_edge2.data());
		  intersection_pnts.push_back(int_pnt_edge2);
	  }
      
      // plane and edge3 intersection check
      std::vector<float> inter_pnt_edge3 = plane_edge_intersection(plane_normal, pnt_on_plane,
                                                                   edge3.pnt1, edge3.pnt2);
	  if (inter_pnt_edge3.size() !=0)
	  {
		  Eigen::Vector3f int_pnt_edge3(inter_pnt_edge3.data());
		  intersection_pnts.push_back(int_pnt_edge3);
	  }  
    } 
    //~ std::cout << "plane intersecting object with points " << intersection_pnts.size() << std::endl;
    
    //visualize cross-section
    #ifdef VISUALIZECROSSSECTION
    glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glTranslatef(0.0,0.0,0.0);
    glBegin(GL_LINES);
    for(int i=0; i<intersection_pnts.size(); ++i)
    {
		glColor3f(0.0,0.0,0.0);
		glVertex3f(intersection_pnts[i][0], intersection_pnts[i][1], intersection_pnts[i][2]);
	}
	glEnd();
	glPopMatrix();
	#endif
	
	// save cross-section as a Face object
	Eigen::Vector3f cross_sec_normal(plane_normal.data());
	Face cross_sec(intersection_pnts, cross_sec_normal);
	return cross_sec;
}

BoundingCircle meshObject::computeBoundingCicle(const Face &polygon_face)
{
	// find center of face vertices
	float x_cum = 0.0;
	float y_cum = 0.0;
	float z_cum = 0.0;
	int num_face_vertices = polygon_face.face_vertices.size();
	for(int i=0; i<num_face_vertices; ++i)
	{
		x_cum += polygon_face.face_vertices[i][0];
		y_cum += polygon_face.face_vertices[i][1];
		z_cum += polygon_face.face_vertices[i][2];
	}
	Eigen::Vector3f face_center(x_cum/num_face_vertices,
	                            y_cum/num_face_vertices,
	                            z_cum/num_face_vertices);
	// find bounding circle radius
	float max_dist = 0.0;
	for(int i=0; i<num_face_vertices; ++i)
	{
		Eigen::Vector3f tmp_diff = face_center - polygon_face.face_vertices[i];
		float temp_dist = sqrt(pow(tmp_diff[0],2)+pow(tmp_diff[1],2)+pow(tmp_diff[2],2));
		if (temp_dist>max_dist)
			max_dist = temp_dist;
	}
	//~ std::cout << "bounding circle radius is " << max_dist << std::endl;
	BoundingCircle bc(face_center, polygon_face.face_normal, max_dist);
	return bc;
}


