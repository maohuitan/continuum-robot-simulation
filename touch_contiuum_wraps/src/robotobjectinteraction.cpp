/*
 * robotobjectinteraction.cpp
 * 
 * Copyright 2016 huitan <huitan@huitan-OptiPlex-990>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


#include "robotobjectinteraction.h"

robotObjectInteraction::robotObjectInteraction()
{
    /**********************************
     ****** 0--Set program mode *******
     **********************************/
    program_mode = 0;
    std::cout << "Set program mode:" << std::endl;
    std::cout << "Type 1 to enter training mode" << std::endl;
    std::cout << "Type 2 to enter testing mode" << std::endl;
    std::cout << "Type 3 to enter mode with hardware (test in simulation)" << std::endl;
    std::cout << "Type 4 to enter mode with hardware (test on hardware)" << std::endl;
    std::cout << "Type 5 to generate chords from one cfg" << std::endl;

    // std::cin >> program_mode;
    program_mode = 3;

    /*************************************************
     ****** 1--Initialize OpenGL drawing index *******
     *************************************************/
    drawingIndex = 0;

    /*************************************************
     ************ 2--Initialize robot ****************
     *************************************************/
    int n_secs = 0;
    // std::cin >> n_secs;
    n_secs = 3; //3;
    std::vector<float> init_config(3*n_secs, 0.0);
    for (int i=0; i<init_config.size(); i=i+3)
    {
        init_config[i] = 0.095;
        init_config[i+1] = 1e-1;
        init_config[i+2] = 0.;
    }

    float arm_base_pos [3] = {0.0};
    /* CW wrappings */
    float arm_base_x [3] = {1.0, 0.0, 0.0};
    float arm_base_y [3] = {0.0, 0.0, -1.0};
    float arm_base_z [3] = {0.0, 1.0, 0.0};
    
    /* CCW wrappings */
    // float arm_base_x [3] = {0.0, 1.0, 0.0};
    // float arm_base_y [3] = {0.0, 0.0, 1.0};
    // float arm_base_z [3] = {1.0, 0.0, 0.0};

    Eigen::Matrix4f mani_base_frame;
    mani_base_frame << arm_base_x[0],   arm_base_y[0],   arm_base_z[0],  arm_base_pos[0], 
                       arm_base_x[1],   arm_base_y[1],   arm_base_z[1],  arm_base_pos[1], 
                       arm_base_x[2],   arm_base_y[2],   arm_base_z[2],  arm_base_pos[2], 
                       0.0, 0.0, 0.0, 1.0; 
    
    /*************************************************
     * 3--initialize test case and mesh setting*******
     *************************************************/
    int test_case = 0;
    // printOptions();
    // std::cin >> test_case;
    test_case = 60;
    if (program_mode==4)
        test_case = 600;
    if (program_mode==5)
        test_case = 2018;

    std::string mesh_path, mesh_env_path;
    float mesh_trans[3] = {0.0};
    float scale_factor[3] = {0.0};
    float rpy[3] = {0.0};

    float mesh_env_trans[3] = {0.0};
    float mesh_env_scale_factor[3] = {0.0};
    float mesh_env_rpy[3] = {0.0};
    
    std::string pkg_path = ros::package::getPath("touch_continuum_wraps");

    switch(test_case)
    {
        case 60:
        {
            std::string archive_path = "/models/objects_obj_iros2016/3DNet/cat10/train/obj/";
            mesh_path = pkg_path + archive_path + "bottle/1cf98e5b6fff5471c8724d5673a063a6.obj";
            obj_name = "bottle_1cf98e5b6fff5471c8724d5673a063a6";
            obj_cat = "bottle";
            mesh_trans[0] = 0.07; mesh_trans[1] = 0.1;  mesh_trans[2] = 0.1; 
            scale_factor[0] = 0.3;scale_factor[1] = 0.3; scale_factor[2] = 0.3;
            rpy[0] = 0.0;rpy[1] = 0.0;rpy[2] = 0.0;
            break;
        }
    }
    
    Eigen::Matrix3f mesh_rot;
    mesh_rot = Eigen::AngleAxisf(rpy[0]*M_PI, Eigen::Vector3f::UnitX())
              *Eigen::AngleAxisf(rpy[1]*M_PI, Eigen::Vector3f::UnitY())
              *Eigen::AngleAxisf(rpy[2]*M_PI, Eigen::Vector3f::UnitZ());
    Eigen::Matrix4f mesh_tf;
    mesh_tf << mesh_rot(0,0), mesh_rot(0,1), mesh_rot(0,2), mesh_trans[0],
               mesh_rot(1,0), mesh_rot(1,1), mesh_rot(1,2), mesh_trans[1],
               mesh_rot(2,0), mesh_rot(2,1), mesh_rot(2,2), mesh_trans[2],
               0.0,           0.0,           0.0,           1.0;
    
    Eigen::Matrix4f mesh_scale;
    mesh_scale << scale_factor[0], 0.0            , 0.0,             0.0,
                  0.0,             scale_factor[1], 0.0,             0.0,
                  0.0,             0.0            , scale_factor[2], 0.0,
                  0.0,             0.0            , 0.0,             1.0; 
    mesh_tf = mesh_tf * mesh_scale;

    // surrounding environment
    Eigen::Matrix3f mesh_env_rot;
    mesh_env_rot =   Eigen::AngleAxisf(mesh_env_rpy[0]*M_PI, Eigen::Vector3f::UnitX())
                    *Eigen::AngleAxisf(mesh_env_rpy[1]*M_PI, Eigen::Vector3f::UnitY())
                    *Eigen::AngleAxisf(mesh_env_rpy[2]*M_PI, Eigen::Vector3f::UnitZ());
    Eigen::Matrix4f mesh_env_tf;
    mesh_env_tf <<   mesh_env_rot(0,0), mesh_env_rot(0,1), mesh_env_rot(0,2), mesh_env_trans[0],
                     mesh_env_rot(1,0), mesh_env_rot(1,1), mesh_env_rot(1,2), mesh_env_trans[1],
                     mesh_env_rot(2,0), mesh_env_rot(2,1), mesh_env_rot(2,2), mesh_env_trans[2],
                     0.0,           0.0,           0.0,           1.0;
    
    Eigen::Matrix4f mesh_env_scale;
    mesh_env_scale << mesh_env_scale_factor[0], 0.0            , 0.0,             0.0,
                      0.0,             mesh_env_scale_factor[1], 0.0,             0.0,
                      0.0,             0.0            , mesh_env_scale_factor[2], 0.0,
                      0.0,             0.0            , 0.0,             1.0; 
    mesh_env_tf = mesh_env_tf * mesh_env_scale;
    /*****************************************************
     * 4--initialize environment with tactile sensing ****
     *****************************************************/ 
    std::vector<Eigen::Vector3f> ver_list;
    Eigen::Vector3f cs_normal;
    env_tactile = new environment_with_tactile_sensing(n_secs, init_config, mani_base_frame,
                                                       mesh_path, mesh_env_path, mesh_tf, mesh_env_tf, ver_list, cs_normal); 
    passed_arm_base_frame.resize(16,0.0);
    env_tactile->mani.Getbaseframe(passed_arm_base_frame);
    /*************************************************************
     *** 5--update cross section with a specified cutting plane **
     *************************************************************/
    std::vector<float> plane_normal;
    plane_normal.push_back(0.0);
    plane_normal.push_back(0.0);
    /* CW wrapings */
    plane_normal.push_back(-1.0);
    /* CCW wrappings */
    // plane_normal.push_back(1.0);
    
    std::vector<float> pnt_on_plane(3,0.0);
    env_tactile->cross_sec = env_tactile->mesh.planeIntersectTriMesh(plane_normal, pnt_on_plane);
    env_tactile->env_cross_sec = env_tactile->mesh_env.planeIntersectTriMesh(plane_normal, pnt_on_plane);
    if (env_tactile->cross_sec.face_edges.empty()) 
    {
        env_tactile->empty_cross_sec_flag = true;
        ROS_WARN("Empty cross section flag set");
    }
    /*************************************************************
     ******* 6--initialize object wrapping settings **************
     *************************************************************/
    // ows.push_back(objWrapSettings(1,1,0.05));
    ows.push_back(objWrapSettings(1,1,0.1));
    // ows.push_back(objWrapSettings(1,1,0.15));
    // ows.push_back(objWrapSettings(1,1,0.2));
    // ows.push_back(objWrapSettings(1,1,0.25));
    // ows.push_back(objWrapSettings(1,2,0.00));
    // ows.push_back(objWrapSettings(1,2,0.05));
    // ows.push_back(objWrapSettings(1,2,0.10));
    // ows.push_back(objWrapSettings(1,2,0.15));
    // ows.push_back(objWrapSettings(1,2,0.20));
    // ows.push_back(objWrapSettings(1,2,0.25));
    // ows.push_back(objWrapSettings(2,0,0.0));
    // ows.push_back(objWrapSettings(3,0,0.0));

    /**********************************
     * 7--Run chord_writer.py *********
     **********************************/
    hist_write_cnt = 0;
    // runChordWriterPY(false);
    // if (program_mode==1) 
    // {
    //     #ifdef SAVEPROBS
    //     runProbWriterPY(); // one-time call of prob_writer
    //     usleep(1000000);
    //     #else
    //     runChordWriterPY(false);
    //     usleep(1000000);
    //     #endif
    // }

    /**********************************
     * 8A--Get current time *********
     **********************************/
    time_t currentTime;
    struct tm *localTime;
    time( &currentTime );                   // Get the current time
    localTime = localtime( &currentTime );  // Convert the current time to the local time

    int Day    = localTime->tm_mday;
    int Month  = localTime->tm_mon + 1;
    int Year   = localTime->tm_year + 1900;
    int Hour   = localTime->tm_hour;
    int Min    = localTime->tm_min;
    int Sec    = localTime->tm_sec;
    /**************************************************************
     * 8B--write initilization information to log files ***********
     **************************************************************/
    std::string log_file_path = pkg_path + "/log/log_file.txt";
    log_file.open(log_file_path.c_str(),std::ios_base::app);
    log_file << " " << std::endl;
    log_file << Day << "/" << Month << "/" << Year << " " << Hour << ":" << Min << ":" << Sec << std::endl;
    log_file << obj_name << std::endl;
    log_file << "mesh_trans " << mesh_trans[0] << " " << mesh_trans[1] << " " << mesh_trans[2] << std::endl;
    log_file << "mesh_rpy " << rpy[0] << " " << rpy[1] << " " << rpy[2] << std::endl;
    log_file << "mesh_scale " << scale_factor[0] << " " << scale_factor[1] << " " << scale_factor[2] << std::endl;
    log_file << "wrap setting " << 1 << " " << 1 << " " << 0.0 << std::endl;
    log_file.close();
    /**********************************************************
     * 9--Initially new plane control flag for testing mode ***
     **********************************************************/
    next_plane_avail = false;
    wrap_service_called = false;
    testing_mode_chord_wrt_sgn = false;
    terminated = false;
}

// wrapByPlane service callback function
bool robotObjectInteraction::wrapNextPlane(touch_continuum_wraps::wrapByPlane::Request& req, 
                                           touch_continuum_wraps::wrapByPlane::Response& res)
{
    next_plane_avail = true;
    wrap_service_called = true;
    ROS_INFO("New wrapping plane received!!!");
    env_tactile->resetEnvByPlnNormal(req.plane_normal);

    if(req.finish) {
        ++hist_write_cnt;
        // run chord writer in testing mode
        std::ostringstream hist_write_cnt_str;
        hist_write_cnt_str << hist_write_cnt;

        std::string chord_writer_cmd;
        chord_writer_cmd = "rosrun chord_recognition chord_writer.py --gl --obj_base " 
                       + obj_name + "_" + hist_write_cnt_str.str() + "_testing" + " --obj_cat " 
                       + obj_cat + "/testingMode/" + obj_name + "'";
        
        runChordWriterPYTesting(chord_writer_cmd);
        usleep(2000000);

        testing_mode_chord_wrt_sgn = true;
        res.success = true;
        res.chord_path = "/home/huitan/collaboration/train/chord_sampling/csv_gl_chords/"
                         + obj_cat + "/testingMode/" + obj_name + "/" + 
                         obj_name + "_" + hist_write_cnt_str.str() + "_testing" + ".csv";
    }
    else
    {
        testing_mode_chord_wrt_sgn = false;
        res.success = true;
        res.chord_path = "null";
    }
    return true;
}

void robotObjectInteraction::drawEnvironment()
{
    // std::cout << std::endl;
    // std::cout << "[Frame " << drawingIndex << "]" << std::endl;
    ++drawingIndex;
    env_tactile->mesh.drawOrgTriMesh();
    // env_tactile->mani.draw_sections();
    // env_tactile->reportRobotStatus();
    // env_tactile->visualizeCrossSection();
    // env_tactile->visualizeEnvCrossSection();
    // drawCrossSectionAndSensedContacts();

    // std::vector<float> eps = env_tactile->mani.get_section_end_points_wrt_world();
    // printf("sec1 ep is (%f,%f,%f)\n", eps[3], eps[4], eps[5]);
    // printf("sec2 ep is (%f,%f,%f)\n", eps[6], eps[7], eps[8]);
    // printf("sec3 ep is (%f,%f,%f)\n", eps[9], eps[10], eps[11]);
}

void robotObjectInteraction::drawManipulator()
{
    env_tactile->mani.draw_sections();
}

void robotObjectInteraction::drawCrossSectionAndSensedContacts()
{
    env_tactile->visualizeCollectedContacts();
    // env_tactile->visualizeCrossSection();
    env_tactile->visualizeInflatedCrossSection();
}

void robotObjectInteraction::test()
{
    if (program_mode==3)
    {
        motionGenerationHardware();
    }
    if (program_mode==4)
    {
        motionGenerationHardwareOrigamiRobot();
    }
}

void robotObjectInteraction::motionGenerationHardware()
{
    if (env_tactile->arm_config_collector.size()!=0)
    {
        #ifdef ONLYVISFINALCONFIG
        env_tactile->mani.update_config(env_tactile->arm_config_collector.back(),
                                        env_tactile->arm_base_frame_collector.back());
        
        env_tactile->arm_config_collector.clear();
        env_tactile->arm_base_frame_collector.clear();
        #else
        /* collect stats */
        passed_arm_cfgs.push_back(env_tactile->arm_config_collector.front());

        /* update opengl drawing */
        // env_tactile->mani.update_config(env_tactile->arm_config_collector.front(),
        //                                 env_tactile->arm_base_frame_collector.front());
        env_tactile->mani.update_config(env_tactile->arm_config_collector.front());
        drawManipulator();
        // /* publish motion cmd */
        // std_msgs::Float32MultiArray cmd;
        // cmd.data = env_tactile->arm_config_collector.front();
        // env_tactile->motion_cmd_pub.publish(cmd);

        env_tactile->arm_config_collector.erase(env_tactile->arm_config_collector.begin());
        // env_tactile->arm_base_frame_collector.erase(env_tactile->arm_base_frame_collector.begin());
        #endif
    }
    else
    {
        if (!env_tactile->arm_physical_limit_warning 
            && !env_tactile->arm_ik_not_found_warning
            && !env_tactile->enough_tip_z_axis_rot_flag
            && !env_tactile->empty_cross_sec_flag)
        {
            std::cout << std::endl;
            std::cout << "*****Planning Robot Arm Motion*****" << std::endl;
            env_tactile->touchDrivenGraspHW();    
        }
        else 
        {
            drawManipulator();
        }
    }
}

void robotObjectInteraction::motionGenerationHardwareOrigamiRobot()
{
    if (env_tactile->arm_config_collector.size()!=0)
    {
        #ifdef ONLYVISFINALCONFIG
        env_tactile->mani.update_config(env_tactile->arm_config_collector.back(),
                                        env_tactile->arm_base_frame_collector.back());
        
        env_tactile->arm_config_collector.clear();
        env_tactile->arm_base_frame_collector.clear();
        #else
        /* for stats */
        passed_arm_cfgs.push_back(env_tactile->arm_config_collector.front());

        // env_tactile->mani.update_config(env_tactile->arm_config_collector.front(),
        //                                 env_tactile->arm_base_frame_collector.front());
        env_tactile->mani.update_config(env_tactile->arm_config_collector.front());
        env_tactile->arm_config_collector.erase(env_tactile->arm_config_collector.begin());
        // env_tactile->arm_base_frame_collector.erase(env_tactile->arm_base_frame_collector.begin());
        #endif
    }
    else
    {
        if (!env_tactile->arm_physical_limit_warning 
            && !env_tactile->arm_ik_not_found_warning
            && !env_tactile->enough_tip_z_axis_rot_flag
            && !env_tactile->empty_cross_sec_flag)
        {
            std::cout << std::endl;
            std::cout << "*****Planning Robot Arm Motion*****" << std::endl;
            env_tactile->touchDrivenGraspHWOrigami();    
        }
        else 
        {
            std::cout << "*********Algorithm terminated**********" << std::endl;
            saveCfgsToFile();
            chordsGeneration();
            getchar();
            getchar();
            // env_tactile->resetEnv(2,0.0);
        }
    }
}

void robotObjectInteraction::chordsGeneration()
{
    accum_chords.clear();
    std::vector<float> base_tf = passed_arm_base_frame;
    for (int i=0; i<passed_arm_cfgs.size(); ++i)
    {
        std::vector<float> cfg = passed_arm_cfgs[i];
        env_tactile->generateChords(cfg, base_tf);
        accum_chords.insert(accum_chords.end(), 
                            env_tactile->chords.begin(),
                            env_tactile->chords.end());
        env_tactile->chords.clear();
    }
    // std::vector<float> cfg = passed_arm_cfgs.back();
    runChordWriterPY(false);
    usleep(1000000);
    env_tactile->chords = accum_chords;
    env_tactile->writeChords();
}

void robotObjectInteraction::runChordWriterPY(bool isPerWrap)
{
    std::ostringstream hist_write_cnt_str;
    hist_write_cnt_str << hist_write_cnt;
    ++hist_write_cnt;
    
    std::string new_ter_cmd = "gnome-terminal -x sh -c '";
    std::string chord_writer_cmd;
    
    if (isPerWrap)
    {
        chord_writer_cmd = "rosrun chord_recognition chord_writer.py --gl --obj_base " 
                       + obj_name + "_" + hist_write_cnt_str.str() + " --obj_cat " 
                       + obj_cat + "/" + obj_name + "'";
    }
    else
    {
        chord_writer_cmd = "rosrun chord_recognition chord_writer.py --gl --obj_base " 
                       + obj_name + " --obj_cat " + obj_cat + "'";
    }
        
    chord_writer_cmd = new_ter_cmd + chord_writer_cmd;
    std::cout << "===========Running=============" << std::endl;
    std::cout << chord_writer_cmd << std::endl;
    system(chord_writer_cmd.c_str());
}

void robotObjectInteraction::runChordWriterPYTesting(std::string chord_writer_cmd)
{
    std::string new_ter_cmd = "gnome-terminal -x sh -c '";
    
    chord_writer_cmd = new_ter_cmd + chord_writer_cmd;
    std::cout << "===========Running=============" << std::endl;
    std::cout << chord_writer_cmd << std::endl;
    system(chord_writer_cmd.c_str());
}

void robotObjectInteraction::runProbWriterPY()
{
    // run prob_writer.py
    std::string new_ter_cmd = "gnome-terminal -x sh -c '";
    std::string prob_writer_cmd = "rosrun active_chord prob_writer.py --gl'";
    prob_writer_cmd = new_ter_cmd + prob_writer_cmd;
    std::cout << "===========Running=============" << std::endl;
    std::cout << prob_writer_cmd << std::endl;
    std::cout << std::endl;
    system(prob_writer_cmd.c_str());
}

void robotObjectInteraction::printOptions()
{
    std::cout << "***************************************" << std::endl;
    std::cout << "*************Choose test cases:" << std::endl;
    std::cout << "*************01: Pentagon" << std::endl;
    std::cout << "*************02: Bunny" << std::endl;
    std::cout << "*************03: Teapot" << std::endl;
    std::cout << "*************04: Monkey" << std::endl;
    std::cout << "*************05: Cylinder 2X12cm" << std::endl;
    std::cout << "*************06: Cylinder 5X25cm" << std::endl;
    std::cout << "*************07: Cylinder 12X25cm" << std::endl;
    std::cout << "*************08: Cube 20cm" << std::endl;
    std::cout << "*************09: Cube 8cm" << std::endl;
    std::cout << "*************10: Sphere 20cm" << std::endl;
    std::cout << "*************11: Sphere 8cm" << std::endl;
    std::cout << "*************12: Cup 1" << std::endl;
    std::cout << "*************13: Cup 2" << std::endl;
    std::cout << "*************14: Cup 3" << std::endl;
    std::cout << "*************15: hammer 1" << std::endl;
    std::cout << "*************16: hammer 2" << std::endl;
    std::cout << "*************17: teapot 1" << std::endl;
    std::cout << "*************18: teapot 2" << std::endl;
    std::cout << "*************19: teapot 3" << std::endl;
    std::cout << "*************20: bowl 1" << std::endl;
    std::cout << "*************21: bowl 2" << std::endl;
    std::cout << "*************22: bowl 3" << std::endl;
    std::cout << "*************23: toilet paper 1" << std::endl;
    std::cout << "*************24: toilet paper 2" << std::endl;
    std::cout << "*************25: toilet paper 3" << std::endl;
    std::cout << "*************26: bottle 1" << std::endl;
    std::cout << "*************27: bottle 2" << std::endl;
    std::cout << "*************28: bottle 3" << std::endl;
    std::cout << "*************29: mug 1" << std::endl;
    std::cout << "*************30: mug 2" << std::endl;
    std::cout << "*************31: mug 3" << std::endl;
    std::cout << "*************32: apple 1" << std::endl;
    std::cout << "*************33: apple 2" << std::endl;
    std::cout << "*************34: apple 3" << std::endl;
    std::cout << "*************35: banana 1" << std::endl;
    std::cout << "*************36: banana 2" << std::endl;
    std::cout << "*************37: banana 3" << std::endl;
    std::cout << "*************38: donut 1" << std::endl;
    std::cout << "*************39: donut 2" << std::endl;
    std::cout << "*************40: donut 3" << std::endl;
    std::cout << "Choose >> " << std::endl;
}

void robotObjectInteraction::saveCfgsToFile()
{
    const std::string file_path("/home/huitan/Dropbox/Development/touch-based-continuum-wrap-hw/catkin_ws/src/touch_contiuum_wraps/log/");
	boost::posix_time::ptime posix_time = ros::Time::now().toBoost();
    std::string time_str = boost::posix_time::to_iso_extended_string(posix_time);
    std::string spec_path = file_path + obj_name;
	ROS_INFO("Saving executed path knot cfgs to file %s", spec_path.c_str());
	if (!passed_arm_cfgs.empty())
	{
		int n_dofs = passed_arm_cfgs.front().size();
		std::ofstream save_file;
		save_file.open (spec_path.c_str());
        save_file << "Base frame:\n";
        for(int i=0; i<passed_arm_base_frame.size(); ++i)
		{
            save_file << passed_arm_base_frame.at(i) << " ";
        }
        save_file << "\n";
        save_file << "\n";
        save_file << "Cfgs:\n";
		for(int i=0; i<passed_arm_cfgs.size(); ++i)
		{
			/* every n_dofs is one knot cfg */
			for (int j=0; j<n_dofs; ++j)
				save_file << passed_arm_cfgs.at(i).at(j) << " ";
            save_file << "\n";
		}

        /* save contact history from hardware sensor */
        save_file << "\n";
        save_file << "\n";
        save_file << "Contact sensor readings:\n";
        for(int i=0; i<env_tactile->cnt_history.size(); ++i)
		{
            int num_of_sensors = 3;
            for (int j=0; j<num_of_sensors; ++j)
                save_file << env_tactile->cnt_history.at(i).at(j) << " ";
            save_file << "\n";
        }

		save_file.close();
	}
	else
		ROS_ERROR("Empty cfgs to save!");
}

robotObjectInteraction::~robotObjectInteraction()
{
    delete env_tactile;
}


