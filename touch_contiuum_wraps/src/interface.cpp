#include "interface.h"

// ROS
#include "ros/ros.h"
#include "chord_training_msgs/ChordSrv.h"

/************ROS Global variables********************/
ros::ServiceClient client;
ros::ServiceClient prob_client;

/************** Program global variables ***********/
bool g_decheck = true;
unsigned g_change_para = 0.0;
robotObjectInteraction* roi;

/*************** Graphics global variabls **********/
static int mainWin, secondWin;

void init(int width, int height) 
{
    glutSetWindow(mainWin);

    GLfloat light_ambient[] = { 0.8, 0.8, 0.8, 1.0 };
    GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
    //~ GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat light_position[] = { 0.0, 0.0, 0.0, 1.0};
    
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(perspecive_fovy, ratial, near_plane, far_plane);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    //~ glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    
    gluLookAt(Look_x, Look_y, Look_z, refer_x, refer_y, refer_z, up_x, up_y, up_z);
    glEnable(GL_DEPTH_TEST);
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glColorMaterial ( GL_FRONT_AND_BACK, GL_EMISSION ) ;
    glColorMaterial ( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE ) ;
    glEnable ( GL_COLOR_MATERIAL );
    
    glClearColor (0.0, 0.0, 0.0, 0.0);
    glShadeModel (GL_SMOOTH);
    glEnable (GL_LINE_SMOOTH);

    glEnable(GL_LIGHT0);    
}

void initSecondWindow(int width, int height)
{
    glutSetWindow(secondWin);
    
    GLfloat light_ambient[] = { 0.8, 0.8, 0.8, 1.0 };
    GLfloat light_diffuse[] = { 1.0, 1.0, 1.0, 1.0 };
    //~ GLfloat light_specular[] = { 1.0, 1.0, 1.0, 1.0 };
    GLfloat light_position[] = { 0.0, 0.0, 0.0, 1.0};
    
    glViewport(0, 0, width, height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(perspecive_fovy, ratial, near_plane, far_plane);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    glTranslatef(-1.0, -1.0, -4.0); // reposition camera
    glLightfv(GL_LIGHT0, GL_AMBIENT, light_ambient);
    glLightfv(GL_LIGHT0, GL_DIFFUSE, light_diffuse);
    //~ glLightfv(GL_LIGHT0, GL_SPECULAR, light_specular);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position);
    
    glEnable(GL_DEPTH_TEST);
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    
    glColorMaterial ( GL_FRONT_AND_BACK, GL_EMISSION ) ;
    glColorMaterial ( GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE ) ;
    glEnable ( GL_COLOR_MATERIAL );
    
    glClearColor (0.0, 0.0, 0.0, 0.0);
    glShadeModel (GL_SMOOTH);
    glEnable (GL_LINE_SMOOTH);

    glEnable(GL_LIGHT0);    
}

void draw()
{
    glutSetWindow(mainWin);
    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glClearColor(1.0,1.0,1.0,0.0);
    glLoadIdentity();
    gluLookAt(Look_x, Look_y, Look_z, refer_x, refer_y, refer_z, up_x, up_y, up_z);
    glMultMatrixf(Transform.M);
    
    ros::spinOnce();
    roi->drawEnvironment();
    if(!g_decheck)
        roi->test(); 
    else
        roi->drawManipulator();
    
    // Draw table surface
    glColor3f(0.3,0.3,0.3);
    glBegin(GL_QUADS);
        glNormal3f(0.0,0.0,1.0); //enabling this will cause lighting issue
        glVertex3f(-10, -10, -0.8);
        glVertex3f(-10,  10, -0.8);
        glVertex3f(10,  10, -0.8);
        glVertex3f(10, -10, -0.8);
    glEnd();
    
    glutSwapBuffers();
    glFlush();
    glutPostRedisplay();
}

void drawCroSec()
{
    glutSetWindow(secondWin);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    
    roi->drawCrossSectionAndSensedContacts();
    
    glutSwapBuffers();
    glFlush();
    glutPostRedisplay();
}

void processMouse(int button, int state, int x, int y)
{
    if(state == GLUT_DOWN)
    {
        isClicked = true;
        if(button == GLUT_LEFT_BUTTON)
        {
            MousePt.s.X = (GLfloat)x;
            MousePt.s.Y = (GLfloat)y;
            isDragging = true;                                      // Prepare For Dragging
            LastRot = ThisRot;                                      // Set Last Static Rotation To Last Dynamic One
            ArcBall.click(&MousePt);                                //
        }
        else if(button == GLUT_RIGHT_BUTTON)
        {
            Matrix3fSetIdentity(&LastRot);                              // Reset Rotation
            Matrix3fSetIdentity(&ThisRot);                              // Reset Rotation
            Matrix4fSetRotationFromMatrix3f(&Transform, &ThisRot);      // Reset Rotation
        }
    }
    else if(state = GLUT_UP)
    {
        isClicked = false;
    }

}

void processMouseActiveMotion(int x, int y)
{
    Quat4fT     ThisQuat;
    MousePt.s.X = (GLfloat)x;
    MousePt.s.Y = (GLfloat)y;
    ArcBall.drag(&MousePt, &ThisQuat);                      // Update End Vector And Get Rotation As Quaternion
    Matrix3fSetRotationFromQuat4f(&ThisRot, &ThisQuat);     // Convert Quaternion Into Matrix3fT
    Matrix3fMulMatrix3f(&ThisRot, &LastRot);                // Accumulate Last Rotation Into This One
    Matrix4fSetRotationFromMatrix3f(&Transform, &ThisRot);  // Set Our Final Transform's Rotation From This One
}

void processSpecialKeys(int key, int x, int y)
{
    if(key == 12)
    {
        g_decheck = !g_decheck;
    }
    if(key == 11)
    {
        g_change_para = 0;
    }
    
    if(key >= 1 && key <= 10)
    {
        g_change_para = 0;
        g_change_para = 1 <<(key);
    }
    
    if(key == GLUT_KEY_UP)
    {
        if(g_change_para == 0)
        {
            float t1 = 0.1;
            float look_axis[3]= {refer_x-Look_x,refer_y-Look_y,refer_z-Look_z};
            float up[3] = {up_x,up_y,up_z};
            Normalize(look_axis,3);
            Normalize(up,3);
    
            Look_x = Look_x + t1*(look_axis[0]);
            Look_y = Look_y + t1*(look_axis[1]);
            Look_z = Look_z + t1*(look_axis[2]);
        }
        
        if(g_change_para& 1<< 1)
        {   

        }
        if(g_change_para& 1<< 2)
        {
            
        }

        if(g_change_para& 1<< 3)
        {
            
        }
        if(g_change_para& 1<< 4)
        {
            
        }
        if(g_change_para& 1<< 5)
        {
            
        }

        if(g_change_para& 1<< 6)
        {
            
        }
        if(g_change_para& 1<< 7)
        {
            
        }
        if(g_change_para& 1<< 8)
        {
            
        }
        if(g_change_para& 1<< 9)
        {
            
        }
        if(g_change_para& 1<< 10)
        {
            float t1 = 0.1;
            float look_axis[3]= {refer_x-Look_x,refer_y-Look_y,refer_z-Look_z};
            Normalize(look_axis,3);

            Look_x = Look_x + t1*(look_axis[0]);
            Look_y = Look_y + t1*(look_axis[1]);
            Look_z = Look_z + t1*(look_axis[2]);
        }
        
        
    }
    if(key == GLUT_KEY_DOWN)
    {
        if(g_change_para == 0)
        {
            float t1 = 0.1;
            float look_axis[3]= {refer_x-Look_x,refer_y-Look_y,refer_z-Look_z};
            float up[3] = {up_x,up_y,up_z};
            Normalize(look_axis,3);
            Normalize(up,3);
    
            Look_x = Look_x - t1*(look_axis[0]);
            Look_y = Look_y - t1*(look_axis[1]);
            Look_z = Look_z - t1*(look_axis[2]);
            
        }
        
        if(g_change_para& 1<< 1)
        {   
            
        }
        if(g_change_para& 1<< 2)
        {
            
        }

        if(g_change_para& 1<< 3)
        {
            
        }
        if(g_change_para& 1<< 4)
        {
            
        }
        if(g_change_para& 1<< 5)
        {
            
        }

        if(g_change_para& 1<< 6)
        {
            
        }
        if(g_change_para& 1<< 7)
        {
            
        }
        if(g_change_para& 1<< 8)
        {
            
        }
        if(g_change_para& 1<< 9)
        {
        
        }
        if(g_change_para& 1<< 10)
        {
            float t1 = -0.3;
            float look_axis[3]= {refer_x-Look_x,refer_y-Look_y,refer_z-Look_z};
            Normalize(look_axis,3);

            Look_x = Look_x + t1*(look_axis[0]);
            Look_y = Look_y + t1*(look_axis[1]);
            Look_z = Look_z + t1*(look_axis[2]);
        }
    }
    else if(key == GLUT_KEY_LEFT)
    {
        float t1 = 0.1;
        float look_axis[3]= {refer_x-Look_x,refer_y-Look_y,refer_z-Look_z};
        float up[3] = {up_x,up_y,up_z};
        Normalize(look_axis,3);
        Normalize(up,3);
        float* left = CrossProduct(up,look_axis);

        Look_x = Look_x + t1*(left[0]);
        Look_y = Look_y + t1*(left[1]);
        Look_z = Look_z + t1*(left[2]);

        refer_x = refer_x + t1*(left[0]);
        refer_y = refer_y + t1*(left[1]);
        refer_z = refer_z+ t1*(left[2]);
        delete [] left;
    }
    else if(key == GLUT_KEY_RIGHT)
    {
        float t1 = 0.1;
        float look_axis[3]= {refer_x-Look_x,refer_y-Look_y,refer_z-Look_z};
        float up[3] = {up_x,up_y,up_z};
        Normalize(look_axis,3);
        Normalize(up,3);
        float* right = CrossProduct(look_axis,up);

        Look_x = Look_x + t1*(right[0]);
        Look_y = Look_y + t1*(right[1]);
        Look_z = Look_z + t1*(right[2]);

        refer_x = refer_x + t1*(right[0]);
        refer_y = refer_y + t1*(right[1]);
        refer_z = refer_z+ t1*(right[2]);
        delete [] right;
    }   
}

int main(int argc, char** argv)
{
    // make a ROS node
    ros::init(argc, argv, "touch_continuum_wraps");
    ros::NodeHandle n;
    // 1. chord writing service client
    client = n.serviceClient<chord_training_msgs::ChordSrv>("/chord_training/chord_srv");
    // 2. wrap next plane service server
    ros::ServiceServer wrap_service = n.advertiseService("continuum_grasp/wrap_by_plane", &robotObjectInteraction::wrapNextPlane, roi);
    // 3. probability writer client
    prob_client = n.serviceClient<chord_training_msgs::ProbWriterSrv>("/chord_training/prob_srv");
    
    roi = new robotObjectInteraction();

    ROS_INFO("Node continuum_grasp up and running.");
    ROS_INFO("Ready to provide ros service [wrap next plane] if in testing mode.");
    ROS_INFO("Ready to call ros service [chord writer srv].");
    ROS_INFO("Ready to call ros service [prob writer srv].");
    
    glutInit(&argc,argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH );
    glutInitWindowPosition(600,100);
    glutInitWindowSize(900,700);
    
    /* create window 1 */
    mainWin = glutCreateWindow("Window1");
    glutDisplayFunc(draw);
    
    glutMouseFunc(processMouse);
    glutMotionFunc(processMouseActiveMotion);
    glutSpecialFunc(processSpecialKeys);
    
    init(900,700); // have to init() for both windows
    /* Enter main loop */
    glutMainLoop();   

    delete roi;
    return 0;
}
