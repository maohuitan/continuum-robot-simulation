#include <GL/glut.h>
#include <GL/gl.h>
#include <GL/glu.h>			// Header File For The GLu32 Library
#include "ManipulatorNSections.h"
#include <assert.h>
#include <iostream>
#include <fstream>
using namespace std;

float* ManipulatorNSections:: ComputeTransMatrix(float s,float k, float phi)
{
	if(fabsf(k) < 0.0001||fabsf(k) == 0.0001)
	{
		float _TranSMatrix[16] = {1,0,0,0,
		                          0,1,0,0,
		                          0,0,1,0,
		                          0,0,s,1};

		float* TranSMatrix = new float [16];
		for(int i = 0; i < 16; ++i)
		{
			TranSMatrix[i] = _TranSMatrix[i];
		}
		return TranSMatrix;
	}
	else
	{
		float _TranSMatrix_1[16] = {cos(phi)*cos(phi)*(cos(k*s)-1)+1, sin(phi)*cos(phi)*(cos(k*s)-1), cos(phi)*sin(k*s), 0,
									sin(phi)*cos(phi)*(cos(k*s)-1), cos(phi)*cos(phi)*(1-cos(k*s))+cos(k*s), sin(phi)*sin(k*s),0,
									-cos(phi)*sin(k*s), -sin(phi)*sin(k*s), cos(k*s),0,
									(cos(phi)*(cos(k*s)-1))/k, (sin(phi)*(cos(k*s)-1))/k, sin(k*s)/k,1};

		float _TranSMatrix_2[16] = {cos(phi), sin(phi),0,0,
		                            -sin(phi), cos(phi),0,0,
		                            0, 0, 1, 0,
		                            0, 0, 0, 1};

		float* _TranSMatrix = MatrixMulti(_TranSMatrix_1,_TranSMatrix_2,4,4,4,4); //ALMEM
		float* TranSMatrix = new float [16]; //ALMEM for returning
		for(int i = 0; i < 16; ++i)
		{
			TranSMatrix[i] = _TranSMatrix[i];
		}
		delete [] _TranSMatrix;
		return TranSMatrix;
	}
}

bool ManipulatorNSections::compute_trans_matrix(vector<float>& _trans_matrix)
{
	if(config.size() == 0 || n_sections == 0|| _trans_matrix.size() != 16*n_sections)
	{
		return false;
	}

	vector<float> s(n_sections);
	vector<float> k(n_sections);
	vector<float> phi(n_sections);

	for(int i = 0; i < n_sections; ++i)
	{
		s[i] = config[i*3];
		k[i] = config[i*3+1];
		phi[i] = config[i*3+2];
	}

	vector<vector<float> > TransMatrix(n_sections);

	for(int i = 0; i < n_sections; ++i)
	{
		if(i == 0)
		{
			float* TranSMatrixi_ips1 = ComputeTransMatrix(s[i],k[i],phi[i]);// allocated memory
			TransMatrix[i] = vector<float>(TranSMatrixi_ips1,TranSMatrixi_ips1+16);
			delete [] TranSMatrixi_ips1;
		}
		else
		{
			float* TranSMatrixi_ips1 = ComputeTransMatrix(s[i],k[i],phi[i]);// allocated memory
			float* TranSMatrix0_i = MatrixMulti(TransMatrix[i-1],TranSMatrixi_ips1,4,4,4,4);// ALLM
			TransMatrix[i] = vector<float>(TranSMatrix0_i,TranSMatrix0_i+16);
			delete [] TranSMatrixi_ips1;
			delete [] TranSMatrix0_i;
		}
	}

	for(int j = 0; j < n_sections; ++j)
	{
		for(int i = 0; i < 16;++i)
		{
			_trans_matrix[j*16 + i] = TransMatrix[j][i];
		}
	}
	return true;

}

bool ManipulatorNSections::compute_section_plane_normals_local( vector<float>& _section_plane_normals)
{
	if(trans_matrix.size() == 0 || _section_plane_normals.size() != 3*(n_sections))
	{
		return false;
	}

	for(int i = 0; i < n_sections; ++i)
	{
		float y_axis_f[3] = {trans_matrix[16*i+4],trans_matrix[16*i+5],trans_matrix[16*i+6]};		
		vector<float> y_axis = 	vector<float>(y_axis_f,y_axis_f+3);
		Normalize(y_axis);
		
		_section_plane_normals[3*i] = y_axis[0];
		_section_plane_normals[3*i+1] = y_axis[1];
		_section_plane_normals[3*i+2] = y_axis[2];	
				
		//data member of ManipulatorNSections Class
	//	section_plane_normals_local[3*i] = y_axis[0];
	//	section_plane_normals_local[3*i+1] = y_axis[1];
	//	section_plane_normals_local[3*i+2] = y_axis[2];	
	}
	
	return true;
}


bool ManipulatorNSections:: compute_section_cir_centers_local(vector<float>& _section_cir_centers_nsections_local )
{
	vector<float> s(n_sections);
	vector<float> k(n_sections);
	vector<float> phi(n_sections);
	vector<float> r(n_sections);
	 
	for(int i = 0; i < n_sections; ++i)
	{
		s[i] = config[i*3];	
		k[i] = config[i*3+1];
		r[i] = 1/fabs(k[i]);
		phi[i] = config[i*3+2];
	}
  
	vector<vector<float> >RotaAboutZs(n_sections);
	for(int i = 0; i < n_sections; ++i)
	{
		float RotaAboutZi[16] = {cos(phi[i]),sin(phi[i]),0,0, -sin(phi[i]),cos(phi[i]),0,0, 0,0,1,0, 0,0,0,1};
		vector<float> temp(RotaAboutZi,RotaAboutZi+16);
		RotaAboutZs[i] = temp;
	}
    vector<float>ForwardTransMat(n_sections*16);
	if(trans_matrix.size() == 0)
	{
		compute_trans_matrix(ForwardTransMat);
	}
	else
	{
		ForwardTransMat = trans_matrix;
	}


	vector<vector<float> >endPoints_local((n_sections+1),vector<float>(3,0));
	
	for(int i = 1; i < n_sections+1; ++i)
	{
		float endPoints_local_tmp [3] = {ForwardTransMat[(i-1)*16+12],ForwardTransMat[(i-1)*16+13],ForwardTransMat[(i-1)*16+14]};
		endPoints_local[i] = vector<float>(endPoints_local_tmp, endPoints_local_tmp+3);
	}


	vector<vector<float> >rotateEPs(n_sections);
	for(int i = 1; i < n_sections; ++i)
	{
		float* rotateEPi = MatrixMulti(&ForwardTransMat[(i-1)*16], RotaAboutZs[i],4,4,4,4); //ALMEM
		rotateEPs[i] = vector<float>(rotateEPi,rotateEPi+16);
		delete [] rotateEPi;
	}


	vector<vector<float> >eP_xs(n_sections);
	for(int i = 0; i < n_sections; ++i)
	{
		float eP_xs_tmp [3] = {ForwardTransMat[0+16*i],ForwardTransMat[1+16*i],ForwardTransMat[2+16*i]};
		eP_xs[i] = vector<float>(eP_xs_tmp,eP_xs_tmp+3);
	}

	vector<vector<float> >ReP_xs(n_sections);
	for(int i = 0; i < n_sections; ++i)
	{
		if(i == 0)
		{
			float  ReP_xs_tmp [3] = {cos(phi[0]),sin(phi[0]),0};
			ReP_xs[i] =vector<float>(ReP_xs_tmp,ReP_xs_tmp+3);
		}
		else
		{
			float  ReP_xs_tmp [3] = {rotateEPs[i][0],rotateEPs[i][1],rotateEPs[i][2]}; 
			ReP_xs[i] = vector<float>(ReP_xs_tmp,ReP_xs_tmp+3);
		}
	}

	for(int i = 0; i < n_sections; ++i)
	{
		float endpt1[3]= {endPoints_local[i][0], endPoints_local[i][1],endPoints_local[i][2]};
		float endpt2[3]= {endPoints_local[i+1][0], endPoints_local[i+1][1],endPoints_local[i+1][2]};
		float pt1_vec[3] = {ReP_xs[i][0],ReP_xs[i][1], ReP_xs[i][2]};
		float pt2_vec[3] = {eP_xs[i][0],eP_xs[i][1], eP_xs[i][2]};
		float *Centi = TwolineIntersection(endpt1,endpt2,pt1_vec,pt2_vec);//ALMEM
		if(!Centi)
		{
			float vec_enp1_enpt2[3] = {endpt2[0] - endpt1[0], endpt2[1] - endpt1[1], endpt2[2] - endpt1[2]};
			Normalize(vec_enp1_enpt2,3);
			float cross_product[3] = {0.0};
			CrossProduct(vec_enp1_enpt2,pt1_vec,cross_product);
			float cross_product_len = norm_vec_3(cross_product);
			if(fabs(cross_product_len) <= 0.001)
			{
				 float center_tmp [3] = {(endpt2[0] + endpt1[0])/2, (endpt2[1] + endpt1[1])/2, (endpt2[2] + endpt1[2])/2};
				_section_cir_centers_nsections_local[i*3] = center_tmp[0] ;
				_section_cir_centers_nsections_local[i*3+1] = center_tmp[1] ;
				_section_cir_centers_nsections_local[i*3+2] = center_tmp[2] ;	
			}
			else
			{
				_section_cir_centers_nsections_local[i*3] = FLT_MAX ;
				_section_cir_centers_nsections_local[i*3+1] = FLT_MAX ;
				_section_cir_centers_nsections_local[i*3+2] = FLT_MAX ;
			}
			
		}
		else
		{
			_section_cir_centers_nsections_local[i*3] = Centi[0];
			_section_cir_centers_nsections_local[i*3+1] = Centi[1];
			_section_cir_centers_nsections_local[i*3+2] = Centi[2];
		}
		delete [] Centi;
	}
	
	return true;
}
bool ManipulatorNSections::compute_section_end_points_wrt_world(vector<float>& _section_end_points)
{
	if(trans_matrix.size() == 0 || base_trans_matrix.size() == 0 || _section_end_points.size() != 3*(n_sections+1))
	{
		return false;
	}
	for(int i = 1; i < n_sections+1; ++i)
	{
		float endPoint_wrt_base[] = {trans_matrix[(i-1)*16+12],trans_matrix[(i-1)*16+13],trans_matrix[(i-1)*16+14],1.0};
		float *endPoint_wrt_world = MatrixMulti(base_trans_matrix,endPoint_wrt_base,4,4,4,1);
		_section_end_points[3*i +0] = endPoint_wrt_world[0];
		_section_end_points[3*i +1] = endPoint_wrt_world[1];
		_section_end_points[3*i +2] = endPoint_wrt_world[2];
		delete [] endPoint_wrt_world;
	}

	_section_end_points[0] = base_trans_matrix[12];
	_section_end_points[1] = base_trans_matrix[13];
	_section_end_points[2] = base_trans_matrix[14];

	return true;

}

bool ManipulatorNSections::compute_section_cir_centers_wrt_world(vector<float>& _section_cir_centers_wrt_world)
{
	vector<float> section_cir_centers_local_tmp(3*n_sections);
	if(this->section_cir_centers_local.size() == 0)
	{
		if(!compute_section_cir_centers_local(section_cir_centers_local_tmp))
			return false;
	}
	else
	{
		for(int i = 0 ; i < 3*n_sections; ++i)
		{
			section_cir_centers_local_tmp[i] = this->section_cir_centers_local[i];
		}
	}
	for(int i = 0; i < n_sections; ++i)
	{
		float centers_wrt_base[] = {section_cir_centers_local_tmp[i*3],section_cir_centers_local_tmp[i*3+1],section_cir_centers_local_tmp[i*3+2],1.0};
		float *centers_wrt_world = MatrixMulti(base_trans_matrix,centers_wrt_base,4,4,4,1);
		
//		section_cir_centers_wrt_world[3*i +0] = centers_wrt_world[0];
//		section_cir_centers_wrt_world[3*i +1] = centers_wrt_world[1];
//		section_cir_centers_wrt_world[3*i +2] = centers_wrt_world[2];
		
		_section_cir_centers_wrt_world[3*i +0] = centers_wrt_world[0];
		_section_cir_centers_wrt_world[3*i +1] = centers_wrt_world[1];
		_section_cir_centers_wrt_world[3*i +2] = centers_wrt_world[2];
		delete [] centers_wrt_world;
	}
	return true;
}

bool ManipulatorNSections::compute_section_plane_normals_wrt_world(vector<float>& _section_plane_normals)
{
	vector<float> normal_wrt_base_tmp (3*n_sections);
	this->compute_section_plane_normals_local(normal_wrt_base_tmp);
	for(int i = 0; i < n_sections; ++i)
	{
		
		float normal_wrt_base[4] = {normal_wrt_base_tmp[i*3],normal_wrt_base_tmp[i*3+1],normal_wrt_base_tmp[i*3+2],1.0};
		float *normals_wrt_world = MatrixMulti(base_trans_matrix,normal_wrt_base,4,4,4,1);
		
	//	section_plane_normals_wrt_world[3*i +0] = normals_wrt_world[0];
	//	section_plane_normals_wrt_world[3*i +1] = normals_wrt_world[1];
	//	section_plane_normals_wrt_world[3*i +2] = normals_wrt_world[2];
		
		_section_plane_normals[3*i +0] = normals_wrt_world[0];
		_section_plane_normals[3*i +1] = normals_wrt_world[1];
		_section_plane_normals[3*i +2] = normals_wrt_world[2];
		
		delete [] normals_wrt_world;
	}
	return true;
}

bool ManipulatorNSections::draw_sections_3_sections()
{
	if(config.size() == 0 || section_cir_centers_local.size() == 0 || quad == NULL || trans_matrix.size() == 0)
	{
		return false;
	}
	float s1 = config[0]; float k1 = config[1]; float phi1 = config[2];
	float s2 = config[3]; float k2 = config[4]; float phi2 = config[5];
	float s3 = config[6]; float k3 = config[7]; float phi3 = config[8];
	float r1 = 1/fabsf(k1); float r2 = 1/fabsf(k2); float r3 = 1/fabsf(k3);

//	float RotaAboutZ0[16] = {cos(phi1),sin(phi1),0,0, -sin(phi1),cos(phi1),0,0, 0,0,1,0, 0,0,0,1};
	float RotaAboutZ1[16] = {cos(phi2),sin(phi2),0,0, -sin(phi2),cos(phi2),0,0, 0,0,1,0, 0,0,0,1};
	float RotaAboutZ2[16] = {cos(phi3),sin(phi3),0,0, -sin(phi3),cos(phi3),0,0, 0,0,1,0, 0,0,0,1};

	float deta_s = 0.05;
	float deta_theta1 = deta_s/r1;
	float deta_theta2 = deta_s/r2;
	float deta_theta3 = deta_s/r3;

	float endPoint0[] = {0,0,0};
	float endPoint1[] = {trans_matrix[12],trans_matrix[13],trans_matrix[14]};
	float endPoint2[] = {trans_matrix[12+16],trans_matrix[13+16],trans_matrix[14+16]};
	float endPoint3[] = {trans_matrix[12+32],trans_matrix[13+32],trans_matrix[14+32]};


    float* rotateEP1 = MatrixMulti(&trans_matrix[0],RotaAboutZ1,4,4,4,4); //ALMEM
	float* rotateEP2 = MatrixMulti(&trans_matrix[16],RotaAboutZ2,4,4,4,4); //ALMEM
//	float* rotateEP3 = MatrixMulti(RotaAboutZ2,&ForwardTransMat[32],4,4,4,4); //ALMEM


	float eP1_x[] = {trans_matrix[0],trans_matrix[1],trans_matrix[2]};
	float eP2_x[] = {trans_matrix[0+16],trans_matrix[1+16],trans_matrix[2+16]};
	float eP3_x[] = {trans_matrix[0+32],trans_matrix[1+32],trans_matrix[2+32]};


	float eP1_y[] = {trans_matrix[4],trans_matrix[5],trans_matrix[6]};
	float eP2_y[] = {trans_matrix[4+16],trans_matrix[5+16],trans_matrix[6+16]};
	float eP3_y[] = {trans_matrix[4+32],trans_matrix[5+32],trans_matrix[6+32]};


	float eP1_z[] = {trans_matrix[8],trans_matrix[9],trans_matrix[10]};
	float eP2_z[] = {trans_matrix[8+16],trans_matrix[9+16],trans_matrix[10+16]};
	float eP3_z[] = {trans_matrix[8+32],trans_matrix[9+32],trans_matrix[10+32]};

	float ReP0_x[] = {cos(phi1),sin(phi1),0};
	float ReP0_y[] = {-sin(phi1),cos(phi1),0};
	float ReP0_z[] = {0,0,1};

    float ReP1_x[] = {rotateEP1[0],rotateEP1[1],rotateEP1[2]};
	float ReP2_x[] = {rotateEP2[0],rotateEP2[1],rotateEP2[2]};

	float ReP1_y[] = {rotateEP1[4],rotateEP1[5],rotateEP1[6]};
	float ReP2_y[] = {rotateEP2[4],rotateEP2[5],rotateEP2[6]};

	float ReP1_z[] = {rotateEP1[8],rotateEP1[9],rotateEP1[10]};
	float ReP2_z[] = {rotateEP2[8],rotateEP2[9],rotateEP2[10]};

	float Cent1 [3]=  {section_cir_centers_local[0],section_cir_centers_local[1],section_cir_centers_local[2]};
	float Cent2 [3]=  {section_cir_centers_local[3],section_cir_centers_local[4],section_cir_centers_local[5]};
	float Cent3 [3]=  {section_cir_centers_local[6],section_cir_centers_local[7],section_cir_centers_local[8]};


	float u1len = sqrtf((endPoint0[0]-Cent1[0])*(endPoint0[0]-Cent1[0]) +(endPoint0[1]-Cent1[1])*(endPoint0[1]-Cent1[1]) + (endPoint0[2]-Cent1[2])*(endPoint0[2]-Cent1[2]));
	float u2len = sqrtf((endPoint1[0]-Cent2[0])*(endPoint1[0]-Cent2[0]) +(endPoint1[1]-Cent2[1])*(endPoint1[1]-Cent2[1]) + (endPoint1[2]-Cent2[2])*(endPoint1[2]-Cent2[2]));
	float u3len =sqrtf((endPoint2[0]-Cent3[0])*(endPoint2[0]-Cent3[0]) +(endPoint2[1]-Cent3[1])*(endPoint2[1]-Cent3[1]) + (endPoint2[2]-Cent3[2])*(endPoint2[2]-Cent3[2]));

	float u1[3] = {(endPoint0[0] - Cent1[0])/u1len,(endPoint0[1] - Cent1[1])/u1len, (endPoint0[2] - Cent1[2])/u1len};
	float u2[3] = {(endPoint1[0] - Cent2[0])/u2len,(endPoint1[1] - Cent2[1])/u2len, (endPoint1[2] - Cent2[2])/u2len};
	float u3[3] = {(endPoint2[0] - Cent3[0])/u3len,(endPoint2[1] - Cent3[1])/u3len, (endPoint2[2] - Cent3[2])/u3len};

	// Set the base frame to draw:
	float base_frame_f[16] = {0};
	for(int i =0; i < 16; ++i)
	{
		base_frame_f[i] = this->base_trans_matrix[i];
	}

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix(); // save the model view matrix before setting the base frame
	glMultMatrixf(base_frame_f);
////////////--------------------Begin to draw each section -------//////////////////
	// plot section 1
	glColor3f(0.0,0.0,0.0); //set the color to be black
	if(fabsf(k1)< 0.0001)
	{

		float Sec1z[3] = {endPoint1[0] - endPoint0[0], endPoint1[1] - endPoint0[1], endPoint1[2] - endPoint0[2]};
		float Sec1x[3] = {1.0,0.0,0.0};
		float Sec1y [3] = {0};
		CrossProduct(Sec1z,Sec1x,Sec1y);
		CrossProduct(Sec1y,Sec1z,Sec1x);
		Normalize(Sec1z,3);
		Normalize(Sec1x,3);
		Normalize(Sec1y,3);
		float m[16] ={Sec1x[0], Sec1x[1], Sec1x[2], 0,
		              Sec1y[0], Sec1y[1], Sec1y[2], 0,
					  Sec1z[0], Sec1z[1], Sec1z[2], 0,
		               endPoint0[0],  endPoint0[1], endPoint0[2], 1};
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glMultMatrixf(m);
		gluCylinder(quad,detaR,detaR,s1,30,30);
		glPopMatrix();
	}
	else
	{
		for(float t = 0; t <= (s1/r1); t +=deta_theta1)
		{
			float p[3] = {0};
			for(int i =0; i<3;++i)
			{
				p[i] = Cent1[i]+ r1*cos(t)*u1[i]+r1*sin(t)*(ReP0_z[i]);
			}

			_Point Sec1x(p[0] - Cent1[0], p[1] - Cent1[1], p[2] - Cent1[2]);
			_Point Sec1y(ReP0_y[0],ReP0_y[1],ReP0_y[2]);
			_Point Sec1z(Sec1x.y*Sec1y.z - Sec1x.z*Sec1y.y, Sec1x.z*Sec1y.x - Sec1x.x*Sec1y.z, Sec1x.x*Sec1y.y - Sec1x.y*Sec1y.x);
			Sec1x.Normalize();
			Sec1y.Normalize();
			Sec1z.Normalize();
			glMatrixMode(GL_MODELVIEW);
			float m[16] ={Sec1x.x, Sec1x.y, Sec1x.z, 0,
						  Sec1y.x, Sec1y.y, Sec1y.z, 0,
						  Sec1z.x, Sec1z.y, Sec1z.z, 0,
						   p[0],  p[1],  p[2], 1};

			glPushMatrix();
			glMultMatrixf(m);
			gluCylinder(quad,detaR,detaR,deta_s,30,30);
			glPopMatrix();
		}
	}

	//plot section 2
	glColor3f(1.0,0.0,0.0);
	if(fabsf(k2)< 0.0001)
	{

		float Sec2z[3] = {endPoint2[0] - endPoint1[0], endPoint2[1] - endPoint1[1], endPoint2[2] - endPoint1[2]};
		float Sec2x[3] = {1.0,0.0,0.0};
		float Sec2y[3] = {0};
		CrossProduct(Sec2z,Sec2x,Sec2y);
		CrossProduct(Sec2y,Sec2z,Sec2x);
		Normalize(Sec2z,3);
		Normalize(Sec2x,3);
		Normalize(Sec2y,3);
		float m[16] ={Sec2x[0], Sec2x[1], Sec2x[2], 0,
		              Sec2y[0], Sec2y[1], Sec2y[2], 0,
					  Sec2z[0], Sec2z[1], Sec2z[2], 0,
		               endPoint1[0],  endPoint1[1], endPoint1[2], 1};
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glMultMatrixf(m);
		gluCylinder(quad,detaR,detaR,s2,30,30);
		glPopMatrix();
	}
	else
	{
		for(float t = 0; t <= (s2/r2); t +=deta_theta2)
		{
			float p[3] = {0};
			for(int i =0; i<3;++i)
			{
				p[i] = Cent2[i]+ r2*cos(t)*u2[i]+r2*sin(t)*(ReP1_z[i]);
			}

			_Point Sec2x(p[0] - Cent2[0], p[1] - Cent2[1], p[2] - Cent2[2]);
			_Point Sec2y(ReP1_y[0],ReP1_y[1],ReP1_y[2]);
			_Point Sec2z(Sec2x.y*Sec2y.z - Sec2x.z*Sec2y.y, Sec2x.z*Sec2y.x - Sec2x.x*Sec2y.z, Sec2x.x*Sec2y.y - Sec2x.y*Sec2y.x);
			Sec2x.Normalize();
			Sec2y.Normalize();
			Sec2z.Normalize();

			glMatrixMode(GL_MODELVIEW);
			float m[16] ={Sec2x.x, Sec2x.y, Sec2x.z, 0,
						  Sec2y.x, Sec2y.y, Sec2y.z, 0,
						  Sec2z.x, Sec2z.y, Sec2z.z, 0,
						   p[0],  p[1],  p[2], 1};

			glPushMatrix();
			glMultMatrixf(m);
			gluCylinder(quad,detaR,detaR,deta_s,30,30);
			glPopMatrix();
		}
	}
	// plot section 3
	glColor3f(0.0,1.0,0.0);
	if(fabsf(k3)< 0.0001)
	{

		float Sec3z[3] = {endPoint3[0] - endPoint2[0], endPoint3[1] - endPoint2[1], endPoint3[2] - endPoint2[2]};
		float Sec3x[3] = {1.0,0.0,0.0};
		float Sec3y [3] = {0};
		CrossProduct(Sec3z,Sec3x,Sec3y);
		CrossProduct(Sec3y,Sec3z,Sec3x);
		Normalize(Sec3z,3);
		Normalize(Sec3x,3);
		Normalize(Sec3y,3);
		float m[16] ={Sec3x[0], Sec3x[1], Sec3x[2], 0,
		              Sec3y[0], Sec3y[1], Sec3y[2], 0,
					  Sec3z[0], Sec3z[1], Sec3z[2], 0,
		               endPoint2[0],  endPoint2[1], endPoint2[2], 1};
		glMatrixMode(GL_MODELVIEW);
		glPushMatrix();
		glMultMatrixf(m);
		gluCylinder(quad,detaR,detaR,s3,30,30);
		glPopMatrix();
	}
	else
	{
		for(float t = 0; t <= (s3/r3); t +=deta_theta3)
		{
			float p[3] = {0};
			for(int i =0; i<3;++i)
			{
				p[i] = Cent3[i]+ r3*cos(t)*u3[i]+r3*sin(t)*(ReP2_z[i]);
			}
			_Point Sec3x(p[0] - Cent3[0], p[1] - Cent3[1], p[2] - Cent3[2]);
			_Point Sec3y(ReP2_y[0],ReP2_y[1],ReP2_y[2]);
			_Point Sec3z(Sec3x.y*Sec3y.z - Sec3x.z*Sec3y.y, Sec3x.z*Sec3y.x - Sec3x.x*Sec3y.z, Sec3x.x*Sec3y.y - Sec3x.y*Sec3y.x);
			Sec3x.Normalize();
			Sec3y.Normalize();
			Sec3z.Normalize();

			glMatrixMode(GL_MODELVIEW);
			float m[16] ={Sec3x.x, Sec3x.y, Sec3x.z, 0,
						  Sec3y.x, Sec3y.y, Sec3y.z, 0,
						  Sec3z.x, Sec3z.y, Sec3z.z, 0,
						   p[0],  p[1],  p[2], 1};
			glPushMatrix();
			glMultMatrixf(m);
			gluCylinder(quad,detaR,detaR,deta_s,30,30);
			glPopMatrix();
		}
	}

	glPopMatrix(); // load the model view matrix before setting the base frame
	delete [] rotateEP1;
	delete [] rotateEP2;

	return true;
}


bool ManipulatorNSections::draw_sections()
{
	int nSections = n_sections;
	vector<float> Config = config;
		
	vector<float> s(nSections);
	vector<float> k(nSections);
	vector<float> phi(nSections);
	vector<float> r(nSections);

	for(int i = 0; i < nSections; ++i)
	{
		s[i] = Config[i*3];
		k[i] = Config[i*3+1];
		r[i] = 1/fabs(k[i]);
		phi[i] = Config[i*3+2];
	}

	vector<vector<float> >RotaAboutZs(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		float RotaAboutZi[16] = {cos(phi[i]),sin(phi[i]),0,0, -sin(phi[i]),cos(phi[i]),0,0, 0,0,1,0, 0,0,0,1};
		vector<float> temp(RotaAboutZi,RotaAboutZi+16);
		RotaAboutZs[i] = temp;
	}

	vector<float>ForwardTransMat(nSections*16);
	if(trans_matrix.size() == 0)
	{
		compute_trans_matrix(ForwardTransMat);
	}
	else
	{
		ForwardTransMat = trans_matrix;
	}
	
	vector<vector<float> >endPoints(nSections+1, vector<float>(3,0));
	
	for(int i = 1; i < nSections+1; ++i)
	{	
		float endPoints_tmp [3] = {ForwardTransMat[(i-1)*16+12],ForwardTransMat[(i-1)*16+13],ForwardTransMat[(i-1)*16+14]};
		endPoints[i] = vector<float>(endPoints_tmp,endPoints_tmp+3);
	}


	vector<vector<float> >rotateEPs(nSections);
	for(int i = 1; i < nSections; ++i)
	{
		float* rotateEPi = MatrixMulti(&ForwardTransMat[(i-1)*16], RotaAboutZs[i],4,4,4,4); //ALMEM
		rotateEPs[i] = vector<float>(rotateEPi,rotateEPi+16);
		delete [] rotateEPi;
	}

	vector<vector<float> >eP_xs(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		float eP_xs_tmp [3] = {ForwardTransMat[0+16*i],ForwardTransMat[1+16*i],ForwardTransMat[2+16*i]};
		eP_xs[i] = vector<float>(eP_xs_tmp,eP_xs_tmp+3);
	}

	vector<vector<float> >ReP_xs(nSections);
	vector<vector<float> >ReP_ys(nSections);
	vector<vector<float> >ReP_zs(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		if(i == 0)
		{
			float ReP_xs_tmp [3] = {cos(phi[0]),sin(phi[0]),0};
			float ReP_ys_tmp [3] = {-sin(phi[0]),cos(phi[0]),0};
			float ReP_zs_tmp [3] = {0,0,1};
			ReP_xs[i] = vector<float> (ReP_xs_tmp,ReP_xs_tmp+3);
			ReP_ys[i] = vector<float> (ReP_ys_tmp,ReP_ys_tmp+3);
			ReP_zs[i] = vector<float> (ReP_zs_tmp,ReP_zs_tmp+3);
		}
		else
		{		
			float ReP_xs_tmp [3] = {rotateEPs[i][0],rotateEPs[i][1],rotateEPs[i][2]};
			float ReP_ys_tmp [3] = {rotateEPs[i][4],rotateEPs[i][5],rotateEPs[i][6]};
			float ReP_zs_tmp [3] = {rotateEPs[i][8],rotateEPs[i][9],rotateEPs[i][10]};
			ReP_xs[i] = vector<float> (ReP_xs_tmp,ReP_xs_tmp+3);
			ReP_ys[i] = vector<float> (ReP_ys_tmp,ReP_ys_tmp+3);
			ReP_zs[i] = vector<float> (ReP_zs_tmp,ReP_zs_tmp+3);
		}
	}

	vector<vector<float> >Cents(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		float endpt1[3]= {endPoints[i][0], endPoints[i][1],endPoints[i][2]};
		float endpt2[3]= {endPoints[i+1][0], endPoints[i+1][1],endPoints[i+1][2]};
		float pt1_vec[3] = {ReP_xs[i][0],ReP_xs[i][1], ReP_xs[i][2]};
		float pt2_vec[3] = {eP_xs[i][0],eP_xs[i][1], eP_xs[i][2]};
		float *Centi = TwolineIntersection(endpt1,endpt2,pt1_vec,pt2_vec);//ALMEM
		
		if(!Centi)
		{
			float vec_enp1_enpt2[3] = {endpt2[0] - endpt1[0], endpt2[1] - endpt1[1], endpt2[2] - endpt1[2]};
			Normalize(vec_enp1_enpt2,3);
			float cross_product[3] = {0.0};
			CrossProduct(vec_enp1_enpt2,pt1_vec,cross_product);
			float cross_product_len = norm_vec_3(cross_product);
			if(fabs(cross_product_len) <= 0.0001)
			{
				float center_tmp [3] = {(endpt2[0] + endpt1[0])/2, (endpt2[1] + endpt1[1])/2, (endpt2[2] + endpt1[2])/2};
				Cents[i] = vector<float>(center_tmp,center_tmp+3);
			}
			else
			{
				Cents[i].clear();
			}
		}
		else
		{
			Cents[i] = vector<float>(Centi,Centi+3);
		}
		delete [] Centi;
	}

	
	
	float base_frame_f[16] = {0};
	for(int i =0; i < 16; ++i)
	{
		base_frame_f[i] = this->base_trans_matrix[i];
	}

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix(); // save the model view matrix before setting the base frame
	glMultMatrixf(base_frame_f);
	
	for(int i = 0; i < nSections; ++i)
	{
		glColor3f(0.7,0.7,0.7);
		// if(i == 0)
		// {
		// 	glColor3f(0.0,0.0,0.0);
		// }
		// if(i == 1)
		// {
		// 	glColor3f(1.0,0.0,0.0);
		// }
		// if(i == 2)
		// {
		// 	glColor3f(0.0,1.0,0.0);
		// }
		// if(i == 3)
		// {
		// 	glColor3f(0.0,0.0,1.0);
		// }
		// if(i == 4)
		// {
		// 	glColor3f(1.0,1.0,0.0);
		// }
		// if(i == 5)
		// {
		// 	glColor3f(0.0,1.0,1.0);
		// }
		// if(i == 6)
		// {
		// 	glColor3f(1.0,0.0,1.0);
		// }
		// if(i == 7)
		// {
		// 	glColor3f(1.0,1.0,1.0);
		// }
		if(Cents[i].size() == 0)
		{
			
			float Secz[3] = {ReP_zs[i][0], ReP_zs[i][1], ReP_zs[i][2]};
			float Secx[3] = {ReP_xs[i][0],ReP_xs[i][1],ReP_xs[i][2]};
			float Secy [3] = {0};
			CrossProduct(Secz,Secx,Secy);
			CrossProduct(Secy,Secz,Secx);
			Normalize(Secz,3);
			Normalize(Secx,3);
			Normalize(Secy,3);
			float m[16] ={Secx[0], Secx[1], Secx[2], 0,
						  Secy[0], Secy[1], Secy[2], 0,
						  Secz[0], Secz[1], Secz[2], 0,
						  endPoints[i][0],  endPoints[i][1], endPoints[i][2], 1};
			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();
			glMultMatrixf(m);
			gluCylinder(quad,detaR,detaR,s[i],30,30);
			glPopMatrix();
			
	
			continue;
		}
		
		float deta_s = 0.005;
		vector<float>deta_thetas(nSections);
		vector<vector<float> >us(nSections);
		deta_thetas[i] = deta_s/r[i];

		float uilen = two_pts_dist(endPoints[i],Cents[i]);
		float us_tmp [3] = {(endPoints[i][0] - Cents[i][0])/uilen,(endPoints[i][1] - Cents[i][1])/uilen, (endPoints[i][2] - Cents[i][2])/uilen};
		us[i] = vector<float> (us_tmp, us_tmp+3);
		
		int num_of_mid_draw = 0.5*(s[i]/r[i]+deta_thetas[i]) / deta_thetas[i];
		int cnt = 0;
		for(float t = 0; t <= (s[i]/r[i])+deta_thetas[i]; t +=deta_thetas[i])
		{
			/* alternate color */
			if (cnt==0)
				glColor3f(0.8,0.8,0.8);
			if (cnt==num_of_mid_draw)
				glColor3f(0.,0.7,0.);

			/* actual drawing */
			float p[3] = {0};
			for(int j =0; j<3; ++j)
			{
				p[j] = Cents[i][j]+ r[i]*cos(t)*us[i][j]+r[i]*sin(t)*(ReP_zs[i][j]);
			}
		 
			float Secx [3] = {p[0] - Cents[i][0], p[1] - Cents[i][1], p[2] - Cents[i][2]};
			float Secy [3] = {ReP_ys[i][0],ReP_ys[i][1],ReP_ys[i][2]};
			float Secz [3] = {Secx[1]*Secy[2] - Secx[2]*Secy[1], Secx[2]*Secy[0] - Secx[0]*Secy[2], Secx[0]*Secy[1] - Secx[1]*Secy[0]};
			Normalize(Secx,3);
			Normalize(Secy,3);
			Normalize(Secz,3);
			
			
			glMatrixMode(GL_MODELVIEW);
			float m[16] ={Secx[0], Secx[1], Secx[2], 0,
						  Secy[0], Secy[1], Secy[2], 0,
						  Secz[0], Secz[1], Secz[2], 0,
						   p[0],  p[1],  p[2], 1};
		
			glPushMatrix();
			glMultMatrixf(m);
			
			gluCylinder(quad,detaR,detaR,deta_s,20,20);
			glPopMatrix(); 

			/* highlight section endplate */
			// if (t > s[i]/r[i])
			// {
			// 	glPushMatrix();
			// 	glMultMatrixf(m);
			// 	glColor3f(0.0, 0.0, 0.0);
			// 	gluCylinder(quad,1.2*detaR,1.2*detaR,1.5*deta_s,20,20);
			// 	glPopMatrix(); 
			// }
			// glColor3f(0.9,0.9,0.5);
			++cnt;
		}
	}
	glPopMatrix(); // pop the base frame of the manipulator	
}

void ManipulatorNSections::generatePointsOnArm(std::vector<secPnts> &sec_pnts,
                                               const float deta_s)
{
	int nSections = n_sections;
	vector<float> Config = config;
		
	vector<float> s(nSections);
	vector<float> k(nSections);
	vector<float> phi(nSections);
	vector<float> r(nSections);

	for(int i = 0; i < nSections; ++i)
	{
		s[i] = Config[i*3];
		k[i] = Config[i*3+1];
		r[i] = 1/fabs(k[i]);
		phi[i] = Config[i*3+2];
	}

	vector<vector<float> >RotaAboutZs(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		float RotaAboutZi[16] = {cos(phi[i]),sin(phi[i]),0,0, -sin(phi[i]),cos(phi[i]),0,0, 0,0,1,0, 0,0,0,1};
		vector<float> temp(RotaAboutZi,RotaAboutZi+16);
		RotaAboutZs[i] = temp;
	}

	vector<float>ForwardTransMat(nSections*16);
	if(trans_matrix.size() == 0)
	{
		compute_trans_matrix(ForwardTransMat);
	}
	else
	{
		ForwardTransMat = trans_matrix;
	}
	
	vector<vector<float> >endPoints(nSections+1, vector<float>(3,0));
	
	for(int i = 1; i < nSections+1; ++i)
	{	
		float endPoints_tmp [3] = {ForwardTransMat[(i-1)*16+12],ForwardTransMat[(i-1)*16+13],ForwardTransMat[(i-1)*16+14]};
		endPoints[i] = vector<float>(endPoints_tmp,endPoints_tmp+3);
	}


	vector<vector<float> >rotateEPs(nSections);
	for(int i = 1; i < nSections; ++i)
	{
		float* rotateEPi = MatrixMulti(&ForwardTransMat[(i-1)*16], RotaAboutZs[i],4,4,4,4); //ALMEM
		rotateEPs[i] = vector<float>(rotateEPi,rotateEPi+16);
		delete [] rotateEPi;
	}

	vector<vector<float> >eP_xs(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		float eP_xs_tmp [3] = {ForwardTransMat[0+16*i],ForwardTransMat[1+16*i],ForwardTransMat[2+16*i]};
		eP_xs[i] = vector<float>(eP_xs_tmp,eP_xs_tmp+3);
	}

	vector<vector<float> >ReP_xs(nSections);
	vector<vector<float> >ReP_ys(nSections);
	vector<vector<float> >ReP_zs(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		if(i == 0)
		{
			float ReP_xs_tmp [3] = {cos(phi[0]),sin(phi[0]),0};
			float ReP_ys_tmp [3] = {-sin(phi[0]),cos(phi[0]),0};
			float ReP_zs_tmp [3] = {0,0,1};
			ReP_xs[i] = vector<float> (ReP_xs_tmp,ReP_xs_tmp+3);
			ReP_ys[i] = vector<float> (ReP_ys_tmp,ReP_ys_tmp+3);
			ReP_zs[i] = vector<float> (ReP_zs_tmp,ReP_zs_tmp+3);
		}
		else
		{		
			float ReP_xs_tmp [3] = {rotateEPs[i][0],rotateEPs[i][1],rotateEPs[i][2]};
			float ReP_ys_tmp [3] = {rotateEPs[i][4],rotateEPs[i][5],rotateEPs[i][6]};
			float ReP_zs_tmp [3] = {rotateEPs[i][8],rotateEPs[i][9],rotateEPs[i][10]};
			ReP_xs[i] = vector<float> (ReP_xs_tmp,ReP_xs_tmp+3);
			ReP_ys[i] = vector<float> (ReP_ys_tmp,ReP_ys_tmp+3);
			ReP_zs[i] = vector<float> (ReP_zs_tmp,ReP_zs_tmp+3);
		}
	}

	vector<vector<float> >Cents(nSections);
	for(int i = 0; i < nSections; ++i)
	{
		float endpt1[3]= {endPoints[i][0], endPoints[i][1],endPoints[i][2]};
		float endpt2[3]= {endPoints[i+1][0], endPoints[i+1][1],endPoints[i+1][2]};
		float pt1_vec[3] = {ReP_xs[i][0],ReP_xs[i][1], ReP_xs[i][2]};
		float pt2_vec[3] = {eP_xs[i][0],eP_xs[i][1], eP_xs[i][2]};
		float *Centi = TwolineIntersection(endpt1,endpt2,pt1_vec,pt2_vec);//ALMEM
		
		if(!Centi)
		{
			float vec_enp1_enpt2[3] = {endpt2[0] - endpt1[0], endpt2[1] - endpt1[1], endpt2[2] - endpt1[2]};
			Normalize(vec_enp1_enpt2,3);
			float cross_product[3] = {0.0};
			CrossProduct(vec_enp1_enpt2,pt1_vec,cross_product);
			float cross_product_len = norm_vec_3(cross_product);
			if(fabs(cross_product_len) <= 0.0001)
			{
				float center_tmp [3] = {(endpt2[0] + endpt1[0])/2, (endpt2[1] + endpt1[1])/2, (endpt2[2] + endpt1[2])/2};
				Cents[i] = vector<float>(center_tmp,center_tmp+3);
			}
			else
			{
				Cents[i].clear();
			}
		}
		else
		{
			Cents[i] = vector<float>(Centi,Centi+3);
		}
		delete [] Centi;
	}

	
	
	float base_frame_f[16] = {0};
	for(int i =0; i < 16; ++i)
	{
		base_frame_f[i] = this->base_trans_matrix[i];
	}

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix(); // save the model view matrix before setting the base frame
	glMultMatrixf(base_frame_f);
	
	for(int i = 0; i < nSections; ++i)
	{
		if(i == 0)
		{
			glColor3f(0.0,0.0,0.0);
		}
		if(i == 1)
		{
			glColor3f(1.0,0.0,0.0);
		}
		if(i == 2)
		{
			glColor3f(0.0,1.0,0.0);
		}
		if(i == 3)
		{
			glColor3f(0.0,0.0,1.0);
		}
		if(Cents[i].size() == 0)
		{
			
			float Secz[3] = {ReP_zs[i][0], ReP_zs[i][1], ReP_zs[i][2]};
			float Secx[3] = {ReP_xs[i][0],ReP_xs[i][1],ReP_xs[i][2]};
			float Secy [3] = {0};
			CrossProduct(Secz,Secx,Secy);
			CrossProduct(Secy,Secz,Secx);
			Normalize(Secz,3);
			Normalize(Secx,3);
			Normalize(Secy,3);
			float m[16] ={Secx[0], Secx[1], Secx[2], 0,
						  Secy[0], Secy[1], Secy[2], 0,
						  Secz[0], Secz[1], Secz[2], 0,
						  endPoints[i][0],  endPoints[i][1], endPoints[i][2], 1};
			glMatrixMode(GL_MODELVIEW);
			glPushMatrix();
			glMultMatrixf(m);
			//~ gluCylinder(quad,detaR,detaR,s[i],30,30);
			glPopMatrix();
			
	
			continue;
		}
		
		// float deta_s = 0.02; // control how many cylinders to approximate arc
		vector<float>deta_thetas(nSections);
		vector<vector<float> >us(nSections);
		deta_thetas[i] = deta_s/r[i];

		float uilen = two_pts_dist(endPoints[i],Cents[i]);
		float us_tmp [3] = {(endPoints[i][0] - Cents[i][0])/uilen,(endPoints[i][1] - Cents[i][1])/uilen, (endPoints[i][2] - Cents[i][2])/uilen};
		us[i] = vector<float> (us_tmp, us_tmp+3);
		
		for(float t = 0; t <= (s[i]/r[i])+deta_thetas[i]; t +=deta_thetas[i])
		{
			float p[3] = {0};
			for(int j =0; j<3; ++j)
			{
				p[j] = Cents[i][j]+ r[i]*cos(t)*us[i][j]+r[i]*sin(t)*(ReP_zs[i][j]);
			}
		 
			float Secx [3] = {p[0] - Cents[i][0], p[1] - Cents[i][1], p[2] - Cents[i][2]};
			float Secy [3] = {ReP_ys[i][0],ReP_ys[i][1],ReP_ys[i][2]};
			float Secz [3] = {Secx[1]*Secy[2] - Secx[2]*Secy[1], Secx[2]*Secy[0] - Secx[0]*Secy[2], Secx[0]*Secy[1] - Secx[1]*Secy[0]};
			Normalize(Secx,3);
			Normalize(Secy,3);
			Normalize(Secz,3);
			
			
			glMatrixMode(GL_MODELVIEW);
			float m[16] ={Secx[0], Secx[1], Secx[2], 0,
						  Secy[0], Secy[1], Secy[2], 0,
						  Secz[0], Secz[1], Secz[2], 0,
						   p[0],  p[1],  p[2], 1};
		
			glPushMatrix();
			glMultMatrixf(m);
			
			//~ gluCylinder(quad,detaR,detaR,deta_s,20,20);
			
			// get current pnt
            float* this_pnt = MatrixMulti(base_frame_f, m, 4,4,4,4);
            Eigen::Vector3f cur_pnt(this_pnt[12],this_pnt[13],this_pnt[14]);
            delete [] this_pnt;
            // save current pnt
			sec_pnts.at(i).push_back(cur_pnt);	
			
			glPopMatrix(); 
		}
	}
	glPopMatrix(); // pop the base frame of the manipulator
	
}
	

ManipulatorNSections::ManipulatorNSections(int _n_sections, float* _config, float* _base_frame)
{
	n_sections = _n_sections;
	config = vector<float>(3*n_sections);
	for(int i = 0; i < 3*n_sections; ++i)
	{
		config[i] = _config[i];
	}

	detaR = 0.01;
	quad = gluNewQuadric();

	// Set the base frame
	float base_trans_f[16] = {0};
	float v_x [3] = {_base_frame[0],_base_frame[1],_base_frame[2]};
	float v_y [3] = {_base_frame[4],_base_frame[5],_base_frame[6]};
	float v_z [3] = {_base_frame[8],_base_frame[9],_base_frame[10]};
	float dot_product_x_y = 0.0;
	float dot_product_y_z = 0.0;
	float dot_product_x_z = 0.0;
	DotProduct(v_x,v_y,&dot_product_x_y);
	DotProduct(v_y,v_z,&dot_product_y_z);
	DotProduct(v_x,v_z,&dot_product_x_z);

	if(fabsf(dot_product_x_y) >= 0.001 || fabsf(dot_product_y_z) >= 0.001 || fabsf(dot_product_x_z) >= 0.001)
	{
		bool success  = false;
	}

	for(int i = 0; i < 12; i = i + 4)
	{
		float vector_f [3] = {_base_frame[i],_base_frame[i+1],_base_frame[i+2]};
		Normalize(vector_f,3);
		base_trans_f[i] = vector_f[0];
		base_trans_f[i+1] = vector_f[1];
		base_trans_f[i+2] = vector_f[2];
		base_trans_f[i+3] = 0;
	}

	base_trans_f[12] = _base_frame[12];
	base_trans_f[13] = _base_frame[13];
	base_trans_f[14] = _base_frame[14];
	base_trans_f[15] = 1.0;
	base_trans_matrix = vector<float>(base_trans_f,base_trans_f+16);


	///////////////
	trans_matrix = vector<float>(n_sections*16);

	if(compute_trans_matrix(trans_matrix))
	{
		section_cir_centers_local = vector<float>(n_sections*3); //section centers wrt to robot base (local) frame
		bool center_computed = compute_section_cir_centers_local(section_cir_centers_local);

		section_cir_centers_wrt_world= vector<float>(n_sections*3);	//section centers wrt to world frame
		compute_section_cir_centers_wrt_world(section_cir_centers_wrt_world);

		section_plane_normals_local = vector<float>(n_sections*3);	//section normal wrt to robot base (local) frame
		bool section_normal_computed = compute_section_plane_normals_local(section_plane_normals_local);
	
		section_plane_normals_wrt_world= vector<float>(n_sections*3); //section normal wrt world
		compute_section_plane_normals_wrt_world(section_plane_normals_wrt_world);
			
		section_end_points_wrt_world = vector<float>((n_sections+1)*3); // sections tip points wrt world frame
		bool section_endpoints_computed = compute_section_end_points_wrt_world(section_end_points_wrt_world);

		trans_matrix_wrt_world = compute_trans_matrix_wrt_world(); // compute the trans matrix wrt the world

	}
	else
	{
		bool success  = false;
	}

}

ManipulatorNSections::ManipulatorNSections( int _n_sections, 
											std::vector<float> init_config, 
											Eigen::Matrix4f init_base_frame)
{
	n_sections = _n_sections;
	config = vector<float>(3*n_sections);
	for(int i = 0; i < 3*n_sections; ++i)
	{
		config[i] = init_config[i];
	}

	detaR = 0.01;
	quad = gluNewQuadric();
	
	float _base_frame[16] = {0.0};
	_base_frame[0] = init_base_frame(0,0);
	_base_frame[1] = init_base_frame(1,0);
	_base_frame[2] = init_base_frame(2,0);
	_base_frame[3] = init_base_frame(3,0);
	
	_base_frame[4] = init_base_frame(0,1);
	_base_frame[5] = init_base_frame(1,1);
	_base_frame[6] = init_base_frame(2,1);
	_base_frame[7] = init_base_frame(3,1);
	
	_base_frame[8] = init_base_frame(0,2);
	_base_frame[9] = init_base_frame(1,2);
	_base_frame[10] = init_base_frame(2,2);
	_base_frame[11] = init_base_frame(3,2);
	
	_base_frame[12] = init_base_frame(0,3);
	_base_frame[13] = init_base_frame(1,3);
	_base_frame[14] = init_base_frame(2,3);
	_base_frame[15] = init_base_frame(3,3);
	
	// Set the base frame
	float base_trans_f[16] = {0};
	float v_x [3] = {_base_frame[0],_base_frame[1],_base_frame[2]};
	float v_y [3] = {_base_frame[4],_base_frame[5],_base_frame[6]};
	float v_z [3] = {_base_frame[8],_base_frame[9],_base_frame[10]};
	float dot_product_x_y = 0.0;
	float dot_product_y_z = 0.0;
	float dot_product_x_z = 0.0;
	DotProduct(v_x,v_y,&dot_product_x_y);
	DotProduct(v_y,v_z,&dot_product_y_z);
	DotProduct(v_x,v_z,&dot_product_x_z);

	if(fabsf(dot_product_x_y) >= 0.001 || fabsf(dot_product_y_z) >= 0.001 || fabsf(dot_product_x_z) >= 0.001)
	{
		bool success  = false;
	}

	for(int i = 0; i < 12; i = i + 4)
	{
		float vector_f [3] = {_base_frame[i],_base_frame[i+1],_base_frame[i+2]};
		Normalize(vector_f,3);
		base_trans_f[i] = vector_f[0];
		base_trans_f[i+1] = vector_f[1];
		base_trans_f[i+2] = vector_f[2];
		base_trans_f[i+3] = 0;
	}

	base_trans_f[12] = _base_frame[12];
	base_trans_f[13] = _base_frame[13];
	base_trans_f[14] = _base_frame[14];
	base_trans_f[15] = 1.0;
	base_trans_matrix = vector<float>(base_trans_f,base_trans_f+16);


	///////////////
	trans_matrix = vector<float>(n_sections*16);

	if(compute_trans_matrix(trans_matrix))
	{
		section_cir_centers_local = vector<float>(n_sections*3); //section centers wrt to robot base (local) frame
		bool center_computed = compute_section_cir_centers_local(section_cir_centers_local);

		section_cir_centers_wrt_world= vector<float>(n_sections*3);	//section centers wrt to world frame
		compute_section_cir_centers_wrt_world(section_cir_centers_wrt_world);

		section_plane_normals_local = vector<float>(n_sections*3);	//section normal wrt to robot base (local) frame
		bool section_normal_computed = compute_section_plane_normals_local(section_plane_normals_local);
	
		section_plane_normals_wrt_world= vector<float>(n_sections*3); //section normal wrt world
		compute_section_plane_normals_wrt_world(section_plane_normals_wrt_world);
			
		section_end_points_wrt_world = vector<float>((n_sections+1)*3); // sections tip points wrt world frame
		bool section_endpoints_computed = compute_section_end_points_wrt_world(section_end_points_wrt_world);

		trans_matrix_wrt_world = compute_trans_matrix_wrt_world(); // compute the trans matrix wrt the world

	}
	else
	{
		bool success  = false;
	}

	/* set section physical limits */
	sectionPhysicalLimits sec_limits;
	sec_limits.length_min = 0.095; //0.03; // 3 cm
	sec_limits.length_max = 0.2; //0.065; // 6.5 cm
	sec_limits.curvature_min = -15.69; //-10.0; // 13.5 m^-1 at 45 mm. curvature min max will vary depending on length 
	sec_limits.curvature_max = 15.69; //10.0; 
	sec_limits.phi_min = 0.; 
	sec_limits.phi_max = 6.28;

	for (int i=0; i<n_sections; ++i)
	{
		section_bounds[i] = sec_limits;
	}
}


bool ManipulatorNSections::Getbaseframe (float*_out_base_frame)
{
	for(int i =0; i < 16; ++i)
	{
		_out_base_frame[i] = base_trans_matrix[i];
	}
	return true;
}

bool ManipulatorNSections::Getbaseframe (vector<float>&_out_base_frame)
{
	for(int i =0; i < 16; ++i)
	{
		_out_base_frame[i] = base_trans_matrix[i];
	}
	return true;
}

bool ManipulatorNSections::compute_section_frame_wrt_world(vector<float>& _section_frame, int _sec_idx)
{
	float base_frame [16] = {0};
	float sec_frame_wrt_base[16] = {0};
	for(int i = 0; i < 16; ++i)
	{
		base_frame[i] =base_trans_matrix[i];
		sec_frame_wrt_base[i] = trans_matrix[_sec_idx*16 + i];
	}
	float*  sec_frame_wrt_world = MatrixMulti(base_frame,sec_frame_wrt_base,4,4,4,4);
	for(int i = 0 ; i < 16; ++i)
	{
		_section_frame[i] = sec_frame_wrt_world[i];
	}
	delete [] sec_frame_wrt_world;
	return true;
}

bool ManipulatorNSections::compute_section_frame_wrt_world(float* _section_frame, int _sec_idx)
{
	float base_frame [16] = {0};
	float sec_frame_wrt_base[16] = {0};
	for(int i = 0; i < 16; ++i)
	{
		base_frame[i] =base_trans_matrix[i];
		sec_frame_wrt_base[i] = trans_matrix[_sec_idx*16 + i];
	}
	float*  sec_frame_wrt_world = MatrixMulti(base_frame,sec_frame_wrt_base,4,4,4,4);
	for(int i = 0 ; i < 16; ++i)
	{
		_section_frame[i] = sec_frame_wrt_world[i];
	}
	delete [] sec_frame_wrt_world;
	return true;
}

vector<float> ManipulatorNSections::compute_trans_matrix_wrt_world()
{
	int nsections = this->get_n_sections();
	vector<float> trans_matrix_wrt_world_v(nsections*16);
	for(int s = 0; s < nsections; ++s)
	{
		float base_frame [16] = {0};
		float sec_frame_wrt_base[16] = {0};
		for(int i = 0; i < 16; ++i)
		{
			base_frame[i] =base_trans_matrix[i];
			sec_frame_wrt_base[i] = trans_matrix[s*16 + i];
		}
		float*  sec_frame_wrt_world = MatrixMulti(base_frame,sec_frame_wrt_base,4,4,4,4);
		
		for(int i = 0 ; i < 16; ++i)
		{
			trans_matrix_wrt_world_v[s*16 + i] = sec_frame_wrt_world[i];
		}
		
		delete [] sec_frame_wrt_world;

	}
	return trans_matrix_wrt_world_v;
}

bool ManipulatorNSections::update_config(vector<float> _config)
{
	if(_config.size() != config.size())
	{
		return false;
	}
	
	for(int i = 0; i < _config.size(); ++i)
	{
		config[i] = _config[i];
	}
	
	if(compute_trans_matrix(trans_matrix))
	{
	//	section_cir_centers_local = vector<float>(n_sections*3);
		bool center_computed = compute_section_cir_centers_local(section_cir_centers_local);// wrt base frame
		
	//	section_cir_centers_wrt_world = vector<float>(n_sections*3);
	    compute_section_cir_centers_wrt_world(section_cir_centers_wrt_world);// wrt world frame

	//	section_plane_normals_local = vector<float>(n_sections*3);
		bool section_normal_computed = compute_section_plane_normals_local(section_plane_normals_local);//wrt base frame

//		section_plane_normals_wrt_world = vector<float>(n_sections*3);
		compute_section_plane_normals_wrt_world(section_plane_normals_wrt_world);//wrt world frame

//		section_end_points_wrt_world = vector<float>((n_sections+1)*3);
		bool section_endpoints_computed = compute_section_end_points_wrt_world(section_end_points_wrt_world);// wrt world frame
		
	
		if(center_computed && section_normal_computed && section_endpoints_computed)
		{
			trans_matrix_wrt_world = compute_trans_matrix_wrt_world(); // compute the trans matrix wrt the world
			return true;
		}
		else
		{
			cout<<"arm configuration Updating is not successful !!!!!!!!!!"<<endl;
			return false;
		}
	}
	else
	{
		cout<<"arm configuration Updating is not successful !!!!!!!!!!"<<endl;
		return false;
	}
}

bool ManipulatorNSections::update_config(vector<float> _config, vector<float> base_frame)
{
	float base_frame_f[16] = {0};
	for(int i = 0; i < 16; ++i)
	{
		base_frame_f[i] = base_frame[i];
	}
	if(!this->set_base_frame(base_frame_f))
	{
		cout<<"arm configuration Updating is not successful, base frame setting failed !!!!!!!!!!"<<endl;
		return false;
	}
	if(!this->update_config(_config))
	{
		cout<<"arm configuration Updating is not successful !!!!!!!!!!"<<endl;
		return false;
	}
	
	return true;
}

vector<float> ManipulatorNSections::get_config () 
{
	return config;
}

void ManipulatorNSections::get_config (float* config_des) 
{
	for(int i = 0; i <config.size(); ++i)
	{
		config_des[i] = config[i];
	}
}

void ManipulatorNSections::get_config (vector<float>& config_des) 
{
    for(int i = 0; i <config.size(); ++i)
	{
		config_des[i] = config[i];
	}
}

int ManipulatorNSections::get_n_sections () 
{
	return this->n_sections;
}

bool ManipulatorNSections::set_base_frame(float* _base_frame)
{
	float base_trans_f[16] = {0};
	for(int i = 0; i < 16; ++i)
	{
		base_trans_f[i] = _base_frame[i];
	}
	base_trans_matrix = vector<float>(base_trans_f,base_trans_f+16);
	return true;
}

bool ManipulatorNSections::set_base_frame(vector<float> _base_frame)
{
	if(_base_frame.size() != 16)
	{
		return false;
	}
	base_trans_matrix = _base_frame;
	return true;
}

float ManipulatorNSections::get_sec_width()
{
	return this->detaR;
}

bool ManipulatorNSections::get_trans_matrix_wrt_world(vector<float>& _trans_mat_world)
{
	if(trans_matrix_wrt_world.size() == 0 || _trans_mat_world.size() != trans_matrix_wrt_world.size())
	{
		return false;
	}

	for(int i = 0; i < trans_matrix_wrt_world.size(); ++i)
	{
		_trans_mat_world[i] = trans_matrix_wrt_world[i];
	}
	return true;
}

vector<float>  ManipulatorNSections::get_section_end_points_wrt_world()
{
	return this->section_end_points_wrt_world;
}

vector<float> ManipulatorNSections::inverseKinematic_one_section(float* _end_point_f, float* base_frame)
{
	//*****************//// inverse kinematics Part to compute the

	float startFrame[16] = {0};
	int nSections = this->n_sections;
	vector<float> _output_config(3);
	for(int i = 0; i < 16; ++i)
	{
		startFrame[i] = base_frame[i];
	}


	float EndPoint[4] = {0,0,0,1};
	for(int i = 0; i < 3; ++i)
	{
		EndPoint[i] = _end_point_f[i];
	}

	// transform the end point to the local section frame
	float* LoacalPoint = MatrixMulti(InverMatrix(startFrame),EndPoint,4,4,4,1);
	float phi = atan(LoacalPoint[1]/LoacalPoint[0]);
	float temp = (LoacalPoint[0]*LoacalPoint[0] + LoacalPoint[1]*LoacalPoint[1] +LoacalPoint[2]*LoacalPoint[2]);
	assert(fabsf(temp) >= 0.00001);

	float k = 2*sqrt(LoacalPoint[0]*LoacalPoint[0] + LoacalPoint[1]*LoacalPoint[1])/(LoacalPoint[0]*LoacalPoint[0] + LoacalPoint[1]*LoacalPoint[1] +LoacalPoint[2]*LoacalPoint[2]);
	float theta = -1.0 ;
	float cos_theta = 1- 2*(LoacalPoint[0]*LoacalPoint[0] + LoacalPoint[1]*LoacalPoint[1])/(LoacalPoint[0]*LoacalPoint[0] + LoacalPoint[1]*LoacalPoint[1] +LoacalPoint[2]*LoacalPoint[2]);

	if(LoacalPoint[2] > 0)
	{
		 theta = acos(cos_theta);
	}
	else
	{
		theta = 2*Pi_jinglin - acos(1-k*sqrt(LoacalPoint[0]*LoacalPoint[0] + LoacalPoint[1]*LoacalPoint[1]));
	}
	if(fabsf(k) < 0.00001)
	{
		int test = 0;
	}
	//assert(fabsf(k) >= 0.00001);

	float s = (1/fabsf(k)) * theta;

	_output_config[0] = s;
	_output_config[1] = (LoacalPoint[0] > 0)? -k:k;
	_output_config[2] = phi;

	return _output_config;

}

float* ManipulatorNSections:: ComputeJacobian(float* Config)
{
    float s1 = Config[0]; float k1 = Config[1]; float phi1 = Config[2];
	float s2 = Config[3]; float k2 = Config[4]; float phi2 = Config[5];
	float s3 = Config[6]; float k3 = Config[7]; float phi3 = Config[8];

	float J1_1 = (sin(k3*s3)*(sin(k2*s2)*cos(phi2)*(k1*sin(k1*s1)*cos(phi1)*cos(phi1)*cos(phi1) + k1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1)) - k1*cos(k1*s1)*cos(k2*s2)*cos(phi1)))/k3 - sin(k1*s1)*cos(phi1) - (cos(phi3)*(cos(k3*s3) - 1)*((k1*sin(k1*s1)*cos(phi1)*cos(phi1)*cos(phi1) + k1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + k1*cos(k1*s1)*cos(phi1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3 - (cos(phi2)*(cos(k2*s2) - 1)*(k1*sin(k1*s1)*cos(phi1)*cos(phi1)*cos(phi1) + k1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1)))/k2 - (k1*cos(k1*s1)*sin(k2*s2)*cos(phi1))/k2 + (sin(phi3)*(cos(k3*s3) - 1)*(sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(k1*sin(k1*s1)*cos(phi1)*cos(phi1)*cos(phi1) + k1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1)))/k3;
	float J2_1 = (sin(k3*s3)*(sin(k2*s2)*cos(phi2)*(sin(phi1)*(k1*sin(k1*s1) - k1*sin(k1*s1)*cos(phi1)*cos(phi1)) + k1*sin(k1*s1)*cos(phi1)*cos(phi1)*sin(phi1)) + sin(k2*s2)*sin(phi2)*(cos(phi1)*(k1*sin(k1*s1) - k1*sin(k1*s1)*cos(phi1)*cos(phi1)) - k1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1)) - k1*cos(k1*s1)*cos(k2*s2)*sin(phi1)))/k3 - sin(k1*s1)*sin(phi1) + (sin(phi3)*(cos(k3*s3) - 1)*((sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(sin(phi1)*(k1*sin(k1*s1) - k1*sin(k1*s1)*cos(phi1)*cos(phi1)) + k1*sin(k1*s1)*cos(phi1)*cos(phi1)*sin(phi1)) - (cos(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) - cos(phi2)*sin(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(cos(phi1)*(k1*sin(k1*s1) - k1*sin(k1*s1)*cos(phi1)*cos(phi1)) - k1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1))))/k3 - (cos(phi3)*(cos(k3*s3) - 1)*((sin(phi1)*(k1*sin(k1*s1) - k1*sin(k1*s1)*cos(phi1)*cos(phi1)) + k1*sin(k1*s1)*cos(phi1)*cos(phi1)*sin(phi1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + (sin(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) + cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(cos(phi1)*(k1*sin(k1*s1) - k1*sin(k1*s1)*cos(phi1)*cos(phi1)) - k1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1)) + k1*cos(k1*s1)*sin(phi1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3 - (cos(phi2)*(cos(k2*s2) - 1)*(sin(phi1)*(k1*sin(k1*s1) - k1*sin(k1*s1)*cos(phi1)*cos(phi1)) + k1*sin(k1*s1)*cos(phi1)*cos(phi1)*sin(phi1)))/k2 - (sin(phi2)*(cos(k2*s2) - 1)*(cos(phi1)*(k1*sin(k1*s1) - k1*sin(k1*s1)*cos(phi1)*cos(phi1)) - k1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1)))/k2 - (k1*cos(k1*s1)*sin(k2*s2)*sin(phi1))/k2;
	float J3_1 = cos(k1*s1) - (sin(k3*s3)*(k1*cos(k2*s2)*sin(k1*s1) + sin(k2*s2)*cos(phi2)*(k1*cos(k1*s1)*cos(phi1)*cos(phi1) + k1*cos(k1*s1)*sin(phi1)*sin(phi1))))/k3 + (cos(phi2)*(cos(k2*s2) - 1)*(k1*cos(k1*s1)*cos(phi1)*cos(phi1) + k1*cos(k1*s1)*sin(phi1)*sin(phi1)))/k2 + (cos(phi3)*(cos(k3*s3) - 1)*((k1*cos(k1*s1)*cos(phi1)*cos(phi1) + k1*cos(k1*s1)*sin(phi1)*sin(phi1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) - k1*sin(k1*s1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3 - (k1*sin(k1*s1)*sin(k2*s2))/k2 - (sin(phi3)*(cos(k3*s3) - 1)*(sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(k1*cos(k1*s1)*cos(phi1)*cos(phi1) + k1*cos(k1*s1)*sin(phi1)*sin(phi1)))/k3;

	float J1_2 = (sin(k3*s3)*(sin(k2*s2)*cos(phi2)*(s1*sin(k1*s1)*cos(phi1)*cos(phi1)*cos(phi1) + s1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1)) - s1*cos(k1*s1)*cos(k2*s2)*cos(phi1)))/k3 - (cos(phi1)*(cos(k1*s1) - 1))/k1*k1 - (s1*sin(k1*s1)*cos(phi1))/k1 - (cos(phi3)*(cos(k3*s3) - 1)*((s1*sin(k1*s1)*cos(phi1)*cos(phi1)*cos(phi1) + s1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + s1*cos(k1*s1)*cos(phi1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3 - (cos(phi2)*(cos(k2*s2) - 1)*(s1*sin(k1*s1)*cos(phi1)*cos(phi1)*cos(phi1) + s1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1)))/k2 - (s1*cos(k1*s1)*sin(k2*s2)*cos(phi1))/k2 + (sin(phi3)*(cos(k3*s3) - 1)*(sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(s1*sin(k1*s1)*cos(phi1)*cos(phi1)*cos(phi1) + s1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1)))/k3;
	float J2_2 = (sin(k3*s3)*(sin(k2*s2)*cos(phi2)*(sin(phi1)*(s1*sin(k1*s1) - s1*sin(k1*s1)*cos(phi1)*cos(phi1)) + s1*sin(k1*s1)*cos(phi1)*cos(phi1)*sin(phi1)) + sin(k2*s2)*sin(phi2)*(cos(phi1)*(s1*sin(k1*s1) - s1*sin(k1*s1)*cos(phi1)*cos(phi1)) - s1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1)) - s1*cos(k1*s1)*cos(k2*s2)*sin(phi1)))/k3 - (sin(phi1)*(cos(k1*s1) - 1))/k1*k1 - (s1*sin(k1*s1)*sin(phi1))/k1 + (sin(phi3)*(cos(k3*s3) - 1)*((sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(sin(phi1)*(s1*sin(k1*s1) - s1*sin(k1*s1)*cos(phi1)*cos(phi1)) + s1*sin(k1*s1)*cos(phi1)*cos(phi1)*sin(phi1)) - (cos(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) - cos(phi2)*sin(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(cos(phi1)*(s1*sin(k1*s1) - s1*sin(k1*s1)*cos(phi1)*cos(phi1)) - s1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1))))/k3 - (cos(phi3)*(cos(k3*s3) - 1)*((sin(phi1)*(s1*sin(k1*s1) - s1*sin(k1*s1)*cos(phi1)*cos(phi1)) + s1*sin(k1*s1)*cos(phi1)*cos(phi1)*sin(phi1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + (sin(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) + cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(cos(phi1)*(s1*sin(k1*s1) - s1*sin(k1*s1)*cos(phi1)*cos(phi1)) - s1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1)) + s1*cos(k1*s1)*sin(phi1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3 - (cos(phi2)*(cos(k2*s2) - 1)*(sin(phi1)*(s1*sin(k1*s1) - s1*sin(k1*s1)*cos(phi1)*cos(phi1)) + s1*sin(k1*s1)*cos(phi1)*cos(phi1)*sin(phi1)))/k2 - (sin(phi2)*(cos(k2*s2) - 1)*(cos(phi1)*(s1*sin(k1*s1) - s1*sin(k1*s1)*cos(phi1)*cos(phi1)) - s1*sin(k1*s1)*cos(phi1)*sin(phi1)*sin(phi1)))/k2 - (s1*cos(k1*s1)*sin(k2*s2)*sin(phi1))/k2;
	float J3_2 = (s1*cos(k1*s1))/k1 - (sin(k3*s3)*(s1*cos(k2*s2)*sin(k1*s1) + sin(k2*s2)*cos(phi2)*(s1*cos(k1*s1)*cos(phi1)*cos(phi1) + s1*cos(k1*s1)*sin(phi1)*sin(phi1))))/k3 - sin(k1*s1)/k1*k1 + (cos(phi2)*(cos(k2*s2) - 1)*(s1*cos(k1*s1)*cos(phi1)*cos(phi1) + s1*cos(k1*s1)*sin(phi1)*sin(phi1)))/k2 + (cos(phi3)*(cos(k3*s3) - 1)*((s1*cos(k1*s1)*cos(phi1)*cos(phi1) + s1*cos(k1*s1)*sin(phi1)*sin(phi1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) - s1*sin(k1*s1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3 - (s1*sin(k1*s1)*sin(k2*s2))/k2 - (sin(phi3)*(cos(k3*s3) - 1)*(sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(s1*cos(k1*s1)*cos(phi1)*cos(phi1) + s1*cos(k1*s1)*sin(phi1)*sin(phi1)))/k3;

	float J1_3 = (sin(k3*s3)*(sin(k2*s2)*sin(phi2)*(cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + sin(k2*s2)*cos(phi2)*((cos(k1*s1) - 1)*sin(phi1)*sin(phi1)*sin(phi1) + ((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1)*sin(phi1)) + cos(k2*s2)*sin(k1*s1)*sin(phi1)))/k3 - (sin(phi1)*(cos(k1*s1) - 1))/k1 - (cos(phi3)*(cos(k3*s3) - 1)*((cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*cos(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) + cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1)) + ((cos(k1*s1) - 1)*sin(phi1)*sin(phi1)*sin(phi1) + ((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1)*sin(phi1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) - sin(k1*s1)*sin(phi1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3 - (sin(phi2)*(cos(k2*s2) - 1)*(cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)))/k2 - (sin(phi3)*(cos(k3*s3) - 1)*((cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*cos(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) - cos(phi2)*sin(phi2)*sin(phi2)*(cos(k2*s2) - 1)) - (sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*((cos(k1*s1) - 1)*sin(phi1)*sin(phi1)*sin(phi1) + ((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1)*sin(phi1))))/k3 - (cos(phi2)*(cos(k2*s2) - 1)*((cos(k1*s1) - 1)*sin(phi1)*sin(phi1)*sin(phi1) + ((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1)*sin(phi1)))/k2 + (sin(k1*s1)*sin(k2*s2)*sin(phi1))/k2;
	float J2_3 = (cos(phi1)*(cos(k1*s1) - 1))/k1 - (sin(k3*s3)*(cos(k2*s2)*sin(k1*s1)*cos(phi1) + sin(k2*s2)*cos(phi2)*(cos(phi1)*cos(phi1)*cos(phi1)*(cos(k1*s1) - 1) + cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1))) + sin(k2*s2)*sin(phi2)*(sin(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1) - sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)))))/k3 + (cos(phi3)*(cos(k3*s3) - 1)*((cos(phi1)*cos(phi1)*cos(phi1)*(cos(k1*s1) - 1) + cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + (sin(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) + cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(sin(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1) - sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1))) - sin(k1*s1)*cos(phi1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3 - (sin(k1*s1)*sin(k2*s2)*cos(phi1))/k2 - (sin(phi3)*(cos(k3*s3) - 1)*((sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(cos(phi1)*cos(phi1)*cos(phi1)*(cos(k1*s1) - 1) + cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1))) - (cos(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) - cos(phi2)*sin(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(sin(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1) - sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)))))/k3 + (cos(phi2)*(cos(k2*s2) - 1)*(cos(phi1)*cos(phi1)*cos(phi1)*(cos(k1*s1) - 1) + cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1))))/k2 + (sin(phi2)*(cos(k2*s2) - 1)*(sin(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1) - sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1))))/k2;
	float J3_3 = 0;

	float J1_4 = sin(k2*s2)*sin(phi2)*(sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) - cos(k2*s2)*sin(k1*s1)*cos(phi1) + (sin(k3*s3)*(k2*sin(k1*s1)*sin(k2*s2)*cos(phi1) - k2*cos(k2*s2)*cos(phi2)*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1)) + k2*cos(k2*s2)*sin(phi2)*(sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))))/k3 - sin(k2*s2)*cos(phi2)*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1)) - (cos(phi3)*(cos(k3*s3) - 1)*((k2*sin(k2*s2)*cos(phi2)*cos(phi2)*cos(phi2) + k2*sin(k2*s2)*cos(phi2)*sin(phi2)*sin(phi2))*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1)) - (sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*(k2*sin(k2*s2) - k2*sin(k2*s2)*cos(phi2)*cos(phi2)) + k2*sin(k2*s2)*cos(phi2)*cos(phi2)*sin(phi2)) + sin(k1*s1)*cos(phi1)*(k2*cos(k2*s2)*cos(phi2)*cos(phi2) + k2*cos(k2*s2)*sin(phi2)*sin(phi2))))/k3 + (sin(phi3)*(cos(k3*s3) - 1)*(sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(k2*sin(k2*s2) - k2*sin(k2*s2)*cos(phi2)*cos(phi2)) - k2*sin(k2*s2)*cos(phi2)*sin(phi2)*sin(phi2)))/k3;
	float J2_4 = -(sin(k3*s3)*(k2*cos(k2*s2)*cos(phi2)*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) - k2*sin(k1*s1)*sin(k2*s2)*sin(phi1) + k2*cos(k2*s2)*sin(phi2)*(cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))))/k3 - sin(k2*s2)*cos(phi2)*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) - sin(k2*s2)*sin(phi2)*(cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1)) - cos(k2*s2)*sin(k1*s1)*sin(phi1) - (cos(phi3)*(cos(k3*s3) - 1)*((k2*sin(k2*s2)*cos(phi2)*cos(phi2)*cos(phi2) + k2*sin(k2*s2)*cos(phi2)*sin(phi2)*sin(phi2))*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) + (cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*(k2*sin(k2*s2) - k2*sin(k2*s2)*cos(phi2)*cos(phi2)) + k2*sin(k2*s2)*cos(phi2)*cos(phi2)*sin(phi2)) + sin(k1*s1)*sin(phi1)*(k2*cos(k2*s2)*cos(phi2)*cos(phi2) + k2*cos(k2*s2)*sin(phi2)*sin(phi2))))/k3 - (sin(phi3)*(cos(k3*s3) - 1)*(cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(k2*sin(k2*s2) - k2*sin(k2*s2)*cos(phi2)*cos(phi2)) - k2*sin(k2*s2)*cos(phi2)*sin(phi2)*sin(phi2)))/k3;
	float J3_4 = cos(k1*s1)*cos(k2*s2) - (sin(k3*s3)*(k2*cos(k1*s1)*sin(k2*s2) + k2*cos(k2*s2)*cos(phi2)*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1))))/k3 - sin(k2*s2)*cos(phi2)*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1)) + (cos(phi3)*(cos(k3*s3) - 1)*(cos(k1*s1)*(k2*cos(k2*s2)*cos(phi2)*cos(phi2) + k2*cos(k2*s2)*sin(phi2)*sin(phi2)) - (k2*sin(k2*s2)*cos(phi2)*cos(phi2)*cos(phi2) + k2*sin(k2*s2)*cos(phi2)*sin(phi2)*sin(phi2))*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1))))/k3;

	float J1_5 = (sin(k3*s3)*(s2*sin(k1*s1)*sin(k2*s2)*cos(phi1) - s2*cos(k2*s2)*cos(phi2)*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1)) + s2*cos(k2*s2)*sin(phi2)*(sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))))/k3 + (sin(phi2)*(cos(k2*s2) - 1)*(sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)))/k2*k2 - (cos(phi3)*(cos(k3*s3) - 1)*((s2*sin(k2*s2)*cos(phi2)*cos(phi2)*cos(phi2) + s2*sin(k2*s2)*cos(phi2)*sin(phi2)*sin(phi2))*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1)) - (sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*(s2*sin(k2*s2) - s2*sin(k2*s2)*cos(phi2)*cos(phi2)) + s2*sin(k2*s2)*cos(phi2)*cos(phi2)*sin(phi2)) + sin(k1*s1)*cos(phi1)*(s2*cos(k2*s2)*cos(phi2)*cos(phi2) + s2*cos(k2*s2)*sin(phi2)*sin(phi2))))/k3 + (sin(k1*s1)*sin(k2*s2)*cos(phi1))/k2*k2 - (cos(phi2)*(cos(k2*s2) - 1)*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1)))/k2*k2 - (s2*sin(k2*s2)*cos(phi2)*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1)))/k2 + (s2*sin(k2*s2)*sin(phi2)*(sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)))/k2 - (s2*cos(k2*s2)*sin(k1*s1)*cos(phi1))/k2 + (sin(phi3)*(cos(k3*s3) - 1)*(sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(s2*sin(k2*s2) - s2*sin(k2*s2)*cos(phi2)*cos(phi2)) - s2*sin(k2*s2)*cos(phi2)*sin(phi2)*sin(phi2)))/k3;
	float J2_5 = (sin(k1*s1)*sin(k2*s2)*sin(phi1))/k2*k2 - (sin(k3*s3)*(s2*cos(k2*s2)*cos(phi2)*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) - s2*sin(k1*s1)*sin(k2*s2)*sin(phi1) + s2*cos(k2*s2)*sin(phi2)*(cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))))/k3 - (cos(phi3)*(cos(k3*s3) - 1)*((sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(s2*sin(k2*s2)*cos(phi2)*cos(phi2)*cos(phi2) + s2*sin(k2*s2)*cos(phi2)*sin(phi2)*sin(phi2)) + (cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*(s2*sin(k2*s2) - s2*sin(k2*s2)*cos(phi2)*cos(phi2)) + s2*sin(k2*s2)*cos(phi2)*cos(phi2)*sin(phi2)) + sin(k1*s1)*sin(phi1)*(s2*cos(k2*s2)*cos(phi2)*cos(phi2) + s2*cos(k2*s2)*sin(phi2)*sin(phi2))))/k3 - (cos(phi2)*(cos(k2*s2) - 1)*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)))/k2*k2 - (sin(phi2)*(cos(k2*s2) - 1)*(cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1)))/k2*k2 - (sin(phi3)*(cos(k3*s3) - 1)*(cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(s2*sin(k2*s2) - s2*sin(k2*s2)*cos(phi2)*cos(phi2)) - s2*sin(k2*s2)*cos(phi2)*sin(phi2)*sin(phi2)))/k3 - (s2*sin(k2*s2)*cos(phi2)*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)))/k2 - (s2*sin(k2*s2)*sin(phi2)*(cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1)))/k2 - (s2*cos(k2*s2)*sin(k1*s1)*sin(phi1))/k2;
	float J3_5 = (s2*cos(k1*s1)*cos(k2*s2))/k2 - (sin(k3*s3)*(s2*cos(k1*s1)*sin(k2*s2) + s2*cos(k2*s2)*cos(phi2)*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1))))/k3 - (cos(phi3)*(cos(k3*s3) - 1)*((s2*sin(k2*s2)*cos(phi2)*cos(phi2)*cos(phi2) + s2*sin(k2*s2)*cos(phi2)*sin(phi2)*sin(phi2))*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1)) - cos(k1*s1)*(s2*cos(k2*s2)*cos(phi2)*cos(phi2) + s2*cos(k2*s2)*sin(phi2)*sin(phi2))))/k3 - (cos(k1*s1)*sin(k2*s2))/k2*k2 - (cos(phi2)*(cos(k2*s2) - 1)*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1)))/k2*k2 - (s2*sin(k2*s2)*cos(phi2)*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1)))/k2;

	float J1_6 = (sin(k3*s3)*(sin(k2*s2)*sin(phi2)*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1)) + sin(k2*s2)*cos(phi2)*(sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))))/k3 - (sin(phi2)*(cos(k2*s2) - 1)*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1)))/k2 - (cos(phi2)*(cos(k2*s2) - 1)*(sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)))/k2 - (cos(phi3)*(cos(k3*s3) - 1)*((sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*cos(phi2)*cos(phi2)*(cos(k2*s2) - 1) + cos(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1))) + ((cos(k2*s2) - 1)*sin(phi2)*sin(phi2)*sin(phi2) + ((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)*sin(phi2))*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1))))/k3 - (sin(phi3)*(cos(k3*s3) - 1)*((sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*sin(phi2)*sin(phi2)*(cos(k2*s2) - 1) - sin(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1))) + (cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*cos(phi2)*(cos(k2*s2) - 1))*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1))))/k3;
	float J2_6 = (cos(phi2)*(cos(k2*s2) - 1)*(cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1)))/k2 - (cos(phi3)*(cos(k3*s3) - 1)*(((cos(k2*s2) - 1)*sin(phi2)*sin(phi2)*sin(phi2) + ((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)*sin(phi2))*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) - (cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*cos(phi2)*cos(phi2)*(cos(k2*s2) - 1) + cos(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)))))/k3 - (sin(phi3)*(cos(k3*s3) - 1)*((cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*cos(phi2)*(cos(k2*s2) - 1))*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) - (cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*sin(phi2)*sin(phi2)*(cos(k2*s2) - 1) - sin(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)))))/k3 - (sin(k3*s3)*(sin(k2*s2)*cos(phi2)*(cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1)) - sin(k2*s2)*sin(phi2)*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))))/k3 - (sin(phi2)*(cos(k2*s2) - 1)*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)))/k2;
	float J3_6 = (sin(k2*s2)*sin(k3*s3)*sin(phi2)*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1)))/k3 - (sin(phi3)*(cos(k3*s3) - 1)*(cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*cos(phi2)*(cos(k2*s2) - 1))*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1)))/k3 - (cos(phi3)*(cos(k3*s3) - 1)*((cos(k2*s2) - 1)*sin(phi2)*sin(phi2)*sin(phi2) + ((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)*sin(phi2))*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1)))/k3 - (sin(phi2)*(cos(k2*s2) - 1)*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1)))/k2;

	float J1_7 = sin(k3*s3)*cos(phi3)*((sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) + cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1)) - (cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + sin(k1*s1)*cos(phi1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))) - cos(k3*s3)*(cos(k2*s2)*sin(k1*s1)*cos(phi1) - sin(k2*s2)*sin(phi2)*(sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) + sin(k2*s2)*cos(phi2)*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1))) + sin(k3*s3)*sin(phi3)*((sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) - cos(phi2)*sin(phi2)*sin(phi2)*(cos(k2*s2) - 1)) + (sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1)));
	float J2_7 = sin(k3*s3)*sin(phi3)*((sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) - (cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) - cos(phi2)*sin(phi2)*sin(phi2)*(cos(k2*s2) - 1))) - cos(k3*s3)*(sin(k2*s2)*cos(phi2)*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) + sin(k2*s2)*sin(phi2)*(cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1)) + cos(k2*s2)*sin(k1*s1)*sin(phi1)) - sin(k3*s3)*cos(phi3)*((sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + (cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) + cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1)) - sin(k1*s1)*sin(phi1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2)));
	float J3_7 = cos(k3*s3)*(cos(k1*s1)*cos(k2*s2) - sin(k2*s2)*cos(phi2)*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1))) - sin(k3*s3)*cos(phi3)*((sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + cos(k1*s1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))) + sin(k3*s3)*sin(phi3)*(sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1));

	float J1_8 = (sin(k3*s3)*(cos(k2*s2)*sin(k1*s1)*cos(phi1) - sin(k2*s2)*sin(phi2)*(sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) + sin(k2*s2)*cos(phi2)*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1))))/k3*k3 - (s3*cos(k3*s3)*(cos(k2*s2)*sin(k1*s1)*cos(phi1) - sin(k2*s2)*sin(phi2)*(sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) + sin(k2*s2)*cos(phi2)*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1))))/k3 + (cos(phi3)*(cos(k3*s3) - 1)*((sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) + cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1)) - (cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + sin(k1*s1)*cos(phi1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3*k3 + (sin(phi3)*(cos(k3*s3) - 1)*((sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) - cos(phi2)*sin(phi2)*sin(phi2)*(cos(k2*s2) - 1)) + (sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1))))/k3*k3 + (s3*sin(k3*s3)*sin(phi3)*((sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) - cos(phi2)*sin(phi2)*sin(phi2)*(cos(k2*s2) - 1)) + (sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1))))/k3 + (s3*sin(k3*s3)*cos(phi3)*((sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) + cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1)) - (cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + sin(k1*s1)*cos(phi1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3;
	float J2_8 = (sin(k3*s3)*(sin(k2*s2)*cos(phi2)*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) + sin(k2*s2)*sin(phi2)*(cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1)) + cos(k2*s2)*sin(k1*s1)*sin(phi1)))/k3*k3 + (sin(phi3)*(cos(k3*s3) - 1)*((sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) - (cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) - cos(phi2)*sin(phi2)*sin(phi2)*(cos(k2*s2) - 1))))/k3*k3 - (s3*cos(k3*s3)*(sin(k2*s2)*cos(phi2)*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) + sin(k2*s2)*sin(phi2)*(cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1)) + cos(k2*s2)*sin(k1*s1)*sin(phi1)))/k3 - (cos(phi3)*(cos(k3*s3) - 1)*((sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + (cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) + cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1)) - sin(k1*s1)*sin(phi1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3*k3 - (s3*sin(k3*s3)*cos(phi3)*((sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + (cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) + cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1)) - sin(k1*s1)*sin(phi1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3 + (s3*sin(k3*s3)*sin(phi3)*((sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) - (cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) - cos(phi2)*sin(phi2)*sin(phi2)*(cos(k2*s2) - 1))))/k3;
	float J3_8 = (s3*cos(k3*s3)*(cos(k1*s1)*cos(k2*s2) - sin(k2*s2)*cos(phi2)*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1))))/k3 - (sin(k3*s3)*(cos(k1*s1)*cos(k2*s2) - sin(k2*s2)*cos(phi2)*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1))))/k3*k3 - (cos(phi3)*(cos(k3*s3) - 1)*((sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + cos(k1*s1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3*k3 + (sin(phi3)*(cos(k3*s3) - 1)*(sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1)))/k3*k3 - (s3*sin(k3*s3)*cos(phi3)*((sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + cos(k1*s1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3 + (s3*sin(k3*s3)*sin(phi3)*(sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1)))/k3;

	float J1_9 = (sin(phi3)*(cos(k3*s3) - 1)*((sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) + cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1)) - (cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + sin(k1*s1)*cos(phi1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3 - (cos(phi3)*(cos(k3*s3) - 1)*((sin(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1) - cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) - cos(phi2)*sin(phi2)*sin(phi2)*(cos(k2*s2) - 1)) + (sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(cos(phi1)*(cos(k1*s1) - 1)*sin(phi1)*sin(phi1) + cos(phi1)*((cos(k1*s1) - 1)*cos(phi1)*cos(phi1) + 1))))/k3;
	float J2_9 = -(sin(phi3)*(cos(k3*s3) - 1)*((sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + (cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(sin(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) + cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1)) - sin(k1*s1)*sin(phi1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3 - (cos(phi3)*(cos(k3*s3) - 1)*((sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(sin(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) + cos(phi1)*cos(phi1)*sin(phi1)*(cos(k1*s1) - 1)) - (cos(phi1)*(cos(k1*s1) - cos(phi1)*cos(phi1)*(cos(k1*s1) - 1)) - cos(phi1)*sin(phi1)*sin(phi1)*(cos(k1*s1) - 1))*(cos(phi2)*(cos(k2*s2) - cos(phi2)*cos(phi2)*(cos(k2*s2) - 1)) - cos(phi2)*sin(phi2)*sin(phi2)*(cos(k2*s2) - 1))))/k3;
	float J3_9 = -(sin(phi3)*(cos(k3*s3) - 1)*((sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1))*(cos(phi2)*(cos(k2*s2) - 1)*sin(phi2)*sin(phi2) + cos(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1)) + cos(k1*s1)*(sin(k2*s2)*cos(phi2)*cos(phi2) + sin(k2*s2)*sin(phi2)*sin(phi2))))/k3 - (cos(phi3)*(cos(k3*s3) - 1)*(sin(phi2)*((cos(k2*s2) - 1)*cos(phi2)*cos(phi2) + 1) - cos(phi2)*cos(phi2)*sin(phi2)*(cos(k2*s2) - 1))*(sin(k1*s1)*cos(phi1)*cos(phi1) + sin(k1*s1)*sin(phi1)*sin(phi1)))/k3;

	float Jacobian[] = { J1_1, J2_1, J3_1,
						 J1_2, J2_2, J3_2,
						 J1_3, J2_3, J3_3,
						 J1_4, J2_4, J3_4,
						 J1_5, J2_5, J3_5,
						 J1_6, J2_6, J3_6,
						 J1_7, J2_7, J3_7,
						 J1_8, J2_8, J3_8,
						 J1_9, J2_9, J3_9};
	float* JacobianResult = new float [27];
	for(int i= 0; i<27;++i)
	{
			JacobianResult[i] = Jacobian[i];
	}
	return JacobianResult;
}

vector<float> ManipulatorNSections:: ComputeJacobian_whole_mobile(float* Config, float* base_pos) // Returns a column major Jacobian
{
	float s1 = Config[0]; float k1 = Config[1]; float phi1 = Config[2];
	float s2 = Config[3]; float k2 = Config[4]; float phi2 = Config[5];
	float s3 = Config[6]; float k3 = Config[7]; float phi3 = Config[8];
	float xb = base_pos[0];
	float yb = base_pos[1];

	float Posx_wrt_xb = 1.0;
	float Posx_wrt_yb = 0.0;
	float Posy_wrt_xb = 0.0;
	float Posy_wrt_yb = 1.0;
	float Posz_wrt_xb = 0.0;
	float Posz_wrt_yb = 0.0;

	float Posx_wrt_s1 = (cos(phi1)*(-(pow(k1,2)*cos(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3))) - k1*sin(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.0),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3)))))/(k1*k2*k3);

	float Posx_wrt_k1 = -((cos(phi1)*(-(k2*k3) + cos(k1*s1)*(pow(k1,2)*s1*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*(k3 + pow(k1,2)*s1*cos(k2*s2)*sin(k3*s3))) + k1*s1*sin(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3)))))/(pow(k1,2)*k2*k3));

	float Posx_wrt_phi1 = (k1*cos(phi1)*(-(k2*cos(phi2)*(-1 + cos(k3*s3))*sin(phi3)) + sin(phi2)*(k3 - cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) + k2*sin(k2*s2)*sin(k3*s3))) - sin(phi1)*(-(k2*k3) - k1*sin(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3)) + cos(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3)))))/(k1*k2*k3);

	float Posx_wrt_s2 = (k1*sin(phi1)*sin(phi2)*(k2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + pow(k2,2)*cos(k2*s2)*sin(k3*s3)) + cos(phi1)*(k1*cos(phi2)*cos(k1*s1)*(-(k2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2)) - pow(k2,2)*cos(k2*s2)*sin(k3*s3)) - k1*sin(k1*s1)*(k2*cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - pow(k2,2)*sin(k2*s2)*sin(k3*s3))))/(k1*k2*k3);

	float Posx_wrt_k2 = (sin(phi1)*sin(phi2)*(-k3 + k2*s2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + cos(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3))) - cos(phi1)*(cos(phi2)*cos(k1*s1)*(-k3 + k2*s2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + cos(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3))) + sin(k1*s1)*(k2*s2*cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - sin(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3)))))/(pow(k2,2)*k3);

	float Posx_wrt_phi2 = (cos(phi1)*cos(k1*s1)*(2*k1*k2*cos(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2) - k1*sin(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))) + k1*sin(phi1)*(k2*(-1 + cos(k3*s3))*sin(phi2)*sin(phi3) + cos(phi2)*(k3 - cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) + k2*sin(k2*s2)*sin(k3*s3))))/(k1*k2*k3);

	float Posx_wrt_s3  = (k1*sin(phi1)*(k2*k3*cos(phi2)*sin(phi3)*sin(k3*s3) + sin(phi2)*(k2*k3*cos(k3*s3)*sin(k2*s2) + k2*k3*cos(phi3)*cos(k2*s2)*sin(k3*s3))) +
     cos(phi1)*(-(k1*sin(k1*s1)*(k2*k3*cos(k2*s2)*cos(k3*s3) - k2*k3*cos(phi3)*sin(k2*s2)*sin(k3*s3))) + cos(k1*s1)*(2*k1*k2*k3*cos((k3*s3)/2.)*sin(phi2)*sin(phi3)*sin((k3*s3)/2.) + k1*cos(phi2)*(-(k2*k3*cos(k3*s3)*sin(k2*s2)) - k2*k3*cos(phi3)*cos(k2*s2)*sin(k3*s3)))))/(k1*k2*k3);

	float Posx_wrt_k3 = (sin(phi1)*(sin(phi2)*sin(k2*s2)*(k3*s3*cos(k3*s3) - sin(k3*s3)) + cos(phi3)*cos(k2*s2)*sin(phi2)*(-1 + cos(k3*s3) + k3*s3*sin(k3*s3)) + cos(phi2)*sin(phi3)*(-1 + cos(k3*s3) + k3*s3*sin(k3*s3))) + cos(phi1)* (-(cos(phi3)*sin(k1*s1)*sin(k2*s2)) + cos(phi3)*cos(k3*s3)*sin(k1*s1)*sin(k2*s2) - 2*cos(k1*s1)*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2) - cos(k2*s2)*sin(k1*s1)*(k3*s3*cos(k3*s3) - sin(k3*s3)) + k3*s3*cos(k1*s1)*sin(phi2)*sin(phi3)*sin(k3*s3) + k3*s3*cos(phi3)*sin(k1*s1)*sin(k2*s2)*sin(k3*s3) - cos(phi2)*cos(k1*s1)*(sin(k2*s2)*(k3*s3*cos(k3*s3) - sin(k3*s3)) + cos(phi3)*cos(k2*s2)*(-1 + cos(k3*s3) + k3*s3*sin(k3*s3)))))/pow(k3,2);

	float Posx_wrt_phi3 = (k1*sin(phi1)*(-(k2*cos(phi2)*cos(phi3)*(-1 + cos(k3*s3))) + k2*cos(k2*s2)*(-1 + cos(k3*s3))*sin(phi2)*sin(phi3)) + cos(phi1)*(k1*k2*(-1 + cos(k3*s3))*sin(phi3)*sin(k1*s1)*sin(k2*s2) + cos(k1*s1)* (-(k1*k2*cos(phi2)*cos(k2*s2)*(-1 + cos(k3*s3))*sin(phi3)) + 2*k1*k2*cos(phi3)*sin(phi2)*pow(sin((k3*s3)/2.),2))))/(k1*k2*k3);

	float Posy_wrt_s1 = (sin(phi1)*(-(pow(k1,2)*cos(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3))) - k1*sin(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3)))))/(k1*k2*k3);

	float Posy_wrt_k1 = -((sin(phi1)*(-(k2*k3) + cos(k1*s1)*(pow(k1,2)*s1*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*(k3 + pow(k1,2)*s1*cos(k2*s2)*sin(k3*s3))) + k1*s1*sin(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3)))))/(pow(k1,2)*k2*k3));

	float Posy_wrt_phi1 = (-(k1*sin(phi1)*(k2*cos(phi2)*(-1 + cos(k3*s3))*sin(phi3) + sin(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3)))) + cos(phi1)*(-(k2*k3) - k1*sin(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3)) + cos(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3)))))/(k1*k2*k3);

	float Posy_wrt_s2 = (k1*cos(phi1)*sin(phi2)*(-(k2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2)) - pow(k2,2)*cos(k2*s2)*sin(k3*s3)) + sin(phi1)*(k1*cos(phi2)*cos(k1*s1)*(-(k2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2)) - pow(k2,2)*cos(k2*s2)*sin(k3*s3)) - k1*sin(k1*s1)*(k2*cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - pow(k2,2)*sin(k2*s2)*sin(k3*s3))))/(k1*k2*k3);

	float Posy_wrt_k2 = (-(cos(phi2)*cos(k1*s1)*sin(phi1)*(-k3 + k2*s2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + cos(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3)))) - cos(phi1)*sin(phi2)*(-k3 + k2*s2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + cos(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3))) + sin(phi1)*sin(k1*s1)*(-(k2*s2*cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))) + sin(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3))))/(pow(k2,2)*k3);

	float Posy_wrt_phi2 = (k1*cos(phi1)*(-(k2*(-1 + cos(k3*s3))*sin(phi2)*sin(phi3)) + cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))) + cos(k1*s1)*sin(phi1)*(2*k1*k2*cos(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2) - k1*sin(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))))/(k1*k2*k3);

	float Posy_wrt_s3 = (k1*cos(phi1)*(-(k2*k3*cos(phi2)*sin(phi3)*sin(k3*s3)) + sin(phi2)*(-(k2*k3*cos(k3*s3)*sin(k2*s2)) - k2*k3*cos(phi3)*cos(k2*s2)*sin(k3*s3))) + sin(phi1)*(-(k1*sin(k1*s1)*(k2*k3*cos(k2*s2)*cos(k3*s3) - k2*k3*cos(phi3)*sin(k2*s2)*sin(k3*s3))) + cos(k1*s1)*(2*k1*k2*k3*cos((k3*s3)/2.)*sin(phi2)*sin(phi3)*sin((k3*s3)/2.) + k1*cos(phi2)*(-(k2*k3*cos(k3*s3)*sin(k2*s2)) - k2*k3*cos(phi3)*cos(k2*s2)*sin(k3*s3)))))/(k1*k2*k3);

	float Posy_wrt_k3 = (-(cos(phi1)*(sin(phi2)*sin(k2*s2)*(k3*s3*cos(k3*s3) - sin(k3*s3)) + cos(phi3)*cos(k2*s2)*sin(phi2)*(-1 + cos(k3*s3) + k3*s3*sin(k3*s3)) + cos(phi2)*sin(phi3)*(-1 + cos(k3*s3) + k3*s3*sin(k3*s3)))) + sin(phi1)* (-(cos(phi2)*cos(k1*s1)*sin(k2*s2)*(k3*s3*cos(k3*s3) - sin(k3*s3))) - (cos(k1*s1)*sin(phi2)*sin(phi3) + cos(phi3)*sin(k1*s1)*sin(k2*s2))*(2*pow(sin((k3*s3)/2.),2) - k3*s3*sin(k3*s3)) + cos(k2*s2)*(-(k3*s3*cos(k3*s3)*sin(k1*s1)) + sin(k1*s1)*sin(k3*s3) + cos(phi2)*cos(phi3)*cos(k1*s1)*(2*pow(sin((k3*s3)/2.),2) - k3*s3*sin(k3*s3)))))/pow(k3,2);

	float Posy_wrt_phi3 = (k1*cos(phi1)*(k2*cos(phi2)*cos(phi3)*(-1 + cos(k3*s3)) - k2*cos(k2*s2)*(-1 + cos(k3*s3))*sin(phi2)*sin(phi3)) + sin(phi1)*(k1*k2*(-1 + cos(k3*s3))*sin(phi3)*sin(k1*s1)*sin(k2*s2) + cos(k1*s1)* (-(k1*k2*cos(phi2)*cos(k2*s2)*(-1 + cos(k3*s3))*sin(phi3)) + 2*k1*k2*cos(phi3)*sin(phi2)*pow(sin((k3*s3)/2.),2))))/(k1*k2*k3);

	float Posz_wrt_s1 = (-(pow(k1,2)*sin(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3))) + k1*cos(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))))/(k1*k2*k3);

	float Posz_wrt_k1 = (cos(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3)) - k1*s1*sin(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(2*k2*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2) + cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))) + s1*cos(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))))/(k1*k2*k3) - (k1*cos(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))))/(pow(k1,2)*k2*k3);

	float Posz_wrt_phi1  = 0.0;

	float Posz_wrt_s2 = (k1*cos(phi2)*sin(k1*s1)*(-(k2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2)) - pow(k2,2)*cos(k2*s2)*sin(k3*s3)) + k1*cos(k1*s1)*(k2*cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - pow(k2,2)*sin(k2*s2)*sin(k3*s3)))/(k1*k2*k3);

	float Posz_wrt_k2 = (-(cos(phi2)*sin(k1*s1)*(-k3 + k2*s2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + cos(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3)))) + cos(k1*s1)*(k2*s2*cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - sin(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3))))/(pow(k2,2)*k3);

	float Posz_wrt_phi2 = (sin(k1*s1)*(2*k1*k2*cos(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2) - k1*sin(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))))/(k1*k2*k3);

	float Posz_wrt_s3 = (k1*cos(k1*s1)*(k2*k3*cos(k2*s2)*cos(k3*s3) - k2*k3*cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(2*k1*k2*k3*cos((k3*s3)/2.)*sin(phi2)*sin(phi3)*sin((k3*s3)/2.) + k1*cos(phi2)*(-(k2*k3*cos(k3*s3)*sin(k2*s2)) - k2*k3*cos(phi3)*cos(k2*s2)*sin(k3*s3))))/(k1*k2*k3);

	float Posz_wrt_k3 = (cos(k1*s1)*(cos(k2*s2)*(k3*s3*cos(k3*s3) - sin(k3*s3)) - cos(phi3)*sin(k2*s2)*(-1 + cos(k3*s3) + k3*s3*sin(k3*s3))) + sin(k1*s1)*(sin(phi2)*sin(phi3)*(-2*pow(sin((k3*s3)/2.),2) + k3*s3*sin(k3*s3)) - cos(phi2)*(sin(k2*s2)*(k3*s3*cos(k3*s3) - sin(k3*s3)) + cos(phi3)*cos(k2*s2)*(-1 + cos(k3*s3) + k3*s3*sin(k3*s3)))))/pow(k3,2);

	float Posz_wrt_phi3 = (-(k1*k2*cos(k1*s1)*(-1 + cos(k3*s3))*sin(phi3)*sin(k2*s2)) + sin(k1*s1)*(-(k1*k2*cos(phi2)*cos(k2*s2)*(-1 + cos(k3*s3))*sin(phi3)) + 2*k1*k2*cos(phi3)*sin(phi2)*pow(sin((k3*s3)/2.),2)))/(k1*k2*k3);
 /////////////////////orientation part of the Jacobian///////////////////////////////////

	float Orix_wrt_s1 = (2*k1*(cos(phi3)*cos(k2*s2)*cos(k3*s3)*sin(phi2) + cos(phi2)*cos(k3*s3)*sin(phi3) - sin(phi2)*sin(k2*s2)*sin(k3*s3)))/(2*pow(cos(phi2),2)*pow(cos(k2*s2),2)*pow(sin(phi3),2)*pow(sin(k1*s1),2) + cos(k2*s2)*sin(2*phi2)*sin(2*phi3)*pow(sin(k1*s1),2) + sin(phi2)*sin(2*phi3)*sin(2*k1*s1)*sin(k2*s2) + 2*pow(cos(phi2),2)*pow(cos(k3*s3),2)*pow(sin(k1*s1),2)*pow(sin(k2*s2),2) - cos(phi2)*pow(cos(k3*s3),2)*sin(2*k1*s1)*sin(2*k2*s2) + cos(phi2)*pow(sin(phi3),2)*sin(2*k1*s1)*sin(2*k2*s2) + 2*pow(sin(phi2),2)*pow(sin(phi3),2)*pow(sin(k1*s1),2)*pow(sin(k3*s3),2) - sin(phi2)*sin(2*phi3)*sin(2*k1*s1)*sin(k2*s2)*pow(sin(k3*s3),2) + pow(cos(phi3),2)* (2*pow(sin(phi2),2)*pow(sin(k1*s1),2) + cos(phi2)*(2*cos(phi2)*pow(cos(k2*s2),2)*pow(sin(k1*s1),2) + sin(2*k1*s1)*sin(2*k2*s2))*pow(sin(k3*s3),2)) + cos(k2*s2)*sin(phi2)*sin(phi3)*sin(2*k1*s1)*sin(2*k3*s3) - sin(2*phi2)*sin(phi3)*pow(sin(k1*s1),2)*sin(k2*s2)*sin(2*k3*s3) + pow(cos(k1*s1),2)*(2*pow(cos(k2*s2),2)*pow(cos(k3*s3),2) + 2*pow(sin(phi3),2)*pow(sin(k2*s2),2) + 2*pow(cos(phi3),2)*pow(sin(k2*s2),2)*pow(sin(k3*s3),2) - cos(phi3)*sin(2*k2*s2)*sin(2*k3*s3)) + cos(phi2)*cos(phi3)*(-4*cos(k2*s2)*sin(phi2)*sin(phi3)*pow(sin(k1*s1),2)*pow(sin(k3*s3),2) - pow(cos(k2*s2),2)*sin(2*k1*s1)*sin(2*k3*s3) + (sin(2*k1*s1)*pow(sin(k2*s2),2) + cos(phi2)*pow(sin(k1*s1),2)*sin(2*k2*s2))*sin(2*k3*s3)));
	float Orix_wrt_k1 = (2*s1*(cos(phi3)*cos(k2*s2)*cos(k3*s3)*sin(phi2) + cos(phi2)*cos(k3*s3)*sin(phi3) - sin(phi2)*sin(k2*s2)*sin(k3*s3)))/(2*pow(cos(phi2),2)*pow(cos(k2*s2),2)*pow(sin(phi3),2)*pow(sin(k1*s1),2) + cos(k2*s2)*sin(2*phi2)*sin(2*phi3)*pow(sin(k1*s1),2) + sin(phi2)*sin(2*phi3)*sin(2*k1*s1)*sin(k2*s2) + 2*pow(cos(phi2),2)*pow(cos(k3*s3),2)*pow(sin(k1*s1),2)*pow(sin(k2*s2),2) - cos(phi2)*pow(cos(k3*s3),2)*sin(2*k1*s1)*sin(2*k2*s2) + cos(phi2)*pow(sin(phi3),2)*sin(2*k1*s1)*sin(2*k2*s2) + 2*pow(sin(phi2),2)*pow(sin(phi3),2)*pow(sin(k1*s1),2)*pow(sin(k3*s3),2) - sin(phi2)*sin(2*phi3)*sin(2*k1*s1)*sin(k2*s2)*pow(sin(k3*s3),2) + pow(cos(phi3),2)*(2*pow(sin(phi2),2)*pow(sin(k1*s1),2) + cos(phi2)*(2*cos(phi2)*pow(cos(k2*s2),2)*pow(sin(k1*s1),2) + sin(2*k1*s1)*sin(2*k2*s2))*pow(sin(k3*s3),2)) + cos(k2*s2)*sin(phi2)*sin(phi3)*sin(2*k1*s1)*sin(2*k3*s3) - sin(2*phi2)*sin(phi3)*pow(sin(k1*s1),2)*sin(k2*s2)*sin(2*k3*s3) + pow(cos(k1*s1),2)*(2*pow(cos(k2*s2),2)*pow(cos(k3*s3),2) + 2*pow(sin(phi3),2)*pow(sin(k2*s2),2) + 2*pow(cos(phi3),2)*pow(sin(k2*s2),2)*pow(sin(k3*s3),2) - cos(phi3)*sin(2*k2*s2)*sin(2*k3*s3)) + cos(phi2)*cos(phi3)*(-4*cos(k2*s2)*sin(phi2)*sin(phi3)*pow(sin(k1*s1),2)*pow(sin(k3*s3),2) - pow(cos(k2*s2),2)*sin(2*k1*s1)*sin(2*k3*s3) + (sin(2*k1*s1)*pow(sin(k2*s2),2) + cos(phi2)*pow(sin(k1*s1),2)*sin(2*k2*s2))*sin(2*k3*s3)));
	float Orix_wrt_phi1 = 0.0;

	float Orix_wrt_s2 = ((-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)))*(cos(k1*s1)*(-(k2*cos(k3*s3)*sin(k2*s2)) - k2*cos(phi3)*cos(k2*s2)*sin(k3*s3)) - cos(phi2)*sin(k1*s1)*(k2*cos(k2*s2)*cos(k3*s3) - k2*cos(phi3)*sin(k2*s2)*sin(k3*s3))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2)) - (sin(phi3)*(k2*cos(k1*s1)*cos(k2*s2) - k2*cos(phi2)*sin(k1*s1)*sin(k2*s2))*(-(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3))) - sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3)))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2));
	float Orix_wrt_k2  = ((-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)))*(cos(k1*s1)*(-(s2*cos(k3*s3)*sin(k2*s2)) - s2*cos(phi3)*cos(k2*s2)*sin(k3*s3)) - cos(phi2)*sin(k1*s1)*(s2*cos(k2*s2)*cos(k3*s3) - s2*cos(phi3)*sin(k2*s2)*sin(k3*s3))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2)) - (sin(phi3)*(s2*cos(k1*s1)*cos(k2*s2) - s2*cos(phi2)*sin(k1*s1)*sin(k2*s2))*(-(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3))) - sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3)))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2));
	float Orix_wrt_phi2 = (sin(k1*s1)*(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)))*(cos(phi2)*sin(phi3)*sin(k3*s3) + sin(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2)) + ((-(cos(phi2)*cos(phi3)*sin(k1*s1)) + cos(k2*s2)*sin(phi2)*sin(phi3)*sin(k1*s1))*(-(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3))) - sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3)))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2));

	float Orix_wrt_s3 = ((-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)))*(cos(k1*s1)*(-(k3*cos(phi3)*cos(k3*s3)*sin(k2*s2)) - k3*cos(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(k3*cos(k3*s3)*sin(phi2)*sin(phi3) - cos(phi2)*(k3*cos(phi3)*cos(k2*s2)*cos(k3*s3) - k3*sin(k2*s2)*sin(k3*s3)))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2));
	float Orix_wrt_k3 = ((-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)))*(cos(k1*s1)*(-(s3*cos(phi3)*cos(k3*s3)*sin(k2*s2)) - s3*cos(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(s3*cos(k3*s3)*sin(phi2)*sin(phi3) - cos(phi2)*(s3*cos(phi3)*cos(k2*s2)*cos(k3*s3) - s3*sin(k2*s2)*sin(k3*s3)))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2));
	float Orix_wrt_phi3 = ((-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)))*(cos(k1*s1)*sin(phi3)*sin(k2*s2)*sin(k3*s3) + sin(k1*s1)*(cos(phi3)*sin(phi2)*sin(k3*s3) + cos(phi2)*cos(k2*s2)*sin(phi3)*sin(k3*s3))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2)) + ((sin(phi2)*sin(phi3)*sin(k1*s1) - cos(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)))*(-(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3))) - sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3)))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2));

	/////////////////the partial Derivative wrt the joint configurations////////////////////
	float Oriz_wrt_s1 = ((cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(k1*cos(phi1)*cos(k3*s3)*sin(phi2)*sin(phi3)*sin(k1*s1) - cos(phi1)*cos(phi3)*cos(k3*s3)*(k1*cos(phi2)*cos(k2*s2)*sin(k1*s1) + k1*cos(k1*s1)*sin(k2*s2)) - cos(phi1)*(k1*cos(k1*s1)*cos(k2*s2) - k1*cos(phi2)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((k1*cos(k3*s3)*sin(phi1)*sin(phi2)*sin(phi3)*sin(k1*s1) + cos(phi3)*cos(k3*s3)*(-(k1*cos(phi2)*cos(k2*s2)*sin(phi1)*sin(k1*s1)) - k1*cos(k1*s1)*sin(phi1)*sin(k2*s2)) - (k1*cos(k1*s1)*cos(k2*s2)*sin(phi1) - k1*cos(phi2)*sin(phi1)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_k1 = ((cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(s1*cos(phi1)*cos(k3*s3)*sin(phi2)*sin(phi3)*sin(k1*s1) - cos(phi1)*cos(phi3)*cos(k3*s3)*(s1*cos(phi2)*cos(k2*s2)*sin(k1*s1) + s1*cos(k1*s1)*sin(k2*s2)) - cos(phi1)*(s1*cos(k1*s1)*cos(k2*s2) - s1*cos(phi2)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((s1*cos(k3*s3)*sin(phi1)*sin(phi2)*sin(phi3)*sin(k1*s1) + cos(phi3)*cos(k3*s3)*(-(s1*cos(phi2)*cos(k2*s2)*sin(phi1)*sin(k1*s1)) - s1*cos(k1*s1)*sin(phi1)*sin(k2*s2)) - (s1*cos(k1*s1)*cos(k2*s2)*sin(phi1) - s1*cos(phi2)*sin(phi1)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_phi1 =  ((cos(k3*s3)*(-(cos(phi2)*sin(phi1)) - cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi1)*cos(phi2)*cos(k1*s1)*cos(k2*s2) - cos(k2*s2)*sin(phi1)*sin(phi2) - cos(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(phi1)*cos(k2*s2)*sin(k1*s1) + (cos(phi1)*cos(phi2)*cos(k1*s1) - sin(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(-(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (cos(phi1)*sin(phi2)*sin(k2*s2) + sin(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_s2 = ((cos(phi3)*cos(k3*s3)*(-(k2*cos(k2*s2)*sin(phi1)*sin(k1*s1)) - k2*cos(phi2)*cos(k1*s1)*sin(phi1)*sin(k2*s2) - k2*cos(phi1)*sin(phi2)*sin(k2*s2)) - (k2*cos(k2*s2)*(cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2)) - k2*sin(phi1)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(-(cos(phi3)*cos(k3*s3)*(-(k2*sin(phi1)*sin(phi2)*sin(k2*s2)) + cos(phi1)*(k2*cos(k2*s2)*sin(k1*s1) + k2*cos(phi2)*cos(k1*s1)*sin(k2*s2)))) + (k2*cos(k2*s2)*sin(phi1)*sin(phi2) - cos(phi1)*(k2*cos(phi2)*cos(k1*s1)*cos(k2*s2) - k2*sin(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_k2 = ((cos(phi3)*cos(k3*s3)*(-(s2*cos(k2*s2)*sin(phi1)*sin(k1*s1)) - s2*cos(phi2)*cos(k1*s1)*sin(phi1)*sin(k2*s2) - s2*cos(phi1)*sin(phi2)*sin(k2*s2)) - (s2*cos(k2*s2)*(cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2)) - s2*sin(phi1)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(-(cos(phi3)*cos(k3*s3)*(-(s2*sin(phi1)*sin(phi2)*sin(k2*s2)) + cos(phi1)*(s2*cos(k2*s2)*sin(k1*s1) + s2*cos(phi2)*cos(k1*s1)*sin(k2*s2)))) + (s2*cos(k2*s2)*sin(phi1)*sin(phi2) - cos(phi1)*(s2*cos(phi2)*cos(k1*s1)*cos(k2*s2) - s2*sin(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_phi2 = ((-(cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k1*s1)*cos(k2*s2)*sin(phi2))) - cos(k3*s3)*(cos(phi1)*cos(phi2)*cos(k1*s1) - sin(phi1)*sin(phi2))*sin(phi3) + (cos(phi2)*sin(phi1)*sin(k2*s2) + cos(phi1)*cos(k1*s1)*sin(phi2)*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((cos(phi3)*cos(k3*s3)*(cos(phi1)*cos(phi2)*cos(k2*s2) - cos(k1*s1)*cos(k2*s2)*sin(phi1)*sin(phi2)) + cos(k3*s3)*(-(cos(phi2)*cos(k1*s1)*sin(phi1)) - cos(phi1)*sin(phi2))*sin(phi3) - (cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(k2*s2)*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_s3 =  ((-(k3*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))) - k3*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3)*sin(k3*s3) - k3*cos(phi3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(k3*cos(k3*s3)*(sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2))) + k3*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)*sin(k3*s3) + k3*cos(phi3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_k3 = ((-(s3*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))) - s3*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3)*sin(k3*s3) - s3*cos(phi3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(s3*cos(k3*s3)*(sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2))) + s3*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)*sin(k3*s3) + s3*cos(phi3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_phi3 = ((-(cos(phi3)*cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))) + cos(k3*s3)*sin(phi3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))))*(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((cos(phi3)*cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2)) - cos(k3*s3)*sin(phi3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriy_wrt_s1 = -((cos(k3*s3)*(-(k1*cos(k1*s1)*sin(phi2)*sin(phi3)) - k1*cos(phi3)*sin(k1*s1)*sin(k2*s2)) - k1*cos(k2*s2)*sin(k1*s1)*sin(k3*s3) + k1*cos(phi2)*cos(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));

	float Oriy_wrt_k1 = -((cos(k3*s3)*(-(s1*cos(k1*s1)*sin(phi2)*sin(phi3)) - s1*cos(phi3)*sin(k1*s1)*sin(k2*s2)) - s1*cos(k2*s2)*sin(k1*s1)*sin(k3*s3) + s1*cos(phi2)*cos(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));

	float Oriy_wrt_phi1 = 0.0;

	float Oriy_wrt_s2 = -((k2*cos(phi3)*cos(k1*s1)*cos(k2*s2)*cos(k3*s3) - k2*cos(k1*s1)*sin(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(-(k2*cos(phi3)*cos(k3*s3)*sin(k2*s2)) - k2*cos(k2*s2)*sin(k3*s3)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3)+ cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));

	float Oriy_wrt_k2 = -((s2*cos(phi3)*cos(k1*s1)*cos(k2*s2)*cos(k3*s3) - s2*cos(k1*s1)*sin(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(-(s2*cos(phi3)*cos(k3*s3)*sin(k2*s2)) - s2*cos(k2*s2)*sin(k3*s3)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));

	float Oriy_wrt_phi2 = -((-(cos(phi2)*cos(k3*s3)*sin(phi3)*sin(k1*s1)) - sin(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));

	float Oriy_wrt_s3 = -((k3*cos(k1*s1)*cos(k2*s2)*cos(k3*s3) - k3*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2))*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(-(k3*cos(k3*s3)*sin(k2*s2)) - k3*cos(phi3)*cos(k2*s2)*sin(k3*s3)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));

	float Oriy_wrt_k3 = -((s3*cos(k1*s1)*cos(k2*s2)*cos(k3*s3) - s3*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2))*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(-(s3*cos(k3*s3)*sin(k2*s2)) - s3*cos(phi3)*cos(k2*s2)*sin(k3*s3)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));

	float Oriy_wrt_phi3 = -((-(cos(phi2)*cos(k2*s2)*cos(k3*s3)*sin(phi3)*sin(k1*s1)) + cos(k3*s3)*(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - cos(k1*s1)*sin(phi3)*sin(k2*s2)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));



	float Orix_wrt_xb = 0.0;
	float Orix_wrt_yb = 0.0;
	float Oriy_wrt_xb = 0.0;
	float Oriy_wrt_yb = 0.0;
	float Oriz_wrt_xb = 0.0;
	float Oriz_wrt_yb = 0.0;
	//column major Jacobian
	float Jacobian[] = { Posx_wrt_s1, Posy_wrt_s1, Posz_wrt_s1,Orix_wrt_s1,Oriy_wrt_s1,Oriz_wrt_s1,
						 Posx_wrt_k1, Posy_wrt_k1, Posz_wrt_k1,Orix_wrt_k1,Oriy_wrt_k1,Oriz_wrt_k1,
						 Posx_wrt_phi1, Posy_wrt_phi1, Posz_wrt_phi1,Orix_wrt_phi1,Oriy_wrt_phi1,Oriz_wrt_phi1,
						 Posx_wrt_s2, Posy_wrt_s2, Posz_wrt_s2,Orix_wrt_s2,Oriy_wrt_s2,Oriz_wrt_s2,
						 Posx_wrt_k2, Posy_wrt_k2, Posz_wrt_k2,Orix_wrt_k2,Oriy_wrt_k2,Oriz_wrt_k2,
						 Posx_wrt_phi2, Posy_wrt_phi2, Posz_wrt_phi2,Orix_wrt_phi2,Oriy_wrt_phi2,Oriz_wrt_phi2,
						 Posx_wrt_s3, Posy_wrt_s3, Posz_wrt_s3,Orix_wrt_s3,Oriy_wrt_s3,Oriz_wrt_s3,
						 Posx_wrt_k3, Posy_wrt_k3, Posz_wrt_k3,Orix_wrt_k3,Oriy_wrt_k3,Oriz_wrt_k3,
						 Posx_wrt_phi3, Posy_wrt_phi3, Posz_wrt_phi3,Orix_wrt_phi3,Oriy_wrt_phi3,Oriz_wrt_phi3,
						 Posx_wrt_xb, Posy_wrt_xb, Posz_wrt_xb, Orix_wrt_xb, Oriy_wrt_xb, Oriz_wrt_xb,
	                     Posx_wrt_yb, Posy_wrt_yb, Posz_wrt_yb, Orix_wrt_yb, Oriy_wrt_yb, Oriz_wrt_yb};

	vector<float>JacobianResult(66);
	for(int i= 0; i<66;++i)
	{
		JacobianResult[i] = Jacobian[i];
	}
	return JacobianResult;
}

vector<float> ManipulatorNSections:: ComputeJacobian_whole_non_mobile(float* Config) // Returns a column major Jacobian
{
	float s1 = Config[0]; float k1 = Config[1]; float phi1 = Config[2];
	float s2 = Config[3]; float k2 = Config[4]; float phi2 = Config[5];
	float s3 = Config[6]; float k3 = Config[7]; float phi3 = Config[8];


	float Posx_wrt_s1 = (cos(phi1)*(-(pow(k1,2)*cos(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3))) - k1*sin(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.0),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3)))))/(k1*k2*k3);

	float Posx_wrt_k1 = -((cos(phi1)*(-(k2*k3) + cos(k1*s1)*(pow(k1,2)*s1*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*(k3 + pow(k1,2)*s1*cos(k2*s2)*sin(k3*s3))) + k1*s1*sin(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3)))))/(pow(k1,2)*k2*k3));

	float Posx_wrt_phi1 = (k1*cos(phi1)*(-(k2*cos(phi2)*(-1 + cos(k3*s3))*sin(phi3)) + sin(phi2)*(k3 - cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) + k2*sin(k2*s2)*sin(k3*s3))) - sin(phi1)*(-(k2*k3) - k1*sin(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3)) + cos(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3)))))/(k1*k2*k3);

	float Posx_wrt_s2 = (k1*sin(phi1)*sin(phi2)*(k2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + pow(k2,2)*cos(k2*s2)*sin(k3*s3)) + cos(phi1)*(k1*cos(phi2)*cos(k1*s1)*(-(k2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2)) - pow(k2,2)*cos(k2*s2)*sin(k3*s3)) - k1*sin(k1*s1)*(k2*cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - pow(k2,2)*sin(k2*s2)*sin(k3*s3))))/(k1*k2*k3);

	float Posx_wrt_k2 = (sin(phi1)*sin(phi2)*(-k3 + k2*s2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + cos(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3))) - cos(phi1)*(cos(phi2)*cos(k1*s1)*(-k3 + k2*s2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + cos(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3))) + sin(k1*s1)*(k2*s2*cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - sin(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3)))))/(pow(k2,2)*k3);

	float Posx_wrt_phi2 = (cos(phi1)*cos(k1*s1)*(2*k1*k2*cos(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2) - k1*sin(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))) + k1*sin(phi1)*(k2*(-1 + cos(k3*s3))*sin(phi2)*sin(phi3) + cos(phi2)*(k3 - cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) + k2*sin(k2*s2)*sin(k3*s3))))/(k1*k2*k3);

	float Posx_wrt_s3  = (k1*sin(phi1)*(k2*k3*cos(phi2)*sin(phi3)*sin(k3*s3) + sin(phi2)*(k2*k3*cos(k3*s3)*sin(k2*s2) + k2*k3*cos(phi3)*cos(k2*s2)*sin(k3*s3))) +
     cos(phi1)*(-(k1*sin(k1*s1)*(k2*k3*cos(k2*s2)*cos(k3*s3) - k2*k3*cos(phi3)*sin(k2*s2)*sin(k3*s3))) + cos(k1*s1)*(2*k1*k2*k3*cos((k3*s3)/2.)*sin(phi2)*sin(phi3)*sin((k3*s3)/2.) + k1*cos(phi2)*(-(k2*k3*cos(k3*s3)*sin(k2*s2)) - k2*k3*cos(phi3)*cos(k2*s2)*sin(k3*s3)))))/(k1*k2*k3);

	float Posx_wrt_k3 = (sin(phi1)*(sin(phi2)*sin(k2*s2)*(k3*s3*cos(k3*s3) - sin(k3*s3)) + cos(phi3)*cos(k2*s2)*sin(phi2)*(-1 + cos(k3*s3) + k3*s3*sin(k3*s3)) + cos(phi2)*sin(phi3)*(-1 + cos(k3*s3) + k3*s3*sin(k3*s3))) + cos(phi1)* (-(cos(phi3)*sin(k1*s1)*sin(k2*s2)) + cos(phi3)*cos(k3*s3)*sin(k1*s1)*sin(k2*s2) - 2*cos(k1*s1)*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2) - cos(k2*s2)*sin(k1*s1)*(k3*s3*cos(k3*s3) - sin(k3*s3)) + k3*s3*cos(k1*s1)*sin(phi2)*sin(phi3)*sin(k3*s3) + k3*s3*cos(phi3)*sin(k1*s1)*sin(k2*s2)*sin(k3*s3) - cos(phi2)*cos(k1*s1)*(sin(k2*s2)*(k3*s3*cos(k3*s3) - sin(k3*s3)) + cos(phi3)*cos(k2*s2)*(-1 + cos(k3*s3) + k3*s3*sin(k3*s3)))))/pow(k3,2);

	float Posx_wrt_phi3 = (k1*sin(phi1)*(-(k2*cos(phi2)*cos(phi3)*(-1 + cos(k3*s3))) + k2*cos(k2*s2)*(-1 + cos(k3*s3))*sin(phi2)*sin(phi3)) + cos(phi1)*(k1*k2*(-1 + cos(k3*s3))*sin(phi3)*sin(k1*s1)*sin(k2*s2) + cos(k1*s1)* (-(k1*k2*cos(phi2)*cos(k2*s2)*(-1 + cos(k3*s3))*sin(phi3)) + 2*k1*k2*cos(phi3)*sin(phi2)*pow(sin((k3*s3)/2.),2))))/(k1*k2*k3);

	float Posy_wrt_s1 = (sin(phi1)*(-(pow(k1,2)*cos(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3))) - k1*sin(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3)))))/(k1*k2*k3);

	float Posy_wrt_k1 = -((sin(phi1)*(-(k2*k3) + cos(k1*s1)*(pow(k1,2)*s1*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*(k3 + pow(k1,2)*s1*cos(k2*s2)*sin(k3*s3))) + k1*s1*sin(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3)))))/(pow(k1,2)*k2*k3));

	float Posy_wrt_phi1 = (-(k1*sin(phi1)*(k2*cos(phi2)*(-1 + cos(k3*s3))*sin(phi3) + sin(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3)))) + cos(phi1)*(-(k2*k3) - k1*sin(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3)) + cos(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3)))))/(k1*k2*k3);

	float Posy_wrt_s2 = (k1*cos(phi1)*sin(phi2)*(-(k2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2)) - pow(k2,2)*cos(k2*s2)*sin(k3*s3)) + sin(phi1)*(k1*cos(phi2)*cos(k1*s1)*(-(k2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2)) - pow(k2,2)*cos(k2*s2)*sin(k3*s3)) - k1*sin(k1*s1)*(k2*cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - pow(k2,2)*sin(k2*s2)*sin(k3*s3))))/(k1*k2*k3);

	float Posy_wrt_k2 = (-(cos(phi2)*cos(k1*s1)*sin(phi1)*(-k3 + k2*s2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + cos(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3)))) - cos(phi1)*sin(phi2)*(-k3 + k2*s2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + cos(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3))) + sin(phi1)*sin(k1*s1)*(-(k2*s2*cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))) + sin(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3))))/(pow(k2,2)*k3);

	float Posy_wrt_phi2 = (k1*cos(phi1)*(-(k2*(-1 + cos(k3*s3))*sin(phi2)*sin(phi3)) + cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))) + cos(k1*s1)*sin(phi1)*(2*k1*k2*cos(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2) - k1*sin(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))))/(k1*k2*k3);

	float Posy_wrt_s3 = (k1*cos(phi1)*(-(k2*k3*cos(phi2)*sin(phi3)*sin(k3*s3)) + sin(phi2)*(-(k2*k3*cos(k3*s3)*sin(k2*s2)) - k2*k3*cos(phi3)*cos(k2*s2)*sin(k3*s3))) + sin(phi1)*(-(k1*sin(k1*s1)*(k2*k3*cos(k2*s2)*cos(k3*s3) - k2*k3*cos(phi3)*sin(k2*s2)*sin(k3*s3))) + cos(k1*s1)*(2*k1*k2*k3*cos((k3*s3)/2.)*sin(phi2)*sin(phi3)*sin((k3*s3)/2.) + k1*cos(phi2)*(-(k2*k3*cos(k3*s3)*sin(k2*s2)) - k2*k3*cos(phi3)*cos(k2*s2)*sin(k3*s3)))))/(k1*k2*k3);

	float Posy_wrt_k3 = (-(cos(phi1)*(sin(phi2)*sin(k2*s2)*(k3*s3*cos(k3*s3) - sin(k3*s3)) + cos(phi3)*cos(k2*s2)*sin(phi2)*(-1 + cos(k3*s3) + k3*s3*sin(k3*s3)) + cos(phi2)*sin(phi3)*(-1 + cos(k3*s3) + k3*s3*sin(k3*s3)))) + sin(phi1)* (-(cos(phi2)*cos(k1*s1)*sin(k2*s2)*(k3*s3*cos(k3*s3) - sin(k3*s3))) - (cos(k1*s1)*sin(phi2)*sin(phi3) + cos(phi3)*sin(k1*s1)*sin(k2*s2))*(2*pow(sin((k3*s3)/2.),2) - k3*s3*sin(k3*s3)) + cos(k2*s2)*(-(k3*s3*cos(k3*s3)*sin(k1*s1)) + sin(k1*s1)*sin(k3*s3) + cos(phi2)*cos(phi3)*cos(k1*s1)*(2*pow(sin((k3*s3)/2.),2) - k3*s3*sin(k3*s3)))))/pow(k3,2);

	float Posy_wrt_phi3 = (k1*cos(phi1)*(k2*cos(phi2)*cos(phi3)*(-1 + cos(k3*s3)) - k2*cos(k2*s2)*(-1 + cos(k3*s3))*sin(phi2)*sin(phi3)) + sin(phi1)*(k1*k2*(-1 + cos(k3*s3))*sin(phi3)*sin(k1*s1)*sin(k2*s2) + cos(k1*s1)* (-(k1*k2*cos(phi2)*cos(k2*s2)*(-1 + cos(k3*s3))*sin(phi3)) + 2*k1*k2*cos(phi3)*sin(phi2)*pow(sin((k3*s3)/2.),2))))/(k1*k2*k3);

	float Posz_wrt_s1 = (-(pow(k1,2)*sin(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3))) + k1*cos(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))))/(k1*k2*k3);

	float Posz_wrt_k1 = (cos(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3)) - k1*s1*sin(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(2*k2*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2) + cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))) + s1*cos(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))))/(k1*k2*k3) - (k1*cos(k1*s1)*((k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + k2*cos(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(k2*(k3 + 2*k1*sin(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2)) + k1*cos(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))))/(pow(k1,2)*k2*k3);

	float Posz_wrt_phi1  = 0.0;

	float Posz_wrt_s2 = (k1*cos(phi2)*sin(k1*s1)*(-(k2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2)) - pow(k2,2)*cos(k2*s2)*sin(k3*s3)) + k1*cos(k1*s1)*(k2*cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - pow(k2,2)*sin(k2*s2)*sin(k3*s3)))/(k1*k2*k3);

	float Posz_wrt_k2 = (-(cos(phi2)*sin(k1*s1)*(-k3 + k2*s2*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3)))*sin(k2*s2) + cos(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3)))) + cos(k1*s1)*(k2*s2*cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - sin(k2*s2)*(k3 + pow(k2,2)*s2*sin(k3*s3))))/(pow(k2,2)*k3);

	float Posz_wrt_phi2 = (sin(k1*s1)*(2*k1*k2*cos(phi2)*sin(phi3)*pow(sin((k3*s3)/2.),2) - k1*sin(phi2)*(-k3 + cos(k2*s2)*(k3 + k2*cos(phi3)*(-1 + cos(k3*s3))) - k2*sin(k2*s2)*sin(k3*s3))))/(k1*k2*k3);

	float Posz_wrt_s3 = (k1*cos(k1*s1)*(k2*k3*cos(k2*s2)*cos(k3*s3) - k2*k3*cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(2*k1*k2*k3*cos((k3*s3)/2.)*sin(phi2)*sin(phi3)*sin((k3*s3)/2.) + k1*cos(phi2)*(-(k2*k3*cos(k3*s3)*sin(k2*s2)) - k2*k3*cos(phi3)*cos(k2*s2)*sin(k3*s3))))/(k1*k2*k3);

	float Posz_wrt_k3 = (cos(k1*s1)*(cos(k2*s2)*(k3*s3*cos(k3*s3) - sin(k3*s3)) - cos(phi3)*sin(k2*s2)*(-1 + cos(k3*s3) + k3*s3*sin(k3*s3))) + sin(k1*s1)*(sin(phi2)*sin(phi3)*(-2*pow(sin((k3*s3)/2.),2) + k3*s3*sin(k3*s3)) - cos(phi2)*(sin(k2*s2)*(k3*s3*cos(k3*s3) - sin(k3*s3)) + cos(phi3)*cos(k2*s2)*(-1 + cos(k3*s3) + k3*s3*sin(k3*s3)))))/pow(k3,2);

	float Posz_wrt_phi3 = (-(k1*k2*cos(k1*s1)*(-1 + cos(k3*s3))*sin(phi3)*sin(k2*s2)) + sin(k1*s1)*(-(k1*k2*cos(phi2)*cos(k2*s2)*(-1 + cos(k3*s3))*sin(phi3)) + 2*k1*k2*cos(phi3)*sin(phi2)*pow(sin((k3*s3)/2.),2)))/(k1*k2*k3);
 /////////////////////orientation part of the Jacobian///////////////////////////////////

	float Orix_wrt_s1 = (2*k1*(cos(phi3)*cos(k2*s2)*cos(k3*s3)*sin(phi2) + cos(phi2)*cos(k3*s3)*sin(phi3) - sin(phi2)*sin(k2*s2)*sin(k3*s3)))/(2*pow(cos(phi2),2)*pow(cos(k2*s2),2)*pow(sin(phi3),2)*pow(sin(k1*s1),2) + cos(k2*s2)*sin(2*phi2)*sin(2*phi3)*pow(sin(k1*s1),2) + sin(phi2)*sin(2*phi3)*sin(2*k1*s1)*sin(k2*s2) + 2*pow(cos(phi2),2)*pow(cos(k3*s3),2)*pow(sin(k1*s1),2)*pow(sin(k2*s2),2) - cos(phi2)*pow(cos(k3*s3),2)*sin(2*k1*s1)*sin(2*k2*s2) + cos(phi2)*pow(sin(phi3),2)*sin(2*k1*s1)*sin(2*k2*s2) + 2*pow(sin(phi2),2)*pow(sin(phi3),2)*pow(sin(k1*s1),2)*pow(sin(k3*s3),2) - sin(phi2)*sin(2*phi3)*sin(2*k1*s1)*sin(k2*s2)*pow(sin(k3*s3),2) + pow(cos(phi3),2)* (2*pow(sin(phi2),2)*pow(sin(k1*s1),2) + cos(phi2)*(2*cos(phi2)*pow(cos(k2*s2),2)*pow(sin(k1*s1),2) + sin(2*k1*s1)*sin(2*k2*s2))*pow(sin(k3*s3),2)) + cos(k2*s2)*sin(phi2)*sin(phi3)*sin(2*k1*s1)*sin(2*k3*s3) - sin(2*phi2)*sin(phi3)*pow(sin(k1*s1),2)*sin(k2*s2)*sin(2*k3*s3) + pow(cos(k1*s1),2)*(2*pow(cos(k2*s2),2)*pow(cos(k3*s3),2) + 2*pow(sin(phi3),2)*pow(sin(k2*s2),2) + 2*pow(cos(phi3),2)*pow(sin(k2*s2),2)*pow(sin(k3*s3),2) - cos(phi3)*sin(2*k2*s2)*sin(2*k3*s3)) + cos(phi2)*cos(phi3)*(-4*cos(k2*s2)*sin(phi2)*sin(phi3)*pow(sin(k1*s1),2)*pow(sin(k3*s3),2) - pow(cos(k2*s2),2)*sin(2*k1*s1)*sin(2*k3*s3) + (sin(2*k1*s1)*pow(sin(k2*s2),2) + cos(phi2)*pow(sin(k1*s1),2)*sin(2*k2*s2))*sin(2*k3*s3)));
	float Orix_wrt_k1 = (2*s1*(cos(phi3)*cos(k2*s2)*cos(k3*s3)*sin(phi2) + cos(phi2)*cos(k3*s3)*sin(phi3) - sin(phi2)*sin(k2*s2)*sin(k3*s3)))/(2*pow(cos(phi2),2)*pow(cos(k2*s2),2)*pow(sin(phi3),2)*pow(sin(k1*s1),2) + cos(k2*s2)*sin(2*phi2)*sin(2*phi3)*pow(sin(k1*s1),2) + sin(phi2)*sin(2*phi3)*sin(2*k1*s1)*sin(k2*s2) + 2*pow(cos(phi2),2)*pow(cos(k3*s3),2)*pow(sin(k1*s1),2)*pow(sin(k2*s2),2) - cos(phi2)*pow(cos(k3*s3),2)*sin(2*k1*s1)*sin(2*k2*s2) + cos(phi2)*pow(sin(phi3),2)*sin(2*k1*s1)*sin(2*k2*s2) + 2*pow(sin(phi2),2)*pow(sin(phi3),2)*pow(sin(k1*s1),2)*pow(sin(k3*s3),2) - sin(phi2)*sin(2*phi3)*sin(2*k1*s1)*sin(k2*s2)*pow(sin(k3*s3),2) + pow(cos(phi3),2)*(2*pow(sin(phi2),2)*pow(sin(k1*s1),2) + cos(phi2)*(2*cos(phi2)*pow(cos(k2*s2),2)*pow(sin(k1*s1),2) + sin(2*k1*s1)*sin(2*k2*s2))*pow(sin(k3*s3),2)) + cos(k2*s2)*sin(phi2)*sin(phi3)*sin(2*k1*s1)*sin(2*k3*s3) - sin(2*phi2)*sin(phi3)*pow(sin(k1*s1),2)*sin(k2*s2)*sin(2*k3*s3) + pow(cos(k1*s1),2)*(2*pow(cos(k2*s2),2)*pow(cos(k3*s3),2) + 2*pow(sin(phi3),2)*pow(sin(k2*s2),2) + 2*pow(cos(phi3),2)*pow(sin(k2*s2),2)*pow(sin(k3*s3),2) - cos(phi3)*sin(2*k2*s2)*sin(2*k3*s3)) + cos(phi2)*cos(phi3)*(-4*cos(k2*s2)*sin(phi2)*sin(phi3)*pow(sin(k1*s1),2)*pow(sin(k3*s3),2) - pow(cos(k2*s2),2)*sin(2*k1*s1)*sin(2*k3*s3) + (sin(2*k1*s1)*pow(sin(k2*s2),2) + cos(phi2)*pow(sin(k1*s1),2)*sin(2*k2*s2))*sin(2*k3*s3)));
	float Orix_wrt_phi1 = 0.0;

	float Orix_wrt_s2 = ((-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)))*(cos(k1*s1)*(-(k2*cos(k3*s3)*sin(k2*s2)) - k2*cos(phi3)*cos(k2*s2)*sin(k3*s3)) - cos(phi2)*sin(k1*s1)*(k2*cos(k2*s2)*cos(k3*s3) - k2*cos(phi3)*sin(k2*s2)*sin(k3*s3))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2)) - (sin(phi3)*(k2*cos(k1*s1)*cos(k2*s2) - k2*cos(phi2)*sin(k1*s1)*sin(k2*s2))*(-(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3))) - sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3)))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2));
	float Orix_wrt_k2  = ((-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)))*(cos(k1*s1)*(-(s2*cos(k3*s3)*sin(k2*s2)) - s2*cos(phi3)*cos(k2*s2)*sin(k3*s3)) - cos(phi2)*sin(k1*s1)*(s2*cos(k2*s2)*cos(k3*s3) - s2*cos(phi3)*sin(k2*s2)*sin(k3*s3))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2)) - (sin(phi3)*(s2*cos(k1*s1)*cos(k2*s2) - s2*cos(phi2)*sin(k1*s1)*sin(k2*s2))*(-(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3))) - sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3)))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2));
	float Orix_wrt_phi2 = (sin(k1*s1)*(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)))*(cos(phi2)*sin(phi3)*sin(k3*s3) + sin(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2)) + ((-(cos(phi2)*cos(phi3)*sin(k1*s1)) + cos(k2*s2)*sin(phi2)*sin(phi3)*sin(k1*s1))*(-(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3))) - sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3)))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2));

	float Orix_wrt_s3 = ((-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)))*(cos(k1*s1)*(-(k3*cos(phi3)*cos(k3*s3)*sin(k2*s2)) - k3*cos(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(k3*cos(k3*s3)*sin(phi2)*sin(phi3) - cos(phi2)*(k3*cos(phi3)*cos(k2*s2)*cos(k3*s3) - k3*sin(k2*s2)*sin(k3*s3)))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2));
	float Orix_wrt_k3 = ((-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)))*(cos(k1*s1)*(-(s3*cos(phi3)*cos(k3*s3)*sin(k2*s2)) - s3*cos(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(s3*cos(k3*s3)*sin(phi2)*sin(phi3) - cos(phi2)*(s3*cos(phi3)*cos(k2*s2)*cos(k3*s3) - s3*sin(k2*s2)*sin(k3*s3)))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2));
	float Orix_wrt_phi3 = ((-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)))*(cos(k1*s1)*sin(phi3)*sin(k2*s2)*sin(k3*s3) + sin(k1*s1)*(cos(phi3)*sin(phi2)*sin(k3*s3) + cos(phi2)*cos(k2*s2)*sin(phi3)*sin(k3*s3))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2)) + ((sin(phi2)*sin(phi3)*sin(k1*s1) - cos(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)))*(-(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3))) - sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3)))))/(pow(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - sin(phi3)*(cos(phi2)*cos(k2*s2)*sin(k1*s1) + cos(k1*s1)*sin(k2*s2)),2) + pow(cos(k1*s1)*(cos(k2*s2)*cos(k3*s3) - cos(phi3)*sin(k2*s2)*sin(k3*s3)) + sin(k1*s1)*(sin(phi2)*sin(phi3)*sin(k3*s3) - cos(phi2)*(cos(k3*s3)*sin(k2*s2) + cos(phi3)*cos(k2*s2)*sin(k3*s3))),2));

	/////////////////the partial Derivative wrt the joint configurations////////////////////
	float Oriz_wrt_s1 = ((cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(k1*cos(phi1)*cos(k3*s3)*sin(phi2)*sin(phi3)*sin(k1*s1) - cos(phi1)*cos(phi3)*cos(k3*s3)*(k1*cos(phi2)*cos(k2*s2)*sin(k1*s1) + k1*cos(k1*s1)*sin(k2*s2)) - cos(phi1)*(k1*cos(k1*s1)*cos(k2*s2) - k1*cos(phi2)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((k1*cos(k3*s3)*sin(phi1)*sin(phi2)*sin(phi3)*sin(k1*s1) + cos(phi3)*cos(k3*s3)*(-(k1*cos(phi2)*cos(k2*s2)*sin(phi1)*sin(k1*s1)) - k1*cos(k1*s1)*sin(phi1)*sin(k2*s2)) - (k1*cos(k1*s1)*cos(k2*s2)*sin(phi1) - k1*cos(phi2)*sin(phi1)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_k1 = ((cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(s1*cos(phi1)*cos(k3*s3)*sin(phi2)*sin(phi3)*sin(k1*s1) - cos(phi1)*cos(phi3)*cos(k3*s3)*(s1*cos(phi2)*cos(k2*s2)*sin(k1*s1) + s1*cos(k1*s1)*sin(k2*s2)) - cos(phi1)*(s1*cos(k1*s1)*cos(k2*s2) - s1*cos(phi2)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((s1*cos(k3*s3)*sin(phi1)*sin(phi2)*sin(phi3)*sin(k1*s1) + cos(phi3)*cos(k3*s3)*(-(s1*cos(phi2)*cos(k2*s2)*sin(phi1)*sin(k1*s1)) - s1*cos(k1*s1)*sin(phi1)*sin(k2*s2)) - (s1*cos(k1*s1)*cos(k2*s2)*sin(phi1) - s1*cos(phi2)*sin(phi1)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_phi1 =  ((cos(k3*s3)*(-(cos(phi2)*sin(phi1)) - cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi1)*cos(phi2)*cos(k1*s1)*cos(k2*s2) - cos(k2*s2)*sin(phi1)*sin(phi2) - cos(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(phi1)*cos(k2*s2)*sin(k1*s1) + (cos(phi1)*cos(phi2)*cos(k1*s1) - sin(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(-(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (cos(phi1)*sin(phi2)*sin(k2*s2) + sin(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_s2 = ((cos(phi3)*cos(k3*s3)*(-(k2*cos(k2*s2)*sin(phi1)*sin(k1*s1)) - k2*cos(phi2)*cos(k1*s1)*sin(phi1)*sin(k2*s2) - k2*cos(phi1)*sin(phi2)*sin(k2*s2)) - (k2*cos(k2*s2)*(cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2)) - k2*sin(phi1)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(-(cos(phi3)*cos(k3*s3)*(-(k2*sin(phi1)*sin(phi2)*sin(k2*s2)) + cos(phi1)*(k2*cos(k2*s2)*sin(k1*s1) + k2*cos(phi2)*cos(k1*s1)*sin(k2*s2)))) + (k2*cos(k2*s2)*sin(phi1)*sin(phi2) - cos(phi1)*(k2*cos(phi2)*cos(k1*s1)*cos(k2*s2) - k2*sin(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_k2 = ((cos(phi3)*cos(k3*s3)*(-(s2*cos(k2*s2)*sin(phi1)*sin(k1*s1)) - s2*cos(phi2)*cos(k1*s1)*sin(phi1)*sin(k2*s2) - s2*cos(phi1)*sin(phi2)*sin(k2*s2)) - (s2*cos(k2*s2)*(cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2)) - s2*sin(phi1)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(-(cos(phi3)*cos(k3*s3)*(-(s2*sin(phi1)*sin(phi2)*sin(k2*s2)) + cos(phi1)*(s2*cos(k2*s2)*sin(k1*s1) + s2*cos(phi2)*cos(k1*s1)*sin(k2*s2)))) + (s2*cos(k2*s2)*sin(phi1)*sin(phi2) - cos(phi1)*(s2*cos(phi2)*cos(k1*s1)*cos(k2*s2) - s2*sin(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_phi2 = ((-(cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k1*s1)*cos(k2*s2)*sin(phi2))) - cos(k3*s3)*(cos(phi1)*cos(phi2)*cos(k1*s1) - sin(phi1)*sin(phi2))*sin(phi3) + (cos(phi2)*sin(phi1)*sin(k2*s2) + cos(phi1)*cos(k1*s1)*sin(phi2)*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((cos(phi3)*cos(k3*s3)*(cos(phi1)*cos(phi2)*cos(k2*s2) - cos(k1*s1)*cos(k2*s2)*sin(phi1)*sin(phi2)) + cos(k3*s3)*(-(cos(phi2)*cos(k1*s1)*sin(phi1)) - cos(phi1)*sin(phi2))*sin(phi3) - (cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(k2*s2)*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_s3 =  ((-(k3*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))) - k3*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3)*sin(k3*s3) - k3*cos(phi3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(k3*cos(k3*s3)*(sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2))) + k3*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)*sin(k3*s3) + k3*cos(phi3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_k3 = ((-(s3*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))) - s3*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3)*sin(k3*s3) - s3*cos(phi3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2))*sin(k3*s3))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3))*(s3*cos(k3*s3)*(sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2))) + s3*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)*sin(k3*s3) + s3*cos(phi3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriz_wrt_phi3 = ((-(cos(phi3)*cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))) + cos(k3*s3)*sin(phi3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))))*(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2)) + ((cos(phi3)*cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2)) - cos(k3*s3)*sin(phi3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)))*(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) - (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3)))/(pow(cos(k3*s3)*(cos(phi1)*cos(phi2) - cos(k1*s1)*sin(phi1)*sin(phi2))*sin(phi3) + cos(phi3)*cos(k3*s3)*(cos(phi2)*cos(k1*s1)*cos(k2*s2)*sin(phi1) + cos(phi1)*cos(k2*s2)*sin(phi2) - sin(phi1)*sin(k1*s1)*sin(k2*s2)) - (cos(k2*s2)*sin(phi1)*sin(k1*s1) + (cos(phi2)*cos(k1*s1)*sin(phi1) + cos(phi1)*sin(phi2))*sin(k2*s2))*sin(k3*s3),2) + pow(-(cos(k3*s3)*(cos(phi2)*sin(phi1) + cos(phi1)*cos(k1*s1)*sin(phi2))*sin(phi3)) - cos(phi3)*cos(k3*s3)*(cos(k2*s2)*sin(phi1)*sin(phi2) + cos(phi1)*(-(cos(phi2)*cos(k1*s1)*cos(k2*s2)) + sin(k1*s1)*sin(k2*s2))) + (sin(phi1)*sin(phi2)*sin(k2*s2) - cos(phi1)*(cos(k2*s2)*sin(k1*s1) + cos(phi2)*cos(k1*s1)*sin(k2*s2)))*sin(k3*s3),2));

	float Oriy_wrt_s1 = -((cos(k3*s3)*(-(k1*cos(k1*s1)*sin(phi2)*sin(phi3)) - k1*cos(phi3)*sin(k1*s1)*sin(k2*s2)) - k1*cos(k2*s2)*sin(k1*s1)*sin(k3*s3) + k1*cos(phi2)*cos(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));

	float Oriy_wrt_k1 = -((cos(k3*s3)*(-(s1*cos(k1*s1)*sin(phi2)*sin(phi3)) - s1*cos(phi3)*sin(k1*s1)*sin(k2*s2)) - s1*cos(k2*s2)*sin(k1*s1)*sin(k3*s3) + s1*cos(phi2)*cos(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));

	float Oriy_wrt_phi1 = 0.0;

	float Oriy_wrt_s2 = -((k2*cos(phi3)*cos(k1*s1)*cos(k2*s2)*cos(k3*s3) - k2*cos(k1*s1)*sin(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(-(k2*cos(phi3)*cos(k3*s3)*sin(k2*s2)) - k2*cos(k2*s2)*sin(k3*s3)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3)+ cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));

	float Oriy_wrt_k2 = -((s2*cos(phi3)*cos(k1*s1)*cos(k2*s2)*cos(k3*s3) - s2*cos(k1*s1)*sin(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(-(s2*cos(phi3)*cos(k3*s3)*sin(k2*s2)) - s2*cos(k2*s2)*sin(k3*s3)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));

	float Oriy_wrt_phi2 = -((-(cos(phi2)*cos(k3*s3)*sin(phi3)*sin(k1*s1)) - sin(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));

	float Oriy_wrt_s3 = -((k3*cos(k1*s1)*cos(k2*s2)*cos(k3*s3) - k3*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2))*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(-(k3*cos(k3*s3)*sin(k2*s2)) - k3*cos(phi3)*cos(k2*s2)*sin(k3*s3)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));

	float Oriy_wrt_k3 = -((s3*cos(k1*s1)*cos(k2*s2)*cos(k3*s3) - s3*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2))*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(-(s3*cos(k3*s3)*sin(k2*s2)) - s3*cos(phi3)*cos(k2*s2)*sin(k3*s3)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));

	float Oriy_wrt_phi3 = -((-(cos(phi2)*cos(k2*s2)*cos(k3*s3)*sin(phi3)*sin(k1*s1)) + cos(k3*s3)*(-(cos(phi3)*sin(phi2)*sin(k1*s1)) - cos(k1*s1)*sin(phi3)*sin(k2*s2)))/sqrt(1 - pow(cos(k3*s3)*(-(sin(phi2)*sin(phi3)*sin(k1*s1)) + cos(phi3)*cos(k1*s1)*sin(k2*s2)) + cos(k1*s1)*cos(k2*s2)*sin(k3*s3) + cos(phi2)*sin(k1*s1)*(cos(phi3)*cos(k2*s2)*cos(k3*s3) - sin(k2*s2)*sin(k3*s3)),2)));


	//column major Jacobian
	float Jacobian[] = { Posx_wrt_s1, Posy_wrt_s1, Posz_wrt_s1,Orix_wrt_s1,Oriy_wrt_s1,Oriz_wrt_s1,
						 Posx_wrt_k1, Posy_wrt_k1, Posz_wrt_k1,Orix_wrt_k1,Oriy_wrt_k1,Oriz_wrt_k1,
						 Posx_wrt_phi1, Posy_wrt_phi1, Posz_wrt_phi1,Orix_wrt_phi1,Oriy_wrt_phi1,Oriz_wrt_phi1,
						 Posx_wrt_s2, Posy_wrt_s2, Posz_wrt_s2,Orix_wrt_s2,Oriy_wrt_s2,Oriz_wrt_s2,
						 Posx_wrt_k2, Posy_wrt_k2, Posz_wrt_k2,Orix_wrt_k2,Oriy_wrt_k2,Oriz_wrt_k2,
						 Posx_wrt_phi2, Posy_wrt_phi2, Posz_wrt_phi2,Orix_wrt_phi2,Oriy_wrt_phi2,Oriz_wrt_phi2,
						 Posx_wrt_s3, Posy_wrt_s3, Posz_wrt_s3,Orix_wrt_s3,Oriy_wrt_s3,Oriz_wrt_s3,
						 Posx_wrt_k3, Posy_wrt_k3, Posz_wrt_k3,Orix_wrt_k3,Oriy_wrt_k3,Oriz_wrt_k3,
						 Posx_wrt_phi3, Posy_wrt_phi3, Posz_wrt_phi3,Orix_wrt_phi3,Oriy_wrt_phi3,Oriz_wrt_phi3};

	vector<float>JacobianResult(54);
	for(int i= 0; i<54;++i)
	{
		JacobianResult[i] = Jacobian[i];
	}
	return JacobianResult;
}

vector<float> ManipulatorNSections:: ComputeJacobian_each_sec_tips_pos_non_mobile_base(float* Config, int sec_ind)
{
	vector<float> result;
	if(sec_ind == 2)
	{
		result.resize(27);
		vector<float>JacobianResult = ComputeJacobian_whole_non_mobile(Config);
		vector<float>JacobianResult_tmp(54);
		for(int i = 0; i < 6; ++i)
		{
			for(int j = 0; j < 9; ++j)
			{
				JacobianResult_tmp[i*9+j] = JacobianResult[j*6+i];
			}
		}
		vector<float> result_t(27);
		for(int i = 0; i < 27; ++i)
		{
			result_t[i] = JacobianResult_tmp[i];
		}

		for(int i = 0; i < 9; ++i)
		{
			for(int j = 0; j < 3; ++j)
			{
				result[i*3+j] = result_t[j*9+i];
			}
		}
	}

	return result;
}

bool ManipulatorNSections::instantaneous_inverse_kine(float* _end_velocity, float* _delta_configs_output)
{
	float config_tmp[9] = {0};
	for(int i = 0; i < 9; ++i)
	{
		config_tmp[i] = this->config[i];
	}
	float base_pos [3] = {this->base_trans_matrix[12],this->base_trans_matrix[13],this->base_trans_matrix[14]};
	vector<float> ColumnMajorJacob = ComputeJacobian_whole_non_mobile(config_tmp);
	float *CMJacob  = new float [ColumnMajorJacob.size()];
	for(int i = 0 ;i < ColumnMajorJacob.size(); ++i)
	{
		CMJacob[i] = ColumnMajorJacob[i];
	}

	float* inv_CMJacob = InverMatrix(CMJacob,6,9);

	float* result = MatrixMulti(inv_CMJacob,_end_velocity,9,6,6,1);

	for(int i = 0; i < 9; ++i)
	{
		_delta_configs_output[i] = result[i];
	}
	delete [] result;
	return true;
}

bool ManipulatorNSections::instantaneous_inverse_kine_w_SelfM(float* _end_velocity, float* _delta_configs_output,float null_space_motion [9])
{
	float config_tmp[9] = {0};
	for(int i = 0; i < 9; ++i)
	{
		config_tmp[i] = this->config[i];
	}
	float base_pos [3] = {this->base_trans_matrix[12],this->base_trans_matrix[13],this->base_trans_matrix[14]};
//	vector<float> ColumnMajorJacob = ComputeJacobian_whole_mobile(config_tmp,base_pos);
	vector<float> ColumnMajorJacob = ComputeJacobian_whole_non_mobile(config_tmp);
	float *CMJacob  = new float [ColumnMajorJacob.size()];
	for(int i = 0 ;i < ColumnMajorJacob.size(); ++i)
	{
		CMJacob[i] = ColumnMajorJacob[i];
	}

	float* inv_CMJacob = InverMatrix(CMJacob,6,9);

	float* J_inv_mult_J = MatrixMulti(inv_CMJacob,CMJacob,9,6,6,9);

	float Null_S_projector [81] = {0};
	for(int i = 0; i < 9; ++i)
	{
		for(int j = 0; j < 9; ++j)
		{
			if (i == j)
			{
				Null_S_projector[i*9+j] = 1.0 - J_inv_mult_J[i*9 +j];
			}
			else
			{
				Null_S_projector[i*9+j] = - J_inv_mult_J[i*9 +j];
			}
		}
	}

	float* result_end = MatrixMulti(inv_CMJacob,_end_velocity,9,6,6,1);

	float* result_null = MatrixMulti(Null_S_projector,null_space_motion,9,9,9,1);
	
	for(int i = 0; i < 9; ++i)
	{
		_delta_configs_output[i] = result_end[i] + 1.0*result_null[i];
	}

	delete [] J_inv_mult_J;
	delete [] inv_CMJacob;
	delete [] result_end;
	delete [] result_null;
	return true;
}

bool ManipulatorNSections::generateKnotConfigs(const std::vector<float> &start_config,
                                               const std::vector<float> &start_base_frame,
                                               const std::vector<float> &end_config,
                                               const std::vector<float> &end_base_frame,
                                               std::vector<std::vector<float> > &knot_configs,
                                               std::vector<std::vector<float> > &knot_base_frames)
{
	// total rotated angle of z-axis around y-axis
	Eigen::Vector3f start_z_axis(start_base_frame[8],start_base_frame[9],start_base_frame[10]);
	Eigen::Vector3f end_z_axis(end_base_frame[8],end_base_frame[9],end_base_frame[10]);
	float rotated_angle_z_axis = acosf(start_z_axis.dot(end_z_axis));
	
	Eigen::Vector3f rotate_axis_start_z_to_end_z = start_z_axis.cross(end_z_axis);
	rotate_axis_start_z_to_end_z.normalize();
	
	int knot_num = 80;
	for (int i=0; i<knot_num; ++i)
	{
		std::vector<float> one_knot_config(3*n_sections);
		std::vector<float> one_knot_base_frame(16,0.0);
		
		// knot config
		for(int j = 0; j < 3*n_sections; ++j)
		{
			one_knot_config[j] = start_config[j] + (end_config[j] - start_config[j]) * ((float)i/knot_num);	
		}
		knot_configs.push_back(one_knot_config);
		
		// knot base frame
		float interM_z_axis [3] = {0.0};
		float angle_z_to_rot_from_curz = ((float)i/knot_num)* rotated_angle_z_axis;
		float axis_rot_curz_to_newz[3] = {rotate_axis_start_z_to_end_z[0],
			                           rotate_axis_start_z_to_end_z[1],
			                           rotate_axis_start_z_to_end_z[2]};
		float arm_base_z[3] = {start_z_axis[0], start_z_axis[1], start_z_axis[2]};
		RotAVec(axis_rot_curz_to_newz, arm_base_z, angle_z_to_rot_from_curz, interM_z_axis);
		Normalize(interM_z_axis,3);
		float interM_y_axis [3] = {start_base_frame[4],start_base_frame[5],start_base_frame[6]};
		float interM_x_axis [3] = {0.0};
		CrossProduct(interM_y_axis,interM_z_axis,interM_x_axis);
		Normalize(interM_x_axis,3);
		
		for (int k=0; k<3; ++k)
		{
			one_knot_base_frame[k] = interM_x_axis[k];
			one_knot_base_frame[4+k] = interM_y_axis[k];
			one_knot_base_frame[8+k] = interM_z_axis[k];
			one_knot_base_frame[12+k] = start_base_frame[12+k];
		}
		one_knot_base_frame[15] = 1.0;
		//~ knot_base_frames.push_back(one_knot_base_frame);
		knot_base_frames.push_back(end_base_frame);
	}
	
	return true;
}

bool ManipulatorNSections::isCfgWithinPhysicalLimits(const std::vector<float>& cfg)
{
	if (cfg.size()!=3*n_sections)
	{
		ROS_ERROR("Specified cfg dimension not match number of sections!");
		return false;
	}
	else
	{
		bool res = true;
		for (int i=0; i<n_sections; ++i)
		{
			sectionPhysicalLimits tmp = section_bounds.find(i)->second;
			if ( cfg[i*3] < tmp.length_min || cfg[i*3] > tmp.length_max )
			{
				ROS_ERROR("Cfg length out of bounds!");
				res = false;
			}
			if ( cfg[i*3+1] < tmp.curvature_min || cfg[i*3+1] > tmp.curvature_max )
			{
				ROS_ERROR("Cfg curvature out of bounds!");
				res = false;
			}
			if ( cfg[i*3+2] < tmp.phi_min || cfg[i*3+2] > tmp.phi_max )
			{
				ROS_ERROR("Cfg phi out of bounds!");
				res = false;
			}
		}
		if (res)
			return true;
		else
			return false;
	}
}

std::vector<bool> ManipulatorNSections::isSecCfgWithinPhysicalLimits(const std::vector<float>& cfg, const int sec_index)
{
	std::vector<bool> res(3, false);
	if (sec_index < 0 || sec_index > (n_sections-1))
	{
		ROS_ERROR("Wrong section index!");
		return res;
	}
	else if (cfg.size()!=3*n_sections)
	{
		ROS_ERROR("Wrong robot cfg dimension!");
		return res;
	}
	else
	{
		sectionPhysicalLimits tmp = section_bounds.find(sec_index)->second;
		if ( cfg[sec_index*3] < tmp.length_min || cfg[sec_index*3] > tmp.length_max )
		{
			// ROS_WARN("Cfg length out of bounds!");
			res[0] = false;
		}
		else
		{
			res[0] = true;
		}
		
		if ( cfg[sec_index*3+1] < tmp.curvature_min || cfg[sec_index*3+1] > tmp.curvature_max )
		{
			// ROS_WARN("Cfg curvature out of bounds!");
			res[1] = false;
		}
		else
		{
			res[1] = true;
		}
		
		if ( cfg[sec_index*3+2] < tmp.phi_min || cfg[sec_index*3+2] > tmp.phi_max )
		{
			// ROS_WARN("Cfg phi out of bounds!");
			res[2] = false;
		}
		else
		{
			res[2] = true;
		}
		return res;
	}
}

