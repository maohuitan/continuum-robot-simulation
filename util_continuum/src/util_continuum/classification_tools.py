#!/usr/bin/env python

# Mabel Zhang
# 4 Sep 2015
#
# Refactored from triangle_sampling package triangles_svm.py
#
# Ref: http://scikit-learn.org/stable/auto_examples/plot_confusion_matrix.html
#


# Numpy
import numpy as np
from sklearn.metrics import confusion_matrix

# matplotlib
import matplotlib.pyplot as plt


# Parameters:
#   ticks: String labels to display on x- and y-ticks. e.g. category names,
#     instead of integer class IDs that won't give you a clue what the class is.
#     If [], will just use default xticks (integers).
#   img_name: Full path to save image to. If '', will not save.
def draw_confusion_matrix (true_lbls, predicted_lbls, ticks=[], img_name='',
  title_prefix='', draw_title=True, raw=False):
  
  # Compute confusion matrix
  # Make sure you keep order the same. In (true, predicted) order, the y-axis
  #   is true label, the x-axis is predicted. If you swap these, then the
  #   plot's x- and y-labels need to swap as well.
  # Ref: http://scikit-learn.org/stable/modules/generated/sklearn.metrics.confusion_matrix.html
  cm = confusion_matrix (true_lbls, predicted_lbls)
  print ('Confusion matrix:')
  print (cm)

  # Do percentages, instead of raw numbers that are returned by default
  if not raw:
    # Sum each row to get total number of objects in each category (y-axis
    #   label of confusion mat is true labels, so sum rows, not columns).
    #   Need floating point so cm divided by this array gives floats, not ints!
    nTtl = np.sum (cm, axis=1).astype (np.float32)

    # Reshape to column vector, so element-wise divide would divide each row of
    #   conf mat by total object sum in that row.
    nTtl = nTtl.reshape ((np.size (nTtl), 1))

    # Element-wise divide each row of confusion matrix by the total # objects
    cm = np.divide (cm, nTtl)
    #print (cm)

  # Show confusion matrix in a separate window
  # Using a figure makes colorbar same length as main plot!
  fig = plt.figure ()
  plt.matshow(cm, fignum=fig.number)
  plt.set_cmap ('jet')
  plt.colorbar()
  if draw_title:
    # Ref move title up: http://stackoverflow.com/questions/12750355/python-matplotlib-figure-title-overlaps-axes-label-when-using-twiny
    plt.title(title_prefix + 'Confusion matrix', y=1.2)
  plt.ylabel('True label')
  plt.xlabel('Predicted label')


  # Display class names on the axes ticks
  if ticks:
    draw_classname_ticks (cm, ticks)

  if img_name:
    plt.savefig (img_name, bbox_inches='tight')
    print ('Plot saved to %s' % img_name)

  plt.show()


# Parameters:
#   unsorted_idx: n x n. Numpy 2D array of indices. Sort this to obtain the
#     0 to n ordering for the matrix.
#   distance_matrix: n x n. Distance matrix btw exhaustive pairwise distances
#     among n objects. Sorted left to right by close to far.
#     Not sorted by sample ID! This function sorts the unsorted_idx indices,
#     and indexes distance_matrix using the sorted indices, so that the
#     confusion matrix can be labeled as 0 to n for x and y axes.
# Useful for plotting knn distances.
def draw_confusion_matrix_per_sample (unsorted_idx, distance_matrix, ticks=[],
  img_name='', title_prefix='',
  draw_title=True, draw_xylbls=True, draw_xticks=True, draw_yticks=True,
  fontsize=10):

  # Matrix is n x n
  nSamples = np.shape (unsorted_idx) [0]

  dists_sorted = np.zeros (np.shape (distance_matrix))
  
  for i in range (0, nSamples):

    # Sort the ith row of sample IDs. Save the indexes from sorting.
    # Ref: http://docs.scipy.org/doc/numpy/reference/generated/numpy.argsort.html
    sorted_idx_rowi = np.argsort (unsorted_idx [i, :])

    # Take ith row of distance matrix, index it using the sorted indices
    dists_sorted [i, :] = distance_matrix [i, :] [sorted_idx_rowi]


  print ('')
  print ('Max distance: %f. Min distance: %f. Mean distance: %f' % \
    (np.max (dists_sorted), np.min (dists_sorted), np.mean (dists_sorted)))

  print ('Confusion matrix:')
  print (dists_sorted)

  # Show confusion matrix in a separate window
  # Using a figure makes colorbar same length as main plot!
  fig = plt.figure ()
  plt.matshow (dists_sorted, fignum=fig.number)
  if draw_title:
    if ticks:
      plt.title(title_prefix + 'Confusion matrix', y=1.3)
    else:
      plt.title(title_prefix + 'Confusion matrix')
  # Reverse colors, so that small dists are hot, far dists are cold.
  # http://stackoverflow.com/questions/3279560/invert-colormap-in-matplotlib
  orig_cmap = plt.get_cmap ()
  plt.set_cmap ('jet_r')
  plt.colorbar()

  if draw_xylbls:
    plt.ylabel('Objects')
    plt.xlabel('Objects')

  ax = plt.gca ()

  # Display class names on the axes ticks
  if draw_xticks or draw_yticks:
    # Ticks passed in
    if ticks:
      draw_classname_ticks (dists_sorted, ticks,
        draw_x=draw_xticks, draw_y=draw_yticks, fontsize=fontsize)

    # Automatically generate numerical ticks
    else:

      if draw_xticks:
        # You can adjust these to make as frequent or as rare ticks as you want
        plt.xticks (np.arange (0, nSamples, 20.0))

      if draw_yticks:
        plt.yticks (np.arange (0, nSamples, 20.0))

  if not draw_xticks:
    # http://stackoverflow.com/questions/2176424/hiding-axis-text-in-matplotlib-plots
    #ax.get_xaxis ().set_visible (False)
    ax.get_xaxis ().set_ticks ([])

  if not draw_yticks:
    #ax.get_yaxis ().set_visible (False)
    ax.get_yaxis ().set_ticks ([])


  for tick in ax.xaxis.get_major_ticks ():
    # http://stackoverflow.com/questions/6390393/matplotlib-make-tick-labels-font-size-smaller
    tick.label.set_fontsize (10)
    # specify integer or one of preset strings, e.g.
    #tick.label.set_fontsize('x-small') 
    #tick.label.set_rotation('vertical')


  if img_name:
    plt.savefig (img_name, bbox_inches='tight')
    print ('Plot saved to %s' % img_name)

  plt.show()

  # This opens a window! I don't know how to do it without opening a window.
  #   So not doing it anymore.
  # Set colors back to whatever they were before
  #plt.set_cmap (orig_cmap)


def draw_classname_ticks (mat, ticks, draw_x=True, draw_y=True, fontsize=10):

  #print ('Plotting using these axis tick labels:')
  #print (ticks)

  # Ref set textual tick labels: http://stackoverflow.com/questions/5439708/python-matplotlib-creating-date-ticks-from-string
  ax = plt.gca ()

  if draw_x:
    ax.set_xticklabels (ticks, rotation='vertical')

    # Show ALL ticks (by default, it doesn't show all of them)
    # Ref: http://stackoverflow.com/questions/12608788/changing-the-tick-frequency-on-x-or-y-axis-in-matplotlib
    # ticks() font size
    #   http://stackoverflow.com/questions/13139630/how-can-i-change-the-font-size-of-ticks-of-axes-object-in-matplotlib
    plt.xticks (np.arange (0, np.shape (mat) [0], 1.0), fontsize=fontsize)

  if draw_y:
    ax.set_yticklabels (ticks)
    plt.yticks (np.arange (0, np.shape (mat) [0], 1.0), fontsize=fontsize)


def calc_accuracy (true_lbls, predicted_lbls, nClasses):

  # Find number of predictions that matched ground truth for test data.
  #   Can use np.sum() or np.count_nonzero() on a boolean array.
  # Ref: http://stackoverflow.com/questions/8364674/python-numpy-how-to-count-the-number-of-true-elements-in-a-bool-array
  nCorrect = np.sum (predicted_lbls == true_lbls)
  nSamples_te = len (true_lbls)

  acc = float (nCorrect) / float (nSamples_te)

  print ('Accuracy out of %d classes: %f%% (%d/%d)' % (nClasses, acc * 100.0,
    nCorrect, nSamples_te))

  return acc


