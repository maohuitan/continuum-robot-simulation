#!/usr/bin/env python

# Mabel Zhang
# 17 Feb 2015
#
# Configure paths used by multiple files
#
# 1 Dec 2016: Copied from tactile_collect tactile_config.py in reFlexHand repo.
#

import os, inspect

import rospkg


# Variable parameter size:
#   args[0]: option. 'train', 'test', 'custom', or 'full'.
#     If 'custom' or 'full', user must supply args[1].
#   args[1]:
#     if arg[0] == 'custom': subpath name inside train/ directory.
#     if arg[0] == 'full': full path.
#
# Ref variable arguments:
#   http://stackoverflow.com/questions/919680/can-a-variable-number-of-arguments-be-passed-to-a-function
#
# 1 Dec 2016: Renamed from config_paths() in tactile_collect tactile_config.py
def config_datapath (*args):

  #print (args)

  if len (args) == 0:
    print ('ERROR in tactile_config.py config_paths(): Must supply at least one argument.')
    raise Exception ()
    return ''

  option = args [0]
  if len (args) > 1:
    subpath = args[1]

  # Sanity checks
  if option == 'custom' and len (args) < 2:
    print ('ERROR in tactile_config.py config_paths(): custom option specified, but a custom subpath is not specified.')
    raise Exception ()
    return ''

  if option == 'full' and len (args) < 2:
    print ('ERROR in tactile_config.py config_paths(): full option specified, but a full path is not specified.')
    raise Exception ()
    return ''


  # Ref: http://stackoverflow.com/questions/50499/in-python-how-do-i-get-the-path-and-name-of-the-file-that-is-currently-executin
  datapath = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
  # Ref: https://docs.python.org/2/library/os.path.html
  datapath = os.path.join (datapath, '../../../../../../train/')

  # Configure this for specific object paths
  if option == 'custom':
    datapath = os.path.join (datapath, subpath)
  elif option == 'full':
    datapath = subpath
  else:
    print ('ERROR in tactile_config.py config_paths(): option string invalid. Specify train or test.')
    # Ref: http://stackoverflow.com/questions/6720119/setting-exit-code-in-python-when-an-exception-is-raised
    raise Exception ()
    return ''

  # realpath() returns canonical path eliminating symbolic links
  #   Ref: https://docs.python.org/2/library/os.path.html
  datapath = os.path.realpath (datapath)

  # http://stackoverflow.com/questions/273192/in-python-check-if-a-directory-exists-and-create-it-if-necessary
  if not os.path.exists (datapath):
    os.makedirs (datapath)

  return datapath


# Raw model path
def get_root_path ():

  root_path = config_datapath ('custom', '')
  return root_path


# Data path
def get_chord_path (mode):

  chord_path = config_datapath ('custom', 'chord_sampling/csv_' + mode + '_chords')
  return chord_path


# Data path
# Parameters:
#   subdir: Subdirectory inside the root histogram path. Returned from
#     get_nbins_subpath().
def get_hist_path (mode, subdir=''):

  hist_path = config_datapath ('custom',
    os.path.join ('chord_sampling', 'csv_' + mode + '_hists', subdir))
  return hist_path


# Data path
def get_prob_path (mode):
  prob_path = config_datapath ('custom', 'active_chord/probs_' + mode)
  return prob_path

def get_cost_path (mode):
  cost_path = config_datapath ('custom', 'active_chord/costs_' + mode)
  return cost_path


def get_active_img_path ():

  img_dir = config_datapath ('custom', 'active_chord/imgs')
  return img_dir


# Configuration file path
def get_recog_meta_path ():

  meta_path = os.path.join (rospkg.RosPack().get_path ('chord_recognition'),
    'config')
  return meta_path


def get_active_meta_path ():

  meta_path = os.path.join (rospkg.RosPack().get_path ('active_chord'),
    'config')
  return meta_path


# Copied and adapted from triangle_sampling config_paths.py
# Used by recognize_svm.py (writer), active_predict.py (reader)
def get_svm_model_path (mode, nbins_subpath):

  hist_path = get_hist_path (mode, nbins_subpath)

  svm_name = os.path.join (hist_path, 'svm_' + mode + \
    '.joblib.pkl')
  svm_lbls_name = os.path.join (hist_path, 'svm_' + mode + \
    '_lbls.pkl')

  return svm_name, svm_lbls_name


# Parameters:
#   bins3D: String of three number of bins, no spaces, e.g. '10,10,10'
# Return configurated folder name, and list of 3 integers converted from
#   bins3D string.
def get_nbins_subpath (bins3D):

  # Get rid of spaces in between, in case there are any
  nbinsStr = bins3D.split (',')
  # Strip any spaces
  nbinsStr = [b.strip() for b in nbinsStr]
  # Store the list of 3 integers, for returning
  nbins = [int(b) for b in nbinsStr]
  # Concat list of strings into one string, separated by underscore
  nbinsStr = '_'.join (nbinsStr)

  subdir_name = 'nbins%s' % (nbinsStr)

  return subdir_name, nbins

