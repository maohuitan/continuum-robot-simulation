#!/usr/bin/env python

# Mabel Zhang
# 29 Nov 2016
#
# Utility functions for YAML I/O.
#

import yaml
# To load yaml file in order, so that csv file column titles can be in order
from collections import OrderedDict


# Read yaml into ordered dictionary
# Refactored from chord_recognition chord_consts.py
# Ref: http://stackoverflow.com/questions/5121931/in-python-how-can-you-load-yaml-mappings-as-ordereddicts
def yaml_ordered_load (stream, Loader=yaml.Loader,
  object_pairs_hook=OrderedDict):

  class OrderedLoader(Loader):
    pass
  def construct_mapping(loader, node):
    loader.flatten_mapping(node)
    return object_pairs_hook(loader.construct_pairs(node))
  OrderedLoader.add_constructor(
    yaml.resolver.BaseResolver.DEFAULT_MAPPING_TAG,
    construct_mapping)
  return yaml.load(stream, OrderedLoader)


# To write OrderedDict to yaml file (by default, yaml writes in alphabetical
#   order).
# Ref: http://stackoverflow.com/questions/16782112/can-pyyaml-dump-dict-items-in-non-alphabetical-order
class UnsortableList(list):
    def sort(self, *args, **kwargs):
        pass

class UnsortableOrderedDict(OrderedDict):
    def items(self, *args, **kwargs):
        return UnsortableList(OrderedDict.items(self, *args, **kwargs))

yaml.add_representer(UnsortableOrderedDict, yaml.representer.SafeRepresenter.represent_dict)


