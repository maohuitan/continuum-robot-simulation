#!/usr/bin/env python

# Mabel Zhang
# 6 Sep 2016
#
# Utility functions for matplotlib
#

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.cm import get_cmap

import numpy as np


# Call this fn at start of program, if you need truetype 42 fonts e.g. for
#   ICRA IROS submission PDF compliance!
def truetype ():

  # Copied from triangle_sampling triangles_svm.py
  # For ICRA PDF font compliance. No Type 3 font (rasterized) allowed
  #   Ref: http://phyletica.org/matplotlib-fonts/
  # You can do this in code, or edit matplotlibrc. But problem with matplotlibrc
  #   is that it's permanent. When you export EPS using TrueType (42), Mac OS X
  #   cannot convert to PDF. So you won't be able to view the file you
  #   outputted! Better to do it in code therefore.
  #   >>> import matplotlib
  #   >>> print matplotlib.matplotlib_fname()
  #   Ref: http://matplotlib.1069221.n5.nabble.com/Location-matplotlibrc-file-on-my-Mac-td24960.html
  matplotlib.rcParams['pdf.fonttype'] = 42
  matplotlib.rcParams['ps.fonttype'] = 42


# Return a color in matplotlib colormap.
# Parameters:
#   item_idx: Index of current item to plot. This number is used to interpolate
#     the colormap and get you the color for this item, given the total number
#     of items you have.
#   n_items: Set higher numbers when have more things to plot!
#   colormap_name: string from
#     http://matplotlib.org/examples/color/colormaps_reference.html
#     jet is the common standard.
#     gist_ncar has quicker color change than jet, good when you have a lot of
#       items to plot.
#     nipy_spectral is also quick changing, but darker. I used this for IROS.
#       Very pretty rich colors in a variety of range. Good for a small number
#       of curves.
#     Dark2 and Set1 are fast changing dark pastels
def mpl_color (item_idx, n_items, rand_color=False,
  colormap_name='nipy_spectral'):

  if rand_color:
    item_idx = np.random.randint (n_items)

  colormap = get_cmap (colormap_name, n_items)

  return colormap (item_idx)


# Copied from triangle_sampling stats_hist_num_bins.py
def plot_line (xdata, ydata, title, xlbl, ylbl, out_name, color, lbl,
  dots=True, grid=True,
  stdev=None, do_save=True):

  #####
  # Plot
  #####

  if dots:
    plt.plot (xdata, ydata, 'o', markersize=5,
      markeredgewidth=0, color=color)
  hdl, = plt.plot (xdata, ydata, '-', linewidth=2, color=color, label=lbl)

  # Plot error bars
  #   http://matplotlib.org/1.2.1/examples/pylab_examples/errorbar_demo.html
  #   Plot error bar without line, fmt='':
  #   http://stackoverflow.com/questions/18498742/how-do-you-make-an-errorbar-plot-in-matplotlib-using-linestyle-none-in-rcparams
  if stdev:
    plt.errorbar (xdata, ydata, fmt='', capthick=2, yerr=stdev, color=color)

  if grid:
    #plt.grid (True, color='gray')
    plt.grid (True, color=[0.8, 0.8, 0.8])
  else:
    plt.grid (False)

  if title:
    plt.title (title)
  plt.xlabel (xlbl)
  plt.ylabel (ylbl)


  # Save to file
  if do_save:
    plt.savefig (out_name, bbox_inches='tight')
    print ('Plot saved to %s' % out_name)

  #plt.show ()

  # If you want legend, pass in a label for each thing you plot. Then legend
  #   will automatically show with just plt.legend().
  # Or you can pass in handles.
  # Ref: http://matplotlib.org/users/legend_guide.html
  #plt.legend (handles=[nn_hdl, svm_hdl], loc=0)

  return hdl

