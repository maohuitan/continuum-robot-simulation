#!/usr/bin/env python

# Mabel Zhang
# 7 Jan 2017
#
# Utility functions for matrix operations
#

import numpy as np

# My packages
from util_continuum.ansi_colors import ansi_colors


# Find if a row in samples is a duplicate of the given row.
# Parameters:
#   row: 1 x m
#   samples: nSamples x m
# Returns indices in samples that are duplicates of row
def find_dups_row (row, samples, thresh=1e-6):

  if row.ndim != 1:
    print ('%sERROR: mat_util.py find_dups(): row arg is not a 1D array. Cannot find duplicates! Check your find_dups() call.%s' % (
      ansi_colors.FAIL, ansi_colors.ENDC))
    return []

  # Check if row is a duplicate of an existing row in samples. Subtract,
  #   check if ~0 to get m x n booleans. Take rowwise AND.
  #   If all columns in a row are True, meaning a duplicate row.
  rowwise_eq = np.all (np.abs (samples - row.reshape (1, row.size)) < 1e-6, axis=1)
  dup_idxes = np.where (rowwise_eq) [0]

  return dup_idxes


# Find rows in incoming rows that already exist in existing rows.
# Order matters!! The return values are ordered based on the params being in
#   order.
# Returns two lists. Values are indices. nDups x 1. These are the rawest
#   returned from this file.
# Parameters:
#   incoming: nIncomingChords x nDims
#   existing: nExistingChords x nDims
def find_dups_multirow_twolists (incoming, existing, thresh=1e-6):

  # Step 1
  # Input:
  #   existing: nExistingChords x nDims
  #   incoming: nIncomingChords x nDims
  # Output:
  #   existing: nExistingChords x nDims
  #   incoming_3d: Tile in 3rd dimension, so the matrix becomes nRows x 1 x nCols,
  #                depth x rows x cols
  incoming_3d = incoming.reshape (incoming.shape [0], 1, incoming.shape [1])

  # Step 2
  # Apply batches and prepare containers
  batch_size = 10000
  num_existing_rows = existing.shape[0]
  num_incoming_rows = incoming_3d.shape[0]

  num_batch_ops = min(num_existing_rows, num_incoming_rows) / batch_size

  dup_existing_row_container = np.empty((0))
  dup_incoming_row_container = np.empty((0))

  # Step 3 
  # Apply batch operations
  for i in range(0, num_batch_ops):
    existing_local = existing[ i*batch_size : batch_size, :]
    incoming_3d_local = incoming_3d[ i*batch_size : batch_size, :, :]

    # Result of subtraction is nIncomingChords x nExistingChords x nDims,
    #   depth x rows x cols.
    #   If a chord is a duplicate, all columns (which is axis=2) of a row
    #     would be 0s.
    # Result of np.all(, axis=2) is nIncomingChords x nExistingChords.
    #   In dup_bool [r, c], r indexes incoming rows, c indexes
    #     existing rows.
    #   If dup_bool [r, c] is true, that means incoming chord r is a duplicate
    #     of existing chord c.
    dup_bool_local = np.all (np.abs (existing_local - incoming_3d_local) < 1e-6, axis=2)
    dup_incoming_row_local, dup_existing_row_local = np.where (dup_bool_local)

    # Step 4
    # Concatenate results
    dup_existing_row_container = np.append(dup_existing_row_container, dup_existing_row_local)
    dup_incoming_row_container = np.append(dup_incoming_row_container, dup_incoming_row_local)

  # Step 5
  # Process remaining elements after batch operations
  existing_local = existing[ num_batch_ops*batch_size : , :]
  incoming_3d_local = incoming_3d[ num_batch_ops*batch_size : , :, :]
  
  dup_bool_local = np.all (np.abs (existing_local - incoming_3d_local) < 1e-6, axis=2)
  dup_incoming_row_local, dup_existing_row_local = np.where (dup_bool_local)

  # Step 6 
  # Append remaining results
  dup_existing_row_container = np.append(dup_existing_row_container, dup_existing_row_local)
  dup_incoming_row_container = np.append(dup_incoming_row_container, dup_incoming_row_local)

  dup_existing_row_container = dup_existing_row_container.astype (int)
  dup_incoming_row_container = dup_incoming_row_container.astype (int)

  # DEBUG
  # print "dup_incoming_row_container"
  # print dup_incoming_row_container
  # print "dup_existing_row_container"
  # print dup_existing_row_container
  return dup_incoming_row_container, dup_existing_row_container


# Returns one list. Its indices index "incoming", its values index "existing".
#   nRows x 1. Value -1 indicates not a duplicate.
# Assumption: incoming != existing. If they are the same, then call
#   find_dups_self() instead! There are cases specific to incoming == existing 
#   that need additional care.
def find_dups_multirow_onelist (incoming, existing, thresh=1e-6):

  # It could be that an incoming row has multiple duplicates in existing rows,
  #   then you can have pairs e.g. (3, 1), (3, 2), (3, 3), note 3 is repeated.
  dup_incoming_row, dup_existing_row = find_dups_multirow_twolists (
    incoming, existing, thresh)

  # If a row finds multiple duplicates in incoming, just pick first one
  # np.unique() returns index of first occurence.
  uniq_dups, uniq_dup_idxes = np.unique (dup_incoming_row, return_index=True)
  '''
  print ('dup_incoming_row:')
  print (dup_incoming_row)
  print ('dup_existing_row:')
  print (dup_existing_row)
  print ('uniq_dups:')
  print (uniq_dups)
  print ('uniq_dup_idxes:')
  print (uniq_dup_idxes)
  '''

  if uniq_dups.size < dup_incoming_row.size:
    print ('find_dups_multirow_onelist(): %d extra pairings (for %d rows) are found. This means some incoming rows have multiple duplicates in existing rows. Will take the first occurrence in existing samples as the duplicate index.' % (
      dup_incoming_row.size - uniq_dup_idxes.size, uniq_dup_idxes.size))
    dup_incoming_row = dup_incoming_row [uniq_dup_idxes]
    dup_existing_row = dup_existing_row [uniq_dup_idxes]
  '''
  print ('dup_incoming_row:')
  print (dup_incoming_row)
  print ('dup_existing_row:')
  print (dup_existing_row)
  '''

  # Initialize to -1, to indicate no duplicates
  dup_idxes = np.zeros (incoming.shape [0],) - 1
  dup_idxes = dup_idxes.astype (int)
  # Mark rows corresponding to duplicate incoming chords
  dup_idxes [dup_incoming_row] = dup_existing_row

  return dup_idxes


def find_dups_self (samples, thresh=1e-6):

  # nSamples x 1
  dup_idxes = find_dups_multirow_onelist (samples, samples, thresh)

  assert (dup_idxes.size == samples.shape [0])

  # Assumption: dup_idxes.size == samples.shape [0]
  idxes = range (0, dup_idxes.size)

  # An item is always duplicate of itself. Don't count it as a duplicate,
  #   i.e. set to -1 to indicate not a duplicate
  dup_idxes [dup_idxes == idxes] = -1

  return dup_idxes


if __name__ == '__main__':
   existing = np.zeros((5,4))
   incoming = np.zeros((5,4))
   incoming[1,3] = 0.1;
   incoming[2,3] = 0.1
   find_dups_multirow_twolists(incoming, existing)
