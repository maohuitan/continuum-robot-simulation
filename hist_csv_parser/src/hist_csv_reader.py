#!/usr/bin/env python

# Huitan Mao
# 2 Jan 2017
#
# Save csv file names into a txt file
#

import os

def hist_csv_list ():
  root_dir = "/home/huitan/collaboration/train/chord_sampling/csv_gl_chords/"
  cat_dirs = ["apple","banana","bottle","bowl","cup","donut","hammer","mug","teapot","toilet_paper"]
  csv_files = []

  for cur_dir in cat_dirs:
    cur_cat_files = os.listdir(root_dir+cur_dir)
    
    local_csv_files = []
    for index, value in enumerate(cur_cat_files):
      if os.path.isfile(root_dir+cur_dir+"/"+value):
        this_file = cur_dir + "/" + value
        local_csv_files.append(this_file)
    
    csv_files.extend(local_csv_files)
  
  # print csv_files
  print('Number of csv files read:')
  print len(csv_files)

  # save to txt
  with open('wraps_summary', 'w') as f:
    for s in csv_files:
        f.write(s + '\n')

if __name__ == '__main__':
  hist_csv_list ()