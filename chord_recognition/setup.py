#!/usr/bin/env python

# Mabel Zhang
# 3 Sep 2015
#
# This file tells catkin to put src/util/*py python modules into
#   PYTHONPATH.
#
# Refs:
# Confusing tutorial because both Python files are in same ROS package:
#   http://wiki.ros.org/rospy_tutorials/Tutorials/Makefile
# Clears confusion as this person has Python files in 2 different ROS pkgs:
#   http://answers.ros.org/question/147391/importing-python-module-from-another-ros-package-setuppy/
# Not useful and repeats tutorial:
#   http://docs.ros.org/api/catkin/html/user_guide/setup_dot_py.html
#
# Troubleshoot:
#   If you can't import a module, check these essential steps are done:
#   1. You .py file is correctly placed under ./src/<package_name>/
#   2. You uncommented catkin_python_setup() in CMakeLists.txt
#   3. You ran "touch __init__.py" in ./src/<package_name>/ to generate an
#      empty __init__.py.
#

## ! DO NOT MANUALLY INVOKE THIS setup.py, USE catkin_python_setup in
##   CMakeLists.txt INSTEAD

from distutils.core import setup
from catkin_pkg.python_setup import generate_distutils_setup

# fetch values from package.xml
setup_args = generate_distutils_setup(
    packages=['chord_recognition'],
    package_dir={'': 'src'}
)

setup(**setup_args)

