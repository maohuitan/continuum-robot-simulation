#cube/cube_8cm.csv
#cube/cube_20cm.csv
#cylinder/cylinder_5x25cm_DxH.csv
#cylinder/cylinder_12x25cm_DxH.csv
#sphere/sphere_8cm.csv
#sphere/sphere_20cm.csv

# replicating for testing on training data for sanity checks, and because we
#   didn't make many different simple objects
#cube/cube_8cm.csv
#cube/cube_20cm.csv
#cylinder/cylinder_5x25cm_DxH.csv
#cylinder/cylinder_12x25cm_DxH.csv
#sphere/sphere_8cm.csv
#sphere/sphere_20cm.csv


#####
# Larger objects

cube/cube_20cm_scale1dot1.csv
cube/cube_20cm_scale1dot2.csv
cube/cube_20cm_scale1dot3.csv
cylinder/cylinder_12x25cm_DxH_scale2dot0.csv
cylinder/cylinder_12x25cm_DxH_scale2dot2.csv
cylinder/cylinder_12x25cm_DxH_scale2dot4.csv
sphere/sphere_20cm_scale1dot4.csv
sphere/sphere_20cm_scale1dot6.csv
sphere/sphere_20cm_scale1dot8.csv

# replicating for testing on training data for sanity checks, and because we
#   didn't make many different simple objects
cube/cube_20cm_scale1dot2.csv
cube/cube_20cm_scale1dot1.csv
cube/cube_20cm_scale1dot3.csv
cylinder/cylinder_12x25cm_DxH_scale2dot2.csv
cylinder/cylinder_12x25cm_DxH_scale2dot0.csv
cylinder/cylinder_12x25cm_DxH_scale2dot4.csv
sphere/sphere_20cm_scale1dot6.csv
sphere/sphere_20cm_scale1dot8.csv
sphere/sphere_20cm_scale1dot4.csv
