#!/usr/bin/env python

# Mabel Zhang
# 3 Dec 2016
#
# Copied and modified from my triangle_sampling/triangles_svm.py in reFlexHand
#   repo.
#
# Uses Scikit-learn to train and test data. Split of train and test data is
#   manually coded in this file.
#
# Input: histograms of chords, one histogram per object
# Output: trained SVM model, class prediction by SVM
#


# Python
import argparse
import os
import csv
import time
import pickle

# Numpy
import numpy as np
from sklearn import svm
from sklearn.metrics.pairwise import chi2_kernel  # For chisquare dist for hists
#from sklearn.cross_validation import train_test_split
from sklearn.externals import joblib  # For saving SVM model to file
from sklearn.decomposition import PCA

import matplotlib

# My packages
from util_continuum.config_paths import config_datapath, get_hist_path, \
  get_recog_meta_path, get_svm_model_path, get_nbins_subpath
from util_continuum.ansi_colors import ansi_colors
from util_continuum.classification_tools import draw_confusion_matrix, calc_accuracy
from chord_recognition.parse_args import parse_args_for_histogram, config_params_from_args
from chord_recognition.recognize_consts import SVMConsts

# Local
from histogram_reader import HistogramReader


# Because sklearn's train_test_split() is retarded and only splits the ratio
#   for overall data. It doesn't keep the ratio for each category. So when you
#   only have a few samples in a category, it can happen that you have 0
#   instances for testing!
# Parameters:
#   samples: NumPy array, nSamples x nDims
#   lbls: Python list of n integers
def my_train_test_split (samples_orig, lbls_orig, test_size, random_state):

  rs = np.random.RandomState ()
  rs.seed (random_state)


  #####
  # Re-shuffle samples first. Generate a permuted list
  #####

  shuffle_idx = rs.permutation (len (lbls_orig))

  nDims = np.shape (samples_orig) [1]
  samples = np.zeros ((0, nDims))
  lbls = []

  for i in range (0, len (shuffle_idx)):
    # Grab row indexed by shuffle_idx[i].
    #   Make it 2D array, to preserve the row shape to pass to np.append().
    samples = np.append (samples,
      np.array ([samples_orig [shuffle_idx [i], :]]), axis=0)
    lbls.append (lbls_orig [shuffle_idx [i]])

  # Now that the samples are shuffled, can assign train/test split in order,
  #   without randomization needed (randomization already applied at
  #   permutation above).
  #   The ordered assignment is easier to check ratio-per-category! Hard to do
  #   that while randomly assigning a split.
  

  #####
  # Assign train/test split
  #####

  samples_tr = np.zeros ((0, nDims))
  samples_te = np.zeros ((0, nDims))
  lbls_tr = []
  lbls_te = []

  TRAIN = 0
  TEST = 1

  # Number of labels
  lbls_uniq = np.unique (lbls)
  # Get total number of samples per class, to calculate percentage later.
  #   Use a hashmap (dictionary), instead of list, because someone's labels
  #   might not start with 0, so you don't want to use the labels as list
  #   index. unique() also returns sorted list, so you won't know which elt to
  #   index! Just use the actual label as a key, easier.
  ttl_per_label = dict ()
  n_tests_per_label = dict ()
  n_trains_per_label = dict ()
  for i in range (0, len (lbls_uniq)):
    # Total number of samples per class
    ttl_per_label [lbls_uniq [i]] = np.size (np.where (lbls == lbls_uniq [i]))

    # Init this dictionary. Number of samples assigned to TEST SET so far
    n_tests_per_label [lbls_uniq [i]] = 0
    n_trains_per_label [lbls_uniq [i]] = 0


  train_size = 1.0 - test_size

  # Assign train or test, keeping ratio satisfied
  for i in range (0, len (lbls)):

    # Prioritize test
    #if float (n_tests_per_label [lbls [i]]) / ttl_per_label [lbls [i]] < \
    #  test_size:
    #  assign = TEST
    #else:
    #  assign = TRAIN

    # Prioritize train, `.` train can't have 0 samples!
    if float (n_trains_per_label [lbls [i]]) / ttl_per_label [lbls [i]] < \
      train_size:
      assign = TRAIN
    else:
      assign = TEST

    # Add ith row in samples to the corresponding split
    if assign == TRAIN:
      samples_tr = np.append (samples_tr, np.array ([samples [i, :]]), axis=0)
      lbls_tr.append (lbls [i])

      n_trains_per_label [lbls [i]] += 1

    else:
      samples_te = np.append (samples_te, np.array ([samples [i, :]]), axis=0)
      lbls_te.append (lbls [i])

      n_tests_per_label [lbls [i]] += 1

  return samples_tr, samples_te, lbls_tr, lbls_te



def main ():

  # API https://docs.scipy.org/doc/numpy/reference/generated/numpy.set_printoptions.html
  np.set_printoptions (precision=3)


  #####
  # User adjust param
  #####

  # Show confusion matrix at the end. Disable this if you are running prune.py
  #   to generate accuracy plots from many different histogram bin and
  #   triangle param choices.
  show_plot_at_end = True


  # Debug tools flags

  # Show confusion matrix after every random split, for each of the 100 splits!
  #   Useful for detailed debugging.
  show_plot_every_split = False

  # Enable to enter test sample index to see 1d histogram intersectin plots
  interactive_debug = False

  # Print all histogram intersection distances
  debug_draw_nn_dists = False

  # To find the index of a specific object you are interested in seeing the
  #   histogram of. If it is a test object, it will print in cyan. Then you
  #   can use the interactive_debug=True mode to enter the index and see the
  #   3 flattened 1D histograms - they are saved to file too.
  debug_find_specific_obj = False
  specific_obj_startswith = '6ed884'

  # Set true_cat and pred_cat below, to find objects of a specific
  #   category that are predicted as another specific category. It will print
  #   in cyan.
  debug_find_specific_misclass = False
  specific_lbl = 'hammer'
  specific_wrong_lbl = 'bottle'

  # Only used for active sensing monte carlo tree search, in active_touch
  #   package. Only set to True when you are working with active_touch and are
  #   sure you want to retrain the model.
  # Set this flag to False at other times, so you don't mistakenly overwrite
  #   model file!! Model file is not committed to repo!
  # This will train SVM with probability=True, in order to output probs at
  #   test time.
  save_svm_to_file = True

  # Dimensionality reduction. Useful when data has too many features to run in
  #   real time.
  do_PCA = False
  PCA_GOAL_VARIANCE = 0.95

 
  #####
  # Parse command line args
  #####

  args, valid = parse_args_for_histogram ()
  if not valid:
    return

  mode = config_params_from_args (args)
  nbins_subpath, nbins = get_nbins_subpath (args.nbins)

  print ('%sBins settings: (%d, %d, %d)%s' % (
    ansi_colors.OKCYAN, nbins[0], nbins[1], nbins[2], ansi_colors.ENDC))


  #####
  # Parse args for each mode
  #####

  rand_splits = args.rand_splits

  # For ICRA PDF font compliance. No Type 3 font (rasterized) allowed
  #   Ref: http://phyletica.org/matplotlib-fonts/
  # You can do this in code, or edit matplotlibrc. But problem with matplotlibrc
  #   is that it's permanent. When you export EPS using TrueType (42), Mac OS X
  #   cannot convert to PDF. So you won't be able to view the file you
  #   outputted! Better to do it in code therefore.
  #   >>> import matplotlib
  #   >>> print matplotlib.matplotlib_fname()
  #   Ref: http://matplotlib.1069221.n5.nabble.com/Location-matplotlibrc-file-on-my-Mac-td24960.html
  if args.truetype:
    matplotlib.rcParams['pdf.fonttype'] = 42
    matplotlib.rcParams['ps.fonttype'] = 42

  draw_title = not args.notitle

  # For writing average accuracy at the end to a stats file
  #   Used by stats_hist_num_bins.py
  write_stats = args.write_stats
  overwrite_stats = args.overwrite_stats
  # If not even writing stats, overwrite doesn't make sense. Don't overwrite
  if overwrite_stats and not write_stats:
    print ('%s--overwrite_stats was specified, without specifying write_stats. That does not make sense, automatically setting overwrite_stats to False.%s' % (\
    ansi_colors.WARNING, ansi_colors.ENDC))

    overwrite_stats = False

  print ('%sSettings: rand_splits = %s. TrueType font = %s. Plot titles = %s %s' % \
    (ansi_colors.OKCYAN, rand_splits, args.truetype, draw_title, ansi_colors.ENDC))


  # Path to save confusion matrix image to
  img_path = config_datapath ('custom', 'chord_sampling/imgs/conf_mats/')
  img_name = os.path.join (img_path, 'svm_' + mode + '.eps')


  #####
  # Load objects data
  #####

  # Unused. Configured in HistogramReader
  hist_path = get_hist_path (mode, nbins_subpath)
  #print ('Loading descriptor data from %s' % hist_path)

  # one_meta_train_test condition from triangles_svm.py
  meta_name = os.path.join (get_recog_meta_path (), args.meta)

  # Read all histogram files
  reader_node = HistogramReader (mode, nbins_subpath, meta_name)

  # Big matrix of n x d samples to pass to SVM
  samples_raw = reader_node.get_samples ()
  lbls = reader_node.get_sample_cat_ids ()

  sample_names = reader_node.get_sample_names ()
  catnames = reader_node.get_cat_names ()
  catids = reader_node.get_cat_ids ()

  # Original version from traingles_svm.py, in case need some of these
  #   quantities later that are not implemented in HistogramReader yet.
  # Each row of samples is the histogram for one objects
  # lbls is numSamples size list with category integer label for each sample
  #[samples, lbls, catnames, catcounts, catids, sample_names] = load_hists ( \
  #  meta_name, hist_path) #, sampling_subpath=sampling_subpath,
  #  #tri_nbins_subpath=tri_nbins_subpath)

  n_samples = np.shape (samples_raw) [0]
  n_feats_raw = np.shape (samples_raw) [1]
  print ('%d samples, %d feats' % (n_samples, n_feats_raw))
  #print ('%d labels' % (len (lbls)))


  #####
  # PCA to reduce dimensionality
  # API: http://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html
  #   sklearn 0.16 does not have svd_solver param, API:
  #     http://scikit-learn.org/0.16/modules/generated/sklearn.decomposition.PCA.html#sklearn.decomposition.PCA
  #####

  # Do dimensionality reduction
  if do_PCA:

    # First pass. n_components=None, which sets it to n_samples when
    #   n_samples < n_components. Just to look at explained_variance_ratio_, for
    #   setting a better n_components to cover ~95% of variance in second pass.
    # Args: if n_components == 'mle' and svd_solver == 'full', then dimension is
    #   automatically guessed. Requires n_samples >= n_features.
    #   if 0 < n_components < 1, and svd_solver == 'full', then dimension is
    #   automatically selected such that variance > n_components.
    #   (For sklearn 0.16, don't need the svd_solver arg.)
    # sklearn 0.18 additionally has:
    #   svd_solver default is 'auto'. Others are 'full', 'arpack', 'randomized'
    pca = PCA ()
    #pca = PCA (n_components='mle', svd_solver='full')  # sklearn 0.18
    #pca = PCA (n_components='mle')  # sklearn 0.16
    samples = pca.fit (samples_raw)

    print ('PCA first-pass explained variance ratio, total %f:' % (
      np.sum (pca.explained_variance_ratio_)))
    print (pca.explained_variance_ratio_)


    # Second pass
    # Set using pca.explained_variance_ratio_. Only need to keep the first few
    #   principal axes that capture most of the information, up to 95% or so.
    #   np.cumsum returns cumulative sum, i.e. always in increasing order. So
    #   can simply take the first index that exceeds 95%, discard it and beyond.
    discard_beyond_idx = np.where (np.cumsum (pca.explained_variance_ratio_) \
      > PCA_GOAL_VARIANCE) [0] [0]
    # Take the element that reaches goal variance, inclusive (in case cumsum is
    #   barely reaching goal, and last elt is very large). Truncate so doesn't go
    #   out of bounds
    pca.n_components = min (discard_beyond_idx + 1, n_feats_raw)
    samples = pca.fit_transform (samples_raw)

    print ('%d dimensions reduced to %d, chosen to cover %f total variance' % (
      n_feats_raw, np.shape (samples) [1], PCA_GOAL_VARIANCE))
    print ('PCA second-pass explained variance ratio, total %f:' % (
      np.sum (pca.explained_variance_ratio_)))
    print (pca.explained_variance_ratio_)
    print ('')

  else:
    # Shallow copy
    samples = samples_raw


  #####
  # Main loop
  #####

  # Run once, no random
  if not rand_splits:
    n_random_splits = 1
    # Specify a fixed number, so no randomness
    random_state = 0

  # Run 10 times, random splits, get average accuracy
  else:
    n_random_splits = 10
    # Specify None, so random each call
    # Ref: http://stackoverflow.com/questions/28064634/random-state-pseudo-random-numberin-scikit-learn
    random_state = None

  accs = []


  # Seconds
  start_time = time.time ()

  for sp_i in range (0, n_random_splits):

    print ('%sRandom split %d%s' % (ansi_colors.OKCYAN, sp_i, ansi_colors.ENDC))

    #####
    # Split into train and test sets
    #   Ref: http://scikit-learn.org/stable/auto_examples/plot_confusion_matrix.html
    #####

    # lbls are Python lists
    # Use my_train_test_split(), to split satisfsying INDIVIDUAL-CATEGORY
    #   ratio. Split such that 0.5 per category is used for train, 0.5 per
    #   category is used for test. This is better if you have very few data,
    #   when sklearn's train_test_split() can end up giving you 0 items for
    #   test data, or 1 for train and 4 for test!
    # Otherwise, you can also call sklearn's train_test_split() many times,
    #   once for every category, and then sort out the indices yourself...
    samples_tr, samples_te, lbls_tr, lbls_te = my_train_test_split (
      samples, lbls,
      test_size=0.5, random_state=random_state)
   

    # Find the original indices of test samples, so caller can see what
    #   objects are misclassified, by tracing back to the input object.
    idx_te = []
    for l_i in range (0, len (lbls_te)):
      # http://stackoverflow.com/questions/25823608/find-matching-rows-in-2-dimensional-numpy-array
      # axis=1 specifies look for rows that are same
      idx_te.append (np.where ((samples == samples_te [l_i, :]).all (axis=1)) \
        [0] [0])

    # Debug output
    print ('Total train %d instances, test %d instances' % ( \
      np.size (lbls_tr), np.size (lbls_te)))
    # Print number of instances per category
    for c_i in range (0, len (catnames)):
      #print (np.where (np.array (lbls_tr) == catids [c_i]))
      #print (np.where (np.array (lbls_te) == catids [c_i]))
      n_tr = np.size (np.where (np.array (lbls_tr) == catids [c_i]))
      n_te = np.size (np.where (np.array (lbls_te) == catids [c_i]))

      print ('%s: train %d instances, test %d instances' % (catnames [c_i],
        n_tr, n_te))

      if n_te == 0:
        print ('Test set for category %s has size 0' % ( \
          catnames [c_i]))
 

    #####
    # Run classification and draw confusion matrix
    #   Ref: http://scikit-learn.org/stable/auto_examples/plot_confusion_matrix.html
    #####

    # Run classifier
    classifier = svm.SVC (kernel='linear')

    # Used for active sensing
    if save_svm_to_file:

      # Train SVM with probabilities
      #   http://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html
      classifier.probability = True
      # Refit with probabilities
      classifier_probs_model = classifier.fit (samples_tr, lbls_tr)
      # nSamples x nClasses
      probs_pred = classifier_probs_model.predict_proba (samples_te)

      svm_name, svm_lbls_name = get_svm_model_path (mode, nbins_subpath)

      print ('%sOutputting SVM model to %s%s' % (
        ansi_colors.OKCYAN, svm_name, ansi_colors.ENDC))
      print ('%sOutputting SVM labels to %s%s' % (
        ansi_colors.OKCYAN, svm_lbls_name, ansi_colors.ENDC))

      svm_dict = dict ()
      svm_dict [SVMConsts.JOBLIB_SVM] = classifier_probs_model
      # Save pca object to file, so test time can load it to do PCA before SVM
      if do_PCA:
        svm_dict [SVMConsts.JOBLIB_PCA_BOOL] = do_PCA
        svm_dict [SVMConsts.JOBLIB_PCA] = pca

      # Save trained model to file. joblib is more efficient for numerical
      #   arrays than pickle
      # Ref joblib: http://stackoverflow.com/questions/10592605/save-classifier-to-disk-in-scikit-learn
      #joblib.dump (classifier_probs_model, svm_name, compress=9)
      joblib.dump (svm_dict, svm_name, compress=9)

      # Save correspondence of {integer_label: class_name} to file, so
      #   reader of SVM model can interpret what classes do the predicted
      #   labels represent!
      svm_lbls = {catids[c_i] : catnames[c_i] for c_i in range (0, len (catids))}
      with open (svm_lbls_name, 'wb') as f:
        # HIGHEST_PROTOCOL is binary, good performance. 0 is text format,
        #   can use for debugging.
        pickle.dump (svm_lbls, f, pickle.HIGHEST_PROTOCOL)

      #print ('Probabilities predicted by SVM:')
      #print ('class1 class2 ...')
      #print (probs_pred)

    # end if save_svm_to_file
 

    classifier_model = classifier.fit (samples_tr, lbls_tr)
    lbls_pred = classifier_model.predict (samples_te)

    print ('Truths and Predictions for test data:')
    print (' id name     (truth cat): predicted cat')
    for i_i in range (0, len (idx_te)):

      sample_base = os.path.basename (sample_names [idx_te [i_i]])
      short_sample_name = sample_base [0 : min (len (sample_base), 8)]

      true_cat = catnames [lbls_te [i_i]]
      pred_cat = catnames [lbls_pred [i_i]]

      # To help me find a specific object to plot for paper figure
      if debug_find_specific_obj and specific_obj_startswith:
        if sample_base.startswith (specific_obj_startswith):
           #or sample_base.startswith ('109d55'):
          print ('%s%s is idx %d%s' % (ansi_colors.OKCYAN,
            sample_base, idx_te [i_i], ansi_colors.ENDC))

      if debug_find_specific_misclass:
        if true_cat == specific_lbl and pred_cat == specific_wrong_lbl:
          print ('%sObject idx %d is %s predicted as %s%s' % ( \
            ansi_colors.OKCYAN, idx_te [i_i], true_cat, pred_cat, ansi_colors.ENDC))

      # If want to print NN distance
      dist_str = ''

      # Most helpful debug info. TEMPORARILY commented out to generate
      #   nbins vs acc plots. Faster if don't print to screen!
      print ('%3d %s (truth %s): predicted %s%s' % (idx_te [i_i],
        short_sample_name, true_cat, pred_cat, dist_str))

 
    # Calculate accuracy
    accs.append (calc_accuracy (lbls_te, lbls_pred, len(catids)))


    # NOTE: Not tested for chords
    # Do this for every random split, for debugging, user can see conf
    #   mats for every test, not just last one.
    # Draw confusion matrix and save to file
    if show_plot_every_split:
      draw_confusion_matrix (lbls_te, lbls_pred, catnames, img_name=img_name,
        title_prefix='SVM ', draw_title=draw_title)

    # NOTE: Not tested for chords
    # Show 1d histogram for the sample that user chooses
    if interactive_debug:
      if args.mixed:
        interact_plot_hist (hist_path, sample_names_tr + sample_names_te,
          lbls_tr + lbls_te, catnames, plot_opt='1d')
      else:
        interact_plot_hist (hist_path, sample_names, lbls, catnames,
          plot_opt='1d')

    print ('')

  # end for sp_i = 0 : n_random_splits


  # Print out running time
  # Seconds
  end_time = time.time ()
  print ('Total time for %d random splits: %f seconds.' % \
    (n_random_splits, end_time - start_time))
  if n_random_splits != 0:
    print ('Average %f seconds per object.\n' % ( \
      (end_time - start_time) / n_random_splits))


  # NOTE: Not tested for chords
  # Draw confusion matrix and save to file (if there are multiple random
  #   splits, this is for the very last run)
  if show_plot_at_end:
    draw_confusion_matrix (lbls_te, lbls_pred, catnames, img_name=img_name,
      title_prefix='SVM ', draw_title=draw_title)

  avg_acc = np.mean (np.asarray (accs))

  if rand_splits:
    print ('%sAverage accuracy over %d runs of random splits: %f %s' % \
      (ansi_colors.OKCYAN, n_random_splits, avg_acc, ansi_colors.ENDC))


  # NOTE: Not tested for chords
  # Write to stats file, for plotting graph for paper
  if write_stats:
    write_stat (hist_parent_path, hist_path, overwrite_stats, accs, #avg_acc,
      img_mode_suffix)



  ##################################################
  ################## ISER2018 ######################
  #### read in test data and do classification ####
  ##################################################
  meta_name = os.path.join (get_recog_meta_path (), "models_gl_origami_test.txt")
  reader_node = HistogramReader (mode, nbins_subpath, meta_name)

  # Big matrix of n x d samples to pass to SVM
  samples_raw = reader_node.get_samples ()
  lbls = reader_node.get_sample_cat_ids ()

  sample_names = reader_node.get_sample_names ()
  catnames_test = reader_node.get_cat_names ()
  catids = reader_node.get_cat_ids ()

  n_samples = np.shape (samples_raw) [0]
  n_feats_raw = np.shape (samples_raw) [1]
  print ('%d samples, %d feats' % (n_samples, n_feats_raw))
  #print ('%d labels' % (len (lbls)))


  #####
  # PCA to reduce dimensionality
  # API: http://scikit-learn.org/stable/modules/generated/sklearn.decomposition.PCA.html
  #   sklearn 0.16 does not have svd_solver param, API:
  #     http://scikit-learn.org/0.16/modules/generated/sklearn.decomposition.PCA.html#sklearn.decomposition.PCA
  #####

  # Do dimensionality reduction
  if do_PCA:

    # First pass. n_components=None, which sets it to n_samples when
    #   n_samples < n_components. Just to look at explained_variance_ratio_, for
    #   setting a better n_components to cover ~95% of variance in second pass.
    # Args: if n_components == 'mle' and svd_solver == 'full', then dimension is
    #   automatically guessed. Requires n_samples >= n_features.
    #   if 0 < n_components < 1, and svd_solver == 'full', then dimension is
    #   automatically selected such that variance > n_components.
    #   (For sklearn 0.16, don't need the svd_solver arg.)
    # sklearn 0.18 additionally has:
    #   svd_solver default is 'auto'. Others are 'full', 'arpack', 'randomized'
    pca = PCA ()
    #pca = PCA (n_components='mle', svd_solver='full')  # sklearn 0.18
    #pca = PCA (n_components='mle')  # sklearn 0.16
    samples = pca.fit (samples_raw)

    print ('PCA first-pass explained variance ratio, total %f:' % (
      np.sum (pca.explained_variance_ratio_)))
    print (pca.explained_variance_ratio_)


    # Second pass
    # Set using pca.explained_variance_ratio_. Only need to keep the first few
    #   principal axes that capture most of the information, up to 95% or so.
    #   np.cumsum returns cumulative sum, i.e. always in increasing order. So
    #   can simply take the first index that exceeds 95%, discard it and beyond.
    discard_beyond_idx = np.where (np.cumsum (pca.explained_variance_ratio_) \
      > PCA_GOAL_VARIANCE) [0] [0]
    # Take the element that reaches goal variance, inclusive (in case cumsum is
    #   barely reaching goal, and last elt is very large). Truncate so doesn't go
    #   out of bounds
    pca.n_components = min (discard_beyond_idx + 1, n_feats_raw)
    samples = pca.fit_transform (samples_raw)

    print ('%d dimensions reduced to %d, chosen to cover %f total variance' % (
      n_feats_raw, np.shape (samples) [1], PCA_GOAL_VARIANCE))
    print ('PCA second-pass explained variance ratio, total %f:' % (
      np.sum (pca.explained_variance_ratio_)))
    print (pca.explained_variance_ratio_)
    print ('')

  else:
    # Shallow copy
    samples = samples_raw

  # predict with probabilities
  probs_pred = classifier_probs_model.predict_proba (samples)
  print ('Probabilities predicted by SVM:')
  print ('class1 class2 ...')
  print (probs_pred)

  # predicdt with labels
  lbls_pred = classifier_model.predict (samples)
  print ('Predictions:')
  for i in range (0, n_samples):
    print (catnames[lbls_pred[i]])

if __name__ == '__main__':
  main ()

