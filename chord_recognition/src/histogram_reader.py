#!/usr/bin/env python

# Mabel Zhang
# 9 Dec 2016
#
# Calls HistogramFromFile class in histogram_of_chords.py to read histogram
#   files.
#


import rospy

import os

import numpy as np

# My packages
from util_continuum.config_paths import get_recog_meta_path, get_nbins_subpath
from util_continuum.parse_meta import read_meta_file, get_meta_cat_name
from chord_recognition.chord_consts import ChordConsts
from chord_recognition.histogram_of_chords import HistogramConfig, HistogramFromFile
from chord_recognition.parse_args import parse_args_for_histogram, config_params_from_args

# Local


class HistogramReader:

  # Parameters:
  #   mode: 'rand', 'gl', or 'real'
  #   meta_name: Full path to meta file
  def __init__ (self, mode, nbins_subpath, meta_name):

    self.mode = mode
    self.nbins_subpath = nbins_subpath
    self.meta_name = meta_name

    self.consts = ChordConsts ()

    # Read hist_conf.yaml
    self.config = HistogramConfig (self.mode, self.nbins_subpath, self.consts)

    # Read the histograms files
    self.hists_linear, self.obj_bases, self.cat_names, self.cat_ids, \
      self.obj_cat_ids = self.read_hists ()


  def read_hists (self):

    # Create an empty numpy array of 0 x histogram linear size
    hists_linear = np.zeros ((0, np.product (self.config.get_n_bins ())))
    # n_objs items
    obj_bases = []
    obj_cat_ids = []

    # Lists of n_cats items
    cat_names = []
    cat_ids = []

    # Read meta file
    lines = read_meta_file (self.meta_name)

    for line in lines:

      obj_cat, obj_base = get_meta_cat_name (line)

      obj_bases.append (obj_base)

      # Save in the short list, indexed by integers, instead of saving a long
      #   list of strings. This speeds up access to class names.
      if obj_cat not in cat_names:
        cat_names.append (obj_cat)
        cat_ids.append (len (cat_ids))
      obj_cat_ids.append (cat_names.index (obj_cat))

      # Read histogram file
      hist_node = HistogramFromFile (obj_base, obj_cat, self.mode,
        self.nbins_subpath, self.consts, self.config)
      curr_hist_linear = hist_node.get_hist_linear ()

      hists_linear = np.append (hists_linear, curr_hist_linear, axis=0)

    print ('')
    print ('HistogramReader debug:')

    print ('Shape of all hists loaded into big matrix:')
    print (hists_linear.shape)

    print ('Categories:')
    print (cat_names)
    print ('Category IDs:')
    print (cat_ids)
    print ('Objects'' category IDs:')
    print (obj_cat_ids)
    print ('')

    return hists_linear, obj_bases, cat_names, cat_ids, obj_cat_ids


  # ==================================== Interface functions for recognition ==

  def get_samples (self):
    return self.hists_linear

  def get_sample_names (self):
    return self.obj_bases

  def get_cat_names (self):
    return self.cat_names

  def get_cat_ids (self):
    return self.cat_ids

  def get_sample_cat_ids (self):
    return self.obj_cat_ids


def main ():

  rospy.init_node ('histogram_reader', anonymous=True)


  #####
  # Parse command line args
  #####

  args, valid = parse_args_for_histogram ()
  if not valid:
    return

  mode = config_params_from_args (args)
  nbins_subpath, _ = get_nbins_subpath (args.nbins)

  print ('%sBins settings: (%d, %d, %d)%s' % (
    ansi_colors.OKCYAN, nbins[0], nbins[1], nbins[2], ansi_colors.ENDC))


  meta_name = os.path.join (get_recog_meta_path (), args.meta)

  reader_node = HistogramReader (mode, nbins_subpath, meta_name)


if __name__ == '__main__':
  main ()

