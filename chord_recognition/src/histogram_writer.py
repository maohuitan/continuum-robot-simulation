#!/usr/bin/env python

# Mabel Zhang
# 5 Dec 2016
#
# Reads the supplied list of chord files, and for each chord file, writes a
#   histogram file by calling HistogramFromChord class in
#   histogram_of_chords.py.
#

# ROS
import rospy

# Python
import os
import sys  # For sys.maxint

import numpy as np

# My packages
from util_continuum.parse_meta import read_meta_file, get_meta_cat_name
from util_continuum.config_paths import get_recog_meta_path, get_nbins_subpath
from util_continuum.ansi_colors import ansi_colors
from chord_recognition.chord_consts import ChordConsts
from chord_recognition.chord_reader import ChordReader
from chord_recognition.histogram_of_chords import HistogramFromChord
from chord_recognition.parse_args import parse_args_for_histogram, config_params_from_args

# Local


class HistogramWriter:

  # Parameters:
  #   meta_name: Full path to meta file
  def __init__ (self, mode, nbins_subpath, nbins, meta_name):

    self.mode = mode
    self.nbins_subpath = nbins_subpath
    self.nbins = nbins
    self.consts = ChordConsts ()

    # Read meta file
    self.lines = read_meta_file (meta_name)

    # Populated by find_bin_ranges()
    self.obj_bases = []
    self.obj_cats = []

    # Indicates write_hist_config() hasn't been called
    self.config_written = False


  # First pass. Find min/max bin ranges for histogram. It is simply the min
  #   and max of each chord parameter.
  # Input: lines
  # Output: bin_mins, bin_maxes, bin_range;
  # Outputs for speedy second pass: obj_bases, obj_cats
  def find_bin_ranges (self):

    print ('Running first pass over meta file, to find min/max bin ranges of all chords...')

    # 7 x 1 vector
    # Init min to a large number, max to a small number, so that inequalities
    #   can find values smaller/larger than this initial value!
    # Ref sys.maxint: http://stackoverflow.com/questions/7604966/maximum-and-minimum-values-for-ints
    bin_mins = np.ones (self.consts.N_PARAMS) * sys.maxint
    bin_maxes = np.zeros (self.consts.N_PARAMS)
 
    wait_rate = rospy.Rate (10)

    # Loop through the list of chord files. For each chord file, record the
    #   min and max values for each chord parameter.
    for line in self.lines:
 
      obj_cat, obj_base = get_meta_cat_name (line)
      chord_node = ChordReader (obj_base, obj_cat, self.mode, self.consts)
 
      # Save to list, for running 2nd pass with easy access to values
      self.obj_bases.append (obj_base)
      self.obj_cats.append (obj_cat)
 
      # Update min and max lists
      # Ref: element-wise min https://docs.scipy.org/doc/numpy-1.10.1/reference/generated/numpy.minimum.html
      bin_mins = np.minimum (chord_node.mins, bin_mins)
      bin_maxes = np.maximum (chord_node.maxes, bin_maxes)
 
      try:
        wait_rate.sleep ()
      except rospy.exceptions.ROSInterruptException, err:
        break

      print ('')
 
    # 7 x 2 NumPy array
    bin_range = np.array ([bin_mins, bin_maxes]).T

    return bin_range


  def write_hists (self, bin_range):

    print ('Running second pass to actually write histograms to file...')

    wait_rate = rospy.Rate (10)

    # Second pass
    # Loop through the list of chord files. For each chord file, write a
    #   histogram file
    # Input: bin_range
    # Output: histograms written to files
    for i in range (0, len (self.obj_bases)):
 
      obj_base = self.obj_bases [i]
      obj_cat = self.obj_cats [i]
      hist_node = HistogramFromChord (obj_base, obj_cat, self.mode,
        self.nbins_subpath, bin_range,
        n_bins_l=self.nbins[0], n_bins_ang=self.nbins[1:3], consts=self.consts)
        #n_bins_l=HistConsts.N_BINS_L, n_bins_ang=HistConsts.N_BINS_ANG, consts=self.consts)

      # Write hist_conf.csv
      if not self.config_written:
        self.config_written = hist_node.write_hist_config ()
 
      hist_node.write_to_file (self.config_written)

      try:
        wait_rate.sleep ()
      except rospy.exceptions.ROSInterruptException, err:
        break

      print ('')
 


def main ():

  rospy.init_node ('histogram_writer', anonymous=True)


  #####
  # Parse command line args
  #####

  args, valid = parse_args_for_histogram ()
  if not valid:
    return

  mode = config_params_from_args (args)
  nbins_subpath, nbins = get_nbins_subpath (args.nbins)

  print ('%sBins settings: (%d, %d, %d)%s' % (
    ansi_colors.OKCYAN, nbins[0], nbins[1], nbins[2], ansi_colors.ENDC))


  meta_name = os.path.join (get_recog_meta_path (), args.meta)

  writer_node = HistogramWriter (mode, nbins_subpath, nbins, meta_name)

  # First pass. Find bin ranges
  bin_range = writer_node.find_bin_ranges ()
  # Second pass, write the histograms to files
  writer_node.write_hists (bin_range)


if __name__ == '__main__':
  main ()

