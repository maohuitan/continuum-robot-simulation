#!/usr/bin/env python

# Mabel Zhang
# 30 Nov 2016
#
# Tests chord_reader.py and chord_writer.py with randomly generated data.
#

# ROS
import rospy
import tf
from std_msgs.msg import Bool
from geometry_msgs.msg import Point # Vector3, Quaternion

# Python
import random
import time
import signal

import numpy as np

# My packages
from chord_training_msgs.msg import Chord
from chord_training_msgs.srv import ChordSrv, ChordSrvRequest


def gen_random_chord ():

  chord = Chord ()
  chord.seq = i

  chord.l = random.random () * (MAX_CHORD_LEN - MIN_CHORD_LEN) + MIN_CHORD_LEN

  # Generate a valid rotation
  chord.c_lon = random.random () * 2.0 * np.pi
  chord.c_lat = random.random () * np.pi

  chord.n0_lon = random.random () * 2.0 * np.pi
  chord.n0_lat = random.random () * np.pi

  chord.n1_lon = random.random () * 2.0 * np.pi
  chord.n1_lat = random.random () * np.pi



  # Old RPY definition of chord. Remove when new definition is settled
  '''
  # Generate a valid rotation
  rand_R = random.random () * np.pi
  rand_P = random.random () * np.pi
  rand_Y = random.random () * np.pi
  chord.c = Point (rand_R, rand_P, rand_Y)

  chord.n0 = Point (random.random () * np.pi,
    random.random () * np.pi,
    random.random () * np.pi)

  chord.n1 = Point (random.random () * np.pi,
    random.random () * np.pi,
    random.random () * np.pi)
  '''

  # Old Chord.msg format, with quaternion. Remove when new definition is
  #   settled.
  '''
  # Generate a valid quaternion
  # Multiply rotations in xyz axes sequentially
  #   In C++, would simply do tf::Quaternion::setRPY(). Bullet types don't
  #     exist in Python. One way is to compute matrices yourself, another is
  #     to use tf.transformations.py.
  quat1 = tf.transformations.quaternion_about_axis (rand_R, (1, 0, 0))
  quat2 = tf.transformations.quaternion_about_axis (rand_P, (0, 1, 0))
  quat3 = tf.transformations.quaternion_about_axis (rand_Y, (0, 0, 1))
  # Multiply the quaternions
  quat = tf.transformations.quaternion_multiply (quat1, quat2)
  quat = tf.transformations.quaternion_multiply (quat, quat3)
  chord.q = Quaternion (quat[0], quat[1], quat[2], quat[3])

  # Normalize so that the vector is valid
  rand_n = [random.random (), random.random (), random.random ()]
  rand_n /= np.linalg.norm (rand_n)
  chord.n0 = Vector3 (rand_n[0], rand_n[1], rand_n[2])

  rand_n = [random.random (), random.random (), random.random ()]
  rand_n /= np.linalg.norm (rand_n)
  chord.n1 = Vector3 (rand_n[0], rand_n[1], rand_n[2])
  '''

  return chord



def main ():

  # Set disable_signals=True to use my own Ctrl+C signal handler, so that
  #   I can sending finish signal to file writer!
  #   Note there will be no rospy.exceptions to catch Ctrl+C, need to use
  #     KeyboardInterrupt.
  #   Tested in test_shutdown.py in triangle_sampling pkg in reFlexHand repo.
  rospy.init_node ('chord_io_tester', anonymous=True, disable_signals=True)
  # Connect custom SIGTERM handler
  signal.signal (signal.SIGTERM, sigterm_handler)

  n_chords = 100 # 3

  # Meters
  MIN_CHORD_LEN = 0.0
  MAX_CHORD_LEN = 0.3

  ROSMSG = 0
  ROSSRV = 1
  TEST_MODE = ROSSRV

  # rosmsg alternative
  chord_pub = rospy.Publisher ('/chord_training/chord', Chord, queue_size=5)
  finished_pub = rospy.Publisher ('/chord_training/finished', Bool,
    queue_size=1)

  # rossrv alternative
  if TEST_MODE == ROSSRV:
    print ('Waiting for rosservice to come up...')
    try:
      rospy.wait_for_service ('/chord_training/chord_srv')
    except rospy.exceptions.ROSInterruptException, err:
      return
    except KeyboardInterrupt:
      return

  # Need to pause, else first message never received on rostopic. Weird
  time.sleep (0.5)

  i = 0
  ended_unexpectedly = True


  # Generate random chords (l, q, n0, n1)
  while not rospy.is_shutdown ():

    chord = gen_random_chord ()


    # Publish the chord to ChordWriter
    # rosmsg alternative
    if TEST_MODE == ROSMSG:
      chord_pub.publish (chord)

    # rossrv alternative
    elif TEST_MODE == ROSSRV:
      try:
        # Recreate a service each time, instead of using persistent connection,
        #   `.` if server restarts, former lets you reconnect to new server
        #   automatically!
        # Ref: http://wiki.ros.org/rospy/Overview/Services
        rospy.wait_for_service ('/chord_training/chord_srv')
        chord_srv = rospy.ServiceProxy ('/chord_training/chord_srv', ChordSrv)
        # Last chord
        finished = (i == (n_chords - 1))
        chord_resp = chord_srv (True, chord, finished)

        # Last service call was sent, don't need to send a separate one just to
        #   signal finish
        if finished:
          ended_unexpectedly = False
      except rospy.ServiceException, e:
        print 'rosservice call to /chord_training/chord_srv FAILED: %s' % e
      # Ctrl+C
      except rospy.exceptions.ROSInterruptException, err:
        pass
      except KeyboardInterrupt:
        pass

    print ('Published chord %d. l: %f' % (i, chord.l))


    try:
      time.sleep (0.3)
    except rospy.exceptions.ROSInterruptException, err:
      break
    # When using my custom signal handler, won't have rospy.exceptions error
    except KeyboardInterrupt:
      break

    i += 1
    if i >= n_chords:
      break

  #print ('ended_unexpectedly: %s' % ended_unexpectedly)

  # Custom signal handler, to indicate to save file. Must put it in a separate
  #   fn than main(), `.` SIGTERM gets sent to main() and still kills rossrv
  #   call!
  sigint_shutdown (TEST_MODE, ROSMSG, ended_unexpectedly)


# Do nothing, allow rosmsg and rossrv to finish being sent to subscriber
# Ref: docs.python.org/2/library/signal.html
def sigterm_handler (signal_number, stack_frame):
  return


# Custom SIGTERM signal handler
def sigint_shutdown (TEST_MODE, ROSMSG, ended_unexpectedly):

  if TEST_MODE == ROSMSG:
    finished_pub = rospy.Publisher ('/chord_training/finished', Bool,
      queue_size=1)
    print ('Publishing 10 msgs to indicate termination...')
    for i in range (0, 10):
      finished_pub.publish (Bool (True))
      time.sleep (0.1)

  # Service doesn't receive rostopic at the same time rosservice is running,
  #   so can't just publish to rostopic "finished"
  else:

    if ended_unexpectedly:
      print ('Ended unexpectedly. Calling with final finished service call.')
      try:
        rospy.wait_for_service ('/chord_training/chord_srv')
        chord_srv = rospy.ServiceProxy ('/chord_training/chord_srv', ChordSrv)
        finished = True
        chord_resp = chord_srv (False, None, finished)
      except rospy.ServiceException, e:
        print 'rosservice call to /chord_training/chord_srv FAILED: %s' % e

  # Manually tell ROS to cleanup properly
  # This line is required when using custom Ctrl+C signal handler (via
  #   siable_signals=True to init_node())
  rospy.signal_shutdown ('Normal shutdown by custom sigterm handling')


if __name__ == '__main__':
  main ()

