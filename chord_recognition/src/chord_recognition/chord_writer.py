#!/usr/bin/env python

# Mabel Zhang
# 29 Nov 2016
#
# Writes chords to .csv files
#

# ROS
import rospy
from std_msgs.msg import Bool

# Python
import os
import csv
import time
import argparse

# My packages
from util_continuum.config_paths import get_chord_path
from util_continuum.ansi_colors import ansi_colors
from chord_training_msgs.msg import Chord, TrainData
from chord_training_msgs.srv import ChordSrv, ChordSrvResponse
from chord_recognition.chord_consts import ChordConsts
from chord_recognition.parse_args import parse_args_for_histogram, config_params_from_args

# Local


# Supress detailed debug prinouts
SUPRESS = True


class ChordWriter:

  # Parameters:
  #   chord_name: Full path to chord file
  def __init__ (self, chord_name):

    # Column titles for csv file
    self.consts = ChordConsts ()
    self.column_titles = self.consts.get_column_titles ()

    self.chord_name = chord_name
    # http://stackoverflow.com/questions/273192/in-python-check-if-a-directory-exists-and-create-it-if-necessary
    csv_path = os.path.dirname (self.chord_name)
    if not os.path.exists (csv_path):
      os.makedirs (csv_path)

    # List of chords, chord_training_msgs/Chord[]
    self.chords = []

    # Publisher should choose either or, not both!! Else result isn't handled
    rospy.Subscriber ('/chord_training/chord', Chord, self.chord_cb)
    rospy.Subscriber ('/chord_training/train_data', TrainData,
      self.train_data_cb)

    rospy.Subscriber ('/chord_training/finished', Bool, self.finished_cb)
    self.doTerminate = False

    # Publisher should choose either this rosservice or a rosmsg above, not
    #   more than one!! Else result isn't handled
    # Ret val must be stored in scope, else service can stop receiving callbacks
    self.chord_srv = rospy.Service ('/chord_training/chord_srv', ChordSrv,
      self.handle_chord_srv)


  # Alternative 1: rostopic. Advantage: quicker than rossrv.
  #   Disadvantage: packets may be ignored if this script starts later than
  #   publisher.
  # Parameters:
  #   msg: chord_training_msgs/Chord
  def chord_cb (self, msg):

    if not SUPRESS:
      print ('Received rostopic chord %d. l: %f' % (msg.seq, msg.l))

    # Store to member field
    self.chords.append (msg)


  # Not tested.
  # Alternative 2: one-time data
  # A single set of training data, to write all to a single csv file at once
  # Parameters:
  #   msg: chord_training_msgs/TrainData
  def train_data_cb (self, msg):

    self.chords = msg


  # Used for Alternatives 1, 2
  def finished_cb (self, msg):

    print ('Received finished msg')

    if msg.data == True:
      self.doTerminate = True


  # Alternative 3: rosservice. Advantage: calls won't be ignored like rosmsg.
  #   Blocks until return (should be... need to test to make sure, rossrv
  #   sometimes doesn't block). Disadvantage: slower than rosmsg.
  def handle_chord_srv (self, req):

    # Store to member field
    if req.valid:
      if not SUPRESS:
        print ('Received rossrv chord %d. l: %f' % (req.chord.seq, req.chord.l))
      self.chords.append (req.chord)

    if req.finished == True:
      print ('Received finished srv call')
      self.doTerminate = True

    # Test if rosservice blocks on caller. Test succeeded.
    #time.sleep (1)

    return ChordSrvResponse (True)


  # Write a single chord to a row in the CSV file
  def write_row (self, chord):

    row = dict ()

    # float32
    row.update ({self.column_titles ['L_STR']: chord.l})

    # float32
    row.update ({self.column_titles ['C_LON']: chord.c_lon})
    row.update ({self.column_titles ['C_LAT']: chord.c_lat})

    row.update ({self.column_titles ['N0_LON']: chord.n0_lon})
    row.update ({self.column_titles ['N0_LAT']: chord.n0_lat})

    row.update ({self.column_titles ['N1_LON']: chord.n1_lon})
    row.update ({self.column_titles ['N1_LAT']: chord.n1_lat})


    ## Old RPY format. Remove when new definition is settled
    '''
    # geometry_msgs/Point
    row.update ({self.column_titles ['CR_STR']: chord.c.x})
    row.update ({self.column_titles ['CP_STR']: chord.c.y})
    row.update ({self.column_titles ['CY_STR']: chord.c.z})

    # geometry_msgs/Point
    row.update ({self.column_titles ['NR0_STR']: chord.n0.x})
    row.update ({self.column_titles ['NP0_STR']: chord.n0.y})
    row.update ({self.column_titles ['NY0_STR']: chord.n0.z})

    # geometry_msgs/Point
    row.update ({self.column_titles ['NR1_STR']: chord.n1.x})
    row.update ({self.column_titles ['NP1_STR']: chord.n1.y})
    row.update ({self.column_titles ['NY1_STR']: chord.n1.z})
    '''

    ## Old format. Remove when new definition is settled
    '''
    # geometry_msgs/Quaternion
    row.update ({self.column_titles ['QX_STR']: chord.q.x})
    row.update ({self.column_titles ['QY_STR']: chord.q.y})
    row.update ({self.column_titles ['QZ_STR']: chord.q.z})
    row.update ({self.column_titles ['QW_STR']: chord.q.w})

    # geometry_msgs/Vector3
    row.update ({self.column_titles ['NX0_STR']: chord.n0.x})
    row.update ({self.column_titles ['NY0_STR']: chord.n0.y})
    row.update ({self.column_titles ['NZ0_STR']: chord.n0.z})

    # geometry_msgs/Vector3
    row.update ({self.column_titles ['NX1_STR']: chord.n1.x})
    row.update ({self.column_titles ['NY1_STR']: chord.n1.y})
    row.update ({self.column_titles ['NZ1_STR']: chord.n1.z})
    '''

    self.csv_writer.writerow (row)


  def write_file (self):

    # Create file
    self.csv_file = open (self.chord_name, 'wb')

    print ('Writing chords to %s' % self.chord_name)

    # Write column headers
    self.csv_writer = csv.DictWriter (self.csv_file,
      fieldnames=self.column_titles.values (), restval='-1')
    self.csv_writer.writeheader ()

    # Write one chord (row) at a time
    for i in range (0, len (self.chords)):
      if not SUPRESS:
        print ('Wrote a chord. l: %f' % self.chords [i].l)
      self.write_row (self.chords [i])

    self.csv_file.close ()

    print ('Chords written to %s' % self.chord_name)


  def get_terminate (self):

    return self.doTerminate



def main ():

  rospy.init_node ('chord_writer', anonymous=True)

  print ('%schord_writer.py: Detailed debug printouts are SUPRESSed (constant SUPRESS=True).%s' % (
    ansi_colors.OKCYAN, ansi_colors.ENDC))


  #####
  # Parse command line args
  #####

  arg_parser = argparse.ArgumentParser ()

  args, valid = parse_args_for_histogram (arg_parser, chord_opt=True)
  if not valid:
    return

  if not args.obj_base or not args.obj_cat:
    print ('--obj_base or --obj_cat was not specified. Please specify both, and try again.')
    return

  mode = config_params_from_args (args)


  obj_cat = args.obj_cat
  obj_base = args.obj_base
  train_name = os.path.join (get_chord_path (mode), obj_cat, obj_base + '.csv')
  this_node = ChordWriter (train_name)


  wait_rate = rospy.Rate (10)

  print ('Listening for incoming chord messages or service call...')
  while not rospy.is_shutdown ():

    # An indicator from publisher of chords, so can write file when all chords
    #   are received.
    if this_node.get_terminate ():

      # Shut down the service, so control returns to main() to write the file.
      #   Else file is never written!! Tried.
      # Ref: http://wiki.ros.org/rospy/Overview/Services
      this_node.chord_srv.shutdown ('Normal shutdown. Received finished srv call')

      this_node.write_file ()
      break
 
    try:
      wait_rate.sleep ()
    except rospy.exceptions.ROSInterruptException, err:
      break


if __name__ == '__main__':
  main ()

