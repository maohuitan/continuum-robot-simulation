#!/usr/bin/env python

# Mabel Zhang
# 29 Nov 2016
#
# Reads chords from .csv files
#

# ROS
import rospkg

# Python
import os

import numpy as np

import yaml

# My packages
from util_continuum.yaml_util import yaml_ordered_load


class ChordConsts:

  # Number of parameters to represent each chord. This will be the number of
  #   dimensions of chord histogram.
  N_PARAMS = 7

  L_STR_IDX = 0
  C_LON_IDX = 1
  C_LAT_IDX = 2
  N0_LON_IDX = 3
  N0_LAT_IDX = 4
  N1_LON_IDX = 5
  N1_LAT_IDX = 6


  def __init__ (self):

    # Load column titles for CSV file

    rospack = rospkg.RosPack ()
    pkg_path = rospack.get_path ('chord_recognition')
    yaml_name = os.path.join (pkg_path, 'yaml', 'chord_constants.yaml')
 
    yaml_content = {}
    with open (yaml_name, 'rb') as yaml_file:
 
      # Load as a dictionary
      # Ref: http://pyyaml.org/wiki/PyYAMLDocumentation
      #yaml_content = yaml.load (yaml_file)

      # Load as an ordered dictionary
      yaml_content = yaml_ordered_load (yaml_file, yaml.SafeLoader)
 
    #self.L_STR = yaml_content ['L_STR']
    #self.Q_STR = yaml_content ['Q_STR']
    #self.N0_STR = yaml_content ['N0_STR']
    #self.N1_STR = yaml_content ['N1_STR']
    #self.column_titles = [self.L_STR, self.Q_STR, self.N0_STR, self.N1_STR]

    # Dictionary, its .values() is:
    # ['l', 'qx', 'qy', 'qz', 'qw', 'nx0', 'ny0', 'nz0', 'nx1', 'ny1', 'nz1']
    self.column_titles = yaml_content ['chord_csv_columns']

    print ('ChordConsts: csv column titles will be: ')
    print (self.column_titles.values ())


  def get_column_titles (self):

    return self.column_titles


# NOTE: Change this if you change order of data in Chord.msg!
def get_discretization_widths (disc_len, disc_ang):

  # This is written for [l, c_lon, c_lat, n0_lon, n0_lat, n1_lon, n1_lat]
  return np.array ([disc_len, disc_ang, disc_ang, disc_ang, disc_ang, disc_ang,
    disc_ang])


