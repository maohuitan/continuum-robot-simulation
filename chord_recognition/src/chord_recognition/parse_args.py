#!/usr/bin/env python

# Mabel Zhang
# 5 Dec 2016
#
# Copied and adapted from triangle_sampling config_paths.py
#


import argparse


# Copied and modified from triangle_sampling config_paths.py parse_args_for_svm()
# Returns the parsed args, and boolean for whether parse was valid, i.e. we
#   did not encounter errors.
def parse_args_for_histogram (arg_parser=None, chord_opt=False):

  #####
  # Parse command line args
  #   Ref: Tutorial https://docs.python.org/2/howto/argparse.html
  #        Full API https://docs.python.org/dev/library/argparse.html
  #####

  if not arg_parser:
    arg_parser = argparse.ArgumentParser ()

  # Add args for individual objects
  if chord_opt:
    # Test with 'test' or whatever
    arg_parser.add_argument ('--obj_base', type=str, default='',
      help='String without spaces. Unique object name. Used as name of chord and histogram .csv file.')
    # Test with 'dummy_cat' or whatever
    arg_parser.add_argument ('--obj_cat', type=str, default='',
      help='String without spaces. Object category. Used as name of folder to hold .csv files.')

  arg_parser.add_argument ('--gl', action='store_true', default=False,
    help='Boolean flag, no args. Run on OpenGL simulated data.')
  arg_parser.add_argument ('--real', action='store_true', default=False,
    help=format ('Boolean flag, no args. Run on real-robot data.'))
  arg_parser.add_argument ('--rand', action='store_true', default=False,
    help=format ('Boolean flag, no args. Run on random data.'))

  arg_parser.add_argument ('--nbins', type=str, default='7,7,7',
    help='Number of histogram bins for (length, theta, phi). E.g. 7,6,6. Used to determine data path.')

  arg_parser.add_argument ('--meta', type=str, default='models.txt',
    help='String. Base name of meta list file in chord_recognition/config directory')

  # Set to True to upload to ICRA. (You can't view the plot in OS X Preview)
  # Set to False if want to see the plot for debugging.
  arg_parser.add_argument ('--truetype', action='store_true', default=False,
    help='Tell matplotlib to generate TrueType 42 font, instead of rasterized Type 3 font. Specify this flag for uploading to ICRA.')
  arg_parser.add_argument ('--notitle', action='store_true', default=False,
    help='Do not plot titles, for paper figures, description should all be in caption.')


  # This is only used for recognize_svm.py

  arg_parser.add_argument ('--rand_splits', action='store_true', default=False,
    help='')

  arg_parser.add_argument ('--write_stats', action='store_true', default=False,
    help='Write average accuracy to csv file.')
  arg_parser.add_argument ('--overwrite_stats', action='store_true',
    default=False, help='Overwrite accuracy file. Only in use if --write_stats is also specified.')

  args = arg_parser.parse_args ()

  if not (args.gl or args.real or args.rand):
    print ('One of three data input modes (--gl, --real, --rand) must be specified! Try again')
    return args, False

  return args, True


def config_params_from_args (args):

  if args.real:
    mode = 'real'
  elif args.gl:
    mode = 'gl'
  elif args.rand:
    mode = 'rand'

  return mode


# Parameters:
#   bins3D: List of comma-separated integers, no spaces, e.g. '10,10,10'
# Returns a list of integers
def get_ints_from_comma_string (commaStr):

  tokens = commaStr.split (',')
  # Strip any spaces
  tokens = [t.strip() for t in tokens]
  # Store the list of 3 integers, for returning
  ilist = [int(t) for t in tokens]

  return ilist


# A more generic version of get_ints_from_comma_string(), can be used for
#   whatever.
# Used by plot_hist_rviz.py to take user args to shift RViz plots by some amt.
# Parameters:
#   bins3D: List of comma-separated floats, no spaces, e.g. '0.1,0.2,0.3'
# Returns a list of floats
def get_floats_from_comma_string (commaStr):

  tokens = commaStr.split (',')
  # Strip any spaces
  tokens = [t.strip() for t in tokens]
  # Store the list of floats, for returning
  flist = [float(t) for t in tokens]

  return flist


