#!/usr/bin/env python

# Mabel Zhang
# 6 Feb 2017
#
# Used by recognize_svm.py (training time) and active_chord classifier.py
#   (test time).
#


class SVMConsts:

  # Dictionary keys in joblib file
  JOBLIB_SVM = 'svm'

  JOBLIB_PCA_BOOL = 'do_PCA'
  JOBLIB_PCA = 'pca'

