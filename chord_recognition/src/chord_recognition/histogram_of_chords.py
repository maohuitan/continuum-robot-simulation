#!/usr/bin/env python

# Mabel Zhang
# 29 Nov 2016
#
# Histogram of chords. A descriptor for object recognition.
# A chord is a segment connecting two points on a curve.
#


# ROS
import rospy
import tf
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import Point
from std_msgs.msg import ColorRGBA

# Python
import os
import csv
from abc import ABCMeta
import argparse

import numpy as np

import yaml

# import matplotlib, then using matplotlib.cm doesn't work on MacBook Air
#   So use from matplotlib import cm.
from matplotlib import cm

# My packages
from util_continuum.config_paths import get_chord_path, get_hist_path, \
  get_recog_meta_path, get_nbins_subpath
from util_continuum.parse_meta import read_meta_file, get_meta_cat_name
from util_continuum.create_marker import create_marker
from util_continuum.ansi_colors import ansi_colors
from util_continuum.yaml_util import UnsortableOrderedDict
from chord_recognition.chord_consts import ChordConsts
from chord_recognition.chord_reader import ChordReader
from chord_recognition.parse_args import parse_args_for_histogram, config_params_from_args

# Local


class HistogramConfig:

  # Init everything to None, to indicate read_hist_config() hasn't been called.
  n_bins_l = None
  n_bins_ang = None
  n_bins = None
  bin_ranges = None


  def __init__ (self, mode, nbins_subpath, consts=None):

    self.mode = mode
    self.nbins_subpath = nbins_subpath

    if not consts:
      self.consts = ChordConsts ()
    else:
      self.consts = consts
    self.params = self.consts.get_column_titles ()

    # Populate this object, by reading the hist_conf.yaml file saved by
    #   histogram_writer.py.
    self.n_bins_l, self.n_bins_ang, self.n_bins, self.bin_ranges = \
      self.read_hist_config ()


  # Reads the histogram config file, written previously by write_hist_config().
  # Returns the number of bins, min and max bin ranges for each dimension.
  # I decided to use YAML instead of CSV for this, because there are too many
  #   parameters for a csv, and there would only be 1 row anyway.
  #   For csv version, see triangle_sampling load_hists.py read_hist_config().
  def read_hist_config (self):

    hist_conf_name = os.path.join (get_hist_path (self.mode,
      self.nbins_subpath), 'hist_conf.yaml')

    print ('%sReading histogram config file %s%s' % (\
      ansi_colors.OKCYAN, hist_conf_name, ansi_colors.ENDC))

    hist_conf = dict ()
    with open (hist_conf_name, 'rb') as hist_conf_file:
 
      # Load as a dictionary
      # Ref: http://pyyaml.org/wiki/PyYAMLDocumentation
      hist_conf = yaml.load (hist_conf_file)

      # Load as an ordered dictionary
      #hist_conf = self.ordered_load (hist_conf_file, yaml.SafeLoader)

    # Assumption: c, n0, n1 have the same number of bins for (theta, phi). So
    #   will just access c's and use it as n_bins_ang for all 3 angles.
    n_bins_l = hist_conf [self.params ['L_STR'] + '_nbins']
    n_bins_ang = [hist_conf [self.params ['C_LON'] + '_nbins'],
      hist_conf [self.params ['C_LAT'] + '_nbins']]
    n_bins = [n_bins_l] + n_bins_ang + n_bins_ang + n_bins_ang
    assert (len (n_bins) == self.consts.N_PARAMS)

    # 7 x 2 NumPy array. i.e. List of 7 lists of 2 floats
    #   ((min,max), (min,max), (min,max), ...)
    bin_ranges = np.zeros ((self.consts.N_PARAMS, 2))
    bin_ranges [self.consts.L_STR_IDX,  0] = hist_conf [self.params ['L_STR']  + '_min']
    bin_ranges [self.consts.C_LON_IDX,  0] = hist_conf [self.params ['C_LON']  + '_min']
    bin_ranges [self.consts.C_LAT_IDX,  0] = hist_conf [self.params ['C_LAT']  + '_min']
    bin_ranges [self.consts.N0_LON_IDX, 0] = hist_conf [self.params ['N0_LON'] + '_min']
    bin_ranges [self.consts.N0_LAT_IDX, 0] = hist_conf [self.params ['N0_LAT'] + '_min']
    bin_ranges [self.consts.N1_LON_IDX, 0] = hist_conf [self.params ['N1_LON'] + '_min']
    bin_ranges [self.consts.N1_LAT_IDX, 0] = hist_conf [self.params ['N1_LAT'] + '_min']

    bin_ranges [self.consts.L_STR_IDX,  1] = hist_conf [self.params ['L_STR']  + '_max']
    bin_ranges [self.consts.C_LON_IDX,  1] = hist_conf [self.params ['C_LON']  + '_max']
    bin_ranges [self.consts.C_LAT_IDX,  1] = hist_conf [self.params ['C_LAT']  + '_max']
    bin_ranges [self.consts.N0_LON_IDX, 1] = hist_conf [self.params ['N0_LON'] + '_max']
    bin_ranges [self.consts.N0_LAT_IDX, 1] = hist_conf [self.params ['N0_LAT'] + '_max']
    bin_ranges [self.consts.N1_LON_IDX, 1] = hist_conf [self.params ['N1_LON'] + '_max']
    bin_ranges [self.consts.N1_LAT_IDX, 1] = hist_conf [self.params ['N1_LAT'] + '_max']

    print ('['),
    for d in range (0, self.consts.N_PARAMS):
      print ('%d, ' % (n_bins[d])),
    print ('] bins')
    print ('%d total bins' % (np.product (n_bins)))
    print ('bin mins:')
    print (bin_ranges [:, 0])
    print ('bin maxes:')
    print (bin_ranges [:, 1])

    # float, list of 2 floats, list of 7 floats, 7 x 2 NumPy array
    return (n_bins_l, n_bins_ang, n_bins, bin_ranges)


  def get_configs (self):
    return (self.n_bins_l, self.n_bins_ang, self.n_bins, self.bin_ranges)

  def get_n_bins (self):
    return self.n_bins

  def get_bin_ranges (self):
    return self.bin_ranges


# ============================================== Parent class ChordHistogram ==

# Parent class. Do NOT instantiate this class directly. Instantiate a child
#   class
class ChordHistogram:

  # Abstract base class
  # Ref: https://docs.python.org/2/library/abc.html
  __metaclass__ = ABCMeta

  # Read chord file, then calculate a histogram from it
  READ_FROM_CHORD = 0
  # Read histogram, which contains a histogram previously written to using
  #   self.write_to_file ().
  READ_FROM_HIST = 1


  # Parameters:
  #   obj_base: Base name of object chord file, including .csv extension
  #   obj_cat: Object category, name of the folder containing the object files
  #   mode: 'real', 'gl', 'rand'. Used to construct folder name to data files,
  #     csv_gl_hists, csv_gl_chords, csv_real_hists, csv_real_chords, etc.
  #   read_from: READ_FROM_CHORD, READ_FROM_HIST.
  #   n_bins_l: Scalar integer. Number of bins for chord length
  #   n_bins_ang: List of 2 integers. Number of bins for (theta, phi) angles
  #     that define the orientation of a vector (e.g. a chord) on a unit sphere.
  def __init__ (self, obj_base, obj_cat, mode, nbins_subpath, read_from,
    #n_bins_l=5, n_bins_ang=[10, 10], consts=None):
    #n_bins_l=HistConsts.N_BINS_L, n_bins_ang=HistConsts.N_BINS_ANG,
    n_bins_l=-1, n_bins_ang=[-1, -1],
    consts=None):

    self.obj_base = obj_base
    self.obj_cat = obj_cat

    # For defining data directory
    self.mode = mode
    self.nbins_subpath = nbins_subpath

    if not consts:
      self.consts = ChordConsts ()
    else:
      self.consts = consts
    self.params = self.consts.get_column_titles ()

    # For visualization
    self.vis_pub = rospy.Publisher ('/visualization_marker', Marker,
      queue_size=5)
    #self.vis_arr_pub = rospy.Publisher ('/visualization_marker_array',
    #  MarkerArray, queue_size=2)


    # Rest is done in child classes!


  def gen_bin_edges (self):

    # Make a dictionary out of the list of edges returned from histogramdd
    self.bin_edges = dict ()
    self.bin_edges ['L_STR'] = self.edgesdd [self.consts.L_STR_IDX]
    self.bin_edges ['C_LON'] = self.edgesdd [self.consts.C_LON_IDX]
    self.bin_edges ['C_LAT'] = self.edgesdd [self.consts.C_LAT_IDX]
    self.bin_edges ['N0_LON'] = self.edgesdd [self.consts.N0_LON_IDX]
    self.bin_edges ['N0_LAT'] = self.edgesdd [self.consts.N0_LAT_IDX]
    self.bin_edges ['N1_LON'] = self.edgesdd [self.consts.N1_LON_IDX]
    self.bin_edges ['N1_LAT'] = self.edgesdd [self.consts.N1_LAT_IDX]


  # ====================================================== Visualization fns ==

  # Visualize list of vectors, parameterized by thetas and phis, as points on
  #   a unit sphere. Optionally, pass in lengths to visualize points at lengths
  #   other than 1 from center of sphere.
  # Difference between this function and visualize_theta_phi_crosslist(): In
  #   this fn, hetas and phis are treated as parallel arrays, not cross
  #   product.
  # Parameters:
  #   thetas, phis: longitude and colatitude angles
  #   ns: namespace
  #   center: list of 3 floats. Center coordinates of sphere
  #   lengths: list of lengths of the vectors. List has same number of items
  #     as thetas and phis.
  def visualize_theta_phi_list (self, thetas, phis, ns, center=[0,0,0],
    lengths=None):

    assert (len (thetas) == len (phis))

    # Radius of sphere
    if lengths is None:
      # All points will lie on unit sphere
      rhos = np.ones ((len (thetas), ))
    else:
      assert (len (lengths) == len (thetas))
      rhos = lengths

    size = 0.02

    zero_vec = np.array ([1, 0, 0, 1])

    # Green dots
    marker = Marker ()
    create_marker (Marker.POINTS, ns, '/base', 0,
      center[0], center[1], center[2], 0, 1, 0, 0.8, size, size, size,
      marker, 0)  # Use 0 duration for forever

    for i in range (0, len (thetas)):

      x = rhos [i] * np.cos (thetas [i]) * np.sin (phis [i])
      y = rhos [i] * np.sin (thetas [i]) * np.sin (phis [i])
      z = rhos [i] * np.cos (phis [i])

      marker.points.append (Point (x, y, z))

    self.vis_pub.publish (marker)
    rospy.sleep (0.5)
    print ('Published %d points' % len (marker.points))


  # Long lat parameterized sphere. Standard parametric sphere equation. Makes
  #   evenly distributed points forming perfect square patches like on a globe.
  def visualize_theta_phi_crosslist (self, thetas, phis):

    # Radius of sphere
    rho = 1.0

    zero_vec = np.array ([1, 0, 0, 1])

    # White dots
    marker = Marker ()
    create_marker (Marker.POINTS, 'theta_phi_sphere', '/base', 0,
      0, 0, 0, 1, 1, 1, 0.8, 0.01, 0.01, 0.01,
      marker, 0)  # Use 0 duration for forever

    # Cross product of theta and phi
    for theta in thetas:
      for phi in phis:

        x = rho * np.cos (theta) * np.sin (phi)
        y = rho * np.sin (theta) * np.sin (phi)
        z = rho * np.cos (phi)

        marker.points.append (Point (x, y, z))

    self.vis_pub.publish (marker)
    rospy.sleep (0.5)
    print ('Published %d points' % len (marker.points))


  # Sphere with points parametrized by (theta, phi), at some set interval
  def visualize_theta_phi_sphere (self):

    # theta: azimuthal coordinate, 0 to 2*pi, longitude
    # phi: polar coordinate, 0 to pi, colatitude
    # Ref: http://mathworld.wolfram.com/Sphere.html
    thetas = np.linspace (0, 2.0 * np.pi, 20)
    phis = np.linspace (0, np.pi, 20)

    self.visualize_theta_phi_crosslist (thetas, phis)


  # Unused
  # RPY parameterized sphere. Terrible parameterization. Points are unevenly
  #   distributed on the sphere.
  def visualize_RPY_sphere (self):

    Rs = np.linspace (0, np.pi, 8)
    Ps = np.linspace (0, np.pi, 8)
    Ys = np.linspace (0, np.pi, 8)

    # Homogeneous coords, 4 x 1
    zero_vec = np.array ([1, 0, 0, 1])

    marker = Marker ()
    create_marker (Marker.POINTS, 'RPY_sphere', '/base', i,
      0, 0, 0, 0, 1, 0, 0.8, 0.01, 0.01, 0.01,
      marker, 0)  # Use 0 duration for forever

    i = 0

    for R in Rs:
      for P in Ps:
        for Y in Ys:

          # Multiply rotations in xyz axes sequentially
          #   In C++, would simply do tf::Quaternion::setRPY(). Bullet types
          #     don't exist in Python. One way is to compute matrices yourself,
          #     another is to use tf.transformations.py.
          quat1 = tf.transformations.quaternion_about_axis (R, (1, 0, 0))
          quat2 = tf.transformations.quaternion_about_axis (P, (0, 1, 0))
          quat3 = tf.transformations.quaternion_about_axis (Y, (0, 0, 1))
          # Multiply the quaternions
          quat = tf.transformations.quaternion_multiply (quat1, quat2)
          quat = tf.transformations.quaternion_multiply (quat, quat3)

          # Rotate the x-axis vector [1 0 0] by the rotation
          rot_mat = tf.transformations.quaternion_matrix (quat)
          vec = np.dot (rot_mat, zero_vec)

          # Normalize the direction vector to unit length
          vec /= np.linalg.norm (vec)

          marker.points.append (Point (vec[0], vec[1], vec[2]))

          i += 1

    self.vis_pub.publish (marker)
    rospy.sleep (0.5)
    print ('Published %d points' % len (marker.points))


  # Visualize square patches on the sphere. Patches are defined at histogram
  #   bin edges; each quad patch represents a histogram bin.
  # Parameters:
  #   VIS_6D: Visualize across all 3 angle quantities in the histogram (True),
  #     or just the key_pf quantity (set to False).
  #     Set to False for READ_FROM_CHORD, to visualize just 1 angle's (theta,
  #       phi), more colorful to see clearly.
  #     Set to True for READ_FROM_HIST, to collapse into 2D, else data is in
  #       full 7D and all bin counts are small, all red or blue, nothing to see.
  def visualize_theta_phi_patches (self, key_pf, center=[0,0,0], VIS_6D=False,
    skip_empty_bins=True):

    # Unit sphere radius
    rho = 1.0

    # Output: thetas, phis, max_bin_counts, hist2d
    if not VIS_6D:

      # When number of bins is huge, use n_bins_ang to 
      #   remake a histogram only for one quantity, c, n0, or n1, and visualize
      #   just this quantity, instead of across all 3 quantities in the entire
      #   hist.

      # Visualize an individual quantity, c, n0, or n1, not across all.
      samples = np.array ([self.chord_reader.chords [key_pf + '_LON'],
        self.chord_reader.chords [key_pf + '_LAT']]).T
      hist2d, edges2d = np.histogramdd (samples,
        self.n_bins_ang, range=self.bin_ranges, normed=True)

      thetas = edges2d [0]
      phis = edges2d [1]

      max_bin_counts = float (np.max (hist2d))

    else:
      # This uses the entire histogram's c_theta, c_phi, which is hard to see
      #   when number of bins is huge, `.` all bins will have small number, all
      #   quads are blue. 
      thetas = self.bin_edges [key_pf + '_LON']
      phis = self.bin_edges [key_pf + '_LAT']
     
      # Plot dots on the sphere grid
      self.visualize_theta_phi_crosslist (thetas, phis),
     
      # Project bin counts to the two dimensions of the angle of interest,
      #   by collapsing the 7D down to 2D.
      #   e.g. if have 3D histogram, to collapse to 2D, you simply sum the bin
      #   counts in 3rd dimension, whatever index in 3rd dimension, all to the
      #   [0]th bin. e.g. h[3,4,5] count gets added to h[3,4,0]; h[3,4,8] count
      #   gets added to h[3,4,0]. Do this for each z. Then you're left with
      #   h[x,y,0], a 2D grid, and you can discard all of h[:,:,1:].
      # This is different from flattening the histogram along each of the 2 
      #   dimensions separately, `.` that will double-count the bins! Because
      #   each angle is defined by 2 params, (lon, lat) collectively, not just 1.
      #   So can't just flatten along each one separately and sum.
      hist2d = np.zeros ([len (thetas) - 1, len (phis) - 1])
      for t_i in range (0, len (thetas) - 1):
        for p_i in range (0, len (phis) - 1):
          # NOTE: Change the summing indices if you change order of histogram
          #   axes!!!
          if key_pf == 'C':
            hist2d [t_i, p_i] = np.sum (self.histdd [:, t_i, p_i, :, :, :, :])
          elif key_pf == 'N0':
            hist2d [t_i, p_i] = np.sum (self.histdd [:, :, :, t_i, p_i, :, :])
          elif key_pf == 'N1':
            hist2d [t_i, p_i] = np.sum (self.histdd [:, :, :, :, :, t_i, p_i])

      print ('hist2d:')
      print (hist2d)

      # Convert to a float, so we can divide by it to get a float to get cmap
      #   which needs values in [0, 1] range
      #max_bin_counts = float (np.max (self.histdd))
      max_bin_counts = float (np.max (hist2d))
      print ('max_bin_counts: %f' % max_bin_counts)


    #####
    # Draw quads on a sphere in RViz

    alpha = 0.8

    marker = Marker ()
    # No square markers exist, so have to use two triangles per square patch
    create_marker (Marker.TRIANGLE_LIST, key_pf + '_patches', '/base', 0,
      center[0], center[1], center[2], 1, 1, 1, alpha, 1, 1, 1, marker, 0)

    cmap = cm.get_cmap ('jet')


    # One t_i, all p_i's, makes one vertical surface strip from north to south
    #   pole
    # thetas and phis are edges, there are (n_edges - 1) bins.
    for t_i in range (0, len (thetas) - 1): #8):
      for p_i in range (0, len (phis) - 1):

        # Make two triangles. Must be clockwise, `.` normal is defined on the
        #   clockwise face.
        # 0 __ 1
        #  | /
        # 2|/
        curr_tris = [((t_i, p_i), (t_i, p_i + 1), (t_i + 1, p_i)),
        #   /|1
        # 0/_|2
                    ((t_i + 1, p_i), (t_i, p_i + 1), (t_i + 1, p_i + 1))]

        for curr_tri in curr_tris:
          for vert_i in curr_tri:

            theta = thetas [vert_i [0]]
            phi = phis [vert_i [1]]
         
            x = rho * np.cos (theta) * np.sin (phi)
            y = rho * np.sin (theta) * np.sin (phi)
            z = rho * np.cos (phi)
         
            bin_count = hist2d [t_i, p_i] / max_bin_counts
            #print (bin_count)

            # Don't draw bins that have 0 count. Leave them empty, to
            #   distinguish bins with low count and bins with 0 count.
            if skip_empty_bins and bin_count > 0.0:

              # Every set of 3 points is treated as a triangle
              marker.points.append (Point (x, y, z))

              # Set color
              rgba = cmap (bin_count)
              marker.colors.append (ColorRGBA (rgba[0], rgba[1], rgba[2], alpha))

    self.vis_pub.publish (marker)
    rospy.sleep (0.5)
    print ('Published %d triangles' % (len (marker.points) / 3.0))


    # Title
    marker_text = Marker ()
    create_marker (Marker.TEXT_VIEW_FACING, key_pf + '_title', '/base', 0,
      center[0], center[1], center[2] - 1.5,
      1, 1, 1, 0.8, 0, 0, 0.2,
      marker_text, 0)
    marker_text.text = '%s' % key_pf

    self.vis_pub.publish (marker_text)
    rospy.sleep (0.5)



# =========================================== Child class HistogramFromChord ==

# Histogram is calculated from an object's chords file.
# Used in histogram_writer.py, which finds bin ranges among all chord files,
#   writes number of bins and bin ranges to hist_conf.csv, and calls
#   write_to_file() in this class to write each individual object's histogram
#   to file.
class HistogramFromChord (ChordHistogram):

  # Parameters:
  #   bin_ranges: 7 x 2 numpy array
  #     ((min,max), (min,max), (min,max), ...)
  #   chord_name: Full path of object. Only used by active_chord
  #     execute_actions.py for interfacing with external robot code.
  #     In normal cases, pass in obj_base and obj_cat to determine systematic
  #     path.
  def __init__ (self, obj_base, obj_cat, mode, nbins_subpath, bin_ranges,
    #n_bins_l=HistConsts.N_BINS_L, n_bins_ang=HistConsts.N_BINS_ANG, consts=None):
    n_bins_l, n_bins_ang, consts=None, chord_name=''):

    # Ref: http://stackoverflow.com/questions/1385759/should-init-call-the-parent-classs-init
    super (HistogramFromChord, self).__init__ (obj_base, obj_cat, mode,
      nbins_subpath, self.READ_FROM_CHORD, n_bins_l, n_bins_ang, consts)

    self.bin_ranges = bin_ranges


    #####
    # Load chords data

    if chord_name == '':
      chord_name = os.path.join (get_chord_path (self.mode), obj_cat, obj_base)
      self.chord_reader = ChordReader (obj_base, obj_cat, mode, self.consts)
    else:
      self.chord_reader = ChordReader (chord_name=chord_name)

    self.rows = self.chord_reader.get_rows ()


    #####
    # Calculate a new histogram

    # 1080 bins total
    #self.n_bins = [5, 3, 2, 3, 2, 3, 2]
    #self.n_bins_ang = [3, 2]
    #self.n_bins = [5] + n_bins_ang + n_bins_ang + n_bins_ang

    # For plotting patches. Only care about one (theta, phi) pair
    #self.n_bins = [1, 10, 10, 1, 1, 1, 1]

    # When calculating a new histogram from a chord file, use the supplied
    #   histogram params.
    self.n_bins_l = n_bins_l
    self.n_bins_ang = n_bins_ang

    # NOTE: If change this, then change the definition in read_hist_config() too.
    self.n_bins = [n_bins_l] + n_bins_ang + n_bins_ang + n_bins_ang
    assert (len (self.n_bins) == self.consts.N_PARAMS)


    # Build a samples matrix, to pass to np.histogramdd()
    # n x 7
    # NOTE: If change order of this, then change ordering in
    #   visualize_theta_phi_patches() too!!
    samples = np.zeros ((self.chord_reader.get_n_chords (), self.consts.N_PARAMS))
    samples [:, self.consts.L_STR_IDX]  = self.chord_reader.chords ['L_STR']
    samples [:, self.consts.C_LON_IDX]  = self.chord_reader.chords ['C_LON']
    samples [:, self.consts.C_LAT_IDX]  = self.chord_reader.chords ['C_LAT']
    samples [:, self.consts.N0_LON_IDX] = self.chord_reader.chords ['N0_LON']
    samples [:, self.consts.N0_LAT_IDX] = self.chord_reader.chords ['N0_LAT']
    samples [:, self.consts.N1_LON_IDX] = self.chord_reader.chords ['N1_LON']
    samples [:, self.consts.N1_LAT_IDX] = self.chord_reader.chords ['N1_LAT']


    # arg 1: n x d Numpy array. n = # points, d = # dimensions.
    #   Data to pass as param 1 to np.histogramdd().
    # edgesdd returned: a list of d arrays
    # Copied from write_hist_3d_csv() in wrist_hist_3d.py in triangle_sampling
    #   of reFlexHand repo.
    self.histdd, self.edgesdd = np.histogramdd (samples,
      self.n_bins, range=self.bin_ranges, normed=True)
    #print (np.nonzero (self.histdd))
    print ('%d bins out of %d are not empty' % (
      len (np.nonzero (self.histdd) [0]), self.histdd.size))

    # Only used by active_chord execute_actions.py for test time
    self.histdd_unnormed, _ = np.histogramdd (samples,
      self.n_bins, range=self.bin_ranges, normed=False)


    # Call parent function to populate self.bin_edges
    self.gen_bin_edges ()

    #print ('n_bins_l:')
    #print (self.n_bins_l)
    #print ('n_bins_ang:')
    #print (self.n_bins_ang)


  def get_histdd_unnormed (self):
    return self.histdd_unnormed


  # ====================================================== Histogram I/O fns ==

  def write_hist_config (self):

    hist_conf_name = os.path.join (get_hist_path (self.mode,
      self.nbins_subpath), 'hist_conf.yaml')

    print ('%sWriting histogram config file %s%s' % (\
      ansi_colors.OKCYAN, hist_conf_name, ansi_colors.ENDC))


    hist_conf = UnsortableOrderedDict ()

    # Convert numpy floats to Python floats, else YAML writes jibberish
    bin_ranges_py = self.bin_ranges.tolist ()

    # Number of bins
    hist_conf [self.params ['L_STR']  + '_nbins'] = self.n_bins_l
    hist_conf [self.params ['C_LON']  + '_nbins'] = self.n_bins_ang[0]
    hist_conf [self.params ['C_LAT']  + '_nbins'] = self.n_bins_ang[1]
    hist_conf [self.params ['N0_LON'] + '_nbins'] = self.n_bins_ang[0]
    hist_conf [self.params ['N0_LAT'] + '_nbins'] = self.n_bins_ang[1]
    hist_conf [self.params ['N1_LON'] + '_nbins'] = self.n_bins_ang[0]
    hist_conf [self.params ['N1_LAT'] + '_nbins'] = self.n_bins_ang[1]

    # Bin range max
    hist_conf [self.params ['L_STR']  + '_min'] = bin_ranges_py [self.consts.L_STR_IDX][0]
    hist_conf [self.params ['C_LON']  + '_min'] = bin_ranges_py [self.consts.C_LON_IDX][0]
    hist_conf [self.params ['C_LAT']  + '_min'] = bin_ranges_py [self.consts.C_LAT_IDX][0]
    hist_conf [self.params ['N0_LON'] + '_min'] = bin_ranges_py [self.consts.N0_LON_IDX][0]
    hist_conf [self.params ['N0_LAT'] + '_min'] = bin_ranges_py [self.consts.N0_LAT_IDX][0]
    hist_conf [self.params ['N1_LON'] + '_min'] = bin_ranges_py [self.consts.N1_LON_IDX][0]
    hist_conf [self.params ['N1_LAT'] + '_min'] = bin_ranges_py [self.consts.N1_LAT_IDX][0]

    # Bin range max
    hist_conf [self.params ['L_STR']  + '_max'] = bin_ranges_py [self.consts.L_STR_IDX][1]
    hist_conf [self.params ['C_LON']  + '_max'] = bin_ranges_py [self.consts.C_LON_IDX][1] 
    hist_conf [self.params ['C_LAT']  + '_max'] = bin_ranges_py [self.consts.C_LAT_IDX][1] 
    hist_conf [self.params ['N0_LON'] + '_max'] = bin_ranges_py [self.consts.N0_LON_IDX][1]
    hist_conf [self.params ['N0_LAT'] + '_max'] = bin_ranges_py [self.consts.N0_LAT_IDX][1]
    hist_conf [self.params ['N1_LON'] + '_max'] = bin_ranges_py [self.consts.N1_LON_IDX][1]
    hist_conf [self.params ['N1_LAT'] + '_max'] = bin_ranges_py [self.consts.N1_LAT_IDX][1]


    # Ref: http://stackoverflow.com/questions/12470665/how-can-i-write-data-in-yaml-format-in-a-file
    #   http://pyyaml.org/wiki/PyYAMLDocumentation
    with open (hist_conf_name, 'w') as hist_conf_file:
      yaml.dump (hist_conf, hist_conf_file, default_flow_style=False)

    #print ('hist_conf:')
    #print (hist_conf)

    config_written = True
    return config_written


  # Write d-dimensional histogram as linearized 1D histogram to csv file.
  # Copied from write_hist_3d_csv() in triangle_sampling write_hist_3d.py in
  #   reFlexHand repo, the "not kde" condition.
  # Parameters:
  #   config_written: First call, pass in False, and the hist_conf.csv config
  #     file will be written. Subsequent calls of the same set of object files
  #     can pass in True, to save time. (If you always pass in False, the same
  #     hist_conf.csv will just be written over and over again, wasting time.)
  #     See usage in histogram_writer.py.
  def write_to_file (self, config_written):

    if not config_written:
      self.write_hist_config ()


    # Create histogram csv file

    hist_name = os.path.join (get_hist_path (self.mode, self.nbins_subpath),
      self.obj_cat, self.obj_base)
    if not os.path.exists (os.path.dirname (hist_name)):
      os.makedirs (os.path.dirname (hist_name))

    hist_file = open (hist_name, 'wb')
    hist_writer = csv.writer (hist_file)


    # Reshape into linear shape, so don't need nested for-loops!
    # Test in bare Python shell:
    '''
    import numpy as np
    a = np.array ([[[1,2,3], [2,3,4]], [[5,6,7], [6,7,8]]])
    a.size
    a.ndim
    a.shape
    np.reshape (a, [a.size, ])

    # Test shaping back to 3D
    a = np.array ([[[1,2,3,4], [5,6,7,8]], [[9,10,11,12],[13,14,15,16]], [[17,18,19,20],[21,22,23,24]]])
    a.shape  # This prints (3,2,4)
    # Numbers are in order in the linear version
    a_lin = np.reshape (a, (a.size,))
    np.reshape (a_lin, (3,2,4))
    np.reshape (a_lin, a.shape)
    '''
    # Doesn't matter which bin is where, as long as all objects are saved the
    #   same way. `.` axes can be swapped around, values will still distribute
    #   the same way in the space.
    hist_linear = np.reshape (self.histdd, [self.histdd.size, ]);
    #if np.any (np.isnan (hist_linear)):
    if np.any (np.isnan (self.histdd)):
      print ('Saw nan in histogram')

    # Convert to Python list, then write to csv
    hist_writer.writerow (hist_linear.tolist ())
    hist_file.close ()

    print ('Histogram written to file %s' % hist_name)

    return hist_linear



  # ====================================================== Visualization fns ==

  # Visualize the orientation of c, n0, or n1, of chords, as points on a unit
  #   sphere. In this unit sphere representation, the orientation can be
  #   recovered as a vector starting at the center of the unit sphere, ending
  #   at the point on the unit sphere.
  # Parameters:
  #   key_prefix: 'C', 'N0', or 'N1'
  #   center: list of 3 floats. Center coordinates of sphere
  def visualize_chord_angles (self, thetas, phis, key_pf, center=[0,0,0],
    lengths=None):

    ns = key_pf + '_theta_phi'
    self.visualize_theta_phi_list (thetas, phis, ns, center, lengths)

    # Draw a title to specify which vector (c, n0, or n1) this sphere is for
    marker_text = Marker ()
    create_marker (Marker.TEXT_VIEW_FACING, key_pf + '_title', '/base', 0,
      center[0], center[1], center[2] - 1.5,
      1, 1, 1, 0.8, 0, 0, 0.2,
      marker_text, 0)
    marker_text.text = '%s' % key_pf

    self.vis_pub.publish (marker_text)
    rospy.sleep (0.5)


  # Visualize chords' orientation as points on a unit sphere, and histogram 
  #   bins as temperature-colored quad patches on the unit sphere.
  # Parameters:
  #   obj_base: Optional, for visualizing a text title in RViz only
  def visualize_chords_patches (self):

    # Visualize individual chords' orientations as dots on a unit sphere
    self.visualize_chord_angles (self.chord_reader.chords ['C_LON'],
      self.chord_reader.chords ['C_LAT'], 'C', [0, 0, 0],
      self.chord_reader.chords ['L_STR'])
    self.visualize_chord_angles (self.chord_reader.chords ['N0_LON'],
      self.chord_reader.chords ['N0_LAT'], 'N0', [2.5, 0, 0])
    self.visualize_chord_angles (self.chord_reader.chords ['N1_LON'],
      self.chord_reader.chords ['N1_LAT'], 'N1', [5, 0, 0])

    # Visualize histogram binned (i.e. discretized) chords' bin counts, as
    #   quad patches on a unit sphere
    self.visualize_theta_phi_patches ('C', [0, 0, 0], False)
    self.visualize_theta_phi_patches ('N0', [2.5, 0, 0], False)
    self.visualize_theta_phi_patches ('N1', [5, 0, 0], False)

    # Draw a main title for the object name, for screenshots
    obj_base_noext, _ = os.path.splitext (self.obj_base)
    marker_text = Marker ()
    create_marker (Marker.TEXT_VIEW_FACING, 'main_title', '/base', 0,
      # Place it above the middle sphere.
      2.5, 0, 1.5,
      1, 1, 1, 0.8, 0, 0, 0.2,
      marker_text, 0)
    marker_text.text = '%s' % obj_base_noext
    self.vis_pub.publish (marker_text)
    rospy.sleep (0.5)


  def discretize_orientations (self):

    # Discretize orientations into histogram bins

    pass



# ============================================ Child class HistogramFromFile ==

# Histogram is loaded directly from a histogram file previously saved
class HistogramFromFile (ChordHistogram):

  def __init__ (self, obj_base, obj_cat, mode, nbins_subpath,
    consts=None, config=None):

    # Ref: http://stackoverflow.com/questions/1385759/should-init-call-the-parent-classs-init
    super (HistogramFromFile, self).__init__ (obj_base, obj_cat, mode,
      nbins_subpath, read_from=self.READ_FROM_HIST, consts=consts)

    # Read hist_conf.yaml
    if not config:
      self.config = HistogramConfig (self.mode, self.nbins_subpath, self.consts)
    else:
      self.config = config
    # Create some shorthands
    self.n_bins_l, self.n_bins_ang, self.n_bins, self.bin_ranges = \
      self.config.get_configs ()

    # Read the histogram file
    self.hist_linear, self.histdd, self.edgesdd = self.read_hist_file ()

    # Call parent function to populate self.bin_edges
    self.gen_bin_edges ()

    #print ('n_bins_l:')
    #print (self.n_bins_l)
    #print ('n_bins_ang:')
    #print (self.n_bins_ang)


  def get_hist_linear (self):

    return self.hist_linear


  # ====================================================== Histogram I/O fns ==

  # Counterpart of load_one_hist() in load_hist.py in triangle_sampling in
  #   reFlexHand repo.
  # Load a one-line linearized histogram .csv file, reshape it into d-D
  #   histogram.
  # Returns histdd, edgesdd.
  def read_hist_file (self):

    hist_name = os.path.join (get_hist_path (self.mode, self.nbins_subpath),
      self.obj_cat, self.obj_base)

    hist_linear = []
 
    # Copied from triangle_sampling/load_hists.py
    # Read individual object's histogram file
    with open (hist_name, 'rb') as hist_file:
 
      # Read csv file
      hist_reader = csv.reader (hist_file)
 
      # There's only 1 row per file, the whole histogram flattened
      row = hist_reader.next ()
 
      # Convert strings to floats
      hist_linear.append ([float (row [i]) for i in range (0, len (row))])
 
    #print (hist_linear)
    hist_linear = np.asarray (hist_linear)

    print ('%d non-zero values in histogram' % (np.count_nonzero (hist_linear)))
 
 
    # Reshape 1D hist loaded into 3D
    # This does the right thing, as long as write_hist_3d.py
    #   write_hist_3d_csv() reshapes the 3D histogram to 1D by
    #   np.reshape(histdd, (histdd.size, )), without any other fancy parameters.
    #   That will give the default reshaping, and to reshape it back, it's
    #   just np.reshape(hist_linear, histdd.shape), without any params.
    #   histdd.shape is (10,10,10), or n_bins.
    histdd = np.reshape (hist_linear, (self.n_bins))
 
    # For n bins, there should be n+1 edges
    #   List of 3 lists of (number of bins) items
    # linspace() has more careful handling of floating endpoints than arange(),
    #   says NumPy arange() documentation. linspace() also includes the endpoint.
    edgesdd = []
    for d in range (0, self.consts.N_PARAMS):
      edgesdd.append (np.linspace (self.bin_ranges[d,0], self.bin_ranges[d,1],
        self.n_bins[d] + 1, endpoint=True))

    #print ('Un-linearized histogram shape:')
    #print (np.shape (histdd))
    #print ('edgesdd:')
    #print (edgesdd)
 
    return hist_linear, histdd, edgesdd


  # ====================================================== Visualization fns ==

  # Visualize square patches on the sphere. Patches are defined at histogram
  #   bin edges; each quad patch represents a histogram bin.
  # Parameters:
  #   obj_base: Optional, for visualizing a text title in RViz only
  def visualize_hist_patches (self):

    # Visualize histogram binned (i.e. discretized) chords' bin counts, as
    #   quad patches on a unit sphere
    self.visualize_theta_phi_patches ('C', [0, 0, 0], True)
    self.visualize_theta_phi_patches ('N0', [2.5, 0, 0], True)
    self.visualize_theta_phi_patches ('N1', [5, 0, 0], True)

    # Draw a main title for the object name, for screenshots
    # Place it above the middle sphere.
    obj_base_noext, _ = os.path.splitext (self.obj_base)
    marker_text = Marker ()
    create_marker (Marker.TEXT_VIEW_FACING, 'main_title', '/base', 0,
      2.5, 0, 1.5,
      1, 1, 1, 0.8, 0, 0, 0.2,
      marker_text, 0)
    marker_text.text = '%s' % obj_base_noext
    self.vis_pub.publish (marker_text)
    rospy.sleep (0.5)



# Register children of abstract base class
# Ref: https://docs.python.org/2/library/abc.html
ChordHistogram.register (HistogramFromChord)
ChordHistogram.register (HistogramFromFile)



# Tests basic functionality of ChordHistogram class, mainly the visualization!
#   Run this script directly with RViz to see sample visualization of the
#   histogram.
# ChordHistogram class is actually used in histogram_writer.py
def main ():

  rospy.init_node ('histogram_of_chords', anonymous=True)

  #####
  # Parse command line args
  #####

  arg_parser = argparse.ArgumentParser ()

  args, valid = parse_args_for_histogram (arg_parser, chord_opt=True)
  if not valid:
    return

  mode = config_params_from_args (args)
  nbins_subpath, nbins = get_nbins_subpath (args.nbins)

  print ('%sBins settings: (%d, %d, %d)%s' % (
    ansi_colors.OKCYAN, nbins[0], nbins[1], nbins[2], ansi_colors.ENDC))

  # If a meta file is specified, visualize every object in the list
  meta_base = args.meta
  if meta_base:
    # Read meta file
    meta_name = os.path.join (get_recog_meta_path (), args.meta)
    lines = read_meta_file (meta_name)
    n_objs = len (lines)

  # Else just visualize the object specified on cmd line
  else:
    obj_base = args.obj_base + '.csv'
    obj_cat = args.obj_cat
    n_objs = 1


  # Select which object class to test, HistogramFromChord or HistogramFromFile
  TEST_MODE = ChordHistogram.READ_FROM_CHORD #HIST

  consts = ChordConsts ()
  if TEST_MODE == ChordHistogram.READ_FROM_HIST:
    config = HistogramConfig (mode, nbins_subpath, consts)


  wait_rate = rospy.Rate (10)

  #while not rospy.is_shutdown ():
  for i in range (0, n_objs):

    obj_cat, obj_base = get_meta_cat_name (lines [i])

    print ('Visualizing object %d out of %d: %s' % (i, n_objs, obj_base))

    if TEST_MODE == ChordHistogram.READ_FROM_CHORD:
      hist_node = HistogramFromChord (obj_base, obj_cat, mode, nbins_subpath,
        bin_ranges=None, n_bins_l=nbins[0], n_bins_ang=nbins[1:3], consts=consts)

      #hist_node.visualize_RPY_sphere ()
      #hist_node.visualize_theta_phi_sphere ()
 
      #hist_node.visualize_theta_phi_patches ()
      hist_node.visualize_chords_patches ()

 
    elif TEST_MODE == ChordHistogram.READ_FROM_HIST:
      hist_node = HistogramFromFile (obj_base, obj_cat, mode, nbins_subpath,
        consts, config)

      hist_node.visualize_hist_patches ()


    try:
      wait_rate.sleep ()
    except rospy.exceptions.ROSInterruptException, err:
      break

    print ('')

    if i >= n_objs:
      break
    else:
      uinput = raw_input ('Enter Q to quit, anything else to continue: ')
      if uinput.lower () == 'q':
        break


if __name__ == '__main__':
  main ()

