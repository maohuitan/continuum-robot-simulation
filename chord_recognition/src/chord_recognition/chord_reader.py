#!/usr/bin/env python

# Mabel Zhang
# 29 Nov 2016
#
# Reads chords from .csv files
#

# ROS
import rospy

# Python
import os
import csv

import numpy as np

# My packages
from util_continuum.config_paths import get_chord_path #config_datapath
from chord_recognition.chord_consts import ChordConsts


class ChordReader:

  # Parameters:
  #   obj_base: Base name of object chord file, including .csv extension
  #   obj_cat: Object category, name of the folder containing the object files
  #   mode: 'real', 'gl', 'rand'. Used to construct folder name to data files,
  #     csv_gl_hists, csv_gl_chords, csv_real_hists, csv_real_chords, etc.
  #   consts: ChordConsts() instances. Instantiating in caller saves I/O time,
  #     so that each instance of ChordReader doesn't have to re-read the YAML
  #     file to get the constants! Caller should just read the consts once and
  #     pass it to instances of ChordReaders.
  def __init__ (self, obj_base='', obj_cat='', mode='', consts=None, chord_name=''):

    # Full path to chord csv file
    if chord_name == '':
      self.chord_name = os.path.join (get_chord_path (mode), obj_cat, obj_base)
    else:
      self.chord_name = chord_name

    if consts is None:
      self.consts = ChordConsts ()
    else:
      self.consts = consts
    self.column_titles = self.consts.get_column_titles ()

    self.csv_file = open (self.chord_name, 'rb')

    self.csv_reader = csv.DictReader (self.csv_file)
    self.rows = self.read_file ()


    # Parse the strings into floats
 
    # n x 1 vector
    ls = np.array ([float (self.rows [i] [self.column_titles [
      'L_STR']]) for i in range (0, len (self.rows))])
 
    c_thetas = np.array ([float (self.rows [i] [self.column_titles [
      'C_LON']]) for i in range (0, len (self.rows))])
    c_phis = np.array ([float (self.rows [i] [self.column_titles [
      'C_LAT']]) for i in range (0, len (self.rows))])
 
    n0_thetas = np.array ([float (self.rows [i] [self.column_titles [
      'N0_LON']]) for i in range (0, len (self.rows))])
    n0_phis = np.array ([float (self.rows [i] [self.column_titles [
      'N0_LAT']]) for i in range (0, len (self.rows))])
 
    n1_thetas = np.array ([float (self.rows [i] [self.column_titles [
      'N1_LON']]) for i in range (0, len (self.rows))])
    n1_phis = np.array ([float (self.rows [i] [self.column_titles [
      'N1_LAT']]) for i in range (0, len (self.rows))])
 
    self.chords = dict ()
    self.chords ['L_STR'] = ls
    self.chords ['C_LON'] = c_thetas
    self.chords ['C_LAT'] = c_phis
    self.chords ['N0_LON'] = n0_thetas
    self.chords ['N0_LAT'] = n0_phis
    self.chords ['N1_LON'] = n1_thetas
    self.chords ['N1_LAT'] = n1_phis

    self.n_chords = np.shape (ls) [0]
    print ('n_chords: %d' % self.n_chords)


    # Min and max values for each parameter

    # 7 x 1 vector
    self.mins = np.zeros (self.consts.N_PARAMS)
    self.mins [self.consts.L_STR_IDX]  = np.min (self.chords ['L_STR'])
    self.mins [self.consts.C_LON_IDX]  = np.min (self.chords ['C_LON'])
    self.mins [self.consts.C_LAT_IDX]  = np.min (self.chords ['C_LAT'])
    self.mins [self.consts.N0_LON_IDX] = np.min (self.chords ['N0_LON'])
    self.mins [self.consts.N0_LAT_IDX] = np.min (self.chords ['N0_LAT'])
    self.mins [self.consts.N1_LON_IDX] = np.min (self.chords ['N1_LON'])
    self.mins [self.consts.N1_LAT_IDX] = np.min (self.chords ['N1_LAT'])

    # 7 x 1 vector
    self.maxes = np.zeros (self.consts.N_PARAMS)
    self.maxes [self.consts.L_STR_IDX]  = np.max (self.chords ['L_STR'])
    self.maxes [self.consts.C_LON_IDX]  = np.max (self.chords ['C_LON'])
    self.maxes [self.consts.C_LAT_IDX]  = np.max (self.chords ['C_LAT'])
    self.maxes [self.consts.N0_LON_IDX] = np.max (self.chords ['N0_LON'])
    self.maxes [self.consts.N0_LAT_IDX] = np.max (self.chords ['N0_LAT'])
    self.maxes [self.consts.N1_LON_IDX] = np.max (self.chords ['N1_LON'])
    self.maxes [self.consts.N1_LAT_IDX] = np.max (self.chords ['N1_LAT'])

    print ('mins:')
    print (self.mins)
    print ('maxes:')
    print (self.maxes)


  def get_chord_name (self):
    return self.chord_name

  def get_column_titles (self):
    return self.column_titles

  def get_rows (self):
    return self.rows

  def get_n_chords (self):
    return self.n_chords

  def get_chords (self):
    return self.chords


  def read_file (self):

    print ('Reading from %s' % self.chord_name)

    rows = []

    for row in self.csv_reader:

      # Print each filed we read
      #for title in self.column_titles.values ():
      #  print ('%s: %f' % (title, float (row [title]))),
      #print ('')

      rows.append (row)

    return rows


def main ():

  rospy.init_node ('chord_reader', anonymous=True)

  this_node = ChordReader (obj_base, obj_cat, mode)

  '''
  wait_rate = rospy.Rate (10)

  while not rospy.is_shutdown ():

    try:
      wait_rate.sleep ()
    except rospy.exceptions.ROSInterruptException, err:
      break
  '''


if __name__ == '__main__':
  main ()

