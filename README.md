## Tested on Ubuntu 16.04

## dependencies
* Install ROS 
http://www.ros.org/
* Install OpenCV: 
$ sudo apt-get install libopencv-dev python-opencv
* Install PCL:
http://pointclouds.org/
* Install Openmesh: 
https://www.openmesh.org/download/ 
* Install OpenGL:
$ sudo apt-get install mesa-common-dev
$ sudo apt-get install freeglut3-dev
$ sudo apt-get install libsoil-dev

## compile and run
1. create a ros catkin workspace.
2. $ cd ~/catkin_ws/src
3. $ git clone https://maohuitan@bitbucket.org/maohuitan/continuum-robot-simulation.git
4. $ cd ~/catkin_ws
5. $ catkin_make
6. $ source devel/setup.bash
7. $ roscore (in a new terminal)
8. $ rosrun touch_continuum_wraps touch_continuum_wraps
9. Press F12 to see an example of touch-driven arm wrapping.

## notes
There are a few ROS packages in this repository. The only one that is relevant to continuum robot simulator is touch_contiuum_wraps.

## important source files in ROS package touch_contiuum_wraps:
1. interface.cpp: handles OpenGL settings
2. robotinteraction.cpp: set up and launch simulation environment
3. environment_with_tactile_sensing.cpp: robot planning and control, collision checking
4. ManipulatorNSections.h: robot kinematic description and visulization in OpenGL








