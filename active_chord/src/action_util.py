#!/usr/bin/env python

# Mabel Zhang
# 10 Dec 2016
#
# Utility functions for an action in active sensing.
# An action is a 4-tuple Quaternion that rotates one plane to another.
#   Each plane represents a cross section that the continuum arm moves into.
#
# Counterpart of active_touch costs_util.py used for triangles.
#

# ROS
import rospy
from visualization_msgs.msg import Marker
from geometry_msgs.msg import Point
import tf

import numpy as np

# My packages
from util_continuum.create_marker import create_marker



# Returns a quaternion (4-elt numpy array) that rotates (phyiscally moves) unit
#   normal vector n1 onto n2.
# Parameters:
#   n1, n2: 3-elt 1D NumPy arrays. Unit normal vectors, each representing a
#     plane. The normal vector is the normal to the plane.
def calc_relative_action (n1, n2):

  # Two vectors make a plane.
  # Take the cross product btw them, and that gives you a normal to their
  #   common plane. Then just need to rotate wrt this normal, by the angle
  #   in between the two orig vectors. This will rotate n1 onto n2.

  # Make sure the inputs are unit vectors
  n1 /= np.linalg.norm (n1)
  n2 /= np.linalg.norm (n2)

  # Find the normal to the plane formed by n1 and n2
  axis = np.cross (n1, n2)

  # Find the angle btw the two vectors, by dot product.
  # dot(a, b) = |a| |b| cos theta
  # If a and b are unit vectors, then dot(a, b) = cos theta, and
  #   theta = arccos (dot (a, b)).
  n1_dot_n2 = np.dot (n1, n2)
  # Avoid Python floating point error, sometimes 1.00000 is slightly >1, so get
  #   RuntimeWarning invalid value encountered in arccos.
  if np.abs (n1_dot_n2 - 1.0) < 1e-6:
    n1_dot_n2 = 1.0

  #print ('Inside arccos: %f' % np.dot (n1, n2))
  angle = np.arccos (n1_dot_n2)
  #print ('angle: %f' % angle)

  q = tf.transformations.quaternion_about_axis (angle, axis)

  return q


# Return a vector (3-elt numpy array), which represents a normal to a plane.
#   (A plane has 2 normals, this can be either one. No guarantee which one.)
# Parameters:
#   n1: a unit 3-vector
def calc_result_plane (n1, q):

  # Apply quaternion q onto unit vector n1, to move onto unit vector n2

  # Convert quaternion to a 4 x 4 matrix
  q_mat = tf.transformations.quaternion_matrix (q)

  # Make sure input is a unit vector
  n1 /= np.linalg.norm (n1)

  # Convert vector to 4 x 1 homogeneous coorindates, then apply quaternion
  #   on it.
  n2 = np.dot (q_mat, np.append (n1, [1]))
  # Take first 3 elements from homogeneous coords
  n2 = n2 [0:3]

  return n2


# Visualize a plane normal to the given unit vector. Draw the plane as two RViz
#   Marker triangles.
# Parameters:
#   n: Unit normal vector to the plane of interest
def visualize_plane (n, vis_pub, ns, color=[1,1,1]):

  alpha = 0.5

  #####
  # Find a quad in the plane. That means 4 points.
  #   2 vectors starting from the same point will give 3 points (e.g. x- and
  #     y- axes). Adding the 2 vectors will give a 3rd vector, which gives
  #     a 4th point.

  # Make sure input is unit vector
  n /= np.linalg.norm (n)

  # Find the rotation from basis z-axis to the given vector. This is the
  #   rotation from basis frame to the frame that has the given vector n as its
  #   z-axis. The same rotation can be used to rotate the basis x-axis to that
  #   frame's x-axis.
  q_basis_to_n = calc_relative_action (np.array ([0, 0, 1]), n)
  # Convert the rotation to a 4 x 4 matrix
  mat = tf.transformations.quaternion_matrix (q_basis_to_n)

  # Find two vectors in the plane of interest, by multiplying the matrix
  #   by the basis vectors x and y.
  # Take first 3 elts, to make 3-vectors.
  x_in_n = np.dot (mat, [1, 0, 0, 1]) [0:3]
  y_in_n = np.dot (mat, [0, 1, 0, 1]) [0:3]

  # 3-vector
  # Don't normalize this! This gives you the 4th point to make a triangle.
  #   It doesn't need to have unit length, in fact, it'll have sqrt(2) length.
  v_in_n = x_in_n + y_in_n


  #####
  # Visualize the two triangles
  #  y ___ v
  #   |  /|
  #   | / |
  #  0|/__|x

  marker = Marker ()
  # No square markers exist, so have to use two triangles per quad
  create_marker (Marker.TRIANGLE_LIST, ns, '/base', 0,
    0, 0, 0, color[0], color[1], color[2], alpha, 1, 1, 1, marker, 0)

  # Note these need to be clockwise, for plane to render with normal facing
  #   the right way. But it's too much work to enforce clockwise and
  #   unnecessary (maybe), so just ordering the 3 points so that the 2
  #   triangles are facing the same way.
  marker.points.append (Point (0, 0, 0))
  marker.points.append (Point (y_in_n[0], y_in_n[1], y_in_n[2]))
  marker.points.append (Point (x_in_n[0], x_in_n[1], x_in_n[2]))

  marker.points.append (Point (x_in_n[0], x_in_n[1], x_in_n[2]))
  marker.points.append (Point (y_in_n[0], y_in_n[1], y_in_n[2]))
  marker.points.append (Point (v_in_n[0], v_in_n[1], v_in_n[2]))

  print ('Points on triangle 1:')
  print (marker.points [0])
  print (marker.points [1])
  print (marker.points [2])
  print ('Points on triangle 2:')
  print (marker.points [3])
  print (marker.points [4])
  print (marker.points [5])
 
  vis_pub.publish (marker)
  rospy.sleep (0.5)


  #####
  # Visualize the given normal vector to the plane, as an arrow.
  #   Start the vector at the center of the quad visualized above. The center
  #   is at midway of v_in_n.

  # Length of arrow to visualize. Arbitrary
  n_len = 0.3

  n_start = v_in_n * 0.5
  n_end = (n_start + n * n_len)

  marker_n = Marker ()
  # ARROW: scale.x is shaft diameter, scale.y is head diameter, scale.z is
  #   head height
  create_marker (Marker.ARROW, ns, '/base', 1,
    0, 0, 0, color[0], color[1], color[2], alpha, 0.02, 0.03, n_len * 0.2,
    marker_n, 0)

  marker_n.points.append (Point (n_start[0], n_start[1], n_start[2]))
  marker_n.points.append (Point (n_end[0], n_end[1], n_end[2]))

  vis_pub.publish (marker_n)
  rospy.sleep (0.5)


  #####
  # Visualize a text label

  n_end = (n_start + n * 0.4)

  marker_t = Marker ()
  # TEXT: scale.z is height of uppercase A
  create_marker (Marker.TEXT_VIEW_FACING, ns, '/base', 2,
    n_end[0], n_end[1], n_end[2], color[0], color[1], color[2], alpha, 0, 0, 0.1,
    marker_t, 0)
  marker_t.text = ns

  vis_pub.publish (marker_t)
  rospy.sleep (0.5)

  print ('Published plane normal to %f %f %f' % (n[0], n[1], n[2]))



# ========================================== Costs from a relative movement ==

# Copied from active_touch costs_util.py get_quaternion_angle()
# Calculates the angle of a Quaternion, offsetted from identity 0 0 0 w=1.
# Returns angle in range [-pi, pi]
def calc_quaternion_angle (qx, qy, qz, qw):

  # Because Quaternions have two identical representations, q = -q, so we need
  #   to have a convention, to represent such equal quaternions in the same
  #   way. This is needed `.` in the following calculation for angle, we ignore
  #   sign of qx qy qz, and only look at sign of qw. If two equal
  #   quaternions are not represented the same way, i.e. one has +w, one has
  #   -w, then the calculated angle will be different, which is incorrect!!
  # Convention: qx is always positive.
  if qx < 0:
    qx = -qx
    qy = -qy
    qz = -qz
    qw = -qw

  # Quaternion distance
  #   Distance is btw (qx, qy, qz, qw) and origin (0, 0, 0, w=1)
  # Signed angle of rotation by shortest arc
  # Ref: http://stackoverflow.com/questions/23260939/distance-or-magnitude-between-two-quaternions
  # atan2(qw, qxyz_norm) gives pi for 0 0 0 1 quaternion!!! That's not right!
  #   It should give 0!! Testing on python shell with
  #   q = tf.transformations.quaternion_about_axis(1.9, (1,0,0))
  #   calc_mvmt_cost(0, 0, 0, *q)
  #   reveals that atan2(qxyz_norm, qw) is the correct argument order. It 
  #   returns angle 1.9. If you flip the order, it returns 1.2, which is wrong.
  angle = 2.0 * np.arctan2 (np.linalg.norm ((qx, qy, qz)), qw)

  # Checked this on python shell. It needs this correction
  if angle > np.pi:
    #print ('Large angle corrected from %f to %f' % (angle, angle-2.0*np.pi))
    angle = angle - 2.0 * np.pi
  elif angle < -np.pi:
    # TODO 14 Jan 2016:
    # Wait... this seems wrong. angle < 0 here. This will make angle over 2*pi!
    #   It should be angle + 2.0 * np.pi, no? Test it
    #   > Tested some random rpy angles. This case NEVER happens! I think
    #     arctan2() above always gives range (0, 2*pi). So that's why even
    #     though this line is wrong, it never affects anything.
    print ('UNTESTED case: Small angle corrected from %f to %f' % (angle, angle+2.0*np.pi))
    #angle = -angle + 2.0 * np.pi
    # I think this line should be right. But since this case is never true,
    #   might as well remove the elif altogether.
    angle = angle + 2.0 * np.pi
  return angle


# Copied from active_touch costs_util.py used for triangles
# Costs must be in range [0, 1] for active system.
# Parameters:
#   q: Quaternion, (qx qy qz qw)
def calc_action_cost (q):

  # In range [-pi, pi]
  angle = calc_quaternion_angle (q[0], q[1], q[2], q[3])

  # I don't need a signed angle, I just want the relative angle difference,
  #   for cost measure. So take abs().
  angle = np.abs (angle)

  # Normalize to range [0, 1]
  max_angle = np.pi
  angle_norm = angle / max_angle

  return angle_norm



# Tests functions in this file
def main ():

  rospy.init_node ('action_util', anonymous=True)

  vis_pub = rospy.Publisher ('/visualization_marker', Marker,
    queue_size=5)


  # Test calc_quaternion_angle() function
  for i in range (0, 20):
    q = tf.transformations.quaternion_from_euler (
      np.random.random (1) * 2 * np.pi,
      np.random.random (1) * 2 * np.pi,
      np.random.random (1) * 2 * np.pi)
    angle = calc_quaternion_angle (q[0], q[1], q[2], q[3])
    print ('quaternion angle (should be in range [-pi, pi]): %f' % angle)


  '''

  # Rotate from x-y plane to y-z plane
  #n1 = (0, 0, 1)
  #n2 = (1, 0, 0)

  # Randomly generate planes
  n1 = np.random.random (3)
  n1 /= np.linalg.norm (n1)
  n2 = np.random.random (3)
  n2 /= np.linalg.norm (n2)

  q = calc_relative_action (n1, n2)
  n2_calculated = calc_result_plane (n1, q)


  wait_rate = rospy.Rate (10)

  while not rospy.is_shutdown ():

    # Visualize to check if correct
    visualize_plane (n1, vis_pub, 'n1', [1, 0, 0])
    visualize_plane (n2, vis_pub, 'n2', [0, 1, 0])
 
    # This should overlap n2, if calc_relative_action() and calc_result_plane()
    #   functions are implemented correctly.
    visualize_plane (n2_calculated, vis_pub, 'n2_calculated', [0, 0, 1])


    try:
      wait_rate.sleep ()
    except rospy.exceptions.ROSInterruptException, err:
      break
  '''


if __name__ == '__main__':
  main ()

