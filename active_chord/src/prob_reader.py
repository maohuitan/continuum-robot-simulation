#!/usr/bin/env python

# Mabel Zhang
# 9 Jan 2017
#
# Copied and modified from active_touch prob_of_observs.py which is for
#   triangles data.
#   This version tries to be more OOP, using ObservationList and PoseList
#   objects.
#
# Write this class so that it is reusable. Its task is to return several
#   conditional probabilities. But depending on the data structure you choose
#   to store the training data in, computing these probabilities will require
#   different code!
#
#   Could try having generic classes like Observation, Action, Pose, etc
#   that keep the one or two lists of data from training. Then make one
#   4D numpy array that is constructed by calling these classes. The 
#   numpy array is what relates these O(n) linear data lists into the
#   inter-dependent conditional probability.
#   The classes can be reused for a different robot or different object
#   descriptor. These classes contain functions to merge two lists of the
#   data, coming from two different object files.
#   If you change the stored data strcutre, then you only need to change the
#   reader function, to pass the correct list to the different classes.
#

# ROS
import rospy

# Python
import os
import pickle
import time
#from abc import ABCMeta
from copy import deepcopy

# NumPy
import numpy as np
from sklearn.externals import joblib

# My packages
from util_continuum.ansi_colors import ansi_colors
from util_continuum.parse_meta import read_meta_file
from util_continuum.config_paths import get_prob_path, get_active_meta_path
from chord_recognition.histogram_of_chords import HistogramConfig
from chord_recognition.chord_consts import ChordConsts
from chord_recognition.hist_util import find_bin_edges_and_volume
from util_continuum.mat_util import find_dups_multirow_onelist

# Local
from config_pkl import ActivePKLConsts as ActivPKL
from observation_list import ObservationList_Chords
from pose_list import PoseList_Chords
from action_util import calc_result_plane
#from prob_writer import ProbWriter



# ============================================================ Parent class ==

# Parent class for accessing probabilities data.
#   Independent of data containers PoseList and ObservationList,
#   which the child class needs to implement in tendam of inheriting this
#   class. Child class defines specific actions (inherit PoseList) and
#   observations (inherit ObservationList) for the application, and inherit
#   this class (ProbReader) to read and access those data.
# Child class needs to implement:
#   load_probs ()
# Inherit from object class so child class can call super().__init__()
class ProbReader (object):

  def __init__ (self, meta_base, mode, n_bins, bin_ranges): #nbins_subpath):

    # Abstract base class
    # Ref: https://docs.python.org/2/library/abc.html
    #__metaclass__ = ABCMeta

    # List of nClasses elts
    self.obs_insts = []
    self.pose_insts = []

    # Populated in load_probs()
    self.cat_names = []
    self.cat_ids = []

    self.n_bins = n_bins
    self.bin_ranges = bin_ranges

    # Load probabilities data
    self.load_probs (meta_base, mode)


    # Load recognition data

    # TODO copy from prob_of_observs.py when done with probabilities stuff.
    #   Recognition code is all rewritten, in histogram_of_chords.py in
    #   chord_recognition.

    # There's HistogramReader class now, so I don't know
    #   if I still need to maintain all the vars in this file!!!
    '''
    self.chord_consts = ChordConsts ()
    self.hist_config = HistogramConfig (mode, nbins_subpath, self.chord_consts)

    # float, list of 2 floats, list of 7 floats, 7 x 2 NumPy array
    n_bins_l, n_bins_ang, n_bins, bin_ranges =
      self.hist_config.read_hist_config ()
    '''


  # =============================================================== Getters ==

  #   c: class idx
  def get_pose_inst (self, c):
    return self.pose_insts [c]


  #   c: class idx
  def get_obs_inst (self, c):
    return self.obs_insts [c]

  #   c: class idx
  #   i: row idx in the class
  def get_obs_c_i (self, c, i):
    return self.obs_insts [c].get_single_obs_i (i)

  def get_classes (self):
    return self.cat_names

  def get_n_classes (self):
    return len (self.cat_names)


# ================================================== Child class for chords ==

class ProbReader_Chords (ProbReader):

  def __init__ (self, meta_base, mode, n_bins, bin_ranges): #nbins_subpath):

    self.OBS_DIMS = 7
    self.POSE_DIMS = 3

    # One list, merged across all classes
    self.obs_inst_all = ObservationList_Chords (self.OBS_DIMS)
    self.pose_inst_all = PoseList_Chords (self.POSE_DIMS)

    # Maps indices in self.obs_insts [c_i].obsl_list [row_i, :] to indices in
    #   self.obs_inst_all.obsl_list [row_i, :]. This allows accessing per-class
    #   observations (or poses) in the 1D list with all observations (or
    #   poses) loaded, independent of class.
    self.obs_perclass_to_all_map = []
    self.pose_perclass_to_all_map = []

    # Call parent init at END of this init, `.` load_probs() is called in
    #   parent init(). Need self.* fields defined before calling that.
    # Ref: http://stackoverflow.com/questions/1385759/should-init-call-the-parent-classs-init
    super (ProbReader_Chords, self).__init__ (meta_base, mode,
      n_bins, bin_ranges) #nbins_subpath)
    # Above line throws error, not sure why
    #   > Parent class either needs to inherit from object, or have this line:
    #     __metaclass__ = ABCMeta
    # Ref: http://stackoverflow.com/questions/9698614/super-raises-typeerror-must-be-type-not-classobj-for-new-style-class
    #ProbReader.__init__ (self, meta_base, mode, nbins_subpath)


  def get_obs_dims (self):
    return self.OBS_DIMS

  def get_obs_inst_all (self):
    return self.obs_inst_all

  def get_obs_all_i (self, i):
    return self.obs_inst_all.get_single_obs_i (i)


  def get_pose_dims (self):
    return self.POSE_DIMS

  def get_pose_inst_all (self):
    return self.pose_inst_all

  def get_n_all_poses (self):
    return self.pose_inst_all.get_n_poses ()

  def get_pose_all_i (self, i):
    return self.pose_inst_all.get_pose_i (i)


  # ======================================================= Load probs data ==

  # Load probabilities saved by prob_writer.py
  # Output: self.raw_data
  def load_probs (self, meta_base, mode):

    meta_name = os.path.join (get_active_meta_path (), meta_base)

    lines, self.cat_names, self.cat_ids = read_meta_file (meta_name,
      return_cats=True)


    # Instantiate a list of class instances, one instance for each trained
    #   object, to hold raw data from which probabilities will be computed.
    for c_i in self.cat_ids:
      # Observation is a chord, 7 dims
      self.obs_insts.append (ObservationList_Chords (self.OBS_DIMS))
      # Pose is a 3-vector normal to cross-section plane, 3 dims
      self.pose_insts.append (PoseList_Chords (self.POSE_DIMS))

    probs_path = get_prob_path (mode)

    # Read raw .pkl data and combine multiple objects in each class into a
    #   per-class profile of raw data.
    for line in lines:

      # Change extension to .pkl, in case it's not
      probs_name = os.path.join (probs_path,
        os.path.splitext (line) [0] + '.pkl')

      print ('Loading probs from %s' % probs_name)

      with open (probs_name, 'rb') as f:
        pkl_wrapper = pickle.load (f)

      # Assumption: each .pkl file has only 1 class. `.` each .pkl file only
      #   contains one object, when saved from prob_writer.py
      # Access using:
      #   ActivPKL.OBJ_BASE, CHORDS, OBSERVS, OBS_TALLIES, ABS_POSES, POSE_OBSL
      obj_cat = pkl_wrapper.keys () [0]

      chords = pkl_wrapper [obj_cat] [ActivPKL.CHORDS]
      obsl_list = pkl_wrapper [obj_cat] [ActivPKL.OBSERVS]
      obs_tallies = pkl_wrapper [obj_cat] [ActivPKL.OBS_TALLIES]
      abs_poses = pkl_wrapper [obj_cat] [ActivPKL.ABS_POSES]
      pose_obsl = pkl_wrapper [obj_cat] [ActivPKL.POSE_OBSL]

      #disc_pose = pkl_wrapper [obj_cat] [ActivPKL.DISC_POSE]
      #disc_chord = pkl_wrapper [obj_cat] [ActivPKL.DISC_CHORD]

      # Combine data from this object, to all objects loaded so far in this
      #   object's class
      obj_catid = self.cat_names.index (obj_cat)

      curr_obs_inst = ObservationList_Chords (self.OBS_DIMS)
      curr_obs_inst.reset_vars (chords, obsl_list, obs_tallies)
      self.obs_insts [obj_catid].merge_with_instance (curr_obs_inst)

      curr_pose_inst = PoseList_Chords (self.POSE_DIMS)
      curr_pose_inst.reset_vars (abs_poses, pose_obsl)
      #print ('Adding')
      #print (curr_pose_inst.pose_obsl)
      #print ('to')
      #print (self.pose_insts [obj_catid].pose_obsl)
      self.pose_insts [obj_catid].merge_with_instance (curr_pose_inst)
      #print (self.pose_insts [obj_catid].pose_obsl)
      #uinput = raw_input ('Press enter: ')

      print ('')
       

    # Discretize to use observations as bin centers, as opposed to raw obs.
    #   This allows to generalize across classes when computing marginalization
    #   over z0 across all classes y. Needed `.` a raw obs z0 may not exist in
    #   every class; if not, then cannot compute its conditional probability.
    for c_i in range (len (self.obs_insts)):
      # bin_edges: list of ndims 1D numpy vector
      bin_edges, bin_centers, _ = find_bin_edges_and_volume (self.n_bins,
        self.bin_ranges)
      # TODO: Implement update of pose_obsl in PoseList
      old_to_new_obsl_map = self.obs_insts [c_i].discretize_to_hist_centers (
        bin_edges, bin_centers)
      self.pose_insts [c_i].replace_pose_obsl (old_to_new_obsl_map)
    print ('')


    print ('Creating maps pose_inst_all and obs_inst_all, mapping from per-class to overall indices...')

    # Create a master list of poses across all classes. This is for tree to
    #   access all poses at once, for tree search to cover for each node.
    self.obs_perclass_to_all_map = [None] * len (self.pose_insts)
    self.pose_perclass_to_all_map = [None] * len (self.pose_insts)
    #combiner = ProbWriter ()
    # For each class, merge the instances.
    for c_i in range (0, len (self.pose_insts)):

      # X| Now ensuring correspondence through loop below
      # TODO:
      # Try merging instead by calling prob_writer.py add_abs_pose_and_obs().
      #   That will add_chords, add_observations, add_poses() and make sure
      #   the ObservationList and PoseList correspond.

      # Eh.... this is difficult. What if the chords aren't in same order as
      #   obs_insts [c_i]???? I'd have to keep a completely separate set of
      #   indices for obs_inst_all! It'd be a mess!!!! Now it seems easier to
      #   just go back and forth between obs_insts[c_i] and obs_inst_all.
      #combiner.add_abs_pose_and_obs (self.pose_insts [c_i].abs_poses,
      #  self.obs_insts [c_i].single_obs, disc_pose, disc_chord)


      # Keep a map from per-class indices to overall indices.
      #   Index row_i indexes obs_insts [c_i].obsl_list [row_i].
      #   Value row_i' indexes obs_inst_all.obsl_list [row_i'].
      obs_perclass_dup_idxes = \
        self.obs_inst_all.merge_with_instance (self.obs_insts [c_i])
      self.obs_perclass_to_all_map [c_i] = obs_perclass_dup_idxes


      # To ensure obs_inst_all and pose_inst_all will correspond.
      # Take the overall indices, replace pose_insts[c_i].pose_obsl's
      #   per-class indices with their corresponding overall indices
      pose_obsl_c_augmented = deepcopy (self.pose_insts [c_i])

      # For each pose, replace its per-class obsl index with the overall obsl
      #   index.
      for i in range (pose_obsl_c_augmented.get_n_poses ()):

        # Get ith pose's obsl list with per-class indices
        obsl_i_c = pose_obsl_c_augmented.get_obsl_i (i)

        # Replace the per-class indices with the corresponding overall indices
        obsl_i_all = obs_perclass_dup_idxes [obsl_i_c].tolist ()

        #print ('Pose %d has overall pose_obsl: ' % (i))
        #print (obsl_i_all)

        # Set the instance's obsl to the overall index one
        pose_obsl_c_augmented.set_obsl_i (i, obsl_i_all)

      self.pose_perclass_to_all_map [c_i] = \
        self.pose_inst_all.merge_with_instance (pose_obsl_c_augmented)

      print ('')


  # =============================================================== Getters ==

  # Returns (obsl_i, pose_i), an obsl made at a random pose.
  # These are scalar indices that index self.obs_inst_all and
  #   self.pose_inst_all.
  # Difference from get_rand_obs_old() is that this ensures no duplicates
  #   with parent_poses_i, correctly! get_rand_obs_old() did not finish this
  #   feature implementation - picking from per-class poses is too much work
  #   to ensure this, requires 2 loops instead of 1.
  #   This fn picks directly from *_inst_all, instead of picking per-class
  #   and then converting to overall indices. This is better, as it is more
  #   evenly random, `.` some poses may appear in different classes, this fn
  #   gives them 1 count, get_rand_obs_old() gives them >1 count if they appear
  #   in >1 classes. It is also easier to convert this fn to sampling by
  #   obs_inst_all.obs_tallies instead of uniform random.
  # Parameters:
  #   parent_poses_i: Used to eliminate target poses that are duplicates of poses
  #     in this list, so that the same absolute pose isn't repeatedly
  #     executed, which doesn't give any new information.
  def get_rand_obs (self, parent_poses_i):

    n_tries = 0
    n_poses = self.pose_inst_all.get_n_poses ()

    # Loop until pick a random pose that has not been seeen in parent_poses_i.
    while True:

      # Pick a random pose
      rand_pose_i = np.random.randint (n_poses)
      n_tries += 1

      # If this pose repeats any parent pose, reject it
      if np.any (rand_pose_i == parent_poses_i):
        # No more poses to choose from. Just return a random one even if it
        #   repeats
        if n_tries == n_poses:
          print ('%sWARN prob_reader.py get_rand_obs(): No more poses to choose from that have not been visited! Returning Nones to indicate all training poses have been explored.%s' % (
            ansi_colors.WARNING, ansi_colors.ENDC))
          #break
          return None, None
        # Try another random one
        else:
          print ('DEBUG prob_reader.py get_rand_obs(): rand_pose_i %d repeats a parent pose. Choosing a different random pose...' % rand_pose_i)
          continue
      else:
        print ('DEBUG prob_reader.py get_rand_obs(): Chose rand_pose_i %d' % rand_pose_i)
        break


    # Get the obsl at the random pose
    obsl_at_pose_i = self.pose_inst_all.get_obsl_i (rand_pose_i)
    # Choose a random one
    rand_idx = np.random.randint (len (obsl_at_pose_i))
    # e.g. 4, chosen from [4,5] by rand_idx 0
    rand_obsl_i = obsl_at_pose_i [rand_idx]

    return rand_obsl_i, rand_pose_i


  # Returns (obsl_i, pose_i), an obsl made at a random pose.
  # These are scalar indices that index self.obs_inst_all and
  #   self.pose_inst_all.
  # Problem: This fn isn't done implementing the feature that the returned
  #   pose should not repeat poses in parent_poses_i. See "Problem" note in fn.
  # Parameters:
  #   parent_poses_i: Used to eliminate target poses that are duplicates of poses
  #     in this list, so that the same absolute pose isn't repeatedly
  #     executed, which doesn't give any new information.
  def get_rand_obs_old (self, parent_poses_i):

    # NOTE: It might be good to sample observations by their probabilities,
    #   instead of picking one at uniform random. That would be:
    #   rand_i = np.random.choice (get_n_obsl, p=obsl_inst_all.obsl_tallies)
    #   But then need to find the pose index corresponding to this obsl.


    # Pick a random pose
    rand_class_i = np.random.randint (len (self.cat_names))
    # print len(self.pose_insts)
    # print rand_class_i
    # print self.pose_insts [rand_class_i].get_n_poses ()


    n_tries = 0
    n_poses = self.pose_insts [rand_class_i].get_n_poses ()

    # Problem: if all poses in this class are exhausted, there are still other
    #   classes! Need an outer loop around this, keep a list of tried
    #   rand_class_i's, then chooses other rand_class_i if one class runs out
    #   of all poses. That's messy, rather just sample from obs_inst_all and
    #   pose_inst_all, since they now do correspond!
    # Loop until pick a random pose that has not been seeen in parent_poses_i.
    while True:

      rand_pose_i = np.random.randint (n_poses)

      # Convert per-class index to overall scalar index
      rand_all_pose_i = self.pose_perclass_to_all_map [rand_class_i] [rand_pose_i]
      n_tries += 1

      # If this pose repeats any parent pose, reject it
      if np.any (rand_pose_i == parent_poses_i):
        # No more poses to choose from. Just return a random one even if it
        #   repeats
        if n_tries == n_poses:
          print ('%sWARN prob_reader.py get_rand_obs(): No more poses to choose from that have not been visited! Just returning a pose, even though it has been visited before.%s' % (
            ansi_colors.WARNING, ansi_colors.ENDC))
          break
        # Try another random one
        else:
          print ('DEBUG prob_reader.py get_rand_obs(): rand_pose_i %d repeats a parent pose. Choosing a different random pose...' % rand_pose_i)
          continue
      else:
        print ('DEBUG prob_reader.py get_rand_obs(): Chose rand_pose_i %d' % rand_pose_i)
        break


    print ('Class has %d chords (obs)' % self.obs_insts [rand_class_i].get_n_single_obs ())
    print ('Class has %d obsl' % self.obs_insts [rand_class_i].get_n_obsl ())
    print ('Class has %d rows in obs_tallies' % self.obs_insts [rand_class_i].obsl_tallies.shape [0])

    print ('Class has %d poses' % self.pose_insts [rand_class_i].abs_poses.shape [0])
    print ('Class pose_obsl has %d items:' % len (self.pose_insts [rand_class_i].pose_obsl))
    #print (self.pose_insts [rand_class_i].pose_obsl)
    print ('Class obsl_list has %d items:' % len (self.obs_insts [rand_class_i].obsl_list))
    #print (self.obs_insts [rand_class_i].obsl_list)


    # Get the observation lists at this pose
    # e.g. it can be [1], or it can be [4, 5] if there are multiple obsl's at
    #   this pose.
    obsl_at_pose_i = self.pose_insts [rand_class_i].get_obsl_i (rand_pose_i)
    print ('obsl_at_pose_i, i = %d:' % rand_pose_i)
    print (obsl_at_pose_i)
    # Choose a random one
    rand_idx = np.random.randint (len (obsl_at_pose_i))
    # e.g. 4, chosen from [4,5] by rand_idx 0
    rand_obsl_i = obsl_at_pose_i [rand_idx]
    print ('rand_obsl_i:')
    print (rand_obsl_i)

    # Test this. Bad behavior
    #rand_all_pose_i = 9
    # Convert per-class index to overall scalar index
    rand_all_obsl_i = self.obs_perclass_to_all_map [rand_class_i] [rand_obsl_i]


    print ('Overall %d discretized chords (obs)' % self.obs_inst_all.get_n_single_obs ())
    print ('Overall %d obsl' % self.obs_inst_all.get_n_obsl ())
    print ('Overall %d rows in obs_tallies' % self.obs_inst_all.obsl_tallies.shape [0])

    print ('Overall %d poses' % self.pose_inst_all.abs_poses.shape [0])
    print ('Overall pose_obsl has %d items:' % len (self.pose_inst_all.pose_obsl))
    #print (self.pose_inst_all.pose_obsl)
    print ('Overall obsl_list has %d items:' % len (self.obs_inst_all.obsl_list))
    #print (self.obs_inst_all.obsl_list)

    print ('self.pose_perclass_to_all_map:')
    #print (self.pose_perclass_to_all_map)
    print ('self.obs_perclass_to_all_map:')
    #print (self.obs_perclass_to_all_map)

    print ('Convert per-class index to overall index:')
    print ('rand_all_pose_i:')
    print (rand_all_pose_i)
    print ('rand_all_obsl_i:')
    print (rand_all_obsl_i)

    # Preserving the API of returning (obsl_i, pose_i), in case later change
    #   mind and want to do individual observations at each pose again.
    return rand_all_obsl_i, rand_all_pose_i


  # ============================================== Conditional getters ==

  # Counterpart of get_obs_tgt_list() and get_obs_tgt_list_n() in
  #   active_touch prob_of_observs.py used for triangles.
  # Looks for target observation candidates z1 that are in the neighborhood
  #   of target pose p1, p1 = A1 * p0. p0's are source poses, found by looking
  #   at all poses that yielded source observation z0.
  # Parameters:
  #   src_obsl_i: Integer name of z_t, source observation.
  #   action: Relative action, actual data
  #   tgt_pose_i: Integer name of target pose. Main input, found as action via
  #     tree policy in MCTS. Used to calculate distance within which to take
  #     candidate neighbors.
  #   parent_poses_i: Used to eliminate target poses that are duplicates of poses
  #     in this list, so that the same absolute pose isn't repeatedly
  #     executed, which doesn't give any new information.
  # Returns observations and per-class probabilities.
  #def get_obs_near_tgt_pose (self, tgt_pose_i, parent_poses_i):
  def find_tgt_obs_candidates (self, src_obsl_i, action, parent_poses_i):

    # 1D parallel arrays, size nObs. Integer names of data types
    obsl_i = np.zeros ((0, ), dtype=int)
    # Probability within a class, p(z_t+1 | y, z_t, a_t)
    probs = np.zeros ((0, ))
    poses_i = np.zeros ((0, ), dtype=int)

    # nClasses size
    percat_counts = []


    # TODO actions need to be relative even in each node after all, cannot be
    #   absolute. This needs to be changed in mcts_active_touch.py.


    #####
    # Find all poses ({p0}) that had observation obs_src (z0)

    # Condition on z_t and a_t+1, to get p(z_t+1 | y, z_t, a_t+1)
    # z_t is associated with absolute pose p_t in training. In order to
    #   condition on z_t and a_t+1, need to use p_t * a_t+1 to find p_t+1,
    #   the next absolute pose by a_t+1. Then take observations z_t+1 seen at
    #   p_t+1.
    # p_t needs to be found by looking at all poses that yielded z_t.

    # To hold all src poses p0, where observation z0 was seen
    # Allocate more than enough, then truncate after populating in loop, to
    #   make it faster
    #tgt_abs_poses = np.zeros ((self.pose_inst_all.get_n_poses (),
    #  self.POSE_DIMS))
    #n_tgt_poses = 0

    parent_poses = self.pose_inst_all.get_pose_i (parent_poses_i)

    # Find poses that had observation z0, which is a scalar overall-class index
    #print ('src_obsl_i:')
    #print (src_obsl_i)
    _, src_poses = self.pose_inst_all.find_poses_with_obsl (src_obsl_i)
    #print ('Found %d poses satisfying z0' % (src_poses.shape [0]))
    
    tgt_poses = np.zeros (src_poses.shape)
    
    # Loop through each p0, compute p1 = A1 * p0
    for p_i in range (src_poses.shape [0]):
    
      # Apply action a1 to p0, to get p1
      tgt_poses [p_i, :] = calc_result_plane (src_poses [p_i, :], action)
    
    # Copied tiling-to-3D code from mat_util.py find_dups_multirow_onelist()
    # Tile in 3rd dimension, so the matrix becomes nRows x 1 x nCols,
    #   depth x rows x cols
    tgt_poses_3d = tgt_poses.reshape (tgt_poses.shape [0], 1,
      tgt_poses.shape [1])


    # Loop through each class to look for observations at target pose
    #   candidates {p1}.
    # Need to look at each class, as opposed to using self.obs_inst_all, `.`
    #   obs_inst_all is not guaranteed to correspond with pose_inst_all, the
    #   way I build these two vars!! So I wouldn't be able to look up the
    #   observation (z1) at each chosen neighborhood target pose (p1).
    #   Only the per-class obs_insts[c_i] and pose_insts[c_i] correspond,
    #   the way I built them in prob_writer.py.
    #   > Now they do correspond. Can simplify by moving more code inside
    #     the loop to before loop TODO
    for c_i in range (0, len (self.cat_names)):

      # Result of subtraction is nFirstSubtractant x nSecondSubtractant x nDims,
      #   depth x rows x cols.
      # Sum across the axis of nDims size. Result is
      #   nFirstSubtractant x nSecondSubtractant, i.e. nAbsPoses x nTgtPoses.
      # It doesn't matter which target pose an absolute pose corresponds to.
      #   There are multiple target poses `.` multiple source poses could have
      #   z0. We just need to take all abs_poses that are within a neighborhood
      #   of ANY of the target poses. Each row is an abs_pose. To take ANY
      #   abs_pose, just OR across the nTgtPoses columns.
      nbrs_dist = np.sqrt (np.sum ((
        self.pose_insts [c_i].abs_poses - tgt_poses_3d) ** 2, axis=2))
      dist_thresh = np.min (nbrs_dist) + 1.5 * np.std (nbrs_dist)
      # Values index abs_poses
      nbr_poses_i = np.where (np.any (nbrs_dist < dist_thresh, axis=1)) [0]

      # For tgt_pose of 1 row
      '''
      # Find neighbors within a radius of target pose.
      #   Sum across the axis of nDims size. Result is vector of n_abs_poses
      #   elts.
      nbrs_dist = np.sqrt (np.sum ((
        self.pose_insts [c_i].abs_poses - tgt_pose) ** 2, axis=1))
      dist_thresh = np.min (nbrs_dist) + 1.5 * np.std (nbrs_dist)
      nbr_poses_i = nbrs_dist < dist_thresh
      '''

      # Eliminate visited abs poses
      # TODO 20 Feb 2017: This fails to do any duplicate elimination.
      #   nbr_poses always prints a rounded value, not sure why. Now added
      #   a check in the p_i loop below to not add duplicates, but this fn
      #   should really be rewritten to be rid of the c_i loop, just find
      #   observations z1 and p1 directly in self.obs_inst_all and
      #   self.pose_inst_all.
      nbr_poses = self.pose_insts [c_i].get_pose_i (nbr_poses_i)

      # Indices index arg1 (incoming), values index arg2 (existing)
      #dup_idx = find_dups_multirow_onelist (parent_poses, nbr_poses)
      dup_idx = find_dups_multirow_onelist (nbr_poses, parent_poses)

      # Delete neighbor poses that are duplicates visited in parents
      nbr_poses_i = np.delete (nbr_poses_i, np.where (dup_idx != -1))

      # TODO
      # Actually it's much easier to find duplicate poses by moving this
      #   out of the loop (now that pose_inst_all corresponds with
      #   obs_inst_all), and comparing overall-class
      #   integer names in nbr_poses_i and parent_poses_i. But that needs
      #   tiling to 2D too, and I need to write new code for that. Might as
      #   well compare nbr_poses and parent_poses for now, as I already have
      #   code that tiles to 3D.
      

      #print ('')
      #print ('dup_idx: %s' % dup_idx)

      #print ('nbr_poses_i:')
      #print (nbr_poses_i)
      # ???? Why are these always rounded values??? Where are these rounded??? TODO
      #print ('nbr_poses:')
      #print (nbr_poses)

      #print ('parent_poses_i:')
      #print (parent_poses_i)
      #print ('parent_poses:')
      #print (parent_poses)

      #print ('These nbr_poses duplicate parent_poses, will be eliminated from being the next node')
      #print (np.where (dup_idx != -1))


      # To be added to poses_i. All items in nbr_poses_i, and some are
      #   replicated several times, to match the length of obsl_i, so that
      #   poses_i and obs_i returned are parallel arrays.
      curr_poses_i = np.zeros ((0, ), dtype=int)
      curr_obsl_i = []

      #print ('self.obs_perclass_to_all_map:')
      #print (self.obs_perclass_to_all_map)

      # For each neighbor pose, get its observations
      # Cannot vectorize this, `.` PoseList.obsl_list is a list, cannot be np
      #   array (`.` each list can have different number of elts).
      for p_i in nbr_poses_i:

        # List of integers that index self.obs_insts [c_i].obsl_list.
        #   e.g. [4, 5]. This is specific to class c_i
        class_obsl_i = self.pose_insts [c_i].get_obsl_i (p_i)
        n_obsl = len (class_obsl_i)

        #print ('class_obsl_i:')
        #print (class_obsl_i)

        # Convert class-specific index (c, i) to all-classes index (i) that
        #   indexes self.obs_inst_all.
        # self.obs_perclass_to_all_map [c_i] is a 1D NumPy array. If
        #   class_obsl_i has more than one element, it'll return a 1D
        #   NumPy array with integer indices in obs_inst_all.
        all_obsl_i = self.obs_perclass_to_all_map [c_i] \
          [class_obsl_i]
        all_poses_i = self.pose_perclass_to_all_map [c_i] [p_i]

        #print ('all_obsl_i:')
        #print (all_obsl_i)

        # If this pose is not a duplicate in parent_poses, add it.
        # NOTE: This is a catch-all of the failure to eliminate duplicate
        #   poses above in nbr_poses_i. This job should have been done above
        #   but for some reason nbr_poses always prints out some rounded value,
        #   so it's never found as a duplicate of parent_poses. Not sure why
        #   it's rounded. Added this if-stmt as a temporary quick solution.
        #   Should just eliminate this c_i loop entirely, and operate
        #   simply directly on self.pose_inst_all!!!!!! TODO 20 Feb 2017
        # > Problem: This is not good. It stops tree search way too early
        #   in every simulation. This makes tree less explored in tree policy
        #   AND rollout, which is bad. If you want to eliminate poses that are
        #   the same, just do it in sel_path()! Don't do it here, this hinders
        #   seeing the entire tree! In the end you get a teeny tree! This is
        #   the FINAL conclusion from 20 Feb 2017.
        #if np.all (all_poses_i != parent_poses_i):

        # Add all-classes index of this obsl to the bigger list
        curr_obsl_i.extend (all_obsl_i.tolist ())
        # Make poses_i's size consistent with obsl_i's size. Concatenate
        #   as many poses as there are observations at this pose. This way,
        #   ret val poses_i will be parallel array with obsl_i.
        curr_poses_i = np.append (curr_poses_i, np.tile (all_poses_i,
          (n_obsl, )))

        #print ('curr_obsl_i:')
        #print (curr_obsl_i)
        #print ('')

      # end nbr_poses_i

      #print ('p1 candidates for next node, curr_poses_i:')
      #print (curr_poses_i)
      #print (self.pose_inst_all.get_pose_i (curr_poses_i))

      #print (self.obs_insts [c_i].obsl_tallies)

      curr_probs = self.obs_inst_all.obsl_tallies [curr_obsl_i]
      # Normalize the probabilities, to get probability within this class.
      #   That means need sum=1 in return value for per-class prob. This
      #   loop already contains only per-class tallies, so just divide by sum.
      curr_probs /= (float (np.sum (curr_probs)))

      # Append current class's quantities to ret vals
      poses_i = np.append (poses_i, curr_poses_i)
      obsl_i = np.append (obsl_i, np.array (curr_obsl_i))
      probs = np.append (probs, curr_probs)
      percat_counts.append (curr_probs.size)

    # end classes

    #tgt_abs_poses = np.delete (tgt_abs_poses,
    #  range (n_tgt_poses, tgt_abs_poses.shape [0]), axis=0)

    #print ('poses_i:')
    #print (poses_i)
    #print ('obsl_i:')
    #print (obsl_i)
    #print (probs)
    #print (percat_counts)

    if poses_i.size == 0:
      print ('%sWARN: poses_i.size=0. No z1 candidates are found for next node! This could happen because all high-probability poses have been seen in previous nodes on the traversal path.%s' % (
        ansi_colors.WARNING, ansi_colors.ENDC))


    # obsl_i: Integer names of observations, indexes obs_inst_all
    # probs: [0, 1] probabilities. Per class's probs sum to 1
    # poses_i: Integer names of poses, indexes pose_inst_all
    # percat_counts: List of nClasses elements. Each element is an integer,
    #   telling you how many elts in the other three parallel lists are in
    #   each class, e.g. [3, 10, 13] means 3 are in class 1, 10 are in class
    #   2, 13 are in class 3, etc. Caller then knows how to access the other
    #   3 lists by class.
    return obsl_i, probs, poses_i, percat_counts



# Register children of abstract base class
# Ref: https://docs.python.org/2/library/abc.html
#ProbReader.register (ProbReader_Chords)


