#!/usr/bin/env python

# Mabel Zhang
# 12 Dec 2016
#
# Copied and adpated from triangle_sampling prob_writer.py
#
# Writer functions for probabilities data, to be stored during training for
#   active touch recognition.
#


import rospy

# Python
import argparse
import os
import pickle

import numpy as np

# My packages
from util_continuum.ansi_colors import ansi_colors
from util_continuum.config_paths import get_prob_path
from chord_training_msgs.srv import ProbWriterSrv, ProbWriterSrvResponse
from chord_recognition.chord_reader import ChordReader
from chord_recognition.chord_consts import ChordConsts as cc, \
  get_discretization_widths
from util_continuum.mat_util import find_dups_row, find_dups_multirow_onelist, \
  find_dups_self

# Local
from chord_recognition.parse_args import config_params_from_args, \
  parse_args_for_histogram, get_ints_from_comma_string, \
  get_floats_from_comma_string
from config_pkl import ActivePKLConsts as ActivPKL
from observation_list import ObservationList_Chords
from pose_list import PoseList_Chords


# ============================================================= Writer class ==

class ProbWriter:

  # Unused. Now just use float for all. `.` int 2 is just equivalent to 0.01.
  #   Float type is more versatile description.
  #DISC_FLOAT = 0
  #DISC_INT = 1


  # All parameters now passed in through rosservice call!
  # Parameters:
  #   disc_fmt: Format of discretization setting given, DISC_FLOAT or DISC_INT.
  #   discretize_q_chord: Discretize data to make smaller files to store on
  #     disc. Quantities to discretize: observations (chord length and angles),
  #     actions (quaternions btw two planes).
  def __init__ (self, mode): # obj_center, disc_fmt, discretize_l_ang_q=(0,0,0)):

    # Ret val must be stored in scope, else service can stop receiving callbacks
    self.chord_srv = rospy.Service ('/chord_training/prob_srv', ProbWriterSrv,
      self.handle_prob_srv)

    # Paths to save active training data
    #   active_training/costs_gz/, active_training/probs_gz/
    #self.costs_root, 
    self.probs_root = get_prob_path (mode)
    #self.costs_name = ''
    self.probs_name = ''

    # Cost not implemented. Don't need to store costs, waste of space. Compute
    #   at test time.
    #self.STORE_COSTS = False
    #if self.STORE_COSTS:
    #  if not os.path.exists (self.costs_root):
    #    os.makedirs (self.costs_root)
    if not os.path.exists (self.probs_root):
      os.makedirs (self.probs_root)

    # Initialize arrays for storing data
    self.reset_vars ()


    # Copied these to handle_prob_srv() and update accordingly
    # All parameters now passed in through rosservice call!
    '''
    self.mode = mode
    self.obj_center = obj_center


    # Discretization amount in number of decimal places. 0 means no
    #   discretization.
    #   Discretization of > 0 means the 3D space of wrist pose gets
    #   divided into cells, instead of continuous space. This saves time saving
    #   probabilities to disk, and saves disk space, as there will be fewer
    #   relative poses (action a) to save as dictionary keys!
    self.DISCRETIZE_L = discretize_l_ang_q [0]
    self.DISCRETIZE_ANG = discretize_l_ang_q [1]
    self.DISCRETIZE_Q = discretize_l_ang_q [2]

    # If floating point, treat it as the discretization cell width.
    #   Find how many decimal places it is
    if disc_fmt == self.DISC_FLOAT:
    #if not isinstance (self.DISCRETIZE_L, int):
      self.RND_PLACES_L = n_decimal_places (self.DISCRETIZE_L)
    else:
      self.RND_PLACES_L = self.DISCRETIZE_L

    if disc_fmt == self.DISC_FLOAT:
    #if not isinstance (self.DISCRETIZE_ANG, int):
      self.RND_PLACES_ANG = n_decimal_places (self.DISCRETIZE_ANG)
    else:
      self.RND_PLACES_ANG = self.DISCRETIZE_ANG

    #if disc_fmt == self.DISC_FLOAT:
    if not isinstance (self.DISCRETIZE_Q, int):
      self.RND_PLACES_Q = n_decimal_places (self.DISCRETIZE_Q)
    else:
      self.RND_PLACES_Q = self.DISCRETIZE_Q
    '''


  # Init / Re-init all data fields to empty arrays.
  # This function must be called before each object, to clear out all data
  #   fields from the previous object in Gazebo training (in case there
  #   was one).
  # Alternatively, you could just instantiate another instance of the
  #   class - probably safest option to clear out everything, but not
  #   space-efficient.
  def reset_vars (self):
 
    # Theoretically, only need 3 lists, and 1 numpy matrix.
    #   Lists are of objects like chords, observations, and actions. So:
    #     1. list of classes
    #     2. list of chords
    #     3. list of list of integers indexing the list of chords (this is the
    #       list of observations, `.` we want each observation to be a list
    #       of chords)
    #     4. list of 4-tuple quaternion (actions)
    #   The numpy matrix:
    #     4D matrix. |y| x |P(z)| x |a| x |P(z)|, where P(.) is power set.
    #       Indices are triplets, (z0, a, z1)
    #       Values are p(z1 | y, z0, a)
    #     Indices of numpy matrix index the action and observation lists to
    #       grab the items.
    #     Values of numpy matrix are tallies! So that you can marginalize over
    #       different probabilities simply by np.sum()!!!
    #   But we won't store the O(n^2) matrix. So we store a few more lists
    #     instead.

    self.chord_dim = 7
    self.obs_inst = ObservationList_Chords (self.chord_dim)
    '''
    # |C| x 7. Integer names for chords
    self.chords = np.zeros ((0, 7))

    # |Z| x 1. Integer names for observations. Indices are integer names.
    #   List of list of chord indices.
    #   List of chords may be different sizes, .`. can't use numpy 2D array,
    #     need a Python list.
    #     Ref: http://stackoverflow.com/questions/3386259/how-to-make-a-multidimension-numpy-array-with-a-varying-row-size
    self.observs = []
    # |Z| x 1. Row i contains tallies of how many times observation i was seen
    self.obs_tallies = np.zeros (0)
    '''

    self.pose_dim = 3
    self.pose_inst = PoseList_Chords (self.pose_dim)
    '''
    # |P| x 3. Integer names for absolute poses
    #   Each row is a [x, y, z] unit vector, normal to cross sectn plane
    self.abs_poses = np.zeros ((0, self.pose_dim))
    # |P| x 1. Index indexes self.abs_poses.
    #   List of list of observation indices, i.e. what observations were made
    #     at this pose. For a single object, each elt may only have 1 item.
    #     But when merge multiple objects at test time, each elt may have >1
    #     items. Initializing as a list of lists makes it easier to combine
    #     objs at test time in active_chords package prob_of_observs.py.
    self.pose_obsl = []
    '''

    # 4D NumPy matrix
    #   Used to marginalize p(z1 | z0, y, a)
    # This doesn't exist here, only in prob_of_observs.py, as sparse matrix
    #   TODO implement this in the new prob_of_observs.py
    #tallies = np.zeros ((1, 0, 0, 0))

    # Don't need this here, only have access to one object.
    #   TODO Put this in prob_of_observs.py, where meta file is loaded and
    #   know how many classes we're running on in the end.
    #classes = []

    self.pkl_wrapper = {}


  def get_obs_inst (self):
    return self.obs_inst

  def get_pose_inst (self):
    return self.pose_inst


  # ROS service handler
  #   mode: 'gl', 'real', or 'rand'. Used to determine directory
  #   obj_base: basename to use in output file's name
  def handle_prob_srv (self, req):

    # Blocking call. Do everything before returning

    if req.valid:

      # string
      self.mode = req.mode
     
      # TODO: Do one file per object for now
      #   To load a whole category, will need to load each object in that class
      #     one by one, and combine the matrices. That means, if a matrix element
      #     with the same index quadruplet (y, z0, a, z1) already exists, then 
      #     combine the two elements. Easiest way to combine is, maintain a matrix
      #     of integer division factors. Same size as the big 4D numpy array.
      #     Values are number of cells to combine into this one cell, i.e.
      #     number of object files that have this quadruplet element. After
      #     checking duplicates of all matrix indices, sum the multiple matrices
      #     element-wise, and then element-wise divide by the integer factor
      #     matrix. This takes the average tally for each matrix element across
      #     all objects of the class.
      #self.costs_name = os.path.join (self.costs_root, req.obj_cat,
      #  req.obj_base + '.pkl')
      self.probs_name = os.path.join (self.probs_root, req.obj_cat,
        os.path.splitext (req.obj_base) [0] + '.pkl')

      cat_path = os.path.dirname (self.probs_name)
      if not os.path.exists (cat_path):
        os.makedirs (cat_path)

      # Don't need this. `.` abs_pose in continuum arm isn't a position, it's
      #   a vector normal to cross section plane. Don't need to offset the
      #   pose by obj_center to put all poses in object frame.
      # geometry_msgs/Point
      #obj_center = np.array ([req.obj_center.x, req.obj_center.y,
      #  req.obj_center.z])
     
      # Discretization amount in number of decimal places. 0 means no
      #   discretization.
      #   Discretization of > 0 means the 3D space of wrist pose gets
      #   divided into cells, instead of continuous space. This saves time saving
      #   probabilities to disk, and saves disk space, as there will be fewer
      #   relative poses (action a) to save as dictionary keys!
      disc_chord = get_discretization_widths (req.disc_l, req.disc_ang)

      # Load chords from temp file
      chord_reader = ChordReader (chord_name=req.chord_name)
      chords = chord_reader.get_chords ()

      # Now loading from file
      self.add_abs_pose_and_obs (req.abs_pose, chords, req.disc_pose,
        disc_chord)
      # When chords was still a field in the rosservice request
      #   Remove when line above works
      #self.add_abs_pose_and_obs (req.abs_pose, req.chords, req.disc_pose,


    if req.finished:
      print ('Received finished signal')

      # Assumption: disc_pose is the same for every service call! Otherwise,
      #   only the one from the last call is written to file.
      # HM
      disc_chord = get_discretization_widths (req.disc_l, req.disc_ang)
      self.write_probs (req.obj_cat, req.obj_base, req.disc_pose, disc_chord)

      print ('Observation tallies:')
      print (self.obs_inst.obsl_tallies)
      print ('List of observations at each pose (pose: observation_list):')
      print (self.pose_inst.pose_obsl)

      self.reset_vars ()
      print ('Resetted data structures to listen to next object. Safest way to save a new object is to RESTART this script though.')
      print ('')

    print ('')


    return ProbWriterSrvResponse (True)



  # ========================================= Add entries to data structures ==

  # Add one (abs_pose: observation) row to the data structures
  #   observation is a list of chords.
  # Parameters:
  #   abs_pose: geometry_msgs/Vector3. A relative action is between two
  #     absolute poses.
  #     For a kinematic arm, this can be e.g. the end effector pose.
  #     For a continuum arm, this is a cross section in which the
  #     continuum arm moves into.
  #   chords: List of Chords, chord_training_msgs/Chord[]
  def add_abs_pose_and_obs (self, abs_pose, chords, disc_pose, disc_chord):

    # Actions and observations are simply stored as linear lists.
    # Cross products of observation-action-observation (z0, a, z1) triplets
    #   are computed at run-time as needed. This is to svae space, storing
    #   O(n) instead of O(n^2), where n is the number of observations.

    # NOTE: For run-time speed, for all the np.append()s, preallocate some amt
    #   so that the append operations are faster. Keep a count of how many
    #   rows are actually filled with valid data. Then remove the empty rows.


    # Populate self.chords
    chords_idx, n_chords_added = self.add_chords (chords, disc_chord)

    # Populate self.observs, self.obs_tallies
    obs_dup_idx = self.add_observation (chords_idx, n_chords_added)
    print ('obs_dup_idx:')
    print (obs_dup_idx)

    # Populate self.abs_poses, self.pose_obsl
    self.add_pose (abs_pose, disc_pose, obs_dup_idx)


  # Input: chords in args
  # Output: updated self.chords, chords_idx
  # Parameters:
  #   chords: chord_training_msgs/Chord[]
  #   disc_chord: discretization factors for each value in chord, a 1D array
  def add_chords (self, chords, disc_chord):

    # n x 7
    chords_np = np.zeros ((chords ['L_STR'].shape [0], self.chord_dim))

    # Convert chord_training_msgs/Chord[] into a NumPy 2D array
    # Now loading chords using chord_reader.py
    chords_np [:, cc.L_STR_IDX] = chords ['L_STR']
    chords_np [:, cc.C_LON_IDX] = chords ['C_LON']
    chords_np [:, cc.C_LAT_IDX] = chords ['C_LAT']
    chords_np [:, cc.N0_LON_IDX] = chords ['N0_LON']
    chords_np [:, cc.N0_LAT_IDX] = chords ['N0_LAT']
    chords_np [:, cc.N1_LON_IDX] = chords ['N1_LON']
    chords_np [:, cc.N1_LAT_IDX] = chords ['N1_LAT']

    # When was still using chords as a field in rosservice call. Could remove
    #   when above works.
    #for i in range (0, len (chords)):
      #z = chords [i]
      #chords_np [i, :] = np.array (
      #  [z.l, z.c_lon, z.c_lat, z.n0_lon, z.n0_lat, z.n1_lon, z.n1_lat])

    # Discretize. axis=1, columns are same dimensions
    chords_disc = discretize (chords_np, disc_chord, 1)

    # Add incoming chords to self.chords
    chords_idx, n_chords_added = self.obs_inst.add_single_obs_array (
      chords_disc)

    return chords_idx, n_chords_added


  # Input: chords_idx. This is the observation, i.e. a list of chord indices.
  # Output: updated self.obesrvs, self.obs_tallies
  def add_observation (self, chords_idx, n_chords_added):

    # If there were no new chords, then this could be a duplicate observation.
    #   Search through self.observs list, to see if the incoming observation
    #   (list of chords) is already listed.
    if n_chords_added > 0:
      print ('observation IS NOT a duplicate')
      obs_dup_idx = self.obs_inst.add_nondup_obsl (chords_idx)

    # If there are new chords, then the incoming observation (a list of chords)
    #   won't exist yet.
    else:
      print ('observation MAY BE a duplicate')
      obs_dup_idx = self.obs_inst.check_dup_and_add_obsl (chords_idx)

    self.obs_inst.print_obsl_list ()

    return [obs_dup_idx]


  # Input: abs_pose
  # Output: Updated self.abs_poses, self.pose_obsl
  def add_pose (self, abs_pose, disc_pose, obs_dup_idx):

    # Normalize to unit vector, in case it's not
    abs_pose_np = np.array ([abs_pose.x, abs_pose.y, abs_pose.z])
    abs_pose_np /= np.linalg.norm (abs_pose_np)

    # Discretize
    # axis=1, columns are same dimensions
    abs_pose_np = discretize (abs_pose_np, disc_pose, 1)

    # Add to existing data so far
    self.pose_inst.check_dup_and_add_pose_obsl (abs_pose_np, obs_dup_idx)


  # ======================================== Compute and store probabilities ==

  # Internal function, do not call from outside
  def write_probs (self, obj_cat, obj_base, disc_pose, disc_chord):

    print ('%sWriting probabilities data to %s ...%s' % (
      ansi_colors.OKCYAN, self.probs_name, ansi_colors.ENDC))

    # If this key doesn't exist in dictionary yet, init it
    if obj_cat not in self.pkl_wrapper.keys ():
      # Store the category string to file, not the integer name, `.` at test
      #   time, I might load different categories, so I will want to regive
      #   them an integer name at test time.
      self.pkl_wrapper [obj_cat] = {}

    # 'obj_base'
    if ActivPKL.OBJ_BASE not in self.pkl_wrapper [obj_cat].keys ():
      self.pkl_wrapper [obj_cat] [ActivPKL.OBJ_BASE] = obj_base

    # If any (abs_pose: observation) is populated, write to file
    if self.pose_inst.get_n_poses () >= 0:
      with open (self.probs_name, 'wb') as f:
        # Data
        self.pkl_wrapper [obj_cat] [ActivPKL.CHORDS] = self.obs_inst.single_obs  #self.chords
        self.pkl_wrapper [obj_cat] [ActivPKL.OBSERVS] = self.obs_inst.obsl_list  #self.observs
        self.pkl_wrapper [obj_cat] [ActivPKL.OBS_TALLIES] = self.obs_inst.obsl_tallies  #self.obs_tallies
        self.pkl_wrapper [obj_cat] [ActivPKL.ABS_POSES] = self.pose_inst.abs_poses  #self.abs_poses
        self.pkl_wrapper [obj_cat] [ActivPKL.POSE_OBSL] = self.pose_inst.pose_obsl  #self.pose_obsl

        # Param
        self.pkl_wrapper [obj_cat] [ActivPKL.DISC_POSE] = disc_pose
        self.pkl_wrapper [obj_cat] [ActivPKL.DISC_CHORD] = disc_chord

        pickle.dump (self.pkl_wrapper, f, pickle.HIGHEST_PROTOCOL)

    print ('Done')


# =========================================================== Utility fns ==

# Parameters:
#   value: NumPy array, some dimensions, doesn't matter.
#     Values to discretize. Discretization is element-wise.
#   cell_w: Float, scalar or a 1D array. Cell widths of discretization grid.
#     If scalar, each elt in values will be discretized by it.
#     If a 1D array, will check which dimension in values array has the same
#       size as cell_w. Discretize each row in that dimension by one elt in
#       cell_w. E.g. values is 4 x 2, cell_w is 4 elts, then elts in row i in
#       values will be discretized by cell_w[i].
#   axis: The axis along which values and cell_w have the same dimension.
#     This helps when cell_w is a 1D array (as opposed to scalar). 
#     E.g. values is nSamples x nDims, cell_w is 1 x nDims, then axis=1.
def discretize (values, cell_w, axis=None):

  if type (cell_w) == float:
    # Divide each elt by the scalar width
    tmp = np.asarray (values) / cell_w

    # Truncate decimals, multiply by bin width. This gives you the low
    #   limits of the bin that the position should fall in.
    # NOTE: NumPy has a floating point error. To reproduce:
    #   value = np.arange (-10, 10, 0.6)
    #   cell_w = 0.5
    #   Pass into this fn, then you'll see 16.0 is truncated to 15, 4.0 is
    #   truncated to 3, etc. I don't know why that happens. Then they're put
    #   into wrong discretize bin - one bin lower! Not sure if that'll lead to
    #   a big problem in our probabilities data. But it sucks!!!!! I don't
    #   know how to get around this problem. nparray.astype(int), np.trunc(),
    #   np.floor() all have this problem.
    #   If you pass in fewer values in np.arange, then this doesn't happen.
    #   It's also not Python's float division problem, `.` they get 16.0 and
    #   4.0 fine. It's at numpy's truncate where this problem starts.
    # NOTE: Negative numbers will be put into one bin higher, with this
    #   discretization method. e.g. with bin width 0.5, -0.4 will get put into
    #    bin 0.0, but it should be in bin -0.5, `.` we always put them into
    #    lower-end bin. This may result in bins around 0 getting more tallies.
    #    But as long as data is consistent across all objs, this might not be
    #    a big problem?
    #    To solve this, you could check if value < 0, then use np.ceil()
    #    instead of np.trunc(). But that'll slow down the discretization.
    disc_values = np.trunc (tmp) * cell_w

  # else assume type (cell_w) == np.ndarray
  else:
    assert (type (cell_w) == np.ndarray)

    if (cell_w.ndim != 1):
      print ('%sERROR: prob_writer.py discretize(): cell_w is neither a 1D array nor scalar float. Cannot discretize! Check your discretize() call.%s' % (
        ansi_colors.FAIL, ansi_colors.ENDC))
      return values

    # Find the axis in values that is same size as cell_w
    mat_shape = np.array (values.shape)

    if axis is None:
      disc_axis = np.where (mat_shape == cell_w.size)

      # This only happens when values is a square matrix, nDims == nSamples
      if disc_axis [0].size > 1:
        # Assume axis=1, the nDims most likely, in a 2D matrix of 
        #   nSamples x nDims
        disc_axis = disc_axis [1]
        print ('%sWARN: values being discretized is a square matrix. Assuming it is %d%s' % (
          ansi_colors.WARNING, disc_axis, ansi_colors.ENDC))
      else:
        disc_axis = np.asscalar (disc_axis [0])
    else:
      disc_axis = axis

    # Do not tile the dimension that is same size as cell_w
    mat_shape [disc_axis] = 1

    # Same size as "values"
    cell_w_tiled = np.tile (cell_w, mat_shape)

    # Divide element-wise by the discretization factor
    tmp = np.divide (np.asarray (values), cell_w_tiled)

    disc_values = np.multiply (np.trunc (tmp), cell_w_tiled)


  #print ('Before discretization:')
  #print (values)
  #print ('After discretization:')
  #print (disc_values)


  # NOTE: If cell_w is larger than value, then everything will fall into 1
  #   discretized bin! Caller is responsible for finding a valid bin width.
  # This also prints if this specific entry is discretized to the 0 bin.
  #   So look how many times this msg is printed, if from time to time, then
  #   it is fine. But if every time, then you should make width bigger.
  #if np.all (np.asarray (values)) < cell_w:
  # 6 Jan 2016: Is the above closing parenthesis misplaced???
  if np.all (np.asarray (values) < cell_w):
    print ('%sWARN: prob_writer.py discretize(): bin size is larger than the values to discretize. If this prints for all values, then all values might fall into the same bin! You should specify a smaller DISCRETIZE_* to prob_writer.py. %s' %(
      ansi_colors.WARNING, ansi_colors.ENDC))

  return disc_values


def n_decimal_places (val):

  # If val has more than this number of decimal places, just ignore the rest,
  #   truncate at this many
  MAX_PLACES = 6

  for i in range (0, MAX_PLACES):
    # If 10^i multiplied by this number gets > 1, then i is the number of
    #   decimal digits in val.
    #   e.g. 0.1 * 10^1 >= 1, 0.01 * 10^2 >= 1, 0.05 * 10^2 >= 1
    if val * 10 ** i >= 1:
      break

  return i


def main ():

  rospy.init_node ('prob_writer', anonymous=True)

  # API https://docs.scipy.org/doc/numpy/reference/generated/numpy.set_printoptions.html
  np.set_printoptions (precision=3)


  #####
  # Parse command line args
  # Only these ones are used: --gl, --rand, --real (for mode),
  #   and ones defined in this file.

  arg_parser = argparse.ArgumentParser ()

  #arg_parser.add_argument ('--disc_float', type=str, default='',
  #  help='Discretization of (length, angle, quaternion), e.g. 0.06,0.08,0.05. --disc_float and --disc_int are interpreted differently, only specify ONE. Int: number of decimal places to keep. Float: Discretization grid size (width).')
  #arg_parser.add_argument ('--disc_int', type=str, default='',
  #  help='Discretization of (length, angle, quaternion), e.g. 2,1,2. --disc_float and --disc_int are interpreted differently, only specify ONE. Int: number of decimal places to keep. Float: Discretization grid size (width).')

  args, valid = parse_args_for_histogram (arg_parser)
  if not valid:
    return

  mode = config_params_from_args (args)


  # Unused. Now only use float mode, better and covers int mode anyway
  # Parse custom args only in this file
  '''
  # If both missing, or both specified, print err msg
  if (not args.disc_float and not args.disc_int) or \
    (args.disc_float and args.disc_int):
    print ('%sMust specify EXACTLY ONE of --disc_float or --disc_int. Try again.%s' % (
      ansi_colors.FAIL, ansi_colors.ENDC))
    return

  # Now only one should be specified, check which one is given
  elif args.disc_float:
    disc_fmt = IOProbs.DISC_FLOAT
    disc_l_ang_q = get_floats_from_comma_string (args.disc_float)

  elif args.disc_int:
    disc_fmt = IOProbs.DISC_INT
    disc_l_ang_q = get_ints_from_comma_string (args.disc_intt)

  else:
    print ('%sThis should never happen. If this prints, correct code. There are two string vars, either empty or not empty, and all 4 cases should be checked.%s' % (
      ansi_colors.FAIL, ansi_colors.ENDC))
    return
  '''


  # Now the params are passed in through rosservice call
  #this_node = ProbWriter (obj_center, mode, disc_fmt, disc_l_ang_q)

  this_node = ProbWriter (mode)


  wait_rate = rospy.Rate (10)

  print ('Listening for incoming service call...')
  while not rospy.is_shutdown ():

    # Maybe don't need this? Not sure if there are special conditions for
    #   shutdown. For now just let user Ctrl+C.
    '''
    # An indicator from publisher of chords, so can write file when all chords
    #   are received.
    if this_node.get_terminate ():

      # Shut down the service, so control returns to main() to write the file.
      #   Else file is never written!! Tried.
      # Ref: http://wiki.ros.org/rospy/Overview/Services
      this_node.chord_srv.shutdown ('Normal shutdown. Received finished srv call')

      this_node.write_file ()
      break
    '''

    try:
      wait_rate.sleep ()
    except rospy.exceptions.ROSInterruptException, err:
      break


if __name__ == '__main__':
  main ()

