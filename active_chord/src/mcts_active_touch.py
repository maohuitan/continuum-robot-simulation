#!/usr/bin/env python

# Mabel Zhang
# 10 Jan 2016
#
# Copied and modified from mcts_active_touch.py in active_touch used for
#   triangles.
#   This version tries to be more OOP, using ObservationList and PoseList
#   objects. Hopefully this makes it easier to be reused for different
#   projects that have different observations and actions. The MCTS
#   algorithmic part is always the same for any data. The caller just
#   implements ObservationList and PoseList objects and pass them in,
#   and the caller is responsible for interpreting them and providing
#   interface for this file to access necessary data e.g. names for pydot
#   visualization.
# Functions to port:
#   All accesses to MCTS_ActiveTouch.probsTrained.* functions
#   Removed MCTS_ActiveTouch.costsTrained, MCTS_ActiveTouch.nn.
#   costs_util.get_relative_pose (p1, q1, p2, q2)
#     -> action_util.calc_relative_action (n1, n2)
#   CostObservs.get_cost (action)
#     -> action_util.calc_action_cost (action)
#   multi_obs_per_action case is different in implementation. `.` now always
#     have multiple chords per action. Observation is a list now, not a single
#     chord. 
#
# Assumption:
#   You must load probabilities (pkl file loaded by prob_reader.py) from as
#     many classes as there are in the trained SVM model (joblib file).
#     `.` when searching the tree, probability p(.|y) conditioned on class y
#     is needed to continue onto a child node. This is in the objective fn.
#     So if a node's SVM prediction is class 6, then the probabilities for
#     class 6 must be loaded. Otherwise tree search cannot continue.
#
#
# X| 15 Feb 2017: No, it's fine. action_list is same for all nodes, no point to
#   use relative there. For the final action sequence, parent_poses is always
#   updated as we traverse down the tree, so these keep changing to be current
#   with current path of tree traversal. Result should be correct.
# Might need to use relative actions everywhere after all. Might not be
#   able to use nominal absolute pose in nodes at all. See prob_of_observs.py
#   comment in get_obs_tgt_list(). Need to fix the bug in prob_reader.py
#   get_obs_near_tgt_pose(), and then see if this is still needed. Then make
#   the changes in this file.
# > Don't need anymore. Remove all comments that say something like "TEMP for
#   abs poses before fixing to use relative actions in tree", when sure.
#
#
# TODO: Once everything works, change these var names so that they are more
#   uniform and clear!!!
#   k_obs_src -> obs_src
#   Add _i to variables that are the integer names of observations and poses.
#     Currently it's not clear which var is the integer name, which var is the
#     actual data!!!
#
# MCTS for active touch.
#

# ROS
import rospy
import tf
from visualization_msgs.msg import Marker, MarkerArray
from geometry_msgs.msg import Point

import numpy as np

# PyDot
import pydot
import time
from copy import deepcopy

# My packages
from util_continuum.ansi_colors import ansi_colors
from util_continuum.create_marker import create_marker

# Local
from mcts_mdp import MCTS_MDP
from action_util import calc_relative_action, calc_action_cost
from incremental_histogram import IncrementalHist
from classifier import get_nn_dist


# ===================================================== Abstract node class ==

# Parent class. Only contains generic containers for a MCTS tree, for any type
#   of observations and actions. Child class should implement functions for
#   specific ObservationList and PoseList for the specific application.
# Copied from MCTSNode_ActiveTouch class and refactored to an abstract class.
class MCTSNode:

  # Format for pydot node/edge name
  node_lbl_format = 'child implements'
  node_lbl_ext_format = 'child implements'
  edge_lbl_format = 'child implements'
  edge_lbl_ext_format = 'child implements'


  # Parameters:
  #   obs_inst: ObservationList object of the entire tree, used to access the
  #     actual observation data represented in this node via "obs" integer
  #     name. Actual observation data is used for adding to incremental hist,
  #     printing, etc. Returned from ProbReader.get_obs_inst_all ().
  #   obs: Unique index in an ObservationList object
  #   depth: Integer, tree depth. Root is at depth 0
  #   abs_pose: Unique index in a PoseList object
  #   action_list: List of indices in an PoseList object (absolute poses)
  def __init__ (self, obs_inst, obs, depth, abs_pose, action_list,
    incHistParams, svm):

    # A pointer to ObservationList containing all observations in the tree
    self.obs_inst = obs_inst

    self.obs = obs
    # 1D array of observation integer names, to enable multiple observations
    #   per action
    self.observs = np.array ([obs])
    self.depth = depth

    # Integer name of absolute pose from training data, used to compute
    #   relative movement actions wrt other absolute poses from training.
    # This gives the relative actions btw observations, `.` each node has 1
    #   observation, and each observation has an absolute pose associated with
    #   it.
    self.nominal_abs_pose = abs_pose

    # This is all possible actions this node can have as children.
    #   This is NOT all action lists that actually have an edge in the graph!
    #   Some items in this list may not be an actual edge in the graph yet!
    #   To get a list of actual edges from this node, use self.children.keys ().
    self.action_list = action_list

    # State stored at a node
    # Key: (obs0, obs1, obs2, depth). obs# is a float. key is int, tree depth
    #   that this node is at, root is at depth 0.
    # Must be unique in the tree, for dictionary access.
    # Depth is in the key, `.` we allow multiple nodes in the tree with same
    #   observation. This must happen if we want to limit each layer to one
    #   time step. Then when an observation matches, but depth doesn't match,
    #   then we must create a new node with same observation.
    #   If an observation already exists at the desired depth, then we want to
    #   point an edge toward that existing node, instead of creating a new one.
    #   This kind of edge preserves the one-timestep-per-tree-layer rule. So
    #   we allow it. It is also necessary, else we won't explore all actions
    #   available at a source observation, if we split the same observation into
    #   two different nodes in a layer; one node might only search 10 out of
    #   20 actions, the other searches 5, as opposed to both getting 15!
    #   Therefore a unique key is the observation AND the depth.
    self.key = MCTSNode_Chords.get_key_from_params (obs, depth)

    # This is an always changing state. It changes in each simulation! `.`
    #   a node can have multiple parents, so the histogram cannot simply be
    #   an accumulation of all its parents' histograms! `.` the opt sequence
    #   at end of tree search can only be a serial path from root to leaf,
    #   can't traverse multiple parents.
    #   .`. the hist is updated whenever the node is visited, in each
    #   simulation. The hist is recalculated by accumulating observations in
    #   the nodes on the path that is traversed down to reach this node.
    #   At end of tree search, similarly, the hist is re-calculated by taking
    #   all nodes on the opt (max-reward) path, and rebuilding a fresh hist.
    self.incHistParams = incHistParams
    self.incHist = IncrementalHist (*self.incHistParams)

    self.svm = svm

    # N, number of total visits
    # Could use 1D NumPy array, but chose dictionary in case the integer name
    #   of actions aren't always in increasing order, or skips some integers.
    # Key: integer name of action
    # Value: Number of visits from this node to action [i]
    self.nVisits = {a: 0 for a in self.action_list}

    # Q-value
    # Key: (action, child_key). Because each action can have multiple
    #   observations! Using both makes a unique dictionary key.
    #   action is a 7-tuple.
    # Value: discounted reward (Q-value) at action [i] at this node. Not just
    #   the simple reward.
    # Reward recorded at a node includes all rewards below this node,
    #   including the child edge of this node, excluding parent edge of this
    #   node.
    # Don't initialize to {a: 0}, `.` when select best path at the end, want
    #   to find max reward. If most are 0, and only reward with an actual child
    #   is negative, then will find a non-existing edge as max edge! So just
    #   add a:r as new children are added. (could init to {a: -infinity} though)
    self.rewards = {}

    # Key: action, on edge leading from this node to child node
    # Value: List of child node keys
    # Don't initialize like a: None, `.` the children don't exist yet, so don't
    #   add the action edge! Else will think there's a child and access None!
    self.children = {}


    # PyDot objects for visualization
    self.pydot_node = pydot.Node (self.make_node_name ())

    # Key: (action, child_key)
    # Value: pydot.Edge object
    # {a: pydot.Edge}
    self.child_pydot_edges = {}


  # Given the key, return the observation part.
  # NOTE: Update this function if key is changed!
  @staticmethod
  def get_obs_from_key (key):
    return key [0]

  #   obs: Integer name of an observation
  @staticmethod
  def get_key_from_params (obs, depth):
    return (obs, depth)

  def get_obs (self):
    return self.obs

  def get_key (self):
    return self.key

  def get_abs_pose (self):
    return self.nominal_abs_pose

  def get_incHist (self):
    return self.incHist


  def get_action_list (self):
    return self.action_list

  def get_rewards_keys (self):
    return self.rewards.keys ()

  def get_nVisits (self, action):
    return self.nVisits [action]

  def get_total_nVisits (self):
    return np.sum (self.nVisits.values ())


  # 7 Feb 2017 TEMP before fixing to use *relative* actions in tree
  # edge_action_data: Need the actual data, not just the index, `.` it is used to
  #   set pydot edge name for display for debugging!
  def add_child (self, edge_action, edge_action_data, child_node):

    if edge_action not in self.action_list:
      print ('%sWARN: add_child(): edge_action is not in parent node''s available action_list. Will add anyway but later behavior is undefined.%s' % (
        ansi_colors.WARNING, ansi_colors.ENDC))

    # Add the newly created node to the children list of parent, under the
    #   corresponding action.
    # If edge already exists, don't add it.
    # NOTE: 2nd condition not tested... what if the same edge from a parent can
    #   lead to different child nodes? That's not allowed in a proper tree, but
    #   I don't know if this situation will happen in the data. If it does, then
    #   this will cause some error, `.` a second repeated edge would not be 
    #   created! If you later need to access child_key from self.children,
    #   you'll never reach it!

    # Not doing this until we figure out what to do for multiple-observations-
    #  per-action. We can't calculate rewards for multiple observatiosn sharing
    #  same action. `.` when choose optimal, which action-observ pair to choose,
    #  if multiple observs have same reward? Not clear which path to go down.
    # Actually, I have an idea. Use 1 reward for the multiple observations.
    #   In tree policy, just select a random observation to get next action
    #   from. This way, we have multiple observations per action, to stand for
    #   the motivation of active touch - having a lot of observations per
    #   action, therefore few action required to observe all triangles. But
    #   still be able to calculate reward and tree policy. When select a path
    #   at the end, just select the path with highest reward. When create hist,
    #   add ALL observations at an action to the hist, not just randomly pick
    #   1. This will let us fill up histogram quickly, which is the orig goal.
    if edge_action not in self.children.keys ():
      # Allow multiple child node for the same action
      self.children [edge_action] = []

    # Allow multiple child node for the same action
    self.children [edge_action].append (child_node.get_key ())


    if (edge_action, child_node.get_key ()) not in \
        self.child_pydot_edges.keys ():

      # Create a PyDot Edge
      #edge_lbl = MCTSNode_Chords.make_edge_lbl (edge_action)
      # 11 Jan 2017: Can I call staticmethod with self.*?
      edge_lbl = self.make_edge_lbl (edge_action_data)
      pydot_edge = pydot.Edge (self.pydot_node, child_node.pydot_node,
        label=edge_lbl)
      # Allow multiple child node for the same action
      self.child_pydot_edges [(edge_action, child_node.get_key ())] = pydot_edge

      # Create an entry in parent node's rewards array
      self.rewards [(edge_action, child_node.get_key ())] = 0

    else:
      # Allow multiple child node for the same action
      pydot_edge = self.child_pydot_edges [(edge_action, child_node.get_key ())]

    # Return the pydot edge
    return pydot_edge


  # Parameters:
  #   reward_key: (action, child_key). Action is chosen from this node's
  #     available list of actions, to traverse downwards to child_key, a child
  #     of this node.
  #   reward: Computed by caller, reward for choosing the specified action at
  #     this node.
  def update_visit (self, reward_key, reward):

    action = reward_key [0]

    self.nVisits [action] += 1

    # Sanity check. rewards[action] should be created when a new child is added
    #   to this node, in create_node(). But since that function is in a diff
    #   class, we can't depend on another class. So do this sanity check anyway
    if reward_key not in self.rewards.keys ():
      self.rewards [reward_key] = 0

    # Discounted reward, i.e. Q-value
    # Q += ((r - Q) / N)
    #   r - Q penalizes reward, if the new reward r is less than the existing
    #     reward Q at the specified action from this node. `.` then r - Q is
    #     negative! So it becomes a cost! Not sure why this decision, `.` it
    #     would FAVOR LATER CHOICES of this action. It requires later choices
    #     r to be greater than Q, in order for the reward to remain positive.
    #     Maybe this is to enforce consistency? So if the previous time when Q
    #     was recorded was chosen by rollout policy, and r is when action is
    #     chosen by tree policy, then you DO want to favor r, `.` tree policy
    #     is more trustworthy than the rollout policy baseline.
    #   N in denominator EVENLY SPREADS r-Q across all the visits to this
    #     action at this node, including the latest visit, which is included
    #     in N in the line above!
    # See discounting (p.11) http://www.cs.rice.edu/~vardi/dag01/givan1.pdf
    #   It is to prefer shorter solutions!
    # reward - self.rewards[action] is the difference btw the new reward and
    #   the existing reward for choosing the specified action from this node.
    # self.nVisits[action] is number of times the specified action has been
    #   chosen from this node.
    #   Dividing the difference btw new and old reward, by the number of times
    #   the action has been chosen (which has been updated in line above to
    #   include THIS new time that the action is chosen), means to get the
    #   average amount of reward across all the visits to the action from this
    #   node.
    self.rewards [reward_key] += ((reward - self.rewards [reward_key]) /
      self.nVisits [action])


  # Parameters:
  #   init_hist: Fresh histogram to re-initialize incHist at
  #   obs_set: List of integer names of observations in
  #     self.obs_inst.single_obs
  def update_incHist (self, init_hist=None, obs_set=None):

    self.incHist = IncrementalHist (*self.incHistParams,
      init_hist=init_hist)

    if obs_set is None:
      # 2D numpy array. Convert the self.obs int to an array, to get 2D returned
      obs_data = self.obs_inst.get_single_obs_i ([self.obs])
      # Add this node's observation to hist. Convert to 2D to pass to fn
      success, _ = self.incHist.add_bin_count (obs_data)

    # If a set of observations is provided, add these to hist instead
    else:
      # nObs x nDims numpy array
      obs_data = self.obs_inst.get_single_obs_i (obs_set)
      # Reshape into 2D, which add_bin_count() assumes
      if obs_data.ndim == 1:
        obs_data = obs_data.reshape (1, obs_data.size)
      success, _ = self.incHist.add_bin_count (obs_data)

    if not success:
      print ('%sNo bin fit some of the data. Check your hist_conf.csv bin ranges, then rerun. Data was:%s' % (ansi_colors.FAIL, ansi_colors.ENDC))
      if obs_set is None:
        print (self.obs)
      else:
        print (obs_set)



# =========================================== node class specific to chords ==

# Copied from MCTSNode_ActiveTouch class and refactored to a child class.
class MCTSNode_Chords (MCTSNode):

  # Format for pydot node/edge name
  # Integer name of observation, tree depth
  node_lbl_format = '%d d%d'
  # Integer name of observation, predicted class, time step (i.e. tree depth),
  #   number of items in unnormalized histogram
  node_lbl_ext_format = 'obs%d %s t%d h%d'
  # 4-tuple action (qx qy qz qw)
  #edge_lbl_format = '%.1f %.1f %.1f %.1f'
  # 4-tuple action (qx qy qz qw), reward
  #edge_lbl_ext_format = '%.1f %.1f %.1f %.1f\nr%.1f'

  # 7 Feb 2017: TEMP before changing to use *relative* actions in tree
  # 3-tuple absolute pose (normal vector to cross section plane
  edge_lbl_format = '%.1f %.1f %.1f'
  #edge_lbl_ext_format = '%.1f %.1f %.1f\nr%.1f'
  # Use just the integer name of absolute pose
  edge_lbl_ext_format = 'p%d r%.1f'


  def make_node_name (self):
    return self.node_lbl_format % (self.key[0], self.key[1])

  @staticmethod
  def make_node_name_from_key (key):
    if key is None:
      return ''
    else:
      return MCTSNode_Chords.node_lbl_format % (key[0], key[1])

  def make_node_lbl_ext (self, svm): #nn):
    pred_class_idx = svm.svm_predict (self.incHist.get_as_feat ())
    pred_class = svm.classifier_lbls [pred_class_idx]
    pred_str = pred_class

    #pred_class = nn.nn_predict (self.incHist.get_as_feat ())
    #pred_dist = nn.get_nn_dist (self.incHist.get_as_feat (), pred_class,
    #  dtype='inner')
    #pred_str = 'd%.2f' % pred_dist

    # Show prediction probability, or NN distance, `.` the
    #   class predictions in the tree alone, without actual observations, are
    #   meaningless! Prediction probs are much more insightful, as it's part
    #   of the objective function too!
    #pred_prob, _ = svm.svm_predict_prob (self.incHist.get_as_feat ())
    #pred_str = 'p%.2f' % np.max (pred_prob)

    return self.node_lbl_ext_format % (self.key[0], pred_str, self.key[1],
      # Add a number, for total number of bin counts in unnormalized hist.
      #   For one observation per node, this should equal to the node depth!
      np.sum(self.incHist.get_hist ()))


  # Tuple of (3-tuple, 7-tuple, 3-tuple), used for self.edge_vis_dict.keys ()
  # Use edge_action in addition to parent and child node keys, to allow
  #   multiple edges between two nodes! These edges can have different actions
  #   on them. If only use the nodes as unique key of dictionary, then there
  #   can be only one edge connecting each pair of nodes.
  @staticmethod
  def make_edge_vis_dict_key (parent_key, edge_action, child_key):
    # Return (string, 7-tuple, string)
    return (MCTSNode_Chords.make_node_name_from_key (parent_key),
      edge_action,
      MCTSNode_Chords.make_node_name_from_key (child_key))

  # Extended label of edge, used at the end of tree search to see the final
  #   info at each edge.
  # Extended info: Reward at each edge from the given node.
  def make_edge_lbl_ext (self, action_i, action, child_key):
    if action is None:
      return ''
    else:
      return self.edge_lbl_ext_format % (
        # 7 Feb 2017 TEMP before fixing to use *relative* actions in tree
        #action[0], action[1], action[2],
        #action[0], action[1], action[2], action[3],
        action_i,
        self.rewards [(action_i, child_key)])

  @staticmethod
  def make_edge_lbl (action):
    if action is None:
      return ''
    else:
      return MCTSNode_Chords.edge_lbl_format % ( #class_name, 
        # 7 Feb 2017 TEMP before fixing to use *relative* actions in tree
        action[0], action[1], action[2])
        #action[0], action[1], action[2], action[3])



# ======================================================= Parent tree class ==

# Inherits MCTS_MDP class
# Parent class for active recognition, independent of data containers.
#   This class just runs tree search without assuming observation and action
#   data types. Aim is for this class to be reusable for another project if
#   we change the data representations.
# These are the functions for active recognition tree search, that should be
#   shared among all child classes:
#   mcts_mdp ()
# Child class needs to implement the data-container dependent functions,
#   including all that use data-specific functions in self.probsTrained
#   type ProbReader):
#   create_node ()
#   create_edge ()
class MCTS_ActiveTouch (MCTS_MDP):

  def create_node (self):
    print ('%sMCTS_ActiveTouch create_node(): This is an abstract class! Child class must implement!%s' % (
      ansi_colors.FAIL, ansi_colors.ENDC))

  def create_edge (self):
    print ('%sMCTS_ActiveTouch create_edge(): This is an abstract class! Child class must implement!%s' % (
      ansi_colors.FAIL, ansi_colors.ENDC))

  # probsTrained: ProbReader. Abstract parent. All functions in this class
  #   need to use functions defined in the abstract parent ProbReader. Child
  #   classes can use functions defined in inheritances of ProbReader specific
  #   to applications e.g. ProbReader_Chords.
  def __init__ (self, params, probsTrained, incHistParams, svm):
    #nn):

    # Parent class ctor
    MCTS_MDP.__init__ (self, params)

    self.root_key = None


    # p(z2 | y, z1, a)
    self.probsTrained = probsTrained

    #self.costsTrained = costsTrained

    self.incHistParams = incHistParams
    self.svm = svm
    #self.nn = nn

    self.depth_reached = 0

    # Debug flag, whether you want to see step-by-step visualizations of what
    #   edge is chosen by tree policy and rollout policy.
    self.VIS_POLS_PER_SIM = False

    self.DISPLAY_TREE_PER_ADD = False
    self.DISPLAY_TREE_PER_SIM = False

    #self.multi_obs_per_action = True


    # User adjust parameters

    # Rollout policy uses this for: 
    #   (1 - lambda) mvmt_cost + lambda * mispred_cost
    # TODO: This will need to be tuned. Will need to write code that passes
    #   this value in from outside, run many times, to see which value is best.
    # Must be in [0, 1] to enforce reward in range [0,1], which UCB1 policy
    #   assumes.
    self.mispred_lambda_wt = 0.5


    # Class prediction at leaf, at end of each simulation
    # These should hopefully converge, as more simulations are run, and
    #   tree policies reach deeper in the tree.
    self.leaf_classes = []

    self.vis_arr_pub = rospy.Publisher ('/visualization_marker_array',
      MarkerArray, queue_size=5)


    # Time profiler. If True, prints a lot of elapsed times
    self.PROFILE = False


  # Called by create_edge() and sel_path().
  # Should be called whenever traverse down the tree. `.` parent's chosen
  #   action arms will change child node's absolute poses (and therefore its 
  #   relative action_list), and set of observations (therefore its
  #   incremental histograms)
  # Parameters:
  #   child_obsl_i: Integer name of observation list, which contains multiple
  #     chords. Indexes self.probsTrained.obs_inst_all.obsl_list.
  #     Main difference btw this version of MCTS for chords and the version
  #     for triangles. Here, each node inherently represents multiple chord
  #     observations, via ObservationList, not just a single chord! This is the
  #     cross product that I did not have time to train or change tree design
  #     for in triangles.
  def update_node_in_traverse (self, parent_key, child_key, child_obsl_i):

    # Don't need. update_incHist just takes the index and looks at the
    #   ObservationList that it has. Remove when works.
    # List of single-observation idxes, e.g. list of chord idxes.
    #   Indexes each node's obs_inst.single_obs.
    # NOTE: Make sure this ObservationList is same as the one you pass to
    #   MCTSNode() ctor!! update_incHist() will be accessing MCTSNode.obs_inst_all,
    #   so these indices must be the same as ones in there!
    #child_obsl = self.probsTrained.get_obs_all_i (child_obsl_i)
    
    # Update incremental histogram of child.
    #   It should be accumulated from observations in the nodes on the path
    #   we had JUST traversed down to reach this node, in this simulation.
    # Copy the latest histogram from parent
    self.tree_nodes [child_key].update_incHist (
      self.tree_nodes [parent_key].incHist.get_hist (), child_obsl_i)

    # Update nominal absolute pose of child.
    #   It is affected by the child edge, whichever we traverse down each time.
    parent_pose = self.tree_nodes [parent_key].get_abs_pose ()
    # Now store absolute pose in actions, don't need to calc from relative pose
    child_pose = self.tree_nodes [child_key].get_abs_pose ()


  # =========================================================== Tree search ==

  # TODO: The goal is that this can be in abstract parent MCTS_ActiveTouch, to
  #   be a generic class!!! Only the containers MCTSNode_*, ObservationList,
  #   and PoseList are different - each child has their own implementation.
  #   This function should only access the functions that are required to
  #   be implemented in the child, as specified in parent class's documentation!
  #   Then this fn can be reused by all future projects!!! They only need to
  #   implement children of the container classes, and don't need to rewrite
  #   any policy functions!
  # Entry point. Runs the tree search.
  #   Outputs result as incremental histogram self.incHist
  # Parameters:
  #   root_obs_i: A single observation from the very first move of the robot.
  #     This initializes the root node of the tree.
  #   root_pose_i: Scalar integer name of an abs pose. The absolute position
  #     associated with root_obs_i in training. Indexes
  #     self.probsTrained.pose_inst_all.abs_poses.
  #     TODO: Treatment of this in caller has to be: not randomly chosen, that
  #     might be why a lot of the poses are unfeasible! It needs to be the
  #     last pose that the robot hand moved to!!! Then discretize it and find
  #     the closest pose in probsTrained. Pass in the index of that!!
  #   init_hist: numpy 3D array, raw unnormalized histogram
  #   prev_poses: 1D numpy array. n integer names of absolute poses from
  #     previous tree searches.
  # Returns:
  #   pa: Normalized reward vector for root node. Vector size is number of
  #     actions available at root node.
  #   R: Vector of rewards. Size is number of simulations
  def mcts_mdp (self, root_obs_i, root_pose_i, init_hist=None,
    prev_poses=np.zeros((0,), dtype=int)):

    # Blue (0) to cyan (1), colored by probability of observation
    # TODO This doesn't exist yet!
    #self.visualize_all_abs_poses ()
    print ('')

    # Init root node
    # First create root, so that first call to simulate() doesn't find a
    #   non-existent node, and call rollout from 1st layer on! It should find
    #   root already exist, and call tree policy at 1st layer, then call
    #   rollout from 2nd layer on.
    # Root is at depth 0.
    root_node = self.create_node (root_obs_i, 0, root_pose_i)
    # (observation, depth)
    self.root_key = root_node.get_key ()

    # Initialize histogram with any hist passed into this fn.
    #   Add the initial observation to the root histogram.
    root_node.update_incHist (init_hist)

    print ('%saction_list has %d items%s' % (ansi_colors.OKCYAN,
      self.probsTrained.get_n_all_poses (), ansi_colors.ENDC))

    # Add previous tree searches' abs poses, to check for duplicates
    # Don't replace orig var, `.` not a deep copy, it will add a duplicate
    #   pose. `.` all poses from this tree search will be added by caller
    #   after we return.
    parent_poses = np.append (prev_poses, [root_pose_i], axis=0)

    # Vector of rewards per simulation, to plot line graph of progression of
    #   rewards
    rewards = np.zeros ((self.params.nSims,))

    runtime = np.zeros ((self.params.nSims,))

    # Run simulations
    for si in range (0, self.params.nSims):

      print ('Simulation %d' % si)

      # Used for saving PyDot image to file with sim number
      # Base 1. `.` ln(n) is computed in UCB1, and ln(0) is illegal. So start
      #   at 1.
      self.sim_i = si + 1

      # Make per-simulation pydot graph visualization, for step-by-step images
      if self.VIS_POLS_PER_SIM:
        # Re-init. Values are lists of tuples ('parent_name', 'child_name')
        #   Used for pydot only, so elts in lists in values must be strings!
        self.per_sim_pols_dict = {'tree': [], 'rollout': []}

      # This is a recursive function, it will call itself to traverse from root
      #   of tree to either a leaf (depth == target depth); or middle of the
      #   tree where tree depth < target, but there are no more child nodes,
      #   in which case a new node will be created and simulation ends.
      # Time the top-level recursive calls
      start_time = time.time ()
      rewards [si] = self.simulate (root_obs_i, 0, parent_poses, 'null', None)
        #np.array ([root_obs_i]))
      runtime [si] = time.time () - start_time
      print ('simulate() elapsed time: %f s' % (runtime [si]))

      if self.VIS_POLS_PER_SIM:
        self.vis_policies_per_sim (si)

      if self.DISPLAY_TREE_PER_SIM:
        self.display_tree (self.tree_vis)

      if rospy.is_shutdown ():
        print ('Received Ctrl+C in mcts_mdp(). Terminating')
        return None

      print ('')

    print ('%sAverage running time per simulation: %f s%s' % (
      ansi_colors.OKCYAN, np.mean (runtime), ansi_colors.ENDC))

    # Normalize rewards vector of root node, by dividng each reward by the sum
    #   of rewards. Resulting normalized rewards will sum to 1.
    #   That gives a distribution of rewards across all possible paths from
    #   root, `.` each node's rewards are ("discounted"?) sums of children
    #   paths.
    root_node = self.tree_nodes [self.root_key]
    # "Sum of discounted rewards"?
    root_rewards_sum = np.sum (root_node.rewards.values ())
    root_rewards_norm = {rk : root_node.rewards [rk] / root_rewards_sum \
      for rk in root_node.rewards.keys ()}

    # Select a path from root to leaf, to be the chosen sequence of actions
    # opt_edges: n x 7 numpy array, relative actions
    # opt_abs_poses: n x 7 numpy array of abs poses in TRAINING! Caller should
    #   NOT use these poses, other than for debugging!
    #   Caller should compute actual poses in current live world frame, using
    #   root_pose and opt_edges.
    # opt_abs_poses_num: 1D numpy array of integer names of abs poses
    opt_edges, opt_incHist, opt_abs_poses, opt_abs_poses_num, last_obs_i = \
      self.sel_path (self.root_key, prev_poses)

    # Update the tree labels with rewards, predicted class from latest incHists
    #   in nodes updated by sel_path.
    self.update_pydot_tree_lbls ()

    #self.display_tree (self.tree_vis)


    # Visualize nominal absolute poses

    # Start markers at previous tree searches' last number + 1. IDs are 0-based
    #   so previous tree searches' last index was len-1.
    marker_id_start = prev_poses.shape [0]

    # Cyan
    #print ('Optimal abs poses:')
    #self.visualize_abs_poses (opt_abs_poses, marker_id_start=marker_id_start)

    # Red. Let active_predict show abs poses calculated from relative edges,
    #   with cyan numbers, easier to see in plots.
    root_pose = self.probsTrained.get_pose_all_i (root_pose_i)
    # TODO: Write visualize_* fns later
    #self.visualize_relative_poses (root_pose, opt_edges, opt_abs_poses,
    #  marker_id_start, show_sqrs=True, show_arrows=True, show_numbers=False)


    print (' sim   leaf predicted class')
    for l_i in range (0, len (self.leaf_classes)):
      print ('%4d   %s' % (l_i, self.svm.classifier_lbls [ \
        self.leaf_classes [l_i]]))

    # opt_edges: n x 7 numpy array of relative actions. Caller should use these
    #   to compute goal poses for robot wrist, starting from root_pose.
    # opt_abs_poses_num: Includes root_pose_i in first elt.
    # ttl runtime: float
    return (root_rewards_norm, rewards, opt_incHist, opt_edges,
      opt_abs_poses_num, last_obs_i, np.sum (runtime))


  # Parameters:
  #   node_obs: Integer name of observation at node we are currently trying to
  #     create
  #   depth: Depth of node_key
  #   parent_poses: n x 1 numpy array. n absolute poses from root to node_key,
  #     inclusive. The last element is the abs pose for the node_key to be
  #     created. Values are integer indices to access a pose through
  #     self.probsTrained.get_abs_pose_by_num().
  #   parent_key: Key of parent of potential new node_key node.
  #     Should only be used for pydot visualization
  #   k_action: Action on the potential new parent-edge of node_key node,
  #     connecting from parent_key to node_key.
  #     Removing, you shouldn't know it yet! It's chosen by tree or rollout pol.
  #   node_obs_set: Set of observations to init key_node's histogram
  #   end_signal: True indicates p_t+1 (therefore z_t+1) was not found, so
  #     there is no node_obs to create.
  def simulate (self, node_obs, depth, parent_poses, parent_key, k_action):
    #node_obs_set):

    node_key = MCTSNode.get_key_from_params (node_obs, depth)

    #print ('Simulating: visiting %s' % str (node_key))

    # This potentially creates one node, the node_key, if it doesn't exist in
    #   the tree yet.
    # If node_key already exists, then no node will be created, and the edge
    #   taken from parent_key to node_key will be chosen by tree policy below.
    # No edge is created from parent_key to node_key!!! The edge is chosen by
    #   tree policy or rollout policy.
    ending = self.end_of_simulate (node_key, depth, parent_poses, parent_key,
      k_action) #, node_obs_set)

    # Base case of recursive call
    # Runs end_of_simulate() first, regardless end or not. Get ret val.
    if ending:
      #print ('Ending')

      pol_lbl = 'rollout'

      # Rollout policy is needed to compute the misprediction cost, which can
      #   only be computed at leaf of tree, at depth tau
      # Returns reward from edge a_t+1 to tau, where t is current node depth,
      #   a_t+1 is the child edge of current node.
      reward, leaf_class = self.rollout_policy (node_key, depth, parent_poses)

      # Store last class (class at greatest depth) as the result from this
      #   simulation
      self.leaf_classes.append (leaf_class)

    # Tree policy. No new node is created, node_key already exists in next
    #   layer. Tree policy just finds an opt action edge to go from parent_key
    #   to node_key.
    else:

      pol_lbl = 'tree'

      #####
      # Choose the edge to go from parent_key to node_key, both existing nodes
      #####

      node = self.tree_nodes [node_key]

      # Choose an action from node, using tree policy. This is to make recursive
      #   call.
      # next_node_obs_i: Scalar index to one row in
      #   self.probsTrained.obs_inst_all (an ObservationList).obsl_list.
      #   The name points to a list of chords.
      #   Node keys must use the index in obs_inst_all, because obs_insts [c_i]
      #   has a class c_i as index, and a node does not know its true class yet!
      #   So cannot use the 2D index within the nodes!
      # next_node_pose_num: Scalar index to one row in
      #   self.probsTrained.pose_inst_all (a PoseList).abs_poses.
      # next_node_obs_set: nObservs x 1, integer names of observations. 
      #   Observations used to initialize new child node's histogram.
      #   Don't need for chords, `.` every node has a observation list - many
      #   chords, not just one. So don't need this extra set to init anymore.
      #   Remove this var when everything works.
      next_class, next_action, next_node_obs_i, next_node_pose_num = \
        self.tree_policy (node_key, parent_poses)
        #next_node_obs_set =
      #print ('next_class:')
      #print (next_class)

      # An indication that z_t+1 was not found, because all candidate absolute
      #   poses p_t+1 have been visited in parent path, or all p_t+1 were too
      #   far from p_t * a_t+1.
      # Call rollout to randomly generate the next nodes.
      if next_class is None:
        #print ('No more next_class found as child of tree. Going to rollout.')
        # NOTE: Make sure this code is same as the rollout policy block above!!
        pol_lbl = 'rollout'
        reward, leaf_class = self.rollout_policy (node_key, depth, parent_poses)
        self.leaf_classes.append (leaf_class)
        return reward

      # A single observation (list of chords)
      #next_node_obs = tuple (self.probsTrained.get_obs_all_i (
      #  next_node_obs_i))

      next_node_key = MCTSNode_Chords.get_key_from_params (next_node_obs_i,
        depth+1)
      # Add the newly chosen action to path. Path is from root to next_node_key
      #root_to_node.append (next_action)

      #print ('next_node_key:')
      #print (next_node_key)

      if rospy.is_shutdown ():
        print ('Received Ctrl+C in simulate(). Terminating')
        return 0


      #####
      # Advance to next iter
      #####

      #print ('Appending %d to parent_poses' % next_node_pose_num)
      next_parent_poses = np.append (parent_poses, [next_node_pose_num], axis=0)
      #print ('next_parent_poses in tree_policy():')
      #print (next_parent_poses)

      # Recursive call
      # Simulate downwards from next node, record reward for next node and its
      #   child path.
      # Returns reward from edge a_t+2 to tau, where t+1 is next node depth,
      #   a_t+2 is the child edge of next node.
      subpath_reward = self.simulate (
        next_node_obs_i, depth + 1,
        next_parent_poses,
        node_key, next_action) #, next_node_obs_set)

      # No new node is added, `.` the node already exist, that's why we're in
      #   tree policy, not rollout. We just want to find an edge to go to that
      #   existing node in the immediate next layer.
      self.create_edge (node_key, next_action, next_node_key, next_node_obs_i)
        # next_node_obs_set)

      # Objective cost function
      #   C = (1-lambda) * (1/tau) * mvmt_cost + lambda * mispred_cost
      #   Pass in relative action, not absolute!!
      # Add the reward for current node, onto the reward from next node and
      #   down, to get cumulative reward from this node down.
      #   Reward recorded at a node includes all rewards below this node,
      #   including the child edge of this node, excluding parent edge of this
      #   node.
      # Reward from edge a_t+1 to tau = (reward of edge a_t+1) +
      #   (reward from edge a_t+2 to tau) 
      # At depth d, the movement cost of a_d+1 should be added. That is the
      #   child edge of node at depth d.
      # Enforce reward in range [0, 1], which UCB1 assumes. Individual mvmt
      #   costs are in range [0, 1]. Reward is 1 - cost.
      # Multiply by 1 / tau, tau is horizon, in effect taking the average mvmt
      #   cost when all costs are summed together.
      curr_pose = self.probsTrained.get_pose_all_i (
        parent_poses [parent_poses.shape [0] - 1])
      next_pose = self.probsTrained.get_pose_all_i (next_node_pose_num)
      rel_action = calc_relative_action (curr_pose, next_pose)
      mvmt_cost = calc_action_cost (rel_action) / self.params.horizon
      # NOTE: Make sure this weighing and the weighing in rollout_policy() are
      #   the same! Else you are computing costs inconsistently. Similarly for
      #   the reward computation.
      wt_mvmt_cost = (1.0 - self.mispred_lambda_wt) * mvmt_cost
      reward = (1.0 - wt_mvmt_cost) + subpath_reward

      # Update the reward at the chosen action from this state
      node.update_visit ((next_action, next_node_key), reward)

      # Make per-simulation pydot graph image for debugging
      if self.VIS_POLS_PER_SIM:
        self.per_sim_pols_dict [pol_lbl].append ((node.make_node_name (),
          MCTSNode_Chords.make_node_name_from_key (next_node_key)))

    return reward


  # Parameters:
  #   node_key: key of node we are trying to create
  #   depth: Depth of node
  #   parent_key: only used for pydot visualization
  #   k_action_i: Integer name of absolute pose. Edge from parent_key to
  #     node_key, to be created
  def end_of_simulate (self, node_key, depth, parent_poses, parent_key,
    k_action): #, node_obs_set):

    #print ('End of simulate: visiting %s' % str (node_key))

    # Integer name of an observation in self.probsTrained.obs_inst_all.
    node_obs = MCTSNode.get_obs_from_key (node_key)

    if node_key not in self.tree_nodes.keys ():

      node_pose_i = parent_poses [parent_poses.shape [0] - 1]
      node_pose = self.probsTrained.get_pose_all_i (node_pose_i)
      # For relative pose, pass in the actual pose
      #self.create_node (node_obs, depth, node_pose)
      # For absolute pose, just pass in the integer name
      self.create_node (node_obs, depth, node_pose_i)
      self.create_edge (parent_key, k_action, node_key, node_obs)

      # Add the new node to the Pydot graph visualization, display latest
      #   graph.
      if parent_key == 'null':
        parent_name = parent_key
      else:
        parent_name = MCTSNode_Chords.make_node_name_from_key (parent_key)

      # Helpful for debugging if creating the correct node under correct parent
      #print ('Creating %s, parent %s, edge %s' % (node_key, parent_key,
      #  MCTSNode_Chords.make_edge_lbl (k_action)))

      print ('End of simulation at depth %d' % depth)
      return True

    # If reached desired depth, but didn't reach last simulation yet, then run
    #   rollout policy to fill in the remaining simulations
    elif depth == self.params.horizon:
      print ('End of simulation at depth %d' % depth)
      return True

    # Otherwise, run tree policy to get real actions
    else:
      return False


  # =========================================================== Tree policy ==

  # Find optimal action a* underneath node_key.
  #   Idea of tree policy is that, it doesn't necessarily chooses the action
  #   with max reward. It tries to balance btw reward and exploration, choosing
  #   the arm not as explored, but weighs in costs too.
  # This function only deals one node, no recursions.
  # Parameters:
  #   node_key: Node down to which has been decided and traversed. This fn
  #     finds an optimal action edge under node_key, to go to child
  #     next_node_key.
  #   parent_poses: n-elt numpy array. Integer name of n absolute poses from
  #     root node to node_key, inclusive.
  def tree_policy (self, node_key, parent_poses):

    #print ('Tree policy')

    node = self.tree_nodes [node_key]

    # z_t
    obs_src = node.get_obs ()

    # x_t
    orig_incHist = node.get_incHist ()

    # p_svm (y | x_t), where t is time step (depth) of node_key
    #   This does not include the newly chosen action, at depth+1.
    # Returns int
    orig_pred_probs, classes = self.svm.svm_predict_prob (
      orig_incHist.get_as_feat ())

    # y_t. Class at node_key
    orig_pred_class_key = self.svm.svm_predict (orig_incHist.get_as_feat ())
    k_class = self.svm.classifier_lbls [orig_pred_class_key]


    #####
    # Find an action a_t that trades off exploration and exploitation (UCB1
    #   helps with that)
    #####

    #if self.PROFILE: start_time = time.time ()

    # a*_t+1
    # Calculate the optimal a*_t+1 that will produce the desired z_t+1
    opt_action = self.calc_next_action (node, orig_incHist)

    #if self.PROFILE: print ('calc_next_action() elapsed time: %f' % (time.time () - start_time))

    if self.PROFILE: start_time = time.time ()

    # \hat{z}_t+1. Desired next observation, given based on a*_t+1
    # desired_child_obs: One observation (list of chords), (c_i, row_i)
    # child_abs_pose: scalar
    # child_obs_set: nObservs x 1, integer names of observations.
    #   Don't need for chords. remove when everything works.
    desired_child_obs_i, child_abs_pose_i = \
      self.infer_next_obs (
        obs_src, opt_action, parent_poses, orig_pred_probs, classes)
      #, child_obs_set_i = \
    if self.PROFILE: print ('infer_next_obs() elapsed time: %f' % (time.time () - start_time))
    if desired_child_obs_i is None:
      return None, None, None, None #, None

    # k_class: string
    return k_class, opt_action, desired_child_obs_i, child_abs_pose_i
      #child_obs_set_i


  # Finds opt action a_t+1
  #   using UCB1 exploration-exploitation.
  # Parameters:
  #   orig_incHist: x_t
  def calc_next_action (self, node, orig_incHist):

    # List of all possible (action, child_key) to choose from this node
    rewards_key_l = node.get_rewards_keys ()

    # Init list of rewards. Optimal action a should maximize this
    bounds = np.zeros ((len (rewards_key_l), ))

    found_unvisited_a = False

    # C_p, exploration weight
    explore = 1.0

    # Choose an action with 0 visits (N_a == 0) first.
    #   `.` if it has not been visited, a reward Q doesn't exist for it, and
    #   we can't do argmax across all a's rewards (argmax_a Q_a)!
    #   First reason, this is the right way of code in papers.
    #   Second reason, it's also necessary in our implementation,
    #   where a (action, child_obs) key cannot be added to node.rewards{} until
    #   the edge has already been created! `.` otherwise child_obs is not
    #   known! So before an action has ever been visited,
    #   rewards[(action, child_obs)] will get KeyError.
    #   Third reason, then when N_a == 0, we don't get division by 0 in UCB1.
    for k_action in node.get_action_list ():
    
      nVisits_a = node.get_nVisits (k_action)
    
      if nVisits_a == 0:
        opt_action = k_action
        found_unvisited_a = True
        break


    # Until an action has been visited, rewards{} will be empty!
    # Until all actions have been added to rewards{}, marginalizing rewards
    #   over all entries in it is not fair, `.` there are unvisited actions
    #   that are not considered, and therefore will never be picked!
    if not found_unvisited_a:

      # Loop through all possible actions, calculate UCB1 bound
      for r_i in range (0, len (rewards_key_l)):
    
        # a
        # Current action
        rewards_key = rewards_key_l [r_i]
        k_action = rewards_key [0]
    
        # -C_a. Existing reward node.Q[a] calculated in previous simulations
        # Use this because 1. cannot calculate total cost on tau until leaf of
        #   tree at depth tau, and 2. this is what makes the Monte Carlo method
        #   fast.
        reward_a = node.rewards [rewards_key]
     
     
        #####
        # Calculate UCB1 bound, for this action
        #####
     
        # Number of times arm a was visited from the node. MCTSNode.N[a]
        nVisits_a = node.get_nVisits (k_action)
    
        # Total number of times that this node has been visited. Sum takes the
        #   total across all actions. node.N[i] is the number of times
        #   action [i] has been visited.
        nVisits = node.get_total_nVisits ()
    
        # Browne Algorithm 2, or UCT eqn in 3.3.1
        #   Q_a / N_a + C_p * sqrt (2 * ln (N) / N_a)
        #   Q_a / N_a is the average reward for arm a, \bar{X_a} in Browne.
        ucb1 = reward_a / nVisits_a + \
          explore * np.sqrt (2.0 * np.log (nVisits) / nVisits_a)
        bounds [r_i] = ucb1
     
        if rospy.is_shutdown ():
          print ('Received Ctrl+C signal in calc_next_action(), terminating')
          break
     
      # end for rewards_key_l
     
      # Action that maximized reward is the optimal action
      #   This is the a* = equation, using UCT. It has to be max, not soft
      #   sample... I think, `.` we wrote a* =, not a* ~ (~ meaning "sampled
      #   from"). Soft sample should be done at the very end when tree search
      #   is done, when we actually pick actions to return, in sel_path().
      opt_r_i = np.argmax (bounds)
      opt_action = rewards_key_l [opt_r_i] [0]

    # end if not found_unvisited_a

    return opt_action


  # Calculates z_t+1 ~ p(z_t+1 | z_t, a_t+1)
  # Parameters:
  #   k_obs_src: z_t
  #   opt_action: a*_t+1
  #   orig_pred_probs: p_svm (y | x_t). 1 x nClasses numpy array
  #   classes: integer class IDs, in sklearn SVC svm.classes_, used to index
  #     classifier_lbls{}. classes_ is in the same order as classes in
  #     orig_pred_probs. classifier_lbls.keys() is classes_. This way, classes
  #     ensures that when we index orig_pred_probs and classifier_lbls, they
  #     are corresponded so that we get the right probability for a class
  #     string name!
  # Most intricate access to probsTrained is in this fn.
  def infer_next_obs (self, k_obs_src, opt_action, parent_poses,
    orig_pred_probs, classes):

    if self.PROFILE: start_time = time.time ()

    # TODO: TEMP when using absolute poses. Change after use relative poses in tree
    src_pose_i = parent_poses [len (parent_poses) - 1]
    tgt_pose_i = opt_action
    src_pose = self.probsTrained.get_pose_inst_all ().get_pose_i (src_pose_i)
    tgt_pose = self.probsTrained.get_pose_inst_all ().get_pose_i (tgt_pose_i)
    action = calc_relative_action (src_pose, tgt_pose)

    # Get a list of possible z_t+1
    #   This call looks for parameters to compute p(z_t+1 | z_t, a_t+1, y). It
    #     returns all satisfying z_t+1's that are conditioned on z_t and a_t+1.
    # 3 NumPy arrays of (nObs,) size, 1 NumPy array of (nClasses,) size.
    # child_obs_l_all_y: integer names of observations, indexes
    #   self.probsTrained.obs_inst_all.obsl_list
    # child_probs_l_all_y has the per-class probabilities, not pooled across
    #   all classes, only within class.
    # percat_counts is a list of integer class names, to tell you which elts in
    #   the 3 lists are for which class
    child_obs_l_all_y, child_probs_l_all_y, child_abs_pose_l_all_y, percat_counts = \
      self.probsTrained.find_tgt_obs_candidates (k_obs_src, action, parent_poses)
      # TODO TEMP line above is for when using absolute poses. Change to line below when use relative poses in tree
      #self.probsTrained.find_tgt_obs_candidates (k_obs_src, opt_action, parent_poses)

    if self.PROFILE: print ('1st get_obs_tgt_list() elapsed time: %f' % (time.time () - start_time))

    if child_obs_l_all_y.size == 0:
      # This means the tree path has to end here, as this node cannot find
      #   next absolute poses p_t+1 that is dist_thresh within p_t * a_t+1!
      #   This should not be possible, because with high enough threshold,
      #   there should always be p_t+1 nearby. dist_thresh is probably too low,
      #   increase it and try in next run. Will execute rollout policy instead
      #   of tree policy in this simulation.
      # Be careful, don't increase dist_thresh too much, else it voids the
      #   action-observation training data, as a high threshold means you'll
      #   be accepting just any abs pose, not even limited to the ones close to
      #   where the opt action takes you to! Then the whole point of opt action
      #   is thrown away.
      print ('%sWARN: infer_next_obs(): get_obs_tgt_list() returned empty observation list. See code comment for details.%s' % (
        ansi_colors.WARNING, ansi_colors.ENDC))
      return None, None

    # Sanity check
    if len (percat_counts) != len (classes):
      print ('%sERROR: len(percat_counts) != len(classes) in infer_next_obs(). Did you load probability files from as many classes as there are in the trained SVM model? This is a tree search assumption. Do that and try again. Returning Nones...%s' % (
        ansi_colors.FAIL, ansi_colors.ENDC))
      return None, None

    # To store 2D matrix, sized |b_t+1| x |Y|
    # Elements are p(z_t+1 | y, z_t, a_t+1)
    # This is used to marginalize across all y's, to decide the final
    #   p (z_t+1 | z_t, a_t+1) independent of y.
    # This will be a sparser matrix, `.` not all classes y will have all the
    #   child_obs_l_all_y. This is implementation choice, to make
    #   marginalization over y easier - just np.sum. Otherwise it's a headache
    #   and error-prone.
    obs_tgt_probs = np.zeros ((child_obs_l_all_y.size, len (classes)))

    # O(1) hashmap to map from child_obs to index in NumPy matrix
    sparse_obs_idx = range (0, child_obs_l_all_y.size)
    map_obs_to_sparse_idx = {}
    map_obs_to_sparse_idx.update (zip (child_obs_l_all_y, sparse_obs_idx))


    if self.PROFILE: start_time_classes = time.time ()

    # Used to access child_obs_l, child_obs_probs
    # e.g. [0 : percat_counts[0]], [percat_counts[0]-1 : percat_counts[1]], etc
    #   start of range is curr_list_start_idx, end of range is 
    #   percat_counts[curr_class_i]
    curr_class_i = 0
    curr_list_start = 0

    # Calculate p(z_t+1 | y, z_t, a_t+1) * p(y | x_t), store them for all y and
    #   all z_t+1.
    # Marginalize over y, i.e. loop over all y's
    for y_i in classes:

      # y
      k_class = self.svm.classifier_lbls [y_i]

      if self.PROFILE: start_time = time.time ()

      # Compute probability of each z_t+1
      # z_t+1's, p(z_t+1 | y, z_t, a_t)'s for each z_t+1.
      #   i.e. list of observations and their probabilities conditioned on
      #   the specific class.
      # Observation of the next node to create / visit in next recursive call
      #   of simulate()
      # Don't use node.children[opt_action], `.` this child may not have been
      #   created under the node yet! It could just be created in next
      #   simulate() call.

      #print (percat_counts)
      #print (curr_class_i)

      child_obs_l = child_obs_l_all_y [curr_list_start : \
        curr_list_start + percat_counts [curr_class_i]]
      child_obs_probs = child_probs_l_all_y [curr_list_start : \
        curr_list_start + percat_counts [curr_class_i]]
      # Update for next iter
      curr_list_start += (percat_counts [curr_class_i])
      curr_class_i += 1

      # This should always print 1.0!!
      #print (np.sum (child_obs_probs))


      # On scale of 0.001
      if self.PROFILE: print ('  2nd get_obs_tgt_list() elapsed time: %f' % (time.time () - start_time))

      # Choose the next observation z_t+1 (3rd eqn in mcts_2016-01-28.pdf):
      #   z_t+1 ~ p(z_t+1 | z_t, a_t+1) =
      #     sum_y (p(z_t+1 | y, z_t, a_t+1) * p_svm (y | x_t))
      # Alternative: Use linear dimensional analysis to pick best observation
      # Loop through the possible z_(t+1)'s
      # In each iteration, fixate z_(t+1), compute the sum_y of product of
      #   probs.

      # List comprehension is twice faster than for-loop
      # 1 x nObs. Index of the observation in the _l_all_y list
      mapped_sparse_idx = [map_obs_to_sparse_idx [obs] for obs in child_obs_l]

      # p(z_t+1 | y, z_t, a_t) * p_svm (y | x_t), pairwise multiply probs,
      #   for a specific z_t+1 (= child_obs_l [o_i]).
      #   Former is a list, latter is a float. Result is a list of probs,
      #   each item in list is for a class y.
      # Element-wise multiply
      #   obs_tgt_probs: nAllObs x nClasses numpy array
      #   child_obs_probs: 1 x nObs numpy array
      #   orig_pred_probs: 1 x nClasses numpy array
      # Each column of obs_tgt_probs should sum to 1.
      obs_tgt_probs [mapped_sparse_idx, y_i] = child_obs_probs * \
        orig_pred_probs [0, y_i]

      if rospy.is_shutdown ():
        print ('Received Ctrl+C signal in infer_next_obs(), terminating')
        return None, None

    # end class y_i

    if self.PROFILE: print ('y_i loop elapsed time: %f' % (time.time () - start_time_classes))
    if self.PROFILE: start_time = time.time ()

    #print ('obs_tgt_probs:')
    #print (obs_tgt_probs)


    # Choose the z_(t+1) that has max probability. However, instead of hard
    #   argmax, do a soft sampling by the probabilities, so that every z1
    #   gets a chance of being picked. Otherwise if always pick max, you may
    #   end up always picking the same one and never seeing others!
    # Sample from child_obs_l, using prob_child_obs as sampling probability
    #   http://docs.scipy.org/doc/numpy/reference/generated/numpy.random.choice.html
    # Marginalize over classes y
    # p(z_t+1 | z_t, a_t+1) = sum_y (p(z_t+1 | y, z_t, a_t+1) * p(y | x_t))
    # Result is 1D array, size len(child_obs_l)
    obs_probs_marg_y = np.sum (obs_tgt_probs, axis=1)
    # Normalize to sum to 1
    obs_probs_marg_y /= np.sum (obs_probs_marg_y)

    # Don't need anymore. `.` now always have multiple chords per action.
    #   Observation is a list now, not a single chord. 
    # TODO 15 Jan 2016: Test. Remove block when everything works.
    '''
    if self.multi_obs_per_action:

      # Take all observations with probabilities above a threshold

      nonzero_idx = np.where (obs_probs_marg_y) [0]
      nonzero_probs = obs_probs_marg_y [nonzero_idx]

      # There are 1000 bins. If one bin has a chance, it'll be 1/1000 on
      #   average, if every bin had 1 count. That is 0.001. Multiply that by
      #   however many percent you want to accept. For bins 5 times above
      #   use 0.005.
      # Or, use a more robust measure, mean + 2 stdevs! 2 stdevs gives you
      #   top 95%, by the 68-95-99.7 rule.
      prob_thresh = np.mean (nonzero_probs) + 2.0 * np.std (nonzero_probs)
      obs_set_idx = np.where (obs_probs_marg_y > prob_thresh) [0]

      # Sanity check. `.` otherwise 0-length will give run-time err at
      #   np.random.choice() below, when pass in first arg 0.
      if obs_set_idx.size == 0:
        print ('%sWARN: infer_next_obs(): Did not find any observations above prob_thresh. Will use rollout instead.%s' % (
          ansi_colors.WARNING, ansi_colors.ENDC))
        return None, None

      # Convert to NumPy array so can index multiple items at once
      obs_set = child_obs_l_all_y [obs_set_idx]

      # Probability of the set
      obs_set_probs = obs_probs_marg_y [obs_set_idx]
      obs_set_probs /= sum (obs_set_probs)

      # z*_t+1
      # Sample a z_t+1, using the probability p(z_t+1 | z_t, a_t+1) as prior
      # Choose from obs_set, `.` need to make sure nominal obs is actually in
      #   the set of observs we're adding to the node's histogram!
      # Indexes obs_set_idx, obs_set, obs_set_probs
      opt_child_obs_idx = np.random.choice (len (obs_set_idx), 1,
        p=obs_set_probs) [0]
      # Convert to list of 1-tuple. Index [0] to retrieve the tuple. Need tuple
      #   for ret val, `.` dictionary keys need to be hashable.
      opt_child_obs = obs_set [opt_child_obs_idx]
      # Nominal absolute pose corresponding to the observation
      child_abs_pose = child_abs_pose_l_all_y [obs_set_idx [opt_child_obs_idx]]

    # Only select one observation
    else:
    '''

    #print ('obs_probs_marg_y:')
    #print (obs_probs_marg_y)

    # z*_t+1
    # Sample a z_t+1, using the probability p(z_t+1 | z_t, a_t+1) as prior
    opt_child_obs_idx = np.random.choice (len (obs_probs_marg_y),
      p=obs_probs_marg_y)
    opt_child_obs = child_obs_l_all_y [opt_child_obs_idx]
    # Nominal absolute pose corresponding to the observation
    child_abs_pose = child_abs_pose_l_all_y [opt_child_obs_idx]

    # Don't need this var for chords. Remove when everything works
    # Convert to a numpy array of size (1,)
    #obs_set = np.array ([opt_child_obs])

    # end if multi_obs_per_action


    if self.PROFILE: print ('Rest of infer_next_obs() elapsed time: %f' % (time.time () - start_time))

    #print ('opt_child_obs:')
    #print (opt_child_obs)
    #print ('child_abs_pose:')
    #print (child_abs_pose)

    # opt_child_obs: Scalar integer name of an observation, indexes
    #   self.probsTrained.obs_inst_all.obsl_list.
    # child_abs_pose: Scalar integer name of a pose, indexes
    #   self.probsTrained.pose_inst_all.abs_poses.
    # obs_set: nObservs x 1 NumPy array
    #   Don't need for chords. Remove this var when everything works
    return opt_child_obs, child_abs_pose
      #, obs_set



  # ======================================================== Rollout policy ==

  # Calculates the misprediction cost at leaf (depth tau) of tree.
  #   If the last node (node_key) from tree policy in this simulation has not
  #   reached depth tau yet, randomly generate the remainder path of nodes
  #   needed to reach depth tau. Compute misprediction cost at leaf of this
  #   path, which is at depth tau.
  #   The randomly generated nodes are discarded - not added to tree, and the
  #   misprediction cost is returned.
  # Parameters:
  #   node_key: Key of existing node, under which we are trying to create edges
  #     and children.
  #   depth: Depth of node_key
  def rollout_policy (self, node_key, depth, parent_poses):

    print ('Rollout policy')

    src_node_key = deepcopy (node_key)
    src_obs_i = MCTSNode_Chords.get_obs_from_key (src_node_key)
    src_node = self.tree_nodes [node_key]
    curr_abs_pose = src_node.get_abs_pose ()

    curr_parent_poses = deepcopy (parent_poses)

    # Make a copy of node's histogram, to build on top of with the randomly
    #   generated node's observations
    incHist = IncrementalHist (*self.incHistParams,
      init_hist=src_node.get_incHist ().get_hist ())

    # Number of all possible actions across all classes
    n_poses = self.probsTrained.get_n_all_poses ()
    #print ('%d absolute poses across classes' % (abs_poses.shape [0]))

    # Range for for-loop
    remaining_depths = range (depth+1, self.params.horizon + 1)

    # To store randomly generated observations from depth+1 to horizon.
    # n x 3 matrix, n = len(range (depth, self.params.horizon + 1)), number of
    #   nodes needed to generate from depth+1 to horizon (tau), inclusive.
    #   i.e. tau + 1 - depth nodes.
    obs_generated = np.zeros ((len (remaining_depths),
      self.probsTrained.get_obs_dims ()))

    # c_m.
    # Movement cost from node_key depth to tau. Some nodes in between may be
    #   randomly generated in the loop above, if node_key is not at depth tau.
    # Movement cost of subpath starting at depth, ending at leaf. Basically
    #   sums the cost of all the edges.
    #   Includes the child edge a_t+1 of node at depth t. Must determine here
    #   `.` once return, since randomly generated nodes are discarded, parent
    #   doesn't know what these edges are, so it can't compute mvmt cost!
    mvmt_cost = 0


    # Traverse from given depth (this is the current new potential node's depth)
    #   to bottom of tree, sum up the rewards.
    for d_i in range (0, len (remaining_depths)):

      curr_depth = remaining_depths [d_i]

      # Instead of the UCB1 a* selection in tree policy, just select a random
      #   one in rollout policy.
      next_action_i = np.random.randint (n_poses)
      # 1 x POSE_DIMS NumPy array. Convert to tuple
      next_action = tuple (self.probsTrained.get_pose_all_i (next_action_i))

      # Add this action edge to movement cost
      # Pass in relative action, not absolute pose
      # Current node is the one just generated in prev iteration, last elt in
      #   curr_parent_poses list.
      curr_abs_pose = self.probsTrained.get_pose_all_i (
        curr_parent_poses [np.shape (curr_parent_poses) [0] - 1])
      rel_action = calc_relative_action (curr_abs_pose, next_action)
      mvmt_cost += calc_action_cost (rel_action)

    
      # Grab next state to go to, based on current state and random action.

      # Call infer_next_obs to get z_t+1 from a_t+1, like in tree policy.
      #   `.` the only difference btw tree and rollout policies is in choosing
      #   a_t+1, right? The part that chooses z_t+1 from a_t+1 should be the
      #   same for tree and rollout policies, both use p(z_t+1 | z_t, a_t+1),
      #   right?

      # p_svm (y | x_t), where t is time step (depth) of node_key
      #   This does not include the newly chosen action, at depth+1.
      # Use current histogram from randomly generated nodes so far
      orig_pred_probs, classes = self.svm.svm_predict_prob (
        incHist.get_as_feat ())

      # \hat{z}_t+1. Desired next observation, given based on a*_t+1
      tgt_obs_i, tgt_pose_i = self.infer_next_obs (src_obs_i,
        next_action_i, curr_parent_poses, orig_pred_probs, classes)
      # If no next obs is found, must break, `.` next behaviors undefined.
      #   k_obs_tgt will be None, won't have an obs to make a node, no point
      #   continuing!
      if tgt_obs_i is None:
        print ('Could not find next observation in rollout, breaking before reaching horizon.')
        # Truncate the rest of the depth that won't have any observations, so
        #   they don't get added to histogram in bin containing [0 0 0]!
        #   Usually there isn't such a bin anyway, but that prints a lot of 
        #   warning and errors that just shouldn't be there in the first place.
        if d_i > 0:
          # Take the depths so far populated
          obs_generated = obs_generated [0:d_i-1, :]
        else:
          # Re-init to empty matrix
          obs_generated = np.zeros ((0, self.probsTrained.get_obs_dims ()))
        break

      tgt_obs = self.probsTrained.get_obs_all_i (tgt_obs_i)
      obs_generated [d_i, :] = np.asarray (tgt_obs)


      # Set next iteration's starting observation z0 = this iteration's new
      #   observation z1
      src_obs_i = tgt_obs_i
      curr_abs_pose = tgt_pose_i
      # Append the actual next node's abs pose, not the action randomly chosen.
      #   The action is a raw action, the actual pose is chosen by
      #   infer_next_obs() and returned by it! `.` an action can correspond to
      #   many resultant poses in the neighborhood, and you need to choose one.
      #print ('Appending %d to parent_poses' % tgt_pose_i)
      curr_parent_poses = np.append (curr_parent_poses,
        [tgt_pose_i], axis=0)
      #print ('next curr_parent_poses in rollout_policy():')
      #print (curr_parent_poses)


    # Speedup
    if obs_generated.shape [0] > 0:
      # Add all randomly generated observations
      incHist.add_bin_count (obs_generated)

    # Get the predicted class of the leaf node
    leaf_pred_class = self.svm.svm_predict (incHist.get_as_feat ())

    # Get probabilities
    # p(y | x~_t+1)
    # Run SVM on the modified hist x~_t+1
    # Returns nSamples x nClasses array
    leaf_pred_probs, _ = self.svm.svm_predict_prob (incHist.get_as_feat ())

    # p(y_t+1 != y)
    mispred_cost = 1.0 - np.max (leaf_pred_probs)

    # Objective cost function
    #   C = (1-lambda) * (1/tau) * mvmt_cost + lambda * mispred_cost
    # Removing the movement cost makes the reward plot trend go steadily down!
    #   It should go upwards, as I've seen when things work right.
    # Enforce reward in range [0,1], which UCB1 policy assumes!
    #   So I'm changing the objective here, with
    #   (1-lambda)*mvmt + lambda*mispred
    #   instead of our written objective
    #   mvmt + lambda*mispred
    # Now since both mispred_cost and mvmt_cost are within [0, 1], weighing
    #   them by a weight in [0, 1] will also produce a final cost within [0, 1]!
    #   Then reward 1-cost will also be in [0, 1]!
    # Multiply 1/tau, in effect taking the average movement cost, when all
    #   layers' mvmt costs are summed. This weighs them early. `.` after fn
    #   returns, caller doesn't know which part of the cost is mvmt, which
    #   part is mispred, so caller can't take this average!
    subpath_cost = \
      (1.0 - self.mispred_lambda_wt) * mvmt_cost / self.params.horizon + \
      self.mispred_lambda_wt * mispred_cost
    subpath_reward = 1.0 - subpath_cost

    return subpath_reward, leaf_pred_class



  # ============================================== After end of tree search ==

  # Call this before returning from simulate(), to update all PyDot Edges
  #   with the final rewards, and PyDot Nodes with the final histograms'
  #   predicted classes, so caller of simulate() can write the complete tree
  #   PNG with useful debug info to file.
  #   > 16 Jan 2017: It doesn't seem like I'm still doing this. I don't know
  #     why, it's not documented.
  # Call this after sel_path(), `.` sel_path will update all the incHists in
  #   the nodes on the chosen optimal path, so here we can label each node
  #   correctly with the predicted class from the latest incHist in each node!
  def update_pydot_tree_lbls (self):

    counter = 0

    #print ('Extended edge labels:')
    for node_key in self.tree_nodes.keys ():

      node = self.tree_nodes [node_key]

      new_node_lbl = node.make_node_lbl_ext (self.svm) #self.nn)
      node.pydot_node.set_label (new_node_lbl)

      for reward_key in node.rewards.keys ():

        action = reward_key [0]
        child_key = reward_key [1]

        # Make extended edge label that includes the reward for this action
        #new_edge_lbl = node.make_edge_lbl_ext (action, child_key)
        # 7 Feb 2017 TEMP before fixing to use *relative* action in tree
        new_edge_lbl = node.make_edge_lbl_ext (action, tuple (self.probsTrained.get_pose_inst_all ().get_pose_i (action)), child_key)
        #print (new_edge_lbl)

        # Update edge label in pydot visualization graph
        node.child_pydot_edges [reward_key].set_label (new_edge_lbl)
        #print (node.child_pydot_edges [action].get_label ())
        #print ('')

        counter += 1

    print ('%d edges updated to extended labels. %d edges total (make sure these numbers are same, else you are missing edges either in PyDot image or in actual tree)' % (counter, 
      len (self.edge_vis_dict.keys ())))


  # After tree search is finished, select a max-reward path from root to leaf
  #   to return
  # Returns a list of n actions with max reward. Actions are
  #   7-tuples (tx ty tz qx qy qz qw).
  # Parameters:
  #   prev_poses: Poses from a previous tree, to eliminate in this tree's
  #     final returned opt path. `.` same abs poses do not produce new info
  #     and might even bias the tree by repeating bin counts!
  def sel_path (self, root_key, prev_poses):

    print ('Select best reward path:')

    # Looks at actions from root. Pick the action with max reward out of all
    #   actions at root. Follow that action down to layer 1's node. Then at
    #   that node, pick the action with max reward, etc. Traverse downwards,
    #   at each node, picking the action with max reward across all actions at
    #   that node.
    #   This is `.` when we backtrack after each search, we update
    #   reward += simulate(), and simulate() is recursive call, and it returns
    #   from children. So each time we are adding children's rewards to
    #   parent. That's why the higher layers' nodes have the cumulative
    #   rewards.
    #   The backtrack is so that you don't need to do depth first search at the
    #   end. It lets you simply follow the action with max reward at each node,
    #   and traverse in one pass from root to leaf! Now I know.

    color = '#00dd00'

    # Start at root
    curr_node = self.tree_nodes [root_key]
    # Root is the only node that we can use nominal_abs_pose. All lower nodes'
    #   abs pose would change as we traverse down the tree, and their
    #   nominal_abs_pose aren't updated - a design decision, so that I can
    #   find duplicate poses and not allow them. TODO something needs to be
    #   done about this. I would prefer to keep all nodes' nominal_abs_pose
    #   to be updated in update_node_in_traverse(), rather than having this
    #   special case that I have to handle differnetly everywhere in code!!!
    curr_pose_i = curr_node.get_abs_pose ()
    curr_pose = self.probsTrained.get_pose_all_i (curr_pose_i)
    # Color the max_reward_edges in pydot visualization
    self.set_pydot_node_color (
      MCTSNode_Chords.make_node_name_from_key (root_key), color)

    # 1D numpy array of integer names of abs poses
    parent_poses_num = np.array ([curr_node.get_abs_pose ()])

    # Debug count
    n_dups_eliminated = 0

    # Keep traversing down the tree until leaf
    #   Depth is 0 at root, horizon at leaf. Total horizon+1 layers.
    #   We go down by number of edges, so loop through horizon number of edges.
    for depth in range (0, self.params.horizon):

      #print ('Rewards at opt node at depth %d:' % depth)
      #print (curr_node.rewards.values ())

      rewards = curr_node.rewards.values ()
      # If this node has no more children, that means this path does not reach
      #   the horizon. Then don't pick this path.
      if not rewards:
        break

      # Index of max reward
      # keys() and values() directly correspond, if called without intervention
      #   Ref: http://stackoverflow.com/questions/835092/python-dictionary-are-keys-and-values-always-the-same-order
      max_r_idx = np.argmax (np.asarray (rewards))

      # Sample an action based on a probability distribution based on the
      #   rewards, instead of picking the hard max. This is supposed to work
      #   better, diversifying the actions instead of always choosing the same
      #   one.
      # TODO 9 Dec 2016: Test this. It is not easy to pick a probability
      #   distribution, `.` each action's probability is probably NOT linearly
      #   proportional to the reward! But I don't know what other way is
      #   reasonable to assign probabilities to the rewards.
      # Normalize the rewards to [0, 1] range. Subtract the min to make everything
      #   > 0, then divide by the resulting max to make everything in [0, 1].
      #prob_actions = np.asarray (rewards) - np.minimum (rewards)
      # Divide by the sum (not the max!) so the probability array sums to 1
      #prob_actions = prob_actions / np.sum (prob_actions)
      #max_r_idx = np.random.choice (len (rewards), 1, replace=False,
      #  p=prob_actions)

      # Access the action key that has max reward
      max_r_action = curr_node.rewards.keys () [max_r_idx] [0]

      #print ('Depth %d, current node: %s' % (depth, curr_node.get_key ()))
      #print ('Children keys:')
      #print (curr_node.children.keys ())

      # Access child, connected to this node by the max-reward action.
      #   Each action can have multiple child nodes! Pick one.
      # TODO: Now just accessing [0], until have better idea what to do about
      #   multiple observations at each action.
      #   One way: Tree policy needs to be updated to look at each individual
      #   observation, instead of all of them. Then the reward is not
      #   associated with each action, but associated with each (action, observ)
      #   pair. Then here, we can add an index to the observation that had the
      #   highest reward.
      #rand_obs_idx = 0
      # 16 Jan 2017: Try this, see if better. Pick a random one instead of [0]
      #rand_obs_idx = np.random.randint (len (curr_node.children [
      #  max_r_action]))
      #next_node_key = curr_node.children [max_r_action] [rand_obs_idx]
      # 19 Feb 2017: Fixed this in triangles code version on 4 Feb 2017 but
      #   forgot to add the fix to this version. above rand_obs_idx is
      #   wrong. This is correct. 
      #   I already have the observation child_key, from the
      #   reward_key at max_r_idx above! The child key is simply
      #   keys()[max_r_idx][1].
      # Try this if the above doesn't produce good results.
      next_node_key = curr_node.rewards.keys () [max_r_idx] [1]


      # Calculate the multiple observations per action, which need to be
      #   recomputed each time we revisit a node, `.` based on which edge we
      #   come from, the set of observations will be different.
      orig_pred_probs, classes = self.svm.svm_predict_prob (
        curr_node.incHist.get_as_feat ())
      child_obs_i, child_pose_i = self.infer_next_obs (curr_node.get_obs (),
        max_r_action, parent_poses_num, orig_pred_probs, classes)

      self.update_node_in_traverse (curr_node.get_key (),
        next_node_key, child_obs_i)


      # Color the max_reward_edges in pydot visualization
      self.set_pydot_node_color (
        self.tree_nodes [next_node_key].make_node_name (), color)
      edge_name = MCTSNode_Chords.make_edge_vis_dict_key (
        curr_node.get_key (), max_r_action,
        self.tree_nodes [next_node_key].get_key ())
      self.set_pydot_edge_color (edge_name, color)


      # TODO 19 Feb 2017: This is wrong! You cannot calculate max_rel_action
      #   using next_node's abs pose. next_node's abs pose isn't updated
      #   in update_node_in_traverse(). It's the nominal abs pose that it had
      #   when it was first created. That is only used for checking duplicates
      #   in parent_poses in simulate(), should not be used to calculate final
      #   opt path!! Opt path needs the action chosen live at every step as
      #   we traverse down. In this case, that is max_r_action.
      # Convert absolute child pose to relative action
      #curr_pose_i = curr_node.get_abs_pose ()
      #curr_pose = self.probsTrained.get_pose_all_i (curr_pose_i)
      # 8 Feb 2017: For absolute pose, use integer name. When change to
      #   relative pose, 
      #next_pose_i = self.tree_nodes [next_node_key].get_abs_pose ()
      #next_pose = self.probsTrained.get_pose_all_i (next_pose_i)
      #max_rel_action = calc_relative_action (curr_pose, next_pose)

      # 19 Feb 2017: Try this
      next_pose_i = max_r_action
      next_pose = self.probsTrained.get_pose_all_i (next_pose_i)
      max_rel_action = calc_relative_action (curr_pose, next_pose)

      # Don't add max_rel_action to max_reward_edges yet.
      #   Wait until after end of loop, when sure which abs poses
      #   are unique. Don't want to add relative actions for duplicate abs
      #   poses, and can't just remove actions from list afterwards, `.`
      #   relative actions depend on the abs poses before and after, one of
      #   which may be removed, so the relative action will need to be recalced!

      # Replace edge label that is absolute pose, with relative pose
      new_edge_label = MCTSNode_Chords.make_edge_vis_dict_key (
        curr_node.get_key (), max_rel_action,
        self.tree_nodes [next_node_key].get_key ())
      self.relabel_pydot_edge (edge_name, new_edge_label)

      # Add latest node to abs poses on parent path
      # Check duplicates. If duplicated, do not add.
      # The prev_poses condition is added on 20 Feb 2017, after messing with
      #   prob_reader.py find_tgt_obs_candidates() and making a mess and
      #   resulted in a teeny un-fully-explored tree, so decided to just add
      #   this here, much simpler solution and enables full tree search still.
      #   HOWEVER, result may be that you get ZERO actions from later trees!!!!!
      #   TODO 20 Feb 2017: Is there a better way?
      if np.all (parent_poses_num != next_pose_i) and \
         np.all (prev_poses != next_pose_i):
        parent_poses_num = np.append (parent_poses_num, [next_pose_i], axis=0)
      else:
        n_dups_eliminated += 1

      # Update for next iter
      curr_node = self.tree_nodes [next_node_key]
      # Don't use curr_node.get_abs_pose()! Other than root, no other node's
      #   nominal_abs_pose is current in the traversal. Must use the actual
      #   pose we chose as we traverse down the tree - the pose passed to
      #   update_node_in_traverse()
      curr_pose = next_pose

    # Get histogram at leaf node
    max_reward_hist = curr_node.get_incHist ()
    # Get observation at last node in path
    last_obs_i = curr_node.get_obs ()


    # Convert integer names of abs poses, into actual abs poses
    parent_poses = np.zeros ((parent_poses_num.shape [0],
      self.probsTrained.get_pose_dims ()))
    for i in range (0, parent_poses_num.shape [0]):
      pose = self.probsTrained.get_pose_all_i (parent_poses_num [i])
      parent_poses [i, :] = pose

    # Calculate relative actions, now that absolute poses are finalized and
    #   duplicates discarded
    # List of actions that gave max reward during tree search
    max_reward_edges = self.convert_abs_poses_to_rel_actions (parent_poses)


    print ('Number of edges on max-reward path (horizon=%d): %d' % (
      self.params.horizon, max_reward_edges.shape [0]))

    print ('%d absolute poses on the max-reward path (%d duplicates have been eliminated as dups do not produce new info):' % (
      parent_poses.shape [0], n_dups_eliminated))
    for i in range (0, parent_poses.shape [0]):
      pose = parent_poses [i, :]

      #print ('%.2f %.2f %.2f' % (pose[0], pose[1], pose[2]))
      print ('abs pose p%d: %s' % (parent_poses_num [i], str (pose)))

      # There are 1 fewer relative poses than there are absolute poses.
      # Relative poses start at abs pose 0->1, 1->2, ..., n-1 -> n.
      #   Abs pose at root is printed first. No relative action leads to
      #   it. First relative action is from root to depth 1 nodes. So start
      #   printing at depth 1.
      if i > 0:
        print ('relative action a: %s' % str (max_reward_edges [i-1, :]))

    # max_reward_edges: n x 7 numpy array of relative actions
    # max_reward_hist: IncrementalHist object
    # parent_poses: n x 7 numpy array of of abs poses
    # parent_poses_num: 1D numpy array of integer names of abs poses
    return max_reward_edges, max_reward_hist, parent_poses, parent_poses_num, \
      last_obs_i


  # Converts absolute poses into relative actions btw each consecutive pair of
  #   poses.
  # Returns (n-1) x 7 numpy array, for abs_poses that is n x 7.
  # Parameters:
  #   abs_poses: List of absolute poses. A relative action will be computed
  #     for each consecutive pair of poses in the list.
  def convert_abs_poses_to_rel_actions (self, abs_poses):

    max_reward_edges = np.zeros ((abs_poses.shape [0] - 1, 0))

    for i in range (0, abs_poses.shape [0] - 1):

      from_pose = abs_poses [i, :]
      to_pose = abs_poses [i+1, :]

      max_rel_action = calc_relative_action (from_pose, to_pose)

      #max_reward_edges = np.append (max_reward_edges, [max_rel_action], axis=0)
      # Resize to the right number of columns (dimensions of action)
      if max_reward_edges.size == 0:
        max_reward_edges = np.resize (max_reward_edges,
          (max_reward_edges.shape [0], max_rel_action.size))
      max_reward_edges [i] = max_rel_action

    return max_reward_edges



  # TODO: Did not port visualize_*() and random_poses() functions over from
  #   triangles. Might not need. visualize_*() will need to be rewritten
  #   anyway. Random poses might not be sufficient for comparison, can compare
  #   to depth-1 tree simply.


# =========================================== tree class specific to chords ==

class MCTS_ActiveChords (MCTS_ActiveTouch):

  # ================================================== Creation and updates ==

  # Create a new node in the tree
  #   obs_inst: ObservationList of the entire tree. Returned from
  #     ProbReader.get_obs_inst_all ().
  #   node_obs: Integer name of the observation at the node to be created.
  #     Integer name accesses the ObservationList.
  #   depth: Depth of node to be created. Integer.
  #     node_obs and depth together create the node's key.
  #   abs_pose: Integer name of the nominal absolute pose associated with this
  #     observation, from training data. Integer name accesses PoseList.
  def create_node (self, node_obs, depth, abs_pose):

    # If node already exists, don't recreate it.
    node_key = MCTSNode_Chords.get_key_from_params (node_obs, depth)
    if node_key in self.tree_nodes.keys ():
      return self.tree_nodes [node_key]

    # Now store absolute poses in actions, `.` as traverse down, each
    #   node's absolute pose change (due to parents' absolute pose and action
    #   edge), therefore this node's relative action also have to change.
    #   `.` relative poses are calculated using this node's abs pose, which
    #   changes!
    # Much easier than storing a pose computed from relative poses - slower
    #   tree search and more code!
    # 1D NumPy array. Integer names (indices) of abs poses
    action_list = self.probsTrained.get_pose_inst_all ().get_idxes ()

    # Create node
    node = MCTSNode_Chords (self.probsTrained.get_obs_inst_all (), 
      node_obs, depth, abs_pose, action_list, self.incHistParams, self.svm)
    self.tree_nodes [node.key] = node

    # Create pydot obj
    node_name = self.tree_nodes [node_key].make_node_name ()
    if node_name not in self.node_vis_dict.keys ():
      self.node_vis_dict [node_name] = node.pydot_node
      self.tree_vis.add_node (node.pydot_node)

    # Update field for log
    if depth > self.depth_reached:
      self.depth_reached = depth

    return node


  # Create a new edge btw two existing nodes. No new node is created.
  #   Edge goes from parent_key to child_key.
  # Parameters:
  #   parent_key: parent node key
  #   edge_action: Integer name of edge action connecting parent_key to new
  #     node. A row in PoseList.
  #   child_obs: Integer name of observations. nObservations-elt 1D NumPy
  #     array. Indexes self.probsTrained.obs_inst_all.obsl_list.
  def create_edge (self, parent_key, edge_action, child_key, child_obs):

    if child_key not in self.tree_nodes.keys ():
      print ('%sERROR in create_edge(): Both parent and child nodes must exist, before creating an edge between them! Create child node first.%s' % (
        ansi_colors.FAIL, ansi_colors.ENDC))
      return

    # Create edge
    if parent_key != 'null':
      # 7 Feb 2017: Temporary before fixing to use *relative* acton everywhere.
      #   This line is only applicable when actions are absolute, then an
      #   index exists for it in pose_inst_all.
      edge_action_data = self.probsTrained.get_pose_inst_all ().get_pose_i (edge_action)
      if edge_action_data is not None:
        # Convert to tuple to be hashable
        edge_action_data = tuple (edge_action_data)

      pydot_edge = self.tree_nodes [parent_key].add_child (edge_action,
        edge_action_data, self.tree_nodes [child_key])

      # Add pydot edge to pydot graph, if not already in graph
      edge_name = MCTSNode_Chords.make_edge_vis_dict_key (parent_key,
        edge_action, child_key)
      if edge_name not in self.edge_vis_dict.keys ():
        self.edge_vis_dict [edge_name] = pydot_edge
        self.tree_vis.add_edge (pydot_edge)

    self.update_node_in_traverse (parent_key, child_key, child_obs)

    if self.DISPLAY_TREE_PER_ADD:
      self.display_tree (self.tree_vis)


