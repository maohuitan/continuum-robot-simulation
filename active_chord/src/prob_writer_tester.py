#!/usr/bin/env python

# Mabel Zhang
# 7 Jan 2016
#
# Tests io_probs.py
#
# $ rosrun active_chord io_probs.py
# $ rosrun active_chord io_probs_tester.py
#

# ROS
import rospy
import tf
from geometry_msgs.msg import Vector3

# Python
import random
import time
import signal
import os
import subprocess

import numpy as np

# My packages
from chord_training_msgs.srv import ProbWriterSrv, ProbWriterSrvRequest
from chord_training_msgs.msg import Chord
from chord_training_msgs.srv import ChordSrv, ChordSrvRequest
from chord_recognition.chord_consts import ChordConsts
from chord_recognition.chord_reader import ChordReader
from chord_recognition.chord_writer import ChordWriter
from util_continuum.ansi_colors import ansi_colors


# Supress detail printouts
SUPRESS = True

def gen_random_pose (ang=0, req=None):

  if req is None:
    req = ProbWriterSrvRequest ()

  if ang != 0:
    # Rotate by the angle
    rot_axis = [0, 0, 1]
    q = tf.transformations.quaternion_about_axis (ang, rot_axis)

    print ('angle: %s' % ang)
    print ('q: %.2f %.2f %.2f %.2f' % (q[0], q[1], q[2], q[3]))

    # Some arbitrary vector to rotate around rot_axis
    # NOTE This must be different from rot_axis, otherwise it won't rotate,
    #   `.` rotating a vector any degrees wrt itself is just identity.
    some_vec = [1, 0, 0, 1]

    # Multiply by homogeneous coordinate
    q_mat = tf.transformations.quaternion_matrix (q)
    v = np.dot (q_mat, some_vec)

    print (q_mat)
    print ('v: %s' % v)
    #print ('v: %.2f %.2f %.2f' % (v[0], v[1], v[2]))

  else:
    v = np.array ([random.random (), random.random (), random.random ()])

  v /= np.linalg.norm (v)
  print ('v normalized: %s' % v)

  req.abs_pose.x = v [0]
  req.abs_pose.y = v [1]
  req.abs_pose.z = v [2]

  return req


# Copied from chord_recognition chord_io_tester.py
#   mode: Only used to pass to chord_writer.py on cmd line
#   obj_cat: Only used to pass to chord_writer.py on cmd line
def write_chords (chords, mode, obj_cat, chord_base):

  n_chords = len (chords)

  # Launch chord_writer, which advertises the service
  _ = subprocess.Popen (['rosrun', 'chord_recognition', 'chord_writer.py',
    '--' + mode, '--obj_base', chord_base, '--obj_cat', obj_cat])

  print ('Waiting for rosservice /chord_training/chord_srv to come up...')
  try:
    rospy.wait_for_service ('/chord_training/chord_srv')
    chord_srv = rospy.ServiceProxy ('/chord_training/chord_srv', ChordSrv)
  except rospy.ServiceException, e:
    print 'rosservice call to /chord_training/chord_srv FAILED: %s' % e


  for i in range (n_chords):

    # Last chord
    finished = (i == (n_chords - 1))
    chord_resp = chord_srv (True, chords [i], finished)

    # Last service call was sent, don't need to send a separate one just to
    #   signal finish
    if finished:
      ended_unexpectedly = False

    if not SUPRESS:
      print ('Published chord %d. l: %f' % (i, chords[i].l))



def main ():

  # Set disable_signals=True to use my own Ctrl+C signal handler, so that
  #   I can sending finish signal to file writer!
  #   Note there will be no rospy.exceptions to catch Ctrl+C, need to use
  #     KeyboardInterrupt.
  #   Tested in test_shutdown.py in triangle_sampling pkg in reFlexHand repo.
  rospy.init_node ('prob_writer_tester', anonymous=True, disable_signals=True)
  # Connect custom SIGTERM handler
  signal.signal (signal.SIGTERM, sigterm_handler)

  print ('%sprob_writer_tester.py: Detailed debug printouts are SUPRESSed (constant SUPRESS=True).%s' % (
    ansi_colors.OKCYAN, ansi_colors.ENDC))


  #####
  # User adjust params

  n_poses = 10
  pose_angs = np.linspace (0, np.pi, n_poses)

  # Test a small number of chords
  n_chords_per_pose = 7


  #####
  # Call rosservice to write probabilities file

  print ('Waiting for rosservice to come up...')
  try:
    rospy.wait_for_service ('/chord_training/prob_srv')
  except rospy.exceptions.ROSInterruptException, err:
    return
  except KeyboardInterrupt:
    return

  # Need to pause, else first message never received on rostopic. Weird
  time.sleep (0.5)


  #####
  # Read a chords file, so we can test with real chords data

  mode = 'gl'

  #obj_cat = 'cube'
  #obj_base = 'cube_20cm_scale1dot3.csv'

  #obj_cat = 'cylinder'
  #obj_base = 'cylinder_12x25cm_DxH_scale2dot2.csv'

  obj_cat = 'sphere'
  obj_base = 'sphere_20cm_scale1dot6.csv'

  consts = ChordConsts ()
  chord_reader = ChordReader (obj_base, obj_cat, mode, consts)

  # Copied from histogram_of_chords.py
  samples = np.zeros ((chord_reader.get_n_chords (), consts.N_PARAMS))
  samples [:, consts.L_STR_IDX]  = chord_reader.chords ['L_STR']
  samples [:, consts.C_LON_IDX]  = chord_reader.chords ['C_LON']
  samples [:, consts.C_LAT_IDX]  = chord_reader.chords ['C_LAT']
  samples [:, consts.N0_LON_IDX] = chord_reader.chords ['N0_LON']
  samples [:, consts.N0_LAT_IDX] = chord_reader.chords ['N0_LAT']
  samples [:, consts.N1_LON_IDX] = chord_reader.chords ['N1_LON']
  samples [:, consts.N1_LAT_IDX] = chord_reader.chords ['N1_LAT']

  chord_dir = os.path.dirname (chord_reader.get_chord_name ())
  chord_base = 'tmp_pose_chord'


  # These params are constants that will not change
  req = ProbWriterSrvRequest ()

  req.mode = mode
  req.obj_cat = obj_cat
  req.obj_base = obj_base

  # Actual values
  #req.disc_l = 0.003
  #req.disc_ang = 0.017
  #req.disc_pose = 0.03
  # Test values. Large values, to test discretizee() and duplicates
  req.disc_l = 0.2
  req.disc_ang = 0.2
  req.disc_pose = 0.2

  req.valid = True
  req.finished = False


  p_i = 0
  ended_unexpectedly = True

  # Generate random (pose, observation)
  while not rospy.is_shutdown ():

    req = gen_random_pose (pose_angs [p_i], req)

    chords = []

    # Create a temp file name
    req.chord_name = os.path.join (chord_dir, chord_base + '.csv')

    # Just populate 3 chords for testing
    for c_i in range (0, n_chords_per_pose):

      idx = p_i * n_chords_per_pose + c_i

      # seq doesn't matter for chord in probabilities. Don't set it
      chord = Chord ()
      chord.l = samples [idx, consts.L_STR_IDX]
      chord.c_lon = samples [idx, consts.C_LON_IDX]
      chord.c_lat = samples [idx, consts.C_LAT_IDX]
      chord.n0_lon = samples [idx, consts.N0_LON_IDX]
      chord.n0_lat = samples [idx, consts.N0_LAT_IDX]
      chord.n1_lon = samples [idx, consts.N1_LON_IDX]
      chord.n1_lat = samples [idx, consts.N1_LAT_IDX]
    
      chords.append (chord)

    # Write chords to temp file
    write_chords (chords, mode, obj_cat, chord_base)

    print ('abs_pose vector: %.2f %.2f %.2f' % (req.abs_pose.x, req.abs_pose.y, req.abs_pose.z))


    try:
      # Recreate a service each time, instead of using persistent connection,
      #   `.` if server restarts, former lets you reconnect to new server
      #   automatically!
      # Ref: http://wiki.ros.org/rospy/Overview/Services
      rospy.wait_for_service ('/chord_training/prob_srv')
      prob_srv = rospy.ServiceProxy ('/chord_training/prob_srv', ProbWriterSrv)
      # Last chord
      req.finished = (p_i == (n_poses - 1))
      # API http://docs.ros.org/jade/api/rospy/html/rospy.impl.tcpros_service.ServiceProxy-class.html
      prob_resp = prob_srv (req)

      # Last service call was sent, don't need to send a separate one just to
      #   signal finish
      if req.finished:
        ended_unexpectedly = False
    except rospy.ServiceException, e:
      print 'rosservice call to /chord_training/prob_srv FAILED: %s' % e
    # Ctrl+C
    except rospy.exceptions.ROSInterruptException, err:
      pass
    except KeyboardInterrupt:
      break

    print ('Published pose %d' % (p_i))


    try:
      time.sleep (0.3)
    except rospy.exceptions.ROSInterruptException, err:
      break
    # When using my custom signal handler, won't have rospy.exceptions error
    except KeyboardInterrupt:
      break

    p_i += 1
    if p_i >= n_poses:
      break

    print ('')

  # Custom signal handler, to indicate to save file. Must put it in a separate
  #   fn than main(), `.` SIGTERM gets sent to main() and still kills rossrv
  #   call!
  sigint_shutdown (ended_unexpectedly, req)


# Do nothing, allow rossrv to finish being sent to subscriber
# Ref: docs.python.org/2/library/signal.html
def sigterm_handler (signal_number, stack_frame):
  return


# Custom SIGTERM signal handler
def sigint_shutdown (ended_unexpectedly, req):

  if ended_unexpectedly:
    print ('Ended unexpectedly. Calling with final finished service call.')
    try:
      rospy.wait_for_service ('/chord_training/prob_srv')
      prob_srv = rospy.ServiceProxy ('/chord_training/prob_srv', ProbWriterSrv)
      req.finished = True
      prob_resp = prob_srv (req)
    except rospy.ServiceException, e:
      print 'rosservice call to /chord_training/prob_srv FAILED: %s' % e

  # Manually tell ROS to cleanup properly
  # This line is required when using custom Ctrl+C signal handler (via
  #   siable_signals=True to init_node())
  rospy.signal_shutdown ('Normal shutdown by custom sigterm handling')


if __name__ == '__main__':
  main ()

