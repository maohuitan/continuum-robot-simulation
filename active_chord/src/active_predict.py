#!/usr/bin/env python

# Mabel Zhang
# 16 Jan 2017
#
# Copied and modified from mcts_active_touch.py in active_touch used for
#   triangles.
#
# Entry point of this package. See README file for usage.
#
# This file is responsible for the highest level of active touch. It calls
#   classes that deal with the probabilities matrix in .pkl file trained from
#   objects, that build the incremental histogram using the observations from
#   the probabilities matrix, etc.
# This file predicts the object class as a result of the active algorithm.
#
# Chord parameters loaded must be the same across these three things:
#   SVM, loaded in this file with svm_name
#   probabilities .pkl file, loaded by prob_reader.py
#   chords from test-time execution of tree search result, computed by
#     robot-dependent test-time execution script.
#
#   If you change definition of chords, make sure to change in all the above.
#   If parameters don't match, then you'll get garbage results. `.`
#     you'd either be running SVM trained from one set of parameters on data
#     that have another set of parameters (most erroneous), or you look at
#     probabilities trained from one set of parameters and then load another
#     set at test-time execution (noticeable when you compare the histograms
#     of tree to Gazebo).
#
# Main parameters:
#   mode: ActP.NOROBOT to run tree only, 'gl' simulation, or real robot
#   n_sims: Number of simulations of tree search. Each simulation creates
#     exactly one node. Root is not included in this count. After n
#     simulations, there will be n+1 nodes in the tree.
#

# ROS
import rospy
from visualization_msgs.msg import Marker, MarkerArray

# Python
import argparse
import os
import csv
import time, datetime
import pickle

# NumPy
import numpy as np

# Matplotlib
import matplotlib
import matplotlib.pyplot as plt

# My packages
from util_continuum.ansi_colors import ansi_colors
from util_continuum.parse_meta import read_meta_file, get_meta_cat_name
from chord_recognition.chord_consts import ChordConsts
from util_continuum.config_paths import config_datapath, get_root_path, \
  get_hist_path, get_active_meta_path, get_svm_model_path, get_nbins_subpath
from chord_recognition.parse_args import parse_args_for_histogram, \
  config_params_from_args
from chord_recognition.histogram_of_chords import HistogramConfig

# Local
from prob_reader import ProbReader_Chords
from incremental_histogram import IncrementalHist
from classifier import SVMClassifier, NNClassifier, get_nn_dist
from mcts_mdp import MCTS_Parameters, plot_rewards
from mcts_active_touch import MCTS_ActiveChords
from config_active import ActiveParams as ActP
# TODO: This doesn't exist yet!!!
# TODO: Execution needs to collect observations and send the actual histogram
#   back (histogram built using histogram_of_chords.py HistogramFromChord?
#   See usage in histogram_writer.py)
#   Probably implement as a rosservice call?
from execute_actions import ExecuteActions
from action_util import calc_result_plane


def main ():

  rospy.init_node ('active_predict', anonymous=True)

  vis_pub = rospy.Publisher ('/visualization_marker', Marker,
    queue_size=5)
  vis_arr_pub = rospy.Publisher ('/visualization_marker_array', MarkerArray,
    queue_size=5)

  # For ICRA PDF font compliance. No Type 3 font (rasterized) allowed
  #   Ref: http://phyletica.org/matplotlib-fonts/
  matplotlib.rcParams['pdf.fonttype'] = 42
  matplotlib.rcParams['ps.fonttype'] = 42


  # ============================================== Parse command line args ==

  # Add a few custom arguments before calling parse_args_for_svm() for the
  #   generic args shared among programs.
  arg_parser = argparse.ArgumentParser ()
  arg_parser.add_argument ('--meta_truth', type=str,
    default='models_active_truth.txt',
    help='Object to recognize at test time. This is different from --meta, which is the data set of objects with trained probabilities. This object will simply be loaded as a mesh in RViz, no other data. Meta file should contain a single uncommented line, for the object.')
  #arg_parser.add_argument ('--meta_nn', type=str,
  #  default='models_active_hist.txt',
  #  help='Histograms of objects corresponding to current probs pkl data being loaded. These ground truth hists are used to compute NN dists btw hist from tree search and simulation, to the ground truth.')

  args, valid = parse_args_for_histogram (arg_parser)
  if not valid:
    return

  # Copied from chord_recognition histogram_reader.py
  mode = config_params_from_args (args)
  nbins_subpath, _ = get_nbins_subpath (args.nbins)

  chord_consts = ChordConsts ()
  # Read hist_conf.yaml
  hist_config = HistogramConfig (mode, nbins_subpath, chord_consts)
  # List
  n_bins = hist_config.get_n_bins ()
  # 7 x 2 NumPy array. i.e. List of 7 lists of 2 floats
  #   ((min,max), (min,max), (min,max), ...)
  bin_ranges = hist_config.get_bin_ranges ()

  svm_name, svm_lbls_name = get_svm_model_path (mode, nbins_subpath)
  svm = SVMClassifier (svm_name, svm_lbls_name)


  # Output log file of predictions from tree search + execution
  # Ref: http://stackoverflow.com/questions/13890935/timestamp-python
  timestamp = time.time ()
  timestring = datetime.datetime.fromtimestamp (timestamp).strftime (
    '%Y-%m-%d-%H-%M-%S')

  log_column_titles = [ActP.LG_ROBOT,
    ActP.LG_ITER, ActP.LG_NSIMS, ActP.LG_HORIZON, ActP.LG_DEPTH,
    ActP.LG_TREETIME, ActP.LG_TREETIME_AVG,
    ActP.LG_POSES, #ActP.LG_PLANNED,
    ActP.LG_TOUCHES, ActP.LG_TRIS,
    ActP.LG_TIMESTRING, ActP.LG_EXECTIME,
    ActP.LG_TRUTH,
    ActP.LG_SVM_PRED] #, ActP.LG_NN_INNER_PRED, ActP.LG_NN_INTER_PRED]

  recog_log_file = None


  # Output pickle file of all actively selected poses
  poses_log_name = None

  # Output rewards
  rewards_csv_name = None
  rewards_pkl_name = None


  ################################################### Consts and parameters ##

  #####
  # RUN_MODE
  #####

  # The real run of program. Uncomment many object classes in config file.
  TREE = 0

  # Random baseline
  RANDOM = 1

  RUN_MODE = TREE


  #####
  # ROBOT mode
  #####

  # Choices are: NOROBOT, GL, REAL

  # Output directory for logs
  if mode == 'gl':
    ROBOT = ActP.GL
    log_path_suffix = '_gl'
    marker_size = 0.015
  elif mode == 'real':
    ROBOT = ActP.REAL
    mode = 'real'
    log_path_suffix = '_real'
    # Need bigger to see on real robot
    marker_size = 0.05
  else:  # mode == 'noRobot'
    ROBOT = ActP.NOROBOT
    log_path_suffix = '_noRobot'
    marker_size = 0.015
  log_path = config_datapath ('custom',
    'active_chord/tree_exec' + log_path_suffix + '/')

  # True for computer with robot (Huitan) to execute_actions(), False else
  #   (Mabel)
  HAS_GL = False


  #####
  # User adjust parameters
  #####

  # Set to False if horizon or n_sims is large. Don't want to take up memory
  #   and time just for one mega huge image!
  SAVE_TREE_PNG = True

  #n_sims = [50]
  #n_sims = [5]  # For rapid testing of whether code runs
  n_sims = [50]


  # Eliminated using NNClassifier in mcts_active_touch.py for node
  #   labels, so that don't have to load nn_refs here. Don't really need it,
  #   just use SVM, most of the time SVM is better...
  #   Can port back in later, if SVM doesn't do well for final classification.


  ################################################# Load probabilities data ##

  # Ground truth
  obj_catname = ''

  # Init nodes
  if ROBOT == ActP.NOROBOT or ROBOT == ActP.GL:

    exec_node = ExecuteActions (n_bins, bin_ranges, mode, nbins_subpath)

    # Load test object into RViz, for plotting
    meta_truth_name = os.path.join (get_active_meta_path (), args.meta_truth)
    truth_dae_name = read_meta_file (meta_truth_name)
    # There should only be ONE line, `.` only one object to recognize, at [0]
    try:
      assert (len (truth_dae_name) == 1)
    except AssertionError:
      print ('%sThere are %d uncommented lines in meta_truth file %s . There should only be ONE! This is the test object. Fix the file and try again. Terminating...%s' % (
        ansi_colors.FAIL, len (truth_dae_name), meta_truth_name,
        ansi_colors.ENDC))
      return

    # Get true object's model path
    train_rootpath = get_root_path ()
    truth_dae_name = os.path.join (train_rootpath, truth_dae_name [0])
    obj_catname, _ = get_meta_cat_name (truth_dae_name)
    # TODO: Don't need to load object on my end for robot, but load it into
    #   RViz so can debug the poses in probabilities .pkl file
    #obj_dims, model_center, model_marker = exec_node.load_object (
    #  truth_dae_name, obj_catname, ROBOT)
    #print ('model_center of mesh in file is at %s' % str(model_center))


  #obj_radii = obj_dims

  probsTrained = ProbReader_Chords (args.meta, mode, n_bins, bin_ranges)

  # Init output log file for prediction results
  if ROBOT != ActP.NOROBOT:

    obj_cats = probsTrained.get_classes ()

    # SVM, NN, histogram intersection
    svm_titles = []
    #nn_titles = []
    hinter_titles = []
    # e.g. svm_prob_sphere, svm_prob_cube, nn_dist_sphere, nn_dist_cube
    for i in range (0, len (obj_cats)):
      svm_titles.append (ActP.LG_SVM_PROB + obj_cats [i])
      #nn_titles.append (ActP.LG_NN_INNER_DIST + obj_cats [i])
      hinter_titles.append (ActP.LG_NN_INTER_DIST + obj_cats [i])
    log_column_titles.extend (svm_titles)
    #log_column_titles.extend (nn_titles)
    log_column_titles.extend (hinter_titles)


  ############################################################## Main loop ##

  INIT_ITER = 1

  # Tests different number of simulations, to see if result is better when run
  #   more simulations.
  #   Especially useful in seeing the 3D hist plots in RViz
  for ni in range (len (n_sims)):

    # User's choice in whether recognition is good enough to stop.
    keep_going = True

    # Will be set in 1st tree search
    incHist = IncrementalHist (n_bins, bin_ranges)
    prev_poses_i = np.zeros ((0,), dtype=int)

    iter_i = INIT_ITER

    # Create log file
    if RUN_MODE == TREE:
      recog_log_name = os.path.join (log_path,
        timestring + ('_tree_%dsims.csv' % n_sims [ni]))
    else:
      recog_log_name = os.path.join (log_path,
        timestring + ('_rand.csv'))
    recog_log_file = open (recog_log_name, 'wb')
    log_writer = csv.DictWriter (recog_log_file,
      fieldnames = log_column_titles, restval='-1')
    log_writer.writeheader ()

 
    # Repeatedly alternate between tree search and robot, using histograms
    #   collected from robot as root histogram at a new tree search.
    while keep_going:
 
      #vis_pub.publish (model_marker)
      time.sleep (0.1)
 
      # Get an initial observation, so we have a source observation k_obs_src
      #   to start from! `.` need z0 to access the probabilities matrix!
      #   For now, just randomly sample an observation for simulation
      # TODO: check for duplicates with previous iterations! Make
      #   get_random_obs_src() take a list of pts to check duplicates.
      # TODO: 14 Jan 2017: In very first tree, can use random, but for all
      #   subsequent trees, use the LAST pose that the robot moved to.
      #   > 18 Feb 2017: Can't. What if last pose doesn't exist in training?
      #   Then can't give it an integer name.
      #   > 22 Feb 2017: Just use the last abs pose from previous tree.
      #   That'll exist.
      if iter_i == INIT_ITER:
        root_obs_i, root_abs_pose_i = probsTrained.get_rand_obs (prev_poses_i)
      else:
        root_obs_i = last_obs_i
        root_abs_pose_i = prev_poses_i [prev_poses_i.size - 1]

      if root_obs_i is None:
        print ('All training poses have been explored. No more tree search to be done.')
        break
 
      params = MCTS_Parameters ()
      params.nSims = n_sims [ni]
      params.horizon = 20
      params.explore = 1
      params.img_dir = os.path.join (params.img_dir, 'mcts_touch')
      if not os.path.exists (params.img_dir):
        os.makedirs (params.img_dir)

      mcTree = MCTS_ActiveChords (params, probsTrained,
        (n_bins, bin_ranges), svm)

      # Visualize root pose
      root_abs_pose = probsTrained.get_pose_all_i (root_abs_pose_i)
      # TODO two lines of code, add it later when have time and if need.
      #   mcTree.visualize_abs_poses() needs to get the actual pose from
      #   rand_pose_i, via:
      #   self.probsTrained.get_pose_all_i (rand_pose_i).
      #   Then it will be a unit vector normal to a cross section plane.
      #   Visualize that plane by calling action_util.py:
      #   from action_util import visualize_plane
      #   visualize_plane(n, vis_pub, ns)
      # Orig code:
      #  mcTree.visualize_abs_poses (np.asarray ([root_abs_pose]),
      #    marker_id_start=prev_poses_i.shape [0], size=marker_size,
      #    show_sqrs=False, show_arrows=False, show_numbers=True)


      # Tree search
      if RUN_MODE == TREE:

        # Run tree search. Pass in incremental histogram from previous
        #   tree searches, to initialize the root.
        # opt_actions is relative actions starting from root_abs_pose
        # opt_abs_poses_num includes root_abs_pose_i
        _, all_rewards, tree_incHist, opt_actions, opt_abs_poses_num, \
          last_obs_i, tree_ttl_secs = mcTree.mcts_mdp (
          root_obs_i, root_abs_pose_i, incHist.get_hist (), 
          prev_poses_i)


        #####
        # Print some info about the tree generated
        #####

        print ('%d tree nodes were created' % len (
          mcTree.get_tree_nodes().keys ()))
        print ('%d counts are in tree_incHist' % (
          tree_incHist.get_total_bin_count ()))
    
        # Visualize tree in pydot and save to file
        if SAVE_TREE_PNG:
          tree_img_name = os.path.join (params.img_dir,
            '%s_tree_vis_nsims%d_iter%d.png' % (timestring, params.nSims,
            iter_i))
          mcTree.tree_vis.write_png (tree_img_name)
          print ('Pydot tree visualization saved to %s' % tree_img_name)
    
        # Plot class predictions for all the simulations
        #   Useful if run thousands of simulations, then printout gets too big
        plt.figure (3)
        plt.plot (range (0, len (mcTree.leaf_classes)), mcTree.leaf_classes)
        plt.xlabel ('Simulation')
        plt.ylabel ('Class prediction at node created')
        plt.title ('Predicted classes in each simulation')
        #plt.show (block=False)
  
        # Plot rewards
        rewards_img_name = os.path.join (params.img_dir,
          '%s_rewards_nsims%d.eps' % (timestring, params.nSims))
        plot_rewards (all_rewards, params, rand_color=False, color_idx=iter_i,
          rewards_img_name=rewards_img_name)
 
      # Baseline. Randomly pick a number of poses, execute in Gazebo, and
      #   see how long it takes to recognize. If takes same time as tree
      #   search, then tree search is useless! Hope is that baseline takes
      #   a lot longer.
      elif RUN_MODE == RANDOM:
        # TODO: Implement this, or compare to depth-1 tree.
        #opt_actions, opt_abs_poses_num, tree_ttl_secs = mcTree.random_poses (
        #  root_abs_pose_i, prev_poses_i)
        print ('%sERROR in active_predict.py: random_poses() not implemented yet!!%s' % (
          ansi_colors.FAIL, ansi_colors.ENDC))
        return

  
      if rospy.is_shutdown ():
        print ('Received Ctrl+C in active_predict.py. Terminating')
        return
 

      #####
      # Execute the opt action sequence on robot
      #####

      # Convert relative poses to current live world poses, starting from
      #   current wrist pose (which should be where you got root_abs_pose
      #   from!)
      # opt_actions should have 1 fewer poses than opt_abs_poses. `.`
      #   root_abs_pose should be the first pose in opt_abs_poses. We don't
      #   need to move to root_abs_pose - robot should already be there 
      #   before tree search started.
      assert (opt_actions.shape [0] == opt_abs_poses_num.shape [0] - 1)
      opt_poses = np.zeros ((opt_actions.shape [0], probsTrained.get_pose_dims ()))
      curr_pose = root_abs_pose
      for i in range (0, opt_actions.shape [0]):
        # pose is unit 3-vector normal to cross section plane
        # action is 4-elt quaternion.
        n2 = calc_result_plane (curr_pose, opt_actions [i, :])
        opt_poses [i, :] = n2
        # Update for next iter
        curr_pose = opt_poses [i, :]

      # TODO 2 lines. See above.
      # Visualize in RViz
      # Orig code:
      # +1 on marker ID, `.` root pose is already visualized above.
      #mcTree.visualize_abs_poses (opt_poses,
      #  marker_id_start=prev_poses_i.shape[0]+1, size=marker_size,
      #  show_sqrs=False, show_arrows=False, show_numbers=True)


      if ROBOT == ActP.GL:
        print ('Executing actions in OpenGL simulation')

        # Convert action sequence to n x 7 numpy array
        # 9 Feb 2017 Temp dummy to test without execute_actions.py being
        #   implemented. TODO Replace with real function when implemented it
        if HAS_GL:
          hist_unnorm, exec_ttl_secs = exec_node.execute_actions (opt_poses)
        # For Mabel's computer which doesn't have the robot to exec actions
        else:
          hist_unnorm = tree_incHist.get_hist ()
        exec_ttl_secs = 0
  
        # Package hist_unnorm into an IncrementalHist object
        exec_incHist = IncrementalHist (n_bins, bin_ranges,
          init_hist=hist_unnorm)
        # Accumulate with previous tree iterations' histogram, like
        #   init_hist=hist_unnorm + incHist.get_hist().
        incHist.add_hist (exec_incHist)

        print ('Accumulated histogram has %d items' % incHist.get_total_bin_count ())

      elif ROBOT == ActP.REAL:
        print ('%sERROR in active_predict.py: ActP.REAL execute_actions() not implemented yet! Returning.%s' % (
          ansi_colors.FAIL, ansi_colors.ENDC))
        return

      elif ROBOT == ActP.NOROBOT:
        if RUN_MODE == TREE:
          # Tree histogram is already accumulated with prev iter's histogram,
          #   which initializes the tree at root. Hists at leaves build on the
          #   root hist. So can directly =, don't need
          #   incHist.add_hist(tree_incHist).
          incHist = tree_incHist

        # For RANDOM mode, there is no incHist from tree. You should always
        #   run RANDOM mode with ROBOT=ActP.REFLEX_GZ, to get triangles from actual
        #   gazebo sampling at the random poses generated. RANDOM with
        #   ROBOT=NOROBOT is not meaningful.
        elif RUN_MODE == RANDOM:
          print ('%sWARN active_predict.py: Did you forget to set ROBOT=ActP.REFLEX_GZ? You are running RANDOM mode. No histograms will be stored if you do not run Gazebo.%s' % (
            ansi_colors.WARNING, ansi_colors.ENDC))
 
      # Append this tree search's poses to previous tree searches' poses
      prev_poses_i = np.append (prev_poses_i, opt_abs_poses_num, axis=0)

      # Save all poses so far. Overwrite any saved in prev iters, `.`
      #   prev_poses is cumulative of all iters. Saving here instead of end
      #   of file in case program ends unexpectedly before reaches end.
      # HIGHEST_PROTOCOL is binary, good performance. 0 is text format,
      #   can use for debugging.
      if RUN_MODE == TREE:
        # Poses actively selected
        poses_log_name = os.path.join (log_path, timestring + \
          '_tree_nsims%d_iter%d_poses.pkl' % (params.nSims, iter_i))

        # Rewards. Save them so can replot the rewards plot if don't like it
        rewards_pkl_name = os.path. join (log_path, timestring + \
          '_tree_nsims%d_iter%d_rewards.pkl' % (params.nSims, iter_i))
        # Save a plain-text version for human inspection
        rewards_csv_name = os.path. join (log_path, timestring + \
          '_tree_nsims%d_iter%d_rewards.csv' % (params.nSims, iter_i))

        # Rewards pkl
        rewards_pkl_file = open (rewards_pkl_name, 'wb')
        pickle.dump (all_rewards, rewards_pkl_file, pickle.HIGHEST_PROTOCOL)
        rewards_pkl_file.close ()

        # Rewards csv
        with open (rewards_csv_name, 'wb') as f:
          rewards_csv_writer = csv.writer (f)
          for i in range (0, all_rewards.shape [0]):
            # writerow() takes a list. Just give it a 1-elt list
            rewards_csv_writer.writerow ([all_rewards [i]])

      else:
        poses_log_name = os.path.join (log_path, timestring + \
          '_rand_iter%d_poses.pkl' % (iter_i))
      # Poses actively selected
      poses_log_file = open (poses_log_name, 'wb')
      prev_poses = probsTrained.get_pose_all_i (opt_abs_poses_num)
      pickle.dump (prev_poses, poses_log_file, pickle.HIGHEST_PROTOCOL)
      poses_log_file.close ()

 
      ###################################################### Plot results ##

      # TODO: obj_base and obj_cat don't exist yet. The histogram is saved
      #   as timestamped file in root of csv_* folders by execute_actions.py,
      #   or wherever the new execute_actions.py decides to store it. Can
      #   simply use that, pass in obj_base = timestamp.csv name, obj_cat =
      #   '' (hopefully HistogramFromFile just sees that as skipping a
      #   directory. OR, can implement a new child class in
      #   histogram_of_chords.py that just takes a histogram from
      #   incHist.get_normalized_hist(), and visualize that.
      '''
      obj_base = 
      obj_cat = 
      staticHist = HistogramFromFile (obj_base, obj_cat, mode,
        nbins_subpath, chord_consts, hist_config)

      staticHist.visualize_hist_patches () 
      '''


      # For output log file
      row = dict ()

      idx_pred_svm = svm.svm_predict (incHist.get_as_feat ())
      lbl_pred_svm = svm.classifier_lbls [idx_pred_svm]
      print ('Predicted class by SVM: %s' % (lbl_pred_svm))

      print ('              SVM probs')

      # probs is nSamples x nClasses
      probs_pred_svm, probs_lbls = svm.svm_predict_prob (
        incHist.get_as_feat ())

      # shape[1] to loop over the number of classes
      for i in range (0, probs_pred_svm.shape [1]):
        obj_cat = svm.classifier_lbls [probs_lbls[i]]

        svm_prob = probs_pred_svm[0, i]
        row [ActP.LG_SVM_PROB + obj_cat] = '%.4f' % svm_prob

        # Take 0th row of probs, `.` there's only 1 sample
        print ('%12s: %f' % (obj_cat, svm_prob))

      print ('  (SVM probs are meaningless for very small datasets):')


      # Write prediction results to log file

      # Log for both RANDOM mode and TREE mode, for comparison to random
      #   baseline.
      # Don't log for NOROBOT, `.` recognition accuracy is not meaningful
      #   without actual observations.
      if ROBOT != ActP.NOROBOT:
        if ROBOT == ActP.GL:
          row [ActP.LG_ROBOT] = 'GL'
        elif ROBOT == ActP.REAL:
          row [ActP.LG_ROBOT] = 'REAL'
        row [ActP.LG_ITER] = iter_i

        # Tree
        # Can add params.explore too, and lambda (make it part of params struct
        #   first. But I don't think I need it. I don't want to make the lines
        #   in the csv too long, I still need them to be human-readable for 
        #   checking!
        if RUN_MODE == TREE:
          row [ActP.LG_NSIMS] = params.nSims
          row [ActP.LG_HORIZON] = params.horizon
          row [ActP.LG_DEPTH] = mcTree.depth_reached
        row [ActP.LG_TREETIME] = '%.3f' % tree_ttl_secs
        row [ActP.LG_TREETIME_AVG] = '%.3f' % (tree_ttl_secs / params.nSims)

        # Execution
        # +1 to include root pose
        row [ActP.LG_POSES] = opt_actions.shape [0] + 1
        # TODO: exec_node not implemented yet.
        #row [ActP.LG_TIMESTRING] = exec_node.timestring
        row [ActP.LG_EXECTIME] = '%.3f' % exec_ttl_secs

        row [ActP.LG_TRUTH] = obj_catname
        row [ActP.LG_SVM_PRED] = lbl_pred_svm

        #row [ActP.LG_PLANNED] = n_plan_success

        # I decided not to write log file if not running Gazebo. No real
        #   results from tree alone anyway!
        log_writer.writerow (row)


        # Take user input to go to next iteration of tree search + Gazebo
        uinput = ''
        while uinput != 'y' and uinput != 'q':
          uinput = raw_input ('Press y to do another round of tree search and Gazebo execution. Press q to end: ')
          if uinput == 'q':
            keep_going = False
            print ('Ending iterations...')
          elif uinput == 'y':
            keep_going = True
            iter_i += 1
            print ('Continuing for another round...')

      # end if ROBOT != ActP.NOROBOT:
 
    # end while keep_going
 
 
    # TODO: exec_node not implemented yet
    #if ROBOT != ActP.NOROBOT:
    #  exec_node.close_output_files ()

    if (RUN_MODE == TREE or RUN_MODE == RANDOM) and \
       (ROBOT != ActP.NOROBOT):
      print ('%sTree prediction results saved to to %s%s' % (
        ansi_colors.OKCYAN, recog_log_name, ansi_colors.ENDC))
    if recog_log_file:
      recog_log_file.close ()


    # Get user key press to advance to next iteration
    if ni + 1 < len (n_sims):
      uinput = raw_input ('Press a key to continue to the next round of (%d) simulations, or q to quit: ' % (n_sims [ni+1]))
      if uinput == 'q':
        break

  # end for n_sims
 
  return


if __name__ == '__main__':
  main ()

