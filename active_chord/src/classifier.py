#!/usr/bin/env python

# Mabel Zhang
# 11 Jan 2016
#
# Copied and modified from active_touch prob_of_observs.py used for triangles.
#


# Python
import pickle

# NumPy
import numpy as np
from sklearn.externals import joblib

# My packages
from util_continuum.ansi_colors import ansi_colors
from chord_recognition.hist_util import find_bin_edges_and_volume, \
  linearize_hist
from chord_recognition.histogram_of_chords import HistogramConfig, \
  HistogramFromFile
from chord_recognition.chord_consts import ChordConsts
from chord_recognition.metrics import l2_dist, hist_inter_dist, chisqr_dist, \
  inner_prod_dist
from chord_recognition.recognize_consts import SVMConsts
from util_continuum.parse_meta import get_meta_cat_name



class SVMClassifier:

  def __init__ (self, svm_name, svm_lbls_name):

    self.svm_name = svm_name
    self.svm_lbls_name = svm_lbls_name

    print ('%sLoading SVM model from %s%s' % (
      ansi_colors.OKCYAN, self.svm_name, ansi_colors.ENDC))
    # Loaded as a sklearn svm.SVC object, saved by triangle_sampling
    #   triangles_svm.py
    # Ref joblib: http://stackoverflow.com/questions/10592605/save-classifier-to-disk-in-scikit-learn
    self.svm_dict = joblib.load (self.svm_name)

    self.classifier_model = self.svm_dict [SVMConsts.JOBLIB_SVM]
    self.do_PCA = self.svm_dict [SVMConsts.JOBLIB_PCA_BOOL]
    if self.do_PCA:
      self.pca = self.svm_dict [SVMConsts.JOBLIB_PCA]

    print ('%sLoading SVM labels from %s%s' % (
      ansi_colors.OKCYAN, self.svm_lbls_name, ansi_colors.ENDC))
    with open (self.svm_lbls_name, 'rb') as f:
      # Loaded as a dictionary, saved by triangle_sampling triangles_svm.py
      self.classifier_lbls = pickle.load (f)


  def apply_pca (self, feat_raw):

    # PCA. This mirrors chord_recognition recognize_svm.py, where SVM is trained
    if self.do_PCA:
      feat = self.pca.transform (feat_raw)
    else:
      feat = feat_raw

    return feat


  # Feeds current histogram into trained SVM model, to get a class prediction.
  # Returns scalar integer label of the predicted class
  def svm_predict (self, feat_raw):

    feat = self.apply_pca (feat_raw)

    # Run SVM on the linearized histogram
    lbl_pred = self.classifier_model.predict (feat)

    # Convert the 1 x 1 array to a scalar int
    # Make sure there's only 1 elt in the array first
    assert (lbl_pred.shape == (1,))
    lbl_pred = int (lbl_pred)

    #print ('Predicted class: %s' % (self.classifier_lbls [lbl_pred]))

    return lbl_pred


  def svm_predict_prob (self, feat_raw):

    feat = self.apply_pca (feat_raw)

    # Note SVM model needs to be trained (fit()) with probability attribute
    #   set to True.
    # Ref: http://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html
    probs = self.classifier_model.predict_proba (feat)

    #print ('Classes in SVM model:')
    #print (self.classifier_model.classes_)

    # probs is nSamples x nClasses, 2D numpy array
    return probs, self.classifier_model.classes_


class NNClassifier:

  # Parameters:
  #   refs: Dictionary, one file per class, 
  #     {'obj category': 'full_hist_csv_file_path'}
  #     If you have more than 1 object per class, only pass in one. Otherwise
  #     you need to change some code to take care of multiple objs per class.
  def __init__ (self, refs, mode, nbins_subpath):

    consts = ChordConsts ()
    config = HistogramConfig (mode, nbins_subpath, consts)
    nbins = config.get_n_bins ()
    bin_ranges = config.get_bin_ranges ()

    # Ret vals are lists of 3 1D numpy vectors
    _, _, self.bin_volume = find_bin_edges_and_volume (nbins, bin_ranges)

    # Reference objects
    self.ref_hists = []
    self.ref_lbls = []
    self.ref_dict = {}
    for lbl in refs.keys ():
      obj_cat, obj_base = get_meta_cat_name (refs [lbl])
      hist = HistogramFromFile (obj_base, obj_cat, mode, nbins_subpath,
        consts, config)

      ref_hist_linear, _ = hist.read_hist_file ()
      self.ref_hists.append (ref_hist_linear)
      self.ref_lbls.append (lbl)

      # Make a dictionary for string label reference. Value is pointer, don't
      #   copy the whole array, waste of space.
      self.ref_dict [lbl] = self.ref_hists [len (self.ref_hists) - 1]


  # Find reference object that has closest distance to given object feature
  #   vector. Return the closest object's name.
  def nn_predict (self, feat, dtype='inner'):

    dists = np.zeros ((len (self.ref_hists),))

    # Output the reference object that has a smaller distance to the given
    #   feature vector
    for i in range (0, len (self.ref_hists)):

      feat2 = self.ref_hists [i]
      dists [i] = get_nn_dist (feat, feat2, dtype, self.bin_volume)

    min_idx = np.argmin (dists)
    return self.ref_lbls [min_idx]


  # Parameters:
  #   hist1: Normalized histogram. Use incHist.get_as_feat()
  #   lbl2: String labels of two objects you'd like to get the distance btw.
  #     e.g. 'sphere'
  def get_nn_dist (self, hist1, lbl2, dtype='inner'):

    hist2 = self.ref_dict [lbl2]
    return get_nn_dist (hist1, hist2, dtype, self.bin_volume)



# Helper fn
# Parameters:
#   one_histdd, another_histdd: Normalized histograms
#   dtype: Type of distance, 'l2', 'chisqr', or 'inner' product
#     I found inner product the best, `.` the distance is in range [0, 1],
#     easier to intuitionized. It's computed as
#     dot product / (|A||B|), which gives cos(theta), which is btw 0 and 1.
#   bin_volume: If using 'hist_inter', and want the distance to be normalized
#     to [0, 1], then pass in the actual bin volume, which is the product of
#     all bin widths.
def get_nn_dist (one_histdd, another_histdd, dtype='inner', bin_volume=1.0):

  if dtype == 'l2':
    dist = l2_dist (one_histdd, another_histdd)

  elif dtype == 'hist_inter':
    dist = hist_inter_dist (one_histdd, another_histdd)

    # Normalize to [0, -1], easier metric for intuition to put in paper.
    #   Value will be negative, for NN, `.` bigger intersection means
    #   closer distance! So we flip the sign.
    dist *= (float (bin_volume))

  elif dtype == 'chisqr':
    dist = chisqr_dist (one_histdd, another_histdd)

  elif dtype == 'inner':
    dist = inner_prod_dist (one_histdd, another_histdd)

  else:
    print ('%sUnrecognized distance type. Will use default (L2).%s' % (
      ansi_colors.WARNING, ansi_colors.ENDC))

    # Default to L2
    dist = l2_dist (one_histdd, another_histdd)

  return dist


