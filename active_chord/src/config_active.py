#!/usr/bin/env python

# Mabel Zhang
# 7 Sep 2016
#
# Constants used by active_predict.py and other files
#

class ActiveParams:

  #####
  # ROBOT_MODE
  #####

  # For lightweight testing, which runs tree search only and
  #   nothing executed on robot.
  NOROBOT = 0

  GL = 1

  REAL = 2


  #####
  # Recognition log _tree.csv / _rand.csv CSV column titles
  #####

  LG_ROBOT = 'robot'

  # Tree
  LG_ITER = 'iter'
  LG_NSIMS = 'tree_n_sims'
  LG_HORIZON = 'tree_horizon'
  LG_DEPTH = 'tree_depth_reached'
  LG_TREETIME = 'tree_ttl_secs'
  LG_TREETIME_AVG = 'tree_avg_secs'

  # Execution and triangles obtained
  LG_POSES = 'n_poses'
  LG_TOUCHES = 'n_contacts'
  LG_TRIS = 'n_cumu_tris'
  # This is timestring for hist and triangles, useful for retrieving hist
  #   files to plot side-by-side with ground truth hists, for paper figure!
  LG_TIMESTRING = 'tri_timestring'
  LG_EXECTIME = 'exec_ttl_secs'

  # Recognition
  LG_TRUTH = 'truth'
  LG_SVM_PRED = 'svm_pred'
  LG_NN_INNER_PRED = 'nn_inner_pred'
  LG_NN_INTER_PRED = 'nn_histInter_pred'

  LG_SVM_PROB = 'svm_prob_'
  LG_NN_INNER_DIST = 'nn_inner_'
  LG_NN_INTER_DIST = 'nn_histInter_'

  # How many poses were successfully planned with MoveIt, when ROBOT==BAXTER
  LG_PLANNED = 'n_plan_success'


