#!/usr/bin/env python

# Mabel Zhang
# 30 Jan 2017
#
# Interface between active system (entry point active_predict.py) and robot
#   execution ROS node outside this package.
#

# ROS
from visualization_msgs.msg import Marker
import rospy
from geometry_msgs.msg import Vector3
from continuum_grasp.srv import *

# Python
import sys
import time
import numpy as np

# My packages
from chord_recognition.chord_consts import ChordConsts
from chord_recognition.histogram_of_chords import HistogramFromChord
#from chord_recognition.chord_reader import ChordReader


class ExecuteActions:

  def __init__ (self, n_bins, bin_ranges, mode, nbins_subpath):

    self.n_bins = n_bins
    self.bin_ranges = bin_ranges

    self.mode = mode
    self.nbins_subpath = nbins_subpath

    self.consts = ChordConsts ()


  # TODO Dummy fn
  # Load object into RViz for plotting
  #def load_object (self, dae_name, obj_cat, mode):
  #
  #  # Dummies so that active_predict.py can run
  #  obj_dims = [0.5, 0.5, 0.5]
  #  model_center = [0, 0, 0]
  #  model_marker = Marker ()
  #
  #  return obj_dims, model_center, model_marker


  def execute_actions (self, actions):
    start_time = time.time ()

    if actions.shape[0] == 0:
      print "empty action!"
      elapsed_time = time.time () - start_time
      return None, elapsed_time
    else:
      ind = 0
      finish = False
      chord_path = ''
      for row in actions:
        pln_nrm = Vector3()
        pln_nrm.x = row[0]
        pln_nrm.y = row[1]
        pln_nrm.z = row[2]
        print "next plane normal is:"
        print pln_nrm

        if ind == actions.shape[0]-1:
          finish = True

        chord_path = arm_wrap_by_plane_client(pln_nrm, finish)
        # To give enough time to finish wrap in openGL
        # can be automated without using hard time thresholds
        time.sleep(20.0)
        ind += 1

      time.sleep(5.0)
      if chord_path == 'null':
        print "no chords data returned!"
        elapsed_time = time.time () - start_time
        return None, elapsed_time
      else:
        # Load chords from file
        hist_node = HistogramFromChord ('', '', self.mode, self.nbins_subpath,
                                        bin_ranges=self.bin_ranges,
                                        n_bins_l=self.n_bins[0], 
                                        n_bins_ang=self.n_bins[1:3],
                                        consts=self.consts,
                                        chord_name=chord_path)
        hist_unnorm = hist_node.get_histdd_unnormed ()

        elapsed_time = time.time () - start_time
        return hist_unnorm, elapsed_time


def arm_wrap_by_plane_client(pln_nrm, finish):
  rospy.wait_for_service('continuum_grasp/wrap_by_plane')
  try:
      arm_wrap = rospy.ServiceProxy('continuum_grasp/wrap_by_plane', wrapByPlane)
      response = arm_wrap(pln_nrm, finish)
      if response.success:
        print "arm wrap by plane is successful"
        print "chord path is:"
        print response.chord_path
      return response.chord_path
  except rospy.ServiceException, e:
      print "Service call failed: %s"%e

if __name__ == '__main__':
  # cutting plane normal
  pln_nrm = Vector3()
  pln_nrm.x = 0.
  pln_nrm.y = 0.2
  pln_nrm.z = -0.98
  finish = False

  arm_wrap_by_plane_client(pln_nrm, finish)

  time.sleep(20.0) # to give enough time to execute
  
  pln_nrm.x = 0.
  pln_nrm.y = 0.
  pln_nrm.z = 1.
  finish = True

  arm_wrap_by_plane_client(pln_nrm, finish)



