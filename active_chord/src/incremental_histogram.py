#!/usr/bin/env python

# Mabel Zhang
# 11 Jan 2016
#
# Copied from active_touch prob_of_observs.py used for triangles.
#   Refactored to individual file instead of being in prob_reader.py.
#

from copy import deepcopy

import numpy as np

# My packages
from chord_recognition.hist_util import find_bin_edges_and_volume, \
  normalize_dd_hist_given_edges, linearize_hist
from util_continuum.ansi_colors import ansi_colors


# A d-dimensional histogram incrementally built from inputs
class IncrementalHist:

  # Set a small threshold, so that triangles that were rounded during Gazebo
  #   training in io_probs.py can still find a bin, if they fall on the
  #   bin range min/max and were rounded outside the range!
  #   This number depends on how many decimal places sample_gazebo.py passed
  #   as DISCRETIZE_TRI to IOProbs in io_probs.py, e.g. for 2 decimal
  #   places, use 0.01.
  # Don't set this too high. e.g. setting to 0.02 when rounding is for 2 
  #   decimal places, immediately makes the NN distance more ambiguous btw
  #   cubes and spheres, `.` many triangles now fall into the wrong bins!!
  #   Can only do 0.01!
  BIN_RANGE_THRESH = 0.01


  # Parameters:
  #   nbins: d-tuple, d = number of dimensions of histogram, values in the
  #     tuples are number of bins in the histogram
  #   bin_ranges: d x 2 NumPy array. d = number of dimensions of histogram.
  #     ((min1, max1), (min2, max2), ..., (min_d, max_d)).
  #   init_hist: An unnormalized histogram. If not None, then this is used as
  #     a copy constructor! This is handy for creating cumulative histograms
  #     while traversing from tree root downwards. Each child node's histogram
  #     is a copy of a parent node's histogram, with one more observation added.
  def __init__ (self, nbins, bin_ranges, init_hist=None):

    self.nbins = nbins
    self.bin_ranges = bin_ranges

    # l x m x n 3D NumPy array
    # Integer tallies. Don't normalize. Easier to work with ints.
    #   If caller wants a normalized hist, call get_normalized_hist().
    if init_hist is None:
      self.hist = np.zeros (nbins)
    else:
      self.hist = deepcopy (init_hist)

    self.bin_edges, self.bin_centers, self.bin_volume = \
      find_bin_edges_and_volume (nbins, bin_ranges)


  def get_nbins (self):
    return self.nbins

  def get_bin_ranges (self):
    return self.bin_ranges


  def get_hist (self):
    return self.hist

  def get_normalized_hist (self):

    return normalize_dd_hist_given_edges (self.hist, self.bin_edges,
      self.bin_volume)

  # Return total number of unnormalized integer tallies in histogram
  def get_total_bin_count (self):
    return np.sum (self.hist)


  # Get linearized histogram, to use as feature vector to a classifier
  def get_as_feat (self):

    # Normalize histogram before feeding to SVM!! Don't forget this, result
    #   will be very wrong. Our SVM is trained on normalized histograms.
    hist_norm = self.get_normalized_hist ()
    hist_linear = linearize_hist (hist_norm)

    # Make the single histogram into 2D array, `.` sklearn svm doesn't like
    #   1D arrays.
    hist_linear = np.array ([hist_linear])

    return hist_linear


  # Same as get_as_feat(), but static method, lets user pass in everything.
  #   Used when user calls add_bin_count(modify_hist=False), which returns an
  #   unnormalized histogram matrix. Then if caller wants to run SVM on it,
  #   need to normalize it and linearize it first.
  @staticmethod
  def get_feat_from_hist (hist, bin_edges, bin_volume):

    hist_norm = normalize_dd_hist_given_edges (hist, bin_edges, bin_volume)
    hist_linear = linearize_hist (hist_norm)

    hist_linear = np.array ([hist_linear])
    return hist_linear


  # Add the data_pt to the histogram. Does this by finding out which bin the
  #   data_pt falls in, then adding 1 to the bin count of that bin.
  #   Histogram is then re-normalized.
  # Parameters:
  #   data_pts: nPts x nDims NumPy 2D array. If you have only one point, reshape
  #     it to 2D, shape (1, nDims), not just (nDims, ).
  #   modify_hists: If True, changes member var, returns (bool, None).
  #     Else, do not change the member var, return the modified hist in
  #     (bool, new_hist).
  #   norm_modified: Only looked at if modify_hist=False. If this param is
  #     True, normalize and reshape the new hist into 1D to return. This format
  #     lets user pass the new histogram directly into svm_predict() fn below.
  def add_bin_count (self, data_pts, modify_hist=True, norm_modified=True):

    # Sanity check: Verify that the data_pts has same number of dimensions as
    #   the histogram!
    assert (data_pts.shape [1] == len (self.bin_edges))

    # Init ret val
    new_hist = None
    if not modify_hist:
      new_hist = deepcopy (self.hist)

    nPts = data_pts.shape [0]
    nDims = data_pts.shape [1]

    found_bin_fit, bin_idxes, _ = self.find_bin_fit (data_pts,
      self.bin_edges, self.bin_centers, self.BIN_RANGE_THRESH)
    for p_i in range (0, nPts):
      # Add integer tally to bin count
      # Must use tuple to access, a list doesn't give you the right outcome!
      #   Indexing with a list would change all elements in the slice, not just
      #   one!
      if found_bin_fit [p_i]:
        if modify_hist:
          self.hist [tuple (bin_idxes [p_i, :])] += 1
        else:
          new_hist [tuple (bin_idxes [p_i, :])] += 1


    #print (self.hist)

    # Normalize and reshape new hist to 1D, to return. This format lets user
    #   pass the new histogram directly into svm_predict() fn below.
    if not modify_hist and norm_modified:
      new_hist = IncrementalHist.get_feat_from_hist (new_hist, self.bin_edges,
        self.bin_volume)

    return np.all (found_bin_fit), new_hist


  # Increment a bin in the histogram by 1, given the bin index directly.
  # Parameters:
  #   bin_idx: n x 3 2D numpy array. Each row is 3D index of bin to add 1
  #     count to.
  def add_bin_count_by_idx (self, bin_idx):

    assert (bin_idx.shape [1] == len (self.bin_edges))

    try:
      self.hist [tuple (bin_idx)] += 1
      return True
    except IndexError:
      return False


  # Return Boolean to indicate success.
  def add_hist (self, that_incHist):

    # Make sure number of bins match btw the two histograms
    if not np.all (np.asarray (self.nbins) - np.asarray (that_incHist.get_nbins ()) < 1e-6):
      print ('%sincremental_histogram.py add_hist(): nbins of the two histograms do not match! Cannot add. nbins:%s' % (
        ansi_colors.WARNING, ansi_colors.ENDC))
      print (self.nbins)
      print (that_incHist.get_nbins ())
      return False

    # Make sure bin ranges match btw the two histograms
    if not np.all (self.bin_ranges - that_incHist.get_bin_ranges () < 1e-6):
      print ('%sincremental_histogram.py add_hist(): bin_ranges of the two histograms do not match! Cannot add. bin_ranges:%s' % (
        ansi_colors.WARNING, ansi_colors.ENDC))
      print (self.bin_ranges)
      print (that_incHist.get_bin_ranges ())
      return False

    self.hist += that_incHist.get_hist ()

    return True


  # Parameters:
  #   data_pts: nPts x nDims NumPy array
  #   bin_edges: List of nDims 1D numpy arrays, each of size nEdges.
  #     Access with bin_edges[dim_i][edge_j].
  @staticmethod
  def find_bin_fit (data_pts, bin_edges, bin_centers, BIN_RANGE_THRESH,
    return_tuples=True):

    # Sanity check
    # nDims x nEdges
    bin_edges_np = np.array (bin_edges)
    if bin_edges_np.shape [1] > 1:
      # ndims 1D numpy array
      bin_width = bin_edges_np [:, 1] - bin_edges_np [:, 0]

      if np.any (BIN_RANGE_THRESH > bin_width):
        print ('%sWARN: incremental_histogram.py find_bin_fit(): BIN_RANGE_THRESH (%d) is greater than bin_width! You probably don''t mean to do this. This will result in some raw data falling into the wrong bins, because your allowance for the threshold around bins is too big! You should decrease BIN_RANGE_THRESH.%s' % (
          ansi_colors.WARNING, BIN_RANGE_THRESH, ansi_colors.ENDC))
        print ('bin_width:')
        print (bin_width)


    # nPts x 3
    # Init to -1, so can easily compare whether a bin was found for a data pt.
    #   A value in the row will be -1 if not found.
    bin_idxes = np.zeros (data_pts.shape) - 1
    found_bin_centers = np.zeros (data_pts.shape)

    nDims = data_pts.shape [1]

    # Find out which bin the data pt falls in. This can be done with a simple
    #   for-loop through the bin edges, comparing <= and >= to data_pt.
    # 2 Dec 2016: Faster way is to simply do a subtraction, for each
    #   dimension, make an array of the bin edges in that direction. Then
    #   do edges array minus the data point. Since bin edges are all in order,
    #   the bin that that data point falls in must have 2 edges whose
    #   subtraction by the data point is the minimum!!
    #   E.g. for ith data point, in dimension d_i:
    #   dif = np.array (bin_edges [d_i] [:]) - data_pt [i, d_i]
    #   min_idxes = dif.argsort () [:2]
    #   # Get the smaller one, `.` either left or right edge could be closer to
    #   #   the data point. Want the edge on the left, to get the bin number.
    #   bin_idx = np.min (min_idxes)
    #   Ref: http://stackoverflow.com/questions/16817948/i-have-need-the-n-minimum-index-values-in-a-numpy-array
    # d for dimension. 3 for 3D histograms.
    for d_i in range (0, nDims):
 
      # Loop through each bin edge.
      # Be careful with indexing: there are n+1 edges for n bins.
      #   Bin [0] is btw edges [0] and [1], bin [n-1] is btw edges [n-1] and
      #     [n].
      #   So bin_idx should be the upper-bound edge index.
      #   So start with e_i = 0 to access edges [0] and [1], then bin_idx = e_i.
      # Add a small BIN_RANGE_THRESH, so that rounded triangles on the edge of
      #   min/max bin edges s can still be grouped into the bin at the end.
      for e_i in range (0, len (bin_edges [d_i])-1):

        '''
        # To test on command line:
# Find bins for two data points, 5 dimensions
bin_idxes = np.zeros ((2, 5))
# A specific bin (bin 45)'s edges
e_i = 45
bin_edge1 = 2
bin_edge2 = 4

# A specific dimension of the two data points, like data_pts[:,d_i]
d_i = 0
data_pt = np.array([2, 3])

leq = bin_edge1 <= data
geq = bin_edge2 > data
found_data_idx = np.logical_and (leq, geq)
bin_idxes [found_data_idx, d_i] = e_i

        '''

        # bin_edges [d_i] [e_i] is a scalar
        # Left bin inclusive, right bin exclusive. Can't both be inclusive! `.`
        #   then there is ambiguity `.` a data point that equals the edge value
        #   can fall into two bins, one on left and one on right of edge!
        leq = (bin_edges [d_i] [e_i] - BIN_RANGE_THRESH <= data_pts [:, d_i])
        geq = (bin_edges [d_i] [e_i+1] + BIN_RANGE_THRESH > data_pts [:, d_i])

        # Data points that found a bin
        found_data_idx = np.logical_and (leq, geq)

        # Set the bin index, so it's no longer -1
        bin_idxes [found_data_idx, d_i] = e_i
        # Save the found bin center to ret val
        found_bin_centers [found_data_idx, d_i] = bin_centers [d_i] [e_i]
 

      # end for e_i

    # end for d_i = 0 : nDims
 
    # np.where(2d array) returns a tuple of 2 NumPy arrays of the same size,
    #   one for col idx, one for row idx. Just look at the size of one of them.
    not_found_idxes = np.where (bin_idxes == -1)
    not_found_rows = np.unique (not_found_idxes [0])
    if not_found_idxes [0].size > 0:
      print ('%s%d dimensions total, for %d out of %d points did not find a histogram bin. These points will be ignored%s' % (
        ansi_colors.FAIL, not_found_idxes [0].size, not_found_rows.size,
        data_pts.shape[0], ansi_colors.ENDC))
      print ('  data_pts: %s' % str(data_pts [not_found_rows, :]))
      print ('  bin_idxes: %s' % str(bin_idxes [not_found_rows, :]))
      print ('  bin range (min, max): '),
      for d in range (0, nDims):
        print ('(%.2f, %.2f) ' % (np.min(bin_edges[d]), np.max(bin_edges[d]))),
      print ('')

    # Collapse by seeing if a row has any -1's. If so, then the 3D data point
    #   on that row did not find a bin fit.
    # nPts-element 1D array
    found_bin_fit = np.all (bin_idxes != -1, axis=1)

    if return_tuples:
      # Return a tuple instead of list, so it can be used for key of dictionary
      return found_bin_fit, bin_idxes, map (tuple, found_bin_centers)
    else:
      # found_bin_centers: nPts x nBins NumPy array
      return found_bin_fit, bin_idxes, found_bin_centers
 


