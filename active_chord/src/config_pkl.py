#!/usr/bin/env python

# Mabel Zhang
# 7 Jan 2016
#
# Define constants for .pkl files that store active touch training.
#
# Used by io_probs.py, which is used by sample_gazebo.py.
#


class ActivePKLConsts:

  # Dictionary key constants

  # Meta info
  OBJ_BASE = 'obj_base'

  # Params
  DISC_POSE = 'disc_pose'
  DISC_CHORD = 'disc_chord'

  # Data
  CHORDS = 'chords'
  OBSERVS = 'observs'
  OBS_TALLIES = 'obs_tallies'
  ABS_POSES = 'abs_poses'
  POSE_OBSL = 'pose_obsl'

