#!/usr/bin/env python

# Mabel Zhang
# 10 Jan 2017
#
# Refactored from prob_writer.py and prob_reader.py
#

from copy import deepcopy

import numpy as np

# My packages
from util_continuum.mat_util import find_dups_row, find_dups_multirow_onelist, \
  find_dups_self
from util_continuum.ansi_colors import ansi_colors

# Local
from incremental_histogram import IncrementalHist


DEBUG_DUPS = False
DEBUG_DISCRETIZE = False

# Parent class.
# Observations trained on an entire object.
# A list of observations (as opposed to a single observation). This allows each
#   action in the MCTS to produce more than a single observation, e.g. a set
#   of triangles, a set of chords, as opposed to a single triangle or a single
#   chord.
# Child class needs to implement:
#   add_single_obs_array ()
#   check_dup_obsl ()
#   add_nondup_obsl ()
#   check_dup_and_add_obsl ()
#   merge_with_instance ()
#   discretize_to_hist_centers ()
class ObservationList:

  # Parameters:
  #   dim: dimension of a single observation
  def __init__ (self, dim):

    # |C| x dim. Indices are integer names for observations
    # Each row is an observation, e.g. a chord, a triangle
    self.single_obs = np.zeros ((0, dim))

    # List of list of observations. Inner list is NumPy array.
    # |Z| x 1. Integer names for observations. Indices are integer names.
    #   List of 1D Numpy array of chord indices.
    #   List of chords may be different sizes, .`. can't use numpy 2D array,
    #     need a Python list.
    #     Ref: http://stackoverflow.com/questions/3386259/how-to-make-a-multidimension-numpy-array-with-a-varying-row-size
    self.obsl_list = []
    # 1D NumPy array. Tally of each observation list
    # |Z| x 1. Row i contains tallies of how many times observation i was seen
    self.obsl_tallies = np.zeros (0)


  # NOTE: If you add fields to this object class, then add fields here!
  def reset_vars (self, single_obs, obsl_list, obsl_tallies):

    self.single_obs = single_obs
    self.obsl_list = obsl_list
    self.obsl_tallies = obsl_tallies


  def get_n_single_obs (self):
    return self.single_obs.shape [0]

  def get_n_obsl (self):
    return len (self.obsl_list)

  # Returns nObs x nDims numpy array
  #   row_idx: integer or list of nObs integers
  def get_single_obs_i (self, row_idx):
    if row_idx is not None:
      return self.single_obs [row_idx, :]
    else:
      return None

  def get_obsl_i (self, i):
    if i is not None:
      return self.obsl_list [i]
    else:
      return None


  def print_obsl_list (self):
    print ('All observations (%d) so far: ' % len (self.obsl_list))
    print (self.obsl_list)


  # Returns the index of the given obsl in obsl_list
  def reverse_lookup_obsl (self, obsl):
    #print (obsl)
    #print (self.obsl_list)
    #idx = self.obsl_list.index (obsl)

    # Return first item in list that matches obsl
    for i in range (len (self.obsl_list)):
      if np.all (np.abs (self.obsl_list [i] - obsl) < 1e-6):
        return i

    # If haven't returned at this point, then obsl was not found in list!!
    print ('%sERROR in observation_list.py reverse_lookup_obsl(): obsl not found in obsl_list. Something is wrong. Track it down in code and fix bug. Returning -1.%s' % (
      ansi_colors.FAIL, ansi_colors.ENDC))
    return -1

    #print ('idx:')
    #print (idx)
    return idx

    # Just let program crash so I can see trace back
    '''
    try:
      return self.obsl_list.index (obsl)
    except ValueError, e:
      print ('%sERROR in observation_list.py reverse_lookup_obsl(): obsl not found in obsl_list. Something is wrong. Track it down in code and fix bug. Returning -1.%s' % (
        ansi_colors.FAIL, ansi_colors.ENDC))
      print (str (e))
      return -1
    '''


class ObservationList_Chords (ObservationList):

  # ================================================= single_obs operations ==

  # Checks duplicates first. Adds nonduplicate single observations.
  # Parameters:
  #   those_single_obs: n x dim NumPy array
  def add_single_obs_array (self, incoming_singles):

    n_incoming_singles = incoming_singles.shape [0]

    # Record number of original chords, BEFORE appending new ones
    n_orig_singles = self.single_obs.shape [0]

    # n x 1
    # Find index of all incoming chords, whether dup or not.
    # Index indexes chords; value indexes self.chords.
    singles_idx = np.zeros (n_incoming_singles)

    # Allocate extra for speed
    # (<= n) x 1. Value indexes incoming chords. Not all chords may be added!
    #   Some may be dups, then they are not included in this array.
    incoming_singles_idx = np.zeros (n_incoming_singles, dtype=int)
    n_nondup_singles = 0


    # Find duplicates in incoming chords, against existing chords
    incoming_existing_dup_idxes = find_dups_multirow_onelist (incoming_singles,
      self.single_obs)
    #print ('incoming_existing_dup_idxes:')
    #print (incoming_existing_dup_idxes)

    # Find duplicates within incoming chords, e.g. two incoming chords are same
    # If there are duplicates, then only the unique chords should be added.
    #   Second, the chord number should be assigned correctly, skipping over
    #   the duplicate ones - don't give the duplicates an integer name.
    # Indexes chords
    incoming_self_dup_idxes = find_dups_self (incoming_singles)
    #print ('incoming_self_dup_idxes:')
    #print (incoming_self_dup_idxes)

    # Look at duplicate indices of each incoming chord in self.single_obs.
    #   Mark which incoming chords are non-duplicate, i.e. to be added to
    #   self.single_obs.
    # Input: incoming_existing_dup_idxes, n_orig_singles
    # Output: singles_idx, incoming_singles_idx, n_nondup_singles
    # i indexes chords
    for i in range (0, incoming_existing_dup_idxes.size):

      # No duplicate, this is a new chord
      if incoming_existing_dup_idxes [i] == -1 and \
        incoming_self_dup_idxes [i] == -1:
        # Offset the index by number of chords already in self.single_obs, so
        #   that can index it after the incoming non-dup chords are added to
        #   self.single_obs. Index indexes chords; value indexes
        #   self.single_obs.
        singles_idx [i] = n_nondup_singles + n_orig_singles

        # Record to be added to self.single_obs member var. Value indexes
        #   chords.
        incoming_singles_idx [n_nondup_singles] = i
        n_nondup_singles += 1

      # ith incoming chord has no duplicates in existing self.single_obs, but
      #   has duplicates in incoming chords
      elif incoming_existing_dup_idxes [i] == -1 and \
        incoming_self_dup_idxes [i] != -1:

        # If this is the first incoming chord in its set of duplicates in
        #   incoming chords, add it to self.single_obs.
        if i < n_nondup_singles:
          singles_idx [i] = n_nondup_singles + n_orig_singles

          incoming_singles_idx [n_nondup_singles] = i
          n_nondup_singles += 1

        # Else, it has already been added, don't add again to self.single_obs, 
        #   i.e. don't add to incoming_singles_idx.
        else:
          # The incoming duplicate's will-be-index in self.single_obs
          singles_idx [i] = singles_idx [incoming_self_dup_idxes [i]]

      # ith incoming chord has duplicates in existing self.single_obs, and
      #   no duplicates in incoming chords. Don't add it to self.single_obs,
      #   just record the chord's integer index.
      elif incoming_existing_dup_idxes [i] != -1 and \
        incoming_self_dup_idxes [i] == -1:
        singles_idx [i] = incoming_existing_dup_idxes [i]

      # Duplicate both in existing self.single_obs and incoming chords.
      #   Don't add it to self.single_obs, just record the chord's integer idx
      else:
        # These should be the same, ideally, but I don't know if they are.
        #singles_idx [i] = incoming_existing_dup_idxes [i]
        singles_idx [i] = singles_idx [incoming_self_dup_idxes [i]]


    # Remove unused rows
    # API https://docs.scipy.org/doc/numpy/reference/generated/numpy.delete.html
    incoming_singles_idx = np.delete (incoming_singles_idx,
      np.s_ [n_nondup_singles:])
    n_singles_added = incoming_singles_idx.shape [0]

    # Debug printout
    '''
    print ('Incoming chords:')
    print (incoming_singles)
    print ('%d chords added' % n_singles_added)
    print (incoming_singles [incoming_singles_idx, :])
    if n_singles_added != n_incoming_singles:
      existing_dups = np.where (incoming_existing_dup_idxes != -1) [0]
      print ('Duplicate chords\' existing indices:')
      print (incoming_existing_dup_idxes [existing_dups])
      print ('Duplicate chords:')
      print (incoming_singles [existing_dups, :])
    incoming_dups = np.where (incoming_self_dup_idxes != -1) [0]
    if incoming_dups.size > 0:
      print ('Duplicate chords\' incoming indices:')
      print (incoming_self_dup_idxes [incoming_dups])
    '''

    # Append non-duplicates to member var
    self.single_obs = np.append (self.single_obs, incoming_singles [incoming_singles_idx, :],
      axis=0)

    return singles_idx, n_singles_added


  # ================================================== obsl_list operations ==

  #   that_obsl: 1D numpy array of integers, these are integer names of chords
  def check_dup_obsl (self, that_obsl):

    # This indexes self.obsl_list and self.obsl_tallies
    obsl_dup_idx = -1

    # Loop through each existing observation
    for i in range (0, len (self.obsl_list)):
      # If number of chords in observation is same as the incoming
      #   obs, then compare the two.
      # (Else the incoming observation must be different)
      if self.obsl_list [i].shape [0] == that_obsl.shape [0]:
        # Subtract the two. If all elts ~ 0, then it's a dup
        if np.all (np.abs (self.obsl_list [i] - that_obsl) < 1e-6):
          obsl_dup_idx = i
          break
    # If none found, then obsl_dup_idx remains -1

    return obsl_dup_idx


  # Returns the index of that_obsl in self.obsl_list
  def add_nondup_obsl (self, that_obsl):

    # Grab the size BEFORE appending to it
    that_obsl_idx = len (self.obsl_list)

    # Add the incoming observation (list of chord indices in self.single_obs)
    #   to self.observs.
    self.obsl_list.append (that_obsl)
    # Init count to 1
    self.obsl_tallies = np.append (self.obsl_tallies, 1)

    #print ('Observation added: ')
    #print (that_obsl)

    # Return the index of the newly added that_obsl, in self.obsl_list
    return that_obsl_idx


  # Returns the index of that_obsl in self.obsl_list
  def check_dup_and_add_obsl (self, that_obsl, that_obsl_tally=1):

    that_obsl_idx = self.check_dup_obsl (that_obsl)

    # Incoming observation is not a duplicate. Add it and create a new integer
    #   name for it.
    if that_obsl_idx == -1:
      that_obsl_idx = self.add_nondup_obsl (that_obsl)

    # Incoming observation is a duplicate, just increase the count of how many
    #   times this observation was seen
    else:
      self.obsl_tallies [that_obsl_idx] += that_obsl_tally

      if DEBUG_DUPS:
        print ('Observation is a duplicate: ')
        print (that_obsl)

    return that_obsl_idx


  # ========================================= operation on all three arrays ==

  # NOTE: If you add fields to this object class, then merge the new fields
  #   in this function!
  def merge_with_instance (self, that_inst):

    # Add the nChords chords
    self.add_single_obs_array (that_inst.single_obs)

    n_that_obsl = that_inst.get_n_obsl ()
    that_obsl_idxes = np.zeros ((n_that_obsl, ), dtype=int)

    # Loop through nObsl observation lists
    for i in range (0, n_that_obsl):

      # Add the obsl and its tally
      that_obsl_idxes [i] = self.check_dup_and_add_obsl (
        that_inst.obsl_list [i], that_inst.obsl_tallies [i])

    # Return the latest index in this instance's obsl that matches each row in
    #   that_inst's obsl_list. If some rows were added from that_inst, then
    #   they now appear at the end of this instance's obsl, this is reflected.
    # Index indexes that_inst. Value indexes this instance (self).
    return that_obsl_idxes


  # ======================================== Discretization and replacement ==

  # Counterpart of prob_of_observs.py discretize_obs_to_hist_centers() used
  #   for triangles.
  # For using histogram bin centers as observations z, instead of specific
  #   individual chords.
  # Returns old_to_new_obsl_map, a dictionary, for caller to replace the
  #   corresponding PoseList.pose_obsl with the new obsl integer names.
  #   Key: obsl number to replace
  #   Value: Value to replace obsl number with
  def discretize_to_hist_centers (self, bin_edges, bin_centers):

    print ('Discretizing chords to histogram bin centers, to enable marginalization over classes y and generalization across object instances...')

    # bin_centers: nPts x nDims
    found_fit, bin_idxes, bin_centers = IncrementalHist.find_bin_fit (
      self.single_obs, bin_edges, bin_centers, IncrementalHist.BIN_RANGE_THRESH,
      return_tuples=False)

    # Replace raw chord observations member field with chord histogram bin
    #   centers!
    # TODO: Caveat: If a chord did not find a fitting histogram, this is
    #   usually because the chord is outside the min and max range of the
    #   entire histogram. Here, we just keep that chord in its raw value.
    #
    #   The problem is that if the tree hits that observation z and tries to
    #   calculate the probability p(.|z,y,.), it might not find z in the other
    #   classes y, because they didn't observe this raw chord, and there is
    #   no histogram bin that this chord fell into, for that class y to ever
    #   have a histogram center representing this raw chord! Then you'll get an
    #   error somewhere, either n_tgt_obs=0 or somewhere else.
    #
    #   To resolve that problem, make sure the min and max histogram range is
    #   inclusive of all the data.
    #   Second way, simpler and quicker but more risky, is to pass in a larger
    #   BIN_RANGE_THRESH, which might absorb that extraneous chord into one of
    #   the bins on the far extremes. However, the risk is that if you set a
    #   BIN_RANGE_THRESH that is larger than the bin size, then you'll group
    #   chords into the wrong bins! So THRESH cannot be larger than bin size.
    #   find_bin_fit() will warn about this.
    self.single_obs [found_fit, :] = bin_centers [found_fit, :]

    if DEBUG_DISCRETIZE:
      print ('Shape before elminating dups:')
      print (self.single_obs.shape)


    # Eliminate duplicate rows in single_obs, after re-assigning above
    old_to_new_obsl_map = self.eliminate_single_obs_dups ()

    return old_to_new_obsl_map


  def eliminate_single_obs_dups (self):

    if DEBUG_DISCRETIZE:
      print ('%sEliminating single-observation duplicates after discretizing...%s' % (
        ansi_colors.OKCYAN, ansi_colors.ENDC))

    #print ('self.single_obs:')
    #print (self.single_obs)

    # Index is incoming, value is existing reference.
    # [index]=value indicates index(th) element is a duplicate of value(th)
    #   elt. Value is the first copy of an existing elt that index(th) elt is
    #   a duplicate of.
    dup_idxes = find_dups_self (self.single_obs)
    if DEBUG_DISCRETIZE:
      print ('dup_idxes:')
      print (dup_idxes)

    # The non-duplicates. This includes the first copy of a duplicate, which we
    #   keep, so it's basically a non-duplicate.
    nondup_where = np.where (dup_idxes == -1)
    if DEBUG_DISCRETIZE:
      print ('%d non-duplicates, these will be kept:' % nondup_where [0].size)
      print ('nondup_where:')
      print (nondup_where)

    dups_where = np.where (dup_idxes != -1)
    if DEBUG_DISCRETIZE:
      print ('%d duplicates, these will be removed:' % dups_where [0].size)
      print ('dups_where:')
      print (dups_where)

    # Remove elements with dup idx -1, meaning not a duplicate.
    #   Also needed `.` np.delete() warns about negative idxes.
    #dup_idxes = np.delete (dup_idxes, nondup_where)
    #print ('dup_idxes after removing nondup_where:')
    #print (dup_idxes)
    # Delete duplicates
    #self.single_obs = np.delete (self.single_obs, dup_idxes, axis=0)

    # Line above is wrong. np.delete() takes the index. dup_idxes returns the
    #   line numbers OF the duplicate that we are keeping, not a boolean of
    #   whether something is a duplicate! To get a boolean, use != -1 to
    #   find the dups.
    self.single_obs = np.delete (self.single_obs, dups_where, axis=0)

    if DEBUG_DISCRETIZE:
      print ('Shape after elminating dups:')
      print (self.single_obs.shape)


    # For each eliminated row, replace the eliminated row number that appears
    #   in obsl_list, with the row number of the kept row that is a duplicate
    #   of the eliminated row.

    '''
    Example:
dup_idxes:
[-1  0  0 -1  3  3 -1  6  6 -1 -1 -1 -1 12 12 12 -1 -1 17 17 17 17 17 -1 -1
 24 -1 -1 27 27 27 27 -1 32 -1 34 34]
15 non-duplicates, these will be kept:
nondup_where:
(array([ 0,  3,  6,  9, 10, 11, 12, 16, 17, 23, 24, 26, 27, 32, 34]),)
22 duplicates, these will be removed:
dups_where:
(array([ 1,  2,  4,  5,  7,  8, 13, 14, 15, 18, 19, 20, 21, 22, 25, 28, 29,
       30, 31, 33, 35, 36]),)
    '''


    # Key: single observation row number to replace
    # Value: Value to replace single observation row number with
    # Every single old row number should be a key!! Both nondup and dup need
    #   to be replaced, `.` dups will shift indices up.
    old_to_new_obs_map = {}

    # For each orig row that was non-dup, also need to replace with its new
    #   index, which will be shifted `.` all the duplicates are deleted.
    # ndu for "non-dup"
    for ndu in nondup_where [0]:
      new_ndu = np.where (nondup_where [0] == ndu) [0] [0]
      old_to_new_obs_map [ndu] = new_ndu

    # For each original row that was found to be a duplicate, find its new
    #   row index.
    for du in dups_where [0]:

      #print ('du:')
      #print (du)

      # Retrieve its first duplicate's (i.e. the first copy, which we see as a
      #   non-duplicate and kept) original row number
      # In example above, du = 4, dup_idxes[du] = dup_idxes[4] = 3 -> ndu
      #   4 is a duplicate chord. Its corresponding nondup that we kept is
      #   chord 3. Chord 3 is the 2nd chord overall that is not a duplicate,
      #   so it will have index [1] in the new single_obs. Therefore anywhere
      #   we see 4, we replace it with 1.
      #   Additionally, wherever we see the old non-dup chord 3, we also need
      #   to replace with 1, its new index (this is recorded in loop above).
      ndu = dup_idxes [du]
      #print ('ndu:')
      #print (ndu)

      # In example above, new_ndu = nondup_where.index (3) = 1
      # Assumption: All elts in nondup_where are unique. This is ensured by
      #   find_dups_self(), which only keeps ONE copy of each duplicate. .`.
      #   can just index [0][0], the latter [0] assumes there is only 1.
      #new_ndu = np.where (nondup_where [0] == ndu) [0] [0]
      #print ('new_ndu:')
      #print (new_ndu)

      # Wherever we see old dup du, we need to replace it with new_ndu.
      old_to_new_obs_map [du] = old_to_new_obs_map [ndu]


    # Recalculate self.obsl_list and self.obsl_tallies with the new chord
    #   replacements.
    old_to_new_obsl_map = self.replace_obsl_list (old_to_new_obs_map)
    if DEBUG_DISCRETIZE:
      print ('New obsl_list has %d elements' % len (self.obsl_list))

    # To see debug outputs
    if DEBUG_DISCRETIZE:
      raw_input ('Press enter')


    # At the end, sanity check sizes are the same btw obsl_list and
    #   obsl_tallies.
    assert (len (self.obsl_list) == self.obsl_tallies.size)

    return old_to_new_obsl_map


  # Internal function. Do not call from outside. Used by
  #   eliminate_single_obs_dups ().
  # Parameters:
  #   old_to_new_obs_map: Dictionary.
  #     {old single obs row number: new single obs row number}
  def replace_obsl_list (self, old_to_new_obs_map):

    if DEBUG_DISCRETIZE:
      print ('%sReplacing single-observation integer names in observation-list after discretizing...%s' % (
        ansi_colors.OKCYAN, ansi_colors.ENDC))

    # Make deepcopies before resetting
    old_obsl_list = deepcopy (self.obsl_list)
    old_obsl_tallies = deepcopy (self.obsl_tallies)
    # Reset member fields, so that we can call self.check_dup_and_add_obsl() to
    #   correctly repopulate obsl_tallies.
    self.obsl_list = []
    self.obsl_tallies = np.zeros (0)

    # Key: obsl number to replace
    # Value: Value to replace obsl number with
    old_to_new_obsl_map = {}

    # For each obsl, perform the replacement in old_to_new_obs_map.
    for i in range (len (old_obsl_list)):

      # 1D NumPy array
      # To replace self.obsl_list [i].
      curr_obsl = old_obsl_list [i]
      curr_tally = old_obsl_tallies [i]

      # For each old row number to replace, replace it
      for du in old_to_new_obs_map.keys ():

        # When correctly implemented, when this is printed in the loop, 1st
        #   number should count in increment of 1. 2nd number should always be
        #   continuous, no integer should be skipped, but consecutive ints
        #   may repeat.
        if DEBUG_DISCRETIZE:
          print ('Replacing %d with %d' % (du, old_to_new_obs_map [du]))

        # If this list doesn't contain du, skip it
        replace_idxes = np.where (curr_obsl == du) [0]
        if replace_idxes.size == 0:
          continue
        # Else replace du
        else:

          if DEBUG_DISCRETIZE:
            print ('Before replacement:')
            print (curr_obsl)
         
          curr_obsl [replace_idxes] = old_to_new_obs_map [du]
         
          if DEBUG_DISCRETIZE:
            print ('After replacement:')
            print (curr_obsl)


      # Add the obsl and its tally *properly*. This is how prob_writer.py adds
      #   them when probs pkl files are trained. This will properly keep count
      #   of the tally, more reliable than manually adding the new obsl and
      #   manually recounting the tally
      #   afterwards.
      # Returns index of curr_obsl in self.obsl_list. Basically the newly
      #   added integer name of it.
      added_obsl_idx = self.check_dup_and_add_obsl (curr_obsl,
        that_obsl_tally=curr_tally)

      old_to_new_obsl_map [i] = added_obsl_idx

      if DEBUG_DISCRETIZE:
        print ('self.obsl_list:')
        print (self.obsl_list)
        print ('self.obsl_tallies:')
        print (self.obsl_tallies)

    return old_to_new_obsl_map

