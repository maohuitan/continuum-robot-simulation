#!/usr/bin/env python

# Translated from Nikolay Atanasov and Mikko Lauri's MATLAB code
# Translated to Python and visualized by Mabel Zhang
# 17 Jan 2016
#
# A simple implementation of Monte Carlo tree search, for Markov Decision
#   Process (MDP).
# A base class for more specific implementations of MDP. Has the visualization
#   code in pydot.
#

# ROS
import rospy
import rospkg

# Python
import os
from copy import deepcopy

# NumPy
import numpy as np
from scipy.stats import rv_discrete

# Matplotlib
import matplotlib
import matplotlib.pyplot as plt

# Pydot
import pydot
from cStringIO import StringIO
import matplotlib.image as mpimg

# My packages
from util_continuum.matplotlib_util import mpl_color
from util_continuum.config_paths import get_active_img_path

# Local


# Translated from matlab/src/StateNode.m
class StateNode:

  # Parameters:
  #   key: String
  #   action_list: Python list of integers
  #   parent: parent StateNode - NOT using anymore, `.` this parent and child
  #     representation only allows for real trees - no more than 1 parent
  #     allowed. It won't print in a way that visualizes common parents either.
  #     Now that I have pydot node and edges to represent parent and child,
  #     I don't need to keep track of parent and child keys here myself.
  def __init__ (self, key, action_list):#, parent=None):

    # String
    self.hashkey = key

    # List of integers
    self.action_list = deepcopy (action_list)

    # List of integers
    # N[i]: Number of visits for action i
    # Q[i]: Max reward for action i?? Q:
    self.N = np.zeros (len (action_list))
    self.Q = np.zeros (len (action_list))

    #self.children = []

    #if parent:
    #  self.parent_key = parent.hashkey
    #  parent.children.append (self.hashkey)
    #else:
    #  self.parent_key = 'null'


  # Parameters:
  #   action_id: Action chosen at this node, to traverse down to a child of 
  #     this node.
  #   r: Reward as a result of choosing the specified reward at this node.
  def update_NQ (self, action_id, r):

    # Increment number of visits by the specified action
    self.N [action_id] += 1

    # Debug printout
    # This happens in matlab code too. This is the cause that sometimes, an
    #   action with larger N can have a smaller Q value. Would make more sense
    #   if I know what this is supposed to be.
    # Move the 2nd term to the right, and you get:
    #   r < self.Q[action_id],
    #   which means, the new reward is greater than the existing reward in the
    #     self.Q list.
    if r - self.Q [action_id] < 0:
      print ('r - self.Q[action_id] is %.2f, negative!' % (r-self.Q[action_id]))

    # Q += ((r - Q) / N)
    #   Q: Is this the UCB1 thing? Q is the most reward can go?
    #   A: No, UCB1 is in tree_policy. Can't figure out what this is, should
    #     ask Nikolay.
    #   r - Q penalizes reward, if the new reward r is less than the existing
    #     reward Q at the specified action from this node. `.` then r - Q is
    #     negative! So it becomes a cost! Not sure why this decision, `.` it
    #     would FAVOR LATER CHOICES of this action. It requires later choices
    #     r to be greater than Q, in order for the reward to remain positive.
    #     Maybe this is to enforce consistency? So if the previous time when Q
    #     was recorded was chosen by rollout policy, and r is when action is
    #     chosen by tree policy, then you DO want to favor r, `.` tree policy
    #     is more trustworthy than the rollout policy baseline.
    #   N in denominator EVENLY SPREADS r-Q across all the visits to this
    #     action at this node, including the latest visit, which is included
    #     in N in the line above!
    self.Q [action_id] += ((r - self.Q [action_id]) / self.N [action_id])


# Translated from MDP struct in matlab code mcts_mdp_nx.m
# I don't need to use this class for our data, I have our own probabilities
#   data in dictionary format.
class MDP:

  actions = []
  stateTransition = []
  sampleState = None
  reward = []

  # Maps from integer state id to list of integer actions
  # For vanilla case, just return all actions, independent of state s. This
  #   means all actions are available to all states.
  # Can't define in here, doesn't work with calling by self.mdp.availableA,
  #   Python seems to pass in self as first arg, resulting in calling lambda
  #   function with 2 args, which will error that labmda only takes 1 arg, 2
  #   given.
  availableA = None


# Translated from params struct in matlab code
class MCTS_Parameters:

  nSims = 0
  # 0-based. root is 0.
  horizon = 0
  explore = 0


  # Full path to directory where images will be saved
  img_dir = get_active_img_path ()

  # Temp for testing locally
  #img_dir = os.path.dirname (os.path.realpath (__file__))



# ======================================================= Parent tree class ==

# Base parent class. No policy methods are implemented. Only implements
#   pydot visualization functions, to create pydot image for easy debugging and
#   analysis.
# Child class needs to implement these functions:
#   simulate()
#   end_of_simulate()
#   rollout_policy()
#   tree_policy()
#   In addition, a version of StateNode for the specific application.
class MCTS_MDP:

  def __init__ (self, params):

    self.params = params
 
    # The actual search tree, built incrementally in each simulation.
    #   Key: node key, whatever type you want
    #     In MCTS_Basic, it's string.
    #     In MCTS_ActiveTouch, it's a 3-tuple for triangle parameters.
    #   Value: node class object, whatever type you want.
    #     In MCTS_Basic, it's StateNode.
    #     In MCTS_ActiveTouch, it's MCTSNode_ActiveNode.
    self.tree_nodes = dict ()
 
    # pydot visualization
    # Ref pydot ex: https://gist.github.com/fportillo/a499f9f625c6169524b8
    # PyDot bugs (I found out the hard way, trying to use them!):
    # Note that deepcopy (self.vis_dict) and deepcopy (self.tree_vis) won't
    #   work correctly, `.` seems PyDot didn't implement their copy function
    #   correctly. The objects would be copied, but they are missing their
    #   member functions, e.g. pydot.Edge will be missing get_label() and
    #   set_label() functions, etc.
    # Note also that pydot.Dot.get_node() and get_edge() don't work correctly.
    #   They either return the wrong node / edge, or don't find the existing
    #   node / edge! Sucks! You are better off keeping track of all the nodes
    #   and edges yourself, using dictionaries!!
    #   Ugh, pydot seems a terribly implemented library.
    self.tree_vis = pydot.Dot (graph_type='digraph')
    # Dictionary that saves all the pydot graph nodes and edges.
    #   Key:
    #     old version of add_node_to_pydot_tree(): 'node_name' string.
    #     X|new version add_node_to_pydot_tree_new(): node_key 4-tuple as in
    #     X|  MCTSNode_ActiveTouch
    #   Value: pydot.Node
    self.node_vis_dict = dict ()
    #   Key: ('parent_name', 'child_name'). Value: pydot.Edge
    self.edge_vis_dict = dict ()
 
    # Display tree whenever a new node is added (most frequent)
    self.DISPLAY_TREE_PER_ADD = False
    # Display tree once each simulation (less frequent)
    self.DISPLAY_TREE_PER_SIM = False
    self.VIS_POLS_PER_SIM = False
 
    self.sim_i = 0


  def get_tree_nodes (self):
    return self.tree_nodes


  # ======================================================== Visualize tree ==

  # This kind of printing only supports trees with no more than one parent.
  #   Since I now have pydot graph that is more general, I don't need this
  #   anymore.
  '''
  def print_tree (self, node_key, list_printed):

    node = self.tree_nodes [node_key]
 
    if node_key in list_printed:
      return
    else:
      list_printed.append (node_key)
 
    # Get child's state and depth from node key
    depth, _ = self.extract_from_node_name (node_key)
 
    # Make spaces ahead of line based on depth
    margin = '  ' * depth
    print ('%s%s  Q: ' % (margin, node_key)) ,

    for i in range (0, np.size (node.Q)):
      print ('%.2f' % node.Q [i]) ,

    print ('  N: ') ,
    for i in range (0, np.size (node.N)):
      print ('%d' % node.N [i]) ,
    print ('')
 
    # Loop through each child of the node specified in args
    for i in range (0, len (node.children)):
      curr_key = node.children [i]
      # Recursive call
      list_printed = self.print_tree (curr_key, list_printed)
 
    return list_printed
  '''


  # Set PyDot visualization's Node object to the given color
  # Parameters:
  #   node_name: String node name.
  #   color: String. Can be a hex color e.g. '#FF8800', or 'green'.
  def set_pydot_node_color (self, node_name, color):
    self.node_vis_dict [node_name].set_color (color)

  # Set PyDot visualization's Edge object to the given color
  # Parameters:
  #   edge_name: ('parent_name', 'child_name') string tuple
  #   color: String. Can be a hex color e.g. '#FF8800', or 'green'.
  def set_pydot_edge_color (self, edge_name, color):
    self.edge_vis_dict [edge_name].set_color (color)
    self.edge_vis_dict [edge_name].set_fontcolor (color)

  def relabel_pydot_edge (self, edge_name, new_label):
    self.edge_vis_dict [edge_name].set_label (new_label)


  # Only used by original vanilla tree in mcts_mdp.py. Not used by
  #   mcts_active_touch.py, which stores the pydot Nodes and edges in
  #   MCTSNode_ActiveTouch object. It is a cleaner implementation.
  # Add an additional node and an edge to its parent node, to the pydot graph.
  # Parameters:
  #   parent_name, child_name: String node names. Must be strings! Otherwise
  #     PyDot won't know how to draw the image. Must not have commas! Else
  #     PyDot errors too.
  #   node_only: If True, does not create edge from parent_name to child_name,
  #     only create the child_name node. This is useful when call from
  #     end_of_simulate(), then don't have an action to connect child to
  #     parent yet! Action is chosen in tree policy and rollout, which are
  #     executed after end_of_simulate().
  #     Actually this shouldn't be needed... if child key doesn't exist yet,
  #     this fn should create it.. how come it doesn't? Seems the error is at
  #     where parent key is not created.
  #   edge_lbl: Label on the new edge to be created
  def add_node_to_pydot_tree (self, parent_name, child_name,
    edge_lbl=''):

    # Must use strings for labels, else pydot doesn't know how to show image
    if not isinstance (parent_name, str) or not isinstance (child_name, str) or \
        not isinstance (edge_lbl, str):
      print ('%sWARNING: Node and Edge label must be a string! Else PyDot cannot visualize the graph correct, will throw error at pydot.Dot.write_png().%s' % (
        ansi_colors.WARNING, ansi_colors.ENDC))


    # Add node
    # If node with the observation exists, but it's not at the immediate next
    #   layer in the tree (enforced by node key (obs0 obs1 obs2 depth), then
    #   create a new node. `.` Each layer in tree must be a unique time step.
    # Ref pydot examples
    # http://stackoverflow.com/questions/24657384/plotting-a-decision-tree-with-pydot
    # https://pythonhaven.wordpress.com/2009/12/09/generating_graphs_with_pydot/
    if child_name not in self.node_vis_dict.keys ():
      new_child = pydot.Node (child_name)
      self.tree_vis.add_node (new_child)
      # Add to member var to access later, `.` PyDot Dot.get_node() never works
      self.node_vis_dict [child_name] = new_child

      #print ('Creating %s, parent %s, edge %s' % (child_name, parent_name,
      #  edge_lbl))

    # If this node is already in graph (happens when this fn is called by
    #   tree policy, not rollout policy. This happens when tree policy adds
    #   an additional parent edge to a node that already has a parent), just
    #   point to it.
    else:
      new_child = self.node_vis_dict [child_name]

    # Add edge, if it's not the fake edge from 'null' to root of tree.
    if parent_name != 'null' and \
        (parent_name, child_name) not in self.edge_vis_dict.keys ():
      new_edge = pydot.Edge (self.node_vis_dict [parent_name], new_child,
        label=edge_lbl)
      self.tree_vis.add_edge (new_edge)
      # Add to member var to access later, `.` PyDot Dot.get_edge() never works
      self.edge_vis_dict [(parent_name, child_name)] = new_edge

    if self.DISPLAY_TREE_PER_ADD:
      self.display_tree (self.tree_vis)


  def display_tree (self, tree_vis):

    plt.figure (2)

    # Ref display pydot image without saving to file:
    #   http://stackoverflow.com/questions/4596962/display-graph-without-saving-using-pydot
    png_str = tree_vis.create_png (prog='dot')
    sio = StringIO ()
    sio.write (png_str)
    sio.seek (0)
    img = mpimg.imread (sio)

    # Display image
    imgplot = plt.imshow (img, aspect='equal')
    # The pause is needed on MacBook Air OS X, not needed on MacBook Pro Ubuntu
    #   Ref: http://stackoverflow.com/questions/2604119/matplotlib-pyplot-pylab-not-updating-figure-while-isinteractive-using-ipython
    plt.pause (0.01)
    plt.show (block=False)

    # Write to file
    # Don't use eps. pydot saves eps that can't be opened
    rewards_img_name = os.path.join (self.params.img_dir,
      'tree_vis_sim%d.png' % (self.sim_i))
    # Don't use plt.savefig for the tree, will be pixelated. Use pydot tree's
    #   built in write_png().
    #   If you must use matplotlib for tree, can try the Agg backend:
    #   matplotlib.use ('Agg')
    #   http://matplotlib.org/faq/howto_faq.html
    #   But I never did that for line plots, bar plots, etc, and matplotlib eps
    #   were saved beautifully. Just pydot's problem.
    #plt.savefig (rewards_img_name)
    # Ref various write fns in pydot.Dot can be accessed simply by write():
    #   https://github.com/erocarrera/pydot/blob/master/pydot.py
    #   http://pydotplus.readthedocs.io/reference.html?highlight=write_ps
    #   but write_ps doesn't work if I used create_png(), it only saves half
    #   the graph, whatever that fits in a letter size page! No good.
    tree_vis.write_png (rewards_img_name)

    print ('Pydot per-sim tree image saved to %s' % rewards_img_name)

    # Uncomment this to see how tree grows step-by-step
    uinput = raw_input ('Press enter: ')


  # Visualize pydot graph in each simulation, with tree policy's and rollout
  #   policy's choices marked on the corresponding edges.
  # Parameters:
  #   sim: Only used for output image name.
  def vis_policies_per_sim (self, sim):

    # Create it from fresh, `.` pydot objs don't copy correctly via deepcopy
    per_sim_vis = pydot.Dot (graph_type=self.tree_vis.get_graph_type ())

    print ('node_vis_dict:')
    print (self.node_vis_dict)
    print ('edge_vis_dict:')
    print (self.edge_vis_dict)

    per_sim_vis_dict = {}
    for e_key in self.edge_vis_dict.keys ():
      # Add the edge and its parent and child node to the new pydot graph.
      # e_key[0] is string name of parent node, e_key[1] is of child node
      if e_key [0] not in per_sim_vis_dict.keys ():
        parent_node = pydot.Node (e_key [0])
        per_sim_vis_dict [e_key [0]] = parent_node

      if e_key [1] not in per_sim_vis_dict.keys ():
        child_node = pydot.Node (e_key [1])
        per_sim_vis_dict [e_key [1]] = child_node

      edge = pydot.Edge (self.node_vis_dict [e_key [0]],
        self.node_vis_dict [e_key [1]])
      per_sim_vis_dict [(e_key [0], e_key [1])] = edge

      per_sim_vis.add_node (parent_node)
      per_sim_vis.add_node (child_node)
      per_sim_vis.add_edge (edge)

    print ('per_sim_pols_dict')
    print (self.per_sim_pols_dict)

    # Change its edge labels to mark which ones were traversed by tree
    #   policy calls, which one is created by rollout policy
    # self.per_sim_pols_dict ['tree'] [ei] is the (ei)th edge traversed by
    #   tree policy.
    for ei in range (0, len (self.per_sim_pols_dict ['tree'])):

      # If this is the dummy edge from null parent to root of tree, ignore
      if self.per_sim_pols_dict ['tree'] [ei] [0] == 'null':
        continue

      # Print the edge
      #print (self.per_sim_pols_dict ['tree'] [ei])
      # Print name of the edge
      #print (self.edge_vis_dict [self.per_sim_pols_dict ['tree'] [ei]].get_label ())

      per_sim_vis_dict [(
        self.per_sim_pols_dict ['tree'] [ei] [0],
        self.per_sim_pols_dict ['tree'] [ei] [1]
        )].set_label ('tree_pol')

    # For edges created by rollout policy
    for ei in range (0, len (self.per_sim_pols_dict ['rollout'])):
      # If this is the dummy edge from null parent to root of tree, ignore
      if self.per_sim_pols_dict ['rollout'] [ei] [0] == 'null':
        continue

      per_sim_vis_dict [(
        self.per_sim_pols_dict ['rollout'] [ei] [0],
        self.per_sim_pols_dict ['rollout'] [ei] [1]
        )].set_label ('rollout_pol')

    policy_img_base = 'per_sim_pol_%d.png' % sim
    policy_img_name = os.path.join (self.params.img_dir, policy_img_base)
    per_sim_vis.write_png (policy_img_name)
    print ('Per-simulation image of policy choice in the graph saved to %s' % (
      policy_img_name))

    self.display_tree (per_sim_vis)
    uinput = raw_input ('Press enter: ')



# ======================================================= Simple tree class ==

# This is a vanilla implementation of MCTS to demonstrate what it does.
# In this class, rewards, state transitions, etc. are all randomly generated.
class MCTS_Basic (MCTS_MDP):

  # Parameters:
  def __init__ (self, mdp, params):

    # Parent class ctor
    MCTS_MDP.__init__ (self, params)

    # For our data, the mdp here corresponds to our p(z2 | y, z1, a) probs data
    self.mdp = mdp

    # Don't use comma, `.` pydot errors on comma in node label
    self.node_delimiter = '_'
    self.node_format = '%d' + self.node_delimiter + '%d'


  def make_node_name (self, depth, state):

    return self.node_format % (depth, state)


  def extract_from_node_name (self, node_key):

    # Assumption: Order of node_format is depth, state
    depth, state = node_key.split (self.node_delimiter)
    depth = int (depth)

    return depth, state


  # Parameters:
  #   s0: Integer start state id
  # Returns:
  #   pa: Normalized reward vector for root node. Vector size is number of
  #     actions available at root node.
  #   R: Vector of rewards. Size is number of simulations
  def mcts_mdp (self, s0):

    # Init root node
    # key is string
    root_key = self.make_node_name (0, s0)
    root = StateNode (root_key, self.mdp.availableA (s0))
    self.tree_nodes [root_key] = root

    # Add root to pydot graph
    self.add_node_to_pydot_tree ('null', root_key)
 
    # Vector of rewards per simulation, to plot line graph of progression of
    #   rewards
    R = np.zeros ((self.params.nSims, 1))
 
    # Simulate each iteration
    for si in range (0, self.params.nSims):

      print ('Simulation %d' % si)

      self.sim_i = si

      # Make per-simulation pydot graph visualization, for step-by-step images
      if self.VIS_POLS_PER_SIM:
        # Re-init. Values are lists of tuples ('parent_name', 'child_name')
        #   Used for pydot only, so elts in lists in values must be strings!
        self.per_sim_pols_dict = {'tree': [], 'rollout': []}
 
      # Root's parent is null
      # This is a recursive function, it will call itself to traverse from root
      #   of tree to either a leaf (depth == target depth); or middle of the
      #   tree where tree depth < target, but there are no more child nodes,
      #   in which case a new node will be created and simulation ends.
      R [si] = self.simulate (s0, 0, 'null')
 
      if self.VIS_POLS_PER_SIM:
        #print ('node_vis_dict.keys():')
        #print (self.node_vis_dict.keys ())
        #print ('per_sim_pols_dict:')
        #print (self.per_sim_pols_dict)
        self.vis_policies_per_sim (si)

      if self.DISPLAY_TREE_PER_SIM:
        self.display_tree (self.tree_vis)

      # Print tree each iteration
      #print ('Current tree:')
      #self.print_tree (root_key, [])
      print ('')

    # Divide rewards by the total rewards, to normalize rewards to sum to 1.
    #   That gives you a distribution of rewards across all possible actions.
    #   Do this only for root (why? Just as an example of how to use the
    #   rewards after tree returns? You'd pick the max reward at each tree
    #   node, thereby traversing down following the max-reward actions?)
    # Need deepcopy, else changes root.Q!!
    pa = deepcopy (root.Q)
    pa /= np.sum (pa)

    # Print tree once at the very end
    #print ('Current tree:')
    #self.print_tree (root_key, [])
    #print ('')

    return (pa, R)


  # Returns reward:
  #   If rollout policy, compute reward from specified depth to horizon, sum
  #     rewards from all these depths.
  #   If tree policy, compute reward from recursive calls to simulate(),
  #     and sum all depths called. That includes the reward returned from
  #     base case of recursion, i.e. the rollout policy, and rewards from
  #     recursive cases of recusion, i.e. one or more tree policies.
  #   This means each depth of the tree traversal is associated with a reward.
  #     Rewards of all depths are summed and returned. But this is not the
  #     final reward assigned to top-level recursive node. The final reward is
  #     calculated by a formula in update_NQ(), Q_a = (r - Q_a) / N_a.
  def simulate (self, state, depth, parent_key):

    print ('state, depth: %d_%d. parent_key: %s' % (state, depth, parent_key))

    node_key = self.make_node_name (depth, state)
    ending = self.end_of_simulate (state, depth, parent_key)

    # Base case of recursive call
    # Runs end_of_simulate() first, regardless end or not. Get ret val.
    if ending:

      pol_lbl = 'rollout'

      r = self.rollout_policy (state, depth)

      if self.VIS_POLS_PER_SIM:
        if parent_key != 'null':
          self.per_sim_pols_dict [pol_lbl].append ((parent_key, node_key))

      # Set to indicate this base case of recursive ended. This tells previous
      #   recursive callers in the stack, that they are on a path that led to
      #   the rollout policy.
      #rec_ended = True

    else:

      pol_lbl = 'tree'

      node = self.tree_nodes [node_key]

      # Keep traversing down, based on tree policy, grab an action
      (a, a_id) = self.tree_policy (node)
      # Grab index of next state to go to, based on current state and action.
      #   It's an integer
      sn = self.mdp.sampleState ((state, a))

      # Add the new edge to the Pydot graph visualization, display latest
      #   graph.
      if parent_key != 'null':
        self.add_node_to_pydot_tree (parent_key, node_key)

      # Recursive call
      # Calculate reward for going to next state sn, simulate down from sn
      #r, rec_ended = self.simulate (sn, depth + 1, node_key)
      # I don't think I need rec_ended, remove line above when works
      r = self.simulate (sn, depth + 1, node_key)
      r += self.mdp.reward [a, sn, state]

      # Update reward at this state
      node.update_NQ (a_id, r)

      # Make per-simulation pydot graph image for debugging
      if self.VIS_POLS_PER_SIM:
        # I think I don't need rec_ended at all. TODO Remove this ret val when
        #   stable. I thought I needed this `.` I didn't add visualize_tree()
        #   in tree policy. Logically I shouldn't need this, `.` anything on
        #   the recursive call should already be on the path to the rollout
        #   policy, so ALL of them WILL END! So all the rec_ended will be True,
        #   never need to check it!
        # If this interior node is on the path to the new node that rollout
        #   policy chose
        #if rec_ended:
          # Label the edges. Indicate an edge by (from_node, to_node) tuple
          if parent_key != 'null':
            self.per_sim_pols_dict [pol_lbl].append ((parent_key, node_key))

    # Return flag to indicate whether the simulation has ended on this *path*,
    #   not just this call. So if the recursive call simulate() is part of
    #   the chosen path, then this node is part of chosen path too. Return the
    #   ret val from recursive call, not from end_of_simulate().
    return r#, rec_ended


  def end_of_simulate (self, state, depth, parent_key):

    node_key = self.make_node_name (depth, state)
 
    # If node doesn't exist in tree yet, create it.
    #   When a new node is created, that marks the end of the simulation.
    #   The new node will be child of parent node with parent_key. But the
    #   edge btw parent and new node still needs to be decided. Rollout policy
    #   picks the action (new edge) that will link the parent to new node.
    if node_key not in self.tree_nodes.keys ():

      print ('Creating %s' % node_key)

      self.tree_nodes [node_key] = StateNode (node_key,
        self.mdp.availableA (state))
        #self.tree_nodes [parent_key])

      # Add the new node to the Pydot graph visualization, display latest
      #   graph.
      self.add_node_to_pydot_tree (parent_key, node_key)

      print ('End of simulation')
      return True
 
    # If reached desired depth, but didn't reach last simulation yet, then run
    #   rollout policy to fill in the remaining simulations
    elif depth == self.params.horizon:
      print ('End of simulation')
      return True
 
    # Otherwise, run tree policy to get real actions
    else:
      return False


  # ============================================================== Policies ==

  # Given a node, randomly pick an action if there exists unvisited nodes, or
  #   take max of something if all nodes have been visited
  def tree_policy (self, node):
    print ('Tree policy')

    # Actions (from this node) that have not been visited.
    action_idx, = np.where (node.N == 0)
    # Find number of zero elements in node.N, these are actions that have been
    #   visited 0 times from this node.
    num_untried = np.size (action_idx)

    # Debug output
    #print ('node.N:')
    #print (node.N)
    #print ('num_untried = %d' % num_untried)

    if num_untried > 0:
      # Randomly pick an available action
      a_id = action_idx [np.random.randint (0, num_untried)]

    else:
      # Total number of times that this node has been visited. Sum takes the
      #   total across all actions. node.N[i] is the number of times action [i]
      #   has been visited.
      ns = np.sum (node.N)

      # Computes optimal action a* (a_id in code) using UCB1 bound formula.
      #   params.explore is the UCB1 parameter, exploration / exploitation
      #     constant
      #   node.Q is a vector, node.Q[i] seems to be reward of action [i]
      #   ns is the total number of simulations so far. It is n in the formula.
      #   node.N is a vector, node.N[i] is the number of times action [i] has
      #     been visited at this node.
      #
      # The only term in this line that's not in the UCB1 formula, is 
      #   params.explore. I think it's a ratio, like a weight, to say how much
      #   weight to put on the sqrt term.
      #   If explore is small decimal, then weight on Q is 1, weight on sqrt
      #     is smaller, then you prioritize reward Q.
      #   If explore is 1 (default), then reward and sqrt have equal weights.
      #   If explore > 1 (maybe), then sqrt has bigger weight.
      #
      #   Exploration vs. exploitation:
      #   A Google says exploitation is a local search, i.e. committing to a
      #   guess we think is correct, exploration is global search.
      #
      #   What does the sqrt mean though? If it has bigger weight, does that
      #   mean we prioritize exploitation (reward) over exploration (more
      #   search in action space)?
      #   It's like a ratio btw ns and N, i.e. number of times all actions are
      #   explored / number of times this action is explored. If this action
      #   has been explored very few times (node.N is small), then sqrt is
      #   big. Then argmax would be more likely to select this action, i.e.
      #   explore this action more! Yeah! So sqrt() IS the exploration term, it
      #   prioritizes exploration.
      #   Exploration means global search. It is done when we aren't certain
      #   about what the correct answer is yet, so we search around larger
      #   regions a little more.
      #
      #   On the other hand, if params.explore is small, then the sqrt is not
      #   prioritize as much. Then you'd select an action i that has high Q[i].
      #   That means an action that yielded high reward in the simulations so
      #   far. Selecting based on Q prioritizes exploitation.
      #   That makes sense. If an action i's reward Q[i] is high, then we must
      #   have had a lot of confidence in this action. So proceed with the
      #   action. Exploitation means a local search, i.e. committing to this
      #   action which we're confident in.
      #
      # This might be in the MCTS paper where this formula is presented, look
      #   there.
      #   Yes! p.5 in http://www.cameronius.com/cv/mcts-survey-master.pdf
      #   It says the reward term (Q) encourages exploitation. Just like I
      #     analyzed above! :D Hey, I have a bit of intuition in this.
      #   "The reward term encourages the exploitation of higher-reward
      #     choices, while the right hand term sqrt encourages the exploration
      #     of less-visited choices.
      # NOTE This eqn is missing denominator N_a underneath Q. It needs to have
      #   it, `.` it's in Browne Algo 2. Because first term is AVERAGE arm
      #   reward, in UCT and UCB1 eqns, it's not the reward itself. So it's the
      #   reward divided by N_a, number of times this action has been visited!
      val = node.Q + self.params.explore * np.sqrt (np.log (ns) / node.N)
      # This is the last step in UCB1 formula:
      #   a* = argmax_a (reward_a + sqrt ((2 ln n) / n_a))
      # Here since Q and N are vectors, no need to do for-loop for each a, just
      #   sum the vectors across all actions, and then do np.argmax(vector).
      a_id = np.argmax (val)

    a = node.action_list [a_id]

    # Debug output
    #print ('Chose a_id = %d' % a_id)

    return (a, a_id)


  # Returns total reward starting from specified depth down to horizon, picking
  #   random actions in depths in between.
  # Depth is 0-based
  def rollout_policy (self, state, depth):

    print ('Rollout policy')

    # Init ret val, reward
    r = 0.0

    # Traverse from given depth to bottom of tree, sum up the rewards.
    # Q: What if some depths of tree doesn't exist yet???
    #   > All are observed, hence MDP, not POMDP. All possible tree nodes
    #     are already in the 3D matrix MDP.StateTransition
    for i in range (depth, self.params.horizon + 1):

      # Pick a random action
      action_list = self.mdp.availableA (state)
      a = action_list [np.random.randint (0, len (action_list))]

      # Grab next state to go to, based on current state and random action
      sn = self.mdp.sampleState ((state, a))
      # Reward, cumulative for all nodes we traverse down to
      r += self.mdp.reward [a, sn, state]

      # Update state for next iteration. Next iteration will start traversing
      #   from this state. This is how the rollout policy traverses downwards
      #   from specified depth to horizon, picking an action that leads to a
      #   state at each depth.
      state = sn

    # Return total reward
    return r



# Parameters:
#   pk: 1D vector
#   nSamples: Number of samples you'd like to draw
#
# Ref: http://docs.scipy.org/doc/scipy-0.16.1/reference/generated/scipy.stats.rv_discrete.html
#   rv_discrete() creates random variable for arbitrary discrete distributions.
#   "Discrete distributions from a list of probabilities
#   Alternatively, you can construct an arbitrary discrete rv defined on a
#   finite set of values xk with Prob{X=xk} = pk by using the values keyword
#   argument to the rv_discrete constructor."
#   See Examples section for a usage of this.
#
# Translation note:
#   discretesample_nx() matlab fn accepts matrix inputs and calls
#     discretesample() from
#     http://www.mathworks.com/matlabcentral/fileexchange/21912-sampling-from-a-discrete-distribution/content/discretesample.m
#   Somebody had to write discretesample(), because MATLAB doesn't have
#     built-in sampling from an arbitrary discrete distribution.
#   Python has a function for this. We don't need to write a fn ourselves.
#     scipy.stats.rv_discrete lets you do arbitrary distributions
#     values=(xk, pk), where xk is x-value, pk is probabilities that sum to 1.
def discretesample (pk, nSamples):

  xk = range (0, np.size (pk))

  custm = rv_discrete (name='custm', values=(xk, pk))

  return custm.rvs (size=nSamples)


# Parameters:
#   rand_color: If True, color_idx is ignored. A random color_idx is generated
def plot_rewards (R, params, rand_color=True, color_idx=0,
  rewards_img_name=None): #, truetype=False):

  # Now set in active_predict.py
  # Copied from triangle_sampling triangles_svm.py, for ICRA PDF compliance
  #if truetype:
  #  matplotlib.rcParams['pdf.fonttype'] = 42
  #  matplotlib.rcParams['ps.fonttype'] = 42


  # Init color for the plot

  # Assumption: you probably won't run more than 10 iterations!
  #   With 7 objects, after 5 iters without Gazebo, it flats out anyway.
  #   With Gazebo, it probably takes longer to flat out, as the poses are more
  #   spread out.
  n_colors = 10

  if rand_color:
    color_idx = np.random.randint (n_colors)

  # gist_ncar has quicker color change than jet, good when you have a lot of
  #   items to plot.
  # nipy_spectral is also quick changing, but darker. I used this for IROS.
  #   Very pretty rich colors in a variety of range. Good for a small number
  #   of curves.
  # Dark2 and Set1 are fast changing dark pastels
  color = mpl_color (color_idx, n_colors, colormap_name='nipy_spectral') #'Dark2') #'gist_ncar')


  # Plot rewards from all simulations in matplotlib if want to

  plt.figure (1)

  line = plt.plot (range (1, params.nSims+1), R, color=color,
    label='Iter %d' % color_idx)
  #plt.setp (line, color='k')

  # Use very light gray, `.` shows up on LaTeX PDF as darker, overwhelms curves
  plt.grid (True, color=[0.8, 0.8, 0.8])
  plt.xlabel ('Simulation')
  plt.ylabel ('Rewards')
  #plt.xlabel ('Simulation epoch')
  #plt.ylabel ('Sum of discounted rewards')


  # Ref: http://matplotlib.org/users/legend_guide.html
  # To use legend, pass a label to plt.plot() for each object you want legend
  # loc=0 automatically chooses the 'best' location.
  #   1=upper right, 2=upper left, 3=lower left, 4= lower right.
  plt.legend (loc=0)

  # Save to .eps
  # Ref: http://stackoverflow.com/questions/5137497/find-current-directory-and-files-directory
  if rewards_img_name is None:
    rewards_img_name = os.path.join (params.img_dir,
      'rewards_nsims%d.eps' % (params.nSims))
  plt.savefig (rewards_img_name, bbox_inches='tight')
  print ('Rewards plot saved to %s' % rewards_img_name)

  #plt.show ()



# Translated from test_mcts_mdp_nx.m
def main ():

  rospy.init_node ('mcts_mdp', anonymous=True)

  # Make a predictable seed, to compare btw Python and original matlab code
  #   Never mind can't compare, because discretesample() is defined differently
  #   btw matlab and python.
  #np.random.seed (0)


  # Algo params
  params = MCTS_Parameters ()
  params.nSims = 10 #1000
  # Depth of tree / total number of allowed actions
  params.horizon = 5
  # exploration/exploitation constant for UCB1
  params.explore = 1
  params.img_dir = os.path.join (params.img_dir, 'mcts_basic')
  if not os.path.exists (params.img_dir):
    os.makedirs (params.img_dir)


  # Number of states
  NS = 5
  # Number of actions
  NA = 3

  mdp = MDP ()
  # Values in this array are used as indices to access mdp.stateTransition(a,,)
  mdp.actions = range (0, NA)

  mdp.stateTransition = np.zeros ((NA, NS, NS))

  # Sample random state transition matrices
  for a in range (0, NA):

    # Pick NS x NS random states, in range [0:1]
    T = np.random.rand (NS, NS)

    # sum(,0) is add column downwards, sum(,1) is add row sideways
    # Divide by row vector of column sums. Then each column np.sum(T,0) should
    #   sum to 1.
    T = T / np.sum (T, 0)

    mdp.stateTransition [a, :, :] = T

  print ('State transitions probabilities:')
  print (mdp.stateTransition)
  print ('')

  # Takes a column out of the 3D matrix MDP.StateTransition. The column index is
  #   a list of possible next states sn's, based on current state s and action a
  #   taken.
  #   Column values are probabilities that sum to 1.
  # Passes the column vector to discretesample(), which picks out 1 random
  #   index from the vector, based on the probability values in the vector.
  # Returns a state to go to, based on state s and action a (which are used
  #   to index MDP.StateTransition matrix).
  # Orig matlab code:
  #   MDP.sampleState = @(s,a) discretesample_nx(transpose(MDP.StateTransition(:,s,a)),1);
  # Lambda functions http://www.secnetix.de/olli/Python/lambda_functions.hawk
  mdp.sampleState = lambda (s, a): \
    discretesample (mdp.stateTransition [a, :, s], 1)

  # Random reward: R(a, s', s) gives reward for executing action a in state s
  #   and ending at state s'.
  # Just a random 3D matrix: MDP.Reward = rand(NS,NS,NA);
  #   MATLAB code's dimensions are NS, NS, NA, NA being the slice.
  #   Numpy's slices are in first dimension.
  mdp.reward = np.random.rand (NA, NS, NS)
  print ('Rewards:')
  print (mdp.reward)
  print ('')

  # List of actions available at s
  # Here just set to all actions
  mdp.availableA = lambda s: mdp.actions


  s0 = 1

  # Execute MCTS
  # pa: distribution over possible actions
  # R: [params.num_simulations x 1] vector of rewards per simulation
  # tree = containers.Map representing the MCTS search tree with key values
  # corresponding to the state and depth, e.g., '1,0' means state 1 at depth
  # 0, i.e. the root 
  thisNode = MCTS_Basic (mdp, params)
  (pa, R) = thisNode.mcts_mdp (s0)


  plot_rewards (R, params)


  # Print node names
  vals = thisNode.tree_nodes.values ()
  print ('All %d node names:' % (len (vals)))
  for i in range (0, len (vals)):
    # vals[i] is a StateNode
    print ('%s' % vals [i].hashkey)


  # Visualize tree in pydot and save to file
  tree_img_name = os.path.join (params.img_dir, 'tree_vis.png')
  thisNode.tree_vis.write_png (tree_img_name)
  print ('Pydot tree visualization saved to %s' % tree_img_name)



if __name__ == '__main__':
  main ()



'''
# Playing with rv_discrete to see how to translate discretesample_nx() into
#   Python.

from scipy import stats
import matplotlib.pyplot as pl

xk3=range (0, 6)
pk=np.random.rand (2,3)
# Make each column sum to 1
pk /= np.sum (pk, 0)
custm = rv_discrete (name='custm', values=(xk3, pk))

# You'd get something like this:
#  array([1, 0, 1, 0, 1, 0, 1, 0, 0, 1])
# because each column sums to 1 already. So it never goes beyond the first
#   column of 2 elements.
# That's why rv_discrete() wouldn't work for a matrix whose columns each sum
#   to 1. So we have to separate each column and pass individual columns to
#   rv_discrete. The matlab code actually does the same - it passes individual
#   columns to discretesample_nx, not the whole matrix at once!
custm.rvs(size=10)


fig, ax = pl.subplots(1, 1)
ax.plot(xk3, custm.pmf(xk3), 'ro', ms=12, mec='r')
ax.vlines(xk3, 0, custm.pmf(xk3), colors='r', lw=4)
pl.show()


# This doesn't work. I don't know how to pass in something in values to make
#   2D matrix, each column is a distribution, work.
# So you'd have to make NA * NS rv_discrete variables... each one for a column
#   in mdp.stateTransition. Or just make each one on the fly when you need it!
#   Yeah that's easier... isn't that what discretesample() pretty much does?
#   it's a function pointer... so it must just processes the data right away
#   each time.
#custm = rv_discrete (name='custm', values=(xk3, pk), shapes='m, n')
'''

