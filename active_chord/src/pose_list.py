#!/usr/bin/env python

# Mabel Zhang
# 10 Jan 2016
#
# Refactored from prob_writer.py and prob_reader.py
#

from copy import deepcopy

import numpy as np

# My packages
from util_continuum.mat_util import find_dups_row, find_dups_multirow_onelist, \
  find_dups_self
from util_continuum.ansi_colors import ansi_colors


DEBUG_DUPS = False
DEBUG_DISCRETIZE = False

# Parent class
# An action is a transformation between two absolute poses.
# Child class needs to implement:
#   check_dup_and_add_pose_obsl ()
#   merge_with_instance ()
class PoseList:

  # Parameters:
  #   dim: dimension of a single observation
  # Ref: will be called automatically if child class does not have __init__,
  #   otherwise call with super()
  #   http://stackoverflow.com/questions/1892269/how-do-derived-class-constructors-work-in-python
  def __init__ (self, dim):

    # |P| x 3. Integer names for absolute poses
    #   Each row is a [x, y, z] unit vector, normal to cross sectn plane
    self.abs_poses = np.zeros ((0, dim))
    # |P| x 1. Index indexes self.abs_poses. Value indexes
    #   ObservationList.obsl_list - if using data stored from prob_writer.py,
    #   which makes ObservationList correspond to PoseList. If you are loading
    #   them from separate sources or artificially merge_with_instance() on each,
    #   then ObservationList and PoseList may not correspond.
    #   List of list of observation-list indices, i.e. what observations were
    #     made at this pose.
    #   e.g. [[1], [2], [3], [4, 5], [6]], the integers index obsl_list, each
    #     one gets you a list of chords.
    self.pose_obsl = []


    '''
Chords (observation): c1 c2 c3
  ObservationList.single_obs is [c1, c2, c3, ...]
  [[0.2, 0.2374, 0.4892, 0.473, 0.124, 0.345, 0.9872], ...]
  nChords x 7 array
Observation-list: Collection of chords. o1 = [c1, c3], o2 = [c2, c3]
  ObservationList.obsl_list is [o1, o2, ...]
  [[40, 37, 26], [27, 38, 21, 28, 30], ...]
Observation-list list: A pose may have multiple observations, `.` we discretize
  poses. p1 = [o1, o2], p2 = [o3, o4]
  PoseList.pose_obsl is [p1, p2, ...]
  [[1], [2], [3], [4, 5], [6], ...]
    '''


  # NOTE: If you add fields to this object class, then add fields here!
  def reset_vars (self, abs_poses, pose_obsl):

    self.abs_poses = abs_poses
    self.pose_obsl = pose_obsl


  def get_n_poses (self):
    return self.abs_poses.shape [0]

  def get_idxes (self):
    # Integer names (i.e. indices) of rows in self.abs_poses
    return np.arange (0, self.abs_poses.shape [0], 1)

  def get_pose_i (self, i):
    if i is not None:
      return self.abs_poses [i, :]
    else:
      return None

  def get_obsl_i (self, i):
    if i is not None:
      return self.pose_obsl [i]
    else:
      return None

  # Get a pose and its corresponding obsl. These two must correspond! (That's
  #   why this is its own function. In case you change other functions or
  #   data structures later. This function needs to make sure the obsl returned
  #   was made at the pose. To make sure of this correspondence, creator of
  #   this class instance needs to ensure that - see prob_writer.py.)
  def get_pose_obsl_i (self, i):
    if i is not None:
      return (self.abs_poses [i, :], self.pose_obsl [i])
    else:
      return None


  def set_obsl_i (self, i, obsl):
    if i is not None:
      self.pose_obsl [i] = obsl


  # Return indices of poses that had observation-list obsl in self.pose_obsl
  def find_poses_with_obsl (self, obsl):

    poses_i = np.zeros ((0, ))
    poses = np.zeros ((0, self.abs_poses.shape [1]))

    # Loop through each pose
    for i in range (self.get_n_poses ()):
      #print ('Looking to see if pose %d (self.pose_obsl [%d]):' % (i, i))
      #print (self.pose_obsl [i])
      #print ('  has obsl z0:')
      #print (obsl)

      # If this pose's observation-list matches the query obsl, then add pose
      #   to ret val
      if obsl in self.pose_obsl [i]:
        #print ('%sYes, pose %d has obsl z0%s' % (
        #  ansi_colors.UNDERLINE, i, ansi_colors.ENDC))
        poses_i = np.append (poses_i, i)
        poses = np.append (poses, [self.abs_poses [i, :]], axis=0)

    return poses_i, poses



class PoseList_Chords (PoseList):

  # ======================================= add new abs_poses and pose_obsl ==

  # Add a single pose and its obsl to this object instance.
  # Parameters:
  #   abs_pose: 3-vector 1D numpy array
  #   obsl_idx: Index of observation-list that is observed at this pose. This
  #     is an index returned by one of the add*() functions in ObservationList
  #     class. It is an integer name for some observation-list.
  def check_dup_and_add_pose_obsl (self, abs_pose, obsl_idx):

    # Check if incoming abs_pose is a duplicate of an existing one in
    #   self.abs_poses.
    # Indexes self.abs_poses, self.pose_obsl.
    dup_idxes = find_dups_row (abs_pose, self.abs_poses)

    # This indexes self.abs_poses
    pose_dup_idx = -1

    # Abs pose is a duplicate, append the observation to its existing list
    if dup_idxes.size > 0:
      # Assumption: Just take first one. There should only be 1 anyway, `.`
      #   we do dup check for every pose!
      pose_dup_idx = dup_idxes [0]

      if DEBUG_DUPS:
        print ('%sPose IS a duplicate:%s' % (ansi_colors.LOW_GREEN, ansi_colors.ENDC))
        print (abs_pose)

      # Check if the observations are duplicates
      for i in obsl_idx:
        if i not in self.pose_obsl [pose_dup_idx]:
          # Append the observation number to the pose's existing list
          self.pose_obsl [pose_dup_idx].append (i)
          if DEBUG_DUPS:
            print ('Adding obsl_idx %d to duplicated pose %d (self.pose_obsl [%d])' % (i, pose_dup_idx, pose_dup_idx))

    # No duplicates. Add this abs_pose
    else:
      if DEBUG_DUPS:
        print ('%sPose IS NOT a duplicate:%s' % (ansi_colors.MAGENTA, ansi_colors.ENDC))
        print (abs_pose)

      # Grab size BEFORE appending new pose
      pose_dup_idx = self.abs_poses.shape [0]
      # Append this pose and its observation in self.abs_poses and
      #   self.pose_obsl
      '''
      print ('Before adding:')
      print ('self.abs_poses:')
      print (self.abs_poses)
      print ('self.pose_obsl:')
      print (self.pose_obsl)
      '''
      self.abs_poses = np.append (self.abs_poses, [abs_pose], axis=0)
      self.pose_obsl.append (obsl_idx)

      '''
      print ('After adding:')
      print ('self.abs_poses:')
      print (self.abs_poses)
      print ('self.pose_obsl:')
      print (self.pose_obsl)
      '''

      #print ('Pose added:')
      #print (abs_pose)

    #print ('Poses so far: ')
    #print (self.abs_poses)

    return pose_dup_idx


  # ========================================= operation on all three arrays ==

  # NOTE: If you add fields to this object class, then merge the new fields
  #   in this function!
  def merge_with_instance (self, that_inst):

    n_that_inst = that_inst.get_n_poses ()
    that_inst_idxes = np.zeros ((n_that_inst, ), dtype=int)

    # Loop through nPoses
    for i in range (n_that_inst):

      # Add that pose and its obsl-index, into this instance
      # Returns scalar. The value indexes self.abs_poses.
      that_inst_idxes [i] = self.check_dup_and_add_pose_obsl (
        that_inst.abs_poses [i, :], that_inst.pose_obsl [i])

    # Index indexes that_inst. Value indexes this instance (self).
    return that_inst_idxes


  # ========================================= Obsl integer name replacement ==

  # Parameters:
  #   old_to_new_obsl_map: Returned from the corresponding
  #     ObserationList.discretize_to_hist_centers()
  def replace_pose_obsl (self, old_to_new_obsl_map):

    print ('%sReplacing pose_obsl after discretizing observations to histogram centers...%s' % (
      ansi_colors.OKCYAN, ansi_colors.ENDC))

    # To replace self.pose_obsl
    # List of list of observation-list indices, i.e. what observations were
    #   made at this pose.
    new_pose_obsl = []

    for p_i in range (len (self.pose_obsl)):

      curr_pose_obsl = []

      #print ('Before replacing:')
      #print (self.pose_obsl [p_i])

      for old_obsl_i in self.pose_obsl [p_i]:

        # Replace old obsl_i with new one. Add to new list
        new_obsl_i = old_to_new_obsl_map [old_obsl_i]
        curr_pose_obsl.append (new_obsl_i)

        if DEBUG_DISCRETIZE:
          print ('Replaced old_obsl_i %d with new_obsl_i %d' % (old_obsl_i,
            new_obsl_i))

      new_pose_obsl.append (curr_pose_obsl)

      #print ('After replacing:')
      #print (new_pose_obsl [p_i])

    self.pose_obsl = new_pose_obsl


    if DEBUG_DISCRETIZE:
      raw_input ('Press enter')


